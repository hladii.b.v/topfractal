@extends('layouts.new-dashboard')

@section('title')
Dashboard | Text Library
@endsection

@section('content')

<div class="dashboard-content">
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-document"> {{Lang::get('messages.Text')}}</h1>
		</div><!-- /.col -->
		<div class="col-md-12">
			<div class="uploadPanel">				
				<div class="add-listing">
					<form action="{{route('text.add', app()->getLocale())}}" enctype="multipart/form-data" method="post">
						<div class="add-listing-section">
	 
							<!-- Title -->
							<div class="row with-forms">
								<div class="col-md-12 margin-top-15">
									<h5>{{Lang::get('messages.description')}}:</h5>
						            <input type="text" class="search-field" id="textName" name="desc" value="">
								</div>
							</div>
	
							<!-- Row -->
							<div class="row with-forms">
	
								<!-- Status -->
								

								<div class="col-md-5">
									<select class="chosen-select-no-single"  name="direction_id" >
										@foreach($directions as $key=>$direction)
											<option value="{{$direction->id}}" selected>{{$direction->name}}</option> 
										@endforeach
									</select>
									
								</div>

								<div class="col-md-5">
									<select  name="text_type" >
										<option value="organizationName" selected>{{Lang::get('messages.Name_of_the_clinic_message')}}</option> 
										<option value="headerTitle" >Header title</option> 
										
										<option value="headerText" >{{Lang::get('messages.header')}}</option> 
										<option value="aboutText" >{{Lang::get('messages.about')}}</option>
										<option value="benefitText"> Benefit text</option>
										<option value="benefitMotto" >Benefit Motto</option> 
										<option value="reviewsText">{{Lang::get('messages.reviews')}}</option>
										<option value="adressText">{{Lang::get('messages.adress')}}</option>
										<option value="teamText"> Team text</option>
										<option value="teamMotto" >Team Motto</option> 
									</select>

									
								</div>
	
								<!-- Type -->
								<div class="col-md-2">
									<select class="chosen-select-no-single"  name="text_lang">
										<option value="ua" selected>UA</option> 
										<option value="ru" >RU</option> 
									</select>
								</div>
	
							</div>
							<!-- Row / End -->
	
						</div>
						<input type="hidden" name="_token" value="{{ Session::token() }}">
						<button type="submit" class="button preview"> {{Lang::get('messages.create')}} </button>
					</form>
				</div><!-- /.bottom -->
			</div><!-- /.uploadPanel -->
		</div><!-- /.col -->
		<div class="col-md-12 margin-top-50">		

			<div class="style-2">
				<!-- Tabs Navigation -->
				<ul class="tabs-nav">
					<li class="active" ><a href="#NameTab">organizationName</a></li>	
					<li><a href="#HeaderTitleTab">Header title</a></li>						
					<li><a href="#HeaderTab"> {{Lang::get('messages.header')}}</a></li>				
					<li><a href="#AboutTab"> {{Lang::get('messages.about')}}</a></li>
					<li ><a href="#BenefitMottoTab">Benefit Motto</a></li>	
					<li><a href="#BenefitTab"> Benefit text</a></li>
					<li><a href="#ReviewsTab"> {{Lang::get('messages.reviews')}}</a></li>
					<li><a href="#AdressTab"> {{Lang::get('messages.adress')}}</a></li>
					<li ><a href="#TeamMottoTab">Team Motto</a></li>	
					<li><a href="#TeamTab"> Team text</a></li>
				</ul>

				<!-- Tabs Content -->
				<div class="tabs-container">
					
					<div class="tab-content active" id="NameTab">
						<div  id="NameTabText">
							@foreach ($organizationName as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach							
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->
	
					<div class="tab-content " id="HeaderTitleTab">
						<div  id="TitleTab">
							@foreach ($headerTitle as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
						
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->
	
					
	
					<div class="tab-content " id="HeaderTab">
						<div  id="HeaderText">
							@foreach ($headerText as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
						
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->
					
					<div class="tab-content " id="AboutTab">
						<div id="AboutText">
							@foreach ($aboutText as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
							
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->
	

					<div class="tab-content " id="BenefitMottoTab">
						<div  id="MottoTab">
							@foreach ($benefitMotto as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
						
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->

					<div class="tab-content " id="BenefitTab">
						<div id="BenefitText">
							@foreach ($benefitText as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
						
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->
	
					<div class="tab-content " id="ReviewsTab">
						<div id="ReviewsText">
							@foreach ($reviewsText as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
							
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->
	
					<div class="tab-content " id="AdressTab">
						<div  id="AdressText">
							@foreach ($adressText as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
						
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->

					<div class="tab-content " id="TeamMottoTab">
						<div  id="TeamMotto">
							asdfasd
							@foreach ($teamMotto as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
						
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->

					<div class="tab-content " id="TeamTab">
						<div id="TeamText">
							@foreach ($teamText as $item)
								<div class="imgTabContainer">
									<div class="imgDirectionName">
										<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
										<h4 ><a onclick="myFunction(this)"> show</a></h4>
									</div>
									<div class="imgDirectionPic">
									@if( count($item['direction_items']) > 0 )
										<div class="images masonry-3" id="aboutImages">
											@foreach( $item['direction_items'] as $admin_tex)
											<li  class="list-group-item">
												<div id='updateData{{$admin_tex->id}}' contenteditable="true"  onfocusout="updateData( {{$admin_tex->id}})">{{$admin_tex->desc}}</div>
												
												<span>{{$admin_tex->lang}}</span>
												<a href="{{route( 'text-delete', ['locale'=>app()->getLocale(), 'benefit'=>$admin_tex->id] ) }}" class="btn btn-danger">X</a>
												
											</li>
												
											@endforeach
										</div>
									@else
										<!-- Alert Info -->
										<div class="alert alert-info">
											<button type="button" class="close fui-cross" data-dismiss="alert"></button>
											Наразі не завантажено жодного тексту
										</div>
									@endif
									</div>
								</div>
							@endforeach	
						
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content -->
				</div>
			</div>

		</div>
	</div><!-- /.row -->
</div><!-- /.container -->


<script type="text/javascript">
	var token = '{{ Session::token() }}';
</script>

<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
		position: relative;
		display: flex;
		padding: 15px 15px;
		margin-bottom: -1px;
		background-color: #fff;
		border: 1px solid #ddd;
		justify-content: space-between;
		align-items: center;
	}
	.list-group-item div {
		width: 80%;
	}
	.masonry-3 {
		width: 100%
	}
</style>

<script>
	function myFunction (element) {
		
		var name = element.parentElement;
		var list =name.parentElement;
        var parent =list.parentElement;
        var child =parent.lastElementChild;
		console.log(child);		
		
		
        if (child.style.display === 'flex') {
            child.style.display ='none';
        }
        else {
            child.style.display ='flex'; 
        }  	    		
	}
	
	function updateData (id) {
		element =document.getElementById("updateData"+id).innerHTML 

	
		$.ajax({
		    type: 'GET', 
		    url:  window.location.origin+'/benefits-update/'+id,
			data: 'desc='+element,
		    error: function() { 
		         console.log(data);
		    }
		})
	}

</script>

@endsection