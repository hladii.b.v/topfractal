@extends('layouts.new-dashboard')

@section('title')
Dashboard | Domain
@endsection

@section('content')


<div class="dashboard-content">
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="dashboard-list-box margin-top-0">				

				<h4>{{Lang::get('messages.Domains')}}</h4>
				<ul>
					@foreach ($data as $value)
						<li class="pending-booking">
							<div class="list-box-listing bookings">
								<div class="list-box-listing-content">
									<div class="inner">
										<h3>{{Lang::get('messages.name')}}: {{$value->site_name}}</h3>

										<div class="inner-booking-list">
											<h5>{{Lang::get('messages.domain')}}:  </h5>
											<ul class="booking-list">
												<li class="highlighted"><a href="http://{{$value->remote_url}}" target="_blank">{{$value->remote_url}}</a></li>
											</ul>
										</div>												
										
									</div>
								</div>
							</div>
							
						</li>
					@endforeach
				</ul>
			</div>
		</div>		
		
	</div><!-- /.row -->
	
	
</div><!-- /.container -->


	

@endsection