
@extends('layouts.new-dashboard')

@section('title')
Dashboard | My_Account
@endsection

@section('content')

<div class="dashboard-content"> 

    <div class="row">

        <!-- Profile -->
        <div class="col-lg-6 col-md-12">
            <div class="dashboard-list-box margin-top-0">
                <h4 class="gray">{{Lang::get('messages.My_Account')}}</h4>
                <div class="dashboard-list-box-static">
                    
                    <form action="{{route('user.uaccount')}}" method="POST"  enctype="multipart/form-data">
                        @csrf
                    <!-- Avatar -->
                        <div class="edit-profile-photo"  id="photoUpload">
                            <img id="blah" src="{{ Auth::user()->avatar }}" alt="">

                            <div class="change-photo-btn">
                                <div class="photoUpload">
                                    <span><i class="fa fa-upload"></i>{{Lang::get('messages.Upload_Image')}} </span>
                                    <input id="imgInp" type="file" name="imgInp" class="upload"  value="{{ Auth::user()->avatar }}"/>
                                </div>
                            </div>
                        </div>

                    <!-- Details -->                   
                        
                        <div class="my-profile">
                            <input type="hidden" name="userID" value="{{ Auth::user()->id }}">
                            <label>{{Lang::get('messages.First_name')}}</label>
                            <input type="text"  name="firstname" placeholder="{{Lang::get('messages.First_name')}}" value="{{ Auth::user()->first_name }}">

                            <label>{{Lang::get('messages.Last_name')}}</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="{{Lang::get('messages.Last_name')}}" value="{{ Auth::user()->last_name }}">
                                        
                        </div>

                        <button class="button margin-top-15">{{Lang::get('messages.Update_Details')}}</button>
                    </form>

                </div>
            </div>
        </div>

        <!-- Change Password -->
        <div class="col-lg-6 col-md-12">
            <div class="dashboard-list-box margin-top-0">
                <h4 class="gray">{{Lang::get('messages.Account')}}</h4>
                <div class="dashboard-list-box-static">

                    <!-- Change Password -->
                    <form action="{{route('user.ulogin')}}" method="POST"  enctype="multipart/form-data">
                        @csrf
                        <div class="my-profile">

                            <label>{{Lang::get('messages.email')}}</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="{{Lang::get('messages.email')}}" value="{{ Auth::user()->email }}">


                            <label class="margin-top-0">{{Lang::get('messages.password')}}</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="{{Lang::get('messages.password')}}" value="">


                            <button class="button margin-top-15">{{Lang::get('messages.Update_Details')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>   
    </div>
</div><!-- /.container -->
<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>	
<script>
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var photoUpload = document.getElementById('photoUpload')
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });
</script>
@endsection