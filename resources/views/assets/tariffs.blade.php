@extends('layouts.new-dashboard')

@section('title')
Dashboard | Tariffs
@endsection

@section('content')

<div class="dashboard-content"> 
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-window"> {{Lang::get('messages.tariffs')}}</h1>
		</div><!-- /.col -->
    </div><!-- /.row -->	    
	<div class="row">	
		<div class="col-md-12">
			<div class="uploadPanel">				
				<div class="add-listing">
					<form method="POST" action="{{route('tariffs.add', app()->getLocale())}}">
						@csrf
						<div class="add-listing-section padding-top-25">				
							<div class="row with-forms">
								<div class="col-md-4">
									<label for="username" class="col-md-3 control-label">Тариф:</label>
									<select  id="tariff_id" name="tariff_id" class="chosen-select-no-single">
										<option value="1" selected> {{Lang::get('messages.tariffs_Basic')}}</option>
										<option value="2" > {{Lang::get('messages.tariffs_Extended')}}</option>
										<option value="3" >{{Lang::get('messages.tariffs_Professional')}}</option>
									</select>
								</div>
								<div class="col-md-2">
									<label for="tariff_type" class="col-md-3 control-label">Тип:</label>
									<select  id="tariff_type" name="tariff_type" class="chosen-select-no-single">
										<option value="text" selected>Text</option>
										<option value="price">Price</option>
									</select>
								</div>
								<div class="col-md-2">
									<label for="tariff_lang" class="col-md-3 control-label">Мова:</label>
									<select  id="tariff_lang" name="tariff_lang" class="chosen-select-no-single">
										<option value="ua" selected>Ua</option>
										<option value="ru">Ru</option>
									</select>
								</div>								
								<div class="col-md-2">
									<h5>Показкувати?:</h5>
									<label class="switch"><input type="checkbox" checked=""><span class="slider round"></span></label>								
								</div>
								<div class="col-md-10">
									<label for="tariff_value" class="col-md-3 control-label">Значення:</label>
									<input  id="tariff_value" name="tariff_value" type='text'  class="form-control">
								</div>
								<div class="col-md-2">
									<button type="submit" class="button preview"> {{Lang::get('messages.create')}} </button>

								</div>
							</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-12 margin-top-30">
			<ul >
				@foreach ($tariffs as $key=>$tariff)
                    <li  class="list-group-item">
                        <a onclick="myFunction( {{$key}} ) " ><i class="fa fa-arrow-down"></i></a> 
                        <div> {{Lang::get('messages.tariffs')}} {{$key}}</div>
                        <div class="fileinput fileinput-new bebefitFlex" data-provides="fileinput">                           
                            <h5>{{ $key==1 ? Lang::get('messages.tariffs_Basic') : ( $key==2 ?Lang::get('messages.tariffs_Extended') : Lang::get('messages.tariffs_Professional') )}}</h5>                                                     
                        </div>	                        
                    </li>
                    <div id="elementService{{$key}}" style="display: none">
                        <ul >                        
                            @foreach ($tariff['tariff_price'] as $tariff_key=>$value)
                            
                            <li  class="list-group-item">
                                <div >Price {{$tariff_key}}</div>
                                <input type="text" id='updateData{{$value['id']}}' value="{{$value['value']}}" >
                                <a onclick="updateData( {{$value['id']}})">Send</a>	        
                            </li>                   
                            @endforeach
                        </ul>
                        <ul >                        
                            @foreach ($tariff['tariff_text'] as  $tariff_key=>$value)
                            
                            <li  class="list-group-item">
                                <span >Text {{$tariff_key}}</span>
								<input type="text" id='updateData{{$value['id']}}' class="tariff_text" value="{{$value['value']}}" >
								
								<label class="switch"><input id="tariff_show{{$value['id']}}"  type="checkbox" {{$value['show']==1 ? 'checked' : ''}}><span class="slider round"></span></label>
                                <a onclick="updateData( {{$value['id']}})">Send</a>	  
								<span >{{$value['lang']}}</span>      
								<a href="{{route( 'tariff-delete', ['locale'=>app()->getLocale(), 'tariff'=>$value['id'] ]) }}" class="btn btn-danger">X</a>
								
                            </li>                   
                            @endforeach
                        </ul>
                    </div>                        
			    @endforeach
			</ul>
		</div>
	</div>	
	
</div><!-- /.container -->


<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
		position: relative;
		display: flex;
		padding: 15px 15px;
		margin-bottom: -1px;
		background-color: #fff;
		border: 1px solid #ddd;
		justify-content: space-around;
		align-items: center;
	}
	.list-group-item input {
		width: 70%;
	}
</style>
<script type="text/javascript">
	function myFunction (id) {
	
        element =document.getElementById("elementService"+id)
        display = element.style.display
        if (display === 'none') {
            element.style.display ='block';
        }
        else {
            element.style.display ='none'; 
        }      		
	}

	function updateData (id) {
		element =document.getElementById("updateData"+id) 
		show =document.getElementById("tariff_show"+id)
		if (show.checked == true){
			toShow = 1;
		} else {
			toShow = 0 ;
		}
        color = element.value
     
		$.ajax({
		    type: 'GET', 
		    url:  window.location.origin+'/en/tariffs-update',
			data: {id: id, desc: color, show: toShow},
		    error: function() { 
		         console.log(color);
		    }
		})
	}
</script>


@endsection