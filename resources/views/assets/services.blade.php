@extends('layouts.new-dashboard')

@section('title')
Dashboard | Services
@endsection

@section('content')

<div class="dashboard-content"> 
	<div class="row margin-bottom-30">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-window"> {{Lang::get('messages.Services')}}</h1>
		</div><!-- /.col -->
	</div><!-- /.row -->
	
	<div class="row">
		
		<div class="col-md-12">
			<div class="uploadPanel">				
				<div class="add-listing">
					<h3> {{Lang::get('messages.createServices')}}</h3>
					<form method="POST" action="{{route('services.add', app()->getLocale())}}">
						@csrf
						<div class="add-listing-section padding-top-25">							
							
							<div class="row with-forms">
								<div class="col-md-7">
									<label for="username" class="col-md-3 control-label">  {{Lang::get('messages.name')}}:</label>
									<input type="text" class="form-control" id="name" name="desc" placeholder=" {{Lang::get('messages.name')}}" value="">
								</div>
								<div class="col-md-2">
									<label for="direction_id" class="col-md-3 control-label">  Напрямок:</label>
									<select  id="direction_id" name="direction_id" >										
										@foreach ($directions as $direction)
											<option value="{{$direction->id}}">{{$direction->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-2">
									<label for="parent_id" class="col-md-3 control-label">  {{Lang::get('messages.parent')}}:</label>
									<select  id="parent_id" name="parent_id">
										<option value="0" selected> {{Lang::get('messages.no_parent')}}</option>
										@foreach ($servicesAll as $service)
											<option value="{{$service->id}}">{{$service->desc}} ({{$service->lang}})</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-1">
									<label for="text_lang" class="col-md-3 control-label">Мова:</label>
									<select id="text_lang" name="text_lang" class="chosen-select-no-single">
										<option value="ua" selected>UA</option> 
										<option value="ru" >RU</option> 
									</select>
								</div>
							</div>
						
							 <button  class="button preview"> {{Lang::get('messages.create')}}</button>
							
						</div>
							
					</form>
				</div>
			</div>
		</div>
	</div>
		

	<div class="row  margin-top-30">
		<div class="col-md-12">
			<ul class="list-group">
				@foreach ($services as $item)
					<div class="imgTabContainer">
						<div class="imgDirectionName">
							<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
							<h4 ><a onclick="myFunctionShow(this)"> show</a></h4>
						</div>
						<div class="imgDirectionPic">
							@if( count($item['direction_items']) > 0 )
								<div class="images masonry-3" id="aboutImages">
									@foreach( $item['direction_items'] as $benefit)
										<li  class="list-group-item">
											<a onclick="myFunction( {{$benefit->id}} ) " ><i class="fa fa-arrow-down"></i></a>
											<div id='updateData{{$benefit->id}}' contenteditable="true"  onfocusout="updateData( {{$benefit->id}})">{{$benefit->desc}}</div>
														
											
											<span>{{$benefit->lang}}</span>
											<a href="{{route( 'benefits-delete', ['locale'=>app()->getLocale(), 'benefit'=>$benefit->id] ) }}" class="btn btn-danger">X</a>
										</li>
										<ul id="elementService{{$benefit->id}}" style="display: none">
										
										</ul>
									@endforeach
								</div>	
							@else
								<!-- Alert Info -->
								<div class="alert alert-info">
									<button type="button" class="close fui-cross" data-dismiss="alert"></button>
									Наразі не завантажено жодного тексту
								</div>

							@endif
						</div>
					</div>						
				@endforeach
			</ul>
		</div>
	</div>
	
</div><!-- /.container -->


<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
		position: relative;
		display: flex;
		padding: 15px 15px;
		margin-bottom: -1px;
		background-color: #fff;
		border: 1px solid #ddd;
		justify-content: space-between;
		align-items: center;
	}
	.masonry-3 {
		width: 100%
	}
</style>

<script type="text/javascript">

	function myFunctionShow (element) {
		
		var name = element.parentElement;
		var list =name.parentElement;
        var parent =list.parentElement;
        var child =parent.lastElementChild;
		console.log(child);		
		
		
        if (child.style.display === 'flex') {
            child.style.display ='none';
        }
        else {
            child.style.display ='flex'; 
        }  	    		
	}
	function myFunction (id) {
		
		$.ajax({
		    type: 'GET', 
		    url:  window.location.origin+'/services-parent/'+id,
		    error: function() { 
		         console.log(data);
		    }
		}).done(function(data){
			var listContainer = '';
			 data.reverse().forEach((data, i) => {
		           listContainer+=`
				   <li  class="list-group-item">
					<div id='updateData${data.id} ' contenteditable="true">${data.desc} </div>
					<a onclick="updateData( ${data.id}  )">Send</a>	
					<span>${data.lang}</span>	
				   <a href="${window.location.origin}/en/services-delete/${data.id}" class="btn btn-danger">X</a></li>`					
		   		
	       })
		   element =document.getElementById("elementService"+id)
		   display = element.style.display
		   if (display === 'none') {
				element.style.display ='block';
		   }
		   else {
			    element.style.display ='none'; 
		   }
		   element.innerHTML=listContainer;
		})
	}
	function updateData (id) {
		element =document.getElementById("updateData"+id).innerHTML 
		
		$.ajax({
		    type: 'GET', 
		    url:  window.location.origin+'/benefits-update/'+id,
			data: 'desc='+element,
		    error: function() { 
		         console.log(data);
		    }
		})
	}
</script>
	

@endsection