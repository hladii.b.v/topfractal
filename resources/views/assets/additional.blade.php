@extends('layouts.new-dashboard')

@section('title')
Dashboard | Tariffs
@endsection

@section('content')

<div class="dashboard-content"> 
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-window"> {{Lang::get('messages.tariffs')}}</h1>
		</div><!-- /.col -->
    </div><!-- /.row -->	    
	<div class="row">	
		<div class="col-md-12">
			<div class="uploadPanel">				
				<div class="add-listing">
					<form method="POST" action="{{route('tariffs.add', app()->getLocale())}}">
						@csrf
						<div class="add-listing-section padding-top-25">				
							<div class="row with-forms">
								<div class="col-md-4">
									<label for="tariff_value" class="col-md-3 control-label">Значення:</label>
									<input  id="tariff_value" name="tariff_value" type='text'  class="form-control">
								</div>
								<div class="col-md-2">
									<label for="tariff_type" class="col-md-3 control-label">Тип:</label>
									<select  id="tariff_type" name="tariff_type" class="chosen-select-no-single">
										<option value="text" selected>Text</option>
										<option value="price">Price</option>
									</select>
								</div>
								<div class="col-md-2">
									<label for="tariff_lang" class="col-md-3 control-label">Мова:</label>
									<select  id="tariff_lang" name="tariff_lang" class="chosen-select-no-single">
										<option value="ua" selected>Ua</option>
										<option value="ru">Ru</option>
									</select>
								</div>								
								<div class="col-md-2">
									<h5>Показкувати?:</h5>
									<label class="switch"><input type="checkbox" checked=""><span class="slider round"></span></label>								
								</div>
								
                            </div>
                            <button type="submit" class="button preview"> {{Lang::get('messages.create')}} </button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-12 margin-top-30">
			<ul >
                
				@foreach ($services as $service)
                    <li  class="list-group-item">                      
                        
                        <div class="service-name">                           
                            <h5 contenteditable="true" onfocusout="changeServices(this)" data-id="{{$service->id}}" data-key="name">
                                {{$service->name}}
                            </h5>                                                     
                        </div>	
                        <div class="service-desk">                           
                            <h5 contenteditable="true" onfocusout="changeServices(this)" data-id="{{$service->id}}" data-key="desk">
                                {{$service->desk}}
                            </h5>                                                     
                        </div>
                        <div class="service-price">                           
                            <h5 contenteditable="true" onfocusout="changeServices(this)" data-id="{{$service->id}}" data-key="price">
                                {{$service->price}}
                            </h5>                                                     
                        </div>
                        <div class="service-lang">                           
                            <h5>
                                {{$service->lang}}
                            </h5>                                                     
                        </div>
                        <div class="service-show">                           
                            <label class="switch">
                                <input id="tariff_show13" type="checkbox" {{$service->show==1 ? 'checked' : ''}}>
                                <span class="slider round"></span>
                            </label>                                                   
                        </div>  
                        <a onclick="deleteServices({{$service->id}})" class="btn btn-danger">X</a>                      
                    </li>                       
			    @endforeach
			</ul>
		</div>
	</div>	
	
</div><!-- /.container -->


<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
		position: relative;
		display: flex;
		padding: 15px 15px;
		margin-bottom: -1px;
		background-color: #fff;
		border: 1px solid #ddd;
		justify-content: space-around;
		align-items: center;
	}
	.list-group-item input {
		width: 70%;
    }
    h5 {
        padding: 10px;
    }
</style>
<script type="text/javascript">
	function changeServices (element) {
        var elementID = element.dataset.id 
        var key = element.dataset.key 
        var elementName = element.innerHTML;
       
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/template-update',
			data: {elementID: elementID, elementName: elementName},		   
            success:function(response) { 
                console.log(response);
            }
		})
    }

    function deleteServices (element) {
        var elementID = element
       
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/additional-delete',
			data: {elementID: elementID},		   
            success:function(response) { 
                console.log(response);
                document.location.reload();
            }
		})
    }
</script>


@endsection