@extends('layouts.new-dashboard')

@section('title')
Dashboard | Image Library
@endsection

@section('content')

<div class="dashboard-content">
	<div class="row">
		<div class="col-md-9 col-sm-8 margin-bottom-50">
			<h1><span class="fui-image"> {{Lang::get('messages.Library')}}</h1>
		</div><!-- /.col -->
	</div><!-- /.row -->

	<div class="row">
		<div class="col-md-3">
			<div class="uploadPanel">
				<div class="top">
					<span class="fui-upload"></span> &nbsp;<b> {{Lang::get('messages.Upload')}}</b>
				</div>
				<div class="bottom">
					<div class="add-listing-section margin-top-45">
						<!-- Dropzone -->
						<div class="submit-section">
							<form action="#" class="dropzone dz-clickable" id="ImageForm" enctype="multipart/form-data" method="post">							
								<div class="dz-default dz-message">
									<span><i class="sl sl-icon-plus"></i> Click here or drop files to upload</span>
								</div>								
							</form>
						</div>

					</div>
					
						<div class="form-group">
							<div class="fileinput fileinput-new" data-provides="fileinput">		
								
								<div class="col-md-12 margin-top-30">
									<select id="direction_id"  class="chosen-select-no-single">
										@foreach ( $directions as $direction)
											<option value="{{$direction->id}}">{{$direction->name}}</option> 
										@endforeach
									</select>
								</div>

								<div class="col-md-12 margin-top-30">
									<select id="block_name"  class="chosen-select-no-single">										
										<option value="about" >{{Lang::get('messages.about')}}</option>
										<option value="header_img"> Картинки хідера</option>
										<option value="header_logo"> Лого хідера</option>
									</select>
								</div>
																	
							</div>
						</div><!-- /.form-group -->						
						
						<button type="submit" class="button preview" onclick="ImageForm()"> {{Lang::get('messages.Upload')}} </button>
					
				</div><!-- /.bottom -->
			</div><!-- /.uploadPanel -->
		</div><!-- /.col -->

		<div class="col-md-9">
			@if( Session::has('success') )
			<div class="notification success closeable">
				<a  class="close" ></a>
				{{ Session::get('success') }}
			</div>
			@endif
			@if( Session::has('error') )
			<div class="notification error closeable">
				<a  class="close" ></a>
				{{ Session::get('error') }}
			</div>
			@endif
			<div class="style-2">
				<ul class="tabs-nav">
					
					<li class="active"><a href="#adminImagesTab" id="ie_admintab"> {{Lang::get('messages.Other_images')}}</a></li>
					<li><a href="#adminImagesAbout"> {{Lang::get('messages.about')}}</a></li>
					<li><a href="#headerImgImages"> Картинки хідера</a></li>
					<li><a href="#headerLogoImages"> Лого хідера</a></li>
				</ul> <!-- /tabs -->

				<div class="tabs-container">				

					<div class="tab-content" id="adminImagesTab">
						<div class="images masonry-3" id="adminImages">
							@if( isset($adminImages) )
								@foreach( $adminImages as $admin_img )
									<div class="image">
										<div class="imageWrap">
											<img src="{{ URL::to($admin_img) }}"  style="cursor:zoom-in"
											onclick="img_modal('{{ URL::to($admin_img) }}')"  data-toggle="modal" data-target="#previewModal">
											
										</div>
										<div class="buttonWrap">
											<a  class="button gray" data-id="{{ URL::to($admin_img) }}" data-toggle="modal" data-target="#deleteModal" onclick="deleteImgModal(this)">
												{{Lang::get('messages.Delete')}}</a>
										</div>
									</div><!-- /.image -->
								@endforeach
							@endif
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content-->

					<div class="tab-content" id="adminImagesAbout">
						@foreach ($aboutImages as $item)
							<div class="imgTabContainer">
								<div class="imgDirectionName">
									<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
									<h4 ><a onclick="myFunction(this)"> show</a></h4>
								</div>
								<div class="imgDirectionPic">
								@if( count($item['direction_pic']) > 0 )
									<div class="images masonry-3" id="aboutImages">
										@foreach( $item['direction_pic'] as $admin_img )
											<div class="image">
												<div class="imageWrap">
													<img src="{{ URL::to($admin_img) }}" style="cursor:zoom-in"
													onclick="img_modal('{{ URL::to($admin_img) }}')"  data-toggle="modal" data-target="#previewModal">
												</div>
												<div class="buttonWrap">
													<a  class="button gray" data-id="{{ URL::to($admin_img) }}" data-toggle="modal" data-target="#deleteModal" onclick="deleteImgModal(this)">
														{{Lang::get('messages.Delete')}}</a>										</div>
											</div><!-- /.image -->
											
										@endforeach
									</div>
								@else
									<!-- Alert Info -->
									<div class="alert alert-info">
										<button type="button" class="close fui-cross" data-dismiss="alert"></button>
										{{Lang::get('messages.upload_panel')}}
									</div>
								@endif
								</div>
							</div>
						@endforeach
						
					</div><!-- /.tab-content-->

					<div class="tab-content" id="headerImgImages">
						<div class="images masonry-3" id="benefitsImages">
							@foreach ($headerImgImages as $item)
							<div class="imgTabContainer">
								<div class="imgDirectionName">
									<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
									<h4 ><a onclick="myFunction(this)"> show</a></h4>
								</div>
								<div class="imgDirectionPic">
								@if( count($item['direction_pic']) > 0 )
									<div class="images masonry-3" id="aboutImages">
										@foreach( $item['direction_pic'] as $admin_img )
											<div class="image">
												<div class="imageWrap">
													<img src="{{ URL::to($admin_img) }}" style="cursor:zoom-in"
													onclick="img_modal('{{ URL::to($admin_img) }}')"  data-toggle="modal" data-target="#previewModal">
												</div>
												<div class="buttonWrap">
													<a  class="button gray" data-id="{{ URL::to($admin_img) }}" data-toggle="modal" data-target="#deleteModal" onclick="deleteImgModal(this)">
														{{Lang::get('messages.Delete')}}</a>										</div>
											</div><!-- /.image -->
											
										@endforeach
									</div>
								@else
									<!-- Alert Info -->
									<div class="alert alert-info">
										<button type="button" class="close fui-cross" data-dismiss="alert"></button>
										{{Lang::get('messages.upload_panel')}}
									</div>
								@endif
								</div>
							</div>
						@endforeach
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content-->

					

					<div class="tab-content" id="headerLogoImages">
						<div class="images masonry-3" id="reviewsImages">
							@foreach ($headerLogoImages as $item)
							<div class="imgTabContainer">
								<div class="imgDirectionName">
									<h4 > {{$item['direction_id']}} {{$item['direction_name']}}</h4>
									<h4 ><a onclick="myFunction(this)"> show</a></h4>
								</div>
								<div class="imgDirectionPic">
								@if( count($item['direction_pic']) > 0 )
									<div class="images masonry-3" id="aboutImages">
										@foreach( $item['direction_pic'] as $admin_img )
											<div class="image">
												<div class="imageWrap">
													<img src="{{ URL::to($admin_img) }}" style="cursor:zoom-in"
													onclick="img_modal('{{ URL::to($admin_img) }}')"  data-toggle="modal" data-target="#previewModal">
												</div>
												<div class="buttonWrap">
													<a  class="button gray" data-id="{{ URL::to($admin_img) }}" data-toggle="modal" data-target="#deleteModal" onclick="deleteImgModal(this)">
														{{Lang::get('messages.Delete')}}</a>										</div>
											</div><!-- /.image -->
											
										@endforeach
									</div>
								@else
									<!-- Alert Info -->
									<div class="alert alert-info">
										<button type="button" class="close fui-cross" data-dismiss="alert"></button>
										{{Lang::get('messages.upload_panel')}}
									</div>
								@endif
								</div>
							</div>
						@endforeach
						</div><!-- /.adminImages -->
					</div><!-- /.tab-content-->
				</div> <!-- /tab-content -->
			</div>
		</div><!-- /.col -->
	</div><!-- /row -->
</div><!-- /.container -->

<!-- Modals -->

<div class="modal fade" id="previewModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Перегляд</h4>
            </div>
            <div class="modal-body" style="background: #cecece;">
               <img src="" id="img_modal">
            </div>
            <div class="modal-footer">                
                
				
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
      
    </div>
</div>
{{-- delete --}}
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Видалення</h4>
            </div>
            <div class="modal-body">
                <p>ви дійсно хочете видлати картинку?</p>
            </div>
            <div class="modal-footer">                
                <form action="{{route('delImage')}}" method="POST">
					@csrf
					<input type="hidden" name="file" id="imgIdDelete"  value=""/>                    
                    <button class="button gray"><i class="sl sl-icon-close"></i> Так</button>
				</form>
				
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
      
    </div>
</div>

<script type="text/javascript" src="scripts/dropzone.js"></script>

<script>
	function deleteImgModal  (element) {		
		document.getElementById('imgIdDelete').value = element.dataset.id
		// console.log(document.getElementById('imgIdDelete').value)
	}
	function img_modal(src) {		
		document.getElementById('img_modal').src = src
	}

	function myFunction (element) {
		
		var name = element.parentElement;
		var list =name.parentElement;
        var parent =list.parentElement;
        var child =parent.lastElementChild;
		console.log(child);		
		
		
        if (child.style.display === 'flex') {
            child.style.display ='none';
        }
        else {
            child.style.display ='flex'; 
        }  	    		
	}

	function ImageForm () {
		var ImageForm = document.getElementById('ImageForm')
		var getImages = ImageForm.getElementsByClassName('dz-image')
		var fileInput = '@csrf'
		var i = 1
		Array.prototype.forEach.call(getImages, function(el) {
		
			fileInput += ` <input type='text' name='userFile[]' value='${el.firstChild.src}'>`
			
		});		
		var block_name= document.getElementById('block_name').value
		var direction_id= document.getElementById('direction_id').value
		
		fileInput += ` <input type='text' name='block_name' value='${block_name}'>`
		fileInput += ` <input type='text' name='direction_id' value='${direction_id}'>`
		
		var url = '{{ route('upload.image', app()->getLocale()) }}';
        var form = $('<form action="' + url + '" method="POST" id="upload_form"  enctype="multipart/form-data">' +
        fileInput + 
        '</form>');
		$('body').append(form);
		
			console.log(fileInput);
		var formData = new FormData($('#upload_form')[0]);

		formData.append('userFile', $('input[type=text]')[0].value[0]);

		
		form.submit();
		
	}
</script>

@endsection