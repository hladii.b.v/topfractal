@extends('layouts.new-dashboard')

@section('title')
Dashboard | Layouts
@endsection

@section('content')


<div class="dashboard-content"> 
	<div class="row margin-bottom-30">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-window"> {{Lang::get('messages.layouts')}}</h1>
		</div><!-- /.col -->
    </div><!-- /.row -->	
    	
  
	<div class="row  margin-top-30">
		<div class="col-md-12"> 
           
            @foreach ($templates as $name=>$layouts)
            <div class="layout-single">
                <div class="list-group-item-header"> 
                    <a onclick="myFunction(this)"><h4>Шаблон - {{$name}}</h4></a>  
                </div>             
                <div  class="layout-single-item">
                    @foreach ($layouts as $layout)
                        @if ($layout['meta_key']=='head_color' || $layout['meta_key']=='title_color' || $layout['meta_key']=='text_color' )
                            <div  class="list-group-item-color">
                                <div>
                                    <label>{{$layout['meta_key']}} </label>
                                </div>
                                <div>   
                                    <input type="color" name="{{$layout['meta_key']}}" value="{{$layout['meta_value']}}" onchange="updateData(this,  '{{$layout['layouts_id']}}', '{{$layout['meta_key']}}')">                            
                                </div>
                            </div>
                        @elseif ($layout['meta_key']=='teams'  ) 
                            <div  class="list-group-item-color">
                                @foreach (unserialize($layout['meta_value']) as $itemArray)
                                    @foreach ($itemArray as $key=>$item)
                                        <div>
                                            <label>{{$key}} </label>
                                        </div>
                                        <div>   
                                            {{$item}} 
                                        </div>
                                    @endforeach  
                                @endforeach                              
                                
                            </div>
                        @else
                            <div  class="list-group-item-img">
                                <div> 
                                    <label>{{$layout['meta_key']}} </label>
                                </div>
                                <div class="list-group-single-img">   
                                    <img src="{{$layout['meta_value']}}" onclick="img_modal('{{$layout['meta_value']}}', '{{$layout['layouts_id']}}', '{{$layout['meta_key']}}')"  data-toggle="modal" data-target="#previewModal">
                            
                                </div>
                            </div>
                        {{-- @else
                            <div  class="list-group-item-img">
                                <div>
                                    <label>{{$layout['meta_key']}} </label>
                                </div>
                                <div>
                                    <img src="{{$layout['meta_value']}}" onclick="img_modal('{{$layout['meta_value']}}', '{{$layout['layouts_id']}}', '{{$layout['meta_key']}}')"  data-toggle="modal" data-target="#previewModal">
                            
                                </div>
                            </div> --}}
                        @endif
                    @endforeach
                </div>
            </div>
            @endforeach
			
		</div>
	</div>
	
</div><!-- /.container -->
<!-- Modals -->
<div class="modal fade" id="previewModal" role="dialog">
    
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Image Library</h4>
            </div>
            <div class="modal-body">               

                <div class="modal-body-content style-2">

                    <ul class="tabs-nav">
                        <li class="active"><a href="#myImagesTab">Зображення</a></li>
                        <li id="uploadTabLI"><a href="#uploadTab">Завантажити нове</a></li>                       
                    </ul> <!-- /tabs -->

                    <div class="tabs-containe">

                        <div class="tab-content  active" id="myImagesTab">                            
                            <!-- Alert Info -->
                            <div >                                
                               <img src="" id="modal-img">
                            </div>
                          

                        </div><!-- /.tab-pane -->

                        <div class="tab-content" id="uploadTab">

                            <form id="imageUploadForm" action="{{ route('layouts.foto') }}" method="POST" enctype="multipart/form-data" >
                                @csrf
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <input type="hidden" name="templateID" id="templateID" required>
                                    <input type="hidden" name="keyImg" id="keyImg" required>
                                    <input type="file" name="imageFile" id="imgInp" required>
                                    <img id="blah" src="" alt="your image" />
                                </div>
                                <hr>

                                <button type="submit"  class="btn btn-primary btn-embossed btn-wide upload btn-block">Завантажити</button>
                            </form>  

                        </div><!-- /.tab-pane -->

                    </div> <!-- /tab-content -->

                </div>
            </div>
            <div class="modal-footer">   
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
        
    </div>
</div>

<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
    position: relative;
    display: flex;
    padding: 15px 15px;
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid #ddd;
    justify-content: space-around;
    align-items: center;
}
</style>
<script type="text/javascript">

    function img_modal(src, id, key) {		
		console.log(key);
        document.getElementById('modal-img').src= src
        document.getElementById('templateID').value= id
        document.getElementById('keyImg').value= key
    }
	function myFunction (element) {	
        
        header = element.parentElement
        parent = header.parentElement
        item = parent.childNodes[2].nextElementSibling
        
        display = item.style.display
        if (display === 'flex') {
            item.style.display ='none';
        }
        else {
            item.style.display ='flex'; 
        }      		
	}

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
        }

        $("#imgInp").change(function() {
        readURL(this);
    });

	function updateData (element, id, key) {
		
        console.log(key);
        color = element.value;
		$.ajax({
		    type: 'GET', 
		    url:  window.location.origin+'/layouts-update',
			data: {templateID: id, value: color, key: key},
		    error: function() { 
		         console.log(color);
            },
            success: function(data) { 
		         console.log(data);
            }
		})
	}
</script>
	

@endsection