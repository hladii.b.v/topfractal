

<style>
.credit-card-div span {
 padding-top:10px;
 }
.credit-card-div img {
 padding-top:30px;
}
.credit-card-div .small-font {
 font-size:9px;
}
.credit-card-div .pad-adjust {
 padding-top:10px;
}
</style>
<div class="alert alert-success successBlock" style="display: none;">
    <h4>{{Lang::get('messages.Hooray')}}!</h4>
   	<p class="liqpayResult"></p>
</div>
<div class="alert alert-danger loader-export-finish errorBlock" style="display: none;">
    <h4>{{Lang::get('messages.Ohh..')}}</h4>
    <p class="liqpayError"></p>
</div>

<form class="form-horizontal" role="form" id="siteSettingsForm">
	<input type="hidden" name="siteID" id="siteID" value="{{ $data[0]['id'] }}">

	<input type="hidden" name="locale" id="locale" value="{{ app()->getLocale() }}">
	<div class="credit-card-div">
		<div class="panel panel-default" >
			<div class="panel-heading">
				<div class="row ">
					<div class="col-md-12">
						<input type="text" id="cc-number" name="cc-number" class="form-control" placeholder="Enter Card Number" />
					</div>
				</div>

				<div class="row ">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<span class="help-block text-muted small-font" > {{Lang::get('messages.card_date')}}</span>
						<input type="text" class="form-control" name="cc-date" id="cc-date" placeholder="MM/YY" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<span class="help-block text-muted small-font" >CCV</span>
						<input type="text" name="cc-cvv" id="cc-ccv" class="form-control" placeholder="CCV" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="hidden" id='env-url' value="{{env('APP_URL_WITHOUT_NAME')}}">
						<img id='card-logo' src="https://bootstraptema.ru/snippets/form/2016/form-card/card.png" class="img-rounded" />
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="row ">
		<label for="name" class="col-sm-3 control-label">{{Lang::get('messages.ttt')}}</label>
		<label for="name" class="col-sm-3 control-label">1200 ₴</label>
	</div>	
</form>

<script>
	$('#cc-number').on('keyup', function(){
		var num = parseInt($('#cc-number').val().replace(/\D+/g,""));
		if(num.toString().length >= 4) {
			var cardInfo = new CardInfo(num.toString());
			if(cardInfo.brandLogo) {
				$('#card-logo').attr('src', $('#env-url').val() + '/src/js' +cardInfo.brandLogo)
				$('#card-logo').width('50');
				$('#card-logo').height('50');
			} else {
				$('#card-logo').attr('src', 'https://bootstraptema.ru/snippets/form/2016/form-card/card.png')
			}
		} else {
			$('#card-logo').attr('src', 'https://bootstraptema.ru/snippets/form/2016/form-card/card.png')
		}
		
	})
	

	$('#cc-number').mask('9999 9999 9999 9999');
	$('#cc-date').mask('99/99');
	$('#cc-ccv').mask('999');

	$('#payLiqpay').on('click', function(e){
		$('.errorBlock').hide();
		$('.successBlock').hide();
        e.preventDefault();
                $.ajax({
                    url: "/siteAjaxUpdate",
                    type: 'post',
                    data: {
                        'siteID': $('#siteID').val(),
                        'cc-number': $('#cc-number').val(),
                        'cc-date': $('#cc-date').val(),
                        'cc-ccv': $('#cc-ccv').val(),
                        'locale': $('#locale').val(),
                    }
                }).then((res) => {
                	if(res.status == 'ok') {
                		$('.liqpayResult').html(res.message)
						$('.successBlock').show();
                		$('.errorBlock').hide();
                		$('#payLiqpay').hide();
                	} else {
                		$('.liqpayError').html(res.message)
                		$('.successBlock').hide();
                		$('.errorBlock').show();
                	}
                })
       })
</script>