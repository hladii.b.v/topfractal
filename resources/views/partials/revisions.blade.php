@if( $data )
@foreach( $data as $revision )
	<li>
		<span class="fui-arrow-right"></span>
		{{ date('Y-m-d H:i:s', strtotime($revision->updated_at)) }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="{{ route('revision.preview', ['locale' => app()->getLocale(), 'site_id' => $revision->site_id, 'datetime' => strtotime($revision->updated_at), 'page' => $page]) }}" target="_blank" title="{{Lang::get('messages.Preview_Revision')}}">
			<span class="fui-export"></span>
		</a>
		&nbsp;
		<a href="{{ route('revision.delete', ['locale' => app()->getLocale(), 'site_id' => $revision->site_id, 'datetime' => strtotime($revision->updated_at), 'page' => $page]) }}" title="{{Lang::get('messages.Delete_Revision')}}" class="link_deleteRevision">
			<span class="fui-trash text-danger"></span>
		</a>
		&nbsp;
		<a href="{{ route('revision.restore', ['locale' => app()->getLocale(), 'site_id' => $revision->site_id, 'datetime' => strtotime($revision->updated_at), 'page' => $page]) }}" title="{{Lang::get('messages.Restore_Revision')}}" class="link_restoreRevision">
			<span class="fui-power text-primary"></span>
		</a>
	</li>
@endforeach
@endif
