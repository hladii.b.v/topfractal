<form class="form-horizontal"  method="POST" action="{{ route('user-update') }}">
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" class="form-control" id="email" name="email" placeholder="Email address" value="{{ $user['userData']->email }}">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="password" class="form-control" id="password" name="password" placeholder="{{Lang::get('messages.password')}}" value="">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12 checkbox-class">
			<label class="checkbox" for="checkbox-admin-{{ $user['userData']->id }}" style="padding-top: 0px;">
				{{Lang::get('messages.Admin_permissions')}}
			</label>
			<input type="checkbox" value="yes" <?php if( $user['userData']['type'] == 'admin' ):?>checked<?php endif;?> name="type" data-toggle="checkbox" id="checkbox-admin-{{ $user['userData']->id }}">

		</div>
	</div>
	<input type="hidden" name="user_id" value="{{ $user['userData']->id }}">
	<input type="hidden" name="_token" value="{{ Session::token() }}">
	<div class="form-group">
		<div class="col-md-12">
			<button  class="btn btn-primary btn-embossed btn-block updateUserButton" ><span class="fui-check"></span> {{Lang::get('messages.Update_Details')}}</button>
		</div>
	</div>
</form>