<!doctype html>
<html >
<head>
	<base href=" {{ url('/') }}">
	<!-- Basic Page Needs
    ================================================== -->
	<title>{{ config('app.name', 'DentEco') }}</title>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="{{ asset('images/faviicon.png') }}">
	

	<!-- CSS
    ================================================== -->


	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/main-color.css" id="colors">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css') }}" />
	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<!-- Styles -->

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

    @if(Auth::user())

    @if (Auth::user()->type == 'admin')
    <div id="dashboard">
    
        <!-- Navigation
        ================================================== -->
    
        <!-- Responsive Navigation Trigger -->
        <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
    
        <div class="dashboard-nav">
            <div class="dashboard-nav-inner">
    
                <ul data-submenu-title="Main">
                    <li ><a href="https://topfractal.com"><i class="im im-icon-Cloud-Settings"></i>Home</a></li>
			        <li class="{{ Request::path() ==  'assets' ? 'active' : ''  }}"><a href="{{ route('assets') }}"><i class="fa fa-picture-o"></i> {{Lang::get('messages.Image_Library')}}</a></li>

                  
			        <li class="{{ Request::path() ==  'page.content.dashboard' ? 'active' : ''  }}"><a href="{{ route('page.content.dashboard') }}"><i class="im im-icon-Folder-Setting"></i>{{Lang::get('messages.dashboard')}}</a></li>
                     <li class="{{ Request::path() ==  'text' ? 'active' : ''  }}"><a href="{{ route('text') }}"><i class="fa fa-calendar-check-o"></i> {{Lang::get('messages.Text')}}</a></li>
                     <li class="{{ Request::path() ==  'tariffs' ? 'active' : ''  }}"><a href="{{ route('tariffs') }}"><i class="sl sl-icon-wallet"></i> {{Lang::get('messages.tariffs')}}</a></li>
                     <li class="{{ Request::path() ==  'additional' ? 'active' : ''  }}"><a href="{{ route('additional') }}"><i class="sl sl-icon-wallet"></i>Додаткові послуги</a></li>
                    <li class="{{ Request::path() ==  'users' ? 'active' : ''  }}"><a href="{{ route('users') }}"><i class="sl sl-icon-user-female"></i> {{Lang::get('messages.Users')}}</a></li>
                    <li class="{{ Request::path() ==  'benefits' ? 'active' : ''  }}"><a href="{{ route('benefits') }}"><i class="sl sl-icon-disc"></i> {{Lang::get('messages.Benefits')}}</a></li>
                    <li class="{{ Request::path() ==  'services' ? 'active' : ''  }}"><a href="{{ route('services') }}"><i class="fa fa-server"></i> {{Lang::get('messages.Services')}}</a></li>
                    <li class="{{ Request::path() ==  'domains' ? 'active' : ''  }}"><a href="{{ route('domains') }}"><i class="list-box-icon sl sl-icon-doc"></i>{{Lang::get('messages.Domains')}}</a></li>
                    <li class="{{ Request::path() ==  'layouts-colors' ? 'active' : ''  }}"><a href="{{ route('layouts.colors') }}"><i class="sl sl-icon-layers"></i> {{Lang::get('messages.layouts')}}</a></li>
                    <li class="{{ Request::path() ==  'templates' ? 'active' : ''  }}"><a href="{{ route('templates') }}"><i class="sl sl-icon-layers"></i> Створення шаблонів</a></li>
                    <li class="{{ Request::path() ==  'categories' ? 'active' : ''  }}"><a href="{{ route('categories') }}"><i class="sl sl-icon-layers"></i> categories</a></li>
                    <li class="{{ Request::path() ==  'elements' ? 'active' : ''  }}"><a href="{{ route('elements') }}"><i class="sl sl-icon-layers"></i> elements</a></li>
                   
                </ul>
                
                <ul data-submenu-title="{{Lang::get('messages.Sites')}}">
                    <li><a><i class="sl sl-icon-layers"></i>{{Lang::get('messages.Sites')}}</a>
                        <ul>
                            <li><a href="{{ route('dashboard', ['locale'=> app()->getLocale(), 'status'=>1 ] ) }}"> {{Lang::get('messages.published')}}<span id="nav-tag-green" class="nav-tag green">0</span></a></li>                           
                            <li><a href="{{ route('dashboard', ['locale'=> app()->getLocale(), 'status'=>0 ] ) }}">{{Lang::get('messages.not_published')}}<span id="nav-tag-red" class="nav-tag red">0</span></a></li>
                        </ul>	
                    </li>
                    {{-- <li><a href="dashboard-reviews.html"><i class="sl sl-icon-star"></i> Reviews</a></li>
                    <li><a href="dashboard-bookmarks.html"><i class="sl sl-icon-heart"></i> Bookmarks</a></li> --}}

                    <li><a href="{{ route('layouts') }}"><i class="fa fa-plus-circle"></i> {{Lang::get('messages.create')}} </a></li>
                </ul>	
    
                <ul data-submenu-title="Account">
                    <li>
                        <a href="{{ route ('account')}}"><i class="sl sl-icon-user"></i>{{Lang::get('messages.My_Account')}}</a></li>
                    <li>
                        <a   href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                         <i class="sl sl-icon-power"></i> {{Lang::get('messages.logout')}}
                        </a>
                        
                    </li>
                    <li>

                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             @csrf
                         </form>
                    </li>
                </ul>
                
            </div>
        </div>


        @yield('content')


	</div>
    @endif

    @else   
        @yield('content')

    @endif
</div>
<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery-migrate-3.1.0.min.js"></script>
<script type="text/javascript" src="scripts/mmenu.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/waypoints.min.js"></script>
<script type="text/javascript" src="scripts/counterup.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>

<script>
    function siteCount () {
        $.ajax ({
            url: "/getSitecount",
            type: 'get',            
            error: function(data) {
               console.log(data)
            },
            success: function (responce) {
               
                document.getElementById('nav-tag-red').innerHTML = responce.notPublishSite
                document.getElementById('nav-tag-green').innerHTML = responce.publishSite
            }
        })
    }

   siteCount ()
</script>
<!-- Style Switcher / End -->

</body>
</html>
