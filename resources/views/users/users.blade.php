@extends('layouts.new-dashboard')

@section('title')
Dashboard | {{Lang::get('messages.Users')}}
@endsection

@section('content') 

<link rel="stylesheet" href="css/user.css">


<div class="dashboard-content">
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-user"></span> {{Lang::get('messages.Users')}}</h1>
		</div><!-- /.col -->
		
	</div><!-- /.row -->
	<hr class="dashed margin-bottom-30">
	<div class="row">
		<div class="col-md-12">
			@if( Session::has('success') )
			<div class="alert alert-success">
				<button type="button" class="close fui-cross" data-dismiss="alert"></button>
				{{ Session::get('success') }}
			</div>
			@endif
			@if( Session::has('error') )
			<div class="alert alert-error">
				<button type="button" class="close fui-cross" data-dismiss="alert"></button>
				{{ Session::get('error') }}
			</div>
			@endif
			<div class="masonry-4 users" id="users">
				@include('partials.users')
			</div><!-- /.masonry -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.container -->



@endsection