<style>
.credit-card-div span {
 padding-top:10px;
 }
.credit-card-div img {
 padding-top:30px;
}
.credit-card-div .small-font {
 font-size:9px;
}
.credit-card-div .pad-adjust {
 padding-top:10px;
}
</style>

<div class="modal fade siteSettingsModal" id="siteSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="fui-gear">{{Lang::get('messages.pay_info')}}</span></h4>
            </div>
            <div class="modal-body">
                
<form class="form-horizontal" role="form" id="siteSettingsForm">
    <input type="hidden" name="locale" id="locale" value="{{ app()->getLocale() }}">
    <div class="credit-card-div">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row ">
                    <div class="col-md-12">
                        <input type="text" id="cc-number" name="cc-number" class="form-control" placeholder="____ ____ ____ ____" />
                    </div>
                </div>

                <div class="row ">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <span class="help-block text-muted small-font" > {{Lang::get('messages.card_date')}}</span>
                        <input type="text" class="form-control" name="cc-date" id="cc-date" placeholder="__/__" />
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <span class="help-block text-muted small-font" >CVV</span>
                        <input type="text" name="cc-cvv" id="cc-ccv" class="form-control" placeholder="___" />
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <input type="hidden" id='env-url' value="{{env('APP_URL_WITHOUT_NAME')}}">
                        <img id='card-logo' src="https://bootstraptema.ru/snippets/form/2016/form-card/card.png" class="img-rounded" />
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="row ">
        <label for="name" class="col-sm-3 control-label">{{Lang::get('messages.ttt')}}</label>
        <label for="name" class="col-sm-3 control-label">1200 ₴</label>
    </div>  
</form>


            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal"><span class="fui-cross"></span> {{Lang::get('messages.cancel')}}</button>
                <button type="button" class="btn btn-primary btn-embossed" id="payLiqpay"><span class="fui-check"></span>{{Lang::get('messages.Pay')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

