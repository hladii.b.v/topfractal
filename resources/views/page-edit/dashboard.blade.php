@extends('layouts.new-dashboard')
 
@section('content')

	
	<div class="dashboard-content">
		<div class="row margin-bottom-50 margin-top-50">		 	
			
			<div class="lang-header-row">				
				<div class="create-new-lang">
					<form  method="POST" action="{{route('add.new.lang')}}">		
						@csrf				
						<input type="text" id="create_name" name="create_name" placeholder="Додати нову мову" required>
						<button type="submit" class="create-lang-button"> Створити </button>
					</form>
				</div>
				<div class="lang-list">
					@foreach ($lang as $item)
						<div  class="lang-list-item">
							<input data-id="{{$item->lang}}" type="checkbox" {{$item->hide_lang==Null ? 'checked' : ''}} onChange="showLang(this)">						
							<span>{{$item->lang}}</span>
							{!!$item->lang=='ua' ? '' : '
							<a data-id="'.$item->lang.'" data-toggle="modal" data-target="#deleteModal" onclick="deleteModal(this)" class="close-list-item"><i class="fa fa-close"></i></a>
							'!!}
						</div>					
					@endforeach
				</div>
			</div>
		</div>
			

		<div class="row">		
			{{-- @dd($page_content)	 --}}
			<!-- Listings -->
			<div class="col-lg-12 col-md-12">
				@foreach ($page_content as $key=>$langs)
					@if($key=='site_create')
				
						<div class="dashboard-list-box margin-top-0">		 		
						
							<a onclick="showContent('{{$key}}')"><h4>{{$key}}</h4></a>
							<div class="style-2" id="{{$key}}" style="display: none">
								<!-- Tabs Navigation -->
								
								<!-- Tabs Content -->
								<div class="row margin-top-25 lang-page">
									@foreach ($langs as $lang)	
								
									<div class="lang-collum">										
										<ul>
											@foreach ($lang as $page)	
												<li class="pending-booking">
													<form action="{{route('update.create.text', app()->getLocale())}}"  method="post">
														@csrf
														<div class="list-box-listing bookings">
															
															<div class="list-box-listing-content">
																<div class="inner">
																	
																	<h3>{{$page['page_key']}} <span class="booking-status pending">{{$page['lang']}}</span></h3>
																
																	<div class="inner-booking-list">
																		<input type="hidden" name="text_id" value="{{$page['id']}}">
																	<textarea name="text"  id="editor{{$page['id']}}"  >{{$page['name']}}</textarea>
																	</div>									
						
																</div>
															</div>
														</div>
														
													</form>
												</li>
										
												@endforeach		
										</ul>
									
									</div>
									
									@endforeach								
								</div>
							</div>
					
						</div>
					@else 
						<div class="dashboard-list-box margin-top-0">				
							
							<a onclick="showContent('{{$key}}')"><h4>{{$key}}</h4></a>
							<div class="style-2" id="{{$key}}" style="display: none">
								<div class="margin-top-25 lang-page">
									@foreach ($langs as $lang)	
								
									<div class="lang-collum">										
										<ul>
											@foreach ($lang as $page)											
												
												<li class="pending-booking">
													{{-- <form action="{{route('update.page.text', app()->getLocale())}}"  method="post"> --}}
														@csrf
														<div class="list-box-listing bookings">
															
															<div class="list-box-listing-content">
																<div class="inner">
																	<h3>{{$page['page_key']}} <span class="booking-status pending">{{$page['lang']}}</span></h3>
																
																	<div class="inner-booking-list">
																		<input type="hidden" name="text_id" value="{{$page['id']}}">
																	<textarea name="text"  id="editor{{$page['id']}}"  onfocusout="updatePageText(this, {{$page['id']}})">{{$page['page_text']}}</textarea>
																	</div>									
						
																</div>
															</div>
														</div>
														
													{{-- </form> --}}
												</li>												
											@endforeach	
										</ul>
									</div>
									@endforeach
								</div>
							</div>
							
						</div>
					@endif
                @endforeach	
			</div>
		</div>

	</div>
	<!-- Modals -->
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Видалення</h4>
            </div>
            <div class="modal-body">
                <p>ви дійсно хочете видлати мову?</p>
            </div>
            <div class="modal-footer">                
                <form action="{{route('delete.lang')}}" method="POST">
                    @csrf
                    <input type="hidden" name="lang"  id="categoryIdDelete" value="" >
                    <button class="button gray"><i class="sl sl-icon-close"></i> Так</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
      
    </div>
</div>
	<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>
    <script>
		function updatePageText (el, id) {
			// var parent =el.parentElement.parentElement;
			// var childDiv = parent.childNodes[2].nextElementSibling.childNodes[0].nextElementSibling.childNodes[0].nextElementSibling.childNodes[1].nextElementSibling
			// var textarea = childDiv.childNodes[2].nextElementSibling.value 
			var textarea = el.value
			console.log(textarea);

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST', 
				url:  window.location.origin+'/update-page-text',
				data: {text_id: id, text: textarea},		   
				success:function(response) { 
					console.log(response);
				}
			})
		}

		function showLang (el) {
			
			let checked = el.checked
			let lang = el.dataset.id
			if(lang == 'ua') {
				checked=true
				
			}
			console.log(checked);
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST', 
				url:  window.location.origin+'/update-lang',
				data: {show: checked, lang: lang},		   
				success:function(response) { 
					console.log(response);
				}
			})
		}
        function showContent (key) {            
            element = document.getElementById(key)
            if (element.style.display ==='none'){
                element.style.display ="block"
            }
            else if(element.style.display ==='block'){
                element.style.display ="none"
            }
		}
		function deleteModal (element) {
			console.log(element);
			
			document.getElementById('categoryIdDelete').value = element.dataset.id
		}
		
		function clearContents(element) {
			
			ClassicEditor
			.create( document.getElementById( element.id) )
			.catch( error => {
				console.error( error );
			} );
		}	
		
	</script>
	
@endsection
