@extends('layouts.new-dashboard')

@section('content')

<div class="dashboard-content">   
  
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="dashboard-list-box margin-top-0">				
                
                <ul>
                    @foreach( $sites as $site )
                    <li class="pending-booking">
                        <div class="list-box-listing bookings">
                            <div class="list-box-listing-content">
                                <div class="inner">
                                    <h3><a href="{{ route('site', ['locale'=> app()->getLocale(), 'site_id'=> $site['siteData']['id'] ] ) }}">{{ $site['siteData']['site_name'] }}
                                        @if( $site['siteData']['ftp_published']==null)
                                            <span class="booking-status unpaid">{{Lang::get('messages.Site_has_not_been_published')}}</span>
                                           
                                        @else
                                            <span class="booking-status approved">{{Lang::get('messages.Site_has_been_published')}}</span>
                                        @endif
                                    </h3>
                                    @if( $site['siteData']['ftp_published']!=null)
                                        <div class="inner-booking-list">                                            
                                            <ul class="booking-list">
                                                <li class="highlighted">{{$site['siteData']['publish_date']}} - {{$site['siteData']['publish_date_end']}}</li>
                                            </ul>
                                        </div>
                                                    
                                        <div class="inner-booking-list">
                                            <h5>Тариф:</h5>
                                            <ul class="booking-list">
                                                <li class="highlighted">{{$site['tariffCurrent']['number'] == 1 ? 'Базовий': ($site['tariffCurrent']['number'] == 2 ?  'Преміум' : 'Преміум Плюс')}}</li>
                                            </ul>
                                        </div>		
                                    @endif
                                    <div class="inner-booking-list">
                                        
                                        <ul class="booking-list">
                                            <li>{{ $site['siteData']['user']['email'] }}</li>
                                            <li>{{ $site['siteData']['user']['first_name'] }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="buttons-to-right">
                            <a href="{{ route('site', ['locale'=> app()->getLocale(), 'site_id'=> $site['siteData']['id'] ] ) }}" class="button gray approve"><i class="sl sl-icon-check"></i> {{Lang::get('messages.Edit_This_Site')}}</a>
                        </div>
                    </li>
                    @endforeach
                    
                </ul>
            </div>            
        </div>
       
    </div>


</div>

@endsection
