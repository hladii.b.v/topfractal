@extends('layouts.new-dashboard')

@section('content')



<div id="contents" class="dashboard-content">  
    
    <!-- Content -->
    <div class="row">
        {{-- <div id="load"></div> --}}
        <div class="col-lg-12 col-md-12 margin-bottom-30">
            <div class="dashboard-list-box margin-top-0">

                <ul>
                    <li>
                        <div class="list-box-listing">

                            
                            <div class="list-box-listing-img">
                                @if( $frames[0] != '' )
                                <a href="{{$data['siteData']['site'][0]['remote_url']==null ? '' : 'http://'.$data['siteData']['site'][0]['remote_url'] }}" target="blank">
                                    <div class="viewport">
                                        <div class="zoomer-wrapper">
                                            <div class="zoomer-small" >
                                                <img src="{{$data['siteData']['site'][0]['logo']}}" frameborder="0" scrolling="0" data-height="500" data-siteid="79" data-href="#" class="iframeButton" >
                                            </div>                                        
                                        </div>
                                    </div>  
                                </a>                              
                                @else
                                <a href="#" target="_blank" class="placeHolder">
                                    <span>{{Lang::get('messages.This_site_is_empty')}}</span>
                                </a>
                                @endif
                            </div>
                            <div class="list-box-listing-content">
                                <div class="inner">
                                    <h3><a href="{{$data['siteData']['site'][0]['remote_url']==null ? '' : 'http://'.$data['siteData']['site'][0]['remote_url'] }}" target="blank">{{$data["siteData"]['site'][0]['site_name']}}</a></h3>
                                    @if($data["siteData"]['site'][0]['ftp_published']==null)
                                        <span> <div class="numerical-rating low">{{Lang::get('messages.Site_has_not_been_published')}}</div></span>
                                    @else 
                                        <span> <div class="numerical-rating high">{{Lang::get('messages.Site_has_been_published')}}</div></span>
                                        <div class="inner-booking-list">
											<h5>{{Lang::get('messages.The_site_is_published_until')}}:</h5>
											<ul class="booking-list">
												<li class="highlighted">{{$data['siteData']['site'][0]['publish_date']}} - {{$data['siteData']['site'][0]['publish_date']}}</li>
											</ul>
										</div>
                                    @endif
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="dashboard-list-box with-icons margin-top-20">
                                    <h4> {{Lang::get('messages.Site_Preview')}}</h4> 
                                    <ul class="margin-top-20">
                                        <li> <a id="previewButton" class="button gray">{{Lang::get('messages.Preview')}}</a></li>
                                    </ul>
                                   
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="dashboard-list-box with-icons margin-top-20">
                                    <h4>{{Lang::get('messages.PublishSite')}}</h4> 
                                    <ul class="margin-top-20">
                                        <li> <a id="exportSubmit" class="button gray"> {{Lang::get('messages.Publish')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="dashboard-list-box with-icons margin-top-20">
                                    <h4>{{Lang::get('messages.Edit')}}</h4> 
                                    <ul class="margin-top-20">
                                        <li> 
                                            <a id="blockManagement" class="button gray"><i class="sl sl-icon-note"></i> Edit</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="dashboard-list-box with-icons margin-top-20">
                                    <h4>{{Lang::get('messages.Site_delete_message')}}</h4> 
                                    <ul class="margin-top-20">
                                        <li> 
                                            <a  class="button gray" data-toggle="modal" data-target="#deleteModal">
                                                 {{Lang::get('messages.Delete')}}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </li>                  

                </ul>
            </div>
        </div>
       
    </div>
    <div class="row">
        <div  class="col-lg-6 ">
            <div class="dashboard-list-box invoices with-icons margin-top-20">  
                <h4><a onclick="myFunction('payment')">Оплата 
                    <i id="arrow-down" class="fa fa-arrow-down"></i>
                    <i id="arrow-up"  class="fa fa-arrow-up" style="display: none"></i></a></h4>
                <ul> 
                    @if($tariff)						
                    <li><i class="list-box-icon sl sl-icon-wallet"></i>
                        <strong>{{$tariff->number == 1 ? 'Базовий': ($tariff->number == 2 ?  'Преміум' : 'Преміум Плюс')}}</strong>
                        <ul>
                            <li class="paid">Оплачено до:</li>
                            <li> {{$data['siteData']['site'][0]['publish_date']}}</li>
                        </ul>
                    </li> 
                    @endif    
                </ul>
            </div>
            <div  id="payment"  class="col-lg-12 col-md-12" style="display: none">                  
            
                <div class="row margin-top-35">                               
                   
                    <div class="payment" >
        
                        <div class="payment-tab payment-tab-active">
                            <div class="payment-tab-trigger">
                                <input checked="" id="paypal" name="cardType" type="radio" value="paypal">
                                <label for="paypal">Credit / Debit Card</label>
                                <img class="payment-logo paypal"  src="https://i.imgur.com/IHEKLgm.png" alt="">
                            </div>
                           
                                <div class="payment-tab-content">
                                    <div class="row">
            
                                        <div class="col-md-12">
                                            <div class="card-label">
                                                <label for="tarriffCard">Тариф</label>
                                                <ul class="list-2 tariffChecked" id="tariffChecked">  
                                                                                            
                                                                                                 
                                                </ul>
                                                <label for="amountCard">Період</label>
                                                <select id="amountCard" name="amount" onchange="LiqPayForm()">
                                                
                                                </select>
                                            </div>
                                        </div>
            
                                    </div>
                                </div>
                        
                        </div>    
        
        
                    </div>
                    <!-- Payment Methods Accordion / End -->
                  
                   
                </div>
                <div class="row">
                    <div class="payment margin-top-35">
                        <ul class="list-3">
                            @foreach($services as $key=>$service)
                                <li class="tariffServices">
                                    <label class="switch">
                                        <input type="checkbox" name="serviceChecked[]" value="{{$service->price}}" data-id="{{$service->id}}" onchange="LiqPayForm()"><span class="slider round"></span>
                                    </label>	
                                    <span>{{$service->name}}</span>
                                    <span>{{$service->desk}}</span>
                                    <span>{{$service->price}} грн</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                 
                    <h3 class="margin-top-55 margin-bottom-30">Сумма <span id="totalCost"></span></h3>
                    <div id="LiqPayForm">
                        
                    </div>
                       
                </div>
            </div> 
        </div>
        <div  class="col-lg-6 ">
            <div class="dashboard-list-box invoices with-icons margin-top-20">  
                <h4>Історія</h4>
                <ul> 
                    @if($histotyArray)	
                        @foreach ($histotyArray as $value)			
                            <li>
                                <ul>
                                    <li class="{{$value['status'] == 'done' ? 'paid' : 'unpaid'}}">
                                        <i class="ist-box-icon sl sl-icon-basket"></i> Замовлення {{$value['order_id']}} 
                                    </li>
                                    <li> Створенно: {{$value['start']}}</li>
                                    <li class="{{$value['status'] == 'done' ? 'paid' : 'unpaid'}}"> {{$value['amount']}} грн</li>
                                </ul> 
                                @if($value['tariff'] != 0)                              
                                <ul>
                                    <li ><strong>Тариф</strong> </li>
                                    <li >{{$value['tariff'] == 1 ? 'Базовий': ($value['tariff'] == 2 ?  'Преміум' : 'Преміум Плюс')}} </li>
                                    <li>Дійсний до: {{$value['end_at']}}</li>
                                </ul>
                                @endif
                                @if(count($value['services'])>0)    
                                <ul>
                                    <li > <strong>Послуги</strong> </li>
                                    @foreach($value['services'] as $service)
                                        <li>{{$service}}</li>
                                    @endforeach                                    
                                </ul>  
                                @endif                              
                            </li> 
                        @endforeach
                    
                    @endif    
                </ul>
            </div>
        </div>        
    </div>


</div>
<div id="pageList" style="display: none">
    <ul  id="page1">
       
    @foreach ( $frames as $key=>$frame)
        <li> <iframe id="ui-id-{{$key+1}}" src="{{ route('getframe', ['frame_id' => $frame['id']]) }}" data-originalurl="{{$frame['original_url']}}"></iframe>      </li>
    @endforeach    
    </ul>     
</div>
<input id="pageID" type="hidden" value="{{$data['pagesData']['index']['id']}}">
<input id="siteID" type="hidden" value="{{$data['siteData']['site'][0]['id']}}">
<input id="pageCss" type="hidden" value="{{$data['pagesData']['index']['css']}}">
<div id="hidden">
          
    @if( $data['siteData']['pages']['index']['css']==15 )
       <iframe src="{{ URL::to('elements/template_2/skeleton2.html') }}" id="skeleton"></iframe>
   @elseif( $data['siteData']['pages']['index']['css']==9 )
       <iframe src="{{ URL::to('elements/template_1/skeleton1.html') }}" id="skeleton"></iframe>  
   @elseif( $data['siteData']['pages']['index']['css']==16 )
       <iframe src="{{ URL::to('elements/template_3/skeleton3.html') }}" id="skeleton"></iframe>
    @else
       <iframe src="{{ URL::to('elements/template_4/skeleton4.html') }}" id="skeleton"></iframe>
   @endif
</div>


<!-- Modals -->
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Видалення</h4>
            </div>
            <div class="modal-body">
                <p>ви дійсно хочете видлати сайт?</p>
            </div>
            <div class="modal-footer">
                <a href="{{route('site.trash', ['site_id'=> $data["siteData"]['site'][0]['id'] ])}}" type="button" >Так</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
      
    </div>
</div>
<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>
<script>
    
    const previewButton = document.getElementById('previewButton');
    previewButton.addEventListener('click',()=>{
        skeleton = document.getElementById('skeleton')
        siteID = document.getElementById('siteID').value
        pageCss = document.getElementById('pageCss').value
        var skeletonData = skeleton.contentWindow.document || skeleton.contentDocument 
        var getFullPage = ''
        var pageFull = document.getElementById('page1')
        for (let i = 1; i <= pageFull.children.length; i++) {
            iframeData = document.getElementById('ui-id-'+i)  
            innerData = iframeData.contentWindow.document || iframeData.contentDocument 
            getFullPage +=  innerData.getElementById('page').innerHTML                   
        }
       skeletonData.getElementById('page').innerHTML = getFullPage
       
       var documentSkelet =  "<html>"+$(skeleton).contents().find('html').html()+"</html>" 

       const stringifyBlockData = documentSkelet.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");;
     

       var url = 'site/live/preview';
        var form = $('<form action="' + url + '" method="POST"  enctype="multipart/form-data" target="_blank">' +
        '@csrf' +
        '<input type="hidden" name="siteID" value="' + siteID + '" />' +
        '<input type="hidden" name="page_css" value="' + pageCss + '" />' +
        '<input type="hidden" name="page" value="' + stringifyBlockData + '" />' +
        '</form>');
        $('body').append(form);
      
        form.submit(); 
        
      
    })

    const blockManagement = document.getElementById('blockManagement');
    blockManagement.addEventListener('click',()=>{
        const iframeHeader = document.getElementById('ui-id-1')           
        let iframeData = ''
        const innerDoc = iframeHeader.contentWindow.document || iframeHeader.contentDocument
        let innerData = ''
        const header_name = innerDoc.getElementById('header_name')
        const header_address = innerDoc.getElementById('header_address')
        const header_phone = innerDoc.getElementById('header_phone')
        const header_text = innerDoc.getElementById('header_text')
        const header_title = innerDoc.getElementById('header_title')
        const header_motto = innerDoc.getElementById('header_motto')
        const header_logo = innerDoc.getElementById('header_logo')
        const header_img = innerDoc.getElementById('header_img')
        const header_color1 = innerDoc.getElementById('header_color1')
        const header_color2 = innerDoc.getElementById('header_color2')
        const header_color3 = innerDoc.getElementById('header_color3')
        let about_title = '';
        let about_text = '';
        let about_img = '';
        let benefits_title = '';
        let benefits_main = '';
        let services_title = '';
        let service_main = '';
        let service_price = '';
        let teams_title = '';
        let teams_name = [];
        let teams_text = [];
        let teams_imgs = [];
        let porfolio_title = '';
        let porfolio_imgs = '';
        let reviews_title = '';
        let name_first = '';
        let name_seconde = '';
        let reviews_first = '';
        let reviews_seconde = '';
        let adress_title = '';
        let adress_email = '';
        let adress_text = '';
        let chart1 = '';
        let chart2 = '';
        let social_name = [];
        let social_url = [];
        
        const blockDataChildren = document.getElementById('page1');
       
        for (let i = 2; i <= blockDataChildren.children.length; i++) {
            iframeData = document.getElementById('ui-id-'+i)  
            innerData = iframeData.contentWindow.document || iframeData.contentDocument           
            if (iframeData.dataset.originalurl.includes('carat_2.html') )  {
                about_title = innerData.getElementById('about_title')
                 about_text = innerData.getElementById('about_text')
                 about_img = innerData.getElementById('about_img')
                
            }   
            else if  (iframeData.dataset.originalurl.includes('carat_3.html') ) {
                benefits_title = innerData.getElementById('benefit_title')
                 benefits_main = innerData.getElementById('benefits_main')
            }
            else if  (iframeData.dataset.originalurl.includes('carat_8.html') )  {
                services_title = innerData.getElementById('services_title')
                service_main = innerData.getElementById('service_main')
                service_price = innerData.getElementById('service_pirce')
            } 
            else if  (iframeData.dataset.originalurl.includes('carat_5.html'))  {
                teams_title = innerData.getElementById('teams_title')
               count =  innerData.getElementById('teams_count').value
               
                for (let i = 0; i <count ; i++) { 
                    teams_name[i] = innerData.getElementById('teams_name'+i).value  
                    teams_text[i] = innerData.getElementById('teams_text'+i).value  
                    teams_imgs[i] = innerData.getElementById('teams_imgs'+i).value                       
                }
                
                
            } 
            else if  (iframeData.dataset.originalurl.includes('carat_6.html'))  {
                porfolio_title = innerData.getElementById('porfolio_title')
                porfolio_imgs = innerData.getElementById('porfolio_imgs')
            } 
            else if  (iframeData.dataset.originalurl.includes('carat_7.html'))  {
                reviews_title = innerData.getElementById('reviews_title')
                name_first = innerData.getElementById('name_first')
                name_seconde = innerData.getElementById('name_seconde')
                reviews_first = innerData.getElementById('reviews_first')
                reviews_seconde = innerData.getElementById('reviews_seconde')
            } 
            else if  (iframeData.dataset.originalurl.includes('carat_9.html'))  {
                adress_title = innerData.getElementById('adress_title')
                adress_text = innerData.getElementById('adress_text')
                 adress_email = innerData.getElementById('adress_email')
                 chart1 = innerData.getElementById('chart1')
                 chart2 = innerData.getElementById('chart2')
                count =  innerData.getElementById('social_count').value               
                for (let i = 0; i <count ; i++) { 
                   
                    social_url[i] = {
                        name: innerData.getElementById('social_name'+i).value,
                        value:  innerData.getElementById('social_url'+i).value   
                    }
                                        
                }
            }    
        }   
        
        const pageID = document.getElementById('pageID').value;
        const blockManagementData = {
            header: {
                header_name: header_name.value,
                header_address: header_address.value, 
                header_phone: header_phone.value,
                header_text: header_text.value, 
                header_title: header_title.value,
                header_motto: header_motto.value,
                header_logo: header_logo.value,
                header_img: header_img.value,
                header_color1: header_color1.value,
                header_color2: header_color2.value,
                header_color3: header_color3.value,
            },
            about: { 
                about_title: about_title.value, 
                about_text: about_text.value, 
                about_img: about_img.value
            },
            benefits: {  
                benefits_title:benefits_title.value, 
                benefits_main: benefits_main.value
            },
            service: { 
                services_title: services_title.value, 
                service_main: service_main.value, 
                service_price: service_price.value
            },
            team: {
                teams_title: teams_title.value, 
                teams_name: teams_name, 
                teams_text: teams_text, 
                teams_imgs: teams_imgs
            },  
            porfolio: {  
                porfolio_title: porfolio_title.value,
                porfolio_imgs: porfolio_imgs.value
            },
            reviews: { 
                reviews_title: reviews_title.value, 
                name_first: name_first.value, 
                name_seconde: name_seconde.value,
                reviews_first: reviews_first.value, 
                reviews_seconde: reviews_seconde.value
            },
            adress: {
                adress_title: adress_title.value, 
                adress_text: adress_text.value, 
                adress_email: adress_email.value, 
                chart1: chart1.value,
                chart2: chart2.value,
                social_name: social_name, 
                social_url: social_url
            },              
        }
        
       

        const stringifyBlockData = JSON.stringify(blockManagementData).replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
            
      

        var url = 'ua/site-edit/' + pageID;
        var form = $('<form action="' + url + '" method="POST"  enctype="multipart/form-data">' +
        '@csrf' +
        '<input type="hidden" name="data" value="' + stringifyBlockData + '" />' +
        '<input type="hidden" name="color1" value="' + header_color1.value + '" />' +
        '<input type="hidden" name="color2" value="' + header_color2.value + '" />' +
        '<input type="hidden" name="color3" value="' + header_color3.value + '" />' +
        '<input type="hidden" name="name" value="' + header_name.value + '" />' +
        '<input type="hidden" name="logo" value="' + header_logo.value + '" />' +
        '<input type="hidden" name="img" value="' + header_img.value + '" />' +
        '</form>');
        $('body').append(form);
       form.submit(); 
    })


    function publishSitePost() {
        $.ajax({
            url: "/en/site/publish",
            type: 'post',                          
            data: {site_id: $('form#publishForm input[name=site_id]').val()},
            dataType: 'json',
            
        });
    }
    
    $('#exportSubmit').on('click', function(e){
        skeleton = document.getElementById('skeleton')
        siteID = document.getElementById('siteID').value
        pageCss = document.getElementById('pageCss').value
        var skeletonData = skeleton.contentWindow.document || skeleton.contentDocument 
        var getFullPage = ''
        var pageFull = document.getElementById('page1')
        for (let i = 1; i <= pageFull.children.length; i++) {
            iframeData = document.getElementById('ui-id-'+i)  
            innerData = iframeData.contentWindow.document || iframeData.contentDocument 
            getFullPage +=  innerData.getElementById('page').innerHTML                   
        }
       skeletonData.getElementById('page').innerHTML = getFullPage
       
       var documentSkelet =  "<html>"+$(skeleton).contents().find('html').html()+"</html>" 

       const stringifyBlockData = documentSkelet.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");;
    
       console.log(documentSkelet);
        var siteID = document.getElementById('siteID').value
        e.preventDefault();
        $('.loader-export').show()
    
            $.ajax({
                headers: { 
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "http://panel.topfractal.com/site/export",
                type: 'post',
                // contentType: "application/json",
                data: {
                    'siteID': siteID, 
                    'content': documentSkelet,  
                },
                error: function() {
                        $('.loader-export').hide();
                        $('.loader-export-finish').show();
                        $('#exportSubmit').hide();
                    },
                //dataType: 'json'
            })
           
                setTimeout(() => {               
                    document.location.reload(true);
                }, 5000)
                
    })

    // document.onreadystatechange = function () {
    //     var state = document.readyState
    //     console.log(state);
    //     if (state == 'interactive') {
    //         document.getElementById('contents').style.visibility="hidden";
    //     } else if (state == 'complete') {
    //         setTimeout(function(){
    //             document.getElementById('interactive');
    //             document.getElementById('load').style.visibility="hidden";
    //             document.getElementById('contents').style.visibility="visible";
    //         },1000);
    //     }
    // }

function getSiteTariff () {

   siteId = document.getElementById('siteID').value 
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST', 
        url:  window.location.origin+'/SitteTariff',
        data: {siteId: siteId },
    
        success:function(response) { 	
            
            inner = ''
            response.map((data, index) =>  
            inner += `<li>
                <label class="switch">
                    <input type="checkbox" name="tariffChecked[]" value="${data.number}" ${ index == 0 ? `checked`: ``} onchange="tariffChecked()"><span class="slider round"></span>
                </label>	
                ${data.number == 1 ? `БАЗОВИЙ`: (data.number == 2 ? `ПРЕМІУМ` :  `ПРЕМІУМ ПЛЮС` )}
            </li>`)
            document.getElementById('tariffChecked').innerHTML = inner
           
            tariffChecked () 
            LiqPayForm ()
        }
    })        
}
getSiteTariff ()

function LiqPayForm (){
    var amount = document.getElementById('amountCard').value
    var totalAmount = parseInt(amount)
    var serviceChecked = ''        

    $('input[name="serviceChecked[]"]:checked').each(function() {
        totalAmount += parseInt(this.value);
        serviceChecked += this.dataset.id+','
    });

    $('input[name="siteChecked[]"]:checked').each(function() {
    siteId = parseInt(this.value);
    
    });

    tariffId = $('#amountCard').find(':selected').data('id')       
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST', 
        url:  window.location.origin+'/LiqPayForm',
        data: {totalAmount:totalAmount, locale: 'ua', siteId: siteId, tariffId: tariffId, serviceChecked: serviceChecked},
        
        success:function(response) { 	         
            document.getElementById('totalCost').innerHTML = totalAmount
            document.getElementById('LiqPayForm').innerHTML = response.form
            
        }
    })	
} 

function  tariffChecked () {                
    var tariff
    $('input[name="tariffChecked[]"]:checked').each(function() {
        tariff = parseInt(this.value);          
    });     
    if (!tariff){
        inner = `<option value="0" data-id="0" selected></option>`
              
        document.getElementById('amountCard').innerHTML = inner
        document.getElementById('amountCard').style.visibility = 'hidden'
        LiqPayForm ()
    }
    else{
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST', 
            url:  window.location.origin+'/tariffChecked',
            data: {locale: 'ua', tariff: tariff},
        
            success:function(response) { 	
                var inner = '';	      
            
                response.priceArray.map((data, index) =>  
            
                    inner += `<option value="${data.price}" data-id="${data.id}" ${ index == 0 ? `selected`: ``}>${data.price}
                        ${ index == 0 ? `грн - 1 місяць`: (index = 1 ? `грн - 6 місяців` :  `грн - 12 місяців`)}  </option>`
                )
                document.getElementById('amountCard').innerHTML = inner
                document.getElementById('amountCard').style.visibility = 'visible'
                LiqPayForm ()
            
            }
        })
    }  
    $(".tariffChecked input:checkbox").on('click', function() {    
        var $box = $(this);
       
            if ($box.is(":checked")) {       
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                
                $(group).prop("checked", false);
                $box.prop("checked", true);
                LiqPayForm ()
            } 
            else {
                $box.prop("checked", false);
            }
        });  
   	
}

$(".tariffChecked input:checkbox").on('click', tariffChecked () )





function myFunction (id) {
	
    element =document.getElementById(id)
    display = element.style.display
    if (display === 'none') {
        element.style.display ='block';
        document.getElementById('arrow-up').style.display ='inline-block';
        document.getElementById('arrow-down').style.display ='none';
    }
    else {
        element.style.display ='none'; 
        document.getElementById('arrow-up').style.display ='none';
        document.getElementById('arrow-down').style.display ='inline-block';
    }      		
}

</script>
@endsection
