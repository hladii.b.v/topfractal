<div id="page">
	<div class="record" id="record226919243" recordid="226919243" off="" data-record-type="604" style="opacity: 1;">

		<div id="rec226919243" class="r" style="padding-top:90px;padding-bottom:90px; " data-animationappear="off">

			<div class="t604" id="gallery">
				<div class="t-slds" style="">
					<div class="t-container t-slds__main">
						<div class="t-slds__container t-width t-width_10 t-margin_auto">

							<div class="t-slds__items-wrapper  t-slds_animated-fast t-slds__witharrows"
								data-slider-transition="300" data-slider-with-cycle="true"
								data-slider-correct-height="false" data-auto-correct-mobile-width="false"
								data-slider-arrows-nearpic="yes" data-slider-is-preview="true"
								data-slider-initialized="true" data-slider-totalslides="4" data-slider-pos="1"
								data-slider-curr-pos="1" data-slider-cycle="" data-slider-animated=""
								style="width: 5760px; transform: translateX(-960px); touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                @if(count($imgList) > 0)

                                    <div class="t-slds__item t-slds__item-loaded" data-slide-index="0"
                                        style="width: 960px;">
                                        <div class="t-width t-width_9 t-margin_auto" >
                                            <div class="t-slds__wrapper t-align_center">
                                                <meta itemprop="image"
                                                    content="{{asset($imgList[0] ) }}">
                                                <div class="t604__imgwrapper" bgimgfield="gi_img__3"
                                                    id="tuwidget198146">
                                                    <div class="t-slds__bgimg  t-bgimg"
                                                        data-original="{{asset($imgList[0] ) }}"
                                                        style="background-image: url('{{asset($imgList[0] ) }}');">
                                                    </div>
                                                    <div class="t604__separator" data-slider-image-width="860"
                                                        data-slider-image-height="550"
                                                        style="padding-bottom: 63.9535%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @foreach ($imgList as $key=>$img)
                                        <div class="t-slds__item t-slds__item-loaded" data-slide-index="{{$key+1}}"
                                        style="width: 960px;">
                                            <div class="t-width t-width_9 t-margin_auto" >
                                                <div class="t-slds__wrapper t-align_center">
                                                    <meta itemprop="image"
                                                        content="{{asset($img ) }}">
                                                    <div class="t604__imgwrapper" bgimgfield="gi_img__{{$key+1}}"
                                                        id="tuwidget198146">
                                                        <div class="t-slds__bgimg  t-bgimg"
                                                            data-original="{{asset($img ) }}"
                                                            style="background-image: url('{{asset($img ) }}');">
                                                        </div>
                                                        <div class="t604__separator" data-slider-image-width="860"
                                                            data-slider-image-height="550"
                                                            style="padding-bottom: 63.9535%;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="t-slds__item t-slds__item-loaded" data-slide-index="{{count($imgList)}}"
                                        style="width: 960px;">
                                        <div class="t-width t-width_9 t-margin_auto" >
                                            <div class="t-slds__wrapper t-align_center">
                                                <meta itemprop="image"
                                                    content="{{asset($imgList[0] ) }}">
                                                <div class="t604__imgwrapper" bgimgfield="gi_img__3"
                                                    id="tuwidget198146">
                                                    <div class="t-slds__bgimg  t-bgimg"
                                                        data-original="{{asset($imgList[0] ) }}"
                                                        style="background-image: url('{{asset($imgList[0] ) }}');">
                                                    </div>
                                                    <div class="t604__separator" data-slider-image-width="860"
                                                        data-slider-image-height="550"
                                                        style="padding-bottom: 63.9535%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
							</div>
						</div>

						<div class="t-slds__arrow_container   t-slds__arrow-nearpic ">



							<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-left"
								data-slide-direction="left" style="height: 550px; width: 521.5px;">
								<div class="t-slds__arrow t-slds__arrow-left t-slds__arrow-withbg"
									style="width: 40px; height: 40px;background-color: rgba(232,232,232,1);">
									<div class="t-slds__arrow_body t-slds__arrow_body-left" style="width: 9px;">
										<svg style="display: block" viewBox="0 0 9.3 17"
											xmlns="http://www.w3.org/2000/svg"
											xmlns:xlink="http://www.w3.org/1999/xlink">
											<desc>Left</desc>
											<polyline fill="none" stroke="#000000" stroke-linejoin="butt"
												stroke-linecap="butt" stroke-width="1"
												points="0.5,0.5 8.5,8.5 0.5,16.5"></polyline>
										</svg>
									</div>
								</div>
							</div>
							<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-right"
								data-slide-direction="right" style="height: 550px; width: 521.5px;">
								<div class="t-slds__arrow t-slds__arrow-right t-slds__arrow-withbg"
									style="width: 40px; height: 40px;background-color: rgba(232,232,232,1);">
									<div class="t-slds__arrow_body t-slds__arrow_body-right" style="width: 9px;">
										<svg style="display: block" viewBox="0 0 9.3 17"
											xmlns="http://www.w3.org/2000/svg"
											xmlns:xlink="http://www.w3.org/1999/xlink">
											<desc>Right</desc>
											<polyline fill="none" stroke="#000000" stroke-linejoin="butt"
												stroke-linecap="butt" stroke-width="1"
												points="0.5,0.5 8.5,8.5 0.5,16.5"></polyline>
										</svg>
									</div>
								</div>
							</div>
						</div>
						<div class="t-slds__bullet_wrapper">
							<div class="t-slds__bullet t-slds__bullet_active" data-slide-bullet-for="1">
								<div class="t-slds__bullet_body" style="background-color: #c7c7c7;"></div>
							</div>
							<div class="t-slds__bullet " data-slide-bullet-for="2">
								<div class="t-slds__bullet_body" style="background-color: #c7c7c7;"></div>
							</div>
							<div class="t-slds__bullet " data-slide-bullet-for="3">
								<div class="t-slds__bullet_body" style="background-color: #c7c7c7;"></div>
							</div>
							<div class="t-slds__bullet " data-slide-bullet-for="4">
								<div class="t-slds__bullet_body" style="background-color: #c7c7c7;"></div>
							</div>
						</div>
						<div class="t-slds__caption__container">
						</div>
					</div>

				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function () {
					t_sldsInit('226919243');
					t604_init('226919243');

					t_slds_UpdateSliderHeight('226919243');
					t_slds_UpdateSliderArrowsHeight('226919243');

				});
			</script>

		</div>

	</div>
</div>
