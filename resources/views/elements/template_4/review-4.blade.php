
<div id="page">
	<div class="record" id="record226942892" recordid="226942892" off="" data-record-type="533" style="opacity: 1;">

		<div id="rec226942892" class="r" style="padding-top:120px;padding-bottom:120px; ">

			<!-- t533 -->

			<div class="t533" style="visibility: visible;">

				<div class="t-section__container t-container" id="reviews">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}
							</div>
						</div>
					</div>
				</div>

				<div class="t-container">
					<div class="t533__row t533__separator" style="">
					@if(count($reviews) > 0)

						@foreach ($reviews as $review)	
						
							<div class="t533__col t-col t-col_4 ">
								<div class="t533__content">

									<div class="t533__textwrapper t-align_center"
										style="border: 0px solid rgb(0, 0, 0);">
										<div class="t533__title t-name t-name_xs" style=""
											field="li_title__1478014727987">
											{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}
										</div>
										<div class="t527__line  t527__bottommargin_lg  t-margin_auto"> </div>
										<div class="t533__descr t-text t-text_xs" style=""
											field="li_descr__1478014727987">
											{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}
										</div>
									</div>									
								</div>
							</div>
						
						@endforeach

					@endif
					</div>
				</div>

			</div>

		</div>
	</div>
</div>


		

