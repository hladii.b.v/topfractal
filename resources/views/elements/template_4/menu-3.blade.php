<div id="page">

	<style>
		
		.t189__descr.t-descr.t-descr_sm, .t-title.t-title_xxs, .t-section__title.t-title.t-title_xs,  .header_socials, .t497__name.t-name.t-name_lg.tittle-benefit

		{            
            color: {{$color1}} !important;
            
        }
	
	   
		.t527__line, .t849__header
		{
            background-color: {{$color1}} !important;
        }
		polyline {
            stroke: {{$color1}} !important;
        }
		
		a.t-menu__link-item:hover, .t189__title h1, .t527__persdescr.t-descr.t-descr_xxs.t527__bottommargin_lg, .t270__title.t-title.t-title_xxs,  a.socials__item,
		.t007__text.t-text.t-text_md.text2

		{
            color: {{$color2}} !important;
		}
		
		
		.t498__col.t-col.t-col_4.t-item:hover .t498__bgimg, #rec238676481 .t-slds__bullet_active .t-slds__bullet_body,  .t-slds__bullet_body, .t849__icon,
		.t849__circle, .t-slds__arrow.t-slds__arrow-left.t-slds__arrow-withbg, .t-slds__arrow.t-slds__arrow-right.t-slds__arrow-withbg,  .t849__icon, .t849__circle
		{
            background-color: {{$color2}} !important;
		}
		.t524__itemwrapper.t524__itemwrapper_4:hover .t524__bgimg.t524__img_circle.t-margin_auto.t-bgimg
		{
			border-color: {{$color2}} !important;
		}
		
		
		a.t-menu__link-item, .t005__text.t-text.t-text_md, .btIco .btIcoHolder:after, .btIco .btIcoHolder:after, .t-descr.t-descr_md,.t-section__descr.t-descr.t-descr_xl.t-margin_auto,
		.t498__title.t-name.t-name_xl, .t270__descr.t-descr.t-descr_xs, .t524__persdescr.t-descr.t-descr_xxs
		{
            color: {{$color3}} !important;
        }
      

		.t-title.t-title_xxs, .t-section__title.t-title.t-title_xs, .t270__title.t-title.t-title_xxs, .t189__descr.t-descr.t-descr_sm
		{
            font-weight: 900  !important;
			line-height: 60px;
			font-size: 55px;
			letter-spacing: -1px;
        }
        
		.t189__title h1, .t527__persdescr.t-descr.t-descr_xxs.t527__bottommargin_lg, .t524__persname.t-name.t-name_lg.t524__bottommargin_sm,
		.t007__text.t-text.t-text_md.text2
		{
			line-height: 20px;
			letter-spacing: 2px;
			font-weight: 700;
			font-size: 14px;
        }
        
		.t-descr.t-descr_md, .t-section__descr.t-descr.t-descr_xl.t-margin_auto, .t498__title.t-name.t-name_xl, 
		.t270__descr.t-descr.t-descr_xs, .t524__persdescr.t-descr.t-descr_xxs, .t497__name.t-name.t-name_lg.tittle-benefit
		{
            font-size: 18px  !important;
		}
		.t460{
			background-color: rgba(32,43,55,.4)  !important;
		}
		.mobile_links{
			color: #fff !important;	
		}
		.t446__additionalwrapper {
			border-left: 0px solid rgba(0, 0, 0, .1);
		}
		.t527__persdescr.t-descr.t-descr_xxs.t527__bottommargin_lg {
			text-transform: uppercase
		}
		.t497__name.t-name.t-name_lg.tittle-benefit{
			line-height: 28px;
    		font-weight: 700;
		}
    </style>
    <div id="rec238324210" class="r t-rec" style="opacity: 1;" data-animationappear="off"
			data-record-type="451">

			<!-- T451 -->
			<div id="nav238324210marker"></div>
			<div id="nav238324210" class="t451" data-menu="yes">
				<div class="t451__container t451__positionfixed t451__panel " data-appearoffset=""
					style="height: 110px;">
					<div class="t451__container__bg" style="background-color: rgba(255,255,255,1); "
						data-bgcolor-hex="#ffffff" data-bgcolor-rgba="rgba(255,255,255,1)"
						data-navmarker="nav238324210marker" data-appearoffset="" data-bgopacity="1"
						data-menu-shadow="0px 1px 3px rgba(0,0,0,0.)"></div>
					<div class="t451__menu__content ">
						<div class="t451__burger t451__burger_mobile">
							<span style=";"></span>
							<span style=";"></span>
							<span style=";"></span>
							<span style=";"></span>
						</div>

						<div class="t451__leftside t451__side">
							<div class="t451__burger">
								<span style=";"></span>
								<span style=";"></span>
								<span style=";"></span>
								<span style=";"></span>
							</div>
						</div>

						<div class="t451__centerside">
							<div class="t451__logo__container t451__textlogo__container">
								<div class="t451__logo__content">
									<img class="dentiqLogo" src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="t451m__overlay">
				<div class="t451m__overlay_bg" style=" "></div>
			</div>

			<div class="t451 t451m t451m__left" data-tooltip-hook="" style="">
				<div class="t451m__close t451m_opened">
					<div class="t451m__close_icon">
						<span style="background-color:#ffffff;"></span>
						<span style="background-color:#ffffff;"></span>
						<span style="background-color:#ffffff;"></span>
						<span style="background-color:#ffffff;"></span>
					</div>
				</div>
				<div class="t451m__container t-align_left">
					<div class="t451m__top">

						<div class="t451m__menu">
							<ul class="t451m__list">
                                @if(count($menu) > 0)

                                @foreach($menu	 as $value)    
                                @if (isset($value['name']))                                     
                                  
                                    <li class="t451m__list_item"><a class=" mobile_links" href="{{$value['link']}}"
										data-menu-submenu-hook="" style="font-size:21px;"
										data-menu-item-number="1">{{$value['name']}}</a>
								    </li>
                                @endif
                                @endforeach
						        @endif 								

							</ul>
						</div>
					</div>
					
				</div>
			</div>


			<script>
				$(document).ready(function () {
					$("#rec238324210").attr('data-animationappear', 'off');
					$("#rec238324210").css('opacity', '1');
					t451_initMenu('238324210');
				});
			</script>



			<style>
				@media screen and (max-width: 980px) {
					#rec238324210 .t-menusub__menu .t-menusub__link-item {
						color: #ffffff !important;
					}

					#rec238324210 .t-menusub__menu .t-menusub__link-item.t-active {
						color: #ffffff !important;
					}
				}
			</style>

	</div>

    <div id="rec238330719" class="r t-rec r_anim r_showed"
        data-record-type="218">
        <!-- T005 -->
        <div class="t005">
            <div class="t-container">
                <div class="t-row">
                    <div class="t-col t-col_6 ">
                        <div class="t005__text t-text t-text_md" style="" field="text">
                            @if (isset($phone))
								<a href="tel:{{isset($phone) ? preg_replace('/[^0-9]/', '', $phone) : '(0532) 63-77-59'}}">
									
									<i class="fas fa-mobile-alt"></i>
								
									{{isset($phone) ? $phone : '(0532) 63-77-59'}}
								</a>
							@endif
							@if (isset($adress))
								<i class="fas fa-map-marker-alt"></i>
								{{isset($adress) ? $adress : 'м. Полтава, вул. Європейська, 110'}}
							@endif
                            <br>
                        </div>
                    </div>
                    <div class="t-col t-col_6 right-align-text">
                        <div class="t005__text t-text t-text_md" style="" field="text2">

                            @if (isset($chart))
								<i class="far fa-clock"></i>
								
								{{isset($chart) ? $chart : ''}}
							@endif
							

                            
                            <div class="t446__additionalwrapper">
                                <div class="t-sociallinks">
									@if(isset($social) && count($social)>0  )
                                    
                                    @foreach ($social as $key=>$soc)
										<div class="t-sociallinks__wrapper">
											<div class="t-sociallinks__item"> 
												<a class="header_socials" href="{{$soc['value']}}" target="_blank"> 
													<i class="fab fa-{{$soc['name']}}"></i>
												</a> 
											</div>
										</div>
									@endforeach     
								
									@endif
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="rec238324537" class="r t-rec" style=" " data-animationappear="off" data-record-type="456">	

        <div id="nav238324537marker"></div>
        <div id="nav238324537" class="t456  t456__positionstatic " 
            data-bgcolor-hex="#000000" data-bgcolor-rgba="rgba(0,0,0,1)" data-navmarker="nav238324537marker"
            data-appearoffset="" data-bgopacity-two="" data-menushadow="" data-bgopacity="1"
            data-menu-items-align="right" data-menu="yes">
            <div class="t456__maincontainer " style="">

                <div class="t456__leftwrapper" style="">
                    <div>
                        <a href="/" style="color:#ffffff;">
                            <img class="dentiqLogo" src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}">
                        </a> 
                    </div>
                </div>

                <div class="t456__rightwrapper t456__menualign_right" style="">
                    <ul class="t456__list">
                        @if(count($menu) > 0)

                        @foreach($menu	 as $value)    
                        @if (isset($value['name'])) 
                            <li class="t456__list_item"><a class="t-menu__link-item" href="{{$value['link']}}" data-menu-submenu-hook=""
                                    style="font-weight:600;" data-menu-item-number="1">{{$value['name']}}</a>
                            </li>
                        
                        @endif
                        @endforeach
                        @endif 
                    </ul>
                </div>

            </div>
        </div>



        <style>
            @media screen and (max-width: 980px) {
                #rec238324537 .t456__leftcontainer {
                    padding: 20px;
                }
            }

            @media screen and (max-width: 980px) {
                #rec238324537 .t456__imglogo {
                    padding: 20px 0;
                }
            }
        </style>


        <script type="text/javascript">


            $(document).ready(function () {
                t456_highlight();   
            });



            $(window).resize(function () {
                t456_setBg('238324537');
            });
            $(document).ready(function () {
                t456_setBg('238324537');
            });
            $(window ).scroll(function () {               
				var scrollTop = $(window).scrollTop();					
				if (scrollTop> 5 ) {
                    $("#nav238324537").css("top", 0);
                    $("#nav238324537").css("background-color", "rgba(255,255,255,1)");
                    
                }
                else {
                    $("#nav238324537").css("top", "60px");
                    $("#nav238324537").css("background-color", "rgba(255,255,255,  .5)");
                }
            });
            

        </script>



        <style>
            @media screen and (max-width: 980px) {
                #rec238324537 .t-menusub__menu .t-menusub__link-item {
                    color: #ffffff !important;
                }

                #rec238324537 .t-menusub__menu .t-menusub__link-item.t-active {
                    color: #ffffff !important;
                }
            }
        </style>
        <!--[if IE 8]>
        <style>
        #rec238324537 .t456 {
            filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#D9000000', endColorstr='#D9000000');
        }
        </style>
        <![endif]-->

    </div>
</div>