<div id="page">

	<div id="rec239024081" class="r r_showed r_anim"
			data-record-type="562">
			<div class="t-container rowItem " id="app">
				<div class="t228__padding40px"></div>

				<div class="t228__leftside" style="min-width: 161px;">
					<div class="t228__leftcontainer">
						<a href="/">
							<div class="t228__logo t-title" field="title">
								<img src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}">
							</div>
						</a> 
					</div>
				</div>

				<div class="t228__centerside ">
					<div class="t228__centercontainer">
						<ul class="t228__list">
							<li class="t228__list_item">
								{{ isset($organizationName) ? $organizationName : 'Карат'}}							
							</li>						

						</ul>
					</div>
				</div>

				<div class="t228__rightside" style="min-width: 161px;">
					<div class="t228__rightcontainer">

						<div class="t228__right_buttons">
							<div class="t228__right_buttons_wrap">
								<div class="t228__right_buttons_but"><a href="" target="" class="t-btn "
										style="color:#000000;background-color:#ffffff;border-radius:20px; -moz-border-radius:20px; -webkit-border-radius:20px;">
										<table style="width:100%; height:100%;">
											<tbody>
												
												@if($social && count($social)>0  )
													<tr>
														@foreach ($social as $key=>$soc)
															<td>
																<a class="socials__item" href="{{$soc['value']}}" target="_blank"> 
																	<i class="fab fa-{{$soc['name']}}"></i>
																</a> 
															</td>
														@endforeach     
													</tr>
												@endif
											</tbody>
										</table>
									</a></div>
							</div>
						</div>

					</div>
				</div>

				<div class="t228__padding40px"></div>
			</div>
			<div class="t562">

				<div class="t-container">

					<div class="t562__col t-col t-col_3">
						<div class="t562__col-wrapper_first t562__col-wrapper">
							<div class="t562__name t-name t-name_md" style="" field="title2">Контакти</div>
							<div class="t562__text t-text t-text_md" style="" field="descr">
								@if (isset($adress))
									<span class="btIco ">
										<span data-ico-pl="" class="btIcoHolder"> 
											<em></em> 
										</span> 
									</span>
									{{isset($adress) ? $adress : 'м. Полтава, вул. Європейська, 110'}}
								@endif
							</div>							
							<div class="t562__text t-text t-text_md" style="" field="descr">									
								{!!isset($email) ? '<i class="fas fa-envelope-open-text"></i> '.$email : 'karat.dental@gmail.com'!!}
							</div>
							<div class="t562__text t-text t-text_md" style="" field="descr">
								@if (isset($phone))
								<a href="tel:{{isset($phone) ? preg_replace('/[^0-9]/', '', $phone) : '(0532) 63-77-59'}}">
									
									<span class="btIco "> 
										<span data-ico-es="" class="btIcoHolder"> 
											<em></em> 
										</span> 
									</span> 
								
									{{isset($phone) ? $phone : '(0532) 63-77-59'}}
								</a>
								@endif
							</div>
						</div>
					</div>

					<div class="t562__col t-col t-col_3">
						<div class=" t562__col-wrapper">
							<div class="t562__name t-name t-name_md" style="" field="title2">Графік</div>
							<div class="t562__text t-text t-text_md" style="" field="descr2">
							@if (isset($chart))
									<span class="btIco "> 
										<span data-ico-es="" class="btIcoHolder"> 
											<em></em> 
										</span> 
									</span> 
							
								{{isset($chart) ? $chart : ''}}
							@endif
							</div>
						</div>
					</div>

					<div class="t562__col t-col t-col_6">
						<div class=" t562__col-wrapper">
							<div class="t562__text t-text t-text_md" style="" field="descr3">
								
								@if(isset($adressText)) 
									{!!$adressText!!}                                        
								@endif  
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="t460">
				
				<div class="t460__wrapper">

					<div class="t460__bottomtext t-descr t-descr_xxs t-align_center" field="text">
						© <a href="https://topfractal.com/" target="_blank">Topfractal</a>. All rights reserved 
					</div>
				</div>
				
			</div>
		</div>
</div>
	