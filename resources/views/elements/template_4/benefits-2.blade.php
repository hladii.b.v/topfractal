<div id="page">

	<div id="rec238676481" class="r" 
			data-animationappear="off" data-record-type="604">

			<div class="t604  t-rec t-rec_pt_90 t-rec_pb_90 vc_custom_1564568213617" id="advantages">
				<div class="t-slds" style="">
					<div class="t-container t-slds__main">
						<div class="t-section__container t-container t-rec  t-rec_mb_90">
							<div class="t-col t-col_12">
								
								<div class="t-section__topwrapper t-align_center">
									<div class="t527__persdescr t-descr t-descr_xxs t527__bottommargin_lg" style="" field="li_persdescr__1478035709182">
										{{ isset($section_name) ? $section_name : 'Чому нас обирають?'}}
									</div>
									<div class="t-section__title t-title t-title_xs" field="btitle">
										{{ isset($benefitMotto) ? $benefitMotto : 'Переваги'}}
									</div>				
									<div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">
										{{ isset($benefitText) ? $benefitText : 'Переваги'}}
									</div>			
								</div>
							</div>
						</div>
						<div class="t-slds__container t-width t-width_10 t-margin_auto">

							<div class="t-slds__items-wrapper  t-slds_animated-fast t-slds__witharrows"
								data-slider-transition="300" data-slider-with-cycle="true"
								data-slider-correct-height="false" data-auto-correct-mobile-width="false"
								data-slider-arrows-nearpic="yes" data-slider-initialized="true"
								data-slider-totalslides="4" data-slider-pos="1" data-slider-curr-pos="1"
								data-slider-cycle="" data-slider-animated=""
								style="width: 5760px; transform: translateX(-960px); touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">

								<div class="t-slds__item t-slds__item-loaded" data-slide-index="0"
									style="width: 960px;">
									<div class="t-width t-width_9 t-margin_auto" itemscope=""
										itemtype="http://schema.org/ImageObject">
										<div class="t-slds__wrapper t-align_center">
											
											<div class="t604__imgwrapper" bgimgfield="gi_img__3">
												
												<div class="t604__separator" data-slider-image-width="860"
											 style="padding-bottom: 1px;">
												</div>
											</div>
										</div>
									</div>
								</div>
								@php 
									$pagedArray = array_chunk($benefitList, 3, true);								
								@endphp
								@if(count($benefitList) > 0)
								@foreach($pagedArray as $key=>$listArray)
									<div class="t-slds__item  t-slds__item_active t-slds__item-loaded" data-slide-index="{{$key+1}}">
										<div class="t-container">
										@foreach($listArray as $list)
											<div class="t498__col t-col t-col_4  t-item" style="height: 236px;">
												<div class="t498__col-wrapper_fisrt t498__col-wrapper">
													<div class="t498__bgimg  t-margin_auto t-bgimg" bgimgfield="img"
														
														style=" background-image: url('{{asset($list['img'] ) }}');"
														data-tu-max-width="400" data-tu-max-height="400"
														id="tuwidget262357"></div>
													<div class="t498__title t-name t-name_xl" style="" field="title">
														{!! $list['desc'] !!}
													</div>
													
												</div>
											</div>
											
										@endforeach     
										</div>
									</div>
								@endforeach     	                           
								@endif

							</div>
						</div>
						<div class="t-slds__bullet_wrapper">
							<div class="t-slds__bullet t-slds__bullet_active" data-slide-bullet-for="1">
								<div class="t-slds__bullet_body" style="background-color: #c7c7c7;"></div>
							</div>
							<div class="t-slds__bullet " data-slide-bullet-for="2">
								<div class="t-slds__bullet_body" style="background-color: #c7c7c7;"></div>
							</div>
							<div class="t-slds__bullet " data-slide-bullet-for="3">
								<div class="t-slds__bullet_body" style="background-color: #c7c7c7;"></div>
							</div>
						</div>
						<div class="t-slds__caption__container">
						</div>
					</div>

				</div>
			</div>


			<script type="text/javascript">
				$(document).ready(function () {
					t_sldsInit('238676481');
					t604_init('238676481');

				}); 
			</script>



			<style>
				#rec238676481 .t-slds__bullet_active .t-slds__bullet_body {
					background-color: #222 !important;
				}

				#rec238676481 .t-slds__bullet:hover .t-slds__bullet_body {
					background-color: #222 !important;
				}
			</style>

		</div>

</div>
	