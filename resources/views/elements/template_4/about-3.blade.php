<div id="page">


	<div class="record" id="record226917740" recordid="226917740" off="" data-record-type="238" style="opacity: 1;">
	
		<div id="rec226917740" class="r" style="padding-bottom:0px; " data-animationappear="off">
			<div  id="about"></div>

			<div class="t-cover" id="recorddiv226917740" bgimgfield="img"
				style="height:170vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); "
				data-tu-noclick="yes">

				<div class="t-cover__carrier" id="coverCarry226917740" data-content-cover-id="226917740"
					data-content-cover-bg="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}"
					data-content-cover-height="170vh" data-content-cover-parallax="fixed"
					style="background-image:url('{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}');height:170vh; ">
				</div>

				<div class="t-cover__filter"
					style="height:170vh;background-image: -moz-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: -o-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: -ms-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe000000', endColorstr='#fe000000');">
				</div>

				<div class="t209">
					<div class="t-container">
						<div class="t-col t-col_5 ">
							<div class="t-cover__wrapper t-valign_bottom" style="height:170vh;">
								<div data-hook-content="covercontent">
									<div class="t209__wrapper">
										<div class="t209__textwrapper">
											<div class="t209__line"></div>
											<div class="t209__text t-text-impact t-text-impact_sm" field="text"
												style="">{{ isset($aboutText) ? $aboutText : 'Одним з основних досягнень клініки є здобута довіра наших пацієнтів, оскільки, велика кількість наших відвідувачів звертається до нас за рекомендаціями своїх рідних чи друзів.
												Успіх завдяки нашій кваліфікованій команді лікарів-стоматологів. Беручи до уваги інтереси наших пацієнтів, ми продовжуємо вдосконалювати наші знання та професійні якості: беремо участь у національних та міжнародних конференціях, курсах підвищення кваліфікації.
											   Наша практика пропонує унікальний спектр стоматологічних послуг, які можуть допомогти Вам і Вашим дітям вести здоровий і комфортний спосіб життя.'}}<br></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<input type="file" class="tu-hidden-input" accept="image/*"
				style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
		</div>

	</div>


</div>
<!--/allrecords-->

