<div id="page">

	<div class="record" id="record226940166" recordid="226940166" off="n" data-record-type="686"
		style="opacity: 1;">
		
		<div id="rec226940166" class="r" style="padding-top:135px;padding-bottom:150px; "
			data-animationappear="off">

			<!-- t686 -->

			<div class="t686" data-tile-ratio="0.714"  id="team">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Команда'}}
							</div>
							
						</div>
					</div>
				</div>
				<div class="t686__container t-container">
					<div class="t686__row">
						@if(count($teams) > 0)
						@foreach ($teams as $team)
							<div class="t686__col t-col t-col_6 t-align_center ">
								<div class="t686__table" style="height: 399.84px; min-height: auto;">
									<div class="t686__cell t-align_center t-valign_middle">
										<div class="t686__bg t686__animation_fast t686__bg_animated t-bgimg"
											bgimgfield="li_img__1494497749748"
											data-original="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
											style="background-image:url('{{isset($team['foto']) ? asset($team['foto']) : ''  }}');"
											id="tuwidget808529"></div>
										<div class="t686__overlay t686__animation_fast"
											style="background-image: -moz-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.80)); background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.80)); background-image: -o-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.80)); background-image: -ms-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.80));">
										</div>
										<div class="t686__textwrapper t686__animation_fast ">
											<div class="t686__textwrapper__content">
												<div class="t686__inner-wrapper">
													<div class="t686__title t-title t-title_xxs"
														field="li_title__1494497749748" style=""
														data-redactor-nohref="yes">{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}</div>
													<div class="t686__text t-descr t-descr_xs"
														field="li_text__1494497749748" style=""
														data-redactor-nohref="yes">{{ isset($team['value']) ? $team['value'] : ''}}</div>
												</div>
											</div>
										</div>
										<input type="file" class="tu-hidden-input" accept="image/*"
											style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
									</div>
								</div>
							</div>
						
						@endforeach
						@endif
					</div>
				</div>
			</div>

		</div>


	</div>

</div>
	