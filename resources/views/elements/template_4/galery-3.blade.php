<div id="page">

	<div class="record" id="record226919081" recordid="226919081" off="" data-record-type="147" style="opacity: 1;">

		<div id="rec226919081" class="r" style="padding-top:30px; ">

			<!-- t214-->
			<div class="t214" id="gallery">
				<div class="t-container">
					<div class="t-row">
                    @if(count($imgList) > 0)
                        @foreach ($imgList as $key=>$img)

                            <div class="t-col t-col_4" style="margin-bottom:20px;">
                                <div class="t214__blockimg t-bgimg" bgimgfield="gi_img__{{$key}}" data-zoom-target="{{$key}}"
                                    data-original="{{asset($img ) }}"
                                    style="background: url('{{asset($img ) }}') center center no-repeat; background-size:cover;"
                                    id="tuwidget414436">
                                    <meta itemprop="image"
                                        content="{{asset($img ) }}">
                                </div>

                            </div>
                        @endforeach
                    @endif
					</div>

				</div>
			</div>

		</div>

	</div>
</div>
