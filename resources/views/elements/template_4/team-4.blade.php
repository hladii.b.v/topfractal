<div id="page">


	<div class="record" id="record226942238" recordid="226942238" off="" data-record-type="539" style="opacity: 1;">
	
		<div id="rec226942238" class="r" style="padding-top:150px;padding-bottom:150px; background-color:#efefef;"
			data-bg-color="#efefef">

			<!-- t539 -->

			<div class="t539" style="visibility: visible;"  id="team">

				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Команда'}}
							</div>
						</div>
					</div>
				</div>

				<div class="t539__container t-container" >
					@if(count($teams) > 0)
					@foreach ($teams as $team)
						<div class="t539__col t-col t-col_4 t539__col-mobstyle t539__col_odd">
							<div class="t539__itemwrapper t539__itemwrapper_3">
								<div class="t539__bgimg t-bgimg" bgimgfield="li_img__1478185125761"
									data-original="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
									style="background-image: url('{{isset($team['foto']) ? asset($team['foto']) : ''  }}'); padding-bottom: 100%;"
									data-tu-max-width="560" data-tu-max-height="560" data-image-width="360"
									data-image-height="360" id="tuwidget428213"></div>
								<div class="t539__textwrapper t539__textwr-padding t-align_left" style="height: 109px;">
									<div class="t539__persname t-name t-name_lg t539__bottommargin_sm" style=""
										field="li_persname__1478185125761">{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}</div>
									<div class="t539__persdescr t-descr t-descr_xxs " style=""
										field="li_persdescr__1478185125761">
										{{ isset($team['value']) ? $team['value'] : ''}}
									</div>
								</div>
								<input type="file" class="tu-hidden-input" accept="image/*"
									style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
							</div>
						</div>
					@endforeach
					@endif
				</div>

			</div>				

		</div>

	</div>

</div>
