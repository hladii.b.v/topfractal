<div id="page">

	<div class="record" id="record226947039" recordid="226947039" off="n" data-record-type="598"
		style="opacity: 1;">
	
		<div id="rec226947039" class="r" style="padding-top:150px;padding-bottom:150px; "
			data-animationappear="off">

			<!-- T598 -->
			<div class="t598"  id="services">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Вартість наших послуг'}}
							</div>
							@if($pdfFile != '' ) 
								<div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">
									<a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="title__text" target="_blank">Прайс лист</a>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="t-container">
					@if(count($servicesList) > 0)
                           
					@foreach($servicesList as $offer)

						<div class="t598__col t-col t-col_4 t-align_center">
							<div class="t598__content">
								
								<div class="t598__title t-name t-name_xl" field="title" style="height: 32px;">
									{{$offer['desc']}}
								</div>
								@if(count($offer['children']) > 0)
									<div class="t598__descr t-descr t-descr_xs" field="descr" style="height: 197px;">
										<ul>
											@foreach($offer['children'] as $offer_child)
												<li>
													<div class="price__service">{{$offer_child['desc']}}</div>
													<div class="price__cost">{{isset($offer_child['price']) ? $offer_child['price'] : ''}}</div>
												</li>
											@endforeach  
										</ul>
									</div>
								@endif
							</div>
						</div>
					@endforeach                                
					@endif

				</div>
			</div>
		</div>
	</div>

</div>
	