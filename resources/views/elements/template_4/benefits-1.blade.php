<div id="page">

	<div id="rec238649333" class="r  r_showed r_anim" >
			<!-- t497 -->


			<div class="t497 t-rec t-rec_pt_75 t-rec_pb_75" id="bt_section5f853f117f401" style="background-image: url({{asset( $background )}});">

				<div class="t-section__container t-container"  id="advantages">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_left">
							<div class="t527__persdescr t-descr t-descr_xxs t527__bottommargin_lg" style="" field="li_persdescr__1478035709182">
								{{ isset($section_name) ? $section_name : 'Чому нас обирають?'}}
							</div>
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($benefitMotto) ? $benefitMotto : 'Переваги'}}
							</div>
							<div class="t527__line  t527__bottommargin_lg"> </div>
							<div class="t-section__descr t-descr t-descr_xl" field="bdescr">
								{{ isset($benefitText) ? $benefitText : 'Переваги'}}
							</div>
						</div>
					</div>
				</div>

				<div class="t497__container t-container">
					@if(count($benefitList) > 0)
 
					@foreach($benefitList as $list)
					<div class="t497__col t-col t-col_4  t497__col_first t-item">
						<div class="t497__item">
							<div class="t-cell t-valign_top">
								<div class="t497__imgwrapper">
									<img src="{{asset($list['img'] ) }}"
										class="t497__img  t-img" imgfield="li_img__1476973323713">
								</div>
							</div>
							<div class="t497__textwrapper t-cell t-valign_top">
								<div class="t497__name t-name t-name_lg" field="li_title__1476973323713" style="">
									{!! $list['desc'] !!}
								</div>
								
							</div>
						</div>
					</div>
					@endforeach                                
					@endif

				</div>




			</div>
		</div>
</div>
	