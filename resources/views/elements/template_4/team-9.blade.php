<div id="page">


	<div class="record" id="record226940371" recordid="226940371" off="n" data-record-type="829"
		style="opacity: 1;">


		<div id="rec226940371" class="r" style="padding-top:150px;padding-bottom:150px; "
			data-animationappear="off">

			<!-- t829 -->

			<div class="t829" id="team">

				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Команда'}}
							</div>
							
						</div>
					</div>
				</div>

				<div class="t829__container  t829__container_padd-column t-container">
					<div class="t829__grid  " style="position: relative; width: 1158px; height: 1137.48px;">
						<div class="t829__grid-sizer" style="width: 360px;"></div>
						<div class="t829__gutter-sizer t829__gutter-sizer_40"></div>
						@if(count($teams) > 0)
						@foreach ($teams as $team)

							<div class="t-align_left t-item t829__grid-item t829__grid-item_mb-40 t829__grid-item_flex_padd-40 t829__grid-item_first-flex_padd-12 "
								style="width: 360px; position: absolute; left: 0px; top: 0px;">
								<div class="t829__content">
									<div class="t829__imgwrapper">
										<img src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
											class="t829__img t-img js-product-img" imgfield="li_img__1521103691910"
											data-tu-max-width="700" id="tuwidget621435" style="opacity: 1;">
										<input type="file" class="tu-hidden-input" accept="image/*"
											style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
									</div>
									<div class="t829__textwrapper  ">
										<div class="t829__title t-name t-name_md" field="li_title__1521103691910"
											style="" data-redactor-nohref="yes">{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}</div>
										<div class="t829__descr t-descr t-descr_xxs" field="li_descr__1521103691910"
											style="" data-redactor-nohref="yes">{{ isset($team['value']) ? $team['value'] : ''}}</div>
									</div>
								</div>
							</div>
						@endforeach
						@endif
					</div>
				</div>

			</div>

		</div>			
	</div>

</div>
	