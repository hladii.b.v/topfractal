<div id="page">


	<div class="record" id="record226917762" recordid="226917762" off="" data-record-type="223" style="opacity: 1;">
		

		<div id="rec226917762" class="r" style="padding-top:90px;padding-bottom:75px; ">

			<!-- T195 -->
			<div class="t195"  id="about">
				<div class="t-container">

					<div class="t-col t-col_4 t-prefix_2">
						<div class="t195__text t-text t-text_md" style="" field="text">{{ isset($aboutText) ? $aboutText : 'Одним з основних досягнень клініки є здобута довіра наших пацієнтів, оскільки, велика кількість наших відвідувачів звертається до нас за рекомендаціями своїх рідних чи друзів.
							Успіх завдяки нашій кваліфікованій команді лікарів-стоматологів. Беручи до уваги інтереси наших пацієнтів, ми продовжуємо вдосконалювати наші знання та професійні якості: беремо участь у національних та міжнародних конференціях, курсах підвищення кваліфікації.
						   Наша практика пропонує унікальний спектр стоматологічних послуг, які можуть допомогти Вам і Вашим дітям вести здоровий і комфортний спосіб життя.'}}</div>
					</div>
					<div class="t-col t-col_6  t195__imgsection" >
						<meta itemprop="image"
							content="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}">
						<img class="t195__img t-img" data-tu-max-width="1200" data-tu-max-height="1200"
							src="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}"
							imgfield="img" id="tuwidget297757" style="opacity: 1;"><br>
						<div class="t195__sectitle t-descr" style="" field="imgtitle" itemprop="name"></div>
						<div class="t195__secdescr t-descr" style="" field="imgdescr" itemprop="description"></div>
						<input type="file" class="tu-hidden-input" accept="image/*"
							style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
					</div>
				</div>
			</div>

		</div>

	</div>

</div>
	