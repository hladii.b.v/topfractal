<div id="page">


	<div class="record" id="record226947066" recordid="226947066" off="n" data-record-type="609"
		style="opacity: 1;">

		<div id="rec226947066" class="r" style="padding-top:150px;padding-bottom:150px; "
			data-animationappear="off">

			<!-- T609 -->
			<div class="t609"  id="services">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Вартість наших послуг'}}
							</div>
							@if($pdfFile != '' ) 
								<div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">
									<a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="title__text" target="_blank">Прайс лист</a>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="t-container">
					@if(count($servicesList) > 0)
                           
					@foreach($servicesList as $offer)
						<div class="t609__col t-col t-col_4">
							<div class="t609__wrapper" style="border-radius: 3px;">
								<div class="t609__imgwrapper">
									
									<div class="t609__textwrapper t-align_center">
										<div class="t609__title t-heading t-heading_xs" field="title" style="">
											{{$offer['desc']}}
											<br>
										</div>
										
									</div>
									<input type="file" class="tu-hidden-input" accept="image/*"
										style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
								</div>
								@if(count($offer['children']) > 0)
									<div class="t609__content t-align_center"
										style="border: 1px solid #e0e6ed;  border-radius: 3px;">
										<div class="t609__descr t-descr t-descr_xs" field="descr" style="height: 285px;">
											<ul>
												@foreach($offer['children'] as $offer_child)
													<li>
														<div class="price__service">{{$offer_child['desc']}}</div>
														<div class="price__cost">{{isset($offer_child['price']) ? $offer_child['price'] : ''}}</div>
													</li>
												@endforeach  
											</ul>
										</div>
									</div>
								@endif
							</div>
						</div>

					@endforeach                                
					@endif

				</div>
			</div>
		</div>

	</div>
</div>
