
<div id="page">
	<div class="record" id="record226942875" recordid="226942875" off="" data-record-type="525" style="opacity: 1;">

		<div id="rec226942875" class="r" style="padding-top:120px;padding-bottom:120px; ">

			<!-- t525 -->

			<div class="t525" id="reviews">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}
							</div>
						</div>
					</div>
				</div>
				<div class="t525__container t-container">

					@if(count($reviews) > 0)

                        @foreach ($reviews as $review)

							<div class="t525__col t-col t-col_6">
								
								<div class="t525__textwrapper t525__textwr_leftpadding t-cell t-valign_top">
									<div class="t525__text t-text t-text_xs" field="li_text__1478014691630" style=" ">
										{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}
									</div>
									<div class="t525__title t-name t-name_xs" field="li_title__1478014691630" style=" ">
										{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}
									</div>
								</div>
							</div>

						@endforeach

					@endif
				</div>


			</div>

		</div>
	
	</div>
</div>


		
