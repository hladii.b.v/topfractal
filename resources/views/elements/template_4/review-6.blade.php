
<div id="page">
	<div class="record" id="record226942922" recordid="226942922" off="" data-record-type="942" style="opacity: 1;">


		<div id="rec226942922" class="r" style=" " data-animationappear="off">


			<div class="t-cover" id="recorddiv226942922" bgimgfield="img"
				style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); "
				data-tu-noclick="yes">

				<div class="t-cover__carrier" id="coverCarry226942922" data-content-cover-id="226942922"
					data-content-cover-bg="https://static.tildacdn.com/tild3234-3866-4266-a366-326532346231/sabbir-ahmed-VDww0Ma.png"
					data-content-cover-height="100vh" data-content-cover-parallax=""
					style="background-image:url('https://static.tildacdn.com/tild3234-3866-4266-a366-326532346231/sabbir-ahmed-VDww0Ma.png');height:100vh;background-attachment:scroll; ">
				</div>

				<div class="t-cover__filter"
					style="height:100vh;background-color:#000;filter:alpha(opacity:40);KHTMLOpacity:0.4;MozOpacity:0.4;opacity:0.4;" id="reviews">
				</div>
				<div class="t942">
					<div class="t942__cover t942__cover_center t-cover__wrapper t-container"
						data-auto-correct-mobile-width="false" style="height: 100vh;">
						<div class="t942__content" data-hook-content="covercontent">
							<div class="t942__heading">
								<div class="t942__textwrapper">
									<div class="t942__title t-title t-title_sm " field="btitle" style="">
										{{isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}
									</div>
								</div>
							</div>
							<div class="t942__cards">
								<div class="t942__row" style="">
								@if(count($reviews) > 0)

                                    @foreach ($reviews as $review)

										<div class="t942__col t-col t-col_4 ">
											<div class="t942__card" style="border-radius:4px;">
												<div class="t942__author">												
													<div class="t942__occupation">
														<div class="t942__author-name t-name t-name_xs"
															field="li_title__1592232389408" style="">
															{{ isset($review['name']) ? $review['name'] : 'fgdgs2233'}}
														</div>
														
													</div>
													
												</div>
												<div class="t942__review">
													<div class="t942__review-text t-descr t-descr_xxs"
														field="li_descr__1592232389408" style="">
														{{ isset($review['value']) ? $review['value'] : 'fgdgs'}}
													</div>
												</div>
											</div>
										</div>

									@endforeach

								@endif		
									
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>		


		</div>

	</div>
</div>
		


		

