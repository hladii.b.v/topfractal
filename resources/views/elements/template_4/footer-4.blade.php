<div id="page"> 
 
	<div class="record" id="record226941324" recordid="226941324" off="" data-record-type="455" style="opacity: 1;">
		<div id="rec226941324" class="r" style=" " data-animationappear="off">

			<div class="t007 footer-wrap" id="app">
				<div class="t-container">
					<div class="t-row">
						<div class="t-col t-col_4 ">
							<div class="t007__text t-text t-text_md t-align_center " style="" field="text">
								<img src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}">
							</div>
						</div>
						<div class="t-col t-col_4">
							<div class="t007__text t-text t-text_md text2" style="" field="text2">
								{{ isset($organizationName) ? $organizationName : 'Карат'}}	
							</div>
						</div>
						<div class="t-col t-col_4">
							<div class="t007__text t-text t-text_md t-align_center socials__container_div" style="" field="text3">
								@if($social && count($social)>0  )
									
									@foreach ($social as $key=>$soc)
										<div class="socials__item_div">
											<a class="socials__item" href="{{$soc['value']}}" target="_blank"> 
												<i class="fab fa-{{$soc['name']}}"></i>
											</a> 
										</div>
									@endforeach     
									
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="t455 footer-wrap">
				<div class="t-container">
					<div class="t562__col t-col t-col_3">
						<div class="t562__col-wrapper_first t562__col-wrapper">
							<div class="t562__name t-name t-name_md" style="" field="title2">
								Контакти
							</div>
							<div class="t527__line  t527__bottommargin_lg" style=""> </div>
							<div class="t562__text t-text t-text_md" style="" field="descr">
								@if (isset($adress))
									<i class="fas fa-map-marker-alt"></i>
									{{isset($adress) ? $adress : 'м. Полтава, вул. Європейська, 110'}}
								@endif
							</div>
							<div class="t562__text t-text t-text_md" style="" field="descr">
								{!!isset($email) ? '<i class="fas fa-envelope-open-text"></i> '.$email : 'karat.dental@gmail.com'!!}
							</div>
							<div class="t562__text t-text t-text_md" style="" field="descr"> 
								@if (isset($phone))
								<a href="tel:{{isset($phone) ? preg_replace('/[^0-9]/', '', $phone) : '(0532) 63-77-59'}}">
									
									<i class="fas fa-mobile-alt"></i>
								
									{{isset($phone) ? $phone : '(0532) 63-77-59'}}
								</a>
								@endif
							</div>
						</div>
					</div>
					<div class="t562__col t-col t-col_3">
						<div class=" t562__col-wrapper">
							<div class="t562__name t-name t-name_md" style="" field="title2">Графік</div>
							<div class="t527__line  t527__bottommargin_lg" style=""> </div>
							<div class="t562__text t-text t-text_md" style="" field="descr2"> 
								@if (isset($chart))
									<i class="far fa-clock"></i>
								
									{{isset($chart) ? $chart : ''}}
								@endif
							</div>
						</div>
					</div>
					<div class="t562__col t-col t-col_6">
						<div class=" t562__col-wrapper">
							<div class="t562__text t-text t-text_md" style="" field="descr3">
								@if(isset($adressText)) 
									{!!$adressText!!}                                        
								@endif  
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="t460">

				<div class="t460__wrapper">

					<div class="t460__bottomtext t-descr t-descr_xxs t-align_center" field="text">
						© <a href="https://topfractal.com/" target="_blank">Topfractal</a>. All rights reserved
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
	