<div id="page">
	<div id="rec238580188" class="r t-rec t-rec_pt_150 t-rec_pb_150 r_showed r_anim"
			style="padding-top:150px;padding-bottom:150px; " data-record-type="527">
		
			<div class="t527" id="team">

				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_left">
							<div class="t527__persdescr t-descr t-descr_xxs t527__bottommargin_lg" style=""
									field="li_persdescr__1478035709182">{{ isset($section_name) ? $section_name : 'Команда'}}</div>
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($teamMotto) ? $teamMotto : 'Команда'}}
							</div>
							<div class="t527__line  t527__bottommargin_lg" >
							</div>
							<div class="t-section__descr t-descr t-descr_xl" field="bdescr">
								{{ isset($teamText) ? $teamText : 'Команда'}}
							</div>
						</div>
					</div>
				</div>

				<div class="t527__container t-container">

					@if(count($teams) > 0)
					@foreach ($teams as $team)

						<div class="t527__col t-col t-col_3 t-align_left t527__col-mobstyle">
							<div class="t527__itemwrapper t527__itemwrapper_3">
								<div class="boldPhotoBox">
									<div class="bpbItem">
										<div class="btImage">
											<img src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
												alt="">
										</div>
									</div>
								</div>
								
								<div class="t527__wrapperleft">
									
									<div class="t527__persname t-name t-name_lg t527__bottommargin_sm" style=""
										field="li_persname__1478035709182">
										{{ isset($team['name']) ? $team['name'] : ''}}
									</div>
									
									<div class="t527__line  t527__bottommargin_lg" style="">
									</div>
									<div class="t527__perstext t-text t-text_xs" style="" field="li_text__1478035709182">
										{{ isset($team['value']) ? $team['value'] : ''}}
									</div>
								</div>
							</div>
						</div>
					@endforeach
					@endif
				</div>




			</div>

			<script type="text/javascript">

				$(document).ready(function () {
					t527_setHeight('238580188');

				});

			</script>
		</div>

</div>

