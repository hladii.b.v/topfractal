<div id="page">


	<div class="record" id="record226940224" recordid="226940224" off="n" data-record-type="688"
		style="opacity: 1;" >
	
		<div id="rec226940224" class="r" style="padding-top:135px;padding-bottom:150px; background-color:#ededed;"
			data-animationappear="off" data-bg-color="#ededed">

			<!-- T688 -->

			<div class="t688"  id="team">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Команда'}}
							</div>
							
						</div>
					</div>
				</div>
				<div class="t-container">

					<div class="t688__row">
						@if(count($teams) > 0)
						@foreach ($teams as $team)
							<div class="t-col t-col_4 t-align_left t688__col">
								<div class="t688__inner-col">
									<div class="t688__img-wrapper">
										<meta itemprop="image"
											content="{{isset($team['foto']) ? asset($team['foto']) : ''  }}">
										<div class="t688__img t-bgimg" imgfield="li_img__1494594985229"
											data-original="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
											style="background-image: url('{{isset($team['foto']) ? asset($team['foto']) : ''  }}'); opacity: 1;"
											id="tuwidget720166"></div>
										<div style="" class="t688__img-separator"></div>
										<input type="file" class="tu-hidden-input" accept="image/*"
											style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
									</div>
									<div class="t688__textwrapper " style="height: 116px;">
										<div class="t688__textwrapper_inner">
											<div class="t688__sp"></div>
											<div class="t688__title t-name t-name_xl" style=""
												field="li_title__1494594985229" data-redactor-nohref="yes">
												{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}
											</div>
											<div class="t688__descr t-descr t-descr_xxs" style=""
												field="li_descr__1494594985229" data-redactor-nohref="yes">
												{{ isset($team['value']) ? $team['value'] : ''}}
												<br>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							@endif
					</div>
				</div>
			</div>

		</div>

	</div>

</div>
	