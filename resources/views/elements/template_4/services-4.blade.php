<div id="page">

	<div class="record" id="record226947142" recordid="226947142" off="" data-record-type="792" style="opacity: 1;">


		<div id="rec226947142" class="r" style="padding-top:135px;padding-bottom:135px; "
			data-animationappear="off">

			<!-- T792 -->

			<div class="t792"  id="services"> 
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Вартість наших послуг'}}
							</div>
							@if($pdfFile != '' ) 
								<div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">
									<a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="title__text" target="_blank">Прайс лист</a>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="t-container">
					@if(count($servicesList) > 0)
                           
					@foreach($servicesList as $offer)
						<div class="t-col t-col_8 t-prefix_2">
							<div class="t792__wrapper">
								<div class="t792__uptitle t-uptitle t-uptitle_xs" field="li_uptitle__1508946768182">
									{{$offer['desc']}}<br>
								</div>
								@if(count($offer['children']) > 0)
									@foreach($offer['children'] as $offer_child)
										<div class="t792__text-wrapper t-valign_top">
											<div class="t792__title t-heading t-heading_sm" field="li_title__1508946768182">
												{{$offer_child['desc']}}
											</div>
										</div>
										<div class="t792__price-wrapper t-valign_top">
											<div class="t792__price t-name t-name_lg" field="li_text__1508946768182" >
												{{isset($offer_child['price']) ? $offer_child['price'] : ''}}
												<br>
											</div>
										</div>
									@endforeach 
								@endif
								
							</div>
							<div class="t792__line" style="margin-top: 28px;   "></div>
						</div>
					@endforeach                                
					@endif
				</div>		

			</div>

		</div>


	</div>

</div>
