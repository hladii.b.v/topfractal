<div id="page">

	<div class="record" id="record226941274" recordid="226941274" off="n" data-record-type="457"
		style="opacity: 1;">
		

		<div id="rec226941274" class="r" style="padding-top:60px;padding-bottom:75px; background-color:#171717;"
			data-animationappear="off" data-bg-color="#171717">

			<!-- T457 -->


			<div class="t457" id="app">
				<div class="t-container">
					<div class="t455__title t-title t-title_md" style="" field="title">
						{{ isset($section_name) ? $section_name : 'Запис на прийом до лікаря'}}
					</div>
					<div class="t455__descr t-descr t-descr_md" style="" field="descr">
						@if(isset($adressText)) 
							{!!$adressText!!}                                        
						@endif   
					</div>
					<div class="t-col t-col_12">
						<ul class="t457__ul">
							<li class="t457__li">
								{{isset($adress) ? $adress : 'м. Полтава, вул. Європейська, 110'}}
							</li>
							<li class="t457__li">
								{{isset($chart) ? $chart : ''}}
							</li>
							<li class="t457__li">
								<a href="tel:{{isset($phone) ? preg_replace('/[^0-9]/', '', $phone) : '(0532) 63-77-59'}}">
									{{isset($phone) ? $phone : '(0532) 63-77-59'}}
								</a>
							</li>
						
							<li class="t457__li">
								{{isset($email) ? $email : 'karat.dental@gmail.com'}}
							</li>
						</ul>
					</div>
					<div class="t-col t-col_12">
						@if($social && count($social)>0  )
							<div class="t455__right_social_links">
								<div class="t455__right_social_links_wrap">
								@foreach ($social as $key=>$soc)
									<div class="t455__right_social_links_item">
										<a class="socials__item" href="{{$soc['value']}}" target="_blank"> 
														
											<div class="socials__round">
												<img  src="{{asset( $soc['img'] )}}">   
											</div>
											<div class="socials__caption">
												{{$soc['name']}}
											</div>
										</a> 
									</div>	
								@endforeach      
								</div>
							</div>
						@endif
					</div>
					<div class="t-col t-col_12">
						<div class="t457__copyright" field="text" style="color: #ffffff;">
							<a href="https://topfractal.com/" target="_blank">topfractal.com</a>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>
	