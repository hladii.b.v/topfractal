<div id="page">


	<div class="record" id="record226946484" recordid="226946484" off="n" data-record-type="849"
		style="opacity: 1;">
		
		<div id="rec226946484" class="r" style="padding-top:135px;padding-bottom:165px; "
			data-animationappear="off">

			<!-- t849-->
			<div class="t849"  id="services">

				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Вартість наших послуг'}}
								<br>
							</div>
							@if($pdfFile != '' ) 
								<div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">
									<a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="title__text" target="_blank">Прайс лист</a>
									<br>
								</div>
							@endif
						</div>
					</div>
				</div>

				<div class="t-container">
					@if(count($servicesList) > 0)
                           
					@foreach($servicesList as $offer)
						<div class="t-col t-col_8 t-prefix_2">
							<div class="t849__accordion" data-accordion="false">
								<div class="t849__wrapper">
									<div class="t849__header " style=" border-top: 1px solid #eee;">
										<div class="t849__title t-name t-name_xl" field="li_title__1531218331971">
											{{$offer['desc']}}
											<br>
										</div>
										<div class="t849__icon">
											<div class="t849__lines">
												<svg width="24px" height="24px" viewBox="0 0 24 24"
													xmlns="http://www.w3.org/2000/svg"
													xmlns:xlink="http://www.w3.org/1999/xlink">
													<g stroke="none" stroke-width="2px" fill="none" fill-rule="evenodd"
														stroke-linecap="square">
														<g transform="translate(1.000000, 1.000000)" stroke="#222222">
															<path d="M0,11 L22,11"></path>
															<path d="M11,0 L11,22"></path>
														</g>
													</g>
												</svg>
											</div>
											<div class="t849__circle" style="background-color: transparent;"></div>
										</div>
										<div class="t849__icon t849__icon-hover">
											<div class="t849__lines">
												<svg width="24px" height="24px" viewBox="0 0 24 24"
													xmlns="http://www.w3.org/2000/svg"
													xmlns:xlink="http://www.w3.org/1999/xlink">
													<g stroke="none" stroke-width="2px" fill="none" fill-rule="evenodd"
														stroke-linecap="square">
														<g transform="translate(1.000000, 1.000000)" stroke="#ffffff">
															<path d="M0,11 L22,11"></path>
															<path d="M11,0 L11,22"></path>
														</g>
													</g>
												</svg>
											</div>
											<div class="t849__circle"></div>
										</div>
									</div>
									@if(count($offer['children']) > 0)
										<div class="t849__content">
											<div class="t849__textwrapper">
												<div class="t849__text t-descr t-descr_sm" field="li_descr__1531218331971"
													style="">
													<ul>
														@foreach($offer['children'] as $offer_child)
															<li>
																<div class="price__service">{{$offer_child['desc']}}</div>
																<div class="price__cost">{{isset($offer_child['price']) ? $offer_child['price'] : ''}}</div>
															</li>
														@endforeach  
													</ul>
												</div>
											</div>
										</div>
									@endif
								</div>
							</div>
						</div>
					@endforeach                                
					@endif
				</div>
			</div>

			<script type="text/javascript">
				$(document).ready(function(){
					t849_init('226946484');
				});
			</script>
		</div>
	</div>

</div>
	