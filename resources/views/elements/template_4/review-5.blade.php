
<div id="page">
	<div class="record" id="record226942912" recordid="226942912" off="" data-record-type="530" style="opacity: 1;">

		<div id="rec226942912" class="r" 
			data-bg-color="#eeeeee">

			<!-- t530 -->

			<div class="t530 vc_custom_1563429402389"  id="reviews">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}
							</div>
						</div>
					</div>
				</div>
				<div class="t530__container t-container">
					@if(count($reviews) > 0)

						@foreach ($reviews as $key=>$review)

							<div class="t530__col t-col t-col_8 t-prefix_2">								
								<div class="t530__cell t-cell {{($key+1)%2 == 0 ? ' t530__cell_right ' : ''}} t-valign_top">
									<div class="t530__bubble t530__bubble_left" style=" ">
										<div class="t530__name t-name t-name_xs" field="li_title__1478014691630" style=" ">
											{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}
										</div>
										
										<div class="t530__text t-text t-text_xs" field="li_text__1478014691630" style=" ">
											{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}
										</div>


									</div>
								</div>
							</div>
							<div class="t530__separator t-clear" style=""></div>

						@endforeach

					@endif

				</div>


			</div>

		</div>
	</div>
</div>
		



		
