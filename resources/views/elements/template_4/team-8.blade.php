<div id="page">

	<div class="record" id="record226940295" recordid="226940295" off="n" data-record-type="912"
		style="opacity: 1;">

		<div id="rec226940295" class="r" style="padding-top:135px;padding-bottom:135px; background-color:#111111;"
			data-animationappear="off" data-bg-color="#111111">

			<!-- t912 -->


			<div class="t912" id="team">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								<div style="color:#ffffff;" data-customstyle="yes">
									{{ isset($section_name) ? $section_name : 'Команда'}}	
								</div>
							</div>
							
						</div>
					</div>
				</div>

				<div class="t-container t912__container">
					@if(count($teams) > 0)
                    @foreach ($teams as $team)
						<div class="t912__col t-col t-col_5 t-align_left t-item">
							<div class="t912__content">
								<div class="t912__imgwrapper" style="padding-bottom: 460px;">
									<meta itemprop="image"
										content="{{isset($team['foto']) ? asset($team['foto']) : ''  }}">
									<meta itemprop="caption" content="The Team teachable"> <img
										src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
										class="t912__img t-img js-product-img" imgfield="li_img__1574083928695"
										alt="The Team teachable" data-tu-max-width="700" id="tuwidget837433">
									<input type="file" class="tu-hidden-input" accept="image/*"
										style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
								</div>
								<div class="t912__textwrapper">
									<div class="t912__title t-name t-name_md" field="li_title__1574083928695"
										style="color:#ffffff;" data-redactor-nohref="yes">
										{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}
									</div>
									<div class="t912__descr t-descr t-descr_xs" field="li_descr__1574083928695"
										style="color:#ffffff;" data-redactor-nohref="yes">
										{{ isset($team['value']) ? $team['value'] : ''}}	
									</div>
								</div>
							</div>
							
						</div>
					@endforeach
					@endif

				</div>
			</div>
		</div>

	</div>


</div>
	