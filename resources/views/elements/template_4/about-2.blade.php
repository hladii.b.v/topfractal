 <div id="page">

	<div id="rec238734242" class="r "
			 data-record-type="479"
			data-bg-color="#eeeeee" data-animationappear="off">


		<!-- t479 -->
		<div class="t479 t-rec t-rec_pt_75" id="bt_section5f859116cdab9">
			<div class="t-container" id="about">
				<div class="t479__col t-margin_auto t-width t-width_10">
					<div class="t479__wrapper">
						<div class="t479__textwrapper t-align_center">
							<div class="t479__title t-title t-title_lg t-margin_auto" field="title" style="">
								{{ isset($section_name) ? $section_name : 'Про нас'}}
							</div>
							<div class="t527__line  t527__bottommargin_lg  t-margin_auto"> </div>
							<div class="t479__descr t-descr t-descr_md t-margin_auto" field="descr" style="">
								{{ isset($aboutText) ? $aboutText : ''}}
							</div>
						</div>
						<div class="t479__videowrapper">
						
							<div class="btImage">
								<img src="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}" alt="">
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">

			var div = $("#rec238734242").find('.t-video-lazyload');
			var iframe = div.find(iframe);
			var height = div.width() * 0.5625;
			div.height(height);
			iframe.height(height);

		</script>
	</div>

</div>