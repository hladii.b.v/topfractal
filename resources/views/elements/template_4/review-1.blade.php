
<div id="page"> 
	<div class="record" id="record226942835" recordid="226942835" off="n" data-record-type="605"
		style="opacity: 1;">

		<div id="rec226942835" class="r" style="padding-top:135px;padding-bottom:135px; ">

			<!-- t605 -->

			<div class="t605 t605__witharrows" id="reviews">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}
							</div>
						</div>
					</div>
				</div>

				<div class="t-slds" style="">
					<div class="t-slds__main t-container">
						<div class="t-slds__container">
							<div class="t-slds__items-wrapper  t-slds_animated-none" data-slider-transition="300"
								data-slider-with-cycle="true" data-slider-correct-height="true"
								data-auto-correct-mobile-width="false" data-slider-is-preview="true"
								data-slider-initialized="true" data-slider-totalslides="3" data-slider-pos="1"
								data-slider-curr-pos="1" data-slider-cycle="" data-slider-animated=""
								style="width: 6000px; height: 380px; transform: translateX(-1200px); touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">

								@if(count($reviews) > 0)

									<div class="t-slds__item t-slds__item-loaded" data-slide-index="0"
										style="width: 1200px;">
										<div class="t-width t-width_7 t-margin_auto">
											<div class="t-slds__wrapper t-align_center">
												
												<div class="t605__text t-text t-text_md " field="li_text__1477576758810"
													data-animate-order="2" data-animate-delay="0.3" style="">You can't
													connect the dots looking forward, you can only connect them looking
													backwards.</div>
												<div class="t605__title t-name t-name_lg "
													field="li_title__1477576758810" data-animate-order="3"
													data-animate-delay="0.3" style="">Alex Larkins</div>
												<div class="t605__descr t-descr t-descr_xxs "
													field="li_descr__1477576758810" data-animate-order="4"
													data-animate-delay="0.2" style="">Founder of <a
														href="https://google.com/">Pic Pen studio</a></div>
											</div>
										</div>
									</div>

									@foreach ($reviews as $key=>$review)
									
										<div class="t-slds__item t-slds__item-loaded" data-slide-index="{{$key+1}}"
											style="width: 1200px;">
											<div class="t-width t-width_7 t-margin_auto">
												<div class="t-slds__wrapper t-align_center">
													
													<div class="t605__text t-text t-text_md " field="li_text__1477576758810"
														data-animate-order="{{$key+1}}" data-animate-delay="0.3" style="">
														{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}
													</div>
													<div class="t605__title t-name t-name_lg "
														field="li_title__1477576758810" data-animate-order="{{$key+1}}"
														data-animate-delay="0.3" style="">
														{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}
													</div>
												
												</div>
											</div>
										</div>

								
									@endforeach

									<div class="t-slds__item t-slds__item-loaded" data-slide-index="{{count($reviews)+1}}"
										style="width: 1200px;">
										<div class="t-width t-width_7 t-margin_auto">
											<div class="t-slds__wrapper t-align_center">
												
												<div class="t605__text t-text t-text_md " field="li_text__1477576758810"
													data-animate-order="2" data-animate-delay="0.3" style="">You can't
													connect the dots looking forward, you can only connect them looking
													backwards.</div>
												<div class="t605__title t-name t-name_lg "
													field="li_title__1477576758810" data-animate-order="{{count($reviews)+1}}"
													data-animate-delay="0.3" style="">Alex Larkins</div>
												<div class="t605__descr t-descr t-descr_xxs "
													field="li_descr__1477576758810" data-animate-order="{{count($reviews)+1}}"
													data-animate-delay="0.2" style="">Founder of <a
														href="https://google.com/">Pic Pen studio</a></div>

												
											</div>
										</div>
									</div>

								@endif

							</div>


						</div>
						<div class="t-slds__arrow_container  ">



							<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-left"
								data-slide-direction="left" style="height: 380px;">
								<div class="t-slds__arrow t-slds__arrow-left t-slds__arrow-withbg"
									style="width: 30px; height: 30px;background-color: rgba(232,232,232,1);">
									<div class="t-slds__arrow_body t-slds__arrow_body-left" style="width: 7px;">
										<svg style="display: block" viewBox="0 0 7.3 13"
											xmlns="http://www.w3.org/2000/svg"
											xmlns:xlink="http://www.w3.org/1999/xlink">
											<desc>Left</desc>
											<polyline fill="none" stroke="#222222" stroke-linejoin="butt"
												stroke-linecap="butt" stroke-width="1"
												points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
										</svg>
									</div>
								</div>
							</div>
							<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-right"
								data-slide-direction="right" style="height: 380px;">
								<div class="t-slds__arrow t-slds__arrow-right t-slds__arrow-withbg"
									style="width: 30px; height: 30px;background-color: rgba(232,232,232,1);">
									<div class="t-slds__arrow_body t-slds__arrow_body-right" style="width: 7px;">
										<svg style="display: block" viewBox="0 0 7.3 13"
											xmlns="http://www.w3.org/2000/svg"
											xmlns:xlink="http://www.w3.org/1999/xlink">
											<desc>Right</desc>
											<polyline fill="none" stroke="#222222" stroke-linejoin="butt"
												stroke-linecap="butt" stroke-width="1"
												points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
										</svg>
									</div>
								</div>
							</div>
						</div>
						<div class="t-slds__bullet_wrapper">

							<div class="t-slds__bullet t-slds__bullet_active" data-slide-bullet-for="1">
								<div class="t-slds__bullet_body"
									style="width: 6px; height: 6px;background-color: #c7c7c7;"></div>
							</div>

							<div class="t-slds__bullet " data-slide-bullet-for="2">
								<div class="t-slds__bullet_body"
									style="width: 6px; height: 6px;background-color: #c7c7c7;"></div>
							</div>

							<div class="t-slds__bullet " data-slide-bullet-for="3">
								<div class="t-slds__bullet_body"
									style="width: 6px; height: 6px;background-color: #c7c7c7;"></div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<script type="text/javascript">
				$(document).ready(function () {
					t_sldsInit('226942835');


					t_slds_UpdateSliderHeight('226942835');
					t_slds_UpdateSliderArrowsHeight('226942835');

				});
			</script>
		</div>

	</div>
</div>
