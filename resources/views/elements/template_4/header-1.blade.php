<div id="page">
	
	
	<div class="record" id="record226875255" recordid="226875255" off="n" data-record-type="18" style="opacity: 1;">
		<div id="rec226875255" class="r"  data-animationappear="off">

			<div class="t-cover" id="recorddiv226875255" bgimgfield="img"
				style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); "
				data-tu-noclick="yes">

				<div class="t-cover__carrier" id="coverCarry226875255" data-content-cover-id="226875255"
					data-content-cover-bg="{{asset( isset($header_img) ? $header_img : '/images/offer__bgi-img-1.png')}}"
					data-content-cover-height="100vh" data-content-cover-parallax="fixed"
					style="background-image:url(&#39;{{asset( isset($header_img) ? $header_img : '/images/offer__bgi-img-1.png')}}&#39;);height:100vh; ">
				</div>

				<div class="t-cover__filter"
					style="height:100vh;background-image: -moz-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: -o-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: -ms-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr=&#39;#4c000000&#39;, endColorstr=&#39;#4c000000&#39;);">
				</div>

				<div class="t-container">
					<div class="t-col t-col_12 ">
						<div class="t-cover__wrapper t-valign_middle" style="height:100vh;">
							<div class="t001 t-align_center">
								<div class="t001__wrapper" data-hook-content="covercontent">
									
									<div class="t001__uptitle t-uptitle t-uptitle_sm"
										style="text-transform:uppercase;" field="subtitle">
										{{isset($headerMotto) ? $headerMotto : 'Карат'}}
									</div>
									<div class="t001__title t-title t-title_xl" style="" field="title">
										{{ isset($headerTitle) ? $headerTitle : 'fghdfghdf'}}
									</div>
									<div class="t001__descr t-descr t-descr_xl t001__descr_center" style=""
										field="descr">
										{{ isset($headerText) ? $headerText : 'fghdfghdf'}}
									</div> <span class="space"></span>
								</div>
							</div>
						</div>
					</div>
				</div>

				
				<!-- arrow -->


			</div>


			<input type="file" class="tu-hidden-input" accept="image/*"
				style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
		</div>


		<div class="recordborderbottom ui-draggable ui-draggable-handle" style="height: 10px; margin-top: -10px;">
		</div>

	</div>		

</div>
