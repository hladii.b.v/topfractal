<div id="page">

    <style>
        a.t-menu__link-item:hover, a.t-menu__link-item,  .btIco .btIcoHolder:after,  a.socials__item{            
            color: {{$color1}} !important;
            
        }
        polyline {
            stroke: {{$color1}} !important;
        }
        .t446__list_item :hover{
            box-shadow: 0 -3px 0 0 {{$color1}}  inset;
        }
        .t527__line, .t849__header, .t460
        {
            background-color: {{$color1}} !important;
        }

        .t479__title.t-title.t-title_lg.t-margin_auto, .t189__title h1, .t-section__title.t-title.t-title_xs, .t-section__title.t-title.t-title_xs, li.t228__list_item,
        .t562__name.t-name.t-name_md
         {
            color: {{$color2}} !important;
        }
        
        .t849__icon, .t-slds__arrow.t-slds__arrow-left.t-slds__arrow-withbg, .t-slds__arrow.t-slds__arrow-right.t-slds__arrow-withbg
        {
            background-color: {{$color2}} !important;
        }
        
        .t189__descr.t-descr.t-descr_sm, .t479__descr.t-descr.t-descr_md.t-margin_auto, .t527__persdescr.t-descr.t-descr_xxs.t527__bottommargin_lg, .t-section__descr.t-descr.t-descr_xl
        .t497__name.t-name.t-name_lg, .t527__persdescr.t-descr.t-descr_xxs.t527__bottommargin_lg, .t527__persname.t-name.t-name_lg.t527__bottommargin_sm,
        .t527__perstext.t-text.t-text_xs, .t-section__descr.t-descr.t-descr_xl, .t497__name.t-name.t-name_lg, .t-section__descr.t-descr.t-descr_xl,
        .t562__text.t-text.t-text_md

        {
            color: {{$color3}} !important;
        }
      

        .t189__title h1, .t479__title.t-title.t-title_lg.t-margin_auto, .t-section__title.t-title.t-title_xs, .t-section__title.t-title.t-title_xs {
            font-weight: 600  !important;
            font-size: 60px !important;
        }
        .t189__descr.t-descr.t-descr_sm, .t527__persdescr.t-descr.t-descr_xxs.t527__bottommargin_lg, .t527__persdescr.t-descr.t-descr_xxs.t527__bottommargin_lg,  
        .t527__persname.t-name.t-name_lg.t527__bottommargin_sm, li.t228__list_item, .t562__name.t-name.t-name_md
        {
            font-size: 25px  !important;
        }
        .t479__descr.t-descr.t-descr_md.t-margin_auto, .t-section__descr.t-descr.t-descr_xl, .t497__name.t-name.t-name_lg, .t562__text.t-text.t-text_md
        {
            font-size: 20px  !important;
        }
        .t849__circle {
            background-color:#ba4cb8;
        }
    </style>

    <div id="rec237532917" class="r" style=" " data-animationappear="off">
        <div id="nav237532917marker"></div>
        <div class="t770__mobile">
            <div class="t770__mobile_container">
                <div class="t770__mobile_text t-name t-name_md" field="text">&nbsp;</div>
                <div class="t770__mobile_burger"> <span></span> <span></span> <span></span> <span></span> </div>
            </div>
        </div>
        <div id="nav237532917" class="t770 t770_redactor-mode t770__hidden  " style="position: static;"
            data-bgcolor-hex="" data-bgcolor-rgba="" data-navmarker="nav237532917marker" data-appearoffset=""
            data-bgopacity-two="" data-menushadow="" data-bgopacity="1" data-menu-items-align="" data-menu="yes"
            data-bgcolor-setbyscript="yes">
            <div class="t770__maincontainer t770__c12collumns">
                <div class="t770__topwrapper" style="height:150px;">
                    <div class="t770__logowrapper">
                        <div class="t770__logowrapper2">
                            <div style="display: block;"> <img
                                    src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}"
                                    class="t770__imglogo t770__imglogomobile" imgfield="img"
                                    style=" max-height: 100px;" alt="Logo" data-img-width="250px"
                                    id="tuwidget874017"> <input type="file" class="tu-hidden-input" accept="image/*"
                                    style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
                            </div>
                        </div>
                    </div>
                    <div class="t770__listwrapper t770__mobilelist">
                        <ul class="t770__list">
                            @if(count($menu) > 0)

                                @foreach($menu	 as $value)    
                                @if (isset($value['name'])) 
                                    
                                    <li class="t770__list_item">
                                        <a class="t-menu__link-item" href="{{$value['link']}}"
                                        data-menu-submenu-hook=""
                                        style="font-weight:600;letter-spacing:0.5px;text-transform:uppercase;"
                                        data-menu-item-number="1">{{$value['name']}}</a> 
                                    </li>
                                @endif
                                @endforeach
						        @endif 
                           
                        </ul>
                    </div>
                    <div class="t770__alladditional">
                        <div class="t770__leftwrapper" style="padding-left: 20px; padding-right:175px;">
                            <div class="t770__additionalwrapper t-align_center">
                                <div class="t770__descr t770__additionalitem" style=""> Дентіс </div>
                            </div>
                        </div>
                        <div class="t770__rightwrapper" style="padding-right: 20px; padding-left:175px;">
                            <div class="t770__additionalwrapper t-align_center">
                                <div class="t770__descr t770__additionalitem" style="">+1 123 456 78
                                    90<br>hello@monomo.com<br></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            @media screen and (max-width: 980px) {
                #rec237532917 .t770__leftcontainer {
                    padding: 20px;
                }

                #rec237532917 .t770__imglogo {
                    padding: 20px 0;
                }
            }
        </style>
        <script> $(document).ready(function () { t770_init('237532917'); }); </script>
        <div class="t-menusub__container"> </div>
        <style>
            @media screen and (max-width: 980px) {
                #rec237532917 .t-menusub__menu .t-menusub__link-item {
                    color: #000000 !important;
                }

                #rec237532917 .t-menusub__menu .t-menusub__link-item.t-active {
                    color: #000000 !important;
                }
            }
        </style>
    </div>
    <div id="rec238251750" class="r t-rec" style=" " data-animationappear="off" data-record-type="230">

        <div id="nav238251750" class="t199__header t199__js__header"
            style="background-color: rgba(255,255,255)" data-menu="yes">
            <div id="rec238207443" class="r" style=" " data-animationappear="off">
                
                <!-- t821 -->
    
    
                <div id="nav238207443marker"></div>
                <div id="nav2382077043marker"></div>
                <div id="nav238207443" class="t821  t821__positionstatic" style="" data-bgcolor-hex="" data-bgcolor-rgba=""
                    data-navmarker="nav238207443marker" data-appearoffset="" data-bgopacity-two="" data-menushadow=""
                    data-bgopacity="1" data-menu-items-align="" data-menu="yes" data-bgcolor-setbyscript="yes">
                    <div class="t821__maincontainer " style="">
    
                        <div class="t821__leftwrapper" style="">
                            <div class="t821__logo-wrapper t821__logo-wrapper_inlineblock">
                                <img src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}" class="btMainLogo" imgfield="img"
                                    alt="/" id="tuwidget220770">
                            </div>
                        </div>
    
                        <div class="t821__rightwrapper" style="">
                            <div class="topBarInLogoArea">
    
                                <div class="topBarInLogoAreaCell">
                                    <span class="btIconWidget ">
                                        <span class="btIconWidgetIcon">
                                            <span class="btIco ">
                                                <span data-ico-es="" class="btIcoHolder">
                                                    <em></em>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="btIconWidgetContent"><span class="btIconWidgetTitle">
                                                Графік
                                            </span>
                                            <span class="btIconWidgetText">
                                                {{isset($chart) ? $chart : ''}}
                                            </span>
                                        </span>
                                    </span>
                                    <span class="btIconWidget ">
                                        <span class="btIconWidgetIcon">
                                            <span class="btIco ">
                                                <span data-ico-es="" class="btIcoHolder">
                                                    <em></em>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="btIconWidgetContent">
                                            <span class="btIconWidgetTitle">
                                                Телефон
                                            </span>
                                            <span class="btIconWidgetText">
                                                {{isset($phone) ? $phone : '(0532) 63-77-59'}}
                                            </span>
                                        </span>
                                    </span>
                                    <span class="btIconWidget ">
                                        <span class="btIconWidgetIcon">
                                            <span class="btIco ">
                                                <span data-ico-pl="" class="btIcoHolder">
                                                    <em></em>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="btIconWidgetContent">
                                            <span class="btIconWidgetTitle">
                                                Адрес
                                            </span>
                                            <span class="btIconWidgetText">
                                                {{isset($adress) ? $adress : 'м. Полтава, вул. Європейська, 110'}}
                                            </span>
                                        </span>
                                    </span>
                                </div><!-- /topBarInLogoAreaCell -->
                            </div>
    
                        </div>
    
    
                    </div>
                </div>
                <div id="nav238207704" class="t446  t446__positionabsolute " data-bgcolor-hex="#000000"
                data-navmarker="nav238207704marker" data-appearoffset="" data-bgopacity-two="" data-menushadow="0"
                data-bgopacity="0.50" data-menu-items-align="left" data-menu="yes" data-bgcolor-setbyscript="yes">
                <div class="t446__maincontainer " style="">


                    <div class="t446__leftwrapper" style="text-align: left; padding-right: 107.5px;">
                        <div class="t446__leftmenuwrapper">
                            <ul class="t446__list">
                                @if(count($menu) > 0)

                                @foreach($menu	 as $value)    
                                @if (isset($value['name'])) 
                                    <li class="t446__list_item"><a class="t-menu__link-item" href="{{$value['link']}}"
                                            data-menu-submenu-hook="" data-menu-item-number="1">{{$value['name']}}</a>
                                    </li>
                                @endif
                                @endforeach
						        @endif 
                            </ul>
                        </div>
                    </div>

                    <div class="t446__rightwrapper" style="text-align: right; padding-left: 107.5px;">
                        <div class="t446__rightmenuwrapper">
                            <ul class="t446__list">
                            </ul>

                        </div>

                        <div class="t446__additionalwrapper">


                            <div class="t-sociallinks">
                                @if(isset($social) && count($social)>0  )
                                    
                                    @foreach ($social as $key=>$soc)
                                        <div class="t-sociallinks__wrapper">
                                            <div class="t-sociallinks__item">
                                                <a class="socials__item" href="{{$soc['value']}}" target="_blank"> 
                                                    <i class="fab fa-{{$soc['name']}}"></i>
                                                </a> 
                                            </div>
                                        </div>
                                    @endforeach     
                                   
                                @endif
                            </div>

                        </div>

                    </div>
                </div>
            </div>
    
                <style type="text/css">
                    @media screen and (max-width: 640px) {
                        #rec238207443 .t821__logo-wrapper_inlineblock {
                            margin-bottom: 0px;
                        }
                    }
                </style>
    
    
    
                <script type="text/javascript">
    
                    $(document).ready(function () {
                        t821_init('238207443');
                    });
    
                </script>
    
    
    
    
    
            </div>
        </div>


        <script>
            $(document).ready(function () {
                t199_showMenu('238251750');
                t199_positionHeader('238251750');

                t199_highlight('238251750');
                t199_checkAnchorLinks('238251750');

            });
        </script>



        <style>
            @media screen and (max-width: 1024px) {}
        </style>


        <style>
            @media screen and (max-width: 980px) {
                #rec238251750 .t-menusub__menu .t-menusub__link-item {
                    color: #000000 !important;
                }

                #rec238251750 .t-menusub__menu .t-menusub__link-item.t-active {
                    color: #000000 !important;
                }
            }
        </style>

    </div>

</div>