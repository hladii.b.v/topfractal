
<div id="page">
	<div class="record" id="record226942861" recordid="226942861" off="n" data-record-type="728"
			style="opacity: 1;">


			<div id="rec226942861" class="r" style="padding-top:135px;padding-bottom:135px; background-color:#eeeeee;"
				data-bg-color="#eeeeee">

				<!-- t728 -->

				<div class="t728 t728__witharrows" id="reviews">
					<div class="t-section__container t-container">
						<div class="t-col t-col_12">
							<div class="t-section__topwrapper t-align_center">
								<div class="t-section__title t-title t-title_xs" field="btitle">
									{{isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}
								</div>
							</div>
						</div>
					</div>

					<div class="t-slds" style="">
						<div class="t-slds__main t-container">
							<div class="t-slds__container">

								<div class="t-slds__items-wrapper  t-slds_animated-none" data-slider-transition="300"
									data-slider-with-cycle="true" data-slider-correct-height="true"
									data-auto-correct-mobile-width="false" data-slider-is-preview="true"
									data-slider-initialized="true" data-slider-totalslides="3" data-slider-pos="1"
									data-slider-curr-pos="1" data-slider-cycle="" data-slider-animated=""
									style="width: 6000px; height: 360px; transform: translateX(-1200px); touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
									@if(count($reviews) > 0)

										<div class="t-slds__item t-slds__item-loaded" data-slide-index="0"
											style="width: 1200px;">
											<div class="t-width t-width_9 t-margin_auto">
												<div class="t-slds__wrapper">
													<div class="t728__wrapper" style="">
														<div class="t728__textcell">
															<div class="t728__textwrapper">
																<div class="">
																	<div class="t728__text t-text t-text_md"
																		field="li_text__1476964743259" style="">The
																		principal element of Suprematism in painting, as in
																		architecture, is its liberation from all social or
																		materialist tendencies. Through Suprematism, art
																		comes into its pure and unpolluted form.</div>
																	<div class="t728__title t-name t-name_xs"
																		field="li_title__1476964743259" style="">Sarah Lewin
																	</div>
																	<div class="t728__descr t-descr t-descr_xxs"
																		field="li_descr__1476964743259" style="">Projects
																		manager in <a href="https://tilda.cc/">Pixels</a>
																	</div>

																</div>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>

										@foreach ($reviews as $key=>$review)
											<div class="t-slds__item t-slds__item-loaded" data-slide-index="{{$key+1}}"
												style="width: 1200px;">
												<div class="t-width t-width_9 t-margin_auto">
													<div class="t-slds__wrapper">
														<div class="t728__wrapper" style="">
															<div class="t728__textcell">
																<div class="t728__textwrapper">
																	<div class="">
																		<div class="t728__text t-text t-text_md"
																			field="li_text__1476964743259" style="">
																			{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}
																		</div>
																		<div class="t728__title t-name t-name_xs"
																			field="li_title__1476964743259" style="">
																			{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}
																		</div>

																	</div>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										@endforeach

										<div class="t-slds__item t-slds__item-loaded" data-slide-index="{{count($reviews)+1}}"
											style="width: 1200px;">
											<div class="t-width t-width_9 t-margin_auto">
												<div class="t-slds__wrapper">
													<div class="t728__wrapper" style="">
														<div class="t728__textcell">
															<div class="t728__textwrapper">
																<div class="">
																	<div class="t728__text t-text t-text_md"
																		field="li_text__1476964743259" style="">The
																		principal element of Suprematism in painting, as in
																		architecture, is its liberation from all social or
																		materialist tendencies. Through Suprematism, art
																		comes into its pure and unpolluted form.</div>
																	<div class="t728__title t-name t-name_xs"
																		field="li_title__1476964743259" style="">Sarah Lewin
																	</div>
																	<div class="t728__descr t-descr t-descr_xxs"
																		field="li_descr__1476964743259" style="">Projects
																		manager in <a href="https://tilda.cc/">Pixels</a>
																	</div>

																</div>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
									@endif
								</div>

							</div>
							<div class="t-slds__arrow_container  ">



								<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-left"
									data-slide-direction="left" style="height: 360px;">
									<div class="t-slds__arrow t-slds__arrow-left t-slds__arrow-withbg"
										style="width: 30px; height: 30px;background-color: rgba(255,255,255,1);">
										<div class="t-slds__arrow_body t-slds__arrow_body-left" style="width: 7px;">
											<svg style="display: block" viewBox="0 0 7.3 13"
												xmlns="http://www.w3.org/2000/svg"
												xmlns:xlink="http://www.w3.org/1999/xlink">
												<desc>Left</desc>
												<polyline fill="none" stroke="#222" stroke-linejoin="butt"
													stroke-linecap="butt" stroke-width="1"
													points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
											</svg>
										</div>
									</div>
								</div>
								<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-right"
									data-slide-direction="right" style="height: 360px;">
									<div class="t-slds__arrow t-slds__arrow-right t-slds__arrow-withbg"
										style="width: 30px; height: 30px;background-color: rgba(255,255,255,1);">
										<div class="t-slds__arrow_body t-slds__arrow_body-right" style="width: 7px;">
											<svg style="display: block" viewBox="0 0 7.3 13"
												xmlns="http://www.w3.org/2000/svg"
												xmlns:xlink="http://www.w3.org/1999/xlink">
												<desc>Right</desc>
												<polyline fill="none" stroke="#222" stroke-linejoin="butt"
													stroke-linecap="butt" stroke-width="1"
													points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
											</svg>
										</div>
									</div>
								</div>
							</div>
							<div class="t-slds__bullet_wrapper">

								<div class="t-slds__bullet t-slds__bullet_active" data-slide-bullet-for="1">
									<div class="t-slds__bullet_body"
										style="width: 6px; height: 6px;background-color: #adadad;"></div>
								</div>

								<div class="t-slds__bullet " data-slide-bullet-for="2">
									<div class="t-slds__bullet_body"
										style="width: 6px; height: 6px;background-color: #adadad;"></div>
								</div>

								<div class="t-slds__bullet " data-slide-bullet-for="3">
									<div class="t-slds__bullet_body"
										style="width: 6px; height: 6px;background-color: #adadad;"></div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function () {
						t_sldsInit('226942861');				

						t_slds_UpdateSliderHeight('226942861');
						t_slds_UpdateSliderArrowsHeight('226942861');

					}); 
				</script>


			</div>
	</div>
</div>
		


		
