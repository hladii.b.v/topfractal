<div id="page">


	<div class="record" id="record226942208" recordid="226942208" off="n" data-record-type="531"
		style="opacity: 1;">
	
		<div id="rec226942208" class="r" style="padding-top:150px;padding-bottom:150px; ">

			<div class="t531"  id="team">

				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Команда'}}
							</div>
							
						</div>
					</div>
				</div>

				<div class="t-container">
					@if(count($teams) > 0)
                    @foreach ($teams as $team)
						<div class="t531__row" style="">
							<div class="t-col t-col_4 t-prefix_1 t531__leftcol">
								<div class="t531__imgwrapper t531__imgwrapper_lg">
									<div class="t531__blockimg t-bgimg" bgimgfield="li_img__1478078229569"
										data-original="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
										style="background-image: url('{{isset($team['foto']) ? asset($team['foto']) : ''  }}'); padding-bottom: 105.556%;"
										data-tu-max-width="560" data-tu-max-height="560" data-image-width="360"
										data-image-height="380" id="tuwidget134977"></div>
									<input type="file" class="tu-hidden-input" accept="image/*"
										style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
								</div>
							</div>
							<div class="t-col t-col_5  t531__rightcol">
								<div class="t531__textwrapper t-align_left" style="height: 380px;">
									<div class="t531__content t-valign_middle">
										<div class="t531__box">
											<div class="t531__title t-name t-name_xl  t531__bottommargin_sm"
												field="li_persname__1478078229569" style="">{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}</div>
											
											<div class="t531__text t-text t-text_sm " field="li_text__1478078229569"
												style="">
												{{ isset($team['value']) ? $team['value'] : ''}}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
					@endif
				</div>

			</div>

		</div>
	</div>

</div>
	