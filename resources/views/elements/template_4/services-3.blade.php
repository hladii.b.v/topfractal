<div id="page">


	<div class="record" id="record226947079" recordid="226947079" off="n" data-record-type="616"
		style="opacity: 1;">

		<div id="rec226947079" class="r" style="padding-top:150px;padding-bottom:150px; "
			data-animationappear="off">
			<!-- T616 -->

			<div class="t616"  id="services">
				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Вартість наших послуг'}}
							</div>
							@if($pdfFile != '' ) 
								<div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">
									<a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="title__text" target="_blank">Прайс лист</a>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="t-container">
					@if(count($servicesList) > 0)
                           
					@foreach($servicesList as $offer)
						<div class="t616__col t-col t-col_4 t-align_center ">
							<div class="t616__wrapper">
								<div class="t616__content" style="background-color: #f0f0f0;">
									<div class="t616__header"
										style="background-color: rgb(35, 35, 196); height: 209px;">
										
										<div class="t616__price__wrapper">
											<div class="t616__price t-title t-title_xs" field="price"
												style="color: rgb(255, 255, 255); height: 51px;">
												{{$offer['desc']}}
											</div>
											
										</div>
									</div>
									@if(count($offer['children']) > 0)
										<div class="t616__footer">
											<div class="t616__text t-descr t-descr_xs" field="descr" style="height: 156px;">
												<ul>
													@foreach($offer['children'] as $offer_child)
														<li>
															<div class="price__service">{{$offer_child['desc']}}</div>
															<div class="price__cost">{{isset($offer_child['price']) ? $offer_child['price'] : ''}}</div>
														</li>
													@endforeach  
												</ul>
											</div>
											
										</div>
									@endif
								</div>
							</div>
						</div>
					@endforeach                                
					@endif
				</div>
			</div>

		</div>
	</div>

</div>
