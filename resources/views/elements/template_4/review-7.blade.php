
<div id="page">
	<div class="record" id="record226942930" recordid="226942930" off="n" data-record-type="730"
	style="opacity: 1;">
	
	<div id="rec226942930" class="r" style=" " data-animationappear="off">

		<!-- t730 -->

		<div class="t730 t730__witharrows"  id="reviews">

			<div class="t-cover" id="recorddiv226942930" bgimgfield="img"
				style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); "
				data-tu-noclick="yes">

				<div class="t-cover__carrier" id="coverCarry226942930" data-content-cover-id="226942930"
					data-content-cover-bg="https://static.tildacdn.com/tild6165-3164-4062-a366-633066633331/nikmacmillan280300_1.jpg"
					data-content-cover-height="100vh" data-content-cover-parallax=""
					style="background-image:url('https://static.tildacdn.com/tild6165-3164-4062-a366-633066633331/nikmacmillan280300_1.jpg');height:100vh;background-attachment:scroll; ">
				</div>

				<div class="t-cover__filter"
					style="height:100vh;background-color:#000000;filter: alpha(opacity:40); KHTMLOpacity: 0.40; MozOpacity: 0.40; opacity: 0.40;">
				</div>



				<div class="t-slds" style="">
					<div class="t-slds__main t-container">
						<div class="t-slds__container">
							<div class="t-cover__wrapper t-valign_middle" data-auto-correct-mobile-width="false"
								style="height:100vh;width: 100%;">
								<div class="t730__content" data-hook-content="covercontent">

									<div class="t730__topsection">
										<div class="t-section__topwrapper t-align_center">
											<div class="t-section__title t-title t-title_xs" field="btitle">
												{{isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}
											</div>
										</div>
									</div>

									<div class="t-slds__items-wrapper  t-slds_animated-none"
										data-slider-transition="300" data-slider-with-cycle="true"
										data-slider-correct-height="true" data-auto-correct-mobile-width="false"
										data-slider-is-preview="true" data-slider-initialized="true"
										data-slider-totalslides="3" data-slider-pos="1" data-slider-curr-pos="1"
										data-slider-cycle="" data-slider-animated=""
										style="width: 6000px; height: 385px; transform: translateX(-1200px); touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">

										@if(count($reviews) > 0)

											<div class="t-slds__item t-slds__item-loaded" data-slide-index="0"
												style="width: 1200px;">
												<div class="t-slds__wrapper t-align_center t-width_8 t-margin_auto">												
													<div class="t730__text t-text t-text_lg"
														field="li_text__1477576758810" style="">
														You can't connect
														the dots looking forward, you can only connect them looking
														backwards.
													</div>
													<div class="t730__title t-name t-name_lg"
														field="li_title__1477576758810" style="">
														Alex Larkins
													</div>												

												</div>
											</div>
											@foreach ($reviews as $key=>$review)
												<div class="t-slds__item t-slds__item-loaded" data-slide-index="{{$key+1}}"
													style="width: 1200px;">
													<div class="t-slds__wrapper t-align_center t-width_8 t-margin_auto">												
														<div class="t730__text t-text t-text_lg"
															field="li_text__1477576758810" style="">
															{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}
														</div>
														<div class="t730__title t-name t-name_lg"
															field="li_title__1477576758810" style="">
															{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}
														</div>												

													</div>
												</div>
											@endforeach
											<div class="t-slds__item t-slds__item-loaded" data-slide-index="{{count($reviews)+1}}"
												style="width: 1200px;">
												<div class="t-slds__wrapper t-align_center t-width_8 t-margin_auto">												
													<div class="t730__text t-text t-text_lg"
														field="li_text__1477576758810" style="">
														You can't connect
														the dots looking forward, you can only connect them looking
														backwards.
													</div>
													<div class="t730__title t-name t-name_lg"
														field="li_title__1477576758810" style="">
														Alex Larkins
													</div>												

												</div>
											</div>
										@endif
										
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="t-slds__arrow_container  ">



						<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-left"
							data-slide-direction="left" style="height: 385px;">
							<div class="t-slds__arrow t-slds__arrow-left t-slds__arrow-withbg"
								style="width: 30px; height: 30px;background-color: rgba(255,255,255,1);">
								<div class="t-slds__arrow_body t-slds__arrow_body-left" style="width: 7px;">
									<svg style="display: block" viewBox="0 0 7.3 13"
										xmlns="http://www.w3.org/2000/svg"
										xmlns:xlink="http://www.w3.org/1999/xlink">
										<desc>Left</desc>
										<polyline fill="none" stroke="#222222" stroke-linejoin="butt"
											stroke-linecap="butt" stroke-width="1"
											points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
									</svg>
								</div>
							</div>
						</div>
						<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-right"
							data-slide-direction="right" style="height: 385px;">
							<div class="t-slds__arrow t-slds__arrow-right t-slds__arrow-withbg"
								style="width: 30px; height: 30px;background-color: rgba(255,255,255,1);">
								<div class="t-slds__arrow_body t-slds__arrow_body-right" style="width: 7px;">
									<svg style="display: block" viewBox="0 0 7.3 13"
										xmlns="http://www.w3.org/2000/svg"
										xmlns:xlink="http://www.w3.org/1999/xlink">
										<desc>Right</desc>
										<polyline fill="none" stroke="#222222" stroke-linejoin="butt"
											stroke-linecap="butt" stroke-width="1"
											points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
									</svg>
								</div>
							</div>
						</div>
					</div>
					<div class="t-slds__bullet_wrapper">

						<div class="t-slds__bullet t-slds__bullet_active" data-slide-bullet-for="1">
							<div class="t-slds__bullet_body"
								style="width: 10px; height: 10px;background-color: transparent;border: 2px solid #ffffff;">
							</div>
						</div>

						<div class="t-slds__bullet " data-slide-bullet-for="2">
							<div class="t-slds__bullet_body"
								style="width: 10px; height: 10px;background-color: transparent;border: 2px solid #ffffff;">
							</div>
						</div>

						<div class="t-slds__bullet " data-slide-bullet-for="3">
							<div class="t-slds__bullet_body"
								style="width: 10px; height: 10px;background-color: transparent;border: 2px solid #ffffff;">
							</div>
						</div>

					</div>
				</div>
			</div>
			<input type="file" class="tu-hidden-input" accept="image/*"
				style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
		</div>
		<script type="text/javascript">
			$(document).ready(function () {
				t_sldsInit('226942930');				

				t_slds_UpdateSliderHeight('226942930');
				t_slds_UpdateSliderArrowsHeight('226942930');

			}); 
		</script>

	</div>

</div>
</div>

		
