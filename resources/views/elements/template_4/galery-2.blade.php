<div id="page">

	<div class="record" id="record226918987" recordid="226918987" off="" data-record-type="169" style="opacity: 1;">

		<div id="rec226918987" class="r" style="padding-top:30px; ">

			<!-- t155 -->
			<div class="t155">
				<div class="t-container">

					<div class="t-row" id="gallery">
                    @if(count($imgList) > 0)
                        @foreach ($imgList as $key=>$img)
                            <div class="t-col t-col_6">
                                <div class="t-bgimg" bgimgfield="gi_img__{{$key}}"
                                    data-original="{{asset($img ) }}"
                                    style="background: url('{{asset($img ) }}') center center no-repeat; background-size:cover; height:660px; margin-bottom:20px;"
                                    itemscope="" itemtype="http://schema.org/ImageObject" >
                                    <meta itemprop="image"
                                        content="{{asset($img ) }}">
                                </div>
                            </div>
                        @endforeach
                    @endif
					</div>

				</div>
			</div>

		</div>

	</div>
</div>

