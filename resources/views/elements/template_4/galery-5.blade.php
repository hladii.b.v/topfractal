<div id="page">
	<div class="record" id="record226919202" recordid="226919202" off="n" data-record-type="603"
		style="opacity: 1;">

		<div id="rec226919202" class="r" style=" " data-animationappear="off">

			<!-- t603-->
			<div class="t603" id="gallery">
				<div class="t603__container t-container_100">
                    @if(count($imgList) > 0)
                        @foreach ($imgList as $key=>$img)
                            <div class="t603__tile t603__tile_25" >
                                <div class="t603__blockimg t603__blockimg_4-3 t-bgimg" bgimgfield="gi_img__{{$key}}"
                                    data-zoom-target="{{$key}}"
                                    data-original="{{asset($img ) }}"
                                    style="background: url('{{asset($img ) }}') center center no-repeat; background-size:cover;"
                                    id="tuwidget541253">
                                    <meta itemprop="image"
                                        content="{{asset($img ) }}">
                                </div>
                            </div>
                        @endforeach
                    @endif
				</div>
			</div>

		</div>
	</div>
</div>
