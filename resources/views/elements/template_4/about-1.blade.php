<div id="page">
	<div id="rec238719015" class="r t-rec" style=" " data-animationappear="off" data-record-type="492">
		<!-- T492 -->
		<div class="t492" id="about">
			
			<table class="t492__blocktable" style="" data-height-correct-vh="yes">
				<tbody>
					<tr>
						<td class="t492__blocktext">
							<div class="t492__textwrapper">
								<div class="t527__persdescr t-descr t-descr_xxs t527__bottommargin_lg" style="" field="li_persdescr__1478035709182">
									{{ isset($section_name) ? $section_name : 'Про нас'}}
								</div>
								<div class="t-title t-title_xxs" style="" field="title">
									{{ isset($aboutMotto) ? $aboutMotto : 'Про нас'}}
								</div>
								<div class="t-descr t-descr_md" style="" field="descr">
									{{ isset($aboutText) ? $aboutText : ''}}
								</div>

							</div>
						</td>
						<td class="t492__blockimg ">
							<img src="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}" alt="">
						</td>
					</tr>
				</tbody>
			</table>
		
		</div>


	</div>

</div>

