<div id="page">
	<div id="rec238622446" class="r t-rec" style=" " data-animationappear="off" data-record-type="289">
		
		<div class="t-cover" id="recorddiv238622446" bgimgfield="img"
			>

			<div class="thsn-title-bar-wrapper" 
				style="background-image:url('{{asset( $background )}}');">
			</div>
			
			<div class="t270" id="team">
				<div class="t-container">
					<div class="t-cover__wrapper t-valign_middle" style="height:100vh;">
						<div class="t270__wrapper">
							<div class="t270__title t-title t-title_xxs" field="title">
								{{ isset($teamMotto) ? $teamMotto : 'Команда'}}
							</div>
							<div class="t270__separator"></div>
							<div class="t270__descr t-descr t-descr_xs" field="descr">
								{{ isset($teamText) ? $teamText : 'Команда'}}
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>

	</div>

	<div id="rec238621221" class="r t-rec t-rec_pt_60 t-rec_pb_90 r_showed r_anim"
	>		
		<div class="t524">
			

			<div class="t524__container t-container">
				@if(count($teams) > 0)
					@foreach ($teams as $team)
						<div class="t524__col t-col t-col_4 t-align_center t524__col-mobstyle">
							<div class="t524__itemwrapper t524__itemwrapper_4">
								<div class="t524__imgwrapper t-margin_auto">
									<div class="t524__bgimg t524__img_circle t-margin_auto t-bgimg"
										bgimgfield="li_img__1477995899606"
										
										style="background-image: url('{{isset($team['foto']) ? asset($team['foto']) : ''  }}');">
									</div>
								</div>
								<div class="t524__wrappercenter">
									<div class="t524__persname t-name t-name_lg t524__bottommargin_sm" style=""
										field="li_persname__1477995899606">
										{{ isset($team['name']) ? $team['name'] : ''}}
									</div>
									<div class="t524__persdescr t-descr t-descr_xxs " style=""
										field="li_persdescr__1477996238469">
										{{ isset($team['value']) ? $team['value'] : ''}}
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
</div>