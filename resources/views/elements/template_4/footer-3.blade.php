<div id="page">


	<div class="record" id="record226941308" recordid="226941308" off="" data-record-type="344" style="opacity: 1;">
	
		<div id="rec226941308" class="r" style="padding-top:60px;padding-bottom:75px; background-color:#1f1f1f;"
			data-animationappear="off" data-bg-color="#1f1f1f">

			<!-- T344 -->

			<div class="t344" id="app">
				<div class="t-container t-align_left">
					<div class="t344__col t-col t-col_3">
						
						<div class="t344__descr t-descr t-descr_xxs" field="descr" style="color: #ffffff;">							
							@if(isset($adressText)) 
								{!!$adressText!!}                                        
							@endif  
						</div>
					</div>
					<div class="t344__col t-col t-col_3">
						
						<div class="t344__descr t-descr t-descr_xxs" field="descr2" style="color: #ffffff;">
							<ul>
								<li>{{isset($adress) ? $adress : 'м. Полтава, вул. Європейська, 110'}}</li>
								<li>{{isset($phone) ? $phone : '(0532) 63-77-59'}}</li>
								<li>{{isset($email) ? $email : 'karat.dental@gmail.com'}}</li>
							</ul>
						</div>
					</div>
					<div class="t344__floatbeaker_lr3"></div>
					<div class="t344__col t-col t-col_3">
						
						<div class="t344__descr t-descr t-descr_xxs" field="descr3" style="color: #ffffff;">
							<ul>
								<li>{{isset($chart) ? $chart : ''}}</li>
								
							</ul>
						</div>
					</div>
					<div class="t344__col t-col t-col_3">
						
						<div class="t344__descr t-descr t-descr_xxs" field="descr4" style="color: #ffffff;">
							@if($social && count($social)>0  )
								<ul>
									@foreach ($social as $key=>$soc)
										<li>											
											<a class="socials__item" href="{{$soc['value']}}" target="_blank"> 
														
												<div class="socials__round">
													<img  src="{{asset( $soc['img'] )}}">   
												</div>
												<div class="socials__caption">
													{{$soc['name']}}
												</div>
											</a> 
										</li>
									@endforeach
								</ul>
							@endif
						</div>
					</div>
					<div class="t-col t-col_12">
						<div class="t457__copyright" field="text" style="color: #ffffff;">
							<a href="https://topfractal.com/" target="_blank">topfractal.com</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
	