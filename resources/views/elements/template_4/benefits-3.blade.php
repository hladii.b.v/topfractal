<div id="page">

	<div id="rec239369741" class="r t-rec t-rec_pt_90 t-rec_pb_75 r_showed r_anim"data-record-type="223">
		<!-- T195 -->
		<div class="t195" id="advantages">
		  	<div class="t-container">
				<div class="t-col t-col_6  t195__imgsection">					
					<img class="t195__img t-img" data-tu-max-width="1200" data-tu-max-height="1200" src="{{asset( $background )}}" imgfield="img">
					<br> 
				</div>
						
			  	<div class="t-col t-col_6">
					<div class="t-section__container t-container t-rec  t-rec_mb_90">
						
							
						<div class="t-section__topwrapper t-align_center">
							<div class="t527__persdescr t-descr t-descr_xxs t527__bottommargin_lg" style="" field="li_persdescr__1478035709182">
								{{ isset($section_name) ? $section_name : 'Чому нас обирають?'}}
							</div>
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($benefitMotto) ? $benefitMotto : 'Переваги'}}
							</div>				
							<div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">
								{{ isset($benefitText) ? $benefitText : 'Переваги'}}
							</div>			
						</div>
						
					</div>
					<div class="t195__text t-text t-text_md benefits-list" style="" field="text">
						@if(count($benefitList) > 0)
	
							@foreach($benefitList as $list)
							<div class="t497__col t-col t-col_4  t497__col_first t-item benefits-list-item">
								<div class="t497__item">
									<div class="t-cell">
										<div class="img-benefit-wraper">
											<img src="{{asset($list['img'] ) }}" class="t497__img  t-img  img-benefit" imgfield="li_img__1476973323713">
										</div>
									</div>
									<div class="t497__textwrapper t-cell t-valign_top tittle-benefit-wraper">
										<div class="t497__name t-name t-name_lg  tittle-benefit" field="li_title__1476973323713" style="">
											{!! $list['desc'] !!}
										</div>										
									</div>
								</div>
							</div>
							@endforeach                                
						@endif
					</div>
			  	</div>
				
			</div>
		</div>
	</div>
</div>
	