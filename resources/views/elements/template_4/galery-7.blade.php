<div id="page">
	<div class="record" id="record226919232" recordid="226919232" off="" data-record-type="675" style="opacity: 1;">


		<div id="rec226919232" class="r" style=" background-color:#222222;" data-animationappear="off"
			data-bg-color="#222222">

			<!-- t675 -->



			<div class="t675 " style="height:90vh;"  id="gallery">
				<div class="t-slds">
					<div class="t-container_100 t-slds__main">
						<div class="t-slds__container">
							<div class="t-slds__items-wrapper t-slds_animated-none" data-slider-transition="300"
								data-slider-correct-height="true" data-auto-correct-mobile-width="false"
								data-slider-is-preview="true" data-slider-initialized="true"
								data-slider-totalslides="3" data-slider-pos="1" data-slider-curr-pos="1"
								data-slider-cycle="" data-slider-animated=""
								style="width: 9515px; height: 562px; transform: translateX(-1903px); touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                @if(count($imgList) > 0)

                                    <div class="t-slds__item t-slds__item-loaded" data-slide-index="0">
                                        <meta itemprop="image"
                                            content="{{asset($imgList[0] ) }}">
                                        <div class="t-slds__wrapper t-align_center t-slds__bgimg t-bgimg"
                                            data-original="{{asset($imgList[0] ) }}"
                                            style="height: 90vh;background-image: url('{{asset($imgList[0] ) }}');">

                                        </div>
                                    </div>

                                    @foreach ($imgList as $key=>$img)
                                        <div class="t-slds__item t-slds__item-loaded" data-slide-index="{{$key+1}}">
                                            <meta itemprop="image"
                                                content="{{asset($img ) }}">
                                            <div class="t-slds__wrapper t-align_center t-slds__bgimg t-bgimg"
                                                data-original="{{asset($img ) }}"
                                                style="height: 90vh;background-image: url('{{asset($img ) }}');">

                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="t-slds__item t-slds__item-loaded" data-slide-index="{{count($imgList)}}}">
                                        <meta itemprop="image"
                                            content="{{asset($imgList[0] ) }}">
                                        <div class="t-slds__wrapper t-align_center t-slds__bgimg t-bgimg"
                                            data-original="{{asset($imgList[0] ) }}"
                                            style="height: 90vh;background-image: url('{{asset($imgList[0] ) }}');">

                                        </div>
                                    </div>

                                @endif
							</div>
							<div class="t-slds__arrow_container">



								<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-left"
									data-slide-direction="left" style="height: 562px;">
									<div class="t-slds__arrow t-slds__arrow-left t-slds__arrow-withbg"
										style="width: 30px; height: 30px;background-color: rgba(255,255,255,1);">
										<div class="t-slds__arrow_body t-slds__arrow_body-left" style="width: 7px;">
											<svg style="display: block" viewBox="0 0 7.3 13"
												xmlns="http://www.w3.org/2000/svg"
												xmlns:xlink="http://www.w3.org/1999/xlink">
												<desc>Left</desc>
												<polyline fill="none" stroke="#000000" stroke-linejoin="butt"
													stroke-linecap="butt" stroke-width="1"
													points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
											</svg>
										</div>
									</div>
								</div>
								<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-right"
									data-slide-direction="right" style="height: 562px;">
									<div class="t-slds__arrow t-slds__arrow-right t-slds__arrow-withbg"
										style="width: 30px; height: 30px;background-color: rgba(255,255,255,1);">
										<div class="t-slds__arrow_body t-slds__arrow_body-right"
											style="width: 7px;">
											<svg style="display: block" viewBox="0 0 7.3 13"
												xmlns="http://www.w3.org/2000/svg"
												xmlns:xlink="http://www.w3.org/1999/xlink">
												<desc>Right</desc>
												<polyline fill="none" stroke="#000000" stroke-linejoin="butt"
													stroke-linecap="butt" stroke-width="1"
													points="0.5,0.5 6.5,6.5 0.5,12.5"></polyline>
											</svg>
										</div>
									</div>
								</div>
							</div>
							<div class="t-slds__bullet_wrapper">
								<div class="t-slds__bullet t-slds__bullet_active" data-slide-bullet-for="1">
									<div class="t-slds__bullet_body"
										style="width: 10px; height: 10px;background-color: #222222;border: 2px solid #ffffff;">
									</div>
								</div>
								<div class="t-slds__bullet " data-slide-bullet-for="2">
									<div class="t-slds__bullet_body"
										style="width: 10px; height: 10px;background-color: #222222;border: 2px solid #ffffff;">
									</div>
								</div>
								<div class="t-slds__bullet " data-slide-bullet-for="3">
									<div class="t-slds__bullet_body"
										style="width: 10px; height: 10px;background-color: #222222;border: 2px solid #ffffff;">
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function () {
					t_sldsInit('226919232');
					t_slds_UpdateSliderHeight('226919232');
					t_slds_UpdateSliderArrowsHeight('226919232');

				});
			</script>

		</div>


	</div>
</div>

