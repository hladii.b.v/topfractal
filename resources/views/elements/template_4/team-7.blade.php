<div id="page">


	<div class="record" id="record226940241" recordid="226940241" off="" data-record-type="902" style="opacity: 1;">

		<div id="rec226940241" class="r" style="padding-top:120px;padding-bottom:120px; background-color:#efefef;"
			data-animationappear="off" data-bg-color="#efefef">
			<!-- t902 -->

			<div class="t902" id="team">

				<div class="t-section__container t-container">
					<div class="t-col t-col_12">
						<div class="t-section__topwrapper t-align_center">
							<div class="t-section__title t-title t-title_xs" field="btitle">
								{{ isset($section_name) ? $section_name : 'Команда'}}
							</div>
						</div>
					</div>
				</div>

				<div class="t-container">

					<div class="t902__row">
						@if(count($teams) > 0)
						@foreach ($teams as $team)
							<div class="t902__col t-col t-col_6   t902__col_first">
								<div class="t902__inner-col"
									style="background-color: rgb(255, 255, 255); height: 239px;">
									<div class="t902__wrap">
										<div class="t902__wrap_left">
											<div class="t902__bgwrapper t902__bgwrapper_height" style="">
												<meta itemprop="image"
													content="{{isset($team['foto']) ? asset($team['foto']) : ''  }}">
												<div class="t902__bgimg t-bgimg" bgimgfield="li_img__1568187704544"
													data-original="{{isset($team['foto']) ? asset($team['foto']) : ''  }}"
													style="background-image: url('{{isset($team['foto']) ? asset($team['foto']) : ''  }}'); height: 239px;"
													id="tuwidget587672"></div>
												<input type="file" class="tu-hidden-input" accept="image/*"
													style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
											</div>
										</div>
										<div class="t902__wrap_right">
											<div class="t902__content">
												<div class="t-name t-name_md t902__bottommargin" style=""
													field="li_title__1568187704544" data-redactor-nohref="yes">
													{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}
													<br>
												</div>
												<div class="t-descr t-descr_xs" style="" field="li_descr__1568187704544"
													data-redactor-nohref="yes">
													{{ isset($team['value']) ? $team['value'] : ''}}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
						@endif
					
					</div>
				</div>

			</div>
		</div>

	</div>

</div>
	