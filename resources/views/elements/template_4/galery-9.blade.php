<div id="page">

	<div class="record" id="record226919270" recordid="226919270" off="" data-record-type="764" style="opacity: 1;">

		<div id="rec226919270" class="r" style="padding-top:135px;padding-bottom:135px; "
			data-animationappear="off">

			<!-- t764 -->

			<div class="t764" id="gallery">
				<div class="t-container js-product">
					<div class="t764__col t764__col_first t-col t-col_12 ">


						<div class="t-slds" style="">
							<div class="t-slds__main">
								<div class="t-slds__container">
									<div class="t-slds__items-wrapper t-slds_animated-none "
										data-slider-transition="300" data-slider-with-cycle="true"
										data-slider-correct-height="true" data-auto-correct-mobile-width="false"
										data-slider-initialized="true" data-slider-totalslides="4"
										data-slider-pos="1" data-slider-curr-pos="1" data-slider-cycle=""
										data-slider-animated=""
										style="width: 4560px; height: 570px; transform: translateX(-760px); touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                        @if(count($imgList) > 0)

                                            <div class="t-slds__item t-slds__item-loaded" data-slide-index="0"
                                                style="width: 760px;">
                                                <div class="t-slds__wrapper" >
                                                    <meta itemprop="image"
                                                        content="{{asset($imgList[0] ) }}">
                                                    <div class="t-slds__imgwrapper" bgimgfield="gi_img__0"
                                                        id="tuwidget729466">
                                                        <div class="t-slds__bgimg  t-bgimg "
                                                            data-original="{{asset($imgList[0] ) }}"
                                                            style="padding-bottom:75%; background-image: url('{{asset($imgList[0] ) }}');">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            @foreach ($imgList as $key=>$img)

                                                <div class="t-slds__item t-slds__item-loaded" data-slide-index="{{$key+1}}"
                                                    style="width: 760px;">
                                                    <div class="t-slds__wrapper" >
                                                        <meta itemprop="image"
                                                            content="{{asset($img ) }}">
                                                        <div class="t-slds__imgwrapper" bgimgfield="gi_img__{{$key+1}}">
                                                            <div class="t-slds__bgimg  t-bgimg "
                                                                data-original="{{asset($img ) }}"
                                                                style="padding-bottom:75%; background-image: url('{{asset($img ) }}');">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            @endforeach

                                            <div class="t-slds__item t-slds__item-loaded" data-slide-index="{{count($imgList)+1}}"
                                                style="width: 760px;">
                                                <div class="t-slds__wrapper" >
                                                    <meta itemprop="image"
                                                        content="{{asset($imgList[0] ) }}">
                                                    <div class="t-slds__imgwrapper" bgimgfield="gi_img__{{count($imgList)+1}}"
                                                        id="tuwidget729466">
                                                        <div class="t-slds__bgimg  t-bgimg "
                                                            data-original="{{asset($imgList[0] ) }}"
                                                            style="padding-bottom:75%; background-image: url('{{asset($imgList[0] ) }}');">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
									</div>
								</div>
							</div>
							<div class="t-slds__thumbsbullet-wrapper ">
                                @if(count($imgList) > 0)
                                @foreach ($imgList as $key=>$img)
                                    <div class="t-slds__thumbsbullet t-slds__bullet t-slds__bullet_active"
                                        data-slide-bullet-for="{{$key+1}}">
                                        <div class="t-slds__bgimg t-bgimg"
                                            data-original="{{asset($img ) }}"
                                            style="padding-bottom: 100%; background-image: url('{{asset($img ) }}');">
                                        </div>
                                        <div class="t-slds__thumbsbullet-border"></div>
                                    </div>
                                @endforeach
                                @endif
							</div>
						</div>

						<!--/gallery -->
					</div>
				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function () {
					t_sldsInit('226919270');
					t_slds_UpdateSliderHeight('226919270');
					t_slds_UpdateSliderArrowsHeight('226919270');

				});
			</script>

		</div>

	</div>
</div>
