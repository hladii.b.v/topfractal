<div id="page">

    <div id="rec183661747" class="r t-rec t-rec_pt_0 t-rec_pb_0 t-screenmin-480px"
			style="padding-top:0px;padding-bottom:0px; " data-animationappear="off" data-record-type="481"
			data-screen-min="480px">
			<!-- T481 -->
			<div id="nav183661747marker"></div>
			<div id="nav183661747" class="t481 t-col_12 t481__positionfixed "
				style="border-radius: 10px;top: 20px;background-color: rgba(255,255,255,1); height:70px; box-shadow: 0px 1px 3px rgba(0,0,0,0.10);"
				data-bgcolor-hex="#ffffff" data-bgcolor-rgba="rgba(255,255,255,1)" data-navmarker="nav183661747marker"
				data-appearoffset="" data-bgopacity-two="" data-menushadow="10" data-bgopacity="1"
				data-menu-items-align="right" data-menu="yes" data-bgcolor-setbyscript="yes">
				<div class="t481__maincontainer " style="height:70px;">
					<div class="t481__padding40px"></div>
					<div class="t481__leftside">
						<div class="t481__leftcontainer"> <a  style="color:#000000;">
                                <div class="t481__logo t-name" field="title" style="color:#000000;">  
                                    {{isset($organizationName) ? $organizationName : 'Карат'}}
                                </div>
                            </a> 
                        </div>
					</div>
					<div class="t481__centerside t481__menualign_right">
						<div class="t481__centercontainer">
							<ul class="t481__list">
                                @if(count($menu) > 0)

                                    @foreach($menu	 as $value)    
                                    @if (isset($value['name'])) 
                                        <li class="t481__list_item">
                                            <a class="t-menu__link-item" href="{{$value['link']}}"
                                            data-menu-submenu-hook="" style="color:#000000;"
                                            data-menu-item-number="1">{{$value['name']}}</a> 
                                        </li>
                                    @endif
                                    @endforeach
                                @endif
							</ul>
						</div>
					</div>
					<div class="t481__rightside">
						<div class="t481__rightcontainer">
							<div class="t481__right_buttons">
								<div class="t481__right_buttons_wrap">
									<div class="t481__right_buttons_but">
                                        <table style="width:100%; height:100%;">
											<tbody>
												<tr>
													<td>+11234567890</td>
												</tr>
											</tbody>
                                        </table>
                                    </div>
								</div>
							</div>
						</div>
					</div>
					<div class="t481__padding40px"></div>
				</div>
			</div>
			<script type="text/javascript"> $(document).ready(function () { t481_highlight(); t481_checkAnchorLinks('183661747'); });
				$(window).resize(function () {
					t481_setBg('183661747');
				});
				$(document).ready(function () {
					t481_setBg('183661747');
				}); </script>
			<script
				type="text/javascript"> $(document).ready(function () { setTimeout(function () { t_menusub_init('183661747'); }, 500); });</script>
			<style>
				@media screen and (max-width: 980px) {
					#rec183661747 .t-menusub__menu .t-menusub__link-item {
						color: #000000 !important;
					}

					#rec183661747 .t-menusub__menu .t-menusub__link-item.t-active {
						color: #000000 !important;
					}
				}
			</style>
			<!--[if IE 8]><style>#rec183661747 .t481 { filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#D9ffffff', endColorstr='#D9ffffff');
}</style><![endif]-->
		</div>

		<div id="rec188590376" class="r t-rec t-rec_pt_0 t-rec_pb_0 t-screenmax-480px"
			style="padding-top:0px;padding-bottom:0px; " data-animationappear="off" data-record-type="327"
			data-screen-max="480px">
			<!-- T282 -->
			<div id="nav188590376marker"></div>
			<div id="nav188590376" class="t282 t282__menu_static" data-menu="yes" data-appearoffset="">
				<div class="t282__container t282__small t282__positionfixed" style="">
					<div class="t282__container__bg"
						style="background-color: rgba(255,255,255,1); box-shadow: 0px 1px 3px rgba(0,0,0,0.10);"
						data-bgcolor-hex="#ffffff" data-bgcolor-rgba="rgba(255,255,255,1)"
						data-navmarker="nav188590376marker" data-appearoffset="" data-bgopacity="1"
						data-menu-shadow="0px 1px 3px rgba(0,0,0,0.10)"></div>
					<div class="t282__container__bg_opened" style="background-color:#ffffff;"></div>
					<div class="<div id=">
						<div class="t282__logo__container" style="height:60px;">
							<div class="t282__logo__content">
								<div field="title" class="t-title t-title_xs" style="color:#000000;font-size:24px;">
                                    {{isset($organizationName) ? $organizationName : 'Карат'}}
                                </div>
							</div>
							<div class="t282__burger"> <span style=";"></span> <span style=";"></span> <span
									style=";"></span> <span style=";"></span> </div>
						</div>
					</div>
				</div>
				<div class="t282__menu__container t282__closed">
					<div class="t282__menu__wrapper" style="background-color:#ffffff;">
						<div class="t282__menu">
							<div class="t282__menu__items"> 
                                @if(count($menu) > 0)

                                    @foreach($menu	 as $value)    
                                    @if (isset($value['name'])) 
                                        <a	class="t282__menu__item t-heading t-heading_md t-menu__link-item"
                                            href="{{$value['link']}}" data-menu-submenu-hook="" style="color:#000000;">
                                            {{$value['name']}}
                                        </a> 
                                    @endif
                                    @endforeach
                                @endif
						</div>
					</div>
				</div>
				<div class="t282__overlay t282__closed"></div>
			</div>
			<script
				type="text/javascript"> $(document).ready(function () { t282_showMenu('188590376'); t282_changeSize('188590376'); t282_highlight(); }); $(window).resize(function () { t282_changeSize('188590376'); }); </script>
			<script
				type="text/javascript"> $(document).ready(function () { setTimeout(function () { t_menusub_init('188590376'); }, 500); });</script>
			<style>
				@media screen and (max-width: 980px) {
					#rec188590376 .t-menusub__menu .t-menusub__link-item {
						color: #000000 !important;
					}

					#rec188590376 .t-menusub__menu .t-menusub__link-item.t-active {
						color: #000000 !important;
					}
				}
			</style>
			<!--[if IE 8]><style>#rec188590376 .t282__container__bg { filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#D9ffffff', endColorstr='#D9ffffff');
}</style><![endif]-->
		</div>

</div>