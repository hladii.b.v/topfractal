
<div id="page" class="page"> 
    

    <div class="item content" id="content_section1">

        <div class="container">
            <section class="advantages" id="advantages">

                <div class="layout__center">
                    <h2 class="title text-center editContent"><span class="title__text">
                        {{ isset($section_name) ? $section_name : 'Чому нас обирають?'}}</span></h2>
                    <div class="advantages__grid">
                        <div class="grid">
                            <div class="grid__row">
                                @if(count($benefitList) > 0)
 
                                    @foreach($benefitList as $list)
                                   
                                    <div class="grid__col-6 grid__col-md-3">
                                        <div class="advantages__item">
                                            <div class="advantages__round">
                                                <div class="advantages__inner">
                                                    <img alt="" class="advantages__icon" src="{{asset($list['img'] ) }}" sizes="50px">
                                                </div>
                                            </div>
                                            <div class="advantages__text editContent">
                                                <p>{!! $list['desc'] !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach                                
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>    
</div>

