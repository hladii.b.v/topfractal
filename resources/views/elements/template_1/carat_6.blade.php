
<div id="page" class="page">    

    <div class="item content" id="content_section1">

        <div class="container">
            <section class="gallery no-team" id="gallery"><img class="advantages__bg-fig lazyautosizes lazyloaded"
                                                               data-sizes="auto"
                                                                alt=""
                                                               src="css/images/advantages__bg-fig.png"
                                                               sizes="1663px">
                <div class="layout__center"><h2 class="title text-center"><span class="title__text editContent">
                    {{ isset($section_name) ? $section_name : 'Оцініть комфорт у нашій клініці'}}</span>
                </h2></div>
                <div class="portfolio_flex">
                @if(count($imgList) > 0)                
                   
                    @foreach ($imgList as $img)
                        <div class="col-md-3 portfolio_flex_img">
                            <img src="{{asset($img ) }}" class="img-rounded width-100" alt="" id="testImage">
                        </div>                            
                    @endforeach 
                           
               
                @endif 
            </div>
            </section> <!-- reviews -->

        </div>
    </div>
</div>

