
<div id="page" class="page">  
    

    <div class="item content" id="content_section1">
        <div class="container">
            <section class="app" id="app">
               

               <img class="app__bg-fig lazyautosizes lazyloaded" data-sizes="auto"  alt="" src="css/images/app__bg-fig.png"  sizes="1663px">
                <div class="layout__center">
                    <div class="app__title">
                        <h2 class="title text-center">
                            <span class="title__text editContent">
                                {{ isset($section_name) ? $section_name : 'Запис на прийом до лікаря'}}
                            </span>
                        </h2>
                    </div>
                    <div class="app__grid">
                        <div class="grid">
                            <div class="grid__row">
                                <div class="grid__col-lg-6">
                                    <div class="app__content contact_contetn">

                                        @if(isset($adressText))
                                            <p>{!!$adressText!!}</p>                                        
                                        @endif   

                                    </div>
                                </div>
                                <div class="grid__col-lg-6" id="app-form">
                                    <div class="app__form">
                                        <div class="app__ciround">
                                            <div class="ciround">
                                                <img class="ciround__img lazyautosizes lazyloaded"   data-sizes="auto"  alt="" src="{{asset( $background )}}"  sizes="818px">
                                                <div class="ciround__overlay"></div>
                                                <div class="ciround__circle"></div>
                                            </div>
                                        </div>
                                        <div class="app__content">
                                            <h3 class="app__list-title editContent">Нас легко знайти</h3>
                                            <div class="app__list editContent">
                                                <div class="contacts__item editContent">
                                                    <div class="contacts__round">
                                                        <img  class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto" alt="Адрес" src="css/images/location(1).svg" sizes="16px">
                                                    </div>
                                                    <div class="contacts__label editContent">Адреса:</div>
                                                    <div class="contacts__text editContent">
                                                        {{isset($adress) ? $adress : 'м. Полтава, вул. Європейська, 110'}}
                                                    </div>
                                                </div>
                                                <div class="contacts__item editContent">
                                                    <div class="contacts__round">
                                                        <img class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto" alt="График работы" src="css/images/clock.svg" sizes="16px">
                                                    </div>
                                                    <div class="contacts__label editContent">Графік роботи:</div>
                                                    <div class="contacts__text editContent">
                                                        {{isset($chart) ? $chart : ''}}
                                                    </div>
                                                </div>
                                                <div class="contacts__item editContent">
                                                    <div class="contacts__round">
                                                        <img class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto" alt="Телефон" src="css/images/call(1).svg" sizes="16px">
                                                    </div>
                                                    <div class="contacts__label editContent">Телефон:</div>
                                                    <div class="contacts__text editContent">                                               
                                                        <a href="tel:{{isset($phone) ? preg_replace('/[^0-9]/', '', $phone) : '(0532) 63-77-59'}}">
                                                            {{isset($phone) ? $phone : '(0532) 63-77-59'}}
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="contacts__item editContent">
                                                    <div class="contacts__round">
                                                        <img class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto"  alt="Электронная почта" src="css/images/mail.svg"  sizes="16px">
                                                    </div>
                                                     <div class="contacts__label editContent">Електронна пошта:</div>
                                                    <div class="contacts__text editContent">
                                                        {{isset($email) ? $email : 'karat.dental@gmail.com'}}
                                                    </div>
                                                </div>
                                                
                                                @if($social && count($social)>0  )
                                                    <div class="socials contacts__socials">
                                                        @foreach ($social as $key=>$soc)
                                                            <a class="socials__item" href="{{$soc['value']}}" target="_blank"> 
                                                                
                                                                <div class="socials__round">
                                                                    <img  src="{{asset( $soc['img'] )}}">   
                                                                </div>
                                                                <div class="socials__caption">
                                                                    {{$soc['name']}}
                                                                </div>
                                                            </a> 
                                                        @endforeach                                         
                                                    </div>
                                                @endif
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layout__center">
                        <div class="app__title">
                            <h5 class="title text-center">
                                <span class=""><a href="https://topfractal.com/" target="_blank">topfractal.com</a></span>
                            </h5>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

