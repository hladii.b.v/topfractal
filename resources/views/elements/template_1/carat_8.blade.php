
<div id="page" class="page"> 
   
    <div class="item content services_content" id="content_section1" >

        <div class="container">
            <section class="price price-alt" id="services">
                <div class="layout__center"><h2 class="title text-center"><span class="title__text editContent">
                    {{ isset($section_name) ? $section_name : 'Вартість наших послуг'}}</span>
                </h2>
                @if($pdfFile != '' ) 
                    <h3 class="price_header">
                        <a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="title__text" target="_blank">Прайс лист</a>
                    </h3>
                @endif
                    <div class="price__item  js-price">
                            @if(count($servicesList) > 0)
                           
                                @foreach($servicesList as $offer)
                                    <div class="price__item ">
                                        <div class="price__head editContent" data-target="cat-{{ $loop->index }}">
                                            <div class="price__cat editContent">{{$offer['desc']}}</div>
                                        </div>
                                        @if(count($offer['children']) > 0)
                                            <div class="price__body" id="cat-{{ $loop->index }}">
                                                @foreach($offer['children'] as $offer_child)
                                                
                                                    <div class="price__data">
                                                        <div class="price__service editContent">{{$offer_child['desc']}}</div>
                                                        <div class="price__cost editContent">{{isset($offer_child['price']) ? $offer_child['price'] : ''}}</div>
                                                    </div>
                                                
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                @endforeach                                
                            @endif
                    </div>
                </div>
            </section> <!-- app -->
        </div>
    </div>
</div>

