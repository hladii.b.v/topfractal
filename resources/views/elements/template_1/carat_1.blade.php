
<div id="page" class="page">
    <style> 
        #backtotop a, .tpan, .title__text:before, .title__text:after, .tpan__call:after, .control__inner, 
        .reviews__swiper-pagination .swiper-pagination-bullet-active, .contacts__round:before,
         .contacts__round:after, .advantages__inner, .memob__square{
            background-color: {{$color1}} !important;
        }
        .logo__title {
            color: {{$color1}} !important;
        }
        p, .tpan__item, .advantages__text, .title__text, .offer__text, .about__text, .price__cat, .reviews__name, .app__content, .app__list-title, .contacts__label, .contacts__text {
            color: {{$color2}} !important;
        }
        #backtotop a, .menu__link, .reviews__text, .price__service, .price__cost {
            color: {{$color3}} !important;
        }
    </style>  

    <div class="item content" id="content_section1">

        <div class="container">
            <header class="layout__head"> <!-- top panel -->
                <div class="tpan"> <div class="layout__center">
                    <div class="tpan__inner no-location">
                        <div class="tpan__item tpan__item_address">

                            
                            </div>
                        </div>
                        <div class="tpan__item tpan__item_tels tpan__item_text-bold">
                            <div class="editContent" >
                               
                            </div>
                        </div>
                        <div class="tpan__right">
                            <div class="tpan__call js-open-modal editContent" data-modal="callback">
                                
                            </div>
                        </div>
                    </div>
                </div>
                </div> <!-- main menu -->
                <div class="lome"> <div class="layout__center fullWidth">
                    <div class="lome__inner">
                        <div class="lome__logo ">
                            <a class="logo">
                                <div class="logo__icon-wrap">
                                    <img alt="" src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}" style="height: 100%;">

                                </div>
                                <p class="logo__title editContent">{{isset($organizationName) ? $organizationName : 'Карат'}}</p>
                                <p class="logo__text editContent">{{isset($headerMotto) ? $headerMotto : 'Карат'}}</p>
                            </a>
                        </div>
                        <div class="lome__menu">
                            <nav class="menu menu_main" id="menu">
                                <ul class="menu__list nav">
                                    @if(count($menu) > 0)

                                        @foreach($menu	 as $value)                                  
                                        <li class="menu__item editContent ">
                                            <a class="menu__link "  onclick="slrolTo('{{$value['link']}}')" >{{$value['name']}}</a>
                                        </li>
                                        @endforeach
                                    @endif 
                                </ul>
                            </nav>
                        </div>
                        <div class="lome__menu-mobile js-open-modal" data-modal="menu">
                        </div>
                    </div>
                </div>
                </div> <!-- mobile menu -->
                <div class="memob is-visible">
                    <div class="memob__square js-open-modal" data-modal="menu">
                        <a onclick="show_mob_menu()">
                            <img alt="" src=" {{asset( 'elements/images/img_523096.png')}}" >
                            </a>
                    </div>
                </div>
                <section class="offer offer_static" style="margin-top: 0px !important;">
                    <div class="offer__bgi-wrap">
                        <picture class="editContent">
                            <source srcset="{{asset( isset($header_img) ? $header_img : '/images/offer__bgi-img-1.png')}}" media="(min-width: 768px)" class="editContent">
                                <img class="offer__bgi-img" src="{{asset( isset($header_img) ? $header_img : '/images/offer__img-mob-1.png')}}" alt=""> </picture>                       
                    </div> <div class="layout__center offer__layout-center">
                        <div class="offer__content">
                            <h1 class="title title_offer">
                                <span class="title__text editContent">{{ isset($headerTitle) ? $headerTitle : 'fghdfghdf'}}</span>
                            </h1>
                            <div class="offer__words">
                                <div class="offer__text">
                                    <p class="editContent">{{ isset($headerText) ? $headerText : 'fghdfghdf'}}</p>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </header>
        </div>
        <!-- modal menu -->
        <div  id="modal-menu-hladii">
            <a class="modal__close modal__close_mob" onclick="hide_mob_menu()">X</a>
          
            <nav class="menu menu_modal">
                <ul class="menu__list">
                    @if(count($menu) > 0)

                        @foreach($menu as $value)                                  
                        <li class="menu__item editContent ">
                            <a class="menu__link " href="{{$value['link']}}" onclick="hide_mob_menu()">{{$value['name']}}</a>
                        </li>
                        @endforeach
                    @endif                    
                </ul>
            </nav>           
        </div>
        <div id="backtotop" class="visible"><a href="#"></a></div>
        <script>
            function show_mob_menu() { 
                var modal_menu = document.getElementById("modal-menu-hladii") 
                modal_menu.style.display = 'block' 
                modal_menu.style.opacity = 1 
                modal_menu.style.backgroundColor = '#252525' 
            } 

            function hide_mob_menu() { 
                var modal_menu = document.getElementById("modal-menu-hladii") 
                modal_menu.style.display = 'none'
                modal_menu.style.opacity = 0 
            } 

            var userSelection = document.getElementsByClassName('menu__link'); 

            function slrolTo (link){ 
                link = link.substring(1); 
                var scrollPosition = document.getElementById(link); 
                console.log(scrollPosition) 
                scrollPosition.scrollIntoView({
                    behavior: 'smooth', 
                    block: 'center'
                }); 
            } 
        </script>
    </div>


</div>

