
<div id="page" class="page"> 
    

    <div class="item content" id="content_section1">

        <div class="container"> 
            <section class="about" id="about">
                <div class="layout__center">
                    <h2 class="title text-center">
                        <span class="title__text editContent">{{ isset($section_name) ? $section_name : 'Про клініку'}}</span>
                    </h2>
                    <div class="about__grid">
                        <div class="grid">
                            <div class="grid__row">
                                <div class="grid__col-lg-6">
                                    <div class="about__text editContent">
                                        <p>{{ isset($aboutText) ? $aboutText : 'Одним з основних досягнень клініки є здобута довіра наших пацієнтів, оскільки, велика кількість наших відвідувачів звертається до нас за рекомендаціями своїх рідних чи друзів.
                                        Успіх завдяки нашій кваліфікованій команді лікарів-стоматологів. Беручи до уваги інтереси наших пацієнтів, ми продовжуємо вдосконалювати наші знання та професійні якості: беремо участь у національних та міжнародних конференціях, курсах підвищення кваліфікації.
                                       Наша практика пропонує унікальний спектр стоматологічних послуг, які можуть допомогти Вам і Вашим дітям вести здоровий і комфортний спосіб життя.'}}</p>
                                    </div>
                                </div>
                                <div class="grid__col-lg-6">
                                    <img alt="" src="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}" sizes="555px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
