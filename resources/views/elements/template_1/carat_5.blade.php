 
<div id="page" class="page">
    

    <div class="item content" id="content_section1"> 

        <div class="container">
            <section class="reviews reviews-alt" id="team">
                <div class="layout__center reviews-mobile"><h2 class="title text-center"><span class="title__text editContent">
                    {{ isset($section_name) ? $section_name : 'Команда'}}</span>
                </h2>
                    <div class="reviews__swiper">
                        <div class="reviews__swiper-controls">
                            <div
                                class="control control_left reviews__control reviews__control_left js-swiper-reviews-move-left"
                                tabindex="0" role="button" aria-label="Previous slide">
                                <div class="control__round">
                                    <div class="control__inner">
                                        <div class="control__icon"></div>
                                    </div>
                                </div>
                            </div>
                            <div
                                class="control control_right reviews__control reviews__control_right js-swiper-reviews-move-right"
                                tabindex="0" role="button" aria-label="Next slide">
                                <div class="control__round">
                                    <div class="control__inner">
                                        <div class="control__icon"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="reviews__swiper-pagination js-swiper-reviews-pagination swiper-pagination-clickable swiper-pagination-bullets">
                            <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0"
                                role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet"
                                                                                        tabindex="0" role="button"
                                                                                        aria-label="Go to slide 2"></span><span
                            class="swiper-pagination-bullet" tabindex="0" role="button"
                            aria-label="Go to slide 3"></span></div>
                        <div class="swiper-container js-swiper-container-reviews swiper-container-horizontal"
                            style="cursor: grab;">
                            <div class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(-1220px, 0px, 0px);">
                                @if(count($teams) > 0)
                                @foreach ($teams as $team)
                                <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
                                    style="width: 530px; margin-right: 80px;">
                                    <div class="reviews__head">
                                        <div class="reviews__img-wrap">
                                            <div class="corner corner__left-top"></div>
                                            <div class="corner corner__right-bottom"></div>
                                            <img class="reviews__img swiper-lazy swiper-lazy-loaded" alt="" src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}">
                                        
                                        </div>
                                        <div class="reviews__figcaption">
                                            <div class="reviews__name editContent" >{{ isset($team['name']) ? $team['name'] : 'fgdgs'}}</div>                                  
                                        </div>
                                    </div>
                                    <div class="reviews__words">
                                        <div class="reviews__text editContent">{{ isset($team['value']) ? $team['value'] : ''}}</div>                                
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                    </div>
                </div>
            </section> <!-- price -->

        </div>
    </div>
</div>

