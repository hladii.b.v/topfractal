
    <div id="page">      
        
        
            <div class="section-doctor-slider" id="team">
                <div class="container section-doctor-slider__container">
                    <div class="section-doctor-slider__text">
                    <h2 class="section-doctor-slider__title">{{ isset($section_name) ? $section_name : 'Команда'}}</h2>

                    </div>
                    <div class="section-doctor-slider__slider">
                        <div class="doctors-slider-banner-img js-doctors-slider-banner-img">
                            <div
                                class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-thumbs">
                                <div class="swiper-wrapper" >
                                    
                                    
                                    @if(count($teams) > 0)
                                    
                                        @foreach ($teams as $key=>$team)  
                                            <div class="swiper-slide doctors-slider-banner-img__item
                                            @if($key==0) 
                                                swiper-slide-active swiper-slide-thumb-active
                                            @elseif($key==1)
                                              swiper-slide-next
                                            @elseif( $key== (count($teams)-1) )
                                            swiper-slide-prev
                                            @endif"
                                            
                                            data-swiper-slide-index="{{$key}}">
                                                <img src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}" alt="DOCTOR">
                                            </div>
                                        @endforeach
                                    @endif
                                    
                                </div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                <div class="reviews__swiper-pagination js-swiper-reviews-pagination swiper-pagination-clickable swiper-pagination-bullets">
                                    <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span>
                                    <span class="swiper-pagination-bullet swiper-pagination-bullet-notactive" tabindex="0" role="button" aria-label="Go to slide 2"></span>
                                </div>
                            </div>
                            
                        </div>
                        <div class="doctors-slider-banner hidden-mobile js-doctors-slider-banner">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                                <div class="swiper-wrapper">
                                    
                                    @if(count($teams) > 0)
                                   
                                        @foreach ($teams as $key=>$team)  

                                            <div class="swiper-slide doctors-slider-banner__item  
                                            @if($key==0) 
                                            swiper-slide-active
                                            @elseif($key==1)
                                            swiper-slide-next
                                            @elseif( $key== (count($teams)-1) )
                                            swiper-slide-prev
                                            @endif"
                                                data-swiper-slide-index="{{$key}}" style="width: 326px; margin-right: 20px;">
                                                <div class="doctors-slider-banner__img">
                                                    <img src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}" alt="DOCTOR"
                                                        title="">
                                                </div>
                                                <div class="doctors-slider-banner__inner">
                                                    <div class="doctors-slider-banner__info">
                                                        <a class="doctors-slider-banner__surname">{{isset($team['name']) ? $team['name'] : ''  }}</a>                                                            
                                                        <div class="doctors-slider-banner__position">
                                                            {{isset($team['value']) ? $team['value'] : ''  }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        @endforeach
                                    @endif
                                    
                                </div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                <div class="reviews__swiper-pagination js-swiper-reviews-pagination swiper-pagination-clickable swiper-pagination-bullets">
                                    <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span>
                                    <span class="swiper-pagination-bullet swiper-pagination-bullet-notactive" tabindex="0" role="button" aria-label="Go to slide 2"></span>
                                </div>
                            </div>
                            
                            <button class="swiper-arrow swiper-arrow-prev btn-arrow btn-arrow_prev" tabindex="0"
                                role="button" aria-label="Previous slide">
                                preview
                            </button>

                            <button class="swiper-arrow swiper-arrow-next btn-arrow btn-arrow_next" tabindex="0"
                                role="button" aria-label="Next slide">
                                &#8250;
                                
                            </button>
                        </div>
                        <div class="doctors-slider-banner visible-mobile js-doctors-slider-banner-mobile">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                                <div class="swiper-wrapper" style="transition-duration: 0ms;">
                                    
                                    @if(count($teams) > 0)
                                   
                                        @foreach ($teams as $key=>$team)                                                  

                                            <div class="swiper-slide doctors-slider-banner__item">
                                                <div class="doctors-slider-banner__img">
                                                    <img src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}" alt="DOCTOR">
                                                    
                                                </div>
                                                <div class="doctors-slider-banner__inner">
                                                    <div class="doctors-slider-banner__info">
                                                        <a class="doctors-slider-banner__surname">{{isset($team['name']) ? $team['name'] : ''  }}</a> 
                                                                                                                
                                                        <div class="doctors-slider-banner__position">
                                                            {{isset($team['value']) ? $team['value'] : ''  }}
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        @endforeach
                                    @endif

                                </div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                <div class="reviews__swiper-pagination js-swiper-reviews-pagination swiper-pagination-clickable swiper-pagination-bullets">
                                    <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span>
                                    <span class="swiper-pagination-bullet swiper-pagination-bullet-notactive" tabindex="0" role="button" aria-label="Go to slide 2"></span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>                 
    </div>
