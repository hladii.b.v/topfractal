
    <div id="page">
       
        <div id="app"></div>
        @if(isset($adressText))
        <div class="container container_lg">
            <div class="text-line">
                <span class="text-line__line"></span>
                <span class="text-line__text">
                    {!!$adressText!!}
                </span>
                <span class="text-line__line"></span>
            </div>
        </div>   
        @endif 
        <footer id="pagefooter" class="footer">
            
            <div class="footer-main">
                <div class="footer-main__container container">
                    <div class="footer-main__contacts">
                        <div class="footer-main__contacts-col footer-main__contacts-col_lg footer-main__contact">
                            {{isset($address) ? $address : 'м. Полтава, вул. Європейська, 110'}}
                        </div>
                        <div class="footer-main__contacts-col">
                            <span data-class="call_phone_1">
                                <a class="callphone footer-main__contact nowrap noevent" href=" tel:{{isset($phone) ?  $phone : '(0532) 63-77-59' }}">
                                     {{isset($phone) ? $phone : '(0532) 63-77-59'}}
                                </a>
                            </span><br>                            
                        </div>
                        <div class="footer-main__contacts-col">
                            <a href="mailto:{{isset($email) ? $email : 'info@medi.msk.ru'}}" class="footer-main__contact nowrap">{{isset($email) ? $email : 'karat.dental@gmail.com'}}</a>
                        </div>
                        <div class="footer-main__contacts-col footer-main__contacts-col_lg">
                            <p>{{isset($chart) ? $chart : ''}}</p>                            
                        </div>
                    </div>
                    <div class="footer-main__cols">
                        
                        @if(count($social)>0)
                            <div class="footer-main__email">
                                
                                <div class="social footer-main__social">
                                    @foreach ($social as $key=>$soc)
                                        <a href="{{$soc['value']}}" class="social__link" target="_blank"
                                            rel="nofollow">
                                            <img src="{{asset( $soc['img'] )}}" alt="">
                                            {{$soc['name']}}
                                        </a> 
                                       
                                                                               
                                    @endforeach                                     
                                    
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="footer-main__copyright">     
                        <a  class="developer" href="https://topfractal.com/" target="_blank">topfractal.com</a>                 
                       
                    </div>
                </div>
            </div>
        </footer>
    </div>
