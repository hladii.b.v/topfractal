
    <div id="page">
       
        <header class="page-inner-header page-inner-header_img is-animate" data-animate-load="" id="about">
            <div class="container">
                <div class="page-inner-header__container">
                    
                    <div class="page-inner-header__bottom animate-line-wrap">
                        <!--div class="tags animate-line">
                            <div class="tag tag_div_st">Стоматология</div>
                        </div-->
                        <h1 class="page-inner-header__h1 animate-line is-animate-line">
                            <div class="h2" style="animation-delay: 0.75s;">{{ isset($section_name) ? $section_name : 'Про клініку'}}
                            </div>
                        </h1>                       
                        <div class="page-inner-header__description animate-line is-animate-line">
                            <div style="animation-delay: 1s;">
                                <p>{{ isset($aboutText) ? $aboutText : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo.'}}</p>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="page-inner-header__img">
                    <img src="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}" alt="">
                </div>
            </div>
        </header>
         
       
    </div>
