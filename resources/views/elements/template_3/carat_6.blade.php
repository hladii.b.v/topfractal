
    <div id="page">
        <style>
        
        </style>
        
        <div class="section-gallery" id="gallery">
            <div class="container container_lg gallery-large">
                <button class="swiper-arrow swiper-arrow-prev btn-arrow btn-arrow_prev" tabindex="0" role="button"
                    aria-label="Previous slide">
                    &#8250;                   
                </button>
                <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                    <div class="swiper-wrapper"
                        style="transform: translate3d(-1347px, 0px, 0px); transition-duration: 0ms;">
                        @if(count($imgList) > 0)                 
            
                        @foreach ($imgList as $key=>$img)
                                <div class="swiper-slide gallery-large__item
                                @if($key==0) 
                                swiper-slide-active
                                @elseif($key==1)
                                swiper-slide-next
                                @elseif( $key== (count($imgList)-1) )
                                swiper-slide-prev
                                @endif"
                                data-swiper-slide-index="{{$key}}" style="width: 1331px; margin-right: 16px;">
                                    <img src="{{asset($img ) }}" alt="">
                                </div>
                            @endforeach                                    
                    
                        @endif
                        
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <button class="swiper-arrow swiper-arrow-next btn-arrow btn-arrow_next" tabindex="0" role="button"
                    aria-label="Next slide">
                    &#8250;                   
                </button>
            </div>
            <div class="gallery-large-thumbs">
                <div
                    class="swiper-container swiper-container-thumbs swiper-container-initialized swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper"
                        style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">                                

                        @if(count($imgList) > 0)                    
            
                            @foreach ($imgList as $key=>$img)
                                <div class="swiper-slide gallery-large-thumbs__item swiper-slide-visible
                                @if($key==0) 
                                swiper-slide-active swiper-slide-thumb-active
                                @elseif($key==1)
                                swiper-slide-next
                                @endif"
                                style="width: 222.143px; margin-right: 18px;">
                                    <img src="{{asset($img ) }}" alt="">
                                </div>
                                
                            @endforeach                                    
                    
                        @endif
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>       
        
    </div>

 
