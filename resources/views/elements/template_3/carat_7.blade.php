    <div id="page">
 
        <div id="reviews"></div>
        <div class="page-inner-content container" style="visibility: visible;">
            <div class="feedback-title">
                <h3 class="feedback-title__title">{{ isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}</h3>
            </div>


            <div class="tabs tabs_noborder tabs_otzyvy">
                
                <div id="tab_1" class="tab" style="">
                    <section class="reviews" id="msglist">
                        <div id="feedbackContainer">

                            @if(count($reviews) > 0)                
                   
                                @foreach ($reviews as $review)
                                    <div class="fdbk_items">
                                        <div class="fdbk_item first_item" id="fdbk101579">
                                            
                                            <div class="fdbk_dialog">
                                                <div class="fdbk_rtext">
                                                    <p>{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}</p>
                                                    <div class="fdbk_rmsg_author">
                                                        <span class="fdbk_rname">{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}</span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                @endforeach                          
               
                            @endif
                            
                        </div>
                        
                    </section>
                </div>
                
            </div>
        </div>
           
        
    </div>
