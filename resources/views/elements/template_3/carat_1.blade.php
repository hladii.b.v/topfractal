
    <div id="page"  class="page">


        <style>
            .footer-main, .section-gallery::before, .section-doctor-slider::before, .main-slider-wrap .slick-dots::before,
            .main-slider-wrap .slick-dots::after, .main-slider-wrap .slick-dots__load, *::selection, .tab-nav__btn::after,
            .header-main__btn-menu::before, .header-main__btn-menu::after, .header-main__btn-menu span, .toggle, .main-banner
            .section-blue, .about-single::before, .page-inner-header_img, .page-inner-header_img::before, .text-line__line, .text-line__line::after,
            .main-slider-wrap .slick-dots__btn::before, .section-doctor-slider__slider::before {                 
                background: {{$color1}} !important;         
            }
            .main-banner__img, #backtotop a  {
                background-color: {{$color1}};      
            }
             span.swiper-pagination-bullet.swiper-pagination-bullet-active {
                background-color: {{$color2}}; 
            }
            .fdbk_rname, .banner__text, .header-main__phone, .feedback-title__title, .gallery-large .btn-arrow, .doctors-slider-banner .btn-arrow, .main-banner__h1, .advantages-ico__title, .menu-catalog__link, .h2, .toggle__btn, .section-doctor-slider__title, .footer-main__contacts-col,
             .footer-main__contact, .footer-main__contacts-col.footer-main__contacts-col_lg, .footer-main__contacts-col.footer-main__contacts-col_lg p{
                color: {{$color2}} !important;
            }
            #backtotop a, .text-line__text,   .text-col__item, .price__service, .price__cost, .doctors-slider-banner__surname, .doctors-slider-banner__position,
            .socials__caption, p, .text p, .tpan__item, .advantages__text, .title__text, .offer__text, .about__text, .price__cat, .reviews__name, .app__content, .app__list-title, .contacts__label, .contacts__text, .price__service, .price__cost, .testimonials-section .text, .contact-box a, .contact-box p, .contact-box li, .section-title p
            .social__link,  .social__link, .developer {
                color: {{$color3}} !important;            
            }
            .swiper-pagination-bullet-notactive{
                background-color: {{$color3}};
            }
        </style>    
        <header class="header">
            <div class="header-top">
            </div>
            <div class="header-main header-scroll">
                <div class="container container_lg header-main__container">
                    <div class="header-main__left">
                        <button class="header-main__btn-menu js-menu-open"><span></span></button>
                        
                            <img src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}" alt="LOGO" class="header-main__logo-img">
                            <img src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}" alt="LOGO" class="header-main__logo-scroll">
                            <span class="header-main__phone ">{{isset($organizationName) ? $organizationName : 'Карат'}}</span>
                    </div>
                    <nav class="header-main__nav nav-main" id="main_nav">                        
                      
                        <ul class="menu-catalog nav-main__catalog">
                            @if(count($menu) > 0)

                                @foreach($menu as $value)                              
                                    <li aria-haspopup="true" class="menu-catalog__item "><a
                                        class="menu-catalog__link " aria-haspopup="true"
                                        href="{{$value['link']}}" onclick="hideMenu('{{$value['link']}}')">{{$value['name']}}</a>                               
                                    </li>                               
                                @endforeach
                            @endif                            
                        </ul>
                    </nav>      
                </div>
            </div>
        </header>
        
            
        <div class="main-slider-wrap">
            <div class="main-slider-wrap__btn"></div>
            <div class="main-slider slick-initialized slick-slider slick-dotted">

                <div class="slick-list draggable">
                    <div class="slick-track" style="opacity: 1; width: width:100%">
                        <div class="main-slider__item slick-slide slick-current slick-active" data-slick-index="0"
                            aria-hidden="false"
                            style="width:100%; position: relative; left: 0px; top: 0px; z-index: 999; opacity: 1;"
                            tabindex="0" role="tabpanel" id="slick-slide00"
                            aria-describedby="slick-slide-control00">
                            <div class="main-banner">
                                <div class="main-banner__item">
                                    <div class="main-banner__img">
                                        <img src="{{asset( isset($header_img) ? $header_img : '/images/offer__bgi-img-1.png')}}" alt="">
                                    </div>
                                    <div class="main-banner__container container container_lg">
                                        <div class="main-banner__description">
                                            <div class="main-banner__h1">{{ isset($headerTitle) ? $headerTitle : 'fghdfghdf'}}</div>
                                            <p class="main-banner__text">{{isset($headerMotto) ? $headerMotto : 'Карат'}}</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="container container_lg main-slider-wrap__dots">
                <ul class="slick-dots" role="tablist">
                    <li class="slick-active" role="presentation"><button class="slick-dots__btn" type="button"
                            role="tab" id="slick-slide-control00" aria-controls="slick-slide00" aria-label="1 of 1"
                            tabindex="0" aria-selected="true">0</button>
                        <span class="slick-dots__load"
                            style="transition: all 2.5s linear 1s; width: calc(100% + 10px);"></span></li>
                </ul>
            </div>
        </div>
           
        
        <div id="backtotop" class="visible"><a href="#"></a></div>
    </div>
