    <div id="page">
       
        <div class="section section-blue">
            <div class="container">    
                <div
                    class="advantages-ico advantages-ico-slider swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="benefits">
                        @if(count($benefitList) > 0)

                            @foreach($benefitList as $list)                                    
                                
                                <div class="swiper-slide swiper-slide-active" style="width: 343px; margin-right: 24px;">
                                    <div class="advantages-ico__item">
                                        <div class="advantages-ico__ico">
                                            <img src="{{asset($list['img'] ) }}" alt="">
                                        </div>
                                        <div class="advantages-ico__title">{!! $list['desc'] !!}</div>
                                    </div>
                                </div>
                            @endforeach
                        @endif	 
                                                        
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                    <div class="reviews__swiper-pagination js-swiper-reviews-pagination swiper-pagination-clickable swiper-pagination-bullets">
                        <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span>
                        <span class="swiper-pagination-bullet swiper-pagination-bullet-notactive" tabindex="0" role="button" aria-label="Go to slide 2"></span>
                    </div>
                </div>
            </div>
        </div>           
        
    </div>

   