
    <div id="page">    
        
        <div class="page-inner-content container" id="clinic_tabdata" data-easytabs="true"
            style="visibility: visible;" >
            <ul class="tab-nav ym-clickmap-ignore" data-tab-nav="clinic" id="services">
                <li class="tab-nav__item tab_services ym-clickmap-ignore active">
                    <a href="/stomatologiya/kliniki/pokrovskiy#uslugi" class="tab-nav__btn js-tab-btn active">
                        <h2 class="h2">
                            {{ isset($services_title) ? $services_title : 'Вартість наших послуг'}}</h2>
                    </a>
                </li>
                @if($pdfFile != '' )
                    <li class="tab-nav__item tab_services ym-clickmap-ignore price_pdf" >
                        <a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="tab-nav__btn js-tab-btn active" target="_blank">
                            <h2 class="h2">Прайс лист</h2>
                        </a>
                    </li>
                @endif
            </ul>

            <div class="content-aside tab-wrap" data-tab-wrap="clinic">
                <div class="content-aside__main" >
                    <div id="uslugi" class="tab-content active" data-tab-content="" style="">
                        <div class="toggle-list">
                            
                            @if(count($servicesList) > 0)
                           
                            @foreach($servicesList as $offer)

                            <div class="toggle first_item"> <button class="toggle__btn">{{$offer['desc']}}</button>
                                @if(count($offer['children']) > 0)
                                    <div class="toggle__content">
                                        <ul class="ul ul-cols">
                                            @foreach($offer['children'] as $offer_child)
                                                <li>
                                                    <div class="price__service editContent">{{$offer_child['desc']}}</div>
                                                    <div class="price__cost editContent">{{isset($offer_child['price']) ? $offer_child['price'] : ''}}</div>
                                                </li>   
                                            @endforeach                                                 
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            @endforeach
                        @endif
                            

                        </div>
                    </div>
                    <!--div id="kontakty" class="tab-content" data-tab-content>
                    <p>Контакты</p>
                    </div-->
                </div>

            </div>

        </div>
          
        
    </div>

  