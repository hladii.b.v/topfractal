<div id="page" class="page">
    

    <div class="item content" id="content_section1">

        <section class="faqs-section news-section sec-pad" id="services">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 faqs-colmun">
                        <div class="faqs-content">
                            <div class="feature-title centered">                      
                                <h2>  {{ isset($section_name) ? $section_name : 'Вартість наших послуг'}} </h2>                       
                            </div>
                            <div class="accordion-box">
                                @if(count($servicesList) > 0)
                           
                                    @foreach($servicesList as $offer)
                                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                                            <div class="acc-btn ">
                                                <h5>{{$offer['desc']}}</h5>                                            
                                            </div>
                                        
                                            @if(count($offer['children']) > 0)
                                            <div class="acc-content">
                                            @foreach($offer['children'] as $offer_child)
                                                
                                                    <div class="price__data" >
                                                        <div class="price__service editContent">{{$offer_child['desc']}}</div>
                                                        <div class="price__cost editContent">{{isset($offer_child['price']) ? $offer_child['price'] : ''}}</div>
                                                    </div>
                                            
                                                @endforeach
                                            </div>
                                            @endif
                                            
                                        </div>
                                    @endforeach
                                @endif
                                
                            </div>
                            <div class="text">
                                @if($pdfFile != '' )
                                    <h3 class="price_header">
                                        <a href="{{asset( isset($pdfFile) ? $pdfFile : '')}}" class="title__text" target="_blank">Прайс лист</a>
                                    </h3>
                                @endif
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
<!-- main-slider end -->
    </div>
</div>
