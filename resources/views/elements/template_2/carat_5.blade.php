
<div id="page" class="page">
   

    <div class="item content" id="content_section1">

        <section class="team-section sec-pad" id="team">
            <div class="container">
                <div class="feature-title centered section-title ">                      
                    <h2>  {{ isset($section_name) ? $section_name : 'Команда'}}</h2>                      
                </div>              
                <div class="row flexClassTeam">
                    @if(count($teams) > 0)
                        @foreach ($teams as $team)                            
                            <div class="col-md-3 col-sm-6 col-xs-12 team-colmun">
                                <div class="single-item">
                                    <div class="img-box">
                                            <figure><img src="{{isset($team['foto']) ? asset($team['foto']) : ''  }}" alt=""></figure>  
                                    </div>
                                    <div class="team-content">
                                        <h5> {{ isset($team['name']) ? $team['name'] : 'fgdgs'}}</h5>                                        
                                        <div class="text">
                                            <p>{{ isset($team['value']) ? $team['value'] : ''}}</p>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
<!-- main-slider end -->
    </div>
</div>
