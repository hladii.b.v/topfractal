<div id="page" class="page">
    

    <div class="item content" id="content_section1">
    
        <section id="contact" class="contact-area ptb-100">
		    <div class="container"  id="app">
                <div class="section-title">
                    <h2> {{ isset($section_name) ? $section_name : 'Запис на прийом до лікаря'}}</h2>                    
                    @if(isset($adressText))
                        <p>{!!$adressText!!}</p>
                    @endif
                </div>
                
		        <div class="">
		            <div class="col-sm-12 col-lg-3 col-md-6">
		                <div class="contact-box">
                            <h3><i class="icofont-google-map"></i>Адреса</h3>
                            <p>{{isset($address) ? $address : 'м. Полтава, вул. Європейська, 110'}}</p>
		                </div>
		            </div>
		            
		            <div class="col-sm-12 col-lg-3 col-md-6">
		                <div class="contact-box">
		                    <h3><i class="icofont-envelope"></i> Email</h3>
		                    <p>{{isset($email) ? $email : 'karat.dental@gmail.com'}}</p>
		                </div>
		            </div>
		            
                    <div class="col-sm-12 col-lg-3 col-md-6">
		                <div class="contact-box">
		                    <h3><i class="icofont-phone"></i> Номер</h3>
		                    <p><a href=" tel:{{isset($phone) ?  $phone : '(0532) 63-77-59' }}"> {{isset($phone) ? $phone : '(0532) 63-77-59'}}</a></p>
		                </div>
		            </div>
		            
		            <div class="col-sm-12 col-lg-3 col-md-6">
		                <div class="contact-box">
		                    <h3><i class="icofont-clock-time"></i>Графік</h3>
		                    <ul>
                                <li> {{isset($chart) ? $chart : ''}}</li>
		                    </ul>
		                </div>
		            </div> 
                </div>
                <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12 footer-colmun">
                        <div class="subscribe-widget footer-widget">
                            
                            @if(count($social)>0)
                                <div class="socials contacts__socials">
                                    @foreach ($social as $key=>$soc)
                                        <a class="socials__item" href="{{$soc['value']}}" target="_blank"> 
                                            <div class="socials__round">
                                                <img  src="{{asset( $soc['img'] )}}">   
                                            </div>
                                            <div class="socials__caption">{{$soc['name']}}</div>
                                           
                                            
                                        </a> 
                                    @endforeach                                         
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                
            </div>
            
        </section>
        <section class="bacgroundColor">
            <div class="devLing">  
                <a class="socials__item" href="https://topfractal.com/" target="_blank">topfractal.com</a>                                     
            </div> 
        </section>
<!-- main-slider end -->
    </div>
</div>
