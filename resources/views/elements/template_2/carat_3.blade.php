
<div id="page" class="page">
    

    <div class="item content" id="content_section1">       

        <section class="boxes-area ptb-100" id="advantages">
		    <div class="container">
		        <div class="row">
                    @if(count($benefitList) > 0)

                        @foreach($benefitList as $list)                                    
                            <div class="col-lg-3 col-md-6">
                                <div class="single-box">
                                        <img alt="" class="advantages__icon" src="{{asset($list['img'] ) }}" sizes="50px">                                        
                                        <h3>{!! $list['desc'] !!}</h3>
                                </div>
                            </div>
                        @endforeach
                    @endif	            
                    
		        </div>
		    </div>
		</section>
<!-- main-slider end -->
    </div>
</div>


