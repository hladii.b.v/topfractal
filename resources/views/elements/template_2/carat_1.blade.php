<div id="page" class="page">
    <style>
        .bacgroundColor, #backtotop a, .theme_menu,.single-box::before, .overlay-item .overlay-box:before,.contact-box::before, .accordion-box .accordion .acc-btn.active h5{
            background-color: {{$color1}} !important;     
            background: {{$color1}} !important;         
        }         
        .testimonials-section .single-item:hover .text,  .testimonials-section .single-item:hover .text:after {
            border: 2px solid {{$color1}} !important;
        }
        .overlay-item .inner-box .overlay-inner .content a, .mobile-menu, .mobile-menu > li > ul, .mobile-menu > li > ul > li > ul {
            background: {{$color1}};      
        }
        .main-slider .banner-title,.testimonials-section .text:before, .overlay-item:hover .lightbox-image, .overlay-item:hover .lightbox-image, .overlay-item .inner-box .overlay-inner .content a:hover {
            color: {{$color1}} !important;
        }
        #backtotop a, .main-menu .navigation > li:hover > a, .main-menu .navigation > li.current > a.stricky-fixed .main-menu .navigation > li > a, h1, h2, h3, h4, h5, h6, .single-box h3, .single-box i, .single-box p{
            color: {{$color2}} !important;
        }
        .socials__caption, p, .text p, .tpan__item, .advantages__text, .title__text, .offer__text, .about__text, .price__cat, .reviews__name, .app__content, .app__list-title, .contacts__label, .contacts__text,
        .price__service,  .price__cost, .testimonials-section .text, .contact-box a, .contact-box p, .contact-box li, .section-title p {
            color: {{$color3}} !important;            
        }
        .overlay-item:hover a.lightbox-image {
            background-color:  #ffffff !important; 
            background:  #ffffff !important; 
        }
    </style>

    <div class="item content" id="content_section1">
        <div class="">
            <header class="main-header">

                <!-- header upper -->
                <div class="header-middile">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="logo-box">
                                    <a href=""><img src="{{asset( isset($header_logo) ? $header_logo : '/images/icons/brush.svg')}}"
                                            alt="" style="height: 75px;
                                            width: auto;"></a>
                                    <div class="contact-info-two">
                                        <h3>{{isset($organizationName) ? $organizationName : 'Карат'}}</h3>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                </div>

                <!-- header lower -->
                <div class="header-lower theme_menu stricky">
                    <div class="container">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="menu-bar">
                                    <nav class="main-menu">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                                data-target=".navbar-collapse">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <div class="navbar-collapse collapse clearfix">
                                            <ul class="navigation clearfix">
                                                @if(count($menu) > 0)

                                                    @foreach($menu as $value)                              
                                                    
                                                    <li><a onclick="slrolTo('{{$value['link']}}')">{{$value['name']}}</a>
                                                    </li>
                                                    @endforeach
                                                @endif 
                                                
                                                
                                            </ul>

                                            <!-- mobile menu -->
                                            <ul class="mobile-menu clearfix">
                                                @if(count($menu) > 0)

                                                @foreach($menu as $value)                              
                                                
                                                <li><a href="{{$value['link']}}">{{$value['name']}}</a>
                                                </li>
                                                @endforeach
                                            @endif 
                                            </ul>
                                        </div>
                                    </nav>
                                    <div class="info-box">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <section class="main-slider">            
                        
            <div class="owl-item" >
                <div class="slide" style="background-image:url({{asset( isset($header_img) ? $header_img : '/images/offer__bgi-img-1.png')}})">
                    <div class="hider-style">
                        <div class="">
                            <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6" >
                                <div class="content">
                                    <div class="banner-title">{{ isset($headerTitle) ? $headerTitle : 'fghdfghdf'}}</div>
                                    <div class="banner-title">{{isset($headerMotto) ? $headerMotto : 'Карат'}}</div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>                   
                   
        </section>
        <div id="backtotop" class="visible"><a href="#"></a></div>
<!-- main-slider end -->
    </div>
</div>
