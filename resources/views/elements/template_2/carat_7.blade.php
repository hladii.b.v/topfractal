<div id="page" class="page">
   

    <div class="item content" id="content_section1">

        <section class="testimonials-section sec-pad" id="reviews">
            <div class="container">
                <div class="feature-title centered section-title">                      
                    <h2>{{ isset($section_name) ? $section_name : 'Наші пацієнти рекомендуют нас'}}</h2>
                    
                </div>
                <div class="displayFlex">  
                    @if(count($reviews) > 0)                
                   
                        @foreach ($reviews as $review)
                            <div class="single-item">
                                <div class="text">{{ isset($review['value']) ? $review['value'] : 'fgdgs2233'}}</div>
                                <div class="author-info">
                                    
                                    <div class="author-thumb">
                                        <h5>{{ isset($review['name']) ? $review['name'] : 'fgdgs'}}</h5>                                
                                    </div>
                                </div>
                            </div>

                        @endforeach                        
               
                    @endif
                </div>
            </div>
        </section>
<!-- main-slider end -->
    </div>
</div>
