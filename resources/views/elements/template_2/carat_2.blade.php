
<div id="page" class="page">
   

    <div class="item content" id="content_section1">
        <section class="catagories-section"  id="about">
            <div class="container">
                <div class="row margin-bottom-75">
                    <div class="feature-title centered">
                    
                        <h2>{{ isset($section_name) ? $section_name : 'Про клініку'}}</h2>
                   
                    </div>
                </div>
                
                <div class="row flexClass">
                    <div class="col-md-3 col-sm-12 col-xs-12 catagories-colmun">
                        <div class="left-colmun text-right">
                            <div class="single-item">                                
                                <div class="text">
                                    <p>{{ isset($aboutText) ? $aboutText : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo.'}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 catagories-colmun flexClassAbout">
                        <div class="img-holder wow zoomIn animated" style="visibility: hidden; animation-name: none;">
                            <div class="img-box centered">
                                <img src="{{asset( isset($about) ? $about : 'elements/images/about/img_6920-1.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 catagories-colmun">
                        <div class="right-colmun">
                            <div class="single-item">                               
                                <div class="text">
                                    <p>{{ isset($aboutText) ? $aboutText : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo.'}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!-- main-slider end -->
    </div>
</div>
