<div id="page" class="page">
   
    <div class="item content" id="content_section1">
        <section class="page-title" style="background-image: url('https://topfractal.com/elements/images/about/2/semejnaya-stomatologiya-2.jpg'});" id="gallery">
            <div class="container">
                <div class="content-box">
                    <div class="title"><h2> {{ isset($section_name) ? $section_name : 'Оцініть комфорт у нашій клініці'}}</h2></div>
                    
                </div>
            </div>
        </section>
        <section class="img-gallery gallery-page" >
            <div class="container">
                <div class="row">
                    <div class="sortable-masonry">
                        <div class="row items-container clearfix" style="position: relative; height: 464.438px;">
                            @if(count($imgList) > 0)                
                   
                                @foreach ($imgList as $img)
                                    
                                    <div class="col-md-4 col-sm-6 col-xs-12 masonry-item default-portfolio-item all dentistry cleaning calal" style="position: absolute; left: 0px; top: 0px;">
                                        <div class="single-item overlay-item">
                                            <div class="inner-box img-box">
                                                <div class="image-box">
                                                    <figure class="image"><img src="{{asset($img ) }}" alt=""></figure>
                                                    <div class="overlay-box">
                                                        <div class="overlay-inner">
                                                            <div class="content">
                                                                <a href="{{asset($img ) }}" class="lightbox-image"><i class="fa fa-search"></i></a>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach 
                                    
                            
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!-- main-slider end -->
    </div>
</div>


