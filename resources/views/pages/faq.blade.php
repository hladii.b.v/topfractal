@extends('layouts.main')

@section('content')

<!-- Content
================================================== -->


<section class="fullwidth padding-top-20" data-background-color="#fff">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <!-- Headline -->

                <h4 class="headline margin-top-40 margin-bottom-25">{{ Lang::get('messages.faq') }}</h4>
                <div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="toggle-wrap style-2  ">
                                <span class="trigger opened active">
                                    <a href="#">FAQ 1<i class="sl sl-icon-plus"></i></a>
                                </span>
                                <div class="toggle-container" style="display: block;">
                                    <p>Vivamus justo arcu, elementum a sollicitudin pellentesque, tincidunt ac enim. 
                                        Proin id arcu ut ipsum vestibulum elementum.
                                    </p>
                
                                </div>
                            </div>
                
                            <div class="toggle-wrap style-2  ">
                                <span class="trigger">
                                    <a href="#">FAQ 2<i class="sl sl-icon-plus"></i></a>
                                </span>
                                <div class="toggle-container" style="display: none;">
                                    <p>
                                        Seded ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, 
                                        totam rem aperiam. Donec ut volutpat metus. Aliquam tortor lorem, fringilla tempor dignissim at, pretium et arcu.
                                    </p>
               
                                </div>
                            </div>
                
                            <div class="toggle-wrap style-2  ">
                                <span class="trigger ">
                                    <a href="#"><i class="sl sl-icon-pin"></i>Video<i class="sl sl-icon-plus"></i></a>
                                </span>
                                <div class="toggle-container" style="display: none;     text-align: center;">
                                    <iframe width="420" height="315"
                                    src="https://www.youtube.com/embed/tgbNymZ7vqY">
                                    </iframe>
                
                                </div>
                            </div>
                
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Row / End -->

    </div>
</section>
<!-- Info Section -->

<!-- Info Section / End -->



@endsection
