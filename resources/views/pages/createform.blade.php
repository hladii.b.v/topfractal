@extends('layouts.main')

@section('content')


	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />


      <div class="dashboard-content" style="margin-left:0px !important">
		<div class="overlay">
			<div class="overlay_loader lds-ring"><div></div><div></div><div></div><div></div></div>
		</div>


            <!-- Headline -->
            <div class="add-listing-headline">
                <h3><i class="sl sl-icon-book-open"></i>{{ Lang::get('messages.Make_your_site') }}</h3>
                <!-- Switcher -->
            </div>

            <form id="main_form" action="{{route('site-create-form', ['locale' => app()->getLocale(), 'template' => $template->id])}}" method="POST" enctype="multipart/form-data">
		
                @csrf
                <div class="row ">
                    <input type="hidden" name="template_id" id="template_id" value="{{$template->id}}">
                    <input type="hidden" name="locale" id="locale" value="{{app()->getLocale()}}">
                    <div class="col-lg-8 col-md-offset-2">

						<div class="tab">
							{{-- {{ Lang::get('messages.Blocks') }}: --}}
							<div class="box1">
								<div class="notification warning closeable" id="checkbox-area">
									<a id="main_block_content" class="showMore" data-id="0"><i class="sl sl-icon-settings"> </i></a>
									<label class="form-check-label margin-left-50">
										{!! Lang::get('messages.main_setings') !!}
									</label>
								</div>
								<div class="main_loader lds-ring">
									<div></div>
									<div></div>
									<div></div>
									<div></div>
								</div>
								<div id="text-generation0" class="drop-down-text is-active-custom" style="display: none;">
									<h5 class="titles-to-items-custom">{{ Lang::get('messages.Name_of_the_clinic') }}
										<a onclick="refresh_text('clinickName')"> <i class="fa fa-refresh" style="font-size:24px"></i></a>
									</h5>
									<input name="site_name" class="search-field"  id="clinickName" type="text" value="Стоматологія" >
									<h5 class="titles-to-items-custom">{!! Lang::get('messages.colors') !!}</h5>
									<ul class="ul-custom"  >
										@foreach ($layouts as $key=>$layout)
											<li class="list-group-item parent-benefit parent-color">
												<input onclick="selectOnlyThis(this)" type="checkbox" name="selected_color" id="selected_color_{{$layout['layouts_id']}}" class="form-check-input"
												value="{{$layout['layouts_id']}}" {{$key==0 ? 'checked' : ''}}/>
													<label class="custom-label" for="selected_color_{{$layout['layouts_id']}}">
														<span class="slider-my-custom round"></span>
														{!! Lang::get('messages.layout') !!} {{$key+1}}
														<img src="{{ URL::to($layout['screan']) }}" id="original" class="imgBlocks">
													</label>
													<div class="color-picker">
														@foreach ($layout['values'] as $value)
															<div  class="color-col">
																<label for="{{$value['meta_key']}}">
																	{{$value['meta_key']=='head_color' ? 
																	Lang::get('messages.main_colors') : 
																	($value['meta_key']=='title_color' ?
																	Lang::get('messages.header_colors') :  Lang::get('messages.text_colors') )}}
																</label>
																<input type="color" id="{{$value['meta_key']}}_{{$layout['layouts_id']}}"  class="color-input"  value="{{$value['meta_value']}}">
																<input type="hidden"  name="selected_color_{{$layout['layouts_id']}}[{{$value['meta_key']}}]" value="{{$value['meta_value']}}">
															</div>														
														@endforeach
													</div>
												</li>

										@endforeach										

										<li class="list-group-item parent-benefit parent-color">
											<input onclick="selectOnlyThis(this)" type="checkbox" name="selected_color" id="selected_color_0" class="form-check-input" value="0" />
											<label class="custom-label last-color" for="selected_color_0">
												<span class="slider-my-custom round "></span>
												{!! Lang::get('messages.own') !!}
											</label>
											<div class="color-picker">
												<div  class="color-col">
													<label for="head_color">{!! Lang::get('messages.main_colors') !!}</label>
													<input type="color" class="color-input" name="selected_color_0[head_color]" value="#000">
												</div>
												<div  class="color-col">
													<label for="title_color">{!! Lang::get('messages.header_colors') !!}</label>
													<input type="color" class="color-input" name="selected_color_0[title_color]" value="#000">
												</div>
												<div  class="color-col">
													<label for="text_color">{!! Lang::get('messages.text_colors') !!}</label>
													<input type="color" class="color-input" name="selected_color_0[text_color]" value="#000">
												</div>
											</div>
										</li>
									</ul>

									<h5 class="titles-to-items-custom">{!! Lang::get('messages.logo') !!}</h5>
									<div class="wrapper-galery">

										<div id="header_logoFiles" class="media-galery">
											<img src="{{ URL::to($layouts[0]['logo']) }}" id="header_logo_file"  class="imgBlocksInside" >
										</div>
										<div class="galery-wrapper-custom">
											<span class="btn btn-primary btn-file">
												{!! Lang::get('messages.download') !!}<input type="file" name="header_logo_file" class="custom-file-input" id="files"  accept=".jpg, .jpeg, .png">
											</span>
											<a onclick="refresh_img('header_logo')"><i class="fa fa-refresh" style="font-size:24px"></i></a>
											<input type="hidden" name="header_logo" id="header_logo" value="{{ URL::to($layouts[0]['logo']) }}">
										</div>
									</div> <br>
									<h5 class="titles-to-items-custom">{!! Lang::get('messages.main_pic') !!}</h5>
									<div class="wrapper-galery">
											
										<div id="header_imgFiles" class="media-galery">
											<img src="{{ URL::to($layouts[0]['img']) }}" id="header_img_file"  class="imgBlocksInside" >
										</div>

										<div class="galery-wrapper-custom">
											<span class="btn btn-primary btn-file">
												{!! Lang::get('messages.download') !!}<input type="file" name="header_img_file" class="custom-file-input" id="files"   accept=".jpg, .jpeg, .png">
											</span>
											<a onclick="refresh_img('header_img')"><i class="fa fa-refresh" style="font-size:24px"></i></a>
											<input type="hidden" name="header_img" id="header_img" value="{{ URL::to($layouts[0]['img']) }}">
										</div>
									</div>
								</div>
							</div>

                            <div id="tamplate_header">

							</div>
							<div id="tamplate_benefits">

                            </div>
                            <ul id="elementCats"  class="form-check">
								
								@if(count($templateElement)>0 )
									@foreach ($templateElement as $item)
										@if($item != Null )
											<div class="notification warning closeable box1" id="checkbox-area">
												<input type="checkbox" name="blocks[]" class="form-check-input" id="element{{$item->id}}" value="{{$item->url}}">
												<span class="slider-my-custom round"></span>
												<a class="showMore" data-id="{{$item->id}}"><i class="sl sl-icon-settings"> </i></a>
												<label class="form-check-label" for="element{{$item->id}}">{{$item->name}}
												<img src="{{$item->thumbnail}}" id="original" class="imgBlocks">
												</label>
													<a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>
											</div>
											@endif
									@endforeach	
								@endif							

                            </ul>
                             <div id="tamplate_footer">

                            </div>

                        </div>
                        <div class="next-button-placer">
                            <div>
                                <button class="button preview"  id = "submitBtn" type="submit">{{ Lang::get('messages.Create') }}</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>


        </div>
        <!-- Material checked -->
		
        <!-- Material checked -->
<script>  	
	var elements ={!! json_encode($templateElement) !!}
</script>
{{-- <script type="text/javascript" src="{{ asset('scripts/create.js')}}"></script> --}}
<script type="text/javascript" src="scripts/jquery.ui.touch.js"></script>
@endsection
