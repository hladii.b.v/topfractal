@extends('layouts.main')

@section('content')

<!-- Content
================================================== -->


<section class="fullwidth padding-top-20" data-background-color="#fff">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <!-- Headline -->

                <h4 class="headline margin-top-40 margin-bottom-25">{{ Lang::get('polityka.name') }}</h4>
                {!! Lang::get('polityka.description') !!}

            </div>
        </div>
        <!-- Row / End -->

    </div>
</section>
<!-- Info Section -->

<!-- Info Section / End -->



@endsection
