@extends('layouts.main')

@section('content')

<!-- Content
================================================== -->
<div id="titlebar" class="gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2>{{Lang::get('messages.choose_templates')}}</h2><span></span>               

            </div>
        </div>
    </div>
</div>

<section class="fullwidth padding-top-20" data-background-color="#fff">
    <div class="container">

       <div class="row">
           
            <div class="displayFlex">

                <!-- Listing Item -->
                <div class="carouselFlex">

                        <div class="listing-item">
                            <img src="elements/images/thumbs/Screenshot_11.jpg" alt="">
                            <div class="listing-item-content">
                                <a href="{{ route('live.preview.mob', ['locale' => app()->getLocale(), 'siteID'=>79, 'page_css' =>83 ]) }}" target="_blank">
                                    <span class="tag">{{Lang::get('messages.demo')}}</span>
                                </a>
                                <h3>{{Lang::get('messages.dentists')}}<i class="verified-icon"></i></h3>
                            <span>{{Lang::get('messages.universal')}}</span>
                            </div>
                        </div>
                        <div class="star-rating-hladii">

                            
                            @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'83']) }}" class="button book-now">
                                {{Lang::get('messages.select')}}
                            </a>
                            @else
                                <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                {{Lang::get('messages.select')}}
                            </a>
                            @endif

                        </div>

                </div>
                <div class="carouselFlex">

                        <div class="listing-item">
                            <img src="elements/images/thumbs/vvsads.png" alt="">
                            <div class="listing-item-content">
                                <a href="{{ route('live.preview.mob', ['locale' => app()->getLocale(), 'siteID'=>79, 'page_css' =>302 ]) }}" target="_blank">
                                    <span class="tag">{{Lang::get('messages.demo')}}</span>
                                </a>
                                <h3>{{Lang::get('messages.dentists')}}<i class="verified-icon"></i></h3>
                            <span>{{Lang::get('messages.universal')}}</span>
                            </div>
                        </div>
                        <div class="star-rating-hladii">

                            
                            @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'302']) }}" class="button book-now">
                                {{Lang::get('messages.select')}}
                            </a>
                            @else
                                <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                {{Lang::get('messages.select')}}
                            </a>
                            @endif{{Lang::get('messages.select')}}</a>

                        </div>

                </div>
                <div class="carouselFlex">

                    <div class="listing-item">
                        <img src="elements/images/thumbs/2.png" alt="">
                        <div class="listing-item-content">
                            <a href="{{ route('live.preview.mob', ['locale' => app()->getLocale(), 'siteID'=>79, 'page_css' =>83 ]) }}" target="_blank">
                                <span class="tag">{{Lang::get('messages.demo')}}</span>
                            </a>
                            <h3>{{Lang::get('messages.dentists')}}<i class="verified-icon"></i></h3>
                        <span>{{Lang::get('messages.universal')}}</span>
                        </div>
                    </div>
                    <div class="star-rating-hladii">

                        
                        @if(\Illuminate\Support\Facades\Auth::check())
                        <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'303']) }}" class="button book-now">
                            {{Lang::get('messages.select')}}
                        </a>
                        @else
                            <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                            {{Lang::get('messages.select')}}
                        </a>
                        @endif

                    </div>

            </div>
        </div>
        <div class="row">  
            <h4>З Бази </h4>  
            <div class="displayFlex">

                @foreach ($templates as $template)

                <!-- Listing Item -->
                    <div class="carouselFlex">

                        <div class="listing-item">
                        <img src="{{ $template->img}}" alt="">
                            <div class="listing-item-content">
                                <a href="{{ $template->demo}}" target="_blank">
                                    <span class="tag">{{Lang::get('messages.demo')}}</span>
                                </a>
                                <h3>{{ $template->name}}<i class="verified-icon"></i></h3>
                               
                            </div>
                        </div>
                        <div class="star-rating-hladii">

                            
                            @if(\Illuminate\Support\Facades\Auth::check())
                                <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=> $template->id]) }}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                            @else
                                <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                            @endif

                        </div>

                    </div>
                @endforeach
            </div>

        </div>
        <!-- Row / End -->

    </div>
</section>
<!-- Info Section -->

<!-- Info Section / End -->



@endsection
