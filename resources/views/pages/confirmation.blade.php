@extends('layouts.main')

@section('content')

<!-- Content
================================================== -->


<section class="fullwidth padding-top-75" data-background-color="#fff">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
    
                <div class="booking-confirmation-page">
                    @if($json['status'] == 'ok')
                        <i class="fa fa-check-circle"></i>
                        <h2 class="margin-top-30">Thanks for select us!</h2>
                        <p>{{$json['message']}}</p>
                        <a href="{{ route('dashboard', ['locale'=>app()->getLocale()]) }}" class="button margin-top-30">{{Lang::get('messages.dashboard')}}</a>
                    @else 
                    <i class="fa fa-ban"></i>
                        <h2 class="margin-top-30">Payment cancel</h2>
                        <p>{{$json['message']}}</p>
                        <a href="{{ route('dashboard', ['status'=>1]) }}" class="button margin-top-30">{{Lang::get('messages.dashboard')}}</a>
                    @endif
                </div>
    
            </div>
        </div>
    </div>
</section>

<style>
    i.fa.fa-ban {
        color: red;
    }
</style>

<!-- Info Section -->

<!-- Info Section / End -->



@endsection
