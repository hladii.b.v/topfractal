@extends('layouts.main')

@section('content')

    <div id="myCarousel" class="carousel slide " data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="images/slider1.jpg" >

                <div class="carousel-caption">                   
                    <h2>{!!isset($page_content['header_1']) ? $page_content['header_1'] : Lang::get('messages.skills1')!!}</h2>
                    <h4>{!!isset($page_content['header_text_1']) ? $page_content['header_text_1'] : Lang::get('messages.learn1')!!}</h4>
                    {{-- <h2>{{Lang::get('messages.skills1')}}</h2>
                    <h4>{{Lang::get('messages.learn1')}}</h4> --}}
                </div>
            </div>
            <div class="item">
                <img src="images/slider2.jpg" >

                <div class="carousel-caption">
                    <h2>{!!isset($page_content['header_2']) ? $page_content['header_2'] : Lang::get('messages.skills2')!!}</h2>
                    <h4>{!!isset($page_content['header_text_2']) ? $page_content['header_text_2'] : Lang::get('messages.learn2')!!}</h4>
                </div>
            </div>
            <div class="item">
                <img src="images/slider3.jpg" >

                <div class="carousel-caption">
                    <h2>{!!isset($page_content['header_3']) ? $page_content['header_3'] : Lang::get('messages.skills3')!!}</h2>
                    <h4>{!!isset($page_content['header_text_3']) ? $page_content['header_text_2'] : Lang::get('messages.learn3')!!}</h4>
                </div>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>


        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>

        </a>

    </div>

<!-- Content
================================================== -->


<section id="layouts" class="fullwidth padding-top-75 " data-background-color="#fff">
    <div class="container width90">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="headline centered headline-extra-spacing">
                <strong class="headline-with-separator">{!!isset($page_content['templates']) ? $page_content['templates'] : Lang::get('messages.templates')!!}</strong>
               </h3>
        </div>
    </div>


    <div  class="row">
        
        <div class="displayFlex">

            <!-- Listing Item -->
            <div class="carouselFlex">

                    <div class="listing-item">
                        <img src="elements/images/thumbs/Screenshot_11.jpg" alt="">
                        <div class="listing-item-content">
                            
                            <a href="{{ route('live.preview.mob', ['locale' => app()->getLocale(), 'siteID'=>79, 'page_css' =>83 ]) }}" target="_blank">
                                <span class="tag borderColor">{!!isset($page_content['demo']) ? $page_content['demo'] : Lang::get('messages.demo')!!}</span>
                            </a>
                            <h3>{!!isset($page_content['templates1']) ? $page_content['templates1'] : ''!!}<i class="verified-icon"></i></h3>
                        <span>{!!isset($page_content['text_templates1']) ? $page_content['text_templates1'] : ''!!}</span>
                        </div>
                    </div>
                    <div class="star-rating-hladii">

                       
                        @if(\Illuminate\Support\Facades\Auth::check())
                        <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'83']) }}" class="button book-now">
                            {!!isset($page_content['select']) ? $page_content['select'] : Lang::get('messages.select')!!}
                        </a>
                        @else
                            <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                {!!isset($page_content['select']) ? $page_content['select'] : Lang::get('messages.select')!!}
                        </a>
                        @endif

                    </div>

            </div>
            <div class="carouselFlex">

                    <div class="listing-item">
                        <img src="elements/images/thumbs/vvsads.png" alt="">
                        <div class="listing-item-content">
                            
                            <a href="{{ route('live.preview.mob', ['locale' => app()->getLocale(), 'siteID'=>79, 'page_css' =>302 ]) }}" target="_blank">
                                <span class="tag borderColor">{!!isset($page_content['demo']) ? $page_content['demo'] : Lang::get('messages.demo')!!}</span>
                            </a>
                            <h3>{!!isset($page_content['templates2']) ? $page_content['templates2'] : ''!!}<i class="verified-icon"></i></h3>
                            <span>{!!isset($page_content['text_templates2']) ? $page_content['text_templates2'] : ''!!}</span>
                        </div>
                    </div>
                    <div class="star-rating-hladii">

                        
                        @if(\Illuminate\Support\Facades\Auth::check())
                        <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'302']) }}" class="button book-now">
                            {!!isset($page_content['select']) ? $page_content['select'] : Lang::get('messages.select')!!}
                        </a>
                        @else
                            <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                {!!isset($page_content['select']) ? $page_content['select'] : Lang::get('messages.select')!!}
                        </a>
                        @endif

                    </div>

            </div>
                        <!-- Listing Item -->
            <div class="carouselFlex">

                <div class="listing-item">
                    <img src="elements/images/thumbs/2.png" alt="">
                    <div class="listing-item-content">
                        
                        <a href="{{ route('live.preview.mob', ['locale' => app()->getLocale(), 'siteID'=>79, 'page_css' =>83 ]) }}" target="_blank">
                            <span class="tag borderColor">{!!isset($page_content['demo']) ? $page_content['demo'] : Lang::get('messages.demo')!!}</span>
                        </a>
                        <h3>{!!isset($page_content['templates3']) ? $page_content['templates3'] : ''!!}<i class="verified-icon"></i></h3>
                        <span>{!!isset($page_content['text_templates3']) ? $page_content['text_templates3'] : ''!!}</span>
                    </div>
                </div>
                <div class="star-rating-hladii">

                   
                    @if(\Illuminate\Support\Facades\Auth::check())
                    <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'303']) }}" class="button book-now">
                        {!!isset($page_content['select']) ? $page_content['select'] : Lang::get('messages.select')!!}
                    </a>
                    @else
                    <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                        {!!isset($page_content['select']) ? $page_content['select'] : Lang::get('messages.select')!!}
                    </a>
                    @endif
                </div>

            </div>

        </div>
         
    </div>
</section>
{{-- 
<section class="fullwidth padding-top-75 padding-bottom-30" data-background-color="#fff">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h3 class="headline centered">
                    <strong class="headline-with-separator">{{Lang::get('messages.about')}}</strong>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- Headline -->
                {{Lang::get('messages.about_text')}}

            </div>
        </div>

    </div>
</section> --}}

<section id="benefits" class="fullwidth padding-top-75 " data-background-color="#fff" style="background: rgb(255, 255, 255);">
    <div class="container big-width">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="headline centered headline-extra-spacing">
                <strong class="headline-with-separator">{!!isset($page_content['benefits']) ? $page_content['benefits'] : ''!!}</strong>
               </h3>
        </div>
    </div>

    <div class="row icons-container">
        <!-- Stage -->
        <div class="col-md-3">
            <div class="icon-box-2 with-line">
                <img src="images/ben1.png"  class="mainBenefits">
                <h3>{!!isset($page_content['BenefitsName1']) ? $page_content['BenefitsName1'] : ''!!}</h3>
                <p>{!!isset($page_content['BenefitsText1']) ? $page_content['BenefitsText1'] : ''!!}</p>
            </div>
        </div>

        <!-- Stage -->
        <div class="col-md-3">
            <div class="icon-box-2 with-line">
                <img src="images/ben2.png"  class="mainBenefits">
                <h3>{!!isset($page_content['BenefitsName2']) ? $page_content['BenefitsName2'] : ''!!}</h3>
                <p>{!!isset($page_content['BenefitsText2']) ? $page_content['BenefitsText2'] : ''!!}</p>
            </div>
        </div>
        
        <!-- Stage -->
        <div class="col-md-3">
            <div class="icon-box-2 with-line">
                <img src="images/ben3.png"  class="mainBenefits">
                <h3>{!!isset($page_content['BenefitsName3']) ? $page_content['BenefitsName3'] : ''!!}</h3>
                <p>{!!isset($page_content['BenefitsText3']) ? $page_content['BenefitsText3'] : ''!!}</p>
            </div>
        </div>

        <!-- Stage -->
        <div class="col-md-3">
            <div class="icon-box-2  with-line">
                <img src="images/ben4.png" class="mainBenefits">
                <h3>{!!isset($page_content['BenefitsName4']) ? $page_content['BenefitsName4'] : ''!!}</h3>
                <p>{!!isset($page_content['BenefitsText4']) ? $page_content['BenefitsText4'] : ''!!}</p>
            </div>
        </div>
    </div>
</div>
</section>



<section class="fullwidth padding-top-75 " data-background-color="#fff">
    <div id="tariffs" class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="headline centered headline-extra-spacing">
                    <strong class="headline-with-separator">{!!isset($page_content['tariffs']) ? $page_content['tariffs'] : ''!!}</strong>
                   </h3>
            </div>
        </div>
        <!-- Row / Start -->
        <div class="row">

            <div class="col-md-12">
                <div class="pricing-container margin-top-30">

                <!-- Plan #1 -->

                    <div class="plan">

                        <div class="plan-price">
                            <h3>{!!isset($page_content['tariffs_Basic']) ? $page_content['tariffs_Basic'] : ''!!}</h3>                           
                            <span class="value">{{$tariffs[1]['tariff_price'][0]['value']}}</span><span class="period-price">{!!isset($page_content['priceValue']) ? $page_content['priceValue'] : ''!!}</span>
                            <span class="period">{{Lang::get('messages.tariffs_Basic_short_desc')}}</span>
                            <a  class="button margin-top-45" href="{{route('tariffs.select', ['locale' => app()->getLocale()])}}">{{Lang::get('messages.create')}}</a>

                        </div>

                        <div class="plan-features">
                        @if(count($tariffs[1]['tariff_text']) > 0)
                            <ul>
                                @foreach ($tariffs[1]['tariff_text'] as $value)
                                @if($value->show == 1)
                                    <li>{{$value->value}}</li>
                                @endif
                                @endforeach
                            </ul>
                            
                            <ul id="more-tariff-1" class="more-tariff active">
                                @foreach ($tariffs[1]['tariff_text'] as $value)
                                @if($value->show == 0)
                                    <li>{{$value->value}}</li>
                                @endif
                                @endforeach
                                <a  class="hide-tariff margin-top-30" id="hide-tariff-1"  onclick="hideMoreTariff(1)"><i class="sl sl-icon-arrow-up"></i></a>
                            </ul>
                            <a  id="show-tariff-1" class="show-tariff margin-top-30" onclick="showMoreTariff(1)"><i class="sl sl-icon-arrow-down"></i></a>

                        @endif
                        </div>

                    </div>

                    <!-- Plan #3 -->
                    <div class="plan featured">

                        <div class="listing-badge">
                            <span class="featured">{!!isset($page_content['Featured']) ? $page_content['Featured'] : ''!!}</span>
                        </div>

                        <div class="plan-price">
                            <h3>{!!isset($page_content['tariffs_Extended']) ? $page_content['tariffs_Extended'] : ''!!}</h3>
                            <span class="value">{{$tariffs[2]['tariff_price'][0]['value']}}</span><span class="period-price">{!!isset($page_content['priceValue']) ? $page_content['priceValue'] : ''!!}</span>
                            <span class="period">{{Lang::get('messages.tariffs_Extended_short_desc')}}</span>
                            <a class="button  margin-top-45 borderColor" href="{{route('tariffs.select', ['locale' => app()->getLocale()])}}">{{Lang::get('messages.create')}}</a>

                        </div>
                        <div class="plan-features">
                            @if(count($tariffs[2]['tariff_text']) > 0)
                                <ul>
                                @foreach ($tariffs[2]['tariff_text'] as $value)
                                @if($value->show == 1)
                                    <li>{{$value->value}}</li>
                                @endif
                                @endforeach
                                </ul>
                                <ul id="more-tariff-2" class="more-tariff active">
                                    @foreach ($tariffs[2]['tariff_text'] as $value)
                                    @if($value->show == 0)
                                        <li>{{$value->value}}</li>
                                    @endif
                                    @endforeach
                                    <a  class="hide-tariff margin-top-30" id="hide-tariff-2"  onclick="hideMoreTariff(2)"><i class="sl sl-icon-arrow-up"></i></a>
                                </ul>
                                <a  id="show-tariff-2" class="show-tariff margin-top-30" onclick="showMoreTariff(2)"><i class="sl sl-icon-arrow-down"></i></a>

                            @endif

                        </div>
                    </div>

                    <!-- Plan #3 -->
                    <div class="plan">

                        <div class="plan-price">
                            <h3>{!!isset($page_content['tariffs_Professional']) ? $page_content['tariffs_Professional'] : ''!!}</h3>
                            <span class="value">{{$tariffs[3]['tariff_price'][0]['value']}}</span><span class="period-price">{!!isset($page_content['priceValue']) ? $page_content['priceValue'] : ''!!}</span>
                            <span class="period">{{Lang::get('messages.tariffs_Professional_short_desc')}}</span>
                            <a class="button margin-top-45" href="{{route('tariffs.select', ['locale' => app()->getLocale()])}}">{{Lang::get('messages.create')}}</a>

                        </div>

                        <div class="plan-features">
                            @if(count($tariffs[3]['tariff_text']) > 0)
                            <ul>
                                @foreach ($tariffs[3]['tariff_text'] as $value)
                                @if($value->show == 1)
                                    <li>{{$value->value}}</li>
                                @endif
                                @endforeach
                                </ul>
                                <ul id="more-tariff-3" class="more-tariff active">
                                    @foreach ($tariffs[3]['tariff_text'] as $value)
                                    @if($value->show == 0)
                                        <li>{{$value->value}}</li>
                                    @endif
                                    @endforeach
                                    <a  class="hide-tariff margin-top-30" id="hide-tariff-3"  onclick="hideMoreTariff(3)"><i class="sl sl-icon-arrow-up"></i></a>
                                </ul>
                                <a  id="show-tariff-3" class="show-tariff margin-top-30" onclick="showMoreTariff(3)"><i class="sl sl-icon-arrow-down"></i></a>

                            @endif
                            

                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <!-- Row / End -->
        <div class="row margin-top-30">
            <div class="col-md-12 ">

                <h3 class="headline centered headline-extra-spacing">
                    <strong class="headline-with-separator">{!!isset($page_content['services']) ? $page_content['services'] : ''!!}</strong>
                   </h3>
                @if(count($services)>0)
                   @foreach ($services as $service)
                   <div class="services">                    
                        <div>{{$service->name}}</div>
                        <div>{{$service->desk}}</div>
                        <div>{{$service->price}}{!!isset($page_content['priceValue']) ? $page_content['priceValue'] : ''!!}</div>
                        <div>
                            <a href="{{route('tariffs.select', ['locale' => app()->getLocale()])}}" class="button">Купити</a>
                        </div>
                    </div>
                   @endforeach
                @endif   
                
            </div>
        </div>
    </div>
</section>

<section id="reviews" class="fullwidth padding-top-75" data-background-color="#fff">
	<!-- Info Section -->
	<div class="container">

        <div class="row">
            <div class="col-md-12">
                <h3 class="headline centered ">
                    <strong class="headline-with-separator">{!!isset($page_content['reviews']) ? $page_content['reviews'] : ''!!}</strong>

                </h3>
            </div>
        </div>

	</div>
	<!-- Info Section / End -->

	<!-- Categories Carousel -->
	<div class="fullwidth-carousel-container margin-top-20 no-dots">
		<div class="testimonial-carousel testimonials">

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial">{!!isset($page_content['reviews1']) ? $page_content['reviews1'] : ''!!}</div>
				</div>
				<div class="testimonial-author">

					<h4>{!!isset($page_content['reviews_name_1']) ? $page_content['reviews_name_1'] : ''!!}</h4>

				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial">{!!isset($page_content['reviews2']) ? $page_content['reviews2'] : ''!!}</div>
                </div>
				<div class="testimonial-author">


					<h4>{!!isset($page_content['reviews_name_2']) ? $page_content['reviews_name_2'] : ''!!}</h4>

				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial">{!!isset($page_content['reviews3']) ? $page_content['reviews3'] : ''!!}</div>
                </div>
				<div class="testimonial-author">


					<h4>{!!isset($page_content['reviews_name_3']) ? $page_content['reviews_name_3'] : ''!!}</h4>

				</div>
			</div>

		</div>
	</div>
	<!-- Categories Carousel / End -->

</section>


<!-- Info Section -->

<!-- Info Section / End -->


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

@endsection
