@extends('layouts.main')

@section('content')

<!-- Content
================================================== -->


<section class="fullwidth padding-top-20" data-background-color="#fff">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <!-- Headline -->

                <h4 class="headline margin-top-40 margin-bottom-25">{{Lang::get('messages.about')}}</h4>
                {!! Lang::get('messages.about_text') !!}

            </div>
        </div>

        <div class="row margin-top-50">

            <!-- Contact Details -->
            <div class="col-md-4">    
                <!-- Contact Details -->
                <div class="sidebar-textbox">
                   
                    <ul class="contact-details">
                        <li><i class="im im-icon-Mail"></i> <strong>{!! Lang::get('messages.email') !!}:</strong> <span>hladii.b.v@gmail.com</span></li>
                        <li><i class="im im-icon-Phone-2"></i> <strong>{!! Lang::get('messages.phone') !!}:</strong> <span>(067) 123-00-50 </span></li>
                        <li><i class="im im-icon-Globe"></i> <strong>{!! Lang::get('messages.web') !!}:</strong> <span><a href="https://dent.eco/">https://dent.eco/</a></span></li>
                    </ul>
                </div>
    
            </div>
    
            <!-- Contact Form -->
            <div class="col-md-8">
    
                <section id="contact">
                    <h4 class="headline margin-bottom-35">{!! Lang::get('messages.contact_form') !!}</h4>
    
                    <div id="contact-message"></div> 
    
                        <form method="post" action="contact.php" name="contactform" id="contactform" autocomplete="on">
    
                        <div class="row">                            
    
                            <div class="col-md-12">
                                <div>
                                    <input name="email" type="email" id="email" placeholder="{!! Lang::get('messages.email') !!}" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required="required">
                                </div>
                            </div>
                        </div>
    
                        <div>
                            <input name="subject" type="text" id="subject" placeholder="{!! Lang::get('messages.subject') !!}" required="required">
                        </div>
    
                        <div>
                            <textarea name="comments" cols="40" rows="3" id="comments" placeholder="{!! Lang::get('messages.message') !!}" spellcheck="true" required="required"></textarea>
                        </div>
    
                        <input type="submit" class="submit button" id="submit" value="{!! Lang::get('messages.submit_message') !!}">
    
                        </form>
                </section>
            </div>
            <!-- Contact Form / End -->
    
        </div>
        <!-- Row / End -->

    </div>
</section>
<!-- Info Section -->

<!-- Info Section / End -->



@endsection
