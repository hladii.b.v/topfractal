@extends('layouts.main')

@section('content')

<!-- Content
================================================== -->


<section class="fullwidth padding-top-75" data-background-color="#fff">
    <div class="container">
        <div class="row">    
            <!-- Content
            ================================================== -->
            <div class="col-lg-8 col-md-8 padding-right-30 col-lg-offset-2">  
                
                <div class="row">
                    <div class="payment">

                        <ul class="list-2 siteChecked">
                            @foreach($sites as $key=>$site)
                                <li>
                                    <label class="switch">
                                        <input type="checkbox" name="siteChecked[]" value="{{$site->id}}" {{$key== 0 ? 'checked' : ''}}  onchange="getSiteTariff()"><span class="slider round"></span>
                                    </label>	
                                    Сайт №{{$site->id}} - {{$site->site_name}}.
                                    @if($site->remote_url != Null)
                                    Період публікації {{$site->publish_date}} - {{$site->publish_date_end}}
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row margin-top-35">
                                   
                   
                    <div class="payment">
        
                        <div class="payment-tab payment-tab-active">
                            <div class="payment-tab-trigger">
                                <input checked="" id="paypal" name="cardType" type="radio" value="paypal">
                                <label for="paypal">Credit / Debit Card</label>
                                <img class="payment-logo paypal"  src="https://i.imgur.com/IHEKLgm.png" alt="">
                            </div>
                           
                                <div class="payment-tab-content">
                                    <div class="row">
            
                                        <div class="col-md-12">
                                            <div class="card-label">
                                                <label for="tarriffCard">Тариф</label>
                                                <ul class="list-2 tariffChecked" id="tariffChecked">  
                                                                                            
                                                                                                 
                                                </ul>
                                                <label for="amountCard">Період</label>
                                                <select id="amountCard" name="amount" onchange="LiqPayForm()">
                                                
                                                </select>
                                            </div>
                                        </div>
            
                                    </div>
                                </div>
                        
                        </div>    
        
        
                    </div>
                    <!-- Payment Methods Accordion / End -->
                  
                   
                </div>
                <div class="row">
                    <div class="payment margin-top-35">
                        <ul class="list-3">
                            @foreach($services as $key=>$service)
                                <li class="tariffServices">
                                    <label class="switch">
                                        <input type="checkbox" name="serviceChecked[]" value="{{$service->price}}" data-id="{{$service->id}}" onchange="LiqPayForm()"><span class="slider round"></span>
                                    </label>	
                                    <span>{{$service->name}}</span>
                                    <span>{{$service->desk}}</span>
                                    <span>{{$service->price}} грн</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                 
                    <h3 class="margin-top-55 margin-bottom-30">Сумма <span id="totalCost"></span></h3>
                    <div id="LiqPayForm">
                        
                    </div>
                       
                </div>
            </div> 
        </div>
    </div>
   

</section>

<script>
    
    
    function getSiteTariff () {

        $('input[name="siteChecked[]"]:checked').each(function() {
           siteId = parseInt(this.value);           
        });

        $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
		    type: 'POST', 
		    url:  window.location.origin+'/SitteTariff',
			data: {siteId: siteId },
		   
			success:function(response) { 	
                
                inner = ''
                response.map((data, index) =>  
                inner += `<li>
                    <label class="switch">
                        <input type="checkbox" name="tariffChecked[]" value="${data.number}" ${ index == 0 ? `checked`: ``} onchange="tariffChecked()"><span class="slider round"></span>
                    </label>	
                    ${data.number == 1 ? `БАЗОВИЙ`: (data.number == 2 ? `ПРЕМІУМ` :  `ПРЕМІУМ ПЛЮС` )}
                </li>`)
                document.getElementById('tariffChecked').innerHTML = inner
               
                tariffChecked () 
                LiqPayForm ()
		    }
		})        
    }
    getSiteTariff ()

    function LiqPayForm (){
        var amount = document.getElementById('amountCard').value
        var totalAmount = parseInt(amount)
        var serviceChecked = ''        

        $('input[name="serviceChecked[]"]:checked').each(function() {
            totalAmount += parseInt(this.value);
            serviceChecked += this.dataset.id+','
        });

        $('input[name="siteChecked[]"]:checked').each(function() {
           siteId = parseInt(this.value);
           
        });
      
        tariffId = $('#amountCard').find(':selected').data('id')       
        $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
		    type: 'POST', 
		    url:  window.location.origin+'/LiqPayForm',
			data: {totalAmount:totalAmount, locale: 'ua', siteId: siteId, tariffId: tariffId, serviceChecked: serviceChecked},
		    
			success:function(response) { 	         
                document.getElementById('totalCost').innerHTML = totalAmount
                document.getElementById('LiqPayForm').innerHTML = response.form
                
		    }
		})	
    } 

    function  tariffChecked () {                
        var tariff
        $('input[name="tariffChecked[]"]:checked').each(function() {
            tariff = parseInt(this.value);          
        });     
       
      
        if (!tariff){
            inner = `<option value="0" data-id="0" selected></option>`
                
            document.getElementById('amountCard').innerHTML = inner
            document.getElementById('amountCard').style.visibility = 'hidden'
            LiqPayForm ()
        }
        else{
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST', 
                url:  window.location.origin+'/tariffChecked',
                data: {locale: 'ua', tariff: tariff},
            
                success:function(response) { 	
                    var inner = '';	      
                
                    response.priceArray.map((data, index) =>  
                
                        inner += `<option value="${data.price}" data-id="${data.id}" ${ index == 0 ? `selected`: ``}>${data.price}
                            ${ index == 0 ? `грн - 1 місяць`: (index = 1 ? `грн - 6 місяців` :  `грн - 12 місяців`)}  </option>`
                    )
                    document.getElementById('amountCard').innerHTML = inner
                    document.getElementById('amountCard').style.visibility = 'visible'
                    LiqPayForm ()
                
                }
            })
        }  
        $(".tariffChecked input:checkbox").on('click', function() {    
        var $box = $(this);
       
            if ($box.is(":checked")) {       
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                
                $(group).prop("checked", false);
                $box.prop("checked", true);
                LiqPayForm ()
            } 
            else {
                $box.prop("checked", false);
            }
        });   
    
    }
    $(".tariffChecked input:checkbox").on('click', tariffChecked () )
    
    
    $(".list-2 input:checkbox").on('click', function() {    
        var $box = $(this);
       console.log($box)
        if ($box.is(":checked")) {       
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            
            $(group).prop("checked", false);
            $box.prop("checked", true);
            LiqPayForm ()
        } 
        else {
            $box.prop("checked", true);
        }
    });    

   
    
</script>



@endsection
