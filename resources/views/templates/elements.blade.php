@extends('layouts.new-dashboard')

@section('title')
Dashboard | Tariffs
@endsection

@section('content')

<div class="dashboard-content"> 
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-window">Блоки</h1>
		</div><!-- /.col -->
    </div><!-- /.row -->	    
	<div class="row">	
		
		<div class="col-md-6 margin-top-30">
            <h4>Категорії</h4>
			<ul сlass="margin-top-30">                
                @foreach ($elementsCategory as $categoryItems)
                    <li  class="list-group" >
                        <div class="list-group-item" >							
                            <div onclick="myFunction (this)" style="width: 100%;text-align: center;">
                                <i class="fa fa-arrow-down"></i> {{$categoryItems['name']}},
                                {{count($categoryItems['elements'])}} ел
                            </div> 
                        </div>
                    
                   
                    @if(count($categoryItems['elements'])>0)
                        <div class="elements-category-container"  style="display: none">
                        @foreach ($categoryItems['elements'] as $element)
                        
                            <div class="elements-category-item" >
                                <div  class="elements-category-img">							
                                    <img src="{{$element['thumbnail']}}" onclick="img_modal('{{$element['thumbnail']}}', '{{$element['id']}}')"  data-toggle="modal" data-target="#previewModal">
                                </div>
                                <div class="elements-category-name">							
                                    <h4 contenteditable="true" class="editElementName" onfocusout="changeName(this)" data-id="{{$element['id']}}">{{$element['name']}}</h4>                            
                                    <label class="switch">
                                        <input type="checkbox" value="{{$element['id']}}" onchange="changeShow(this)" 
                                        {{$element['show']==1 ? 'checked' : ''}}><span class="slider round"></span>
                                        
                                    </label>
                                </div>
                                <div class="elements-category-form">
                                    <form action="{{route('category.element',  ['locale' => app()->getLocale()])}}" method="POST" class="category-select-container">
                                        @csrf
                                        <input type="hidden" name="elementId" value="{{$element['id']}}" >
                                        <select  name="categotyId">
                                            <option value="0">Без категорії</option> 
                                            @foreach ($categories as $item)
                                                <option value="{{$item->id}}" 
                                                    {{$element['category'] == $item->id ? 'selected' : ''}}>{{$item->name}} </option> 
                                            @endforeach
                                        </select>
                                        <button class="button gray"><i class="sl sl-icon-note"></i>Add</button>
                                    </form>
                                   
                                </div>
                                 <a  href="{{ $element['url']}}" class="button gray" target="_blank">Demo</a>
                            </div>                   
                        @endforeach
                        </div>
                    @endif
                    </li>
                @endforeach
			</ul>
        </div>
        <div class="col-md-6 margin-top-30">
            <h4>Без категорії</h4>
			<div class="elements-category-container" >
				@foreach ($elementsUnSlected as $element)
                   
                    <div class="elements-category-item" >
                        <div  class="elements-category-img">							
                            <img src="{{$element['thumbnail']}}" onclick="img_modal('{{$element['thumbnail']}}', '{{$element['id']}}')"  data-toggle="modal" data-target="#previewModal">
                        </div>
                        <div class="elements-category-name">
                            						
                            <h4 contenteditable="true"  class="editElementName"  onfocusout="changeName(this)" data-id="{{$element['id']}}">{{$element['name']}}</h4>                            
                            <label class="switch">
                                <input type="checkbox" value="{{$element['id']}}" onchange="changeShow(this)" 
                                {{$element['show']==1 ? 'checked' : ''}}><span class="slider round"></span>                                
                            </label>
                        </div>
                        <div class="elements-category-form">
                            <form action="{{route('category.element',  ['locale' => app()->getLocale()])}}" method="POST"  class="category-select-container">
                                @csrf
                                <input type="hidden" name="elementId" value="{{$element['id']}}" >
                                <select  name="categotyId">
                                    <option value="0">Без категорії</option> 
                                    @foreach ($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}} </option> 
                                    @endforeach
                                </select>
                                <button class="button gray"><i class="sl sl-icon-note"></i>Add</button>
                            </form>
                        </div>
                        <a  href="{{ $element['url']}}" class="button gray" target="_blank">Demo</a>

                    </div>
                        
                                       
			    @endforeach
            </div>
		</div>
	</div>	
	
</div><!-- /.container -->

<!-- Modals -->
<div class="modal fade" id="previewModal" role="dialog">
    
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Image Library</h4>
            </div>
            <div class="modal-body">               

                <div class="modal-body-content style-2">

                    <ul class="tabs-nav">
                        <li class="active"><a href="#myImagesTab">Зображення</a></li>
                        <li id="uploadTabLI"><a href="#uploadTab">Завантажити нове</a></li>                       
                    </ul> <!-- /tabs -->

                    <div class="tabs-containe">

                        <div class="tab-content  active" id="myImagesTab">                            
                            <!-- Alert Info -->
                            <div >                                
                               <img src="" id="modal-img">
                            </div>
                          

                        </div><!-- /.tab-pane -->

                        <div class="tab-content" id="uploadTab">

                            <form id="imageUploadForm" action="{{ route('image.upload.element') }}" method="POST" enctype="multipart/form-data" >
                                @csrf
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <input type="hidden" name="loadElementID" id="loadElementID" required>
                                    <input type="file" name="imageFile" id="imgInp" required>
                                    <img id="blah" src="" alt="your image" />
                                </div>
                                <hr>

                                <button type="submit"  class="btn btn-primary btn-embossed btn-wide upload btn-block">Завантажити</button>
                            </form>  

                        </div><!-- /.tab-pane -->

                    </div> <!-- /tab-content -->

                </div>
            </div>
            <div class="modal-footer">   
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
        
    </div>
</div>
 
<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
		position: relative;
		display: flex;
		padding: 15px 15px;
		margin-bottom: -1px;
		background-color: #fff;
		border: 1px solid #ddd;
		justify-content: space-around;
		align-items: center;
	}
	.list-group-item input {
		width: 70%;
	}
</style>
<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
        }

        $("#imgInp").change(function() {
        readURL(this);
    });

    function img_modal(src, id) {		
		console.log(src);
        document.getElementById('modal-img').src= src
        document.getElementById('loadElementID').value= id
    }
    
	function myFunction (element) {
        var list = element.parentElement;
        var parent =list.parentElement;
        var child =parent.lastElementChild;
        
        display = child.style.display
        if (display === 'none') {
            child.style.display ='flex';
        }
        else {
            child.style.display ='none'; 
        }  	    		
	}

    function changeName (element) {
        var elementID = element.dataset.id 
        var elementName = element.innerHTML;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/element-name',
			data: {elementID: elementID, elementName: elementName},		   
            success:function(response) { 
                console.log(response);
            }
		})
    }

	function changeShow (element) {
        
        var elementID = element.value
        var elementToShow= 0

        if(element.checked==true){
            elementToShow = 1
        }    
     
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/element-show',
			data: {elementID: elementID, elementShow: elementToShow},		   
            success:function(response) { 
                console.log(response);
            }
		})
	}
</script>


@endsection