@extends('layouts.new-dashboard')

@section('title')
Dashboard | Tariffs
@endsection

@section('content')

<div class="dashboard-content">
    <div class="col-md-6"> 
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <h1><span class="fui-window"> Категорії</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->	    
        <div class="row">	
            <div class="col-md-12">
                <div class="uploadPanel">				
                    <div class="add-listing">
                    
                        <form method="POST" action="{{route('category.add', app()->getLocale())}}">
                            @csrf
                            <div class="add-listing-section padding-top-25">				
                                <div class="row with-forms">							
                                    
                                    
                                    <div class="col-md-5">
                                        <label for="category_name" class="col-md-3 control-label">Name:</label>
                                        <input  id="category_name" name="name" type='text'  class="form-control" required>
                                    </div>
                                    <div class="col-md-5">
                                        <label for="category_link" class="col-md-3 control-label">Link:</label>
                                        <input  id="category_link" name="link" type='text'  class="form-control" required>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <button type="submit" class="button preview"> {{Lang::get('messages.create')}} </button>
                        
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkboxes in-row margin-bottom-20 checkboxes-categoty">
                                            @foreach ($paremeters as $item)
                                                <input type="checkbox" name="parameters[]" id="{{$item->key}}" value="{{$item->id}}">
                                                <label for="{{$item->key}}">{{$item->name}}</label>
                                            @endforeach                        
                                    
                                        </div>									
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12 margin-top-30">
                <ul >
                    
                    @foreach ($getCategory as $category)
                        <li  class="list-group" >
                            <div class="list-group-item categoty-items" >
                                
                                <div class="categoty-item-name">
                                    <div>
                                        <h4>{{$category['name']}}</h4>
                                        <input type="hidden" value="{{$category['id']}}" class="categoryId">
                                    </div>
                                    <div>
                                        <h4>{{$category['link']}}</h4>
                                        
                                    </div>
                                    <div>
                                        <a onclick="editCategory(this)" class="button gray">
                                            <i class="sl  sl-icon-note"></i> Edit
                                        </a>    
                                        <a  class="button gray" data-id="{{$category['id']}}" data-toggle="modal" data-target="#deleteModal" onclick="deleteCategoryModal(this)">
                                            {{Lang::get('messages.Delete')}}
                                        </a> 
                                    </div> 
                                </div> 
                                
                                <div class="checkboxes in-row margin-bottom-20 checkboxes-categoty">

                                    @foreach ($paremeters as $item)
                                        <input type="checkbox" name="parameters[]" id="{{$item->key}}{{$category['id']}}" value="{{$item->id}}"
                                        {{ in_array($item->id, $category['paremeters']) ? 'checked' : ''}}>
                                        <label for="{{$item->key.$category['id']}}">{{$item->name}}</label>
                                    @endforeach                          
                            
                                </div>
                                
                            </div>
                            
                        </li>                     
                    @endforeach
                </ul>
            </div>
        </div>	
    </div>
    <div class="col-md-6"> 
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <h1><span class="fui-window">Напрямки</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->	    
        <div class="row">	
            <div class="col-md-12">
                <div class="uploadPanel">				
                    <div class="add-listing">                    
                        <form method="POST" action="{{route('direction.add', app()->getLocale())}}">
                            @csrf
                            <div class="add-listing-section padding-top-25">				
                                <div class="row with-forms">							
                                    
                                    
                                    <div class="col-md-6">
                                        <label for="category_name" class="col-md-3 control-label">Name:</label>
                                        <input  id="category_name" name="name" type='text'  class="form-control" required>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <button type="submit" class="button preview"> {{Lang::get('messages.create')}} </button>
                        
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkboxes in-row margin-bottom-20">
                                            @foreach ($getCategory as $key=>$category)
                                                <input type="checkbox" name="categories[]" id="{{$category['name']}}" value="{{$category['id']}}"
                                                {{$key == 0 ? 'checked' : ''}}>
                                                <label for="{{$category['name']}}">{{$category['name']}}</label>                                            
                                            @endforeach
                                        </div>									
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12 margin-top-30">
                <ul >
                    @foreach ($getDirection as $direction)
                        <li  class="list-group" >
                            <div class="list-group-item categoty-items" >
                                
                                <div class="categoty-item-name">
                                    <div>
                                        <h4>{{$direction['name']}}</h4>
                                        <input type="hidden" value="{{$direction['id']}}" class="directionId">
                                    </div>
                                    <div>
                                        <a onclick="editDirection(this)" class="button gray">
                                            <i class="sl  sl-icon-note"></i> Edit
                                        </a>    
                                        <a  class="button gray" data-id="{{$direction['id']}}" data-toggle="modal" data-target="#deleteDirection" onclick="deleteDirectionModal(this)">
                                            {{Lang::get('messages.Delete')}}
                                        </a> 
                                    </div> 
                                </div> 
                                <div class="checkboxes in-row margin-bottom-20 checkboxes-categoty">
                                    @foreach ($getCategory as $key=>$category)
                                        <input type="checkbox" name="categories[]" id="{{$category['name']}}{{$direction['id']}}" value="{{$category['id']}}"
                                        {{ in_array($category['id'], $direction['categories']) ? 'checked' : ''}}>
                                        <label for="{{$category['name']}}{{$direction['id']}}">{{$category['name']}}</label>                                            
                                    @endforeach
                                </div>
                                
                            </div>
                            
                        </li>                     
                    @endforeach
                </ul>
            </div>
        </div>	
    </div>
</div><!-- /.container -->

<!-- Modals -->
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Видалення</h4>
            </div>
            <div class="modal-body">
                <p>ви дійсно хочете видлати категорію?</p>
            </div>
            <div class="modal-footer">                
                <form action="{{route('category.delete',  ['locale' => app()->getLocale()])}}" method="POST">
                    @csrf
                    <input type="hidden" name="categotyId"  id="categoryIdDelete" value="" >
                    <button class="button gray"><i class="sl sl-icon-close"></i> Так</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
      
    </div>
</div>
<!-- Direction Modals -->
<div class="modal fade" id="deleteDirection" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Видалення</h4>
            </div>
            <div class="modal-body">
                <p>ви дійсно хочете видлати напрямок?</p>
            </div>
            <div class="modal-footer">                
                <form action="{{route('direction.delete',  ['locale' => app()->getLocale()])}}" method="POST">
                    @csrf
                    <input type="hidden" name="directionId"  id="directionIdDelete" value="" >
                    <button class="button gray"><i class="sl sl-icon-close"></i> Так</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
      
    </div>
</div>

<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
		position: relative;
		display: flex;
		padding: 15px 15px;
		margin-bottom: -1px;
		background-color: #fff;
		border: 1px solid #ddd;
		justify-content: space-around;
		align-items: center;
	}
	.list-group-item input {
		width: 70%;
	}
</style>
<script type="text/javascript">

    function deleteCategoryModal (element) {
        console.log(element);
		
		document.getElementById('categoryIdDelete').value = element.dataset.id
	}

    function deleteDirectionModal (element) {
        console.log(element);
		
		document.getElementById('directionIdDelete').value = element.dataset.id
	}

	function myFunction (element) {
        var list = element.parentElement;
        var parent =list.parentElement;
        var child =parent.lastElementChild;
        
        display = child.style.display
        if (display === 'none') {
            child.style.display ='block';
        }
        else {
            child.style.display ='none'; 
        }  	    		
	}

	function editCategory (element) {
        var selectedValue = []
       
        var categotyId = element.parentElement.parentElement.getElementsByClassName('categoryId')[0].value
        var elementChildrens = element.parentElement.parentElement.parentElement.getElementsByClassName('checkboxes')[0].children
        for (var i=0, child; child=elementChildrens[i]; i++) {
            if (child.type == 'checkbox' && child.checked==true ) {
                selectedValue.push(child.value)
            }                
        }
        console.log(selectedValue);        
     
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/category-update',
			data: {categotyId: categotyId, selectedValue: selectedValue},		   
            success:function(response) { 
                console.log(response);
            }
		})
	}

    function editDirection (element) {
        var selectedValue = []
       
        var directionId = element.parentElement.parentElement.getElementsByClassName('directionId')[0].value
        var elementChildrens = element.parentElement.parentElement.parentElement.getElementsByClassName('checkboxes')[0].children
        for (var i=0, child; child=elementChildrens[i]; i++) {
            if (child.type == 'checkbox' && child.checked==true ) {
                selectedValue.push(child.value)
            }                
        }
        console.log(selectedValue);        
     
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/direction-update',
			data: {directionId: directionId, selectedValue: selectedValue},		   
            success:function(response) { 
                console.log(response);
            }
		})
	}
</script>


@endsection