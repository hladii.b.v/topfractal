@extends('layouts.new-dashboard')

@section('title')
Dashboard | Tariffs
@endsection

@section('content')

<div class="dashboard-content"> 
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-window">Шаблони</h1>
		</div><!-- /.col -->
    </div><!-- /.row -->	    
	<div class="row">	
		<div class="col-md-12">
			<div class="uploadPanel">				
				<div class="add-listing">
                   
					<form method="POST" action="{{route('add.templates', app()->getLocale())}}">
						@csrf
						<div class="add-listing-section padding-top-25">				
							<div class="row with-forms">							
								
								
								<div class="col-md-6">
									<label for="template_name" class="col-md-3 control-label">Name:</label>
									<input  id="template_name" name="name" type='text'  class="form-control">
                                </div>
                                <div class="col-md-4">
									<label for="tariff_type" class="col-md-3 control-label">Тип:</label>
									<select  id="tariff_type" name="direction_id" class="chosen-select-no-single">
										@foreach ($directions as $key=>$direction)
											<option value="{{$direction['id']}}" selected>{{$direction['name']}}</option>
										@endforeach										
									</select>
								</div>
                                <div class="col-md-2">
									<button type="submit" class="button preview"> {{Lang::get('messages.create')}} </button>
					
								</div>
                            </div>
						</div>
                    </form>
				</div>
			</div>
		</div>
		<div class="col-md-12 margin-top-30">
			<ul >
				@foreach ($templates as $template)
                    <li  class="list-group" >
                        <div class="list-group-item" >
							
                            <div>
								<a href="{{route('template',  ['locale' => app()->getLocale(), 'template'=>$template['id']])}}">
									{{$template['name']}}
								</a>
							</div> 
                            <div>
								<a href="{{route('template',  ['locale' => app()->getLocale(), 'template'=>$template['id']])}}">
									{{$template['direction']['name']}}
								</a>
							</div>   
							<a href="{{route('template',  ['locale' => app()->getLocale(), 'template'=>$template['id']])}}" class="button gray">
								<i class="sl  sl-icon-note"></i> Edit
							</a> 
							<a  class="button gray" data-id="{{$template['id']}}" data-toggle="modal" data-target="#deleteModal" onclick="deleteTemplateModal(this)">
								{{Lang::get('messages.Delete')}}
						   	</a>                                    
                        </div>
                        
                    </li>                     
			    @endforeach
			</ul>
		</div>
	</div>	
	
</div><!-- /.container -->
<!-- Modals -->
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Видалення</h4>
            </div>
            <div class="modal-body">
                <p>ви дійсно хочете видлати шаблон?</p>
            </div>
            <div class="modal-footer">
                <form action="{{route('delete.templates',  ['locale' => app()->getLocale()])}}" method="POST">
					@csrf
					<input type="hidden" name="template" value="" id="templateIdDelete">
					<button class="button gray"><i class="sl sl-icon-close"></i> Так</button>
				</form>  
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
      
    </div>
</div>

<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
		position: relative;
		display: flex;
		padding: 15px 15px;
		margin-bottom: -1px;
		background-color: #fff;
		border: 1px solid #ddd;
		justify-content: space-around;
		align-items: center;
	}
	.list-group-item input {
		width: 70%;
	}
</style>
<script type="text/javascript">

	function deleteTemplateModal (element) {
		
		document.getElementById('templateIdDelete').value = element.dataset.id
	}

	function myFunction (element) {
        let list = element.parentElement;
        let parent =list.parentElement;
        let child =parent.lastElementChild;
        
        display = child.style.display
        if (display === 'none') {
            child.style.display ='block';
        }
        else {
            child.style.display ='none';  
        }  	    		
	}

	function updateData (id) {
		element =document.getElementById("updateData"+id) 
		show =document.getElementById("tariff_show"+id)
		if (show.checked == true){
			toShow = 1;
		} else {
			toShow = 0 ;
		}
        color = element.value
     
		$.ajax({
		    type: 'GET', 
		    url:  window.location.origin+'/en/tariffs-update',
			data: {id: id, desc: color, show: toShow},
		    error: function() { 
		         console.log(color);
		    }
		})
	}
</script>


@endsection