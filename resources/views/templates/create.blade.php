@extends('layouts.new-dashboard')

@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


<div id="contents" class="dashboard-content">  
    
    <!-- Content -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="dashboard-list-box invoices with-icons margin-top-20">
                
                <h4>
                    <img src="{{$getTemplate['img']}} " class="template-img"  
                    onclick="img_modal('{{ URL::to($getTemplate['img']) }}', '{{$getTemplate['id']}}')" data-toggle="modal" data-target="#previewModal"
                    style="min-width: 20px;
                    min-height: 20px;" alt="Немає зображення">
                    <span contenteditable="true"  class="editElementName"  onfocusout="changeName(this)" data-id="{{$getTemplate['id']}}" >
                        {{$getTemplate['name']}} 
                    </span>
                    <div class="comission-taken">Тип: 
                        <strong>{{$getTemplatDirections['name']}}</strong>
                    </div>
                </h4>
                <input type="hidden" value="{{$getTemplate['id']}}" id="template">             
            </div>
        </div>       
    </div>
    <div class="row">
        <div  class="col-lg-4 ">
            <div class="dashboard-list-box invoices with-icons margin-top-20">  
                <h4>Доданні елементи</h4>  

                <ul  id="blocks"  class="col-lg-12 col-md-12  margin-top-15" > 
                   
                    @if($templateMeta)	
                        @foreach ($templateMeta as $categoryItems)
                            <li  class="list-group" >
                            <div class="list-group-item" >							
                                <div {!!$categoryItems['elements'] != '' ? 'onclick="myFunction (this)"' : ''!!} style="width: 100%;text-align: center;">
                                    {!!$categoryItems['elements'] != '' ? '<i class="fa fa-arrow-down"></i>' : ''!!} {{$categoryItems['name']}}                                  
                                </div> 
                            </div>                       
                    
                            @if($categoryItems['elements'] != '')
                                <div class="elements-category-container"  style="display: none">                                                           
                                    <div class="template-elements-small " data-category="{{$categoryItems['id']}}">
                                        <div  class="elements-category-img">							
                                            <img src="{{$categoryItems['elements']['thumbnail']}}">
                                        </div>
                                       
                                        <div class="elements-category-name">							
                                            <h4 >{{$categoryItems['elements']['name']}}</h4>   
                                            
                                                <span>Показувати?</span>                 
                                                <label class="switch">
                                                    <input type="checkbox" data-category="{{$categoryItems['id']}}" data-element="{{$categoryItems['elements']['id']}}" 
                                                    onchange="deleteMeta(this)" checked><span class="slider round"></span>
                                                </label>
                                        </div>
                                        <div class="elements-category-name" style="margin-top: 30px;">	
                                            <h4 >Тип</h4>                                                              
                                            
                                            <select class="element-type" data-category="{{$categoryItems['id']}}" data-id="{{$getTemplate['id']}}"
                                            data-element="{{$categoryItems['elements']['id']}}"   onchange="changeType(this)">                                                       
                                                <option value="0" {{$categoryItems['type']==null ? 'selected' : ''}}>Немає</option>
                                                <option value="required" {{$categoryItems['type']=='required' ? 'selected' : ''}}>Обовязковий</option>
                                                <option value="selected" {{$categoryItems['type']=='selected' ? 'selected' : ''}}>Вибраний</option>
                                            </select>                                               
                                            
                                        </div>
                                        <a  href="{{ $categoryItems['elements']['url']}}" class="button gray" target="_blank">Demo</a>

                                    </div>                   
                              
                                </div>
                            @endif
                            </li>
                        @endforeach
                    @else 
                        <li>Немає</li>  
                    @endif
                </ul>   
                             
            </div>
           
        </div>
        <div  class="col-lg-8">
            <div class="dashboard-list-box invoices with-icons margin-top-20">  
                <h4>Всі елементи</h4>  

                <div  id="template-elements"  class="col-lg-12 col-md-12  margin-top-15"> 
                    @if($elementsCategory)	
                        @foreach ($elementsCategory as $key=>$categoryItems)
                        <li  class="list-group" >
                            <div class="list-group-item" >							
                                <div onclick="myFunction (this)" style="width: 100%;text-align: center;">
                                    <i class="fa fa-arrow-down"></i> {{$categoryItems['name']}},
                                    {{count($categoryItems['elements'])}} ел
                                </div> 
                            </div>                       
                    
                            @if(count($categoryItems['elements'])>0)
                                <div class="elements-category-container" data-category="{{$categoryItems['id']}}"  style="display: none">
                                @foreach ($categoryItems['elements'] as $element) 
                               
                                    @if( $element['id'] != $templateMeta[$key]['element'])
                                    
                                        <div class="template-elements-small" >
                                            <div  class="elements-category-img">							
                                                <img src="{{$element['thumbnail']}}">
                                            </div>
                                            <div class="elements-category-name">							
                                                <h4 >{{$element['name']}}                       
                                                    <label class="switch">
                                                        <input type="checkbox" data-category="{{$categoryItems['id']}}" data-element="{{$element['id']}}" onchange="addMeta(this)"><span class="slider round"></span>
                                                    </label>
                                                </h4>    
                                            </div>
                                             <a  href="{{ $element['url']}}" class="button gray" target="_blank">Demo</a>

                                        </div>  
                                    @endif                                                            
                                                     
                                @endforeach
                                </div>
                            @endif
                            </li>
                        @endforeach
                    @else 
                        <div>Немає</div>  
                    @endif
                </div>   
                             
            </div>
           
        </div>
        
    </div>


</div>



<!-- Modals -->
<div class="modal fade" id="previewModal" role="dialog">
    
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Image Library</h4>
            </div>
            <div class="modal-body">               

                <div class="modal-body-content style-2">

                    <ul class="tabs-nav">
                        <li class="active"><a href="#myImagesTab">Зображення</a></li>
                        <li id="uploadTabLI"><a href="#uploadTab">Завантажити нове</a></li>                       
                    </ul> <!-- /tabs -->

                    <div class="tabs-containe">

                        <div class="tab-content  active" id="myImagesTab">                            
                            <!-- Alert Info -->
                            <div >                                
                               <img src="" id="modal-img">
                            </div>
                          

                        </div><!-- /.tab-pane -->

                        <div class="tab-content" id="uploadTab">

                            <form id="imageUploadForm" action="{{ route('image.upload.template') }}" method="POST" enctype="multipart/form-data" >
                                @csrf
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <input type="hidden" name="loadElementID" id="loadElementID" required>
                                    <input type="file" name="imageFile" id="imgInp" required>
                                    <img id="blah" src="" alt="your image" />
                                </div>
                                <hr>

                                <button type="submit"  class="btn btn-primary btn-embossed btn-wide upload btn-block">Завантажити</button>
                            </form>  

                        </div><!-- /.tab-pane -->

                    </div> <!-- /tab-content -->

                </div>
            </div>
            <div class="modal-footer">   
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
        
    </div>
</div>
<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>
<script> 

    function changeType (element, id) {
        var templateID = element.dataset.id 
        var categoryID = element.dataset.category 
        var elementID = element.dataset.element 
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/template-element-type',
			data: {type: element.value, templateID: templateID, categoryID: categoryID, elementID: elementID},		   
            success:function(response) { 
                console.log(response);
            }
		})
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
        }

        $("#imgInp").change(function() {
        readURL(this);
    });

    function img_modal(src, id) {		
		console.log(src);
        document.getElementById('modal-img').src= src
        document.getElementById('loadElementID').value= id
    }

    function changeName (element) {
        var elementID = element.dataset.id 
        var elementName = element.innerHTML;
        console.log(elementID);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
		    type: 'POST', 
		    url:  window.location.origin+'/template-update',
			data: {elementID: elementID, elementName: elementName},		   
            success:function(response) { 
                console.log(response);
            }
		})
    }

    function addMeta (element) { 
        var meta_value = element.dataset.element;
        var meta_key = element.dataset.category;
        var template_id = document.getElementById('template').value
       
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/ajaxMeta-add",
            type: 'post',                          
            data: {template_id: template_id, meta_key: meta_key, meta_value: meta_value},
            error:function(response) { 
                 console.log(response);	
            },
            success:function(response) { 
                 console.log(response);	
                document.location.reload();
            }            
        });
    }

    function myFunction (element) {
        var list = element.parentElement;
        var parent =list.parentElement;
        var child =parent.lastElementChild;
        
        display = child.style.display
        if (display === 'none') {
            child.style.display ='flex';
        }
        else {
            child.style.display ='none'; 
        }  	    		
	}
    function deleteMeta (element) {
        var meta_value = element.dataset.element;
        var meta_key = element.dataset.category;
        var template_id = document.getElementById('template').value
        var parentCategory = element.parentElement.parentElement.parentElement.parentElement
        var parentCategoryID = parentCategory.dataset.category
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/ajaxMeta-delete",
            type: 'post',                          
            data: {template_id: template_id, meta_value: meta_value, meta_key: meta_key},
            success:function(response) { 
                
                document.location.reload();
            }            
        });
       
    }

    function publishSitePost() {
        $.ajax({
            url: "/en/site/publish",
            type: 'post',                          
            data: {template_id: $('form#publishForm input[name=site_id]').val()},
            dataType: 'json',
            
        });
    }
       
</script>
@endsection
