@extends('layouts.main')

@section('content')

    {{-- <div id="titlebar" class="margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{Lang::get('messages.register')}}</h2>

                </div>
            </div>
        </div>
    </div> --}}
    <div class="container">


        <div class="row margin-top-75 full-height">
            <!-- Contact Form -->
            <div class="col-md-8 col-md-offset-2">
                <div class="row " >
                    <div class="col-md-8 col-md-offset-2">
                        <div class="btns-wrapper">
                            <a class=" button btn btn-lg btn-primary btn-block" href="{{ route('auth.google', app()->getLocale()) }}">
                                <i class="fa fa-google"></i>Вхід через  Google
                            </a>
                            <a href="{{ route('facebook.redirect', app()->getLocale()) }}" class="button btn btn-primary">
                                <i class="fa fa-facebook"></i>Вхід через Facebook
                            </a>
                        </div>
                    </div>
                </div>
                
                <hr class="margin-bottom-35 margin-top-35">

                <form  method="POST" action="{{ route('register', app()->getLocale()) }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="email" class="col-md-6 col-form-label text-md-right">E-Mail</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                    </div>
                    <div class="row">
                        <div class="register-wrapper">
                            <button type="submit" class="btn btn-primary register-btn-style">
                                {{Lang::get('messages.registers')}}
                            </button> 
                        </div>
                    </div>
                </form>

            </div>
            <!-- Contact Form / End -->

        </div>   

@endsection

