
@extends('layouts.new-dashboard')
@section('content')

    {{-- <div id="titlebar" class="margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{Lang::get('messages.login')}}</h2>

                </div>
            </div>
        </div>
    </div> --}}
    <div class="container">


        <div class="row margin-top-75  full-height">
            <!-- Contact Form -->
            <div class="col-md-8 col-md-offset-2">
                <hr class="margin-bottom-35 margin-top-35">
                <form  action="{{ route('login', app()->getLocale()) }}"  method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{Lang::get('auth.failed')}}</strong>
                                </span>
                            @enderror

                            <label for="password" class="col-md-4 col-form-label text-md-right">{{Lang::get('messages.password')}}</label>


                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{Lang::get('auth.failed')}}</strong>
                                </span>
                            @enderror

                        </div>
                    </div>

                     <div class="row">

                        <div class="login-wrapper">
                            <button type="submit" class="button login-btn">
                            {{Lang::get('messages.login')}}
                            </button>
                            @if (Route::has('password.request', app()->getLocale()))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                        </div>

                    </div>

                </form>
            </div>
        </div>



    </div>

</div>

@endsection

{{-- <div class="row margin-bottom-75">
    <div class="col-md-8 col-md-offset-2">
        <div class="row text-aligen-center">
            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-6 padding-bottom-50 padding-top-50 text-aligen-center">
                    <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/google') }}">
                        <i class="fa fa-google"></i> Google
                    </a>
                </div>
                <div class="col-md-6 padding-bottom-50 padding-top-50">

                    <a href="{{ route('facebook.redirect') }}" class="btn btn-primary"><i class="fa fa-facebook"></i> Facebook</a>
                </div>
            </div>
        </div>
    </div>
</div> --}}
