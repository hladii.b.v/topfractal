<?php

return [
    'name' => 'Public offer',
    'description' => 'PUBLIC AGREEMENT (OFFER)
                    to order, manufacture, sell and deliver goods
                    This agreement between Fedorchak Olga Ihorivna, who provides "Seller" to members of the official website, in the company "Customer", offers a contract to order, uses and delivers the goods, which is the main order, and offers delivery via the website ladyandbeau. com.ua Check, using the necessary offer of the goods, adhere to this contract of purchase and sale of the goods (further - the Contract) on the following pages.
                     
  1 Definition of terms  
  1.1. Public offer (hereinafter - the "Offer") - the public offer of the Seller, addressed to an indefinite circle, use the trade offer of purchase and sale of goods, which is in the section (hereinafter - "Agreement"), and please stay in the relevant Offer, including all applications.  
  1.2. Order - the Customer\'s decision to order the goods and their delivery, published in online stores and / or orders for the production and delivery of goods.
 
  2 GENERAL PROVISIONS  
  2.1. The following information is an official offer (offer) of the online store ladyandbeau.com.ua to any individual (hereinafter - the Buyer) to enter into a contract of sale of goods. This agreement is public, ie. E. According to Article 633 of the Civil Code of Ukraine, its conditions are the same for all buyers.  
  2.2. According to Article 642 of the Civil Code of Ukraine, the full and unconditional acceptance of the terms of this offer (offer), which confirms the conclusion of the Contract of sale on the following terms, is the fact of registration and confirmation of the order.  
  2.4. By placing an Order, the Buyer confirms the agreement and unconditional acceptance of the terms of this offer (offer).  
  2.5. By concluding the Agreement (ie accepting the terms of this Offer (Offered Opportunities) by placing an Order), the Buyer confirms the following:  
  a) The buyer is fully acquainted with and agrees to the terms of this offer (offer);  
b) he gives permission for the collection, processing and transfer of personal data under the conditions specified below in the Reserve on the collection, processing and transfer of personal data permission for personal data processing is valid for the entire term of the Agreement and for an indefinite period after his actions. In addition, by concluding the Agreement, the Customer confirms that he is notified (without additional notice) of the rights established by the Law of Ukraine "On Personal Data Protection", the purposes of data collection, and that his personal data is transferred to the Seller to meet the conditions of this Agreement, the possibility of mutual settlements, as well as to obtain invoices, acts and other documents. The Customer also agrees that the Seller has the right to provide access and transfer his personal data to third parties without any additional notice to the Customer, without changing the purpose of personal data processing. The scope of the Customer\'s rights as a subject of personal data in accordance with the Law of Ukraine "On Personal Data Protection" is known and understood.
 
  3 PRICE OF GOODS  
  3.1. The price for each item of the Goods is indicated on the website of the Online Store.  
  3.2. The Seller has the right to unilaterally change the price for any item of the Goods.  
  3.3. In the event of a change in the price of the ordered Goods, the Seller undertakes to inform the Buyer about the change in the price of the Goods.  
  3.4. The Buyer has the right to confirm or cancel the Order for the purchase of the Goods, if the price is changed by the Seller after placing the Order.  
  3.5. The Seller is not allowed to change the price for the Goods paid by the Buyer.  
  3.6. The Seller indicates the cost of delivery of the Goods on the website of the Online Store or notifies the Buyer when placing an order by the Operator.  
  3.7. The Buyer\'s obligations to pay for the Goods shall be deemed fulfilled from the moment the Seller receives the funds.  
  3.8. Payments between the Seller and the Buyer for the Goods are made in the ways specified on the website of the Online Store in the section "SHIPPING AND PAYMENT PAYMENT".
 
  4 ORDERING  
  4.1. The Goods are ordered by the Buyer through the service of the ladyandbeau.com.ua Online Store website  
  4.2. When registering on the website of the Online Store, the Buyer undertakes to provide the following registration information:  
  4.2.1. last name, first name, patronymic of the Buyer or the person (recipient) specified by him;  
  4.2.2. the address to which the Goods are to be delivered (if delivery to the Buyer\'s address);  
  4.2.3. Email address;  
  4.2.4. contact phone.  
  4.3. The name, quantity, article, price of the Goods selected by the Buyer are indicated in the Buyer\'s basket on the website of the Online Store.  
  4.4. If the Seller needs additional information, he has the right to request it from the Buyer. In case of failure to provide the necessary information by the Buyer, the Seller is not responsible for providing quality service to the Buyer when buying goods on the Internet market.  
  4.5. When placing an Order through the Operator (clause 4.1. Of this Offer), the Buyer undertakes to provide the information specified in clause 4.2. of this Offer.  
  4.6. The Buyer\'s approval of the terms of this Offer is made by the Buyer entering the relevant data in the registration form on the website of the Online Store or when placing an Order through the Operator. After placing an Order through the Operator, the Buyer\'s data is registered in the Seller\'s database.  
  4.7. The Buyer is responsible for the accuracy of the information provided when placing the Order.  
  4.8. The contract of sale by remote means between the Seller and the Buyer is considered concluded from the moment of electronic ordering on the website of the online store or issuance by the Seller to the Buyer of a cash or commodity check or other document confirming payment for the Goods.
 
  5 DELIVERY AND DELIVERY OF GOODS TO THE BUYER  
  5.1. Methods, procedure and terms of delivery of goods are specified on the website in the section "DELIVERY". The order and conditions of delivery of the ordered goods the Buyer agrees with the operator of the online store at the time of purchase.  
  5.2. Delivery of the goods is carried out by own employees of employees of the Internet market according to conditions of delivery, or with involvement of the third parties (carrier).  
  5.3. Upon receipt of the goods, the Customer must, in the presence of a courier representative, check the conformity of the Goods to the qualitative and quantitative characteristics (name of the goods, quantity, completeness, expiration date).  
  5.4. The Customer or the Customer\'s Representative during the acceptance of the goods confirms with his signature in the sales receipt and / or order for delivery of goods, which has no claims to the quantity of goods, appearance and completeness of goods.
 
  6 RETURN OF GOODS  
  6.1. The customer has the right to refuse non-excisable goods at any time before their transfer, and after the transfer of non-excisable goods - in the manner and under the conditions specified by the Law of Ukraine "On Consumer Protection".  
  6.2. Return of non-excisable goods of proper quality is possible if its appearance, consumer properties, as well as a document confirming the purchase and conditions of the order of the goods are preserved.  
  6.3. The Customer has no right to refuse the goods of proper quality, which has individually defined properties, if the specified goods can be used only by the Consumer who purchased it (including non-standard, at the request of the Customer, sizes, etc.). Confirmation that the product has individually defined properties is the difference between the size of the product and other characteristics specified in the online store.  
  6.4. Return of goods, in cases provided by law and this Agreement, is carried out at the address specified on the site in the section "CONTACTS".  
  6.5. If the Buyer refuses the non-excisable goods of proper quality, the Seller shall refund the amount of the value of such Goods, except for the costs of the seller for the delivery of the returned goods.  
  6.6. Return of the amount specified in clause 6.5. carried out simultaneously with the return of the goods.
 
  7 RESPONSIBILITY OF THE PARTIES  
  7.1. The Seller shall not be liable for any damage caused to the Buyer as a result of improper use of the Goods pre-ordered within the Ladyandbeau.com.ua Service and purchased from the Seller.  
  7.2. The Seller shall not be liable for improper, untimely fulfillment of the Orders and its obligations in the event that the Buyer provides inaccurate or erroneous information.  
  7.3. The Seller and the Buyer are responsible for fulfilling their obligations in accordance with the current legislation of Ukraine and the provisions of this Agreement.  
  7.4. The Seller or the Buyer shall be released from liability for full or partial non-performance of its obligations, if the non-performance is the result of force majeure such as war or hostilities, earthquake, flood, fire and other natural disasters that occurred regardless of the Seller\'s will and / or Buyer after concluding this agreement. A Party that is unable to fulfill its obligations shall immediately notify the other Party.  '
];
