<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrameMeta extends Model
{  
   
    public function frame()
    {
    	return $this->belongsTo('App\Frame');
    }

    public function getHistoryAttribute()
    {
        return unserialize($this->meta_value);
    }
    
    public function setHistoryAttribute($value)
    {
        $this->attributes['meta_value'] = serialize($value);
    }
}
