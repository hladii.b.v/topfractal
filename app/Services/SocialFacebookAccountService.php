<?php

namespace App\Services;
use App\SocialFacebookAccount;
use App\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                 $password = '';

                $desired_length = rand(8, 12);

                for($length = 0; $length < $desired_length; $length++) {
                    $password .= chr(rand(32, 126));
                }
                if ($providerUser->getEmail() == null) {
                    $user_mail = 'newMail'.time().'@gmail.com';
                }
                else {
                    $user_mail = $providerUser->getEmail() ;
                }
                $to_name = $user_mail;
                $to_email = $user_mail;
                $data_mail = array('name'=>$to_name, 'body' => 'Ваш пароль ' . $password);
                Mail::send('auth/email/mail', $data_mail, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                            ->subject('Створення нового конристувач');
                    $message->from( env('MAIL_USERNAME', 'hladii.b.v@gmail.com'), 'Створення нового конристувача');
                });

                $user = User::create([
                    'role_id' => 1,
                    'email' => $user_mail,
                    'name' => $providerUser->getName(),
                    'password' => Hash::make($password),
                    'type' => 'user',
                    'active' => 1

                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }

}

