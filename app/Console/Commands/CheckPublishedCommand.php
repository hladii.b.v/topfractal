<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Site;
use Carbon\Carbon;
use Illuminate\Support\Facades\File as FacadesFile;
use App\Utils;


class CheckPublishedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sites:checkPublished';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command check published date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sites = Site::whereDate('publish_date_end', '<', Carbon::now()->endOfDay())->where('ftp_published', '=', 1)->get();
        foreach ($sites as $site) {
        	$site->ftp_published = 2;
        	$site->save();
        	$site_name = $this->getDomainName($site);
        	$domain = $site_name . '.dent.eco';
			FacadesFile::put('/etc/apache2/sites-enabled/' . $domain . '.conf', $this->getDomainConfText($domain, 'dent/public'));
			FacadesFile::put('/etc/apache2/sites-available/' . $domain . '.conf', $this->getDomainConfText($domain, 'dent/public'));
        }

        exec('/usr/bin/sudo /etc/init.d/apache2 restart');
		exec('/usr/bin/sudo /etc/init.d/apache2 reload');
        
    }

    private function getDomainName($site)
	{
		$domainName = Utils::transliterate($site->site_name);
		$sites = Site::where('site_name', '=', $site->site_name)->get();
		if($sites->count() > 1) {
			$domainName .= $site->id;
		}

		return $domainName;

	}

public function getDomainConfText($domain, $folder)
	{
		return "
			<VirtualHost *:80>
				ServerAdmin admin@178.62.41.211
				ServerName {$domain}
				ServerAlias {$domain}
				DocumentRoot /var/www/{$folder}

				<Directory /var/www/{$folder}/>
					Options Indexes FollowSymLinks MultiViews
					AllowOverride All
					Order allow,deny
					allow from all
					Require all granted
				</Directory>

				ErrorLog \${APACHE_LOG_DIR}/error.log
        CustomLog \${APACHE_LOG_DIR}/access.log combined

			</VirtualHost>
		";
	}

}
