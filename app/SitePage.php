<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SitePage extends Model
{
    use Notifiable;     
	 
    public static function content($page_name, $lang)
    {   
        return SitePage::where('page_name', $page_name)->where('lang', $lang)->get();
    }
}
