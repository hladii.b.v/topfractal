<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Options;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Filesystem\Filesystem;

use App\Site;
use App\Page;
use App\Element;
use App\Frame;
use App\FrameMeta;
use App\Template;
use App\Category;
use App\Paremeter;
use App\Benefit;
use File;
use App\Direction;
use App\Payment;
use App\Tariff;
use App\Service;
use App\Utils;
use App\Setting;
use App\Layouts;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;


class SiteController extends Controller
{
     /**
     * @OA\get(
     *      path="/directions",
     *      operationId="Directions",
     *      tags={"Site"},
     *      description="Get directions",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    // отримання всіх напрямків
    public function direction(Response $response)
    {
        $directions = Direction::all(['id', 'name']);
        if ($directions) {

            return [
                'status'=>  $response->status(),
                'directions' => $directions
            ];
        }
        else {
            return $response->setStatusCode(404, 'No directions');
        }
    }

     /**
     * @OA\get(
     *      path="/direction/{direction_id}",
     *      operationId="Direction",
     *      tags={"Site"},
     *      description="Get direction templates",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    //  отримання всіх шаблонів для напрямку
    public function directionTemplates(Response $response, Direction $direction)
    {

        if ($direction) {
            $directionTemplates =  $direction->templates;
            if (count($directionTemplates)>0) {
                foreach ($directionTemplates as $directionTemplate) {
                    $obj_child[] = [
                        'id' => $directionTemplate->id,
                        'name' => $directionTemplate->name,
                        'img' => $directionTemplate->img,
                        'demo' => $directionTemplate->demo
                    ];
                }
                return [
                    'status'=>  $response->status(),
                    'directionId'=> $direction->id,
                    'directionName'=>  $direction->name,
                    'templates' => $obj_child
                ];
            }
            else {
                return $response->setStatusCode(404, 'No templates');
            }

        }

    }

     /**
     * @OA\get(
     *      path="/template/{template_id}/{lang}",
     *      operationId="Template",
     *      tags={"Site"},
     *      description="Get template elements ",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    //  отримання всіх eлементів шаблону по категоріям з параметрами
    public function template(Response $response, Template $template, $lang)
    {
        if ($template) {
            
            $userID = Auth::user()->id;
            $metaElenents =  $template->metaElenents;
           
            $templateElement = [];
            $elements = [];
            $direction = $template->direction;
            foreach ($metaElenents as $meta) {
                $elements = [];
                if ( $meta->meta_value != Null ) {
                    $category = Category::where('id',  $meta->meta_key)->first();

                    $elements = Element::
                        where('id',  $meta->meta_value)
                         ->where('show',  1)
                        ->first(['id','name', 'url', 'thumbnail']);
                    if(!$elements) {
                        continue;
                    }

                    $element = [
                        'id' => $elements->id,
                        'name' => $elements->name,
                        'url'  => $elements->url,
                        'thumbnail'  => $elements->thumbnail,
                        'link' => $category->link,
                        'type' => $meta->type
                    ];

                    $categoryParameters = [];
                    $categoryParameter = [];
                    foreach ($category->getHistoryAttribute() as $paremeter) {
                        $categoryParameter = Paremeter::where('id', $paremeter)->where('lang', $lang)->first();

                        $random_quote = Benefit::where('type', $categoryParameter->key)
                            ->where('lang', $categoryParameter->lang)
                            ->where('direction_id', $direction->id)
                            ->inRandomOrder()->first();

                        $paramValue = null;

                        if ($random_quote) {
                            $paramValue = $random_quote->desc;
                        }

                        if ($categoryParameter->key == 'social') {
                            $paramValue = [
                                [
                                    'value' => '',
                                    'img' => 'elements/images/icons/viber.svg',
                                    'name' => 'Viber',
                                    'placeholder' => 'viber://chat?number=380967146755',
                                ],
                               
                                [
                                    'value' => '',
                                    'img' => 'elements/images/icons/telegram.svg',
                                    'name' => 'Telegram',
                                    'placeholder' => 'https://t.me/username',
                                ],
                                 [
                                    'value' => '',
                                    'img' => 'elements/images/icons/whatsapp.svg',
                                    'name' => 'Whatsapp',
                                    'placeholder' => 'https://wa.me/380956987456',
                                ],
                                 [
                                    'value' => '',
                                    'img' => 'elements/images/icons/instagram.svg',
                                    'name' => 'Instagram',
                                    'placeholder' => 'https://www.instagram.com/uk.visa.service',
                                ],
                                 [
                                    'value' => '',
                                    'img' => 'elements/images/icons/facebook.svg',
                                    'name' => 'Facebook',
                                    'placeholder' => 'https://www.facebook.com/username',
                                ],
                            ];
                        }

                        if ($categoryParameter->key == 'benefitList') {

                            $data =Benefit::
                                where('parent_id', null)
                                ->where('type', 'benefit')
                                ->where('lang', $lang)
                                ->get();

                            $getBenefits = [];

                            foreach ($data as $key=>$value) {
                                $benefit = Benefit::find($value->id);

                                $getBenefits[] = [
                                    'id' => $value->id,
                                    'type'  => $value->type,
                                    'img'  => 'elements/images/benefits/'. $value->img,
                                    'desc' => $value->desc,
                                    'children' => $benefit->children( $userID, $key)
                                ];

                            }
                            $paramValue = $getBenefits;
                        }

                        if ($categoryParameter->key == 'servicesList') {

                            $data =Benefit::
                                where('parent_id', null)
                                ->where('type', 'services')
                                ->where('lang', $lang)
                                ->get();

                            $getBenefits = [];

                            foreach ($data as  $key=>$value) {
                                $benefit = Benefit::find($value->id);
                                $benefitStatus = false;

                                if ( $key < 4) {
                                    $benefitStatus = true;
                                }

                                $getBenefits[] = [
                                    'id' => $value->id,
                                    'type'  => $value->type,
                                    'desc' => $value->desc,
                                    'selected' =>  $benefitStatus,
                                    'children' => $benefit->children( $userID, $key)
                                ];

                            }
                            $paramValue = $getBenefits;
                        }

                        if ($categoryParameter->key == 'section_name') {
                            $paramValue = $category->name;
                        }

                        if ($categoryParameter->key == 'reviews') {
                            $paramValue = [
                                0 => [
                                    'name' => 'Іван',
                                    'value' => 'Гарна стоматологія',
                                ]
                            ];
                        }

                        // if ($categoryParameter->key == 'teams') {
                        //     $paramValue = [
                        //         0 => [
                        //             'name' => 'Іван Доктор',
                        //             'foto' => 'elements/images/img_8276.jpg',
                        //             'value' => 'Терапевт',
                        //         ]
                        //     ];
                        // }

                        if ($categoryParameter->key == 'imgList') {
                            $paramValue = [];
                        }

                        if ($categoryParameter->key == 'about') {

                            $folder = 'elements/images/' . $categoryParameter->key.'/'. $direction->id;
                            $folderContent= File::allFiles($folder);
                            $imgRand =  array_rand( $folderContent);
                            $file =  $folderContent[$imgRand];
                            $filename = $folder . '/' . $file->getRelativePathname();
                            if ($folderContent) {
                                $paramValue = $filename;
                            }
                        }

                        if ($categoryParameter->key == 'header_logo') {

                            $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_logo')->first();

                            if ($getImg) {
                                $paramValue = $getImg->meta_value;
                            }
                        }

                        if ($categoryParameter->key == 'header_img') {

                            $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_img')->first();

                            if ($getImg) {
                                $paramValue = $getImg->meta_value;
                            }
                        }

                        if ($categoryParameter->key == 'color1') {

                            $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'head_color')->first();

                            if ($getImg) {
                                $paramValue = $getImg->meta_value;
                            }
                        }

                        if ($categoryParameter->key == 'color2') {

                            $getImg = Layouts::where('layouts_id', $template->id)->where('meta_key', 'title_color')->first();

                            if ($getImg) {
                                $paramValue = $getImg->meta_value;
                            }
                        }

                        if ($categoryParameter->key == 'color3') {

                            $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'text_color')->first();

                            if ($getImg) {
                                $paramValue = $getImg->meta_value;
                            }
                        }

                        if ($categoryParameter->key == 'about') {

                            $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'about')->first();

                            if ($getImg) {
                                $paramValue = $getImg->meta_value;
                            }
                        }
                        if ($categoryParameter->key=='teams' ) {
                        
                            $getParam = Layouts::where('layouts_id',$template->id)->where('meta_key', 'teams')->first();

                            if ($getParam) {
                                $paramValue = unserialize($getParam->meta_value);
                            }
                        }
                                   


                        $categoryParameters[] = [
                            'id' => $categoryParameter->id,
                            'key' => $categoryParameter->key,
                            'type' => $categoryParameter->type,
                            'name' => $categoryParameter->name,
                            'lang' => $categoryParameter->lang,
                            'value' => $paramValue,
                        ];
                    }

                    $templateElement[] = [
                        'categoryID' =>$category->id,
                        'categoryName' =>$category->name,
                        'categoryParameters' =>$categoryParameters,
                        'element' => $element
                    ];
                }

            }
            if (count($templateElement)>0) {
                return [
                    'status'=> $response->status(),
                    'templateId'=> $template->id,
                    'templateName'=>  $template->name,
                    'elements' => $templateElement
                ];
            }
            else {
                return $response->setStatusCode(404, 'No templates');
            }
        }

    }

     /**
     * @OA\get(
     *      path="/site-data/{site_id}/{lang}",
     *      operationId="getSiteData",
     *      tags={"Site"},
     *      description="Get site data for eding",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    //  отримання данних для редагування сайту по id
    public function getSiteData(Response $response, Site $site, $lang)
    {

        if ($site->user_id != Auth::user()->id && Auth::user()->type != 'admin' && $site->site_trashed == 1) {
            return $response->setStatusCode(404, 'Wrong site_id');
        }
        if ( $site->site_trashed == 1) {
            return $response->setStatusCode(404, 'Wrong site_id');
        }
        if ($site) {
            $page = Page::where('site_id', $site->id)->first();
            $template = Template::where('id', $page->css)->first();
          
            $userID = Auth::user()->id;
            if ($template) {
                $metaElenents =  $template->metaElenents;
                $templateElement = [];
               
                $direction = $template->direction;           
                
                foreach ($metaElenents as $key=>$meta) {
                    
                    $elements = [];
                    if ( $meta->meta_value != Null ) {
                        $category = Category::where('id',  $meta->meta_key)->first();
                        $selected = 'required';
                        $elements = Element::
                              where('id',  $meta->meta_value)
                            ->where('show',  1)
                            ->first(['id','name', 'url', 'thumbnail', ]);
                        $allSelected =  Frame::where('site_id', $site->id)
                                ->where('element_id', $elements->id)
                                ->where('revision', 0)
                                ->first();

                        if($allSelected ){  
                            $selected =  'selected';
                            if ($key==0 || $key+1 == count($metaElenents)){
                                $selected =  'required';
                            }                           
                          
                        }
                        else {
                          
                            $selected =  null;
                             
                            foreach ($category->elements as $el) {

                                $selectedNoTemplate =  Frame::where('site_id', $site->id)
                                    ->where('element_id', $el->id)
                                    ->where('revision', 0)
                                    ->first();
                                if($selectedNoTemplate ) {  
                                    $selected =  'selected';
                                    $elements =  $el;
                                    $allSelected =  Frame::where('site_id', $site->id)
                                        ->where('element_id', $el->id)
                                        ->where('revision', 0)
                                        ->first();
                                    break;
                                }
                            }
                            
                        }                        
                        

                       
                        $categoryParameters = [];
                        $categoryParameter = [];
                        foreach ($category->getHistoryAttribute() as $paremeter) {
                            $categoryParameter = Paremeter::where('id', $paremeter)->where('lang', $lang)->first();
                            $paramValue = null;

                            if ($selected != Null) {
                                $frameMeta = FrameMeta::where('meta_key', $categoryParameter->key)
                                    ->where('frame_id', $allSelected->id)
                                    ->where('site_id', $site->id)
                                    ->where('revision', 0)
                                    ->first();
                                if ($frameMeta == Null) {
                                    continue;
                                }
                                $paramValue = $frameMeta->getHistoryAttribute();

                                if ($categoryParameter->key == 'benefitList') {

                                    $benefitSelected = [];

                                    foreach ($paramValue as $value) {
                                        $benefitSelected[] = $value['id'];
                                    }
        
        
                                    $data =Benefit::
                                        where('parent_id', null)
                                        ->where('type', 'benefit')
                                        ->where('lang', $lang)
                                        ->get();
        
                                    $getBenefits = [];
        
                                    foreach ($data as $value) {
                                        $benefit = Benefit::find($value->id);
        
                                        $getBenefits[] = [
                                            'id' => $value->id,
                                            'type'  => $value->type,
                                            'img'  => 'elements/images/benefits/'. $value->img,
                                            'desc' => $value->desc,
                                            'children' => $benefit->childrenSelected( $userID, $benefitSelected)
                                        ];
        
                                    }
                                    $paramValue = $getBenefits;
        
                                }

                                if ($categoryParameter->key == 'servicesList') {

                                    $servicesSelected = [];
                                    $servicesChildrenSelected = [];
                                    foreach ($paramValue as $value) {
                                        $servicesSelected[] = $value['id'];
                                        if (count( $value['children'])>0) {
                                            foreach ($value['children'] as $children) {

                                                if (array_key_exists ('price' , $children )) {
                                                    $servicesChildrenSelected[] = [
                                                        'id' => $children['id'],
                                                        'price' => $children['price']
                                                    ];
                                                }
                                                else {
                                                    $servicesChildrenSelected[] = [
                                                        'id' => $children['id'],
                                                        'price' => null
                                                    ];
                                                }
                                            }
                                        }
                                    }

                                    $data =Benefit::
                                        where('parent_id', null)
                                        ->where('type', 'services')
                                        ->where('lang', $lang)
                                        ->get();

                                    $getServices = [];
                                    // return $servicesChildrenSelected;
                                    foreach ($data as $value) {
                                        $benefit = Benefit::find($value->id);
                                        $servicestStatus = false;

                                        if (in_array (  $value->id, $servicesSelected) ) {
                                            $servicestStatus = true;
                                        }
                                        $getServices[] = [
                                            'id' => $value->id,
                                            'type'  => $value->type,
                                            'img'  => 'elements/images/benefits/'. $value->img,
                                            'desc' => $value->desc,
                                            'selected' =>  $servicestStatus,
                                            'children' => $benefit->childrenServicesSelected( $userID, $servicesChildrenSelected)
                                        ];

                                    }
                                    $paramValue = $getServices;

                                }


                            }
                            else {
                                
                                $random_quote = Benefit::where('type', $categoryParameter->key)
                                    ->where('lang', $categoryParameter->lang)
                                    ->where('direction_id', $direction->id)
                                    ->inRandomOrder()->first();

                                if ($random_quote) {
                                    $paramValue = $random_quote->desc;
                                }

                                if ($categoryParameter->key == 'benefitList') {

                                    $data =Benefit::
                                        where('parent_id', null)
                                        ->where('type', 'benefit')
                                        ->where('lang', $lang)
                                        ->get();

                                    $getBenefits = [];

                                    foreach ($data as $key=>$value) {
                                        $benefit = Benefit::find($value->id);

                                        $getBenefits[] = [
                                            'id' => $value->id,
                                            'type'  => $value->type,
                                            'img'  => 'elements/images/benefits/'. $value->img,
                                            'desc' => $value->desc,
                                            'children' => $benefit->children( $userID, $key)
                                        ];

                                    }
                                    $paramValue = $getBenefits;
                                }

                                if ($categoryParameter->key == 'servicesList') {

                                    $data =Benefit::
                                        where('parent_id', null)
                                        ->where('type', 'services')
                                        ->where('lang', $lang)
                                        ->get();

                                    $getBenefits = [];

                                    foreach ($data as  $key=>$value) {
                                        $benefit = Benefit::find($value->id);
                                        if ( $key < 4) {
                                            $benefitStatus = true;
                                        }
                                        $getBenefits[] = [
                                            'id' => $value->id,
                                            'type'  => $value->type,
                                            'desc' => $value->desc,
                                            'selected' =>  $benefitStatus,
                                            'children' => $benefit->children( $userID, $key)
                                        ];

                                    }
                                    $paramValue = $getBenefits;
                                }

                                if ($categoryParameter->key == 'section_name') {
                                    $paramValue = $category->name;
                                }

                                if ($categoryParameter->key == 'reviews') { 
                                    $paramValue = [
                                        0 => [
                                            'name' => 'Іван',
                                            'value' => 'Гарна стоматологія',
                                        ]
                                    ];
                                }

                                if ($categoryParameter->key == 'teams') {
                                    $paramValue = [
                                        0 => [
                                            'name' => 'Іван Доктор',
                                            'foto' => 'elements/images/img_8276.jpg',
                                            'value' => 'Терапевт',
                                        ]
                                    ];
                                }

                                if ($categoryParameter->key == 'about') {

                                    $folder = 'elements/images/' . $categoryParameter->key.'/'. $direction->id;
                                    $folderContent= File::allFiles($folder);
                                    $imgRand =  array_rand( $folderContent);
                                    $file =  $folderContent[$imgRand];
                                    $filename = $folder . '/' . $file->getRelativePathname();
                                    if ($folderContent) {
                                        $paramValue = $filename;
                                    }
                                }
                            }
                            $categoryParameters[] = [
                                'id' => $categoryParameter->id,
                                'key' => $categoryParameter->key,
                                'type' => $categoryParameter->type,
                                'name' => $categoryParameter->name,
                                'lang' => $categoryParameter->lang,
                                'value' => $paramValue,
                            ];
                            $element = [
                                'id' => $elements->id,
                                'name' => $elements->name,
                                'url'  => $elements->url,
                                'thumbnail'  => $elements->thumbnail,
                                'link' => $category->link,
                                'type' => $selected
                            ];
                        }

                        $templateElement[] = [
                            'categoryID' =>$category->id,
                            'categoryName' =>$category->name,
                            'categoryParameters' =>$categoryParameters,
                            'element' => $element
                        ];
                    }

                }
                if (count($templateElement)>0) {
                    return [
                        'status'=> $response->status(),
                        'templateId'=> $template->id,
                        'templateName'=>  $template->name,
                        'elements' => $templateElement
                    ];
                }
                else {
                    return $response->setStatusCode(404, 'No templates');
                }
            }
        }

    }

      /**
     * @OA\get(
     *      path="/sites",
     *      operationId="getAllSites",
     *      tags={"Site"},
     *      description="Get all sites",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    //  отримання всіх сайтів
    public function getAllSites(Response $response)
    {

        $user_id = Auth::user()->id;
        $sites = Site::where('user_id', $user_id)->where('site_trashed', 0)->orderBy('created_at', 'DESC')->get();
        $allSites = [];
        if (count($sites)>0) {
            foreach ($sites as $site) {
                $page = Page::where('site_id', $site->id)->first();
                $template = Template::where('id', $page->css)->first();
                if ($template) {

                    $paymentID = Payment::where('site_id', $site->id)
                        ->where('tariff_id', '<>', 0)
                        ->where('status', 'done')
                        ->orderBy('end_at', 'desc')
                        ->first('tariff_id');

                    if ($paymentID) {
                        $tariffCurrent = Tariff::where('id', $paymentID->tariff_id)->first(['number']);
                    }
                    else {
                        $tariffCurrent = null;
                    }
                    $allSites[] =   [
                        'site'=> $site,
                        'templateName'=>  $template->name,
                        'tariffCurrent' => $tariffCurrent
                    ];

                }

            }
            return [
                'status'=> $response->status(),
                'sites'=> $sites
            ];
        }
        else {
            return [
                'sites'=> []
            ];
        }
    }

     /**
     * @OA\get(
     *      path="/site/{site_id}/{lang}",
     *      operationId="getSite",
     *      tags={"Site"},
     *      description="Get site data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    //  отримання сайту
    public function getSite(Response $response, Site $site, $lang)
    {

        if ($site->user_id != Auth::user()->id && Auth::user()->type != 'admin') {
            return $response->setStatusCode(404, 'Wrong site_id');
        }
        if ( $site->site_trashed == 1) {
            return $response->setStatusCode(404, 'Wrong site_id');
        }
        if ($site) {
            $page = Page::where('site_id', $site->id)->first();
            $template = Template::where('id', $page->css)->first();
            if ($template) {

                $paymentID = Payment::where('site_id', $site->id)
                    ->where('tariff_id', '<>', 0)
                    ->where('status', 'done')
                    ->orderBy('end_at', 'desc')
                    ->first('tariff_id');
                $tariffArray = [];
                $tariffs = Tariff::groupBy('number')->get('number');
                foreach ($tariffs as $value) {
                    if ($value->number == 1) {
                        $name = 'Базовий';
                    }
                    elseif ($value->number == 2) {
                        $name = 'Преміум';
                    }
                    elseif ($value->number == 3) {
                        $name = 'Преміум Плюс';
                    }

                    $price = Tariff::where('number', $value->number)->where('meta', 'price')->get();
                    $text = Tariff::where('number', $value->number)->where('meta', 'text')->where('lang',  $lang)->get();
                    $tariffArray[] = [
                        'id'=>$value->number,
                        'name' => $name,
                        'month' => [
                            'price'=> $price[0]->value
                        ],
                        'half_year' => [
                            'price'=> $price[1]->value
                        ],
                        'year' => [
                            'price'=> $price[2]->value
                        ],
                        'text'=>$text
                    ];
                }

                if ($paymentID) {
                    $tariffCurrent = Tariff::where('id', $paymentID->tariff_id)->first(['number']);
                }
                else {
                    $tariffCurrent = null;
                }

                $histoty = Payment::where('site_id', $site->id)->where('status', '<>', 'pending')->get();
                $histotyArray = [];
                $serviceArray = [];
                foreach ($histoty as $value) {
                    $serviceArray = [];
                    $tariffNumber = '';
                    $tariff = Tariff::where('id', $value->tariff_id)->first(['number']);
                    if ($tariff ) {
                        $tariffNumber =$tariff->number;
                    }
                    foreach(explode(',', $value->service_id) as $service) {
                        if ($service != '') {
                            $serviceGet= Service::where('id', $service)->first();
                            $serviceArray[] = $serviceGet->name;
                        }
                    }
                    $created_at = Carbon::parse($site->created_at)->addYear()->endOfDay()->format('Y-m-d H:i:s');

                    $histotyArray[]= [
                        'order_id' => $value->order_id,
                        'amount' => $value->amount,
                        'tariff' => $tariffNumber,
                        'services' => $serviceArray,
                        'status' => $value->status,
                        'start' => $created_at,
                        'end_at' => $value->end_at,
                    ];
                }

                $services = Service::where('show', 1)->where('lang', $lang)->get();

                return [
                    'status'=> $response->status(),
                    'site'=> $site,
                    'templateName'=>  $template->name,
                    'services'=>$services,
                    'tariff' => $tariffArray,
                    'tariffCurrent' => $tariffCurrent,
                    'histotyArray' => $histotyArray
                ];
            }
            else {
                return $response->setStatusCode(404, 'No templates');
            }
        }

    }

     /**
     * @OA\get(
     *      path="/category/{category_id}/{template_id}",
     *      operationId="category",
     *      tags={"Site"},
     *      description="Get category data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    //  отримання всіх елементів категорії
    public function category(Response $response, Category $category, Template $template)
    {

        if ($category) {
            if ( $template->id == 9 || $template->id == 15 || $template->id == 16) {
                $elementsTemplates =  $template->metaElenents;
                foreach ($elementsTemplates as $elementsTemplate)  {
                    if ($elementsTemplate->meta_key == $category->id) {
                        $elements[] = Element::where('id', $elementsTemplate->meta_value)->first();
                    }
                }
                if(count($elements)>0) {
               
                    $elementsArray = [];
                    foreach ($elements as $element) {
                        
                       
    
                        if($element->show != 0 ) {
                            $elementsArray[] = [
                                'id' => $element->id,
                                'name' => $element->name,
                                'url'  => $element->url,
                                'thumbnail'  => $element->thumbnail
                            ];
                        }
                    }
                    return [
                        'status'=> $response->status(),
                        'categoryId'=> $category->id,
                        'templateName'=>  $category->name,
                        'elements' => $elementsArray
                    ];
                }
                else {
                    return $response->setStatusCode(404, 'No elements');
                }
            }
            else {
                
                $elements = $category->elements;

                if(count($elements)>0) {
               
                    $elementsArray = [];
                    foreach ($elements as $element) {
                        $subNames = explode('/', $element->url);
                        $nameFrame = strtok(array_pop($subNames), '.');
                        $nameFrame = explode('_', $nameFrame);
                        $nameFrame = $nameFrame[0];
                       
    
                        if($element->show != 0 && $nameFrame != 'carat') {
                            $elementsArray[] = [
                                'id' => $element->id,
                                'name' => $element->name,
                                'url'  => $element->url,
                                'thumbnail'  => $element->thumbnail
                            ];
                        }
                    }
                    return [
                        'status'=> $response->status(),
                        'categoryId'=> $category->id,
                        'templateName'=>  $category->name,
                        'elements' => $elementsArray
                    ];
                }
                else {
                    return $response->setStatusCode(404, 'No elements');
                }
                
            }          

           

        }
    }
     /**
     * @OA\Post(
     *      path="/site-create",
     *      operationId="CreateSite",
     *      tags={"Site"},
     *      description="Create site",
     *      @OA\RequestBody(
        *    required=true,
        *
        *    @OA\JsonContent(
        *       required={"organizationName","templateId","elements","logo"},
        *       @OA\Property(property="organizationName", type="string",  example="Dent"),
        *       @OA\Property(property="templateId", type="inteGet", example="9"),
        *       @OA\Property(property="elements", type="array", collectionFormat="multi",
    *              @OA\Items(
    *                 type="object",
    *                 example={"{name: Про клініку}","{url: elements/template_1/carat_2.html}"},
    *              )),
    *           @OA\Property(property="logo", type="string", example="http://panel.topfractal.com/elements/images/header_logo/2/ppp.png"),
        *    ),
    * ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    //  створення сайту
    public function createSite (Response $response, Request $request) {

        $user = Auth::user();
        // if (count($user->sites)>=2) {

        //     return $response->setStatusCode(402 , 'Sites limit');
        // }
      

        if ( $request->organizationName  && $request->templateId && $request->elements ) {

            $request->validate([
                'organizationName' => 'required|string|max:255',
                'templateId' => 'required|numeric|max:255',
                'elements' => 'required|array|max:255|nullable',               
            ]);


            $site = new Site();
            $site->user_id = Auth::user()->id;
            $site->site_name = $request->organizationName;
            $site->logo = $request->elements[0]['parameters']['header_logo'];
            $site->site_trashed = 0;
            $site->save();

            $page = new Page();
            $page->site_id = $site->id;
            $page->name = 'index';
            $page->css = $request->templateId;
            $page->save();

            if ($request->elements != []) {
                $blocks =$request->elements;
                $blocks[0]['parameters']['organizationName'] = $request->organizationName;
                foreach ($blocks as $block) {

                    $content = $this->getFrameContentEdit($block['id'], $block['parameters'], $request->templateId, $blocks);
                    $template_frame = new Frame();
                    $template_frame->page_id = $page->id;
                    $template_frame->site_id = $site->id;
                    $template_frame->content = $content;
                    $template_frame->original_url = $block['url'];
                    $template_frame->element_id =  $block['id'];
                    $template_frame->revision = 0;
                    $template_frame->save();

                    foreach ( $block['parameters'] as $key=>$parameter) {
                        $frameMeta = new FrameMeta ();
                        $frameMeta->site_id = $site->id;
                        $frameMeta->frame_id = $template_frame->id;
                        $frameMeta->meta_key = $key;
                        $frameMeta->setHistoryAttribute($parameter);
                        $frameMeta->save();
                    }
                }
            }

            if ( $site) {
                $this->getSitePreview ($site);
                return   [
                    'status'=> $response->status(),
                    'site'=> $site
                ];
            }
            else {
                return $response->setStatusCode(404, 'Site dosen`t create');

                return [
                    'status'=> 404
                ];
            }
        }
        else {
            return $response->setStatusCode(404, 'Empty data');
        }

    }


     /**
     * @OA\Patch(
     *      path="/site-edit",
     *      operationId="edit Site",
     *      tags={"Site"},
     *      description="Edit site",
     *      @OA\RequestBody(
        *    required=true,
        *
        *    @OA\JsonContent(
        *       required={"siteId", "organizationName","templateId","elements","logo"},
        *       @OA\Property(property="siteId", type="inteGet",  example="1700"),
        *       @OA\Property(property="organizationName", type="string",  example="Dent"),
        *       @OA\Property(property="templateId", type="inteGet", example="9"),
        *       @OA\Property(property="elements", type="array", collectionFormat="multi",
    *              @OA\Items(
    *                 type="object",
    *                 example={"{name: Про клініку}","{url: elements/template_1/carat_2.html}"},
    *              )),
    *           @OA\Property(property="logo", type="string", example="http://panel.topfractal.com/elements/images/header_logo/2/ppp.png"),
        *    ),
    * ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    // редагування сайту
    public function editSite (Response $response, Request $request) {


        if ( $request->organizationName && $request->siteId && $request->templateId && $request->elements ) {

            $request->validate([
                'organizationName' => 'required|string|max:255',
                'templateId' => 'required|numeric|max:255',
                'elements' => 'required|array|max:255|nullable'                
            ]);

            $site = Site::where('id', $request->siteId)->first();

            if(!$site) {
                return $response->setStatusCode(404, 'Wrong site_id');
            }

            if ( $site->site_trashed == 1) {
                return $response->setStatusCode(404, 'Wrong site_id');
            }

            if ($site->user_id != Auth::user()->id && Auth::user()->type != 'admin' && $site->site_trashed != 0) {
                return $response->setStatusCode(404, 'Wrong site_id');
            }

            $site->site_name = $request->organizationName;
            $site->logo = $request->elements[0]['parameters']['header_logo'];           
            $site->update();


            $page =  Page::where('site_id', $site->id)->first();
            $page->css = $request->templateId;
            $page->update();
            Frame::where('site_id', $site->id)->update(['revision'=> 1]);
            FrameMeta::where('site_id', $site->id)->update(['revision'=> 1]);
           

            if ($request->elements != []) {
                foreach ($request->elements as $block) {

                    
                    $content = $this->getFrameContentEdit($block['id'], $block['parameters'], $request->templateId, $request->elements);
                    $template_frame = new Frame();
                    $template_frame->page_id = $page->id;
                    $template_frame->site_id = $site->id;
                    $template_frame->content = $content;
                    $template_frame->original_url = $block['url'];
                    $template_frame->element_id =  $block['id'];
                    $template_frame->revision = 0;
                    $template_frame->save();

                    foreach ( $block['parameters'] as $key=>$parameter) {
                        $frameMeta = new FrameMeta ();
                        $frameMeta->site_id = $site->id;
                        $frameMeta->frame_id = $template_frame->id;
                        $frameMeta->meta_key = $key;
                        $frameMeta->setHistoryAttribute($parameter);
                        $frameMeta->save();
                    }
                }
            }

            if ( $site) {
                $this->getSitePreview ($site);
                return   [
                    'status'=> $response->status(),
                    'site'=> $site
                ];
            }
            else {
                return $response->setStatusCode(404, 'Site dosen`t create');

                return [
                    'status'=> 404
                ];
            }
        }
        else {
            return $response->setStatusCode(404, 'Empty data');
        }

    }

    //  передача данних введених користувачем в відповідні елементи
    private function getFrameContentEdit($block,  $parameters, $templateId, $elements)
	{
        $block = Element::where('id', $block)->first();
        $subNames = explode('/', $block->url);
        $nameFrame1 = strtok(array_pop($subNames), '.');
        $nameFrame2 = array_pop($subNames);
        $nameFrame = $nameFrame2 . '/'.$nameFrame1;
        $data = [];
        foreach ($parameters as $key=>$parameter) {

            $data[$key] = $parameter;
            if ($key=='social') {
                $socialArray = [];
                foreach ($parameter as $value) {
                    if ($value['value'] != '') {
                        $socialArray[] = $value;
                    } 
                }
                $data[$key] =  $socialArray;
            } 
           
            $category = $block->category;
            if ($category->id == 3 || $category->id == 4 || $category->id == 5 || $category->id == 10 || $category->id == 16 || $category->id == 17) {
                $key = explode('#', $category->link);
                $layout = Layouts::where('layouts_id', $templateId)->where('meta_key', $key[1])->first();
                $data['background'] = $layout->meta_value;
            }
            if ($category->id == 0) {
                $lastKey = array_key_last ( $elements );
                $chart = $elements[$lastKey]['parameters']['chart'];
                $data['chart'] = $chart;

                $email = $elements[$lastKey]['parameters']['email'];
                $data['email'] = $email;

                $phone = $elements[$lastKey]['parameters']['phone'];
                $data['phone'] = $phone;

                $adress = $elements[$lastKey]['parameters']['adress'];
                $data['adress'] = $adress;

                $social = $elements[$lastKey]['parameters']['social'];
                $data['social'] = $social;
            }
            if ($category->id == 17) {
                $header_logo = $elements[0]['parameters']['header_logo'];
                $data['header_logo'] = $header_logo;

                $organizationName = $elements[0]['parameters']['organizationName'];
                $data['organizationName'] = $organizationName;
            }
        }

        $content = view('elements.'.$nameFrame, $data)->render();

        return $content;
    }

     /**
     * @OA\Get(
     *      path="/site/preview/{site}",
     *      operationId="getSitePreview",
     *      tags={"Site"},
     *      description="Get Site Preview",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    // Отримання шаблона
    public function getSitePreview ( Site $site) {     

        if ($site->user_id != Auth::user()->id && Auth::user()->type != 'admin') {
            return $response->setStatusCode(404, 'Wrong site_id');
        }
        if ( $site->site_trashed == 1) {
            return $response->setStatusCode(404, 'Wrong site_id delete');
        }
        if ($site) {
            $page = Page::where('site_id', $site->id)->first();

            if( $page->css==15 ) {
                $skelet = 'elements/template_2/skeleton2.html';
            }
            elseif( $page->css==9 ) {
                $skelet = 'elements/template_1/skeleton1.html';
            }
            elseif( $page->css==16 ){
                $skelet = 'elements/template_3/skeleton3.html';
            }
            else {
                $skelet = 'elements/template_4/skeleton4.html';
            }

            $getFrames = Frame::where('site_id', $site->id)->where('revision', 0)->get('content');

            $dom = new Dom;

            $dom->setOptions(

                (new Options())-> setRemoveScripts(false)
                ->setRemoveStyles(false)

            );       


            $dom->loadFromFile($skelet);
            $contents = $dom->getElementbyId('page');
            
            $content = '';
            foreach ($getFrames as $key=>$frame) {
                $frameContent = new Dom;
                $frameContent->setOptions(

                    (new Options())-> setRemoveStyles(false)
                    -> setRemoveScripts(false)

                );
              
                $frameContent->loadStr($frame->content);

                $content .= $frameContent->getElementbyId('page')->innerHtml;
            }

             $contents->firstChild()->setText($content);
           
            $file_name = 'website.zip';
            $file_pdf = new Filesystem;
            $file_pdf->cleanDirectory(public_path() . '/tmp');

            //return str_replace(' <!--headerIncludes-->', '', "<!DOCTYPE html>\n" . $dom->innerHtml);
            $zip = new ZipArchive();
            $zip->open(public_path() . '/tmp/' . $file_name, ZipArchive::CREATE);
            $zip->addFromString( "skelet.html",  $dom->innerHtml);
            $zip->close();

            $zip = new ZipArchive();
            $zip->open(public_path() . '/tmp/' . $file_name);
            $zip->extractTo('/var/www/dent/public/elements/sites/'.$site->id);
            $zip->close();


            return  [                
                'skelet'=> 'https://topfractal.com/elements/sites/'.$site->id.'/skelet.html',
            ];
        }
    }

      /**
     * @OA\Post(
     *      path="/site-delete/{site_id}",
     *      operationId="deleteSite",
     *      tags={"Site"},
     *      description="Live Preview",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    // видалення сайту
    public function deleteSite (Response $response, Site $site) {

        if ($site->user_id != Auth::user()->id && Auth::user()->type != 'admin') {
            return $response->setStatusCode(404, 'Wrong site_id');
        }
        if ( $site->site_trashed == 1) {
            return $response->setStatusCode(404, 'Wrong site_id');
        }
        if ($site) {
           $site->site_trashed = 1;
           $site->update();
           return  [
                'status'=> $response->status(),
                'site'=> $site
            ];
        }
    }

     /**
     * @OA\Post(
     *      path="/site/publish/{site}",
     *      operationId="sitePublish",
     *      tags={"Site"},
     *      description="site Publish",
     *
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function sitePublish (Site $site) {

        $theSite = Site::where('id', $site->id)->first();

		if(!$theSite->ftp_published)
		{
			$domainName = $this->getDomainName($theSite);
			$this->newDomain($domainName);
			$theSite->publish_date = Carbon::now()->addDays(0)->endOfDay();
			$theSite->publish_date_end = Carbon::now()->addDays(7)->endOfDay();
			$theSite->ftp_published = 1;
			$theSite->save();
		} else {
			if(Carbon::parse($theSite->publish_date_end)->endOfDay() < Carbon::now()->endOfDay()) {
				$theSite->ftp_published = 2;
				$theSite->save();
			}
		}
		$frames = Frame::where('site_id', $theSite->id)->where('revision', 0)->get();

		$content = '';
		foreach ($frames as $val) {
			$content .= $val->content;
		}
		$pages = ['index' => $content];

		$this->export($site, $pages);

	}



	private function export($site, $pagesContent)
	{

		$file_name = Setting::where('name', 'export_fileName')->first();
		$zip = new ZipArchive();
		$zip->open(public_path() . '/tmp/' . $file_name->value, ZipArchive::CREATE);
		$asset_path = Setting::where('name', 'export_pathToAssets')->first();
		$temp = explode('|', $asset_path->value);
		foreach ($temp as $thePath) {
			// Create recursive directory iterator
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator($thePath),
				RecursiveIteratorIterator::LEAVES_ONLY
			);
			foreach ($files as $name => $file) {
				if ($file->getFilename() != '.' && $file->getFilename() != '..') {
					// Get real path for current file
					$filePath = $file->getRealPath();
					$temp = explode("/", $name);
					array_shift($temp);
					$newName = implode("/", $temp);
					if ($thePath == 'elements/images') {

					} else {

						$zip->addFile($filePath, $newName);

					}
				}
			}
		}

        $theSite = Site::where('id',  $site->id )->first();
        $page = Page::where('site_id', $site->id)->first();

        if( $page->css==15 ) {
            $skelet = 'elements/template_2/skeleton2.html';
        }
        elseif( $page->css==9 ) {
            $skelet = 'elements/template_1/skeleton1.html';
        }
        elseif( $page->css==16 ){
            $skelet = 'elements/template_3/skeleton3.html';
        }
        else {
            $skelet = 'elements/template_4/skeleton4.html';
        }

        $getFrames = Frame::where('site_id', $site->id)->where('revision', 0)->get('content');


        $dom = new Dom;
        $dom->setOptions(

            (new Options())-> setRemoveStyles(false)
            -> setRemoveScripts(false)

        );
        $dom->loadFromFile($skelet);

        $contents = $dom->getElementbyId('page');

        $content = '';
        foreach ($getFrames as $key=>$frame) {
            $frameContent = new Dom;
            $frameContent->setOptions(

                (new Options())-> setRemoveStyles(false)
                -> setRemoveScripts(false)

            );
            $frameContent->loadStr($frame->content);
            $content .= $frameContent->getElementbyId('page')->innerHtml;
        }

        $contents->firstChild()->setText($content);
           

		foreach ($pagesContent as $page => $content) {

			$zip->addFromString($page . ".html",  $dom->innerHtml);
        }

		$zip->close();

		$site_name = $this->getDomainName($theSite);
		$theSite->remote_url = $site_name . '.dent.ws';
		$theSite->save();
		$zip = new ZipArchive();
		$zip->open(public_path() . '/tmp/' . $file_name->value);
		$zip->extractTo('/var/www/' . $site_name );
		$zip->close();

		$domain = $site_name . '.dent.ws';


		if($theSite->ftp_published != 2) {
			FacadesFile::put('/etc/apache2/sites-available/' . $domain . '.conf', $this->getDomainConfText($domain, $site_name));
			FacadesFile::put('/etc/apache2/sites-enabled/' . $domain . '.conf', $this->getDomainConfText($domain, $site_name));
		}
		//dump($domain);
		$process = exec('a2ensite ' . $domain);
		dump($process);

		$process = exec('a2dissite 000-default.conf');
		dump($process);

		exec('/usr/bin/sudo /etc/init.d/apache2 restart');
		exec('/usr/bin/sudo /etc/init.d/apache2 reload');

	}


	public function getDomainConfText($domain, $folder)
	{
		return "
			<VirtualHost *:80>
				ServerAdmin admin@178.62.41.211
				ServerName {$domain}
				ServerAlias {$domain}
				DocumentRoot /var/www/{$folder}

				<Directory /var/www/{$folder}/>
					Options Indexes FollowSymLinks MultiViews
					AllowOverride All
					Order allow,deny
					allow from all
					Require all granted
				</Directory>

				ErrorLog \${APACHE_LOG_DIR}/error.log
        CustomLog \${APACHE_LOG_DIR}/access.log combined

			</VirtualHost>
		";
    }

    private function getDomainName($site)
	{
		$domainName = Utils::transliterate($site->site_name);
		$sites = Site::where('site_name', '=', $site->site_name)->get();
		if($sites->count() > 1) {
			$domainName .= $site->id;
		}

		return $domainName;

    }

    public function newDomain($name = 'test')
	{
		$response = Http::withHeaders([
			'Content-Type' => 'application/json',
			// 'Authorization' => 'Bearer b72243518f12b3b1625e61e78a1b9be8070d96637132e633bdd86f63f747c889'
		])
			->withToken('b72243518f12b3b1625e61e78a1b9be8070d96637132e633bdd86f63f747c889')
			->post('https://api.digitalocean.com/v2/domains/dent.ws/records', [
				"type" => "A",
				"name" => $name,
				"data" => "178.62.41.211",
				"priority" => null,
				"port" => null,
				"ttl" => 1800,
				"weight" => null,
				"flags" => null,
				"tag" => null
			]);
	}

}
