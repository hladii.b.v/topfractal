<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use App\Payment;

class PaymentController extends Controller
{

    
     /**
     * @OA\Post(
     *      path="/create-order",
     *      operationId="createOrder",
     *      tags={"Payment"},
     *      description="createOrder",
     *      @OA\RequestBody(
        *    required=true,
        *   
        *    @OA\JsonContent(
        *       required={"orderId","siteId","tariffId","serviceId","totalAmount"},
        *       @OA\Property(property="orderId",   example="12312321321343"),
        *       @OA\Property(property="siteId", example="9"),
    *           @OA\Property(property="tariffId",  example="1"),
  *             @OA\Property(property="serviceId", type="string", example="1,2,"),
    *           @OA\Property(property="totalAmount",  example="234"),
          *    ),
            * ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function createOrder (Response $response, Request $request) 
    {
        $request->validate([                
            'orderId' => 'required|max:255', 
            'siteId' => 'required|max:255', 
            'tariffId' => 'nullable|max:255',
            'serviceId' => 'nullable|max:255', 
            'totalAmount' => 'required|max:255'         
        ]); 
        $orderId = $request->orderId;
        $userID  = Auth::user()->id;
        $created_at = date("Y-m-d H:i:s");

        $payment = new Payment;
		$payment->site_id = $request->siteId;
		$payment->order_id = $orderId;
		$payment->user_id = $userID ;
		$payment->tariff_id =  $request->tariffId;
		$payment->service_id =  $request->serviceId;
		$payment->amount = $request->totalAmount;	
		$payment->status = 'pending';	
		$payment->created_at = $created_at;	
        $payment->save();
        
        return   [
            'status'=> $response->status(),
            'result_url' => 'https://panel.topfractal.com/siteAjaxUpdate/'. $orderId,
            'payment'=> $payment
        ];
    }
}
