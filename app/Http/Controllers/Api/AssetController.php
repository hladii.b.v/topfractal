<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use App\Benefit;
use App\Template;
use App\Paremeter;
use File;
use Storage;
use Validator;

class AssetController extends Controller
{   

     /**
     * @OA\Get(
     *      path="/refresh-text",
     *      operationId="refreshText",
     *      tags={"Assets"},  
     *      description="refresh Text",
     *      @OA\RequestBody(
        *    required=true,        *    
        *    @OA\JsonContent(
        *       required={"templateId", "type", "lang"},
        *       @OA\Property(property="templateId",  type="integer",  example="9"),
         *       @OA\Property(property="lang",  type="string",  example="ua"),
    *           @OA\Property(property="type",  type="string", example="headerTitle"),  ),     
        *    ),   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function refreshText(Response $response, Request $request)
	{
        $template = Template::where('id', $request->templateId)->first();
        if ($template) {
            $direction = $template->direction;        
            $random_quote = Benefit::where('type', $request->type)
                ->where('lang', $request->lang)
                ->where('direction_id', $direction->id)
                ->inRandomOrder()->first();
                
            if ($random_quote) {
                return [
                    'status'=> $response->status(),
                    'text' => $random_quote                   
                ];               
            }
            else {
                return $response->setStatusCode(404, 'Wrong type');
                
            }          
            
        }
        else {
            return $response->setStatusCode(404, 'Wrong templateId');           
        } 
       	
    }


     /**
     * @OA\Get(
     *      path="/refresh-img",
     *      operationId="refreshImg",
     *      tags={"Assets"},  
     *      description="refresh Img",
     *      @OA\RequestBody(
        *    required=true,        *    
        *    @OA\JsonContent(
        *       required={"templateId", "type"},
        *       @OA\Property(property="templateId",  type="integer",  example="9"),
    *           @OA\Property(property="type",  type="string", example="header_logo"),  ),     
        *    ),   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function refreshImg(Response $response, Request $request)
	{
        $template = Template::where('id', $request->templateId)->first();

        if ($template) {
            $direction = $template->direction; 
            $folder = 'elements/images/' . $request->type.'/'. $direction->id;
            $folderContent= File::allFiles($folder);		
            $imgRand =  array_rand( $folderContent);
            $file =  $folderContent[$imgRand];
            $filename = $folder . '/' . $file->getRelativePathname();
            if ($folderContent && $request->type!=null) {
                
                return [
                    'status'=> $response->status(),
                    'img' => $filename                   
                ];               
            }
            else {

                return $response->setStatusCode(404, 'Wrong type');               
            } 
        }   
        else {
            return $response->setStatusCode(404, 'Wrong template');           
        } 
        
    }


    /**
     * @OA\Get(
     *      path="/get-img",
     *      operationId="getImg",
     *      tags={"Assets"},  
     *      description="get all Img",
     *      @OA\RequestBody(
        *    required=true,        *    
        *    @OA\JsonContent(
        *       required={"templateId", "type"},
        *       @OA\Property(property="templateId",  type="integer",  example="9"),
    *           @OA\Property(property="type",  type="string", example="header_logo"),  ),     
        *    ),   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function getImg(Response $response, Request $request)
	{
        $template = Template::where('id', $request->templateId)->first();

        if ($template) {
            $direction = $template->direction; 
            $userID = Auth::user()->id;

            $folder = 'elements/images/' . $request->type.'/'. $direction->id;
            $folderUser = 'elements/images/users/' . $userID.'/'. $request->type;
           
            $filename = [];
            $filenameUser = [];

            if (file_exists($folder)) {

                $folderContent = File::allFiles($folder);
                foreach ($folderContent as $file) {
                    $filename[] = $folder . '/' . $file->getRelativePathname();
                }
            }           
            if (file_exists($folderUser)) {

                $folderContentUser = File::allFiles($folderUser);
                foreach ($folderContentUser as $fileUser) {
                    $filenameUser[] = $folderUser . '/' . $fileUser->getRelativePathname();
                }
            }
            
            if ($request->type!=null) {
                
                return [
                    'status'=> $response->status(),
                    'imgAdmin' => $filename,
                    'imgUser' =>  $filenameUser                  
                ];               
            }
            else {

                return $response->setStatusCode(404, 'Wrong type');               
            } 
        }   
        else {
            return $response->setStatusCode(404, 'Wrong template');           
        } 
        
    }

        /**
     * @OA\Post(
     *      path="/upload-pdf",
     *      operationId="uploadPdf",
     *      tags={"Assets"},  
     *      description="uploadPdf",
     *      @OA\RequestBody(
        *    required=true,        *    
        *    @OA\JsonContent(
        *       required={"InputFile",},
        *       @OA\Property(property="InputFile",  example="pdf"),
        *       ),     
        *    ),   
     *      @OA\Response( 
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function uploadPdf(Response $response, Request $request)
	{
        

        if ($request->InputFile){             
           
            $userID = Auth::user()->id;
            
            $filePath = 'images/users/'. $userID . '/userPdf';     

            $file = $request->file('InputFile');
            $filename = time() . '.' . $request->file('InputFile')->extension();
           
         

            if(  $file->move($filePath, $filename) ) {
                return [
                    'status'=> $response->status(),
                    'url'=> $filePath.'/'. $filename,
                    'message' => 'File  upload!'              
                ];
            }
            else {
                return $response->setStatusCode(402, 'File not upload'); 
            }
        }
        else {
            return $response->setStatusCode(401, 'No file'); 
        } 
    }
     /**
     * @OA\Post(
     *      path="/upload-img",
     *      operationId="uploadImg",
     *      tags={"Assets"},  
     *      description="upload Img",
     *      @OA\RequestBody(
        *    required=true,        *    
        *    @OA\JsonContent(
        *       required={"imageFile", "type"},
        *           @OA\Property(property="type",  type="string", example="header_logo"),
        *       @OA\Property(property="imageFile",  type="array", collectionFormat="multi",
    *              @OA\Items(
    *                 type="string", format="base64",
    *                 example={"data: image/png; asdSDsdf"},
    *              ))),     
        *    ),   
     *      @OA\Response( 
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */


    public function uploadImg(Response $response, Request $request)
	{
        

        if ($request->imageFile){  
            
            $paremeter = Paremeter::where('key', $request->type)->first();
            
            if(!$paremeter){
                return [
                    'status'=> 404,
                    'message' => 'Wrong type'              
                ];
            }

            $userID = Auth::user()->id;
            $userFolders = [];
            $userFolder = 'images/users/'. $userID . '/'.  $request->type;
           
            
            foreach($request->imageFile as $key=>$userFile) {
                
                $data = substr($userFile, strpos($userFile, ',') + 1);              
                $fileName = $key. time(). '.png';
                $data = base64_decode($data);

                if (!$this->check_base64_image($userFile)) {
                    continue;
                } 
                
                Storage::disk('public')->put($userFolder . '/' . $fileName, $data);
                $userFolders[] = 'elements/' . $userFolder . '/' . $fileName;
            }

            if($request->imageFile) {
                return [
                    'status'=> $response->status(),
                    'url'=> $userFolders,
                    'message' => 'File  upload!'              
                ];
            }
            else {
                return $response->setStatusCode(404, 'File not upload'); 
            }
        }
        else {
            return $response->setStatusCode(404, 'No file'); 
        } 
    }

    private function check_base64_image($userFile) {
        $userFile = explode(';', $userFile);
        $userFile = explode(':', $userFile[0]);
        $userFile = explode('/', $userFile[1]);
        if ( $userFile[0] =='image') {
            return true;
        }
        else {
            return false;
        }
        
    }

    /**
     * @OA\Post(
     *      path="/add-benefit",
     *      operationId="add Benefit",
     *      tags={"Assets"},  
     *      description="add Benefit",
     *      @OA\RequestBody(
        *    required=true,        *    
        *    @OA\JsonContent(
        *       required={"parent_id", "value", "lang"},
        *       @OA\Property(property="parent_id",  type="integer",  example="202"),
     *          @OA\Property(property="value",  type="string",  example="sdfsdfsdfsdfsdfds"),
    *           @OA\Property(property="lang",  type="string", example="ua"),  ),     
        *    ),   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function addBenefit (Response $response, Request $request)
	{    
         $request->validate([
            'parent_id' => 'required|numeric',           
            'value' => 'required|string',
            'lang' => 'required|string|max:20',
        ]); 
        
        $userID = Auth::user()->id;		
		
        $new_benefit = new Benefit;
        $new_benefit->type = 'benefit';
        $new_benefit->lang = $request->lang;
        $new_benefit->desc = $request->value;
        $new_benefit->parent_id = $request->parent_id;
        $new_benefit->user_id = $userID;
        $new_benefit->save();

        $data =Benefit::
            where('parent_id', null)
            ->where('type', 'benefit')
            ->where('lang', $request->lang)
            ->get();  

        $getBenefits = [];
        
        foreach ($data as $value) {
            $benefit = Benefit::find($value->id);		
            
            $getBenefits[] = [
                'id' => $value->id,
                'type'  => $value->type,
                'img'  => 'elements/images/benefits/'. $value->img,
                'desc' => $value->desc,
                'children' => $benefit->childrenAdmin( $userID)
            ];

        }        
        return [
            'status'=> $response->status(),
            'benefit'=> $new_benefit,                    
        ];
		
    }


     /**
     * @OA\Post(
     *      path="/add-services",
     *      operationId="addServices",
     *      tags={"Assets"},  
     *      description="add Services",
     *      @OA\RequestBody(
        *    required=true,        *    
        *    @OA\JsonContent(
        *       required={"parent_id", "value", "lang"},
        *       @OA\Property(property="parent_id",  type="integer",  example="70"),
     *          @OA\Property(property="value",  type="string",  example="sdfsdfsdfsdfsdfds"),
    *           @OA\Property(property="lang",  type="string", example="ua"),  ),     
        *    ),   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function addServices (Response $response, Request $request)
	{    
         $request->validate([
            'parent_id' => 'required|numeric',           
            'value' => 'required|string',
            'lang' => 'required|string',
        ]); 
        
        $userID = Auth::user()->id;		
		
        $new_benefit = new Benefit;
        $new_benefit->type = 'services';
        $new_benefit->lang = $request->lang;
        $new_benefit->desc = $request->value;
        $new_benefit->parent_id = $request->parent_id;
        $new_benefit->user_id = $userID;
        $new_benefit->save();

        $data =Benefit::
            where('parent_id', null)
            ->where('type', 'services')
            ->where('lang', $request->lang)
            ->get(); 

        $getBenefits = [];
        
        foreach ($data as $value) {
            $benefit = Benefit::find($value->id);		
            
            $getBenefits[] = [
                'id' => $value->id,
                'type'  => $value->type,               
                'desc' => $value->desc,
                'children' => $benefit->childrenAdmin( $userID)
            ];

        }        
        return [
            'status'=> $response->status(),
            'service'=> $new_benefit,                    
        ];
		
    }
}
