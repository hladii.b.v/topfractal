<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Filesystem\Filesystem;

use Storage;
use File;

use App\User;
use App\Paremeter;
use App\SocialFacebookAccount;


class ApiController extends Controller
{


    public function retutTrue () {
        return true;
    }

     /**
     * @OA\Post(
     *      path="/logout",
     *      operationId="logout",
     *      tags={"User"},
     *      description="User logout",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function logout (Response $response, Request $request)
    {
        $user = Auth::user();
        if ($user) {

            $user->api_token = Null;
            $user->update();
            return [
                'status'=>  $response->status(),
                'user' => $user
            ];
        }
        else {
            return $response->setStatusCode(404, 'Wrong token');
        }
    }

     /**
     * @OA\Post(
     *      path="/createUser",
     *      operationId="createUser",
     *      tags={"User"},
     *      description="Create New User",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"email", "lang"},
        *       @OA\Property(property="email",  type="string", format="email",  example="easdas@asdas.asdas"), 
    *           @OA\Property(property="lang",  type="string",  example="ua"), ),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function createUser (Response $response, Request $request)
    {
        if($request->email && $request->email!=''){
            $email = $request->email;
        }
        else {
            return $response->setStatusCode(404, 'Wrong email');
        }

        $request->validate([
            'email' => 'required|string|email|max:255',
            'lang' => 'required|string|max:255'
        ]);
        $lang = $request->lang;
        $user = User::where('email', $email)->first();
        if (!$user) {
            $password = '';

            $desired_length = rand(8, 12);

            for($length = 0; $length < $desired_length; $length++) {
                $password .= chr(rand(32, 126));
            }
            $to_name = $email;
            $to_email = $email;

            $body = '';
            $mailSubject = '';
            if($lang == 'ua'){
                $body = 'Ваш пароль ' . $password;
                $mailSubject = 'Створення нового користувача';
            }
            elseif ($lang == 'ru'){
                $body = 'Ваш пароль ' . $password;
                $mailSubject = 'Создание нового пользователя';
            }
            else {
                $body = 'Ваш пароль ' . $password;
                $mailSubject = 'Створення нового користувача';
            }
            
            
            $data_mail = array('name'=>$to_name, 'body' =>  $body);
            Mail::send('auth/email/mail', $data_mail, function($message) use ($to_name, $to_email,  $mailSubject) {
                $message->to($to_email, $to_name)
                        ->subject($mailSubject);
                $message->from( env('MAIL_USERNAME', 'hladii.b.v@gmail.com'), $mailSubject);
            });

            $user = User::create([
                'role_id' => 1,
                'email' => $email,
                'password' => Hash::make($password),
                'api_token' => Str::random(80),
                'type' => 'user',
                'active' => 1
            ]);

            $token = Str::random(80);
            $user->api_token = hash('sha256', $token);
            $user->update();

            return [
                'status'=> $response->status(),
                'user' => $user,
                'token' => $token
            ];

        }
        else {
            return $response->setStatusCode(404, 'Wrong email');
        }
    }

     /**
     * @OA\Post(
     *      path="/getToken",
     *      operationId="getToken",
     *      tags={"User"},
     *      description="Get User Token ",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"email", "password"},
        *       @OA\Property(property="email",  type="string", format="email",  example="easdas@asdas.asdas"),
    *           @OA\Property(property="password",  type="string", example="123qweasd"),  ),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function getToken(Response $response, Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ( $user) {
            if (Hash::check($request->password, $user->password)) {

                $token = Str::random(80);
                $user->api_token = hash('sha256', $token);
                $user->update();

                return [
                    'status'=> $response->status(),
                    'user' => $user,
                    'token' => $token
                ];
            }
            else {
                return $response->setStatusCode(404, 'Wrong password');
            }
        }
        else {
            return $response->setStatusCode(404, 'Wrong email');
        }
    }


     /**
     * @OA\Get(
     *      path="/getUserByToken",
     *      operationId="getUserByToken",
     *      tags={"User"},
     *      description="get User By Token",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function getUserByToken(Response $response)
    {
        $user = Auth::user();
        if ( $user) {

            return [
                'status'=> $response->status(),
                'user' => $user
            ];
        }
        else {
            return $response->setStatusCode(404, 'Wrong token');
        }
    }


     /**
     * @OA\Post(
     *      path="/update-user",
     *      operationId="updatteUser",
     *      tags={"User"},
     *      description="Update User",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"email", "last_name", "first_name"},
        *       @OA\Property(property="email",  type="string", format="email",  example="easdas@asdas.asdas"),
        *       @OA\Property(property="last_name",  type="string",  example="Ivan"),
    *           @OA\Property(property="first_name",  type="string",  example="Ivanov"),  ),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function updatteUser(Response $response, Request $request)
    {
        $user = Auth::user();
        if ( $user) {

            $request->validate([
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
            ]);

            $checkMail = User::where('email', $request->email)->where('id', '<>', $user->id)->first();

            if ( $checkMail) {

                return $response->setStatusCode(404, 'Wrong email');
            }

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->update();

            return [
                'status'=> $response->status(),
                'user' => $user
            ];
        }
        else {
            return $response->setStatusCode(404, 'Wrong token');
        }
    }

     /**
     * @OA\Post(
     *      path="/update-user-password",
     *      operationId="updateUserPassword",
     *      tags={"User"},
     *      description="update User Password",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"password", "password_new"},
        *       @OA\Property(property="password_new",  type="string",  example="sdgfsdfg23432"),
    *           @OA\Property(property="password",  type="string",  example="sdfsd22222"),  ),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function updateUserPassword(Response $response, Request $request)
    {
        $user = Auth::user();
        if ( $user) {

            $request->validate([
                'password' => 'required|string|min:6|max:255',
                'password_new' => 'required|string|min:6|max:255'
            ]);

            if (Hash::check($request->password, $user->password)) {

                $user->password =  Hash::make($request->password_new);
                $user->update();

                return [
                    'status'=> $response->status(),
                    'user' => $user
                ];
            }
            else {
                return $response->setStatusCode(404, 'Wrong old password');
            }
        }
        else {
            return $response->setStatusCode(404, 'Wrong token');
        }
    }

    /**
     * @OA\Post(
     *      path="/get-reset-password",
     *      operationId="resetPasswordForm",
     *      tags={"User"},
     *      description="send Email link",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"email", "lang"},
        *       @OA\Property(property="email",  type="email",  example="sdgfsdf@dasd.asd"),
        *           @OA\Property(property="lang",  type="string",  example="ua"), 
    *             ),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function resetPasswordForm(Response $response, Request $request)
    {

        $request->validate([
            'email' => 'required|email|max:255',
            'lang' => 'required|string|max:255'
        ]);
        $email = $request->email;
        $lang = $request->lang;
        $user = User::where('email', $email)->first();

        if ($user) {
            $forgotten_password_code = $email . time();
            $forgotten_password_code =hash('sha256', $forgotten_password_code);
            $user->forgotten_password_code = $forgotten_password_code;
            $user->update();
            $to_name = $email;
            $to_email = $email;
            $body = '';
            $mailSubject = '';
            if($lang == 'ua'){
                $body = 'Для відновлення паролю перейдіть по посиланню https://topfractal.com/change-password/'.$forgotten_password_code;
                $mailSubject = 'Відновлення паролю';
            }
            elseif ($lang == 'ru'){
                $body = 'Для восстановления пароля перейдите по ссылке https://topfractal.com/change-password/'.$forgotten_password_code;
                $mailSubject = 'Восстановление пароля';
            }
            else {
                $body = 'Для відновлення паролю перейдіть по посиланню https://topfractal.com/change-password/'.$forgotten_password_code;
                $mailSubject = 'Відновлення паролю';
            }

            $data_mail = array('name'=>$to_name,
                'body' => $body);
            Mail::send('auth/email/mail', $data_mail, function($message) use ($to_name, $to_email, $mailSubject) {
                $message->to($to_email, $to_name)
                        ->subject($mailSubject);
                $message->from( env('MAIL_USERNAME', 'hladii.b.v@gmail.com'), $mailSubject);
            });
            return [
                'status'=> $response->status()
            ];
        }
        else {
            return $response->setStatusCode(404, 'No user');
        }
    }

     /**
     * @OA\Post(
     *      path="/reset-password",
     *      operationId="resetPassword",
     *      tags={"User"},
     *      description="update User Password",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"code", "password"},
        *       @OA\Property(property="code",  type="string",  example="sdgfsdfg23432"),
    *           @OA\Property(property="password",  type="string",  example="sdfsd22222"),  ),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function resetPassword (Response $response,  Request $request)
    {

        $request->validate([
            'code' => 'required|string',
            'password' => 'required|string|min:6|max:255',
        ]);

        $code = $request->code;

        $user = User::where('forgotten_password_code', $code)->first();

        if($user) {
            $token = Str::random(80);

            $user->password  =  Hash::make($request->password);
            $user->forgotten_password_code  =  null;
            $user->api_token = hash('sha256', $token);

            $user->update();

            return [
                'status'=> $response->status(),
                'user'=> $user,
                'token'=> $token,
            ];
        }
        else {
            return $response->setStatusCode(404, 'Wrong code');
        }
    }


     /**
     * @OA\Post(
     *      path="/update-user-avatar",
     *      operationId="loadAvatar",
     *      tags={"User"},
     *      description="update User Avatar",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"imageFile"},
        *       @OA\Property(property="imageFile", type="string", format="base64", example={"data: image/png; asdSDsdf"}), ),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function loadAvatar (Response $response,  Request $request)
    {
        if ($request->imageFile){
            $userFile = $request->imageFile;
            $userID = Auth::user()->id;
            $user = User::where('id', $userID)->first();

            $userFolder = 'images/users/'. $userID . '/avatar';
            if (file_exists($userFolder)) {
                $file = new Filesystem;
                $file->cleanDirectory($userFolder);
            }

            $data = substr($userFile, strpos($userFile, ',') + 1);
            $fileName = time(). '.png';
            $data = base64_decode($data);

            if (!$this->check_base64_image($userFile)) {
                return  $response->setStatusCode(404, 'No file'); ;
            }

            Storage::disk('public')->put($userFolder . '/' . $fileName, $data);
            $userAvatar = 'elements/' . $userFolder . '/' . $fileName;
            $user->avatar = $userAvatar;
            $user->update();

            return [
                'status'=> $response->status(),
                'url'=> $userAvatar,
            ];
        }
        else {
            return $response->setStatusCode(404, 'No file');
        }
    }

    private function check_base64_image($userFile) {
        $userFile = explode(';', $userFile);
        $userFile = explode(':', $userFile[0]);
        $userFile = explode('/', $userFile[1]);
        if ( $userFile[0] =='image') {
            return true;
        }
        else {
            return false;
        }

    }

    /**
     * @OA\Get(
     *      path="/get-user-img",
     *      operationId="getImg",
     *      tags={"User"},
     *      description="get all Img",     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function getImg(Response $response, Request $request)
	{

        $userID = Auth::user()->id;

        $folderUserHeader = 'elements/images/users/' . $userID .'/header_img';
        $folderUserLogo = 'elements/images/users/' . $userID .'/header_logo';
        $folderUserAbout = 'elements/images/users/' . $userID .'/about';
        $folderUserGallery = 'elements/images/users/' . $userID .'/imgList';
        $folderUserTeam = 'elements/images/users/' . $userID .'/teams';

        $folderHeader = [];
        $folderLogo = [];
        $folderAbout = [];
        $folderGallery = [];
        $folderTeam = [];

        if (file_exists('elements/images/users/' . $userID)) {

            if (file_exists( $folderUserHeader)) {
                $folderContentHeader = File::allFiles($folderUserHeader);

                foreach ($folderContentHeader as $file) {

                    $folderHeader[] =  $folderUserHeader . '/' . $file->getRelativePathname();
                }
            }
            if (file_exists( $folderUserLogo)) {
                $folderContentLogo = File::allFiles($folderUserLogo);

                foreach ($folderContentLogo as $file) {
                    $folderLogo[] = $folderUserLogo . '/' . $file->getRelativePathname();
                }
            }
            if (file_exists( $folderUserAbout)) {
                $folderContentAbout = File::allFiles($folderUserAbout);

                foreach ($folderContentAbout as $file) {
                    $folderAbout[] = $folderUserAbout . '/' . $file->getRelativePathname();
                }
            }
            if (file_exists( $folderUserGallery)) {
                $folderContentGallery = File::allFiles($folderUserGallery);

                foreach ($folderContentGallery as $file) {

                    $folderGallery[] = $folderUserGallery . '/' . $file->getRelativePathname();
                }
            }
            if (file_exists( $folderUserTeam)) {
                $folderContentTeam = File::allFiles($folderUserTeam);

                foreach ($folderContentTeam as $file) {
                    $folderTeam[] = $folderUserTeam . '/' . $file->getRelativePathname();
                }
            }

            return [
                'status'=> $response->status(),
                'files' => [
                    'header_img' => $folderHeader,
                    'header_logo' => $folderLogo,
                    'about' => $folderAbout,
                    'imgList' => $folderGallery,
                    'teams' => $folderTeam,
                ]
            ];
        }
        else {
            return [
                'status'=> $response->status(),
                'files' => []
            ];
        }
    }

    /**
     * @OA\Post(
     *      path="/upload-user-img",
     *      operationId="uploadImg",
     *      tags={"User"},
     *      description="upload Img",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"imageFile", "type"},
        *           @OA\Property(property="type",  type="string", example="header_logo"),
        *       @OA\Property(property="imageFile",  type="array", collectionFormat="multi",
    *              @OA\Items(
    *                 type="string", format="base64",
    *                 example={"data: image/png; asdSDsdf"},
    *              ))),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function uploadImg(Response $response, Request $request)
	{

        if ($request->imageFile){

            $paremeter = Paremeter::where('key', $request->type)->first();

            if(!$paremeter){
                return [
                    'status'=> 404,
                    'message' => 'Wrong type'
                ];
            }

            $userID = Auth::user()->id;
            $userFolders = [];
            $userFolder = 'images/users/'. $userID . '/'.  $request->type;


            foreach($request->imageFile as $key=>$userFile) {

                $data = substr($userFile, strpos($userFile, ',') + 1);
                $fileName = $key. time(). '.png';
                $data = base64_decode($data);

                if (!$this->check_base64_image($userFile)) {
                    continue;
                }

                Storage::disk('public')->put($userFolder . '/' . $fileName, $data);
                $userFolders[] = 'elements/' . $userFolder . '/' . $fileName;
            }

            if($request->imageFile) {
                return [
                    'status'=> $response->status(),
                    'url'=> $userFolders,
                    'message' => 'File  upload!'
                ];
            }
            else {
                return $response->setStatusCode(404, 'File not upload');
            }
        }
        else {
            return $response->setStatusCode(404, 'No file');
        }
    }

     /**
     * @OA\Post(
     *      path="/delete-user-img",
     *      operationId="delImage",
     *      tags={"User"},
     *      description="delete Img",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"imageFile"},        *
        *       @OA\Property(property="imageFile",  type="array", collectionFormat="multi",
    *              @OA\Items(
    *                 type="string", format="string",
    *                 example={"elements/images/users/204/header_img/01600416923.png"},
    *              ))),
        *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function delImage(Response $response, Request $request)
	{
        if ($request->imageFile){


            $userID = Auth::user()->id;


            $userFolders = [];

            foreach($request->imageFile as $userFile) {
                $checkId = explode ('/', $userFile);


                if ($checkId[3] !=  $userID) {
                    continue;
                }
                if (file_exists( $userFile)) {

                    $userFolders[] = $userFile;
                    unlink($userFile);
                }

            }

            if($request->imageFile) {
                return [
                    'status'=> $response->status(),
                    'url'=> $userFolders,
                    'message' => 'File  Deleted!'
                ];
            }
        }
        else {
            return $response->setStatusCode(404, 'No file');
        }
    }

     /**
     * @OA\Post(
     *      path="/facebook-login",
     *      operationId="facebookLogin",
     *      tags={"User"},
     *      description="facebookLogin",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"facebook_user_id",  "email", "avatar", "first_name", "last_name"},        *
        *       @OA\Property(property="facebook_user_id",  type="string",  example="2788416651278013"),
        *       @OA\Property(property="email",  type="email",  example="dsf@dfsds.fdsf"),
        *       @OA\Property(property="avatar",  type="string",  example="elements/images/users/asdas.png"),
        *       @OA\Property(property="first_name",  type="string",  example="first_name"),
        *       @OA\Property(property="last_name",  type="string",  example="last_name"),
    *           ),
        *   ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function facebookLogin(Response $response, Request $request)
	{

        $request->validate([
            'facebook_user_id' => 'required|max:255',
            'email' => 'nullable|string|email|max:255',
            'avatar' => 'nullable|string',
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255'
        ]);

        $facebook_user_id = $request->facebook_user_id;

        $user = SocialFacebookAccount::where('provider_user_id', $facebook_user_id)->first();
        if (!$user) {         

            $email = $request->email;

            if ( $email != '') {
                $user = User::where('email',  $email)->first();
                if ( !$user ) {
                    $user = new User;
                    $user->role_id = 1;
                    $user->email = $email;
                    $user->type = 'user';
                    $user->active = 1;
                    $user->save();
                }
            }
            else {
                $user = new User;
                $user->role_id = 1;
                $user->type = 'user';
                $user->active = 1;
                $user->save();               
            }

            SocialFacebookAccount::create([
                'user_id' => $user->id,
                'provider_user_id' =>$facebook_user_id,
                'provider' => 'facebook'
            ]);

        }
        else {
            
            $user = User::where('id',   $user->user_id)->first();
        }

        $token = Str::random(80);
        
        $user->api_token = hash('sha256', $token);
        $user->avatar = $request->avatar;
        $user->first_name = $request->first_name;
        $user->last_name =  $request->last_name;
        $user->update();

        return [
            'status'=> $response->status(),
            'user' => $user,
            'token' => $token
        ];
    }


    /**
     * @OA\Post(
     *      path="/google-login",
     *      operationId="googleLogin",
     *      tags={"User"},
     *      description="googleLogin",
     *      @OA\RequestBody(
        *    required=true,        *
        *    @OA\JsonContent(
        *       required={"google_id",  "email", "avatar", "first_name", "last_name"},        *
        *       @OA\Property(property="google_id",  type="string",  example="2788416651278013"),
        *       @OA\Property(property="email",  type="email",  example="dsf@dfsds.fdsf"),
        *       @OA\Property(property="avatar",  type="string",  example="elements/images/users/asdas.png"),
        *       @OA\Property(property="first_name",  type="string",  example="first_name"),
        *       @OA\Property(property="last_name",  type="string",  example="last_name"),
    *           ),
        *   ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function googleLogin(Response $response, Request $request)
	{

        $request->validate([
            'google_id' => 'required|max:255',
            'email' => 'nullable|string|email|max:255',
            'avatar' => 'nullable|string',
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255'
        ]);

        $googleId = $request->google_id;

        $user = User::where('google_id', $googleId)->first();
        if (!$user) {

            $email = $request->email;
            $user = User::where('email',  $email)->first();
            if ( !$user ) {

                $user = User::create([
                    'role_id' => 1,
                    'email' =>$email,
                    'type' => 'user',
                    'active' => 1,
                    'google_id' => $googleId
                ]);
            }
            else {
                $user->google_id = $googleId;
                $user->update();
            }


        }

        $token = Str::random(80);

        $user->api_token = hash('sha256', $token);
        $user->avatar = $request->avatar;
        $user->first_name = $request->first_name;
        $user->last_name =  $request->last_name;
        $user->update();

        return [
            'status'=> $response->status(),
            'user' => $user,
            'token' => $token
        ];
    }
}
