<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Tariff;
use App\SitePage;
use App\Service;
use App\Template;

class PageController extends Controller
{

    /**
    * @OA\Get(
    *      path="/home-page/{lang}",
    *      operationId="getHomePageList",
    *      tags={"Pages"},
    *      summary="Get list of home page keys",
    *      description="Returns list of home page keys",
    *      @OA\Response(
    *          response=200,
    *          description="Successful operation",
    *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
        *   @OA\Response(
        *          response=404,
        *          description="Resource Not Found"
        *     )
     *     )
     */
    public function home(Response $response, $lang)
    {
        // $lang = 'ua';
        
        $tariffs_number = Tariff::groupBy('number')->get('number');
		
		$tariffs = [];
		foreach ($tariffs_number as $value) {
			$tariff_price = Tariff::where('number', $value->number)->where('meta', 'price')->get(['id', 'value']);
			$tariff_text = Tariff::where('number', $value->number)->where('meta', 'text')->where('lang', $lang)->get(['id', 'value', 'lang', 'show']);
			if ($value->number==3) {
                $name =  SitePage::where('lang', $lang)->where('page_key' , 'tariffs_Professional')->first();
                $name = $name->page_text;
            }
            elseif ($value->number==2) {
                $name =  SitePage::where('lang', $lang)->where('page_key' , 'tariffs_Extended')->first();
                $name = $name->page_text;
            }
            else {
                $name =  SitePage::where('lang', $lang)->where('page_key' , 'tariffs_Basic')->first();
                $name = $name->page_text;
            }

			$tariffs[$value->number] = [
                'name'=>$name,
				'tariff_price' => $tariff_price,
				'tariff_text' => $tariff_text	
			];
        }
        $services = Service::where('lang', $lang)->where('show', 1)->get();
        $template= Template::take(3)->get();

        $pagesArray = []; 
        $pages = SitePage::groupBy('page_name')->get('page_name');    
       
       
        foreach ($pages as $page) {
            
            $content = SitePage::where('lang', $lang)->where('page_name' , $page->page_name)->get();    
            
            $page_content = [];
            foreach ($content as $value) {
                $page_content[$value->page_key] = $value->page_text;
            }
            if($value->page_name == 'home' ) {
                $page_content['services'] =  $services;
                $page_content['tariffs'] =  $tariffs;
                $page_content['template'] =  $template;
            }

            $pagesArray[$page->page_name] = $page_content;
           
        }      
       

        if ($pagesArray) { 
            return [
                'status'=>  $response->status(),
                'page_content' => $pagesArray
            ]; 
        }       
        else {
            return $response->setStatusCode(404, 'Error'); 
        }       
    }

     /**
    * @OA\Get(
    *      path="/get-lang",
    *      operationId="getLang",
    *      tags={"Pages"},
    *      summary="getLang",
    *      description="Returns list of home page keys",
    *      @OA\Response(
    *          response=200,
    *          description="Successful operation",
    *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
        *   @OA\Response(
        *          response=404,
        *          description="Resource Not Found"
        *     )
     *     )
     */
    public function getLang (Response $response)
    {
        $langArray=[];
        
        $langs = SitePage::where('hide_lang', Null)->groupBy('lang')->get('lang');
        foreach ($langs as $lang) {
            $langArray[] = $lang->lang;
        }
       
        return [
            'status'=>  $response->status(),
            'lang' => $langArray
        ]; 
            
    }
}
