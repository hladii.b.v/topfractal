<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Service;
use App\Benefit;
use App\Layouts;
use App\Tariff;
use App\Direction;
use App\Element;
use App\User;
use App\Category;
use App\TemplateMeta;
use App\Payment;
use App\Template;
use App\Paremeter;
use File;
use Storage;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Options;
use Illuminate\Support\Facades\Auth;
use Illuminate\Filesystem\Filesystem;
use Redirect,Response;
use ZipArchive;
 
class AssetController extends Controller
{
	/**
	 * List all image from image directory
	 */


	public function getAdditional()
	{
		$services = Service::orderBy('lang', 'DESC')->get();
		
		
		return view('assets.additional', compact('services'));
	}

	public function deleteAdditional (Request $request) 
	{
		Service::where('id', $request->elementID)->delete();

		return redirect()->route('additional');
	}
	
	public function deleteTariff (Request $request) 
	{
		Tariff::where('id', $request->tariff)->delete();

		return redirect()->route('tariffs', app()->getLocale());
	}

	public function selectTariffs($lang) 
	{
		$userID = Auth::user()->id;
		$user = User::find($userID);
		
		$sites = $user->sites()->get();
		$services = Service::where('lang', $lang)->where('show', 1)->get();
		if ($sites) {				
			return view('pages.tariffs', compact('sites', 'services'));
		}
		return view('pages.layouts');
	}

	public function SitteTariff (Request $request) {

		$paymentID = Payment::where('site_id', $request->siteId)->where('status', 'done')->orderBy('end_at', 'desc')->first('tariff_id');
		
		if ($paymentID) {
			$tariffCheck = Tariff::where('id', $paymentID->tariff_id)->first(['number']);
			$tariffs = Tariff::where('number', '>=', $tariffCheck->number)->groupBy('number')->get(['number']);
		}
		else {
			$tariffs = Tariff::groupBy('number')->get(['number']);
		}
		return $tariffs;
	}


	public function tariffChecked (Request $request) {

		
		$tariffID =  $request->tariff;
		

		$tariff_price = Tariff::where('number', $tariffID)->where('meta', 'price')->get(['value', 'id']);
		$priceArray = [];
		foreach ($tariff_price as $price){
			$priceArray[] = [
				'price' => $price->value,
				'id' => $price->id,
				
			];
		}
		$return = [
			'priceArray' => $priceArray			
		];
		return $return;
	}

	public function getTariffs()
	{
		$tariffs_number = Tariff::groupBy('number')->get('number');
		
		$tariffs = [];
		foreach ($tariffs_number as $value) {
			$tariff_price = Tariff::where('number', $value->number)->where('meta', 'price')->get(['id', 'value']);
			$tariff_text = Tariff::where('number', $value->number)->where('meta', 'text')->get(['id', 'value', 'lang', 'show']);
			
			$tariffs[$value->number] = [
				'tariff_price' => $tariff_price,
				'tariff_text' => $tariff_text	
			];
		}
		//dd($tariffs);
		return view('assets.tariffs', compact('tariffs'));
	}

	public function tariffsUpdate (Request $request)
	{
		
        Tariff::where('id', $request->id)->update(['value'=> $request->desc, 'show'=> $request->show]);

        return Response::json($request);

	}
	public function addTariffs (Request $request) {
		
		if ($request->tariff_show) {
			$tariff_show=1;
		}
		else {
			$tariff_show = 0;
		}
		$new_tariff = new Tariff;
		$new_tariff->number = $request->tariff_id;
		$new_tariff->meta = $request->tariff_type;
		$new_tariff->value =  $request->tariff_value;
		$new_tariff->lang = $request->tariff_lang;
		$new_tariff->show = $tariff_show;
		$new_tariff->save();

		return redirect()->route('tariffs', ['locale' => app()->getLocale()]);
	}


	public function deleteLayouts (Request $request) 
	{
		Layouts::where('layouts_id', $request->layoutID)->delete();
		return redirect()->route('layouts.colors', ['locale' => app()->getLocale()]);
	}

	public function addLayouts (Request $request) 
	{
		$maxLayouts = Layouts::max('layouts_id');
		++$maxLayouts;
		
		$logoFolder = 'elements/images/header_logo';
		$imgFolder = 'elements/images/header_img';
		$screanolder = 'elements/images/thumbs';
		
		$file = $request->file('layout_logo');
		$fileName = $file->getClientOriginalName();
		$file->move($logoFolder, $fileName);
		$header_logo = $logoFolder.'/'.$fileName;

		$file = $request->file('layout_img');
		$fileName = $file->getClientOriginalName();
		$file->move($imgFolder, $fileName);
		$header_img = $imgFolder.'/'.$fileName;

		$file = $request->file('layout_screan');
		$fileName = $file->getClientOriginalName();
		$file->move($screanolder, $fileName);
		$layout_screan = $screanolder.'/'.$fileName;

		$new_layouts = new Layouts;
		$new_layouts->layouts_id = $maxLayouts;
		$new_layouts->meta_key = 'head_color';
		$new_layouts->meta_value = $request->head_color;
		$new_layouts->screan =  $layout_screan;
		$new_layouts->logo = $header_logo;
		$new_layouts->img = $header_img;
		$new_layouts->parent = $request->parent;
		$new_layouts->save();

		$new_layouts = new Layouts;
		$new_layouts->layouts_id = $maxLayouts;
		$new_layouts->meta_key = 'title_color';
		$new_layouts->meta_value = $request->title_color;
		$new_layouts->save();

		$new_layouts = new Layouts;
		$new_layouts->layouts_id = $maxLayouts;
		$new_layouts->meta_key = 'text_color';
		$new_layouts->meta_value = $request->text_color;
		$new_layouts->save();

		return redirect()->route('layouts.colors', ['locale' => app()->getLocale()]);
	}

	public function pageLayouts (Request $request) {
		$layouts_id = Layouts::where('layouts_id', $request->id)->first(['img', 'logo']);
		return $layouts_id;
	}

	public function getColors() 
	{
		$templatesAll = Template::all();
		$templates = [];
		foreach ($templatesAll as $template) {

			$layout = Layouts:: where('layouts_id', $template->id)->get();
			if ( count($layout) == 0 ||  !$layout) {

				$mainLays = Layouts:: where('layouts_id', 9)->get();
				foreach ($mainLays as $main) {
					$layout = new Layouts; 
					$layout->layouts_id = $template->id;
					$layout->meta_key =  $main->meta_key;
					$layout->meta_value =$main->meta_value;
					$layout->save();
				}	
				
				$layout = Layouts:: where('layouts_id', $template->id)->get();
			}

		 	$templates[$template->name] = $layout;
		}
		
		return view('assets.layouts', compact( 'templates','templatesAll' ));

	}

	public function fotoLayouts(Request $request)
	{	
		$template = Template::where('id', $request->templateID)->first();
		$direction = $template->direction;
		$userFolder = 'elements/images/' .$request->keyImg  . '/'  . $direction->id ;
		$lang = 'ua';
        $userID = Auth::user()->id;
		$img = null;
		if ($request->imageFile) {
			$file = $request->imageFile;
			$img = $file->getClientOriginalName();
			$file->move($userFolder, $img);
		}
		
		Layouts::where('meta_key',  $request->keyImg)->where('layouts_id', $request->templateID )->update( ['meta_value' =>$userFolder.'/'. $img] );
		$lang = 'ua';
        $userID = Auth::user()->id;

		if( $request->templateID==15 ) {
            $skelet = 'elements/template_2/skeleton2.html';
        }
        elseif( $request->templateID==9 ) {
            $skelet = 'elements/template_1/skeleton1.html';
        }
        elseif( $request->templateID==16 ){
            $skelet = 'elements/template_3/skeleton3.html';
        }
        else {
            $skelet = 'elements/template_4/skeleton4.html';
        }

        $getFrames = TemplateMeta::where('meta_value', '<>', Null)
            ->where('template_id', $request->templateID)
            ->orderBy('meta_key',  'ASC')->get();

        $dom = new Dom;

        $dom->setOptions(

            (new Options())-> setRemoveScripts(false)
            ->setRemoveStyles(false)

        );       


        $dom->loadFromFile($skelet);
        $contents = $dom->getElementbyId('page');
        
        $content = '';

        $template = Template::where('id', $request->templateID)->first();
        $direction = $template->direction;
        foreach ($getFrames as $frame) {

            $element = Element::where('id', $frame->meta_value)->first();
            $category = Category::where('id',  $frame->meta_key)->first();
            $frameContent = new Dom;
            $frameContent->setOptions(

                (new Options())-> setRemoveStyles(false)
                -> setRemoveScripts(false)

            );

            $subNames = explode('/', $element->url);
            $nameFrame1 = strtok(array_pop($subNames), '.');
            $nameFrame2 = array_pop($subNames);
            $nameFrame = $nameFrame2 . '/'.$nameFrame1;
            $data = [];
            
            foreach ($category->getHistoryAttribute() as $paremeter) {
                $categoryParameter = Paremeter::where('id', $paremeter)->where('lang', 'ua')->first();

                $random_quote = Benefit::where('type', $categoryParameter->key)
                    ->where('lang', $categoryParameter->lang)
                    ->where('direction_id', $direction->id)
                    ->inRandomOrder()->first();

                $paramValue = null;

                if ($random_quote) {
                    $paramValue = $random_quote->desc;
                }

                if ($categoryParameter->key == 'social') {
                    $paramValue = [
                        [
                            'value' => '',
                            'img' => 'elements/images/icons/viber.svg',
                            'name' => 'Viber',
                            'placeholder' => 'viber://chat?number=380967146755',
                        ],
                       
                        [
                            'value' => '',
                            'img' => 'elements/images/icons/telegram.svg',
                            'name' => 'Telegram',
                            'placeholder' => 'https://t.me/username',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/whatsapp.svg',
                            'name' => 'Whatsapp',
                            'placeholder' => 'https://wa.me/380956987456',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/instagram.svg',
                            'name' => 'Instagram',
                            'placeholder' => 'https://www.instagram.com/uk.visa.service',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/facebook.svg',
                            'name' => 'Facebook',
                            'placeholder' => 'https://www.facebook.com/username',
                        ],
                    ];
                }

                if ($categoryParameter->key == 'benefitList') {

                    $data =Benefit::
                        where('parent_id', null)
                        ->where('type', 'benefit')
                        ->where('lang', $lang)
                        ->get();

                    $getBenefits = [];

                    foreach ($data as $key=>$value) {
                        $benefit = Benefit::find($value->id);

                        $getBenefits[] = [
                            'id' => $value->id,
                            'type'  => $value->type,
                            'img'  => 'elements/images/benefits/'. $value->img,
                            'desc' => $value->desc,
                            'children' => $benefit->children( $userID, $key)
                        ];

                    }
                    while (count($getBenefits)> 6 ) {
                       array_pop($getBenefits);
                    }
                    $paramValue = $getBenefits;
                }

                if ($categoryParameter->key == 'servicesList') {

                    $data =Benefit::
                        where('parent_id', null)
                        ->where('type', 'services')
                        ->where('lang', $lang)
                        ->get();

                    $getBenefits = [];

                    foreach ($data as  $key=>$value) {
                        $benefit = Benefit::find($value->id);
                        $benefitStatus = false;

                        if ( $key < 4) {
                            $benefitStatus = true;
                        }

                        $getBenefits[] = [
                            'id' => $value->id,
                            'type'  => $value->type,
                            'desc' => $value->desc,
                            'selected' =>  $benefitStatus,
                            'children' => $benefit->children( $userID, $key)
                        ];

                    }
                    while (count($getBenefits) >6 ) {
                        array_pop($getBenefits);
                    }
                  
                    $paramValue = $getBenefits;
                }

                if ($categoryParameter->key == 'section_name') {
                    $paramValue = $category->name;
                }

                if ($categoryParameter->key == 'reviews') {
                    $paramValue = [
                        0 => [
                            'name' => 'Іван',
                            'value' => 'Гарна стоматологія',
                        ]
                    ];
                }

                if ($categoryParameter->key == 'teams') {
                    $paramValue = [
                        0 => [
                            'name' => 'Іван Доктор',
                            'foto' => 'elements/images/img_8276.jpg',
                            'value' => 'Терапевт',
                        ]
                    ];
                }

                if ($categoryParameter->key == 'imgList') {
                    $paramValue = [];
                }

                if ($categoryParameter->key == 'about') {

                    $folder = 'elements/images/' . $categoryParameter->key.'/'. $direction->id;
                    $folderContent= File::allFiles($folder);
                    $imgRand =  array_rand( $folderContent);
                    $file =  $folderContent[$imgRand];
                    $filename = $folder . '/' . $file->getRelativePathname();
                    if ($folderContent) {
                        $paramValue = $filename;
                    }
                }

                if ($categoryParameter->key == 'header_logo') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_logo')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'header_img') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_img')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color1') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'head_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color2') {

                    $getImg = Layouts::where('layouts_id', $template->id)->where('meta_key', 'title_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color3') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'text_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'about') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'about')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }
                if ($categoryParameter->key=='teams' ) {
                        
                    $getParam = Layouts::where('layouts_id',$template->id)->where('meta_key', 'teams')->first();

                    if ($getParam) {
                        $paramValue = unserialize($getParam->meta_value);
                    }
                }

                if ($categoryParameter->key == 'menu') {                   
                    $paramValue[0] = [
                        'link'=> '#about',
                        'name'=> 'Про нас'
                    ];  
                    $paramValue[1] = [
                        'link'=> '#advantages',
                        'name'=> 'Переваги'
                    ];   
                    $paramValue[2] = [
                        'link'=> '#services',
                        'name'=> 'Послуги'
                    ];   
                    $paramValue[3] = [
                        'link'=> '#team',
                        'name'=> 'Команда'
                    ];   
                    $paramValue[4] = [
                        'link'=> '#gallery',
                        'name'=> 'Галерея'
                    ];   
                    $paramValue[5] = [
                        'link'=> '#reviews',
                        'name'=> 'Відгуки'
                    ];   
                    $paramValue[6] = [
                        'link'=> '#app',
                        'name'=> 'Футер'
                    ];                    
                }
                $data[$categoryParameter->key] =   $paramValue;    

                if ($category->id == 3 || $category->id == 4 || $category->id == 5 || $category->id == 10 || $category->id == 16 || $category->id == 17) {
                    $key = explode('#', $category->link);
                    $layout = Layouts::where('layouts_id', $template->id)->where('meta_key', $key[1])->first();
                    $data['background'] = $layout->meta_value;
                }       
                if ($category->id == 17) {
                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_logo')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                    $data['header_logo'] = $paramValue;
                }    
            }
            
       

            $blade = view('elements.'.$nameFrame, $data)->render();
                       
            $frameContent->loadStr($blade);            

            $content .= $frameContent->getElementbyId('page')->innerHtml;
        }
        
        $contents->firstChild()->setText($content);
       
        $file_name = 'website.zip';
        $file_pdf = new Filesystem;
        $file_pdf->cleanDirectory(public_path() . '/tmp');

        //return str_replace(' <!--headerIncludes-->', '', "<!DOCTYPE html>\n" . $dom->innerHtml);
        $zip = new ZipArchive();
        $zip->open(public_path() . '/tmp/' . $file_name, ZipArchive::CREATE);
        $zip->addFromString( "skelet.html",  $dom->innerHtml);
        $zip->close();

        $zip = new ZipArchive();
        $zip->open(public_path() . '/tmp/' . $file_name);
        $zip->extractTo('/var/www/dent/public/elements/template/'.$request->templateID);
        $zip->close();
		return redirect()->route('layouts.colors', app()->getLocale());
	}

	public function layoutsUpdate (Request $request) 
	{
		
		Layouts::where('meta_key',  $request->key)->where('layouts_id', $request->templateID)->update(['meta_value'=>  $request->value]);
		$lang = 'ua';
        $userID = Auth::user()->id;

		if( $request->templateID==15 ) {
            $skelet = 'elements/template_2/skeleton2.html';
        }
        elseif( $request->templateID==9 ) {
            $skelet = 'elements/template_1/skeleton1.html';
        }
        elseif( $request->templateID==16 ){
            $skelet = 'elements/template_3/skeleton3.html';
        }
        else {
            $skelet = 'elements/template_4/skeleton4.html';
        }

        $getFrames = TemplateMeta::where('meta_value', '<>', Null)
            ->where('template_id', $request->templateID)
            ->orderBy('meta_key',  'ASC')->get();

        $dom = new Dom;

        $dom->setOptions(

            (new Options())-> setRemoveScripts(false)
            ->setRemoveStyles(false)

        );       


        $dom->loadFromFile($skelet);
        $contents = $dom->getElementbyId('page');
        
        $content = '';

        $template = Template::where('id', $request->templateID)->first();
        $direction = $template->direction;
        foreach ($getFrames as $frame) {

            $element = Element::where('id', $frame->meta_value)->first();
            $category = Category::where('id',  $frame->meta_key)->first();
            $frameContent = new Dom;
            $frameContent->setOptions(

                (new Options())-> setRemoveStyles(false)
                -> setRemoveScripts(false)

            );

            $subNames = explode('/', $element->url);
            $nameFrame1 = strtok(array_pop($subNames), '.');
            $nameFrame2 = array_pop($subNames);
            $nameFrame = $nameFrame2 . '/'.$nameFrame1;
            $data = [];
            
            foreach ($category->getHistoryAttribute() as $paremeter) {
                $categoryParameter = Paremeter::where('id', $paremeter)->where('lang', 'ua')->first();

                $random_quote = Benefit::where('type', $categoryParameter->key)
                    ->where('lang', $categoryParameter->lang)
                    ->where('direction_id', $direction->id)
                    ->inRandomOrder()->first();

                $paramValue = null;

                if ($random_quote) {
                    $paramValue = $random_quote->desc;
                }

                if ($categoryParameter->key == 'social') {
                    $paramValue = [
                        [
                            'value' => '',
                            'img' => 'elements/images/icons/viber.svg',
                            'name' => 'Viber',
                            'placeholder' => 'viber://chat?number=380967146755',
                        ],
                       
                        [
                            'value' => '',
                            'img' => 'elements/images/icons/telegram.svg',
                            'name' => 'Telegram',
                            'placeholder' => 'https://t.me/username',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/whatsapp.svg',
                            'name' => 'Whatsapp',
                            'placeholder' => 'https://wa.me/380956987456',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/instagram.svg',
                            'name' => 'Instagram',
                            'placeholder' => 'https://www.instagram.com/uk.visa.service',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/facebook.svg',
                            'name' => 'Facebook',
                            'placeholder' => 'https://www.facebook.com/username',
                        ],
                    ];
                }

                if ($categoryParameter->key == 'benefitList') {

                    $data =Benefit::
                        where('parent_id', null)
                        ->where('type', 'benefit')
                        ->where('lang', $lang)
                        ->get();

                    $getBenefits = [];

                    foreach ($data as $key=>$value) {
                        $benefit = Benefit::find($value->id);

                        $getBenefits[] = [
                            'id' => $value->id,
                            'type'  => $value->type,
                            'img'  => 'elements/images/benefits/'. $value->img,
                            'desc' => $value->desc,
                            'children' => $benefit->children( $userID, $key)
                        ];

                    }
                    while (count($getBenefits)> 6 ) {
                       array_pop($getBenefits);
                    }
                    $paramValue = $getBenefits;
                }

                if ($categoryParameter->key == 'servicesList') {

                    $data =Benefit::
                        where('parent_id', null)
                        ->where('type', 'services')
                        ->where('lang', $lang)
                        ->get();

                    $getBenefits = [];

                    foreach ($data as  $key=>$value) {
                        $benefit = Benefit::find($value->id);
                        $benefitStatus = false;

                        if ( $key < 4) {
                            $benefitStatus = true;
                        }

                        $getBenefits[] = [
                            'id' => $value->id,
                            'type'  => $value->type,
                            'desc' => $value->desc,
                            'selected' =>  $benefitStatus,
                            'children' => $benefit->children( $userID, $key)
                        ];

                    }
                    while (count($getBenefits) >6 ) {
                        array_pop($getBenefits);
                    }
                  
                    $paramValue = $getBenefits;
                }

                if ($categoryParameter->key == 'section_name') {
                    $paramValue = $category->name;
                }

                if ($categoryParameter->key == 'reviews') {
                    $paramValue = [
                        0 => [
                            'name' => 'Іван',
                            'value' => 'Гарна стоматологія',
                        ]
                    ];
                }

                if ($categoryParameter->key == 'teams') {
                    $paramValue = [
                        0 => [
                            'name' => 'Іван Доктор',
                            'foto' => 'elements/images/img_8276.jpg',
                            'value' => 'Терапевт',
                        ]
                    ];
                }

                if ($categoryParameter->key == 'imgList') {
                    $paramValue = [];
                }

                if ($categoryParameter->key == 'about') {

                    $folder = 'elements/images/' . $categoryParameter->key.'/'. $direction->id;
                    $folderContent= File::allFiles($folder);
                    $imgRand =  array_rand( $folderContent);
                    $file =  $folderContent[$imgRand];
                    $filename = $folder . '/' . $file->getRelativePathname();
                    if ($folderContent) {
                        $paramValue = $filename;
                    }
                }

                if ($categoryParameter->key == 'header_logo') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_logo')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'header_img') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_img')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color1') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'head_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color2') {

                    $getImg = Layouts::where('layouts_id', $template->id)->where('meta_key', 'title_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color3') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'text_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'about') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'about')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }
                if ($categoryParameter->key=='teams' ) {
                        
                    $getParam = Layouts::where('layouts_id',$template->id)->where('meta_key', 'teams')->first();

                    if ($getParam) {
                        $paramValue = unserialize($getParam->meta_value);
                    }
                }

                if ($categoryParameter->key == 'menu') {                   
                    $paramValue[0] = [
                        'link'=> '#about',
                        'name'=> 'Про нас'
                    ];  
                    $paramValue[1] = [
                        'link'=> '#advantages',
                        'name'=> 'Переваги'
                    ];   
                    $paramValue[2] = [
                        'link'=> '#services',
                        'name'=> 'Послуги'
                    ];   
                    $paramValue[3] = [
                        'link'=> '#team',
                        'name'=> 'Команда'
                    ];   
                    $paramValue[4] = [
                        'link'=> '#gallery',
                        'name'=> 'Галерея'
                    ];   
                    $paramValue[5] = [
                        'link'=> '#reviews',
                        'name'=> 'Відгуки'
                    ];   
                    $paramValue[6] = [
                        'link'=> '#app',
                        'name'=> 'Футер'
                    ];                    
                }
                $data[$categoryParameter->key] =   $paramValue;    

                if ($category->id == 3 || $category->id == 4 || $category->id == 5 || $category->id == 10 || $category->id == 16 || $category->id == 17) {
                    $key = explode('#', $category->link);
                    $layout = Layouts::where('layouts_id', $template->id)->where('meta_key', $key[1])->first();
                    $data['background'] = $layout->meta_value;
                }       
                if ($category->id == 17) {
                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_logo')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                    $data['header_logo'] = $paramValue;
                }    
            }
            
       

            $blade = view('elements.'.$nameFrame, $data)->render();
                       
            $frameContent->loadStr($blade);            

            $content .= $frameContent->getElementbyId('page')->innerHtml;
        }
        
        $contents->firstChild()->setText($content);
       
        $file_name = 'website.zip';
        $file_pdf = new Filesystem;
        $file_pdf->cleanDirectory(public_path() . '/tmp');

        //return str_replace(' <!--headerIncludes-->', '', "<!DOCTYPE html>\n" . $dom->innerHtml);
        $zip = new ZipArchive();
        $zip->open(public_path() . '/tmp/' . $file_name, ZipArchive::CREATE);
        $zip->addFromString( "skelet.html",  $dom->innerHtml);
        $zip->close();

        $zip = new ZipArchive();
        $zip->open(public_path() . '/tmp/' . $file_name);
        $zip->extractTo('/var/www/dent/public/elements/template/'.$request->templateID);
        $zip->close();

        return Response::json($request);

	}

	public function refreshImg(Request $request)
	{
		$folderContentLogo = File::files('elements/images/' . $request->param);		
		$imgRand =  array_rand( $folderContentLogo, 1);
		$logo= $folderContentLogo[$imgRand];
		return $logo;	
	}

	public function refreshText($lang, Request $request)
	{
		$random_quote = Benefit::where('type', $request->param)->where('lang', $lang)->inRandomOrder()->first();	
		return $random_quote->desc;	
	}


	public function getAsset()
	{
		$directions = Direction::all();

		if (Auth::user()->type == 'admin') {
			//Get  Images
			$adminImages = array();
			$aboutImages = array();
			$headerImgImages = array();		
			$headerLogoImages = array();	
			
			
			$pathAdmin = 'elements/images/';
			
			if (file_exists($pathAdmin)) {

				$folderContentAdmin = File::files($pathAdmin); 
				foreach ($folderContentAdmin as $key => $item)
				{
					if( ! is_array($item))
					{						
						array_push($adminImages, $item);						
					}
				}				
			}

			foreach ($directions as $direction) {			
				

				$aboutImagesDeriction = [];
				$path = 'elements/images/about/'.$direction->id;
				
				if (file_exists($path)) {

					$folderContentAbout = File::files($path); 
					foreach ($folderContentAbout as $key => $item)
					{
						if( ! is_array($item))
						{						
							array_push($aboutImagesDeriction, $item);						
						}
					}				
				}
				$aboutImages[] = [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_pic' => $aboutImagesDeriction,
				];

				$headerImgImagesDeriction = [];
				$path_header_img = 'elements/images/header_img/'.$direction->id;
				
				if (file_exists($path_header_img)) {

					$folderContentHeaderImg = File::files($path_header_img); 
					foreach ($folderContentHeaderImg as $key => $item)
					{
						if( ! is_array($item))
						{						
							array_push($headerImgImagesDeriction, $item);						
						}
					}				
				}
				$headerImgImages[] = [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_pic' => $headerImgImagesDeriction,
				];

				$headerLogoImagesDeriction = [];
				$path_header_logo = 'elements/images/header_logo/'.$direction->id;
				
				if (file_exists($path_header_logo)) {

					$folderContentHeaderLogo = File::files($path_header_logo); 
					foreach ($folderContentHeaderLogo as $key => $item)
					{
						if( ! is_array($item))
						{						
							array_push($headerLogoImagesDeriction, $item);						
						}
					}				
				}
				$headerLogoImages[] = [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_pic' => $headerLogoImagesDeriction,
				];		

			}
			return view('assets/images', compact('adminImages', 'aboutImages', 'headerImgImages', 'headerLogoImages', 'directions'));
		}
		else {
			$userImages = array();
			$userID = Auth::user()->id;
			$images_uploadDir = Setting::where('name', 'images_uploadDir')->first();
			$aboutImages = [];
			$headerImgImages = [];
			$headerLogoImages = [];
			return view('assets/images-user', compact( 'aboutImages', 'headerImgImages', 'headerLogoImages', 'directions'));

		}
	}

	/**
	 * Upload image file
	 * @param  Request $request
	 */
	public function uploadImage(Request $request)
	{	
		//return $request;
		if ($request->userFile)	{
			
			//User upload directory
			$userID = Auth::user()->id;			
			$block_name = $request->block_name;
			$direction_id = $request->direction_id;
			if ($block_name == 'my_img') {
				$userFolder = 'images/uploads/' . $userID. '/'. $direction_id;
			}
			else  {
				$userFolder = 'images/'. $block_name . '/'. $direction_id;
			}				
		
			foreach ($request->userFile as $userFile) {

				$data = substr($userFile, strpos($userFile, ',') + 1);
				$fileName = time(). '.png';
				$data = base64_decode($data);
				Storage::disk('public')->put($userFolder . '/' . $fileName, $data);		
			
			}		
			
		}
		else {
			$request->session()->flash('error', 'There was an error in upload image!');				
		}
		
		return redirect()->route('assets', app()->getLocale());
	}

	public function imageUploadAjax(Request $request)
	{

		//die(json_encode($request));
		if ($request->hasFile('imageFile'))
		{
			//User upload directory
			$userID = Auth::user()->id;
			$images_uploadDir = Setting::where('name', 'images_uploadDir')->first();
			$block_name = $request->block_name;
			if ($block_name == 'my_img') {
				$userFolder = $images_uploadDir->value . '/' . $userID;
			}
			else  {
				$userFolder = 'elements/images/'. $block_name ;
			}
			

			//Check if the file extension is valid
			$allowedExt = Setting::where('name', 'images_allowedExtensions')->first();
			$tempExt = explode('|', $allowedExt->value);
			$file = $request->file('imageFile');
			$ext = File::extension($file->getClientOriginalName());

			if (in_array($ext, $tempExt))
			{
				if ($file->move($userFolder, $file->getClientOriginalName()))
				{
					$return = array();
					$temp = array();
					$temp['header'] = 'All set!';
					$temp['content'] = 'Your image was uploaded successfully and can now be found under the \'My Images\' tab.';
					//include the partials "myimages" with all the uploaded images
					$userFolderContent = directory_map($userFolder, 2);
					if ($userFolderContent)
					{
						$userImages = array();
						foreach ($userFolderContent as $userKey => $userItem)
						{
							if ( ! is_array($userItem))
							{
								// Check the file extension
								$ext = pathinfo($userItem, PATHINFO_EXTENSION);
								// Prepared allowed extensions file array
								if (in_array($ext, $tempExt))
								{
									array_push($userImages, $userItem);
								}
							}
						}
					}
					if (isset($userImages))
					{
						$elementsDir = Setting::where('name', 'elements_dir')->first();
						$uploadDir = Setting::where('name', 'images_uploadDir')->first();
						$userSrc = url('/') . '/' . $userFolder;
						$dataURL = str_replace($elementsDir->value . '/', '', $uploadDir->value);
						$myImages = View('partials.myimages', array('userImages' => $userImages, 'userSrc' => $userSrc, 'dataURL' => $dataURL));
						$return['myImages'] = $myImages->render();
					}
					$return['responseCode'] = 1;
					$view = View('partials.success', array('data' => $temp));
					$return['responseHTML'] = $view->render();
					die(json_encode($return));
				}
				else
				{
					$temp = array();
					$temp['header'] = 'Ouch! Something went wrong:';
					$temp['content'] = 'Something went wrong when trying to upload your image.';
					$return = array();
					$return['responseCode'] = 0;
					$view = View('partials.error', array('data' => $temp));
					$return['responseHTML'] = $view->render();
					die(json_encode($return));
				}
			}
			else
			{
				$temp = array();
				$temp['header'] = 'Ouch! Something went wrong:';
				$temp['content'] = 'Something went wrong when trying to upload your image.';
				$return = array();
				$return['responseCode'] = 0;
				$view = View('partials.error', array('data' => $temp));
				$return['responseHTML'] = $view->render();
				die(json_encode($return));
			}
		}

	}

	/**
	 * Delete image file of user with ajax request
	 */
	public function delImage()
	{
		if (isset($_POST['file']) && $_POST['file'] != '')
		{		
		
			$userID = Auth::user()->id;
			//disect the URL
			$temp = explode("/", $_POST['file']);
			$fileName = array_pop( $temp );
			$userDirID = array_pop( $temp );
			unset($temp[0]);
			unset($temp[1]);
			unset($temp[2]);
			$temp = implode("/", $temp);			
			//make sure this is the user's images
			if ($userID == $userDirID)
			{
				//all good, remove!
				$images_uploadDir = Setting::where('name', 'images_uploadDir')->first();
				unlink($temp .'/'.$fileName);
				
			}
			elseif ($temp == 'images'){
				$image_path = './elements/images/'.$fileName;
				
				unlink($temp. '/'.$fileName);
			}
			else {
				$image_path = './elements/images/'.$userDirID.'/'.$fileName;
				
				unlink($temp.'/'.$userDirID. '/'.$fileName);
			
			}
		}		
		return redirect()->route('assets', app()->getLocale());
	}
	
	
	public	function imageUploadElement (Request $request) {

		if ($request->hasFile('imageFile')) {
			
			$file = $request->file('imageFile');		
			$userFolder = 'elements/images/thumbs';
			if ($file->move($userFolder, $file->getClientOriginalName())) { 
				
				$loadFile = $userFolder .'/'. $file->getClientOriginalName();
				$element = Element::where('id', $request->loadElementID)->first();
				$element->thumbnail = $loadFile;
				$element->update();
			
			}
		}
	
		return redirect()->route('elements');
	}

	public	function imageUploadTemplate (Request $request) {

		if ($request->hasFile('imageFile')) {
			
			$file = $request->file('imageFile');		
			$userFolder = 'elements/images/thumbs';
			if ($file->move($userFolder, $file->getClientOriginalName())) { 
				
				$loadFile = $userFolder .'/'. $file->getClientOriginalName();
				$element = Template::where('id', $request->loadElementID)->first();
				$element->img = $loadFile;
				$element->update();
			
			}
		}
	
		return redirect()->route('template', ['template'=>$request->loadElementID]);
	}
}