<?php

namespace App\Http\Controllers;

use App\Template;
use App\TemplateMeta;
use App\Element;
use App\Category;
use App\Paremeter;
use App\Direction;
use App\Benefit;
use App\Layouts;
use File;
use Illuminate\Support\Facades\Auth;
use App\Events\NewMessage;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Options;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Filesystem\Filesystem;

use Illuminate\Support\Facades\Broadcast;

use Illuminate\Http\Request;
use ZipArchive;

class TemplateController extends Controller
{
    public function getTemplate ()
	{
        $getTemplates = Template::all();
        $directions = Direction::all();
        $templates = [];
        foreach ($getTemplates as $template){

            $templates[] = [
                'id' => $template->id,
                'name' =>$template->name,
                'direction' =>$template->direction
            ];
        }

		return view('templates.templates', compact('templates', 'directions'));
    }

    public function getTemplateData ( $template)
	{
        $msg= '';
        $success = broadcast(new NewMessage($msg));

        $getTemplate = Template::where('id', $template)->first();
        $getTemplatDirections = $getTemplate->direction;
        $directions = Direction::where('id', $getTemplatDirections['id'])->first();;
        $categories = $directions->getHistoryAttribute();
        
        $elementsCategory = [];
        $templateMeta=[];
        $elementsUnSlected =Element::where('category_id', null)->get();
        foreach ($categories as $categoryID) {
            $category = Category::where('id', $categoryID)->first();
            $elements = $category->elements;
            if ( count($elements)>0) {
                $elementsSlected = [];
                foreach ($elements as $element) {
                    if ($element['show']==1) {
                        $elementsSlected[] = [
                            'id' => $element['id'],
                            'name' =>$element['name'],
                            'thumbnail' =>$element['thumbnail'],
                            'url' =>$element['url'],
                            'category' =>$category->id
                        ];
                    }

                }
                $tMeta = TemplateMeta::where('template_id', $template)
                    ->where('meta_key', $category->id)
                    ->whereNotNull('meta_value')->first();

                if ($tMeta) {
                    $templateMeta[] = [
                        'id' => $category->id,
                        'name' =>$category->name,
                        'element' => $tMeta->meta_value,
                        'type' => $tMeta->type,
                        'elements' =>Element::where('id', $tMeta->meta_value)->first()->toArray(),
                    ];
                }
                else {
                    $templateMeta[] = [
                        'id' => $category->id,
                        'name' =>$category->name,
                        'element' => '',
                        'elements' => '',
                    ];
                }

                $elementsCategory[] = [
                    'id' => $category->id,
                    'name' =>$category->name  ,
                    'elements' =>$elementsSlected,
                ];
            }
        }

		return view('templates.create', compact('getTemplate', 'templateMeta', 'elementsCategory', 'getTemplatDirections'));
    }

    public function addTemplate (Request $request)
	{
		$template = new Template;
        $template->name = $request->name;
        $template->direction_id = $request->direction_id;      
        $template->save();

        $template->demo = 'elements/template/'.$template->id.'/skelet.html';
        $template->update();

        $categories = Category::all();

        foreach ( $categories as $category) {
            $templateMeta = new TemplateMeta;
            $templateMeta->template_id = $template->id;
            $templateMeta->meta_key = $category->id;
            $templateMeta->meta_value = null;
            $templateMeta->save();
        }

        $templates = Template::all();
        return redirect()->route('templates');
    }

    public function deleteTemplate (Request $request)
	{
        Template::where('id', $request->template)->delete();
        TemplateMeta::where('template_id', $request->template)->delete();
        return redirect()->route('templates', ['locale' => app()->getLocale()]);
    }

    public function updateTemplate (Request $request)
	{
        Template::where('id', $request->elementID)
            ->update([
                'name'=> $request->elementName
            ]);

        $templates = Template::all();
		return $templates;
    }


    public function deleteAjaxMeta (Request $request)
	{
        $tMeta = TemplateMeta::where('template_id', $request->template_id)->where('meta_key', $request->meta_key)->first() ;
        $tMeta->meta_value = null;
        $tMeta->update();
        return true;
    }

    public function addAjaxMeta (Request $request)
	{
        $tMeta = TemplateMeta::where('template_id', $request->template_id)->where('meta_key', $request->meta_key)->first() ;
        $lang = 'ua';
        $userID = Auth::user()->id;
        if($tMeta) {
            $tMeta->meta_value = $request->meta_value;
            $tMeta->update();
        }
        else {
            $tMeta = new TemplateMeta;
            $tMeta->template_id = $request->template_id;
            $tMeta->meta_key = $request->meta_key;
            $tMeta->meta_value = $request->meta_value;
            $tMeta->save();
        }

        if( $request->template_id==15 ) {
            $skelet = 'elements/template_2/skeleton2.html';
        }
        elseif( $request->template_id==9 ) {
            $skelet = 'elements/template_1/skeleton1.html';
        }
        elseif( $request->template_id==16 ){
            $skelet = 'elements/template_3/skeleton3.html';
        }
        else {
            $skelet = 'elements/template_4/skeleton4.html';
        }

        $getFrames = TemplateMeta::where('meta_value', '<>', Null)
            ->where('template_id', $request->template_id)
            ->orderBy('meta_key',  'ASC')->get();

        $dom = new Dom;

        $dom->setOptions(

            (new Options())-> setRemoveScripts(false)
            ->setRemoveStyles(false)

        );       


        $dom->loadFromFile($skelet);
        $contents = $dom->getElementbyId('page');
        
        $content = '';

        $template = Template::where('id', $request->template_id)->first();
        $direction = $template->direction;
        foreach ($getFrames as $frame) {

            $element = Element::where('id', $frame->meta_value)->first();
            $category = Category::where('id',  $frame->meta_key)->first();
            $frameContent = new Dom;
            $frameContent->setOptions(

                (new Options())-> setRemoveStyles(false)
                -> setRemoveScripts(false)

            );

            $subNames = explode('/', $element->url);
            $nameFrame1 = strtok(array_pop($subNames), '.');
            $nameFrame2 = array_pop($subNames);
            $nameFrame = $nameFrame2 . '/'.$nameFrame1;
            $data = [];
            
            foreach ($category->getHistoryAttribute() as $paremeter) {
                $categoryParameter = Paremeter::where('id', $paremeter)->where('lang', 'ua')->first();

                $random_quote = Benefit::where('type', $categoryParameter->key)
                    ->where('lang', $categoryParameter->lang)
                    ->where('direction_id', $direction->id)
                    ->inRandomOrder()->first();

                $paramValue = null;

                if ($random_quote) {
                    $paramValue = $random_quote->desc;
                }

                if ($categoryParameter->key == 'social') {
                    $paramValue = [
                        [
                            'value' => '',
                            'img' => 'elements/images/icons/viber.svg',
                            'name' => 'Viber',
                            'placeholder' => 'viber://chat?number=380967146755',
                        ],
                       
                        [
                            'value' => '',
                            'img' => 'elements/images/icons/telegram.svg',
                            'name' => 'Telegram',
                            'placeholder' => 'https://t.me/username',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/whatsapp.svg',
                            'name' => 'Whatsapp',
                            'placeholder' => 'https://wa.me/380956987456',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/instagram.svg',
                            'name' => 'Instagram',
                            'placeholder' => 'https://www.instagram.com/uk.visa.service',
                        ],
                         [
                            'value' => '',
                            'img' => 'elements/images/icons/facebook.svg',
                            'name' => 'Facebook',
                            'placeholder' => 'https://www.facebook.com/username',
                        ],
                    ];
                }

                if ($categoryParameter->key == 'benefitList') {

                    $data =Benefit::
                        where('parent_id', null)
                        ->where('type', 'benefit')
                        ->where('lang', $lang)
                        ->get();

                    $getBenefits = [];

                    foreach ($data as $key=>$value) {
                        $benefit = Benefit::find($value->id);

                        $getBenefits[] = [
                            'id' => $value->id,
                            'type'  => $value->type,
                            'img'  => 'elements/images/benefits/'. $value->img,
                            'desc' => $value->desc,
                            'children' => $benefit->children( $userID, $key)
                        ];

                    }
                    while (count($getBenefits)> 6 ) {
                       array_pop($getBenefits);
                    }
                    $paramValue = $getBenefits;
                }

                if ($categoryParameter->key == 'servicesList') {

                    $data =Benefit::
                        where('parent_id', null)
                        ->where('type', 'services')
                        ->where('lang', $lang)
                        ->get();

                    $getBenefits = [];

                    foreach ($data as  $key=>$value) {
                        $benefit = Benefit::find($value->id);
                        $benefitStatus = false;

                        if ( $key < 4) {
                            $benefitStatus = true;
                        }

                        $getBenefits[] = [
                            'id' => $value->id,
                            'type'  => $value->type,
                            'desc' => $value->desc,
                            'selected' =>  $benefitStatus,
                            'children' => $benefit->children( $userID, $key)
                        ];

                    }
                    while (count($getBenefits) >6 ) {
                        array_pop($getBenefits);
                    }
                  
                    $paramValue = $getBenefits;
                }

                if ($categoryParameter->key == 'section_name') {
                    $paramValue = $category->name;
                }

                if ($categoryParameter->key == 'reviews') {
                    $paramValue = [
                        0 => [
                            'name' => 'Іван',
                            'value' => 'Гарна стоматологія',
                        ]
                    ];
                }

                if ($categoryParameter->key == 'teams') {
                    $paramValue = [
                        0 => [
                            'name' => 'Іван Доктор',
                            'foto' => 'elements/images/img_8276.jpg',
                            'value' => 'Терапевт',
                        ]
                    ];
                }

                if ($categoryParameter->key == 'imgList') {
                    $paramValue = [];
                }

                if ($categoryParameter->key == 'about') {

                    $folder = 'elements/images/' . $categoryParameter->key.'/'. $direction->id;
                    $folderContent= File::allFiles($folder);
                    $imgRand =  array_rand( $folderContent);
                    $file =  $folderContent[$imgRand];
                    $filename = $folder . '/' . $file->getRelativePathname();
                    if ($folderContent) {
                        $paramValue = $filename;
                    }
                }

                if ($categoryParameter->key == 'header_logo') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_logo')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'header_img') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_img')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color1') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'head_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color2') {

                    $getImg = Layouts::where('layouts_id', $template->id)->where('meta_key', 'title_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'color3') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'text_color')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }

                if ($categoryParameter->key == 'about') {

                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'about')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                }
                if ($categoryParameter->key=='teams' ) {
                        
                    $getParam = Layouts::where('layouts_id',$template->id)->where('meta_key', 'teams')->first();

                    if ($getParam) {
                        $paramValue = unserialize($getParam->meta_value);
                    }
                }

                if ($categoryParameter->key == 'menu') {                   
                    $paramValue[0] = [
                        'link'=> '#about',
                        'name'=> 'Про нас'
                    ];  
                    $paramValue[1] = [
                        'link'=> '#advantages',
                        'name'=> 'Переваги'
                    ];   
                    $paramValue[2] = [
                        'link'=> '#services',
                        'name'=> 'Послуги'
                    ];   
                    $paramValue[3] = [
                        'link'=> '#team',
                        'name'=> 'Команда'
                    ];   
                    $paramValue[4] = [
                        'link'=> '#gallery',
                        'name'=> 'Галерея'
                    ];   
                    $paramValue[5] = [
                        'link'=> '#reviews',
                        'name'=> 'Відгуки'
                    ];   
                    $paramValue[6] = [
                        'link'=> '#app',
                        'name'=> 'Футер'
                    ];                    
                }
                $data[$categoryParameter->key] =   $paramValue;    

                if ($category->id == 3 || $category->id == 4 || $category->id == 5 || $category->id == 10 || $category->id == 16 || $category->id == 17) {
                    $key = explode('#', $category->link);
                    $layout = Layouts::where('layouts_id', $template->id)->where('meta_key', $key[1])->first();
                    $data['background'] = $layout->meta_value;
                }       
                if ($category->id == 17) {
                    $getImg = Layouts::where('layouts_id',$template->id)->where('meta_key', 'header_logo')->first();

                    if ($getImg) {
                        $paramValue = $getImg->meta_value;
                    }
                    $data['header_logo'] = $paramValue;
                }    
            }
            
       

            $blade = view('elements.'.$nameFrame, $data)->render();
                       
            $frameContent->loadStr($blade);            

            $content .= $frameContent->getElementbyId('page')->innerHtml;
        }
        
        $contents->firstChild()->setText($content);
       
        $file_name = 'website.zip';
        $file_pdf = new Filesystem;
        $file_pdf->cleanDirectory(public_path() . '/tmp');

        //return str_replace(' <!--headerIncludes-->', '', "<!DOCTYPE html>\n" . $dom->innerHtml);
        $zip = new ZipArchive();
        $zip->open(public_path() . '/tmp/' . $file_name, ZipArchive::CREATE);
        $zip->addFromString( "skelet.html",  $dom->innerHtml);
        $zip->close();

        $zip = new ZipArchive();
        $zip->open(public_path() . '/tmp/' . $file_name);
        $zip->extractTo('/var/www/dent/public/elements/template/'.$request->template_id);
        $zip->close();



        return true;
        //return ['templateMeta' => $templateMeta, 'addElements' =>$addElements];
    }

    public function templateElementType (Request $request) {
        $tMeta =  TemplateMeta::
            where('template_id', $request->templateID)->
            where('meta_key', $request->categoryID)->
            where('meta_value', $request->elementID)->first();
        if ($request->type=='0') {
            $type = null;
        }
        else {
           $type = $request->type;
        }
        $tMeta->type =  $type;
        $tMeta->update();
    }

}
