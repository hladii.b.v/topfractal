<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SitePage;
use App\Site;
use App\Paremeter;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller 
{
    
    public function index()
    {       
        if (Auth::user()->type != 'admin') {			
			return redirect()->route('dashboard.new', ['locale'=> app()->getLocale(), 'status'=>1 ] );
			
        }
        
        $page_content = []; 
        $pages = SitePage::groupBy('page_name')->get('page_name');
        $lang = SitePage::groupBy('lang')->get('lang', 'hide_lang');
       
        foreach ($pages as $page) {

            foreach ($lang as $item) {
                $page_content[$page->page_name][$item->lang]  = SitePage::content( $page->page_name,   $item->lang);
            }

        }
        $langParemeter = Paremeter::groupBy('lang')->get('lang');

      
        foreach ($langParemeter as $item) {            
            $page_content['site_create'][$item->lang]  =  Paremeter::where('lang', $item->lang )->get();
        }      

        $pagesNew = SitePage::where('page_name', 'profile')->get();

        // foreach ( $pagesNew as $value) {
        //     $newContent = new SitePage;

        //     $newContent->page_name = $value->page_name;
        //     $newContent->page_key = $value->page_key;
        //     $newContent->page_text = $value->page_text;
        //     $newContent->lang = 'ru';

        //     $newContent->save();
        // }

        return view('page-edit.dashboard', compact('page_content', 'lang'));        
    }
    public function updateText(Request $request) 
    { 
        
        $content = SitePage::where('id', $request->text_id)->first();
        $content->page_text = $request->text;
        $content->update();
        return $content;
        //return redirect()->route('page.content.dashboard')  ; 
    } 

    public function updateCreateText(Request $request) 
    {
        
        $content = Paremeter::where('id', $request->text_id)->first();
        $content->name = $request->text;
        $content->update();
        return redirect()->route('page.content.dashboard'); 
    } 


    public function clearText( $id) 
    {
        $content = SitePage::where('id', $id)->first();
        $content->page_text = '';
        $content->update();
        return redirect()->route('dashboard', ['status' => 1])  ; 
    } 

    public function getSitecount () 
    {
        if (Auth::user()->type != 'admin') {			
			$publishSite = Site::where('user_id', Auth::user()->id )->where('site_trashed', 0)->whereNotNull('ftp_published')->count();
            $notPublishSite = Site::where('site_trashed', 0)->whereNull('ftp_published')->count();			
        }
        else {
            $publishSite = Site::where('site_trashed', 0)->whereNotNull('ftp_published')->count();
            $notPublishSite = Site::where('site_trashed', 0)->whereNull('ftp_published')->count();
        }       
        $countArray = [
            'publishSite'=>$publishSite,
            'notPublishSite'=>$notPublishSite
        ];
        return $countArray; 
    } 

    public function addLang (Request $request) {
        $pagesNew = SitePage::where('lang', 'ua')->get();       

        foreach ( $pagesNew as $value) {
            $newContent = new SitePage;

            $newContent->page_name = $value->page_name;
            $newContent->page_key = $value->page_key;
            $newContent->page_text = $value->page_text;
            $newContent->lang = $request->create_name;

            $newContent->save();
        }

        return redirect()->route('page.content.dashboard')  ; 
    }

    public function daleteLang (Request $request) {
        
        if( $request->lang == 'ua') {
            return redirect()->route('page.content.dashboard')  ; 
        }

        SitePage::where('lang', $request->lang)->delete();

        return redirect()->route('page.content.dashboard')  ; 
    }

    public function updateLang (Request $request) {

        if( $request->lang == 'ua') {
            return $request; 
        }
        if ( $request->show == 'false') {
          
            SitePage::where('lang', $request->lang)->update(['hide_lang'=>1]);
        }
        elseif( $request->show == true) {
          
            SitePage::where('lang', $request->lang)->update(['hide_lang'=>Null]);
        }

       
    }

}
