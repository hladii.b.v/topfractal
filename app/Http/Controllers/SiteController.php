<?php

namespace App\Http\Controllers;

use App\User;
use App\Benefit;
use App\Category;
use App\LiqPay;
use App\Site;
use App\Page;
use App\Frame;
use App\Setting;
use App\Layouts;
use App\Payment;
use App\Tariff;
use App\Service;
use App\Template;
use App\Element;
use App\Direction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;
use DB;
use Sunra\PhpSimple\HtmlDomParser;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use App\CI_Library\FTP\CI_FTP;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Http;
use Symfony\Component\Process\Process;
use App\Utils;
use Carbon\Carbon;
use Redirect,Response;
use Illuminate\Filesystem\Filesystem;

class SiteController extends Controller
{
	public function getDomain(Request $request)
	{

        $data =Site::all();

       return view('assets.domain', compact('data'));

	}
 
	public function getText()
	{
		$directions = Direction::all(); 
		

		foreach ($directions as $direction) {	
			$organizationNameArray = [];
			$headerTitleArray =[];
			$headerMottoArray =[];
			$headerTextArray =[];
			$aboutTextArray =[];
			$benefitTextArray =[];
			$reviewsTextArray =[];
			$adressTextArray =[];
			$teamMottoArray =[];
			$teamTextArray =[];


			$organizationNameArray = Benefit::where('type', 'organizationName')->where('direction_id', $direction->id)->get();
		
			$organizationName[] =  [
				'direction_id' => $direction->id,
				'direction_name' => $direction->name,
				'direction_items' => $organizationNameArray,
			];
			

			$headerTitleArray = Benefit::where('type', 'headerTitle')->where('direction_id', $direction->id)->get();
		
				$headerTitle[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $headerTitleArray,
				];
			

			$benefitMottoArray = Benefit::where('type', 'benefitMotto')->where('direction_id', $direction->id)->get();
		
				$benefitMotto[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $benefitMottoArray,
				];
			

			$headerTextArray = Benefit::where('type', 'headerText')->where('direction_id', $direction->id)->get();
			
				$headerText[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $headerTextArray,
				];
		

			$aboutTextArray = Benefit::where('type', 'aboutText')->where('direction_id', $direction->id)->get();
			
				$aboutText[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $aboutTextArray,
				];
			

			$benefitTextArray = Benefit::where('type', 'benefitText')->where('direction_id', $direction->id)->get();
		
				$benefitText[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $benefitTextArray,
				];
			

			$reviewsTextArray = Benefit::where('type', 'reviewsText')->where('direction_id', $direction->id)->get();
			
				$reviewsText[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $reviewsTextArray,
				];
			

			$adressTextArray = Benefit::where('type', 'adressText')->where('direction_id', $direction->id)->get();
			
				$adressText[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $adressTextArray,
				];

			$teamMottoArray = Benefit::where('type', 'teamMotto')->where('direction_id', $direction->id)->get();
			
				$teamMotto[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $teamMottoArray,
				];

			$teamTextArray = Benefit::where('type', 'teamText')->where('direction_id', $direction->id)->get();
			
				$teamText[] =  [
					'direction_id' => $direction->id,
					'direction_name' => $direction->name,
					'direction_items' => $teamTextArray,
				];
			
		}
	
				
		return view('assets/text', compact('teamText', 'teamMotto', 'directions','organizationName','headerTitle','benefitText','benefitMotto','headerText', 'aboutText', 'reviewsText', 'adressText'));
	}

	public function selectDirection(Request $request)
	{
		$direction = Direction::where( 'id', $request->directionId )->first();
		$categoryid= $direction->getHistoryAttribute(); 
		$text_type = [];
		$i = 0;
		foreach ($categoryid as $category) {

			$getCategory = Category::where('id', $category)->first();
			$categoryParam[] = $getCategory->getHistoryAttribute();

			if( in_array("organizationName", $getCategory->getHistoryAttribute() ) && !in_array("headerTitle", $text_type )) {
				$text_type[$i] = 'organizationName';
				$i++;
			}
			if( in_array("headerTitle", $getCategory->getHistoryAttribute() ) &&  !in_array("headerTitle", $text_type )) {
				$text_type[$i] = 'headerTitle';
				$i++;
			}
			if( in_array("headerMotto", $getCategory->getHistoryAttribute() ) &&  !in_array("headerMotto", $text_type )) {
				$text_type[$i] = 'headerMotto';
				$i++;
			}
			if( in_array("headerText", $getCategory->getHistoryAttribute() ) &&  !in_array("headerText", $text_type )) {
				$text_type[$i] = 'headerText';
				$i++;
			}
			if( in_array("aboutText", $getCategory->getHistoryAttribute() ) &&  !in_array("aboutText", $text_type )) {
				$text_type[$i] = 'aboutText';
				$i++;
			}
			if( in_array("benefitText", $getCategory->getHistoryAttribute() ) &&  !in_array("benefitText", $text_type )) {
				$text_type[$i] = 'benefitText';
				$i++;
			}
			if( in_array("reviewsText", $getCategory->getHistoryAttribute() ) &&  !in_array("reviewsText", $text_type )) {
				$text_type[$i] = 'reviewsText';
				$i++;
			}
			if( in_array("adressText", $getCategory->getHistoryAttribute() ) &&  !in_array("adressText", $text_type )) {
				$text_type[$i] = 'adressText';
				$i++;
			}
		}

		return $text_type;
	}

	public function addText(Request $request)
	{

		$new_benefit = new Benefit;
		$new_benefit->lang = $request->text_lang;
		$new_benefit->type = $request->text_type;
		$new_benefit->desc = $request->desc;
		$new_benefit->direction_id = $request->direction_id;
		$new_benefit->save();
		return redirect()->route('text', app()->getLocale());
	}

	public function deleteText(Request $request)
	{

		 Benefit::where('id', $request->benefit)->delete();

		 return redirect()->route('text', app()->getLocale());
	}

	public function ServicesBenefits($lang,Request $request)
	{
		$services = Benefit::
				where('parent_id', null)
				->where('type', 'services')
				->where('lang', $lang);

		$data =Benefit::
				where('parent_id', null)
				->where('type', 'benefit')
				->where('lang', $lang)
				->union($services)
				->get();

		$userID = Auth::user()->id;
		$response = [];
		
       foreach ($data as $value) {
			$benefit = Benefit::find($value->id);		
			
        	$response[] = [
        		'id' => $value->id,
        		'type'  => $value->type,
        		'desc' => $value->desc,
        		'children' => $benefit->children( $userID)
			];

		}
		//dd($response);
        return Response::json($response);

	}

	public function parentServices(Request $request)
	{
		$userID = Auth::user()->id;
		$benefit = Benefit::find($request->service);	
        $data = $benefit->childrenAdmin( $userID);

        return Response::json($data);

	}

	public function benefitsUpdate (Request $request)
	{

        Benefit::where('id', $request->benefit)->update(['desc'=>$request->desc]);

        return Response::json($request);

	}
	public function Benefits()
	{
		$directions = Direction::all();
		$benefitsAll = Benefit::where('type', 'benefit')
			->where('parent_id', null)->get();

		foreach ($directions as $direction) {
			$benefitsArray = [];
			$benefitsArray = Benefit::where('type', 'benefit')
				->where('parent_id', null)
				->where('direction_id', $direction->id)->get();
			$benefits[] =  [
				'direction_id' => $direction->id,
				'direction_name' => $direction->name,
				'direction_items' => $benefitsArray,
			];
		}
		
		return view('assets.benefits', compact('benefits', 'benefitsAll' , 'directions'));
	}

	public function deleteBenefits(Request $request)
	{

		 Benefit::where('id', $request->benefit)->delete();


		return redirect()->route('benefits', app()->getLocale());
	}

	public function fotoBenefits(Request $request)
	{
		$userFolder = 'elements/images/benefits' ;
		$img = null;
		if ($request->hasFile('userFile')) {
			$file = $request->file('userFile');
			$img = $file->getClientOriginalName();
			$file->move($userFolder, $img);
		}
		Benefit::where('id', $request->benefitID )->update( ['img' => $img] );
		return redirect()->route('benefits', app()->getLocale());
	}

	public function addBenefits(Request $request)
	{

		$userFolder = 'elements/images/benefits' ;
		$img = null;
		$parent_id = null;

		if ( $request->parent_id == 0) {

			if ($request->hasFile('userFile')) {
				$file = $request->file('userFile');
				$img = $file->getClientOriginalName();
				$file->move($userFolder, $img);
			}
		}
		else {
			$parent_id = $request->parent_id;
		}

		$new_benefit = new Benefit;
		$new_benefit->type = 'benefit';
		$new_benefit->desc = $request->desc;
		$new_benefit->lang = $request->text_lang;
		$new_benefit->parent_id = $parent_id;
		$new_benefit->img = $img;
		$new_benefit->direction_id = $request->direction_id;
		$new_benefit->save();
		return redirect()->route('benefits', app()->getLocale());
	}

	public function addUserBenefits($lang, Request $request)
	{ 
		
		$userID = Auth::user()->id;
		if ($request->param == '') {
			return 'error';
		}
		else {
			$new_benefit = new Benefit;
			$new_benefit->type = $request->type;
			$new_benefit->lang = $lang;
			$new_benefit->desc = $request->param;
			$new_benefit->parent_id = $request->id;
			$new_benefit->user_id = $userID;
			$new_benefit->save();
			$response = [
				'id' => $new_benefit->id,
				'desc' => $new_benefit->desc,
				'parent_id' => $request->id,
			];
			return $response;
		}

	}

	public function Services()
	{
		$directions = Direction::all();
		$servicesAll = Benefit::where('type', 'services')
			->where('parent_id', null)->get();

		foreach ($directions as $direction) {
			$benefitsArray = [];
			$benefitsArray = Benefit::where('type', 'services')
				->where('parent_id', null)
				->where('direction_id', $direction->id)->get();
			$services[] =  [
				'direction_id' => $direction->id,
				'direction_name' => $direction->name,
				'direction_items' => $benefitsArray,
			];
		}
		
		return view('assets.services', compact('services', 'servicesAll' , 'directions'));
		
	}
	public function deleteServices(Request $request)
	{

		Benefit::where('id', $request->service)->delete();
		$services = Benefit::where('type', 'services')->where('parent_id', null)->get();

		return redirect()->route('services', app()->getLocale());
	}

	public function addServices(Request $request)
	{

		if ( $request->parent_id == 0) {
			$parent_id = null;
		}
		else {
			 $parent_id = $request->parent_id;
		}

		$new_benefit = new Benefit;
		$new_benefit->type = 'services';
		$new_benefit->desc = $request->desc;
		$new_benefit->lang = $request->text_lang;
		$new_benefit->parent_id = $parent_id;
		$new_benefit->save();

		$services = Benefit::where('type', 'services')->where('parent_id', null)->get();

		return view('assets.services', compact('services'));
	}



	public function newDomain($name = 'test')
	{
		$response = Http::withHeaders([
			'Content-Type' => 'application/json',
			// 'Authorization' => 'Bearer b72243518f12b3b1625e61e78a1b9be8070d96637132e633bdd86f63f747c889'
		])
			->withToken('b72243518f12b3b1625e61e78a1b9be8070d96637132e633bdd86f63f747c889')
			->post('https://api.digitalocean.com/v2/domains/topfractal.com/records', [
				"type" => "A",
				"name" => $name,
				"data" => "178.62.41.211",
				"priority" => null,
				"port" => null,
				"ttl" => 1800,
				"weight" => null,
				"flags" => null,
				"tag" => null
			]);
	}

	/**
	 * Dashboard after login
	 */
	public function getDashboard()
	{
		// Get site data

		if (Auth::user()->type != 'admin') {
			$siteData = Site::with('user')->where('user_id', Auth::user()->id)->where('site_trashed', 0)->orderBy('id', 'asc')->get()->toArray();
		} else {
			$siteData = Site::with('user')->where('site_trashed', 0)->orderBy('id', 'asc')->get()->toArray();
		}

		//dd($siteData);
		//echo $siteData[0]['user']['email'];

		//array holding all sites and associated data
		$allSites = array();
		// Get page data
		foreach ($siteData as $site) {
			$temp = array();
			$temp['siteData'] = $site;

			// Get the number of pages
			$pages = Page::where('site_id', $site['id'])->orderBy('id', 'asc')->get()->toArray();
			$temp['nrOfPages'] = count($pages);

			// Grab the last frame of site
			$indexPage = Page::where('name', 'index')->where('site_id', $site['id'])->orderBy('id', 'asc')->get()->toArray();
			if (count($indexPage) > 0) {
				//dd($indexPage);
				$frame = Frame::where('page_id', $indexPage[0]['id'])->where('revision', 0)->orderBy('id', 'asc')->first();
				if (!empty($frame)) {
					$temp['lastFrame'] = $frame->toArray();
				} else {
					$temp['lastFrame'] = '';
				}
			} else {
				$temp['lastFrame'] = '';
			}
			$allSites[] = $temp;
		}
		//dd($allSites);
		$users = User::orderBy('id', 'asc')->get();
		//dd($allSites);
		return view('sites.sites', array('sites' => $allSites, 'users' => $users));
	}

	public function getDashboardNew( $status)
	{
		// Get site data
		
		if($status == 1){
			if (Auth::user()->type != 'admin') {
				$siteData = Site::with('user')->where('user_id', Auth::user()->id)->where('site_trashed', 0)->whereNotNull('ftp_published')->orderBy('id', 'asc')->get()->toArray();
			} else {
				$siteData = Site::with('user')->where('site_trashed', 0)->whereNotNull('ftp_published')->orderBy('id', 'asc')->get()->toArray();
			}
			
		}
		elseif ($status == 0){
			if (Auth::user()->type != 'admin') {
				$siteData = Site::with('user')->where('user_id', Auth::user()->id)->where('site_trashed', 0)->where('ftp_published',  NULL)->orderBy('id', 'asc')->get()->toArray();
			} else {
				$siteData = Site::with('user')->where('site_trashed', 0)->where('ftp_published',  NULL)->orderBy('id', 'asc')->get()->toArray();
			}
			
		}
		
		//dd($siteData);
		//echo $siteData[0]['user']['email'];

		//array holding all sites and associated data
		$allSites = array();
		// Get page data
		foreach ($siteData as $site) {
			$temp = array();
			$temp['siteData'] = $site;

			$paymentID = Payment::where('site_id', $site['id'])
				->where('tariff_id', '<>', 0)
				->where('status', 'done')
				->orderBy('end_at', 'desc')
				->first('tariff_id');

			
			if ($paymentID) {
				$tariffCurrent = Tariff::where('id', $paymentID->tariff_id)->first(['number']);
				
			}
			else {
				$tariffCurrent= null;
			} 
			
			$temp['tariffCurrent'] = $tariffCurrent;
			// Get the number of pages
			$pages = Page::where('site_id', $site['id'])->orderBy('id', 'asc')->get()->toArray();
			$temp['nrOfPages'] = count($pages);

			// Grab the last frame of site
			$indexPage = Page::where('name', 'index')->where('site_id', $site['id'])->orderBy('id', 'asc')->get()->toArray();
			if (count($indexPage) > 0) {
				//dd($indexPage);
				$frame = Frame::where('page_id', $indexPage[0]['id'])->where('revision', 0)->orderBy('id', 'asc')->first();
				if (!empty($frame)) {
					$temp['lastFrame'] = $frame->toArray();
				} else {
					$temp['lastFrame'] = '';
				}
			} else {
				$temp['lastFrame'] = '';
			}
			$allSites[] = $temp;
		}
		//dd($allSites);
		$users = User::orderBy('id', 'asc')->get();
		
		//dd($allSites);
		return view('sites.sites-new', array('sites' => $allSites, 'users' => $users));
	}


	/**
	 * Create New Site
	 */

	public function getSiteEdit(Request $request)
	{
		//dd($request);
		
		$template = Page::where('id', $request->page_id)->first();
		$template_id = $template['css'];
		$site_id = $template['site_id'];
		$logo =  DB::table('sites')->where('id', $site_id)->update(['logo'=>$request->get('header_logo')]);
		DB::table('frames')
                ->where('page_id', $request->page_id)
                ->update(['revision' => 1]);
		if ($request->blocks != []) {
			foreach ($request->blocks as $block) {
				$frames[] = Frame::where('page_id', $template_id)->where('revision', 0)->where('original_url', 'like',  '%' . $block)->orderBy('id', 'ASC')->first();
				$subNames = explode('/', $block);
				$nameFrame = strtok(array_pop($subNames), '.');
				$views[] = $nameFrame;
			}
		}

		$frames = array_filter($frames);
		foreach ($frames as $frame) {
			$subNames = explode('/', $frame->original_url);
			$nameFrame1 = strtok(array_pop($subNames), '.');
			$nameFrame2 = array_pop($subNames);
			$nameFrame = $nameFrame2 . '/'.$nameFrame1;

			$content = $this->getFrameContentEdit($nameFrame, $request, $site_id);
			$template_frame = new Frame();
			$template_frame->page_id = $request->page_id;
			$template_frame->site_id = $site_id;
			$template_frame->content = $content;
			$template_frame->height =  700;
			$template_frame->original_url = $frame->original_url;
			$template_frame->loaderfunction = NULL;
			$template_frame->sandbox =  0;
			$template_frame->revision = 0;
			$template_frame->save();
		}


		return redirect()->route('site', [ 'site_id' => $site_id, 'locale' => app()->getLocale()]);
	}

	private function getFrameContentEdit($frameName, Request $request, $site_id)
	{

		
		$color_id = $request->get('selected_color');
		$color_array = [];
		foreach ($request->get('selected_color_'.$color_id) as $key => $color) {
			$color_array[$key] = $color;
		}
		
		$smallName = explode('/', $frameName);

		if ( $smallName[1] == 'carat_1'){

				$logo_array = explode('/', $request->get('header_logo'));
				$img_array = explode('/', $request->get('header_img'));
				$userID = Auth::user()->id;

				$data['user_menu'] = [];
				foreach ($request->get('blocks') as $key=>$values ) {
					
				
					$menu_items = explode('/', $values);
					if ($menu_items[2] == 'carat_2.html') {						
						$data['user_menu'][] = [
							"link"=> '#about',
							'name'=> $request->get('about_title')
						];
					}
					elseif ($menu_items[2] == 'carat_3.html') {
						$data['user_menu'][] = [
							"link"=> '#advantages',
							'name'=> $request->get('benefit_title')
						];						
					}
					elseif ($menu_items[2] == 'carat_8.html') {
						$data['user_menu'][] = [
							"link"=> '#services',
							'name'=> $request->get('services_title')
						];		
						
					}
					elseif ($menu_items[2] == 'carat_5.html') {
						$data['user_menu'][] = [
							"link"=> '#team',
							'name'=> $request->get('team_title')
						];		
						
					}
					elseif ($menu_items[2] == 'carat_6.html') {
						$data['user_menu'][] = [
							"link"=> '#gallery',
							'name'=> $request->get('porfolio_title')
						];		
						
					}
					elseif ($menu_items[2] == 'carat_7.html') {
						$data['user_menu'][] = [
							"link"=> '#reviews',
							'name'=> $request->get('reviews_title')
						];		
						
					}
					elseif ($menu_items[2] == 'carat_9.html') {
						$data['user_menu'][] = [
							"link"=> '#app',
							'name'=> $request->get('adress_title')
						];						
					}					
				}
				
				if($logo_array[0] == 'elements'){
					$header_logo =  $request->get('header_logo');
				}
				elseif ($logo_array[0] == 'https:' || $logo_array[0] == 'http:'){
					$header_logo =  $request->get('header_logo');
				}
				else {
					$userFolder = 'elements/images/header_logo/'. $userID .'/'. $site_id;
					if (file_exists($userFolder)) {
						$file_pdf = new Filesystem;
						$file_pdf->cleanDirectory($userFolder);
					}
					$file = $request->file('header_logo_file');
					$fileName = $file->getClientOriginalName();
					$file->move($userFolder, $fileName);
					$header_logo = $userFolder.'/'.$fileName;
				}

				if($img_array[0] == 'elements'){
					$header_img =  $request->get('header_img');
				}
				elseif ($img_array[0] == 'https:'  || $img_array[0] == 'http:'){
					$header_img =  $request->get('header_img');
				}
				else {
					$userFolder = 'elements/images/header_img/'. $userID .'/'. $site_id;
					if (file_exists($userFolder)) {
						$file_pdf = new Filesystem;
						$file_pdf->cleanDirectory($userFolder);
					}
					$file = $request->file('header_img_file');
					$fileName = $file->getClientOriginalName();
					$file->move($userFolder, $fileName);
					$header_img = $userFolder.'/'.$fileName;
				}				
				
				

				$data['name'] =  $request->get('site_name');
				$data['address'] = $request->get('adress');
				$data['phone'] = $request->get('phone');
				
				$data['text'] = $request->get('header_text');
				$data['title'] = $request->get('header_title');
				$data['motto'] = $request->get('header_motto');
				$data['logo'] = $header_logo;
				$data['header_img'] = $header_img;
				$data['color1'] = $color_array['head_color'];
				$data['color2'] = $color_array['title_color'];
				$data['color3'] = $color_array['text_color'];
				$content = view('elements.'.$frameName, $data)->render();
			
				return $content;
				
			}
			elseif ( $smallName[1] == 'carat_2'){
				$data = [];

				$folderContentAbout = File::files('elements/images/about');
				$imgRand =  array_rand( $folderContentAbout, 1);
				$img_array = explode('/', $request->get('about_img'));
				$userID = Auth::user()->id;
				if($img_array[0] == 'elements'){
					$about_img =  $request->get('about_img');
				}
				elseif ($img_array[0] == 'https:'  || $img_array[0] == 'http:'){
					$about_img =  $request->get('about_img');
				}
				else {
					$userFolder = 'elements/images/about/'. $userID .'/'. $site_id;
					if (file_exists($userFolder)) {
						$file_pdf = new Filesystem;
						$file_pdf->cleanDirectory($userFolder);
					}
					$file = $request->file('about_file');
					$fileName = $file->getClientOriginalName();
					$file->move($userFolder, $fileName);
					$about_img = $userFolder.'/' . $fileName;
				}

				$data['img'] = $about_img;
				$data['color1'] = $color_array['head_color'];
				$data['color2'] = $color_array['title_color'];
				$data['color3'] = $color_array['text_color'];
				$data['text'] =  $request->get('about_text');
				$data['header_text'] = $request->get('header_text');
				$data['about_title'] =  $request->get('about_title');
				$content = view('elements.'.$frameName, $data)->render();
				
				return $content;
				
			}
			elseif ( $smallName[1] == 'carat_3'){
				$data['cards'] = [];

				$cards_id= '';
				if( $offers = $request->get('elements/'.$frameName.'_html' )  ) {
					foreach ($offers as $key => $offer) {
						$get_benefit = Benefit::where('type', 'benefit')->where('id', $offer)->first();
						$child_array=[];
						if( $offers_child = $request->get('benefit_'.$get_benefit['id'] )  ) {

							foreach ($offers_child as $offer_child) {

								$get_benefitoffer_child = Benefit::where('type', 'benefit')->where('id', $offer_child)->first();
								$child_array[] = [
									'title' => $get_benefitoffer_child['desc'],
									'price' => 'від 200 грн.',
								];

								$cards_id .= $offer_child .',';
							}
							$data['cards'][] = [
								'title' => $get_benefitoffer_child['desc'],

								'img' =>'elements/images/benefits/' . $get_benefit['img'],
							];
							$cards_id .= $offer .',';
						}
						else {
							//dd($key);
							unset($offers[$key]);
						}


					}

				}
				if( $request->get('benefit_text')) {
					$data['text'] =  $request->get('benefit_text');					
				}
				$data['benefit_title'] =  $request->get('benefit_title');
				$data['cards_id'] = rtrim($cards_id, ", ");
				$data['color1'] = $color_array['head_color'];
				$data['color2'] = $color_array['title_color'];
				$data['color3'] = $color_array['text_color'];
				$content = view('elements.'.$frameName, $data)->render();
				
				return $content;
				
			}
			elseif ( $smallName[1] == 'carat_8'){
				$data['extOffers'] = [];
				$cards_id= '';
				$userID = Auth::user()->id;
				if( $offers = $request->get('elements/'.$frameName.'_html' )  ) {
					foreach ($offers as $offer) {
						$services_name = $request->get('services_names');
						$get_benefit = Benefit::where('type', 'services')->where('id', $offer)->first();
						$child_array=[];
						if( $offers_child = $request->get('benefit_'.$get_benefit['id'] )  ) {

							foreach ($offers_child as $offer_child) {
								$get_benefitoffer_child = Benefit::where('type', 'services')->where('id', $offer_child)->first();
								if ($request->get('benefit_'.$get_benefitoffer_child['id'] )[0] == 'ціна' ||$request->get('benefit_'.$get_benefitoffer_child['id'] )[0] == '') {
									$serveice_price = '';
								}
								else {
									$serveice_price = $request->get('benefit_'.$get_benefitoffer_child['id'] )[0] ;
								}
								$child_array[] = [
									'title' => $get_benefitoffer_child['desc'],
									'price' => $serveice_price,
								];
								$cards_id .= $offer_child .':'.$serveice_price .',';
							}
						}

						$data['extOffers'][] = [
									'title' => $services_name[$offer],
									'price' => 'від 200 грн.',
									'child' => $child_array,
								];
								$cards_id .= $offer .': від 200 грн,';
					}

				}
				$data['pirce'] = '';
				if ($request->services_pirce) {
					
					$userFolder = $userFolder = 'elements/carat_price/'. $userID.'/'. $site_id;
					if (file_exists($userFolder)) {
						$file_pdf = new Filesystem;
						$file_pdf->cleanDirectory($userFolder);
					}
					$file = $request->file('services_pirce');
					$fileName = $file->getClientOriginalName();
					$file->move($userFolder, $fileName);
					$data['pirce'] = $userFolder .'/'. $fileName;
				}
				
				$data['color1'] = $color_array['head_color'];
				$data['color2'] = $color_array['title_color'];
				$data['color3'] = $color_array['text_color'];
				$data['services_title'] =  $request->get('services_title');
				$data['cards_id'] = rtrim($cards_id, ", ");
				$content = view('elements.'.$frameName, $data)->render();
				
				return $content;
				

			}
			elseif ( $smallName[1] == 'carat_5'){
				$data = [];
				$userID = Auth::user()->id;
				$data['team_title'] =  $request->get('team_title');				
				$data['teams_name'] = $request->get('doctor_name');
				
				$data['teams_name_string'] =  implode(",", $request->get('doctor_name') );
				$data['teams_text'] = $request->get('doctor');
				$data['teams_text_string'] =  implode(",", $request->get('doctor') );
				$data['imgs']=[];
				$cards_id = '';
				$userFolder = $userFolder = 'elements/images/team/'. $userID .'/'. $site_id;

				foreach ($request->doctor_file_text as $key=>$doctor_file_text) {
					
					$array = explode('/', $doctor_file_text);
					
					if ($array[0] == 'https:' || $array[0] == 'http:'){
						$data['imgs'][$key] =  $doctor_file_text;
						$cards_id .= $doctor_file_text .',';
					}
					elseif ($array[0] == null) {
						$data['imgs'][$key] = '';
						$cards_id .= '' .',';
					}
					else {									
						
						$file = $request->file('doctor_file')[$key];
						
						$fileName = $file->getClientOriginalName();
						
						if (file_exists($userFolder.'/'.$fileName)) {
							$fileName = time().'.'.$file->getClientOriginalExtension();
						}
						$file->move($userFolder, $fileName);
						$data['imgs'][$key] = $userFolder.'/'.$fileName;
						$cards_id .= $userFolder .'/'. $fileName .',';
					}
				}
				

				$data['cards_id'] = rtrim($cards_id, ", ");
				$content = view('elements.'.$frameName, $data)->render();
				
				return $content;
				
			}
			elseif ( $smallName[1] == 'carat_6'){
				$data = [];
				$img_array = explode('/', $request->get('header_img'));
				$cards_id = '';
				$userID = Auth::user()->id;
				$data['color1'] = $color_array['head_color'];
				$data['color2'] = $color_array['title_color'];
				$data['color3'] = $color_array['text_color'];
				$data['porfolio_title'] =  $request->get('porfolio_title');
				$data['imgs']=[];

				$userFolder = $userFolder = 'elements/images/portfolio/'. $userID .'/'. $site_id;
				if($img_array[0] == 'elements'){
					$header_img =  $request->get('header_img');
				}
				elseif ($img_array[0] == 'https:'  || $img_array[0] == 'http:'){
					$header_img =  $request->get('header_img');
				}
				else {
					$userFolder_header = 'elements/images/header_img/'. $userID .'/'. $site_id;
					$folderContentLogo = File::files($userFolder_header);		
					$imgRand =  array_rand( $folderContentLogo, 1);
					$header_img= $folderContentLogo[$imgRand];
				}				
				
				if ($request->porfolio_file && file_exists($userFolder)) {
					
					$file_pdf = new Filesystem;
					$file_pdf->cleanDirectory($userFolder);		

					foreach ($request->file('porfolio_file') as $file) {
						
						$fileName = $file->getClientOriginalName();
						$file->move($userFolder, $fileName);
						$data['imgs'][] =	$userFolder .'/'. $fileName;
						$cards_id .= $userFolder .'/'. $fileName .',';
					}
				}
				elseif (file_exists($userFolder)) {
					$folderContent = File::files($userFolder);
					foreach ($folderContent as $file) {
						//dd($file);
						$fileName = $file->getFilename();
						
						$data['imgs'][] =	$userFolder .'/'. $fileName;
						$cards_id .= $userFolder .'/'. $fileName .',';
					}
				}
				elseif ($request->porfolio_file) {
					
					foreach ($request->file('porfolio_file') as $file) {
						
						$fileName = $file->getClientOriginalName();
						$file->move($userFolder, $fileName);
						$data['imgs'][] =	$userFolder .'/'. $fileName;
						$cards_id .= $userFolder .'/'. $fileName .',';
					}
				}
				$data['img'] = $header_img;
				$data['cards_id'] = rtrim($cards_id, ", ");
				$content = view('elements.'.$frameName, $data)->render();
				
				return $content;
				
			}
			elseif ( $smallName[1] == 'carat_7'){
				$data = [];

				if($request->get('reviews_name1')==''){
					$data['name_first'] ='name1';
				}else {
					$data['name_first'] = $request->get('reviews_name1');
				}
				if($request->get('reviews_name2')==''){
					$data['name_seconde'] ='name1';
				}else {
					$data['name_seconde'] = $request->get('reviews_name2');
				}
				
				$data['text_first'] = $request->get('reviews1');
				$data['text_seconde'] = $request->get('reviews2');
				$data['reviews_title'] =  $request->get('reviews_title');
				$data['color1'] = $color_array['head_color'];
				$data['color2'] = $color_array['title_color'];
				$data['color3'] = $color_array['text_color'];

				$content = view('elements.'.$frameName, $data)->render();
				
				return $content;
				
			}
			elseif ( $smallName[1] == 'carat_9'){
				$data = [
					'email' => $request->get('email'),
					'address' => $request->get('adress'),
					'phone' => $request->get('phone'),
				];
				$data['color1'] = $color_array['head_color'];
				$data['color2'] = $color_array['title_color'];
				$data['color3'] = $color_array['text_color'];
				$data['social'] = [];
				foreach ( $request->get('social') as $key => $social) {
					if ( $social != NULL && $key =='Viber'){
						$data['social'][]  = [
							'name' => $key,
							'url' => 'viber://chat?number='.$social,
							'img' => 'elements/images/icons/viber.svg',
							'value' => $social,
						];
					}
					elseif ( $social != NULL && $key =='Telegram'){
						$data['social'][]  = [
							'name' => $key,
							'url' => 'https://t.me/' . $social,
							'img' => 'elements/images/icons/telegram.svg',
							'value' => $social,
						];
					}
					elseif ( $social != NULL && $key =='Whatsapp'){
						$data['social'][]  = [
							'name' => $key,
							'url' => 'https://wa.me/' . $social,
							'img' => 'elements/images/icons/whatsapp.svg',
							'value' => $social,
						];
					}
					elseif ( $social != NULL && $key =='Instagram'){
						$data['social'][]  = [
							'name' => $key,
							'url' => 'https://www.instagram.com/' . $social,
							'img' => 'elements/images/icons/instagram.svg',
							'value' => $social,
						];
					}
					elseif ( $social != NULL && $key =='Facebook'){
						$data['social'][]  = [
							'name' => $key,
							'url' =>  $social,
							'img' => 'elements/images/icons/facebook.svg',
							'value' => $social,
						];
					}
				}
				$data['adress_title'] =  $request->get('adress_title');
				$data['chart1'] =  $request->get('chart1');
				$data['chart2'] =  $request->get('chart2');
				$random_quote = Benefit::where('type', 'adressText')->inRandomOrder()->first();
				$data['text'] = $request->get('adress_text');

				$content = view('elements.'.$frameName, $data)->render();
				return $content;
				
			}
			else {
				$content = view('elements.'.$frameName)->render();
				return $content;
			}
		
	}

	public function getSiteCreate(Request $request)
	{
		// 
		
		$site = new Site();
		$site->user_id = Auth::user()->id;
		$site->site_name = $request->site_name;
		$site->site_phone = $request->phone;
		$site->site_adress = $request->adress;
		$site->logo = $request->get('header_logo');
		$site->site_trashed = 0;
		$site->save();


		$page = new Page();
		$page->site_id = $site->id;
		$page->name = 'index';
		$page->css = $request->page_id;
		$page->save();
		$frames = [];

		if ($request->blocks != []) {
			foreach ($request->blocks as $block) {
				$frames[] = Frame::where('page_id', $request->page_id)->where('revision', 0)->where('original_url', 'like',  '%' . $block)->orderBy('id', 'ASC')->first();
				$subNames = explode('/', $block);
				$nameFrame = strtok(array_pop($subNames), '.');
				$views[] = $nameFrame;
			}
		}
	
		$frames = array_filter($frames);
		foreach ($frames as $frame) {
			$subNames = explode('/', $frame->original_url);
			$nameFrame1 = strtok(array_pop($subNames), '.');
			$nameFrame2 = array_pop($subNames);
			$nameFrame = $nameFrame2 . '/'.$nameFrame1;

			$content = $this->getFrameContentEdit($nameFrame, $request, $site->id);
			$template_frame = new Frame();
			$template_frame->page_id = $page->id;
			$template_frame->site_id = $site->id;
			$template_frame->content = $content;		
			$template_frame->height =  700;
			$template_frame->original_url = $frame->original_url;
			$template_frame->loaderfunction = NULL;
			$template_frame->sandbox =  0;
			$template_frame->revision = 0;
			$template_frame->save();
		}


		return redirect()->route('site', [ 'site_id' => $site->id, 'locale' => app()->getLocale()]);
	}

	private function getDomainName($site)
	{
		$domainName = Utils::transliterate($site->site_name);
		$sites = Site::where('site_name', '=', $site->site_name)->get();
		if($sites->count() > 1) {
			$domainName .= $site->id;
		}

		return $domainName;

	}

	public function SiteCreate($lang,  $template)
	{
		$layouts_id = Layouts::where('img',  '<>', '')
				->where('parent', 83)
				->groupBy('layouts_id')
				->get(['layouts_id', 'img', 'screan', 'logo']);
	
		$layouts = [];
		foreach ($layouts_id as $value) {
			$layout = Layouts::where('layouts_id', $value->layouts_id)->get(['id', 'meta_key', 'meta_value']);
			
			$layouts[] = [
				'layouts_id' => $value->layouts_id,
				'values' => $layout,
				'img' => $value->img,
				'logo' => $value->logo,
				'screan' => $value->screan
			];
		}
		$template = Template::where('id', $template)->first();

		$templateElement = [];
            
		foreach ($template->metaElenents as $meta) {
			$templateElement[$meta->meta_key] = Element::where('id',  $meta->meta_value)->where('show',  1)->whereNotNull('category_id')->first(); 
		}		
	
		return view('pages/createform', ['template' => $template, 'layouts' => $layouts, 'templateElement'=>$templateElement]);
	}

	public function SiteEditForm($lang, Page $page, Request $request)
	{
		$phpArray = json_encode($request->data);
		//dd($request);
		$parentPage = Page::where('id',  $page->id)->first('css');
		
		$layouts_id = Layouts::where('img',  '<>', '')
				->where('parent', $parentPage->css)
				->groupBy('layouts_id')
				->get(['layouts_id', 'img', 'screan', 'logo']);
	
		$layouts = [];
		foreach ($layouts_id as $value) {
			$layout = Layouts::where('layouts_id', $value->layouts_id)->get(['id', 'meta_key', 'meta_value']);
			
			$layouts[] = [
				'layouts_id' => $value->layouts_id,
				'values' => $layout,
				'img' => $value->img,
				'logo' => $value->logo,
				'screan' => $value->screan
			];
		}
		return view('pages/editform', [
			'page' => $page, 
			'parent' => $parentPage->css, 
			'data'=>$phpArray, 
			'color1'=> $request->color1, 
			'color2'=>$request->color2, 
			'color3'=>$request->color3, 
			'name'=>$request->name, 
			'logo'=>$request->logo, 
			'img'=>$request->img, 
			'layouts' => $layouts
		]);
	}

	public function templateParent($page_id)
	{
		$template = Page::where('id', $page_id)->first();
		$template_id = $template->css;
		return $template_id;
	}

	/**
	 * Bring saved site on canvas
	 * @param  Request $request
	 * @param  Integer $site_id
	 */
	

	public function getSite( $site_id)
	{		
		$site = Site::where('id', $site_id)->first();

		// If user is not an admin then check if user own this site
		if (Auth::user()->type != 'admin') {
			if ($site['user_id'] != Auth::user()->id) {
				return redirect()->route('dashboard', app()->getLocale());
			}
		}

		// Get site details
		$siteArray['site'] = Site::where('id', $site_id)->get();
		$pageFrames = [];
		// Get page details
		$pages = Page::where('site_id', $site_id)->get();

		if (count($pages) > 0) {

			foreach ($pages as $page) {
				//print_r($page);die();
				$frames = Frame::where('page_id', $page->id)->where('revision', 0)->orderBy('id', 'ASC')->get();
				$pageDetails['blocks'] = $frames;
				$pageDetails['page_id'] = $page->id;
				$pageDetails['pages_title'] = $page->title;
				$pageDetails['meta_description'] = $page->meta_description;
				$pageDetails['meta_keywords'] = $page->meta_keywords;
				$pageDetails['header_includes'] = $page->header_includes;
				$pageDetails['css'] = $page->css;
				$pageFrames[$page->name] = $pageDetails;
			}

			$siteArray['pages'] = $pageFrames;
		}

		// Get directory details
		$settings = Setting::where('name', 'elements_dir')->first();
		$siteArray['assetFolders'] = File::directories($settings['value']);

		if (count($siteArray) > 0) {
			// Get Site Data
			$data['siteData'] = $siteArray;

			// Get Page Data
			$pageA = Page::where('site_id', $site_id)->get();
			foreach ($pageA as $pageB) {
				$framesA = Frame::where('page_id', $pageB->id)->where('revision', 0)->orderBy('id', 'ASC')->get();
				$pageFrame[$pageB->name] = $pageB;
			}
			$data['pagesData'] = $pageFrame;

			// Collect data for the image library
			//
			// User Images
			$elementsDir = Setting::where('name', 'elements_dir')->first();
			$uploadDir = Setting::where('name', 'images_uploadDir')->first();
			$imageDir = Setting::where('name', 'images_dir')->first();
			$allowedExt = Setting::where('name', 'images_allowedExtensions')->first();
			$temp = explode('|', $allowedExt->value);
			if (is_dir($uploadDir->value . '/' . Auth::user()->id)) {
				$userFolderContent = directory_map($uploadDir->value . '/' . Auth::user()->id, 2);
				if ($userFolderContent) {
					$userImages = array();
					foreach ($userFolderContent as $userKey => $userItem) {
						if (!is_array($userItem)) {
							// Check the file extension
							$ext = pathinfo($userItem, PATHINFO_EXTENSION);
							// Prepared allowed extensions file array
							if (in_array($ext, $temp)) {
								array_push($userImages, $userItem);
							}
						}
					}
				} else {
					$userImages = false;
				}
			} else {
				$userImages = false;
			}
			//dd($userImages);
			//$data['userImages'] = $userImages;
			if (isset($userImages)) {
				$userSrc = url('/') . '/' . $uploadDir->value . '/' . Auth::user()->id;
				$dataURL = str_replace($elementsDir->value . '/', '', $uploadDir->value);
				$data['userImages'] = View('partials.myimages', array('userImages' => $userImages, 'userSrc' => $userSrc, 'dataURL' => $dataURL));
			}
			// Admin Images
			$adminFolderContent = directory_map($imageDir->value, 2);
			if ($adminFolderContent) {
				$adminImages = array();
				foreach ($adminFolderContent as $adminKey => $adminItem) {
					if (!is_array($adminItem)) {
						// Check the file extension
						$ext = pathinfo($adminItem, PATHINFO_EXTENSION);
						// Prepared allowed extensions file array
						if (in_array($ext, $temp)) {
							array_push($adminImages, $adminItem);
						}
					}
				}
			} else {
				$adminImages = false;
			}
			//dd($adminImages);
			//$data['adminImages'] = $adminImages;
			if (isset($adminImages)) {
				$adminSrc = url('/') . '/' . $imageDir->value;
				$dataURL = str_replace($elementsDir->value . '/', '', $imageDir->value);
				$data['adminImages'] = View('partials.adminimages', array('adminImages' => $adminImages, 'adminSrc' => $adminSrc, 'dataURL' => $dataURL));
			}
			// Pre-build templates
			$templatePages = Page::where('template', 1)->get();

			foreach ($templatePages as $templatePage) {
				$templaePageFrames = array();
				$templateFrames = Frame::where('page_id', $templatePage->id)->where('revision', 0)->get();
				foreach ($templateFrames as $templateFrame) {
					$tFrame = array();
					$tFrame['pageName'] = $templatePage->name;
					$tFrame['pageID'] = $templatePage->id;
					$tFrame['id'] = $templateFrame->id;
					$tFrame['height'] = 700;
					$tFrame['original_url'] = $templateFrame->original_url;
					$templatePageFrames[] = $tFrame;
				}
				$templates[$templatePage->id] = $templatePageFrames;
			}

			if (isset($templates)) {

				$data['templates'] = View('partials.templateframes', array('pages' => $templates));
			}
			//dd($data['templates']);
		} else {
			return redirect()->route('dashboard', app()->getLocale());
		}

		// Grab all revisions
		$pages = Page::where('site_id', $site_id)->where('name', 'index')->get();

		// $pages = $pages->toArray();
		// dd($pages);
		if (count($pages) > 0) {
			$pageID = $pages[0]->id;
			$frames = Frame::where('site_id', $site_id)->where('revision', 1)->where('page_id', $pageID)->orderBy('updated_at', 'DESC')->get();
		} else {
			$frames = false;
		}


		$data['revisions'] = $frames;
		//dd($data['revisions']);
		$revisionView = View('partials.revisions', array('data' => $data['revisions'], 'page' => $pages[0]->name));
		$data['revisionView'] = $revisionView->render();
		//dd($data['revisionView']);

		// Generate pagedata view
		if (count($data['pagesData']) > 0) {
			$pageView = View('partials.pagedata', array('data' => $data['pagesData']));
			$data['pagedataView'] = $pageView->render();
		} else {
			$pageView = View('partials.pagedata', array('data' => $data['siteData']['pages']['index']));
			$data['pagedataView'] = $pageView->render();
		}

		$data['builder'] = true;
		$data['page'] = 'site';

		$layouts_id = Layouts::where('img',  '<>', '')->groupBy('layouts_id')->get(['layouts_id', 'img', 'screan']);
		
		$layouts = [];
		foreach ($layouts_id as $value) {
			$layout = Layouts::where('layouts_id', $value->layouts_id)->get(['id', 'meta_key', 'meta_value']);
			
			$layouts[$value->layouts_id] = [
				'values' => $layout,
				'img' => $value->img,
				'screan' => $value->screan
			];
		}
		$pages = Page::where('site_id', $site_id)->get();
		foreach ($pages as $page) {
			$frames = Frame::where('page_id', $page->id)->where('revision', 0)->orderBy('id', 'ASC')->get();	
					
		}	
		$paymentID = Payment::where('site_id', $site_id)
			->where('tariff_id', '<>', 0)
			->where('status', 'done')
			->orderBy('end_at', 'desc')
			->first('tariff_id');

		
		if ($paymentID) {
			$tariffCurrent = Tariff::where('id', $paymentID->tariff_id)->first(['number']);
		}
		else {
			$tariffCurrent = null;
		} 

		$histoty = Payment::where('site_id', $site_id)->where('status', '<>', 'pending')->get();
		$histotyArray = [];
		$serviceArray = [];
		foreach ($histoty as $value) {
			$serviceArray = [];
			$tariffNumber = '';
			$tariff = Tariff::where('id', $value->tariff_id)->first(['number']);
			if ($tariff ) {
				$tariffNumber =$tariff->number;
			}
			foreach(explode(',', $value->service_id) as $service) {
				if ($service != '') {
					$serviceGet= Service::where('id', $service)->first();
					$serviceArray[] = $serviceGet->name;
				}
			}
			$created_at = Carbon::parse($site->created_at)->addYear()->endOfDay()->format('Y-m-d H:i:s');	
			
			$histotyArray[]= [
				'order_id' => $value->order_id,
				'amount' => $value->amount,
				'tariff' => $tariffNumber,
				'services' => $serviceArray,
				'status' => $value->status,
				'start' => $created_at,
				'end_at' => $value->end_at,
			];		
		} 
		
		$services = Service::where('show', 1)->get();
		//$logo = $requset->

		
		return view('sites.create',
			[
				'data' => $data, 
				'logo' => $layouts,
				'frames' => $frames,
				'services'=>$services, 
				'tariff' => $tariffCurrent,
				'histotyArray' => $histotyArray
		])->render();
	}
	/**
	 * Bring the site data
	 * @param  Request $request
	 * @return JSON
	 */
	public function getSiteData($lang, Request $request)
	{
		$siteID = $request->session()->get('siteID');
		//echo var_dump($request->session());
		$site = Site::where('id', $siteID)->get();
		//echo var_dump($site);
		$siteArray['site'] = $site[0];

		$pages = Page::where('site_id', $siteID)->get();
		foreach ($pages as $page) {
			$frames = Frame::where('page_id', $page->id)->where('revision', 0)->orderBy('id', 'ASC')->get();
			$pageDetails['blocks'] = $frames;
			$pageDetails['page_id'] = $page->id;
			$pageDetails['pages_title'] = $page->title;
			$pageDetails['meta_description'] = $page->meta_description;
			$pageDetails['meta_keywords'] = $page->meta_keywords;
			$pageDetails['header_includes'] = $page->header_includes;
			$pageDetails['page_css'] = $page->css;
			$pageFrames[$page->name] = $pageDetails;
			
		}
		$siteArray['pages'] = $pageFrames;

		echo json_encode($siteArray);
	}

	/**
	 * Save page and frame
	 * @param  Request $request
	 * @param  Integer $forPublish
	 * @return JSON
	 */

	public function postTSave(Request $request, $forPublish = 0)
	{
		//print_r($request['siteData']); die();

		if (!isset($request['siteData'])) {
			$temp = array();
			$temp['header'] = 'Ouch! Something went wrong:';
			$temp['content'] = 'The siteData is missing, please try again.';
			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();
			die(json_encode($return));
		}
		$siteData = $request['siteData'];

		//dd($siteData);
		$site = Site::where('id', $siteData['id'])->first();
		//die(var_dump($site));
		$site->site_name = $siteData['site_name'];
		$site->update();
		//dd($request['pages']);

		foreach ($request['pages'] as $page => $pageData) {

			Page::where('site_id', 15)->where('name', $page)->update(['template' => 1]);
		}
		die(json_encode($request));
	}

	public function postSave(Request $request, $forPublish = 0)
	{
		//print_r($request['siteData']); die();


		if (!isset($request['siteData'])) {
			$temp = array();
			$temp['header'] = 'Ouch! Something went wrong:';
			$temp['content'] = 'The siteData is missing, please try again.';
			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();
			die(json_encode($return));
		}
		$siteData = $request['siteData'];

		//dd($siteData);
		$site = Site::where('id', $siteData['id'])->first();
		//die(var_dump($site));
		$site->site_name = $siteData['site_name'];
		$site->update();
		//dd($request['pages']);

		foreach ($request['pages'] as $page => $pageData) {
			//dd($pageData);
			//dd($page);
			//dealing with a changed page
			if ($pageData['status'] == 'changed') {
				// Get page data
				$pageNew = Page::where('site_id', $siteData['id'])->where('name', $page)->first();
				$pageNew->site_id = $siteData['id'];
				$pageNew->name = $page;
				$pageNew->title = $pageData['pageSettings']['title'];
				$pageNew->meta_keywords = $pageData['pageSettings']['meta_keywords'];
				$pageNew->meta_description = $pageData['pageSettings']['meta_description'];
				$pageNew->header_includes = $pageData['pageSettings']['header_includes'];
				$pageNew->css = $pageData['pageSettings']['page_css'];
				$pageNew->update();
				$pageID = $pageNew->id;
			} elseif ($pageData['status'] == 'new') {
				$pageNew = new Page();
				$pageNew->site_id = $siteData['id'];
				$pageNew->name = $page;
				$pageNew->title = $pageData['pageSettings']['title'];
				$pageNew->meta_keywords = $pageData['pageSettings']['meta_keywords'];
				$pageNew->meta_description = $pageData['pageSettings']['meta_description'];
				$pageNew->header_includes = $pageData['pageSettings']['header_includes'];
				$pageNew->css = $pageData['pageSettings']['page_css'];
				$pageNew->save();
				$pageID = $pageNew->id;
			}


			// Page done, onto the blocks
			// Push existing frames into revision
			$frames = Frame::where('page_id', $pageID)->get();
			if ($frames) {
				foreach ($frames as $frame) {
					$frame->revision = 1;
					$frame->update();
				}
			}

			//dd($frames);

			if (isset($pageData['blocks'])) {
				//dd($pageData['blocks']);

				foreach ($pageData['blocks'] as $block) {

					//print_r($block); die();
					$frames = new Frame();
					$frames->page_id = $pageID;
					$frames->site_id = $siteData['id'];
					$frames->content = $block['frameContent'];
					$frames->height = 700;
					$frames->original_url = $block['original_url'];
					$frames->loaderfunction = $block['loaderFunction'];
					$frames->sandbox = ($block['sandbox'] == 'true') ? 1 : 0;
					$frames->revision = 0;
					$frames->save();
				}
			}
		}


		// Delete any pages?
		//dd($request['toDelete']);
		if (isset($request['toDelete']) && is_array($request['toDelete']) && count($request['toDelete']) > 0) {
			foreach ($request['toDelete'] as $page) {
				$page = Page::where('site_id', $siteData['id'])->where('name', $page)->first();
				if ($page) {
					$page->delete();
				}
			}
		}

		$return = array();

		// Regular site save
		if ($forPublish == 0) {
			$temp = array();
			$temp['header'] = "Success!";
			$temp['content'] = "The site has been saved successfully!";
		}
		// Saving before publishing, requires different message
		elseif ($forPublish == 1) {
			$temp = array();
			$temp['header'] = "Success!";
			$temp['content'] = "The site has been saved successfully! You can now proceed with publishing your site.";
		}

		$return['responseCode'] = 1;
		$view = View('partials.success', array('data' => $temp));
		$return['responseHTML'] = $view->render();

		die(json_encode($return));
	}

	/**
	 * Get frame content by Frame ID
	 * @param  Integer 	$frame_id
	 * @return String
	 */
	public function getFrame($frame_id)
	{
		$frame = Frame::where('id', $frame_id)->first();
		//dd($frame);
		echo $frame->content;
	}

	

	/**
	 * Live preview site
	 * @param  Request $request
	 * @return HTML
	 */
	public function postLivePreview(Request $request) 
	{

		//die(print_r( $request->all() ));
		
		//dd($request);
		if ($request->has('siteID')) {
			$siteData = Site::where('id', $request->input('siteID'))->first();
		}
		$head = "";
		
		
		echo str_replace(' <!--headerIncludes-->', $head, "<!DOCTYPE html>\n" . $request['page']);
	}

	public function getLivePreview($site_id, $page_css)
	{
		if ($site_id) {
			$siteData = Site::where('id', $site_id )->first();
			$pageTemplate = Page::where('id', $page_css)->where('site_id', $site_id)->first();
			if(	$pageTemplate ){
				$pageData = Frame::where('site_id', $site_id)->where('page_id', $page_css)->where('revision', 0)->get();
			}
			else {
				$pageData = Frame::where('site_id', $site_id)->where('revision', 0)->get();
			}
		}
		$content = '';
		foreach ($pageData as $val) {
			$content .= $val->content;
		}
		//dd($pageData);
		$head = "";
		// Title

		// Page css
		if ($page_css) {
			$head .= "\n<style>" . $page_css . "</style>\n";
		}
		// Global css
		if ($siteData->global_css != '') {
			$head .= "\n<style>" . $siteData->global_css . "</style>\n";
		}
		// Custom header to deal with XSS protection
		header("X-XSS-Protection: 0");
		echo str_replace(' <!--headerIncludes-->', $head, "<!DOCTYPE html>\n" . $content);
	}

	/**
	 * Get revision info
	 * @param  Integer $site_id
	 * @param  String  $page
	 */
	public function getRevisions($site_id, $page)
	{

		if ($site_id != '' && $page != '') {
			// Grab all revisions
			$pages = Page::where('site_id', $site_id)->where('name', $page)->get();
			if (count($pages) > 0) {
				$page_id = $pages[0]->id;
				$frames = Frame::where('site_id', $site_id)->where('revision', 1)->where('page_id', $page_id)->orderBy('updated_at', 'DESC')->get();
			} else {
				$frames = false;
			}

			$data['revisions'] = $frames;
			//dd($data['revisions']);
			$revisionView = View('partials.revisions', array('data' => $data['revisions'], 'page' => $pages[0]->name));
			$revision = $revisionView->render();
			echo $revision;
		}
	}

	/**
	 * Retrive a preview for a revision
	 * @param  Integer $site_id
	 * @param  Integer $datetime
	 * @param  String  $page
	 */
	public function getRevisionPreview($site_id, $datetime, $page)
	{
		if ($site_id == '' || $datetime == '' || $page == '') {
			die('Missing data, revision could not be loaded');
		}

		$page = Page::where('site_id', $site_id)->where('name', $page)->first();
		$page_id = $page->id;
		$frames = Frame::where('site_id', $site_id)->where('updated_at', date('Y-m-d H:i:s', $datetime))->where('revision', 1)->where('page_id', $page_id)->get();
		$skeleton = HtmlDomParser::file_get_html('./elements/skeleton.html');

		// Get the page container
		$ret = $skeleton->find('div[id=page]', 0);

		$page = '';

		foreach ($frames as $frame) {
			//dd($frame);
			$frameHTML = HtmlDomParser::str_get_html($frame->content);
			//dd($frameHTML);
			$frameContent = $frameHTML->find('div[id=page]', 0);
			$page .= $frameContent->innertext;
		}

		$ret->innertext = $page;

		// Print it!
		echo $skeleton;

		//$revisionOutput = $this->revisionmodel->buildRevision($siteID, $revisionStamp, $page);

		//echo $revisionOutput;
	}

	/**
	 * Delete Revision
	 * @param  Integer $site_id
	 * @param  Integer $datetime
	 * @param  String  $page
	 * @return JSON
	 */
	public function getRevisionDelete($site_id, $datetime, $page)
	{
		//dd($page);
		//DB::enableQueryLog();
		$return = array();
		if ($site_id == '' || $datetime == '' || $page == '') {
			$return['code'] = 0;
			$return['message'] = 'Some data is missing, we can not delete this revision right now. Please try again later.';
			die(json_encode($return));
		}

		$q_page = Page::where('site_id', $site_id)->where('name', $page)->first();
		$frame = Frame::where('site_id', $site_id)->where('page_id', $q_page->id)->where('updated_at', date('Y-m-d H:i:s', $datetime))->where('revision', 1)->first();
		//dd(DB::getQueryLog());
		//dd($frame);
		$frame->delete();


		$return['code'] = 1;
		$return['message'] = 'The revision was removed successfully.';

		echo json_encode($return);
	}

	/**
	 * Restore revision site
	 * @param  Integer $site_id
	 * @param  Integer $datetime
	 * @param  String  $page
	 */
	public function getRevisionRestore($site_id, $datetime, $page)
	{
		if ($site_id == '' || $datetime == '' || $page == '') {
			die('Missing data, revision could not be restore.');
		}

		$page = Page::where('site_id', $site_id)->where('name', $page)->first();
		//dd($page);

		// Update current frame as revision
		$page_id = $page->id;

		$frame = Frame::where('site_id', $site_id)->where('page_id', $page_id)->where('revision', 0)->update(['revision' => 1]);

		// Restore revision by recreating the old revision
		$frames = Frame::where('site_id', $site_id)->where('page_id', $page_id)->where('updated_at', date('Y-m-d H:i:s', $datetime))->get();
		//dd($frames);
		foreach ($frames as $frame) {
			$new_frame = new Frame();
			$new_frame->page_id = $page_id;
			$new_frame->site_id = $site_id;
			$new_frame->content = $frame->content;
			$new_frame->height = 700;
			$new_frame->original_url = $frame->original_url;
			$new_frame->loaderfunction = $frame->loaderfunction;
			$new_frame->sandbox = $frame->sandbox;
			$new_frame->revision = 0;
			$new_frame->save();
		}

		return redirect()->route('site', [$site_id]);
	}

	/**
	 * Site Settings data update with ajax
	 * @param  Request $request
	 * @return JSON
	 */

	public function LiqPayForm (Request $request)
	{
		$userID = Auth::user()->id;
		$public_key = 'i4340652402';
		$private_key = 'YH5tb7kqdw0nPSYlYDKfFlFzSGDfI5tGJADdGFK8';
		$order = rand(1, 9999999);
		$liqpay = new LiqPay($public_key, $private_key);
		$res = $liqpay->cnb_form(array(
		   'action'         => 'pay',
		   'amount'         => 1,
		   //'amount'         => $request->get('totalAmount'),
		   'currency'       => 'UAH',
		   'description'    => 'Оплата тарифа',
		   'order_id'       => $order,
		   'version'        => '3',		 
		   'product_category'   => $request->get('tariffId'),
		   'product_name'   => $request->get('siteId'),
		   'result_url'		=> 'https://topfractal.com/' . $request->get('locale') . '/siteAjaxUpdate/'. $order,
		   'server_url'		=> 'https://topfractal.com/' . $request->get('locale') . '/siteAjaxUpdateChech'
		));
	

		DB::table('payments')->where('site_id', $request->get('siteId'))->where('status','pending')->delete();
		$created_at = date("Y-m-d H:i:s");
		$payment = new Payment;
		$payment->site_id = $request->get('siteId');
		$payment->order_id = $order;
		$payment->user_id = $userID ;
		$payment->tariff_id =  $request->get('tariffId');
		$payment->service_id =  $request->get('serviceChecked');
		$payment->amount = $request->get('totalAmount');	
		$payment->status = 'pending';	
		$payment->created_at = $created_at;	
		$payment->save();

		$return = [
			'form' => $res,
			'amount' => $request->get('totalAmount')				
		];
		return $return;
	}

	

	public function postAjaxUpdateChech (Request $request) {
		if( $request) {
			$data = $request->data;			
			$private_key = 'YH5tb7kqdw0nPSYlYDKfFlFzSGDfI5tGJADdGFK8';		
			$sign = base64_encode( sha1( 
				$private_key .  
				$data . 
				$private_key 
				, 1 
			));
		}
	}

	public function postAjaxUpdate( $order)
	{
		$messageOK = 'Вітаємо, оплата успішна!';
		$messageWrongCard = 'Помилкові дані карти. Спробуйте ще раз!';

		$public_key = 'i4340652402';
		$private_key = 'YH5tb7kqdw0nPSYlYDKfFlFzSGDfI5tGJADdGFK8';		
		
		$liqpay = new LiqPay($public_key, $private_key);
		$res = $liqpay->api("request", array(
			'action'        => 'status',
			'version'       => '3',
			'order_id'      => $order
		));
			
	
		$getOrder = Payment::where('order_id', $order)->first();
		
		if($res->status == 'success' && $getOrder) {
			$site_id = $res->product_name;
			$tariff_id = $res->product_category;
			$amount = $res->amount;			
			$userID = Auth::user()->id;
			$created_at = date("Y-m-d H:i:s");
			$user = User::find($userID);
			$site = Site::find($site_id);

			$history = $user->payment()->where('site_id', $site_id)->orderBy('end_at', 'desc')->first();			
			if($history && ( $created_at <= $history['end_at']) ){
				$created_at =  date("Y-m-d H:i:s", strtotime("+1 day", strtotime($history['end_at'])));
			}
			
			$end_date = '';

			$tariff_number = Tariff::where('id', $tariff_id )->first('number');

			$tariff_value = Tariff::where('number', $tariff_number->number )->where('meta', 'price')->orderBy('id')->get('value');

		
			if(count($tariff_value)>0) {
				if ($amount == $tariff_value[0]['value']) {
					$end_date = date("Y-m-d H:i:s", strtotime("+1 months", strtotime($created_at)));
				}
				elseif ($amount == $tariff_value[1]['value']) {
					$end_date = date("Y-m-d H:i:s", strtotime("+6 months", strtotime($created_at)));
				}
				elseif ($amount == $tariff_value[2]['value']) {
					$end_date = date("Y-m-d H:i:s", strtotime("+12 months", strtotime($created_at)));
				}
				elseif ($amount == 1) {
					$end_date = date("Y-m-d H:i:s", strtotime("+12 months", strtotime($created_at)));
				}

				$site->publish_date = Carbon::parse($site->publish_date)->addYear()->endOfDay()->format('Y-m-d H:i:s');
				if(Carbon::parse($site->publish_date) >= Carbon::now()) {
					$site->ftp_published = 1;
					$site->publish_date_end = $end_date;
				}
				$site->save();
				if($site->ftp_published == 1) {
					$site_name = $this->getDomainName($site);
					$domain = $site_name . '.topfractal.com';
					FacadesFile::put('/etc/apache2/sites-enabled/' . $domain . '.conf', $this->getDomainConfText($domain, $site_name));
					FacadesFile::put('/etc/apache2/sites-available/' . $domain . '.conf', $this->getDomainConfText($domain, $site_name));
				} 
			}			
			
			
			$getOrder->status = 'done';
			$getOrder->created_at = $created_at;
			$getOrder->end_at = $end_date;
			$getOrder->update();			

			$json = ['status' => 'ok', 'message' => $messageOK];
			return view('pages.confirmation', compact('json'));
		}		
		elseif($res->status == 'failure' && $getOrder) {
			$getOrder->status = 'failure';
			$getOrder->update();
			$json = ['status' => 'error', 'message' => $messageWrongCard];
			return view('pages.confirmation', compact('json'));
		}
		else {
			$json = ['status' => 'error', 'message' => $messageWrongCard];
			return view('pages.confirmation', compact('json'));
		}

	}

	public function restartApach()
	{
		exec('/usr/bin/sudo /etc/init.d/apache2 restart');
		exec('/usr/bin/sudo /etc/init.d/apache2 reload');
	}

	/**
	 * Update page data with ajax call
	 * @param  Request $request
	 * @return JSON
	 */
	public function postUpdatePageData(Request $request)
	{
		// Validation

		// Update page data
		$page = Page::firstOrNew(array('id' => $request->input('pageID')));
		$page->site_id = $request->input('siteID');
		$page->title = $request->input('pageData_title');
		$page->meta_keywords = $request->input('pageData_metaKeywords');
		$page->meta_description = $request->input('pageData_metaDescription');
		$page->header_includes = $request->input('pageData_headerIncludes');
		$page->css = $request->input('pageData_headerCss');
		$page->save();

		// Return page data as well
		// Get page details
		$pages = Page::where('site_id', $request->input('siteID'))->get();
		foreach ($pages as $page) {
			$frames = Frame::where('page_id', $page->id)->where('revision', 0)->orderBy('id', 'ASC')->get();
			$pageDetails['blocks'] = $frames;
			$pageDetails['page_id'] = $page->id;
			$pageDetails['pages_title'] = $page->title;
			$pageDetails['meta_description'] = $page->meta_description;
			$pageDetails['meta_keywords'] = $page->meta_keywords;
			$pageDetails['header_includes'] = $page->header_includes;
			$pageDetails['css'] = $page->css;
			$pageFrames[$page->name] = $pageDetails;
		}
		$siteArray['pages'] = $pageFrames;

		if (count($siteArray) > 0) {
			$return['siteData'] = $siteArray;

			$pageA = Page::where('site_id', $site_id)->get();
			foreach ($pageA as $pageB) {
				$framesA = Frame::where('page_id', $pageB->id)->where('revision', 0)->orderBy('id', 'ASC')->get();
				$pageFrame[$pageB->name] = $pageB;
			}
			$return['pagesData'] = $pageFrame;
		}

		$temp['header'] = 'All set!';
		$temp['content'] = 'The page settings were successfully updated.';

		$return['responseCode'] = 1;
		$view = View('partials.success', array('data' => $temp));
		$return['responseHTML'] = $view->render();

		die(json_encode($return));
	}

	/**
	 * Export Site
	 * @param  Request $request
	 */
	public function postExport(Request $request)
	{

		$theSite = Site::where('id', $request->siteID)->first();
		if(!$theSite->ftp_published)
		{
			$domainName = $this->getDomainName($theSite);
			$this->newDomain($domainName);
			$theSite->publish_date = Carbon::now()->addDays(0)->endOfDay();
			$theSite->publish_date_end = Carbon::now()->addDays(7)->endOfDay();
			$theSite->ftp_published = 1;
			$theSite->save();
		} else {
			if(Carbon::parse($theSite->publish_date_end)->endOfDay() < Carbon::now()->endOfDay()) {
				$theSite->ftp_published = 2;
				$theSite->save();
			}
		}
		$frames = Frame::where('site_id', $theSite->id)->where('revision', 0)->get();

		$content = '';
		foreach ($frames as $val) {
			$content .= $val->content;
		}
		$pages = ['index' => $content];

		$this->export($request, $pages);
		$return = ['status'=>true ];
		return $return;
	}



	private function export($request, $pagesContent)
	{
		//dd($request->all());
		$user_id = Auth::user()->id;
		//dd($user_id);
		$file_name = Setting::where('name', 'export_fileName')->first();
		$zip = new ZipArchive();
		$zip->open(public_path() . '/tmp/' . $file_name->value, ZipArchive::CREATE);
		$asset_path = Setting::where('name', 'export_pathToAssets')->first();
		$temp = explode('|', $asset_path->value);
		foreach ($temp as $thePath) {
			// Create recursive directory iterator
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator($thePath),
				RecursiveIteratorIterator::LEAVES_ONLY
			);
			foreach ($files as $name => $file) {
				if ($file->getFilename() != '.' && $file->getFilename() != '..') {
					// Get real path for current file
					$filePath = $file->getRealPath();
					$temp = explode("/", $name);
					array_shift($temp);
					$newName = implode("/", $temp);
					if ($thePath == 'elements/images') {
						// Check if this is a user file
						// if (strpos($file, '/uploads') !== false) {
						// 	if (strpos($file, '/uploads/' . Auth::user()->id . '/') !== false || Auth::user()->type == 'admin') {
						// 		// Add current file to archive
						// 		$zip->addFile($filePath, $newName);
						// 		//echo $filePath."<br>";
						// 	}
						// } else {
						// 	// Add current file to archive
						// 	$zip->addFile($filePath, $newName);
						// 	//echo $filePath."<br>";
						// }
					} else {
						// Add current file to archive
						$zip->addFile($filePath, $newName);
						//echo $filePath."<br>";
					}
				}
			}
		}

		$theSite = Site::where('id', $request->input('siteID'))->first();
		//dd($theSite);
		foreach ($pagesContent as $page => $content) {
			
			$pageContent = $request->input('content');
			
			$zip->addFromString($page . ".html",  $pageContent);
		}
		$zip->close();

		//$yourfile = $this->config->item('export_fileName');
		// $file_name_dl = basename($file_name->value);
		// //dd($file_name_dl);
		// header("Content-Type: application/zip");
		// header("Content-Transfer-Encoding: Binary");
		// header("Content-Disposition: attachment; filename=$file_name_dl");
		// header("Content-Length: " . filesize(public_path() . "/tmp/" . $file_name->value));

		// readfile(public_path() . "/tmp/" . $file_name->value);

		// dump($theSite);
		$site_name = $this->getDomainName($theSite);
		$theSite->remote_url = $site_name . '.topfractal.com';
		$theSite->save();
		$zip = new ZipArchive();
		$zip->open(public_path() . '/tmp/' . $file_name->value);
		$zip->extractTo('/var/www/' . $site_name );
		$zip->close();

		// unlink(public_path() . '/tmp/' . $file_name->value);

		$domain = $site_name . '.topfractal.com';


		if($theSite->ftp_published != 2) {
			FacadesFile::put('/etc/apache2/sites-available/' . $domain . '.conf', $this->getDomainConfText($domain, $site_name));
			FacadesFile::put('/etc/apache2/sites-enabled/' . $domain . '.conf', $this->getDomainConfText($domain, $site_name));
		}
		//dump($domain);
		$process = exec('a2ensite ' . $domain);
		dump($process);

		$process = exec('a2dissite 000-default.conf');
		dump($process);

		//$process = exec('sudo service apache2 restart');
		//dump($process);

		//$process = exec('systemctl restart apache2');
		//dump($process);

		exec('/usr/bin/sudo /etc/init.d/apache2 restart');
		exec('/usr/bin/sudo /etc/init.d/apache2 reload');
		//dump($process);
		//dd('nice');
	}


	public function getDomainConfText($domain, $folder)
	{
		return "
			<VirtualHost *:80>
				ServerAdmin admin@178.62.41.211
				ServerName {$domain}
				ServerAlias {$domain}
				DocumentRoot /var/www/{$folder}

				<Directory /var/www/{$folder}/>
					Options Indexes FollowSymLinks MultiViews
					AllowOverride All
					Order allow,deny
					allow from all
					Require all granted
				</Directory>

				ErrorLog \${APACHE_LOG_DIR}/error.log
        CustomLog \${APACHE_LOG_DIR}/access.log combined

			</VirtualHost>
		";
	}

	/**
	 * Publish site with ajax call
	 * @param  Request $request
	 * @param  String  $type
	 * @return JSON
	 */
	public function postOldPublish(Request $request, $type = null)
	{
		$site_id = $request->input('site_id');
		// Get site details
		$siteArray['site'] = Site::where('id', $site_id)->first();
		//dd($siteArray['site']);

		$this->newDomain($siteArray['site']['site_name']);

		// Get page details
		$pages = Page::where('site_id', $site_id)->get();
		foreach ($pages as $page) {
			$frames = Frame::where('page_id', $page->id)->where('revision', 0)->orderBy('id', 'ASC')->get();
			$pageDetails['blocks'] = $frames;
			$pageDetails['page_id'] = $page->id;
			$pageDetails['pages_title'] = $page->title;
			$pageDetails['meta_description'] = $page->meta_description;
			$pageDetails['meta_keywords'] = $page->meta_keywords;
			$pageDetails['header_includes'] = $page->header_includes;
			$pageDetails['css'] = $page->css;
			$pageFrames[$page->name] = $pageDetails;
		}
		$siteArray['pages'] = $pageFrames;

		// Get directory details
		$settings = Setting::where('name', 'elements_dir')->first();
		$siteArray['assetFolders'] = File::directories($settings['value']);

		// Site ok?
		if ($siteArray == false) {
			$temp = array();
			$temp['header'] = 'Ouch! Something went wrong:';
			$temp['content'] = 'It appears the site ID is missing OR incorrect. Please refresh your page and try again.';
			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();
			die(json_encode($return));
		}

		// Do we have anythin to publish at all?
		if (!isset($request->item) || $request->item == '') // Nothing to upload
		{
			$temp = array();
			$temp['header'] = 'Ouch! Something went wrong:';
			$temp['content'] = 'It appears there are no assets selected for publication. Please select the assets you\'d like to publish and try again.';
			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();
			die(json_encode($return));
		}

		// Site info for FTP
		$site = Site::where('id', $site_id)->first();
		$ftp = new CI_FTP;
		$config = array(
			'hostname' => $site->ftp_server,
			'username' => $site->ftp_user,
			'password' => $site->ftp_password,
			'port' => $site->ftp_port,
		);
		if (!$ftp->connect($config)) {
			$temp = array();
			$temp['header'] = 'Ouch! Something went wrong:';
			$temp['content'] = 'It appears there are no assets selected for publication. Please select the assets you\'d like to publish and try again.';
			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();
			die(json_encode($return));
		}
		if (trim($site->ftp_path) == '/') {
			$ftp_path = '';
		} else {
			$ftp_path = trim($site->ftp_path);
		}

		// Uploading files
		if ($type == 'asset') // Asset publishing
		{
			//echo $request->input('item');
			//dd($ftp_path);
			set_time_limit(0); // Prevent timeout
			if ($request->input('item') == 'elements/images') {
				// Create the /imaged folder?
				if (!$ftp->list_files($ftp_path . "/images/")) {
					$ftp->mkdir($ftp_path . "/images/");
				}
				$dirMap = directory_map(public_path() . '/elements/images/', 2);
				//dd($dirMap);

				foreach ($dirMap as $key => $entry) {
					if (is_array($entry)) {
						// Folder, do all but take special care of /uploads
						if ($key != 'uploads/') {
							$ftp->mirror(public_path() . '/elements/images/' . $key, $ftp_path . "/images/" . $key);
						} else // Take special care of the uploads folder
						{
							$user_id = Auth::user()->id;
							$uploadsMap = directory_map(public_path() . '/elements/images/uploads/', 1);

							foreach ($uploadsMap as $userIDFolder) {
								if ($userIDFolder == $user_id . "/") {
									// Create the /imaged folder?
									if (!$ftp->list_files($ftp_path . "/images/uploads/")) {
										$ftp->mkdir($ftp_path . "/images/uploads/");
									}

									$ftp->mirror(public_path() . '/elements/images/uploads/' . $userIDFolder, $ftp_path . "/images/uploads/" . $userIDFolder);
								}
							}
						}
					} else {
						// File
						$sourceFile = public_path() . '/elements/images/' . $entry;
						$destinationFile = $ftp_path . "/images/" . $entry;
						//echo $sourceFile."\n";
						//echo $_SERVER['DOCUMENT_ROOT'].$sourceFile."\n";
						$ftp->upload($sourceFile, $destinationFile);
					}
				}
			} else {
				$item = explode('/', $request->input('item'));
				$locpath = public_path() . '/' . $request->input('item') . '/';
				$rempath = $ftp_path . '/' . $item[1] . '/';
				$st = $ftp->mirror($locpath, $rempath);
				// echo $locpath . '<br>';
				// echo $rempath;
				// dd($st);
			}
		} elseif ($type == 'page') // Page publishing
		{
			// Create temp files
			// Check to make sure the /tmp folder is writable
			if (!is_writable(public_path() . '/tmp/')) {
				$temp = array();
				$temp['header'] = 'Ouch! Something went wrong:';
				$temp['content'] = 'It appears there are no assets selected for publication. Please select the assets you\'d like to publish and try again.';
				$return = array();
				$return['responseCode'] = 0;
				$view = View('partials.error', array('data' => $temp));
				$return['responseHTML'] = $view->render();
				die(json_encode($return));
			}

			// Get page meta
			$meta = '';
			$pageMeta = Page::where('site_id', $site_id)->where('name', $request->input('item'))->first();
			if ($pageMeta) {
				// Insert title, meta keywords and meta description
				$meta .= '<title>' . $pageMeta->title . '</title>' . "\r\n";
				$meta .= '<meta name="keywords" content="' . $pageMeta->meta_keywords . '">' . "\r\n";
				$meta .= '<meta name="description" content="' . $pageMeta->meta_description . '">';

				$pageContent = str_replace('<!--pageMeta-->', $meta, $request->input('pageContent'));

				// Insert header includes;
				$includesPlusCss = '';
				if ($pageMeta->header_includes != '') {
					$includesPlusCss .= $pageMeta->header_includes;
				}
				if ($pageMeta->css != '') {
					$includesPlusCss .= "\n<style>" . $pageMeta->css . "</style>\n";
				}
				if ($site->global_css != '') {
					$includesPlusCss .= "\n<style>" . $site->global_css . "</style>\n";
				}
				// Insert header includes;
				$pageContent = str_replace("<!--headerIncludes-->", $includesPlusCss, $pageContent);
			} else {
				$pageContent = $request->input('pageContent');
			}

			if (!write_file(public_path() . '/tmp/' . $request->input('item') . ".html", "<!-- DOCTYPE html -->" . $pageContent)) {
				//echo 'Unable to write the file';
			} else {
				//echo 'File written!';
			}

			// Upload temp files
			set_time_limit(0); //prevent timeout
			$st = $ftp->mirror(public_path() . '/tmp/', $ftp_path . "/");
			//dd($st);
			// Remove all temp fiels
			File::deleteDirectory(public_path() . '/tmp/', true);
		}

		// All went well
		//$this->sitemodel->published( $_POST['siteID'] );
		$site = Site::where('id', $site_id)->first();
		$site->ftp_published = 1;
		$site->update();

		$return = array();
		$return['responseCode'] = 1;
	}

	/**
	 * Trash site with ajax call
	 * @param  Integer $site_id
	 * @return JSON
	 */
	public function getTrash($site_id)
	{
		if ($site_id == '' || $site_id == 'undefined') {
			$temp = array();
			$temp['header'] = 'Ouch! Something went wrong:';
			$temp['content'] = 'The site ID is missing or corrupt. Please try reloading the page and then try deleting the site once more.';

			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();

			die(json_encode($return));
		}

		// All good, move to trash
		$site = Site::where('id', $site_id)->first();
		$site->site_trashed = 1;
		$site->update();

		$temp = array();
		$temp['header'] = 'All set!';
		$temp['content'] = 'The site was successfully deleted from the system.';

		$return = array();
		$return['responseCode'] = 1;
		$view = View('partials.success', array('data' => $temp));
		$return['responseHTML'] = $view->render();
		return redirect()->route('dashboard', ['locale' => app()->getLocale(), 'status'=>1]);
		//die(json_encode($return));
	}

	/**
	 * List FTP folder content
	 * @param  Request $request
	 * @return JSON
	 */
	public function postFTPConnect(Request $request)
	{
		$ftp = new CI_FTP;
		$config = array(
			'hostname' => trim($request->input('siteSettings_ftpServer')),
			'username' => trim($request->input('siteSettings_ftpUser')),
			'password' => trim($request->input('siteSettings_ftpPassword')),
			'port' => trim($request->input('siteSettings_ftpPort')),
			'debug' => FALSE,
		);
		if ($ftp->connect($config)) {
			$path = ($request->input('siteSettings_ftpPath')) ? trim($request->input('siteSettings_ftpPath')) : '/';
			$list = $ftp->list_files($path);
			if ($list) {
				$temp = array();
				$temp['list'] = $list;
				$temp['data'] = $_POST;

				$return = array();
				$return['responseCode'] = 1;
				$view = View('partials.ftplist', array('data' => $temp));
				$return['responseHTML'] = $view->render();
			} else {
				$temp = array();
				$temp['header'] = 'Error:';
				$temp['content'] = 'The path you have provided is not correct or you might not have the required permissions to access this path.';

				$return = array();
				$return['responseCode'] = 0;
				$view = View('partials.error', array('data' => $temp));
				$return['responseHTML'] = $view->render();
			}
		} else {
			$temp = array();
			$temp['header'] = 'Error:';
			$temp['content'] = 'The connection details (server, username, password and/or port) you provided are not correct. Please update the details and try again.';

			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();
		}

		$ftp->close();
		die(json_encode($return));
	}

	/**
	 * Test FTP connection
	 * @param  Request $request
	 * @return JSON
	 */
	public function postFTPTest(Request $request)
	{
		$path = ($request->input('siteSettings_ftpPath')) ? trim($request->input('siteSettings_ftpPath')) : '/';
		$ftp = new CI_FTP;
		$config = array(
			'hostname' => trim($request->input('siteSettings_ftpServer')),
			'username' => trim($request->input('siteSettings_ftpUser')),
			'password' => trim($request->input('siteSettings_ftpPassword')),
			'port' => trim($request->input('siteSettings_ftpPort')),
			'debug' => FALSE,
		);
		if ($ftp->connect($config)) {
			$list = $ftp->list_files($path);
			if ($list) {
				$temp = array();
				$temp['header'] = 'All good!';
				$temp['content'] = 'The provided FTP details are all good and can be used to publish this site.';

				$return = array();
				$return['responseCode'] = 1;
				$view = View('partials.success', array('data' => $temp));
				$return['responseHTML'] = $view->render();
			} else {
				$temp = array();
				$temp['header'] = 'Error:';
				$temp['content'] = 'The path you have provided is not correct or you might not have the required permissions to access this path.';

				$return = array();
				$return['responseCode'] = 0;
				$view = View('partials.error', array('data' => $temp));
				$return['responseHTML'] = $view->render();
			}
		} else {
			$temp = array();
			$temp['header'] = 'Error:';
			$temp['content'] = 'The connection details (server, username, password and/or port) you provided are not correct. Please update the details and try again.';

			$return = array();
			$return['responseCode'] = 0;
			$view = View('partials.error', array('data' => $temp));
			$return['responseHTML'] = $view->render();
		}

		$ftp->close();
		die(json_encode($return));
	}

	/**
	 * Test for FTP call
	 */
	public function getTest()
	{
		$ftp = new CI_FTP;
		$config = array(
			'hostname' => 'innovativebd.net',
			'username' => 'latest@innovativebd.info',
			'password' => 'admin123!',
			'port' => 21,
		);
		if ($ftp->connect($config)) {
			$ftp->mirror(public_path() . '/elements/images/', "/");
			dd($ftp->list_files());
		} else {
			die("can't connect");
		}
	}
}
