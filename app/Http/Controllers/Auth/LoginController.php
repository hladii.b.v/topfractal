<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use App\User; 
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Cookie;
 
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/ru';
    public function redirectTo() {
        
        return view('layouts.main');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function redirectToGoogle() {   
        
        $lang = app()->getLocale();
        Cookie::queue('lang', $lang, $lang);     
        
        return Socialite::driver('google')->redirect();
    }
    public function handleGoogleCallback() {
        $lang = Cookie::get('lang');
       //dd( Socialite::driver('google'));
        try {

            $user = Socialite::driver('google')->user();
         
            $finduser = User::where('email', $user->email)->first();
            if ($finduser) {
                auth()->login($finduser);
                return view('layouts.main');
            } else {
                 $password = '';

                $desired_length = rand(8, 12);

                for($length = 0; $length < $desired_length; $length++) {
                    $password .= chr(rand(32, 126));
                }
                $to_name = $user->name;
                $to_email = $user->email;
                $data_mail = array('name'=>$to_name, 'body' => 'Ваш пароль ' . $password);
                Mail::send('auth/email/mail', $data_mail, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                            ->subject('Створення нового конристувач');
                    $message->from( env('MAIL_USERNAME', 'hladii.b.v@gmail.com'), 'Створення нового конристувача');
                });
                $newUser = User::create([
                    'role_id' => 1,
                    'email' => $to_email,
                    'password' => Hash::make($password),
                    'first_name' => $to_name,
                    'type' => 'user',
                    'active' => 1,
                    'google_id' => $user->id
                ]);

                auth()->login($newUser);
                return view('layouts.main');
            }
        }
        catch(Exception $e) {
            return redirect('auth/google');
        }
    }

    public function logout(Request $request) {       
        auth()->logout();
        return view('layouts.main');
    }
}
