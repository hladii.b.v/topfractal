<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    public function redirectTo() {
        
        return route('home.page', ['locale' =>app()->getLocale()]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $password = '';

        $desired_length = rand(8, 12);

        for($length = 0; $length < $desired_length; $length++) {
            $password .= chr(rand(32, 126));
        }
        $to_name = $data['email'];
        $to_email = $data['email'];
        $data_mail = array('name'=>$to_name, 'body' => 'Ваш пароль ' . $password);
        Mail::send('auth/email/mail', $data_mail, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Створення нового конристувач');
            $message->from( env('MAIL_USERNAME', 'hladii.b.v@gmail.com'), 'Створення нового конристувача');
        });
        return User::create([
            'role_id' => 1,
            'email' => $data['email'],
            'password' => Hash::make($password),
            'api_token' => Str::random(80),
            'type' => 'user',
            'active' => 1
        ]);
    }
}
