<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialFacebookAccountService;
use Cookie;

class SocialAuthFacebookController extends Controller
{
  /**
   * Create a redirect method to facebook api.
   *
   * @return void
   */
    public function redirect()
    {
        $lang = app()->getLocale();
        Cookie::queue('lang', $lang, $lang);  
      
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */

    public function callback(SocialFacebookAccountService $service)
    {
        $lang = Cookie::get('lang');
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
        auth()->login($user);
        return redirect()->route('home.page', ['locale' => $lang]);

    }
}

