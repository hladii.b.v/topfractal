<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tariff;
use App\SitePage;
use App\Service;
use App\Template;
use App\TemplateMeta;
use App\Element;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index($lang)
    {
       
        $tariffs_number = Tariff::groupBy('number')->get('number');
		
		$tariffs = [];
		foreach ($tariffs_number as $value) {
			$tariff_price = Tariff::where('number', $value->number)->where('meta', 'price')->get(['id', 'value']);
			$tariff_text = Tariff::where('number', $value->number)->where('meta', 'text')->where('lang', $lang)->get(['id', 'value', 'lang', 'show']);
			
			$tariffs[$value->number] = [
				'tariff_price' => $tariff_price,
				'tariff_text' => $tariff_text	
			];
        }
        $page_content = [];
        $content = SitePage::where('page_name', 'home')->where('lang', $lang)->get();      
        foreach ($content as $value) {
            $page_content[$value->page_key] = $value->page_text;
        }
        
        $services = Service::where('lang', $lang)->where('show', 1)->get();
        return view('pages.home', compact('tariffs', 'page_content', 'services'));        
    }

    public function templates($lang)
    {
        $templates = Template::all();
        // $templateElements = [];
        // foreach ($templates as $template) {
        //     $templateElement = [];
            
        //     foreach ($template->metaElenents as $meta) {
        //         $templateElement[$meta->meta_key] = Element::where('id',  $meta->meta_value)->first(); 
        //     }
        //     $templateElements[] = [
        //         'id' => $template->id,
        //         'name' => $template->name,
        //         'element' => $templateElement,
        //     ];
        // }
        // dd($templateElements);
        return view('pages.layouts', compact('templates')); 
    }
}
