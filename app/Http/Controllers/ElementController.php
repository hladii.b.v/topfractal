<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Element;
use App\Category;
use App\Direction;
use App\TemplateMeta;
use App\Paremeter;

class ElementController extends Controller
{
    public function addCategory (Request $request) {
        
        $category = new Category;

        $category->name = $request->name;
        $category->link = $request->link;        
        $category->setHistoryAttribute($request->parameters);   
        $category->save();

        return redirect()->route('categories', ['locale' => app()->getLocale()]);
    }

    public function updateCategory (Request $request) {

        $category = Category::where('id', $request->categotyId)->first();
        
        $category->setHistoryAttribute($request->selectedValue);
        $category->update();
        return $request;
    }
    public function deleteCategory (Request $request) {

        Category::where('id', $request->categotyId)->delete();
        return redirect()->route('categories', ['locale' => app()->getLocale()]);
    }

    public function getCategories () {
        $categories = Category::all();
        $directions = Direction::all();
        $paremeters = Paremeter::where('lang', 'ua')->get();
        foreach ($categories as $category) { 
            $getCategory[]=  [
                'id' =>$category->id,
                'name' =>$category->name,
                'link' =>$category->link,
                'paremeters' => $category->getHistoryAttribute(),
            ]; 
        } 
        foreach ($directions as $direction) { 
            $getDirection[]=  [
                'id' =>$direction->id,
                'name' =>$direction->name,
                'categories' => $direction->getHistoryAttribute(),
            ]; 
        }            
		return view('templates.categories', compact('getCategory', 'getDirection', 'paremeters'));
    }

    public function deleteDirection (Request $request) {
       
        Direction::where('id', $request->directionId)->delete();
        return redirect()->route('categories', ['locale' => app()->getLocale()]);
    }

    public function updateDirection (Request $request) {

        $direction = Direction::where('id', $request->directionId)->first();
        $directionTemplates = $direction->templates;

        foreach ($directionTemplates as $template) {
            $templateMeta = TemplateMeta::where('template_id', $template->id)->get();
            foreach ($templateMeta as $meta) {
                if (in_array($meta->meta_key, $request->selectedValue)) {
                    continue;
                }
                else {
                    TemplateMeta::where('id', $meta->id)->delete();
                }
            }
        }
            
        $direction->setHistoryAttribute($request->selectedValue);
        $direction->update();
        return $request;
    }

    public function addDirection (Request $request) {
        
        $direction = new Direction ;

        $direction->name = $request->name;
        $direction->setHistoryAttribute($request->categories);
        $direction->save();

        return redirect()->route('categories', ['locale' => app()->getLocale()]);
    }

    public function getElements () {
        
        
        $categories = Category::all();
        $elementsCategory = [];
       
        $elementsUnSlected =Element::where('category_id', null)->get();
        foreach ($categories as $category) {
            $elements = $category->elements;           
            if ( count($elements)>0) {
                $elementsSlected = [];
                foreach ($elements as $element) {
                    $elementsSlected[] = [
                        'id' => $element['id'],
                        'name' =>$element['name'],
                        'url' =>$element['url'],
                        'thumbnail' =>$element['thumbnail'],
                        'show' =>$element['show'],
                        'category' =>$category->id  
                    ];
                }
                $elementsCategory[] = [
                    'id' => $category->id,
                    'name' =>$category->name  ,
                    'elements' =>$elementsSlected,
                ];                              
            }                 
        }
        
       
		return view('templates.elements', compact('elementsCategory', 'elementsUnSlected', 'categories'));
    }

    public function elementCategory (Request $request) {

        $element = Element::where('id', $request->elementId)->first();
        if ( $request->categotyId == 0) {
            
            $category = null;
        }
        else  {
            $category = $request->categotyId;
        }
        
        $element->category_id = $category; 
        $element->update();
        return redirect()->route('elements', ['locale' => app()->getLocale()]);
    }

    public function showElement (Request $request) {
        $element = Element::where('id', $request->elementID)->first();
        $element->show = $request->elementShow; 
        $element->update();
        return $request;
    }

    public function nameElement (Request $request) {
        $element = Element::where('id', $request->elementID)->first();
        $element->name = $request->elementName; 
        $element->update();
        return $request;
    }

}
