<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    protected $fillable = [

        'role_id',
        'email',
        'password',
        'type',
        'active',
        'google_id'

    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sites()
    {
    	return $this->hasMany('App\Site');
    }

    public function payment()
    {
    	return $this->hasMany('App\Payment');
    }
}
