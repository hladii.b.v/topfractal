<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frame extends Model
{
    public function metaFrame()
    {
    	return $this->hasMany('App\FrameMeta');
    }
}
