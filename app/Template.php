<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public function metaElenents()
    {
    	return $this->hasMany('App\TemplateMeta')->orderBy('meta_key');
    }

    public function direction()
    {
    	return $this->belongsTo('App\Direction');
    }
}
