<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    public function templates()
    {
    	return $this->hasMany('App\Template');
    }

    public function benefits()
    {
    	return $this->hasMany('App\Benefit');
    }

    public function getHistoryAttribute()
    {
        return unserialize($this->category_id);
    }
    public function setHistoryAttribute($value)
    {
        $this->attributes['category_id'] = serialize($value);
    }
}
