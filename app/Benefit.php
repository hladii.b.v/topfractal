<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Benefit extends Model
{
    use Notifiable;    
    
    public function childrenAdmin($user)
    {
        $user = Benefit::where('user_id', $user)
            ->where('parent_id', $this->id);
      
        $benefits =  Benefit::where('user_id', NULL)       
            ->where('parent_id', $this->id)
            ->union($user)
            ->get();
        
        return $benefits;

    }
	 
    public function children($user, $count)
    {
        $user = Benefit::where('user_id', $user)
            ->where('parent_id', $this->id);
      
        $benefits =  Benefit::where('user_id', NULL)       
            ->where('parent_id', $this->id)
            ->union($user)
            ->get();
        $benefitArray = [];
        foreach ($benefits as $key=>$benefit) {
            $benefitStatus = false;
            if ( $count < 4 && $key == 0) {
                $benefitStatus = true;
            }
            $benefitArray[] = [
                'id' =>  $benefit->id,
                'desc' =>  $benefit->desc,
                'img' =>  $benefit->img,
                'parent_id' =>  $benefit->parent_id,
                'selected' =>  $benefitStatus,
            ];
        }
        return $benefitArray;

    }

    public function childrenSelected($user, $array)
    {      
        $user = Benefit::where('user_id', $user)
            ->where('parent_id', $this->id);
      
        $benefits =  Benefit::where('user_id', NULL)       
            ->where('parent_id', $this->id)
            ->union($user)
            ->get();
        $benefitArray = [];
        foreach ($benefits as $benefit) {
            $benefitStatus = false;
            if (in_array (  $benefit->id, $array) ) {
                $benefitStatus = true;
            }
            $benefitArray[] = [
                'id' =>  $benefit->id,
                'desc' =>  $benefit->desc,
                'img' =>  $benefit->img,
                'parent_id' =>  $benefit->parent_id,
                'selected' =>  $benefitStatus,
            ];
        }
        return $benefitArray;
    }

    public function childrenServicesSelected($user, $array)
    {      
        $user = Benefit::where('user_id', $user)
            ->where('parent_id', $this->id);
      
        $services =  Benefit::where('user_id', NULL)       
            ->where('parent_id', $this->id)
            ->union($user)
            ->get();
        $servicesArray = [];
        foreach ($services as $service) {
            $serviceStatus = false;
            $price = null;
            foreach ($array as $value) {

                if (in_array (  $service->id, $value) ) {
                    $serviceStatus = true;
                    if ( isset($value['price'])) {
                        $price =  $value['price'];
                    }
                    
                }
            }
           
            $servicesArray[] = [
                'id' =>  $service->id,
                'desc' =>  $service->desc,
                'img' =>  $service->img,
                'parent_id' =>  $service->parent_id,
                'selected' =>  $serviceStatus,
                'price' => $price,
            ];
        }
        return $servicesArray;
    }

    public function direction()
    {
    	return $this->belongsTo('App\Direction');
    }

}
