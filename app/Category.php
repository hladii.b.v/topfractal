<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    

    public function elements()
    {
    	return $this->hasMany('App\Element', 'category_id');
    }

    public function getHistoryAttribute()
    {
        return unserialize($this->paremeters);
    }
    public function setHistoryAttribute($value)
    {
        $this->attributes['paremeters'] = serialize($value);
    }

}
