<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'client_id' => '255806118955205',
        'client_secret' => '96c10f76943a1e2f2e37ba4e372954f6',
        'redirect' => 'https://topfractal.com/callback'
    ],
    'google' => [
        'client_id' => '827143410680-ad89eag6n70npuf5ge4oc9mgkums1me7.apps.googleusercontent.com',
        'client_secret' => 'jMpUyZWfGSxxKozK7TikLeWx',
        'redirect' => 'https://topfractal.com/auth/callback/google',
    ],

];
