<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





//Route::view('/{path?}','layouts.main')->name('home'); 
Route::group(array('domain' => 'topfractal.com'), function(){

    // Route::get('/restart', 'SiteController@restartApach');
    // Route::get('/callback', 'SocialAuthFacebookController@callback');
    // Route::get('auth/callback/google', 'Auth\LoginController@handleGoogleCallback');

    // Route::group([
    //     'prefix' => '{locale}',
    //     'where' => ['locale' => '[a-zA-Z]{2}'],
    //     'middleware' => 'setlocale'
    // ], function () {

    //     Route::get('auth/google', 'Auth\LoginController@redirectToGoogle')->name('auth.google');
    //     Route::get('/redirect', 'SocialAuthFacebookController@redirect')->name('facebook.redirect');
    // });

    // Route::get('/', function () {
    //     return File::get(public_path() .'/index.php');
    // })->name('home');
    
    Route::get('/{path}', function () {
        return view('layouts.main');
    })->where('path', '.*')->name('home');
 
});


// Site and Page settings

Route::group(array('domain' => 'panel.topfractal.com'), function(){

    Auth::routes();

    Route::get('/layouts', [
        'uses' => 'HomeController@templates',
        'as' => 'layouts',
        'middleware' => 'auth'
    ]);

    // Dashboard section

    Route::get('/', [
        'uses' => 'DashboardController@index',
        'as' => 'page.content.dashboard',
        'middleware' => 'auth'
    ]);

    Route::get('/clear-page-text/{id}', [
        'uses' => 'DashboardController@clearText',
        'as' => 'dashboardText',
        'middleware' => 'auth'
    ]);

    Route::post('/update-page-text', [
        'uses' => 'DashboardController@updateText',
        'as' => 'update.page.text',
        'middleware' => 'auth'
    ]);

    Route::post('/update-create-text', [
        'uses' => 'DashboardController@updateCreateText',
        'as' => 'update.create.text',
        'middleware' => 'auth'
    ]);

    Route::get('/dashboard/{status}', [
        'uses' => 'SiteController@getDashboardNew',
        'as' => 'dashboard',
        'middleware' => 'auth'
    ]);

    Route::get('/siteAjaxUpdate/{order}', [
        'uses' => 'SiteController@postAjaxUpdate',
        'as' => 'siteAjaxUpdateG',
        'middleware' => 'auth'
    ]);

    Route::post('/siteAjaxUpdateChech', [
        'uses' => 'SiteController@postAjaxUpdateChech',
        'as' => 'siteAjaxUpdate',
    ]);

    Route::get('/site/{site_id}', [
        'uses' => 'SiteController@getSite',
        'as' => 'site',
        'middleware' => 'auth'
    ]);


    Route::post('/site/tsave', [
        'uses' => 'SiteController@postTSave',
        'as' => 'site-tsave',
        'middleware' => 'auth'
    ])->name('site-tsave');;

    Route::get('/siteData', [
        'uses' => 'SiteController@getSiteData',
        'as' => 'siteData',
        'middleware' => 'auth'
    ]);


    // Publish and export section
    Route::post('/site/export', [
        'uses' => 'SiteController@postExport',
        'as' => 'site.export',
        'middleware' => 'auth'
    ]);

    Route::post('/site/publish/{type?}', [
        'uses' => 'SiteController@postPublish',
        'as' => 'site.publish',
        'middleware' => 'auth'
    ]);

    // Live preview route

    Route::post('/updatePageData', [
        'uses' => 'SiteController@postUpdatePageData',
        'as' => 'updatePageData',
        'middleware' => 'auth'
    ]);

    // User section route
    Route::get('/users', [
        'uses' => 'UserController@getUserList',
        'as' => 'users',
        'middleware' => 'auth'
    ]);

    Route::get('/benefits', [
        'uses' => 'SiteController@Benefits',
        'as' => 'benefits',
        'middleware' => 'auth'
    ]);

    Route::get('/benefits-update/{benefit}', [
        'uses' => 'SiteController@benefitsUpdate',
        'as' => 'benefits.update',
        'middleware' => 'auth'
    ]);

    Route::get('/blocks', [
        'uses' => 'ElementController@getElements',
        'as' => 'elements',
        'middleware' => 'auth'
    ]);

    Route::get('/categories', [
        'uses' => 'ElementController@getCategories',
        'as' => 'categories',
        'middleware' => 'auth'
    ]);

    Route::get('/templates', [
        'uses' => 'TemplateController@getTemplate',
        'as' => 'templates',
        'middleware' => 'auth'
    ]);
    Route::post('/templates-add', [
        'uses' => 'TemplateController@addTemplate',
        'as' => 'add.templates',
        'middleware' => 'auth'
    ]);
    Route::post('/templates-delete', [
        'uses' => 'TemplateController@deleteTemplate',
        'as' => 'delete.templates',
        'middleware' => 'auth'
    ]);
    Route::get('/template/{template}', [
        'uses' => 'TemplateController@getTemplateData',
        'as' => 'template',
        'middleware' => 'auth'
    ]);

    Route::post('/template-update', [
        'uses' => 'TemplateController@updateTemplate',
        'as' => 'update.template',
        'middleware' => 'auth'
    ]);

    Route::get('/layouts-update', [
        'uses' => 'AssetController@layoutsUpdate',
        'as' => 'layouts.update',
        'middleware' => 'auth'
    ]);

    Route::post('/benefits', [
        'uses' => 'SiteController@addBenefits',
        'as' => 'benefits.add',
        'middleware' => 'auth'
    ]);
    Route::get('/tariffs-select', [
        'uses' => 'AssetController@selectTariffs',
        'as' => 'tariffs.select',
        'middleware' => 'auth'
    ]);

    Route::get('/tariffs', [
        'uses' => 'AssetController@getTariffs',
        'as' => 'tariffs',
        'middleware' => 'auth'
    ]);

    Route::get('/additional', [
        'uses' => 'AssetController@getAdditional',
        'as' => 'additional',
        'middleware' => 'auth'
    ]);

    
    Route::post('/additional-delete', [
        'uses' => 'AssetController@deleteAdditional',
        'as' => 'additional.delete',
        'middleware' => 'auth'
    ]);

    Route::get('/tariff-delete/{tariff}', [
        'uses' => 'AssetController@deleteTariff',
        'as' => 'tariff-delete',
        'middleware' => 'auth'
    ]);

    Route::get('/tariffs-update', [
        'uses' => 'AssetController@tariffsUpdate',
        'as' => 'tariffs.update',
        'middleware' => 'auth'
    ]);

    Route::post('/tariffs', [
        'uses' => 'AssetController@addTariffs',
        'as' => 'tariffs.add',
        'middleware' => 'auth'
    ]);

    Route::post('/text', [
        'uses' => 'SiteController@addText',
        'as' => 'text.add',
        'middleware' => 'auth'
    ]);

    Route::get('/text-delete/{benefit}', [
        'uses' => 'SiteController@deleteText',
        'as' => 'text-delete',
        'middleware' => 'auth'
    ]);

    Route::get('/benefits-delete/{benefit}', [
        'uses' => 'SiteController@deleteBenefits',
        'as' => 'benefits-delete',
        'middleware' => 'auth'
    ]);

    Route::post('/benefits-foto', [
        'uses' => 'SiteController@fotoBenefits',
        'as' => 'benefits.foto',
        'middleware' => 'auth'
    ]);

    Route::post('/layouts-foto', [
        'uses' => 'AssetController@fotoLayouts',
        'as' => 'layouts.foto',
        'middleware' => 'auth'
    ]);

    Route::get('/domains', [
        'uses' => 'SiteController@getDomain',
        'as' => 'domains',
        'middleware' => 'auth'
    ]);

    Route::get('/services', [
        'uses' => 'SiteController@Services',
        'as' => 'services',
        'middleware' => 'auth'
    ]);

    Route::get('/services-benefits', [
        'uses' => 'SiteController@ServicesBenefits',
        'as' => 'services-benefits',
        'middleware' => 'auth'
    ]);

    Route::get('/services-parent/{service}', [
        'uses' => 'SiteController@parentServices',
        'as' => 'services-parent',
        'middleware' => 'auth'
    ]);

    Route::post('/services', [
        'uses' => 'SiteController@addServices',
        'as' => 'services.add',
        'middleware' => 'auth'
    ]);

    Route::get('/services-delete/{service}', [
        'uses' => 'SiteController@deleteServices',
        'as' => 'services-delete',
        'middleware' => 'auth'
    ]);

    Route::post('/user-create', [
        'uses' => 'UserController@postUserCreate',
        'as' => 'user-create',
        'middleware' => 'auth'
    ]);

    Route::post('/user-update', [
        'uses' => 'UserController@postUserUpdate',
        'as' => 'user-update',
        'middleware' => 'auth'
    ]);

    Route::get('/user-delete/{user_id}', [
        'uses' => 'UserController@getUserDelete',
        'as' => 'user-delete',
        'middleware' => 'auth'
    ]);

    Route::get('/user-enable-disable/{user_id}', [
        'uses' => 'UserController@getUserEnableDisable',
        'as' => 'user-enable-disable',
        'middleware' => 'auth'
    ]);
    Route::get('/account',  [
        'uses' => 'UserController@getUser',
        'as' => 'account',
        'middleware' => 'auth'
    ]);

    // Image Library section route
    Route::get('/assets', [
        'uses' => 'AssetController@getAsset',
        'as' => 'assets',
        'middleware' => 'auth'
    ]);

    Route::get('/text', [
        'uses' => 'SiteController@getText',
        'as' => 'text',
        'middleware' => 'auth'
    ]);

    Route::get('/layouts-colors', [
        'uses' => 'AssetController@getColors',
        'as' => 'layouts.colors',
        'middleware' => 'auth'
    ]);

    Route::post('/layouts-colors', [
        'uses' => 'AssetController@addLayouts',
        'as' => 'layouts.create',
        'middleware' => 'auth'
    ]);

    Route::post('/layouts-delete', [
        'uses' => 'AssetController@deleteLayouts',
        'as' => 'layouts.delete',
        'middleware' => 'auth'
    ]);

    Route::post('/upload-image', [
        'uses' => 'AssetController@uploadImage',
        'as' => 'upload.image',
        'middleware' => 'auth'
    ]);

    Route::get('/user-benefits', [
        'uses' => 'SiteController@addUserBenefits',
        'as' => 'user.benefits',
        'middleware' => 'auth'
    ]);
    Route::get('/refresh-text', [
        'uses' => 'AssetController@refreshText',
        'as' => 'refresh.text',
        'middleware' => 'auth'
    ]);

});

Route::post('/site/live/preview', [
    'uses' => 'SiteController@postLivePreview',
    'as' => 'live.preview',
    'middleware' => 'auth'
]);


Route::get('/site/getframe/{frame_id}', [
    'uses' => 'SiteController@getFrame',
    'as' => 'getframe',
    'middleware' => 'auth'
]);

Route::get('/site/trash/{site_id}', [
    'uses' => 'SiteController@getTrash',
    'as' => 'site.trash',
    'middleware' => 'auth'
]);

Route::get('/template-parent/{page_id}', [
    'uses' => 'SiteController@templateParent',
    'as' => 'template-parent',
    'middleware' => 'auth'
]);



Route::post('assets/delImage', [
    'uses' => 'AssetController@delImage',
    'as' => 'delImage',
    'middleware' => 'auth'
]);


Route::get('/refresh-img', [
    'uses' => 'AssetController@refreshImg',
    'as' => 'refresh.img',
    'middleware' => 'auth'
]);

Route::get('/user/ulogin', [
    'uses' => 'UserController@postULogin',
    'as' => 'user.ulogin',
    'middleware' => 'auth'
]);


Route::post('/user/uaccount', [
    'uses' => 'UserController@postUAccount',
    'as' => 'user.uaccount',
    'middleware' => 'auth'
]);

Route::get('/page-layouts', [
    'uses' => 'AssetController@pageLayouts',
    'as' => 'page.layouts',
    'middleware' => 'auth'
]);

Route::post('/LiqPayForm', [
    'uses' => 'SiteController@LiqPayForm',
    'as' => 'LiqPayForm',
    'middleware' => 'auth'
]);

Route::get('/getSitecount', [
    'uses' => 'DashboardController@getSitecount',
    'as' => 'getSitecount',
    'middleware' => 'auth'
]);

Route::POST('/tariffChecked', [
    'uses' => 'AssetController@tariffChecked',
    'as' => 'tariffChecked',
    'middleware' => 'auth'
]);

Route::POST('/SitteTariff', [
    'uses' => 'AssetController@SitteTariff',
    'as' => 'SitteTariff',
    'middleware' => 'auth'
]);

Route::post('/ajaxMeta-delete', [
    'uses' => 'TemplateController@deleteAjaxMeta',
    'as' => 'ajax.delete.templateMeta',
    'middleware' => 'auth'
]);

Route::post('/ajaxMeta-add', [
    'uses' => 'TemplateController@addAjaxMeta',
    'as' => 'ajax.add.templateMeta',
    'middleware' => 'auth'
]);

Route::post('/direction-add', [
    'uses' => 'ElementController@addDirection',
    'as' => 'direction.add',
    'middleware' => 'auth'
]);

Route::post('/direction-update', [
    'uses' => 'ElementController@updateDirection',
    'as' => 'direction.update',
    'middleware' => 'auth'
]);

Route::post('/direction-delete', [
    'uses' => 'ElementController@deleteDirection',
    'as' => 'direction.delete',
    'middleware' => 'auth'
]);

Route::post('/category-add', [
    'uses' => 'ElementController@addCategory',
    'as' => 'category.add',
    'middleware' => 'auth'
]);

Route::post('/category-delete', [
    'uses' => 'ElementController@deleteCategory',
    'as' => 'category.delete',
    'middleware' => 'auth'
]);

Route::post('/category-update', [
    'uses' => 'ElementController@updateCategory',
    'as' => 'category.update',
    'middleware' => 'auth'
]);

Route::post('/category-element', [
    'uses' => 'ElementController@elementCategory',
    'as' => 'category.element',
    'middleware' => 'auth'
]);

Route::post('/element-show', [
    'uses' => 'ElementController@showElement',
    'as' => 'element.show',
    'middleware' => 'auth'
]);
Route::post('/element-name', [
    'uses' => 'ElementController@nameElement',
    'as' => 'element.name',
    'middleware' => 'auth'
]);

Route::get('/select-direction', [
    'uses' => 'SiteController@selectDirection',
    'as' => 'select.direction',
    'middleware' => 'auth'
]);

Route::post('/image-upload-ajax', [
    'uses' => 'AssetController@imageUploadAjax',
    'as' => 'image.upload.ajax',
    'middleware' => 'auth'
]);

Route::get('/site/rpreview/{site_id}/{datetime}/{page}', [
    'uses' => 'SiteController@getRevisionPreview',
    'as' => 'revision.preview',
    'middleware' => 'auth'
]);

Route::get('/deleterevision/{site_id}/{datetime}/{page}', [
    'uses' => 'SiteController@getRevisionDelete',
    'as' => 'revision.delete',
    'middleware' => 'auth'
]);

Route::get('/restorerevision/{site_id}/{datetime}/{page}', [
    'uses' => 'SiteController@getRevisionRestore',
    'as' => 'revision.restore',
    'middleware' => 'auth'
]);

Route::post('/image-upload-element', [
    'uses' => 'AssetController@imageUploadElement',
    'as' => 'image.upload.element',
    'middleware' => 'auth'
]);

Route::post('/image-upload-template', [
    'uses' => 'AssetController@imageUploadTemplate',
    'as' => 'image.upload.template',
    'middleware' => 'auth'
]);

Route::post('/template-element-type', [
    'uses' => 'TemplateController@templateElementType',
    'as' => 'template.element.type',
    'middleware' => 'auth'
]);

Route::post('/add-new-lang', [
    'uses' => 'DashboardController@addLang',
    'as' => 'add.new.lang',
    'middleware' => 'auth'
]);

Route::post('/delete-lang', [
    'uses' => 'DashboardController@daleteLang',
    'as' => 'delete.lang',
    'middleware' => 'auth'
]);

Route::post('/update-lang', [
    'uses' => 'DashboardController@updateLang',
    'as' => 'update.lang',
    'middleware' => 'auth'
]);
