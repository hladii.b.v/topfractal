<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api', 'prefix' => 'v1'], function() {

    Route::post('/broadcasting/auth', 'ApiController@retutTrue');

    Route::post('/getToken','ApiController@getToken');

    Route::post('/createUser','ApiController@createUser');

    Route::post('/facebook-login','ApiController@facebookLogin');

    Route::post('/google-login','ApiController@googleLogin');

    Route::post('/get-reset-password','ApiController@resetPasswordForm');

    Route::post('/reset-password','ApiController@resetPassword');

    Route::get('/home-page/{lang}', 'PageController@home');

    Route::get('/get-lang', 'PageController@getLang');


    Route::group(['middleware' => 'auth:api'], function () {

        Route::post('/logout',  'ApiController@logout');

        Route::get('/getUserByToken',  'ApiController@getUserByToken');

        Route::post('/update-user',  'ApiController@updatteUser');

        Route::post('/update-user-password',  'ApiController@updateUserPassword');

        Route::post('/update-user-avatar',  'ApiController@loadAvatar');

        Route::get('/get-user-img',  'ApiController@getImg');

        Route::post('/upload-user-img', 'ApiController@uploadImg');

        Route::post('/delete-user-img', 'ApiController@delImage');

        Route::get('/directions',  'SiteController@direction');

        Route::get('/direction/{direction}',  'SiteController@directionTemplates');

        Route::get('/template/{template}/{lang}',  'SiteController@template');

        Route::get('/category/{category}/{template}',  'SiteController@category');

        Route::post('/site-create',  'SiteController@createSite');

        Route::get('/site-data/{site}/{lang}',  'SiteController@getSiteData');

        Route::get('/site/{site}/{lang}',  'SiteController@getSite');

        Route::get('/sites',  'SiteController@getAllSites');

        Route::patch('/site-edit',  'SiteController@editSite');

        Route::post('/site-delete/{site}',  'SiteController@deleteSite');

        Route::get('/preview/{site}',  'SiteController@getSitePreview');

        Route::post('/site/publish/{site}',  'SiteController@sitePublish');

        Route::get('/refresh-text', 'AssetController@refreshText');

        Route::get('/refresh-img', 'AssetController@refreshImg');

        Route::get('/get-img', 'AssetController@getImg');

        Route::post('/upload-img', 'AssetController@uploadImg');

        Route::post('/upload-pdf', 'AssetController@uploadPdf');

        Route::post('/add-benefit', 'AssetController@addBenefit');

        Route::post('/add-services', 'AssetController@addServices');

        Route::post('/create-order', 'PaymentController@createOrder');

    });
});
