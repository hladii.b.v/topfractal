	
	var	locale = document.getElementById('locale').value;

    function selectOnlyThis (id){
          var myCheckbox = document.getElementsByName("selected_color");
          Array.prototype.forEach.call(myCheckbox,function(el){
            el.checked = false;
          });
        id.checked = true;
        imgType = 'header_img'
        imgLogo = 'header_logo'
        timeSlots = ''
        Slots = ''
        response = ''
        header_logo =''
        document.getElementById(imgType+"Files").innerHTML=''
        $.ajax({
            type: 'GET',
            url: window.location.origin +'/page-layouts',
            data: {id: id.value },
            success: function (response) {	
                if(response.img === undefined &&  response.logo  === undefined) {
                    img = 'elements/images/header_img/main.jpg'
                    logo = 'elements/images/header_logo/logoxxx05-01 [преобразованный].png'
                    timeSlots = "<img src='elements/images/header_img/main.jpg'  id=\""+ imgType + "_file\"  class=\"imgBlocksInside\" >";
                    Slots = "<img src='elements/images/header_logo/logoxxx05-01 [преобразованный].png'  id=\""+ imgLogo + "_file\"  class=\"imgBlocksInside\" >";
                }	
                else {
                    img = response.img
                    logo = response.logo
                    timeSlots = "<img src=\"" + response.img + "\"  id=\""+ imgType + "_file\"  class=\"imgBlocksInside\" >";
                    Slots = "<img src=\"" + response.logo + "\"  id=\""+ imgLogo + "_file\"  class=\"imgBlocksInside\" >";
                    
                }		
                
                document.getElementById(imgType).value = img
                document.getElementById(imgType+"Files").innerHTML=timeSlots;
                document.getElementById(imgLogo).value = logo
                document.getElementById(imgLogo+"Files").innerHTML=Slots;
            }
        })	
    
        
    }
    
    function showDoctorImg(id) {
        var selDiv_doctor = "";
    
        document.querySelector('#files_doctor'+id).addEventListener('change', handleFileSelect_doctor, false);
        selDiv_doctor = document.querySelector("#doctor"+id+"Files");
        selDiv_doctor.innerHTML = "";
        function handleFileSelect_doctor(e) {
    
            if(!e.target.files || !window.FileReader) return;
    
            selDiv_doctor.innerHTML = "";
            var i=0;
            var files = e.target.files;
            var filesArr = Array.prototype.slice.call(files);
            filesArr.forEach(function(f) {
                var f = files[i];
                if(!f.type.match("image.*")) {
                    return;
                }
    
                var reader = new FileReader();
                reader.onload = function (e) {
                    var html = "<img src=\"" + e.target.result + "\" class=\"imgBlocksInside\" ><input type=\"hidden\" name=\"doctor_file_text[]\" value="+ e.target.result+"\">";
                    selDiv_doctor.innerHTML = html;
                }
                i++;
                reader.readAsDataURL(f);
            });
    
        }
    }
    
    
    function user_benefit (id) {
        name = document.getElementById('user_benefit_'+id).value
        $.ajax({
                type: 'GET',
                url: window.location.origin + '/user-benefits',
                data: {id: id, param: name, type: 'benefit'},
                success: function (response) {
                    console.log(response)
                    var el = document.getElementById('text-generation'+id);
                    el.insertAdjacentHTML('beforeend',
                    `<li class="list-group-item">
                        <input key=${response.id} type='checkbox' name="benefit_${response.parent_id}[]" id="${response.id}" class="form-check-input" value=${response.id} />
                        <label class="custom-label khasan-for-label" for="${response.id}">
                            <span class="slider-my-custom round"></span>
                            ${response.desc}
                        </label>
                    </li>`);
                },
                error: function () {
                    console.log(response);
                }
            })
    }
    
    
    function user_services (id) {
        name = document.getElementById('user_benefit_'+id).value
        $.ajax({
                type: 'GET',
                url: window.location.origin +'/' +locale+ '/user-benefits',
                data: {id: id, param: name, type: 'services'},
                success: function (response) {
                    console.log(response)
                    var el = document.getElementById('text-generation'+id);
                    el.insertAdjacentHTML('beforeend',
                    `<li class="list-group-item li-services">
                        <input key=${response.id} type='checkbox' name="benefit_${response.parent_id}[]" id="${response.id}" class="form-check-input" value=${response.id} checked/>
                        <label class="custom-label khasan-for-label" for="${response.id}">
                            <span class="slider-my-custom round"></span>
                            ${response.desc}
                        </label>
                        <input type="text" class="li-services-price" name="benefit_${response.id}[]"  placeholder="ціна">
                    </li>`);
                },
                error: function () {
                    console.log(response);
                }
            })
    }
    
    $("#submitBtn").click(function () {
        $('html, body').css({
            overflow: 'hidden',
            height: '100%'
        });
        $("#header, #cloned").hide();
        $(".overlay").show()	
    })
    
    document.getElementById('header_logo').value = document.getElementById('header_logo_file').src
    document.getElementById('header_img').value = document.getElementById('header_img_file').src
    
        function refresh_img (imgType) {
    
            $("#"+imgType+"Files").before("<div class='load lds-ring'><div></div><div></div><div></div><div></div></div>");
            $("#"+imgType+"_file").hide()
            $.ajax({
                type: 'GET',
                url: window.location.origin + '/refresh-img',
                data: {param: imgType},
                success: function (response) {
    
                    let timeSlots = '';
                    timeSlots = "<img src=\"" + response + "\"  id=\""+ imgType + "_file\"  class=\"imgBlocksInside\" >";
    
                    document.getElementById(imgType).value = response
                    document.getElementById(imgType+"Files").innerHTML=timeSlots;
                    document.getElementById(imgType+"_file").style.display = 'none';
                    document.getElementById(imgType+"_file").onload = function() {
                        document.getElementById(imgType+"_file").style.display = 'block';
                        $(".load").remove();
                    }
                },
                error: function () {
                    console.log(response);
                }
            })
        }
    
        function refresh_text (textType) {
            $.ajax({
                type: 'GET',
                url: window.location.origin  + '/' +locale+ '/refresh-text',
                data: {param: textType},
                success: function (response) {
                    document.getElementById(textType).value = response
                },
                error: function () {
                    console.log(response);
                }
            })
        }
        const listContainer = document.querySelector('#elementCats')
    const tamplate_footer = document.querySelector('#tamplate_footer')
    const tamplate_header = document.querySelector('#tamplate_header')
    const page_id = document.getElementById('page_id').value
    const parent_id = document.getElementById('parent_id').value
    const tamplate_benefits = document.querySelector('#tamplate_benefits')
    
    
    let elements = ''
    const dataFromUrl = window.location.search
    //const normalizeData = JSON.parse(dataromUrl.split('data=')[1].split('%22').join('"'))
   
    
    $.getJSON(window.location.origin + '/elements.json').then((res) => {
    
        if (parent_id == 83) {
            elements = res.elements.Carat_section
        }
        else if (parent_id == 302) {
            elements = res.elements.Prime_section
        }
        else if (parent_id == 303) {
            elements = res.elements.Medi_section
        } 
        else {
            getTemplate = $.ajax({
                type: 'GET',
                url: window.location.origin + '/template-parent/' + page_id,
                error: function () {
                    console.log(response);
                },
                success: function (data) {
                    if (data && data == 83) {
                        elements = res.elements.Carat_section
                    }
                    else if (data && data == 302) {
                        elements = res.elements.Prime_section
                    }
                }
            })
        
        }
    
    
        contentElements = $.ajax({
            type: 'GET',
            url:  window.location.origin + '/'+locale+'/services-benefits',
            error: function () {
                console.log(data);
            }
        }).done(function (data) {
    
  
            elements.reverse().forEach((element, i) => {
                $(".main_loader").fadeOut();
            
                if (element.id === 3) {
    
                if (parent_id == 302 || parent_id==303) {
                    insertItemm = tamplate_benefits
                }
                else if (parent_id == 83) {
                    insertItemm = listContainer
                }
    
                locale = document.getElementById('locale').value;
                insertItemm.insertAdjacentHTML("afterbegin",
                    `<div class="box1">
                <div class="notification warning closeable" id="checkbox-area">
                    <input type="checkbox" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}" 
                    ${Object.values(normalizeData.benefits).length ? 'checked':''}/>
                    <span class="slider-my-custom round"></span>
                    <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                    <label class="form-check-label" for="element${element.id}">
                        ${locale == 'ru' ? 'ПРЕИМУЩЕСТВА' : 'ПЕРЕВАГИ'}
                        
                        <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                    </label>
                    ${(parent_id == 83 ? '<a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>' : '')}
                </div>
                <div id="text-generation${element.id}" class="drop-down-text is-active-custom" style="display: none;">
                    ${(parent_id == 83 ? `<h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Изменить название секции' : 'Змінити назву секції')}</h5>
                    <input  name="benefit_title"  value="${ Object.values(normalizeData.benefits).length  ? 
                        normalizeData.benefits.benefits_title : (locale == 'ru' ?  'ПРЕИМУЩЕСТВА' : 'ПЕРЕВАГИ') }">
                    </br> `: '')}					
                    <ul class="ul-custom" >
                        ${data.map((benefitDesc, index) =>
                        ((element.id === 3 && benefitDesc.type === 'benefit'))
                            ?
                            `<li class="list-group-item parent-benefit" >
                                <input key=${index} type='checkbox' name="${element.url}[]" id="${benefitDesc.id}" class="form-check-input" value=${benefitDesc.id} checked/>
    
                                <a class="showMoreText" data-id="${benefitDesc.id}"/>	${benefitDesc.desc}</a>
    
    
                                <a class="showMore" data-id="${benefitDesc.id}"/><i class="sl sl-icon-settings"> </i></a>
                            </li>
    
                            <ul id="text-generation${benefitDesc.id}" class=" once_checked drop-down-text is-active-custom" style="display: none;">
    
                                ${benefitDesc.children && benefitDesc.children.map(khasan =>
                                `<li class="list-group-item">
                                        <input key=${khasan.id} type='checkbox' name="benefit_${benefitDesc.id}[]" id="${khasan.id}" class="form-check-input" value=${khasan.id} />
                                        <label class="custom-label khasan-for-label" for="${khasan.id}">
                                            <span class="slider-my-custom round"></span>
                                            <span class="benefit-text-custom">${khasan.desc}</span>
                                        </label>
                                    </li>`
                            ).join('')}
                            <div class="plus-wrapper">
                                <input type="text" id="user_benefit_${benefitDesc.id}">
                                <a onclick="user_benefit(${benefitDesc.id})"><i class="sl sl-icon-plus"></i></a>
                            </div>
                            </ul>`
                            : ''
                    ).join('')}
                    </ul>
                </div>
                </div>
                `)
                // }
                }
                else if (element.id === 4 ) {
    
                locale = document.getElementById('locale').value;
                listContainer.insertAdjacentHTML("afterbegin",
                    `<div class="box1">
                        <div class="notification warning closeable" id="checkbox-area">
                            <input type="checkbox" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}"
                            ${Object.values(normalizeData.service).length ? 'checked':''}/> 
                            <span class="slider-my-custom round"></span>
                            <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                            <label class="form-check-label" for="element${element.id}">
                                ${(locale == 'ru' ? 'УСЛУГИ' : 'ПОСЛУГИ')}
                                <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                            </label>
                            <a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>
                        </div>
                        <div id="text-generation${element.id}" class="drop-down-text is-active-custom" style="display: none;">
                            <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Изменить название секции' : 'Змінити назву секції')}</h5>
                            <input  name="services_title"  value="${ Object.values(normalizeData.service).length  ? 
                                normalizeData.service.services_title : (locale == 'ru' ?  'УСЛУГИ' : 'ПОСЛУГИ')}"></br>
                            <ul class="ul-custom"  >
                                ${data.map((benefitDesc,index) =>
                                (((element.id === 4 && benefitDesc.type === 'services')))
                                ?
                                    `<li class="list-group-item parent-benefit">
                                        <input key=${index} type='checkbox' name="${element.url}[]" id="${benefitDesc.id}" class="form-check-input" value=${benefitDesc.id} checked/>
                                        <label class="custom-label" for="${benefitDesc.id}">
                                            <span class="slider-my-custom round"></span>
                                        </label>
                                        <input type="text" name="services_names[${benefitDesc.id}]" value="${benefitDesc.desc}">
                                        <a class="showMore" data-id="${benefitDesc.id}"/><i class="sl sl-icon-settings"> </i></a>
                                    </li>
                                    <ul  id="text-generation${benefitDesc.id}" class="drop-down-text is-active-custom" style="display: none;">
                                        ${benefitDesc.children && benefitDesc.children.map( khasan =>
                                            `<li class="list-group-item li-services">
                                                <input key=${khasan.id} type='checkbox' name="benefit_${benefitDesc.id}[]" id="${khasan.id}" class="form-check-input" value=${khasan.id} checked/>
                                                <label class="custom-label khasan-for-label" for="${khasan.id}">
                                                    <span class="slider-my-custom round"></span>
                                                    <span class="benefit-text-custom">${khasan.desc}</span>
                                                </label>
                                                <input type="text" id="benefit_${khasan.id}" class="li-services-price" name="benefit_${khasan.id}[]" placeholder="ціна">
                                            </li>`
                                        ).join('') }
                                        <div class="plus-wrapper">
                                            <input type="text" id="user_benefit_${benefitDesc.id}">
                                            <a onclick="user_services(${benefitDesc.id})"><i class="sl sl-icon-plus"></i></a>
                                        </div>
                                    </ul>`
                                : ''
                                ).join('')}
    
                            </ul>
                            <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Файл с ценами' : 'Файл з цінами')}</h5>
                            <label class="custom-file-input" for="services_pirce" ></label>
                            <input type="file" id="services_pirce" name="services_pirce" accept=".pdf" value="${Object.values(normalizeData.service).length   ? 
                                normalizeData.service.service_price : ''}">
                        </div>
                    </div>
                    `)
    
                // }
                }
                else if (element.id === 1) {
                locale = document.getElementById('locale').value;
                tamplate_header.insertAdjacentHTML("afterbegin",
                `<div class="notification warning closeable box1" id="checkbox-area">
                    <input type="text" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}"   style="visibility: hidden;">
                    <span class="slider-my-custom round"  style="visibility: hidden;"></span>
                    <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                    <label class="form-check-label" for="element${element.id}">
                        ${(locale == 'ru' ? 'ЗАГОЛОВКИ' : 'ЗАГОЛОВКИ')}
    
                        <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                    </label>
                </div>
                <div id="text-generation${element.id}" class="drop-down-text is-active-custom" style="display: none;">
                    ${(parent_id == 83 ? (
                    `<h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Текст на основной картинке' : 'Текст на основній картинці')}
                    <a onclick="refresh_text('headerText')"> <i class="fa fa-refresh" style="font-size:24px"></i></a></h5>
                    <input name="header_text" id="headerText"
                    value="${ Object.values(normalizeData.header).length  ? 
                                normalizeData.header.header_text :
                                'Ми надаємо повний комплекс високоякісних стоматологічних послуг для всіх членів сім`ї доступними цінами.'}">
                    </br>`): '')}
                    <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Девиз стоматологии' : 'Девіз стоматології')}
                        <a onclick="refresh_text('headerMotto')"> <i class="fa fa-refresh" style="font-size:24px"></i></a></h5>
                    <input  name="header_motto" id="headerMotto" value="
                    ${ Object.values(normalizeData.header).length  ? 
                                normalizeData.header.header_motto :
                                'Дбайливе ставлення до кожного пацієнта'}">
                    </br>
                    <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Текст на основной картинке' : 'Текст на основній картинці ')}
                        <a onclick="refresh_text('headerTitle')"> <i class="fa fa-refresh" style="font-size:24px"></i></a>
                    </h5>
                    <input  name="header_title" id="headerTitle" value="
                    ${ Object.values(normalizeData.header).length  ? 
                                normalizeData.header.header_title :
                                'Стоматологічна клініка для всієї родини!'}"></br>
    
                </div>
                `)
                }
                else if (element.id === 2) {
                locale = document.getElementById('locale').value;
                $('.imgBlocks').slick();
                listContainer.insertAdjacentHTML("afterbegin",
                    `<div class="notification warning closeable box1" id="checkbox-area">
                    <input type="checkbox" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}" 
                    ${ Object.values(normalizeData.about).length  ? 'checked' : ''}/>
                    <span class="slider-my-custom round"></span>
                    <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                    <label class="form-check-label" for="element${element.id}">
                    ${(locale == 'ru' ? 'О КЛИНИКЕ' : 'ПРО КЛІНІКУ')}
                    <div data-slick='{"slidesToShow": 4, "slidesToScroll": 4}'>
                    <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                    </div>
                    </label>
                    <a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>
                </div>
                <div id="text-generation${element.id}" class="drop-down-text is-active-custom" style="display: none;">
                    <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Изменить название секции' : 'Змінити назву секції')}</h5>
                    <input  name="about_title" id="about_title" value="${ Object.values(normalizeData.about).length  ? 
                                normalizeData.about.about_title : 'Про клініку'}"></br>
                    <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Текст "О клинике"' : 'Текст "Про клініку"')}
                        <a onclick="refresh_text('aboutText')"> <i class="fa fa-refresh" style="font-size:24px"></i></a></h5>
                    <textarea name="about_text" id="aboutText">
                        ${ Object.values(normalizeData.about).length  ? 
                                normalizeData.about.about_text : 
                                'Одним з основних досягнень клініки є здобута довіра наших пацієнтів, оскільки, велика кількість наших відвідувачів звертається до нас за рекомендаціями своїх рідних чи друзів'}.
                    </textarea></br>
                    ${(parent_id == 302 || parent_id == 303 ? (
                    `<h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Текст "О клинике"' : 'Текст "Про клініку"')}
                    <a onclick="refresh_text('headerText')"> <i class="fa fa-refresh" style="font-size:24px"></i></a></h5>
                    <textarea name="header_text" id="headerText">${ Object.values(normalizeData.header).length  ? 
                                normalizeData.header.header_text :
                                'Ми надаємо повний комплекс високоякісних стоматологічних послуг для всіх членів сім`ї доступними цінами.'}</textarea>
                    </br>`): '')}
                    <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Фото "О клинике"' : 'Фото "Про клініку"')}</h5>
    
                    <div class="wrapper-galery">
    
                        <div id="aboutFiles" class="media-galery">
                            <img src="${ Object.values(normalizeData.header).length  ? 
                                normalizeData.about.about_img : 
                                './elements/images/about/21916_v-sevastopole-nachnut-rabot.jpg'}" id=\"about_file\"  class=\"imgBlocksInside\" >
                        </div>
                        <div class="galery-wrapper-custom">
                            <span class="btn btn-primary btn-file">
                                ${(locale == 'ru' ? 'Загрузить' : 'Завантажити')}<input type="file" name="about_file" class="custom-file-input" id="files"   accept=".jpg, .jpeg, .png">
                            </span>
                            <a onclick="refresh_img('about')"> <i class="fa fa-refresh" style="font-size:24px"></i></a>
                            <input type="hidden" name="about_img" id="about" value="
                            ${ Object.values(normalizeData.header).length  ? normalizeData.about.about_img : 
                                './elements/images/about/21916_v-sevastopole-nachnut-rabot.jpg'}">
                        </div>
                    </div>
    
                </div>
                `)
                }
                else if (element.id === 5) {
                locale = document.getElementById('locale').value;
                listContainer.insertAdjacentHTML("afterbegin",
                    `<div class="notification warning closeable box1" id="checkbox-area">
                    <input type="checkbox" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}" 
                    ${ Object.values(normalizeData.team.teams_name).length  ? 'checked' : ''}>
                    <span class="slider-my-custom round"></span>
                    <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                    <label class="form-check-label" for="element${element.id}">
                        ${(locale == 'ru' ? 'КОМАНДА' : 'КОМАНДА')}
                        <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                    </label>
                        <a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>
                    </div>
                    <div id="text-generation${element.id}" class="drop-down-text is-active-custom " style="display: none;">
                    <input class="doc_count" type="hidden" name="doc_count" value="${ Object.values(normalizeData.team.teams_name).length}">
                    <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Изменить название секции' : 'Змінити назву секції')}</h5>
                    <input  name="team_title"  value="${(locale == 'ru' ? 'КОМАНДА' : 'КОМАНДА')}"></br>
                    <div class="doctor-wrapper">
                        <a href="#" class="add_doctor"><i class="sl sl-icon-plus"></i> 
                        ${(locale == 'ru' ? 'Добавить врача' : 'Додати лікаря')}</a>
                        <a href="#" class="del_doctor">${(locale == 'ru' ? 'Удалить последнего' : 'Видалити останього')}</a>
                    </div>
                    ${ Object.values(normalizeData.team.teams_name).length ? normalizeData.team.teams_name.map((teams,index) =>
                    `<div class="${index == 0 ? 'doctor' : 'new_doc'}">				
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Врач '+(index+1) : 'Лікар '+(index+1))}</h5>
                        <div class="wrapper-galery">
    
                            <div id="doctor${(index+1)}Files" class="media-galery">
                                <img src="${normalizeData.team.teams_imgs[index]}" class="imgBlocksInside">
                                <input type="hidden" name="doctor_file_text[]" value="${normalizeData.team.teams_imgs[index]}" >
                            </div>
                            <div class="galery-wrapper-custom">
                                <span class="btn btn-primary btn-file">
                                    ${(locale == 'ru' ? 'Загрузить' : 'Завантажити')}
                                    <input type="file" name="doctor_file[${(index)}]" class="custom-file-input" id="files_doctor${(index+1)}" onClick="showDoctorImg(${(index+1)})"   accept=".jpg, .jpeg, .png" >
                                   
                                </span>
                            </div>
                        </div>
                        <input type="text"  name="doctor_name[]"  value="${normalizeData.team.teams_name[index]}"></br>
                        <input type="text" name="doctor[]"  value="${normalizeData.team.teams_text[index]}"></br>
                    </div>`)
                    : 
                    `<div class="doctor">
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Врач 1' : 'Лікар 1')}</h5>
                        <div class="wrapper-galery">

                            <div id="doctor1Files" class="media-galery">
                            </div>
                            <div class="galery-wrapper-custom">
                                <span class="btn btn-primary btn-file">
                                    ${(locale == 'ru' ? 'Загрузить' : 'Завантажити')}
                                    <input type="file" name="doctor_file[]" class="custom-file-input" id="files_doctor1" onClick="showDoctorImg(1)"   accept=".jpg, .jpeg, .png">
                                </span>
                            </div>
                        </div>
                        <input type="text"  name="doctor_name[]"  value="${(locale == 'ru' ? 'Имя' : 'Ім`я')}"></br>
                        <input type="text" name="doctor[]"  value="${(locale == 'ru' ? 'Описание' : 'Опис')}"></br>
                    </div>`}
    
                    </div>
    
                    `)
                    
                    $(".del_doctor").hide();
                    if(Object.values(normalizeData.team.teams_name).length > 1) $(".del_doctor").show();
                    $(".add_doctor").click(function (e) {
                        e.preventDefault()
                        count = $("#text-generation5 .doc_count").val();
                        countLoad = $("#text-generation5 .doc_count").val();
                        count++;
                        $("#text-generation5").append(`
                        <div class="new_doc">
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Врач '+count : 'Лікар '+count)}</h5>
                        <div class="wrapper-galery">
                            <div id="doctor`+count+`Files" class="media-galery">
                            <input type="hidden" name="doctor_file_text[`+countLoad+`]" value="">
                            </div>
                            <div class="galery-wrapper-custom">
                                <span class="btn btn-primary btn-file">
                                    ${(locale == 'ru' ? 'Загрузить' : 'Завантажити')}
                                    <input type="file" name="doctor_file[${countLoad}]" class="custom-file-input" id="files_doctor`+count+`" onClick="showDoctorImg(`+count+`)"   accept=".jpg, .jpeg, .png">
                                </span>
                            </div>
                            </div>
                            <input  type="text" name="doctor_name[]"  value="${(locale == 'ru' ? 'Имя' : 'Ім`я')}"></br>
                            <input type="text" name="doctor[]" value="${(locale == 'ru' ? 'Описание' : 'Опис')}"></br>
                        </div>
                        `)
                    $("#text-generation5 .doc_count").val(count)
                    $(".del_doctor").show();
                    })
    
                    $("#text-generation5").delegate(".del_doctor", "click", function (e) {
                    e.preventDefault();
                    count = $("#text-generation5 .doc_count").val();
                    count--;
                    $("#text-generation5 .new_doc").last().remove()
                    $("#text-generation5 .doc_count").val(count)
                    if(count < 2) $(".del_doctor").hide();
                    })
                }
                else if (element.id === 6) {
                locale = document.getElementById('locale').value;
                listContainer.insertAdjacentHTML("afterbegin",
                    `<div class="notification warning closeable box1" id="checkbox-area">
                        <input type="checkbox" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}"
                        ${ Object.values(normalizeData.porfolio).length  ? 'checked' : ''} >
                        <span class="slider-my-custom round"></span>
                        <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                        <label class="form-check-label" for="element${element.id}">
                        ${(locale == 'ru' ? 'ПОРТФОЛИО' : 'ПОРТФОЛІО')}
                        <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                        </label>
                        <a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>
                    </div>
                    <div id="text-generation${element.id}" class="drop-down-text is-active-custom " style="display: none;">
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Изменить название секции' : 'Змінити назву секції')}</h5>
                        <input  name="porfolio_title"  value="${(locale == 'ru' ? 'ПОРТФОЛИО' : 'ПОРТФОЛІО')}"></br>
                        <div class="wrapper-galery portfolio-wrapper">
    
                            <div id="carousel" >
    
                            </div>
                            <div class="galery-wrapper-custom">
                                <span class="btn btn-primary btn-file">
                                    ${(locale == 'ru' ? 'Загрузить' : 'Завантажити')}
                                    <input type="file" name="porfolio_file[]" class="custom-file-input" id="files_portfolio"   accept=".jpg, .jpeg, .png" multiple
                                    value>
                                </span>
                            </div>
                        </div>
                    </div>
                `)
                }
                else if (element.id === 7) {
                locale = document.getElementById('locale').value;
                listContainer.insertAdjacentHTML("afterbegin",
                    `<div class="notification warning closeable box1" id="checkbox-area">
                        <input type="checkbox" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}" 
                        ${ Object.values(normalizeData.reviews).length  ? 'checked' : ''}/>
                        <span class="slider-my-custom round"></span>
                        <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                        <label class="form-check-label" for="element${element.id}">
                        ${(locale == 'ru' ? 'ОТЗЫВЫ' : 'ВІДГУКИ')}
                        <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                        </label>
                        <a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>
                    </div>
                    <div id="text-generation${element.id}" class="drop-down-text is-active-custom" style="display: none;">
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Изменить название секции' : 'Заголовок Відгуків')}</h5>
                        <input  name="reviews_title"  value="${ Object.values(normalizeData.reviews).length  ? 
                        normalizeData.reviews.reviews_title : (locale == 'ru' ? 'ОТЗЫВЫ' : 'ВІДГУКИ')}"></br>
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Отзыв 1' : 'Відгук 1')}</h5>
                        <input  name="reviews_name1"  value="${ Object.values(normalizeData.reviews).length  ? 
                        normalizeData.reviews.name_first : (locale == 'ru' ? 'Имя' : 'Ім`я')}"></br>
                        <textarea name="reviews1">${ Object.values(normalizeData.reviews).length  ? 
                        normalizeData.reviews.reviews_first : 'Ціна не кусається, та й в цілому обстановка налаштовує на лікування!'}</textarea></br>
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Отзыв 2' : 'Відгук 2')}</h5>
                        <input  name="reviews_name2"  value="${ Object.values(normalizeData.reviews).length  ? 
                        normalizeData.reviews.name_seconde : (locale == 'ru' ? 'Имя' : 'Ім`я')}"></br>
                        <textarea name="reviews2">${ Object.values(normalizeData.reviews).length  ? 
                        normalizeData.reviews.reviews_seconde : 'На лікуванні вже півроку, всі зуби стали на місце(як на мене), та лікар доводе до ідеалу. Шкодую, що раніше не звернулася. Велике спасибі!'}</textarea></br>
                    </div>
                `)
                }
                else if (element.id === 9) {
                locale = document.getElementById('locale').value;
                tamplate_footer.insertAdjacentHTML("afterbegin",
                    `<div class="notification warning closeable box1" id="checkbox-area">
                        <input type="text" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}"   style="visibility: hidden;">
                        <span class="slider-my-custom round"  style="visibility: hidden;"></span>
                        <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                        <label class="form-check-label" for="element${element.id}">
                            ${(locale == 'ru' ? 'АДРЕС ' : 'АДРЕСА ')}
                        <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                        </label>
                    </div>
                    <div id="text-generation${element.id}" class="drop-down-text is-active-custom" style="display: none;">
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Изменить название секции' : 'Змінити назву секції')}</h5>
                        <input  name="adress_title"  value="${ Object.values(normalizeData.adress).length  ? 
                        normalizeData.adress.adress_title : (locale == 'ru' ? 'АДРЕС ' : 'АДРЕСА ')}"></br>
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Телефон клиники' : 'Телефон клініки')}</h5>
                        <input name="phone" value="${ Object.values(normalizeData.adress).length  ? 
                        normalizeData.header.header_phone : '(0532) 63-77-59'}" ></br>
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Адрес клиники' : 'Адреса клініки')}</h5>
                        <input  name="adress" value="${ Object.values(normalizeData.adress).length  ? 
                        normalizeData.header.header_address: 'Adress_1'}" ></br>
                        <h5 class="titles-to-items-custom">Email:</h5>
                        <input name="email" type="email" value="${ Object.values(normalizeData.adress).length  ? 
                        normalizeData.adress.adress_email : 'yourMail@mail.com'}" /></br>
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'Текст секции' : 'Текст секції')}
                        <a onclick="refresh_text('adressText')"> <i class="fa fa-refresh" style="font-size:24px"></i></a></h5>
                        <input name="adress_text" id="adressText" value="${ Object.values(normalizeData.adress).length  ? 
                        normalizeData.adress.adress_text : 'Ми допоможемо впоратися з БУДЬ-ЯКИМ стоматологічним недоліком, оскільки вирішуємо питання комплексно і запобігаємо появі нових проблем на ранньому етапі їхнього виникнення.'}'" ></br>
                        
                        <h5 class="titles-to-items-custom">${(locale == 'ru' ? 'График работы' : 'Графік роботи')}:</h5>
                        WorkDays: <input type="text" name="chart1" value="${ Object.values(normalizeData.adress).length  ? 
                        normalizeData.adress.chart1 : ''}" placeholder="Пн.-Пт.: 09:00-19:00"></br>
                        WeekDays: <input type="text" name="chart2" value="${ Object.values(normalizeData.adress).length  ? 
                        normalizeData.adress.chart2 : ''}" placeholder="Сб.: 09:00-14:00, Нд.: вихідний"></br>
                        <h5 class="titles-to-items-custom">Viber</h5>
                        <input name="social[Viber]" value="${normalizeData.adress.social_url.map((social,index) =>
                            social.name ==  'Viber'  ? social.value : ''
                            ).join('')}" placeholder="Напишіть номер телефону у форматі +380000000000"><br>
                        <h5 class="titles-to-items-custom">Telegram</h5>
                        <input name="social[Telegram]" value="${normalizeData.adress.social_url.map((social,index) =>
                            social.name ==  'Telegram'  ? social.value : ''
                            ).join('')}" placeholder="Напишіть номер телефону у форматі +380000000000"><br>
                        <h5 class="titles-to-items-custom">WhatsApp</h5>
                        <input name="social[Whatsapp]" value="${normalizeData.adress.social_url.map((social,index) =>
                            social.name ==  'Whatsapp'  ? social.value : ''
                            ).join('')}"  placeholder="Напишіть номер телефону у форматі +380000000000"><br>
                        <h5 class="titles-to-items-custom">Instagram</h5>
                        <input name="social[Instagram]" value="${normalizeData.adress.social_url.map((social,index) =>
                            social.name ==  'Instagram'  ? social.value : ''
                            ).join('')}"  placeholder="Напишіть ім'я користувача"><br>
                        <h5 class="titles-to-items-custom">FaceBook</h5>
                        <input name="social[Facebook]" value="${normalizeData.adress.social_url.map((social,index) =>
                            social.name ==  'Facebook'  ? social.value : ''
                            ).join('')}"  placeholder="Вставте лінк на сторінку у Facebook"><br>
                    </div>
                `)
                }
                else {
                listContainer.insertAdjacentHTML("afterbegin",
                    `<div class="notification warning closeable box1" id="checkbox-area">
                        <input type="checkbox" name="blocks[]" class="form-check-input" id="element${element.id}" value="${element.url}"
                        checked/>
                        <span class="slider-my-custom round"></span>
                        <a class="showMore" data-id="${element.id}"/><i class="sl sl-icon-settings"> </i></a>
                        <label class="form-check-label" for="element${element.id}">${element.name}
                        <img src="${window.location.origin}/${element.thumbnail}" id="original" class="imgBlocks">
                        </label>
                        <a class="listener"><img src="../images/burger-drag-drop.svg" id="drag-burger" class="drag-burger"></a>
                    </div>
                `)
                }
            })
            document.getElementById('header_logo').value = document.getElementById('header_logo_file').src
            document.getElementById('about').value = document.getElementById('about_file').src
            
            var isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
            if (isIOS) {
                var selectElement = document.querySelectorAll('input[type="file"]');
                selectElement.forEach(function(El) {
                    El.classList.add('fileIPhone')
                })
            } else {
    
                var selectElement = document.querySelectorAll('input[type="file"]');
    
                selectElement.forEach(function(El) {
                    El.classList.remove('fileIPhone')
                })
    
            }
            document.getElementById('about').value = document.getElementById('about_file').src
            var selDiv_portfolio = "";
            init_portfolio();
            function init_portfolio() {
                document.querySelector('#files_portfolio').addEventListener('change', handleFileSelect_portfolio, false);
                selDiv_portfolio = document.querySelector("#carousel");
                if (Object.values(normalizeData.porfolio).length) {
                    selDiv_portfolio.style.visibility = 'visible'
                    porfolio_main_arr = normalizeData.porfolio.porfolio_imgs.split(",") 
                    var i=0;
                    $.each(porfolio_main_arr, function (i, value) {				
                            var html = "<div class=\"slide\"><img src=\"" + value + "\" class=\"imgPortfolioLoad\" ></div>";
                            selDiv_portfolio.innerHTML += html;		
                    })
                }
                
            }
    
            function handleFileSelect_portfolio(e) {
                console.log(document.querySelector('#files_portfolio').value)
    
    
                if(!e.target.files || !window.FileReader) return;
                selDiv_portfolio.style.visibility = 'visible'
                selDiv_portfolio.innerHTML = "";
                var i=0;
                var files = e.target.files;
                var filesArr = Array.prototype.slice.call(files);
                filesArr.forEach(function(f) {
                    var f = files[i];
                    if(!f.type.match("image.*")) {
                        return;
                    }
    
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var html = "<div class=\"slide\"><img src=\"" + e.target.result + "\" class=\"imgPortfolioLoad\" ></div>";
                        selDiv_portfolio.innerHTML += html;
                    }
                    i++;
                    reader.readAsDataURL(f);
                });
            }
            $(function () {
    
                $("#text-generation4 > .ul-custom input").each(function () {
                    $(this).prop("checked", false)
                })
    
                if ( Object.values(normalizeData.benefits).length ) {
                    benefits_main_arr = normalizeData.benefits.benefits_main.split(",")
                    benefits_main_arr = benefits_main_arr.filter((el) => !!el)
                    
                    $.each(benefits_main_arr, function (key, value) {
                        $(".list-group-item input[key="+value+"]").prop("checked", true)
                        //$(".list-group-item input[key="+value+"]").parent().parent().prev().children("input").prop("checked", true)
                    })
                }
                if ( Object.values(normalizeData.service).length ) {
                    service_main_arr = normalizeData.service.service_main.split(",")
                    service_main_arr = service_main_arr.filter((el) => !!el)
                    $.each(service_main_arr, function (key, value) {
                        service_main_value = value.split(':')
                        
                        service_price = document.getElementById("benefit_"+service_main_value[0])
                        if (service_price && service_main_value[1]!='') {
                            
                            service_price.value = service_main_value[1]
                                                    
                        }
    
                        $(".list-group-item input[key="+service_main_value[0]+"]").prop("checked", true)
                        $(".list-group-item input[key="+service_main_value[0]+"]").parent().parent().prev().children("input").prop("checked", true)
                    })
                }			
    
                $(".ul-custom>.list-group-item input").on("change", function () {
                    click = $(this).is(":checked")
                    $(this).parent().next("ul").children().each(function () {
                        if (click) {
                            if (!$(this).children("input").is(":checked")) {
                                $(this).children("input").prop('checked', true);
                            }
                        } else {
                            if ($(this).children("input").is(":checked")) {
                                $(this).children("input").prop('checked', false);
                            }
                        }
                    })
                })
    
                $(".ul-custom ul .list-group-item input").on("change", function () {
                    i = 0
                    $(this).parent().children("input").each(function () {
                        if ($(this).is(":checked")) i++;
                    })
                    if (i > 0) {
                        if (!$(this).parent().parent().prev("li").children("input").is(":checked")) {
                            $(this).parent().parent().prev("li").children("input").prop('checked', true);
                        }
    
                    }
                })
    
                $(".once_ckecked input").each(function(){
                    $(this).prop('checked', false)
                })
                
    
                $(".once_checked input").on("change", function(){
                    $(this).parent("li").parent("ul").find("input").each(function () {
                        $(this).prop('checked', false);
                    })
                    if(!$(this).is(":checked")) $(this).prop("checked", true)
                })
    
                $("#checkbox-area > input").on("change", function () {
                    
                    if($(this).attr("id") !== "element3"){
                        click = $(this).is(":checked")
                        $(this).parent().next("div").children("ul").children("li").each(function () {
                            if (click) {
                                if (!$(this).children("input").prop('checked')) $(this).children("input").prop('checked', true)
                            } else {
                                if ($(this).children("input").prop('checked')) $(this).children("input").prop('checked', false)
                            }
                        })
                        $(this).parent().next("div").children("ul").children("ul").each(function () {
                            if (click) {
                                if (!$(this).children("li").children("input").prop('checked')) $(this).children("li").children("input").prop('checked', true)
                            } else {
    
                                $(this).children("li").each(function () {
                                    if ($(this).children("input").prop('checked')) {
                                        $(this).children("input").prop('checked', false)
                                    }
                                })
    
    
                            }
                        })
                    }
                    
                })
    
                $(".ul-custom>li>input").on("change", function () {
                    if ($(this).is(":checked")) {
                        $(this).parent().parent().parent().prev("#checkbox-area").children("input").prop('checked', true)
                    }
                })
                $(".ul-custom>ul>li>input").on("change", function () {
                    if ($(this).is(":checked")) {
                        $(this).parent().parent().parent().parent().prev("#checkbox-area").children("input").prop('checked', true)
                    }
                })
                clicks = 0;
                $(".showMoreText").click(function () {
                    $this = $(this)
                    if (clicks == 0) {
    
                        id = $this.attr("data-id")
                        $div = $("#text-generation" + id)
    
                        if ($div.hasClass("is-active-custom")) {
                            $div.css('padding', '30px')
                            $div.show("slow")
                            $div.removeClass("is-active-custom")
                        } else {
                            $div.hide("slow")
                            $div.addClass("is-active-custom")
                        }
                        clicks++;
                    }
                    setTimeout(function () {
                        clicks = 0;
                    }, 500)
    
                })
                $(".showMore").click(function () {
                    $this = $(this)
                    if (clicks == 0) {
                    
                        id = $this.attr("data-id")
                        $div = $("#text-generation" + id)
    
                        if ($div.hasClass("is-active-custom")) {
                            $div.css('padding', '30px')
                            $div.show("slow")
                            $div.removeClass("is-active-custom")
                        } else {
                            $div.hide("slow")
                            $div.addClass("is-active-custom")
                        }
                        clicks++;
                    }
                    setTimeout(function () {
                        clicks = 0;
                    }, 500)
                    
                })
    
                init()
    
                function init() {
                    document.querySelectorAll('#files').forEach((btn) => {
                        
                    btn.addEventListener('change', handleFileSelect, false);
                    })
                }
    
                function handleFileSelect(e) {
                    if(!e.target.files || !window.FileReader) return;
                    const {name} = e.target
                    if (name.includes("header")) {
                        selDiv = document.querySelector("#header_logoFiles");
                        idName= 'header_logo'
                    } else if (name.includes("about")) {
                        selDiv = document.querySelector("#aboutFiles");
                        idName= 'about'
                    }			
                    
                    selDiv.innerHTML = "";
                    var i=0;
                    var files = e.target.files;
                    var filesArr = Array.prototype.slice.call(files);
                    filesArr.forEach(function(f) {
                        var f = files[i];
                        if(!f.type.match("image.*")) {
                            return;
                        }
    
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var html = "<img src=\"" + e.target.result + "\"  id=\""+idName+"_file\"  class=\"imgBlocksInside\" >";
                            selDiv.innerHTML += html;
                            document.getElementById(idName).value = e.target.result
                        }
                        i++;
                        
                        reader.readAsDataURL(f);
                    });
                    
                }
    
                $("#elementCats").sortable({
                    handle: ".listener"
                });
    
                //
                // Extend jQuery feature detection
                //
                $.extend($.support, {
                    touch: "ontouchend" in document
                });
    
                //
                // Hook up touch events
                //
                $.fn.addTouch = function () {
                    if ($.support.touch) {
                        this.each(function (i, el) {
                            el.addEventListener("touchstart", iPadTouchHandler, false);
                            el.addEventListener("touchmove", iPadTouchHandler, false);
                            el.addEventListener("touchend", iPadTouchHandler, false);
                            el.addEventListener("touchcancel", iPadTouchHandler, false);
                        });
                    }
    
                    return this;
                }
                $('.listener').addTouch()
            });
            function strip(str) {
                str.replace(/\s+/g, ' ') // заменить длинные пробелы одним
                    .replace(/^\s/, '')    // удалить пробелы в начале строки
                    .replace(/\s$/, '');   // удалить пробелы в конце строки
    
                return str;
            }
        });
    
    })