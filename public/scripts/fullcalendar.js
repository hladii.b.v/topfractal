

$(document).ready(function(){
    var baseurl = window.location.origin+window.location.pathname;
    events_array = null;
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    jQuery.ajax({
        url:  baseurl,
        data: {date: true},
        type: "GET",
        success: function (response) {

            day_slot = JSON.parse(response.replace(/&quot;/g,'"'));
            init_calendar ( day_slot);
        }
    });

    function init_calendar (events_array) {
        startS = null;
        endS = null;

        $('#calendar').fullCalendar({
            editable: true,
            header: {
                left: 'prev,next today',
                center: '',
                right: 'agendaWeek'
            },
            timeFormat: 'H(:mm)',
            height: 650,
            slotDuration: '00:15:00',
            snapDuration: '00:15:00',
            defaultView: 'agendaWeek',

            displayEventTime: true,
            events: events_array ,
            selectable: true,
            selectHelper: true,
            select: function (start, end) {

                $('.modal').modal('show');
                startS = moment(start).format('Y-MM-DD HH:mm:ss');
                endS =  moment(end).format('Y-MM-DD HH:mm:ss');

            },
            eventResize: function(event, delta, revertFunc) {
                var startR = moment( event.start ).format('Y-MM-D HH:mm:ss');
                var endR = moment(event.end).format('Y-MM-D HH:mm:ss');

                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                jQuery.ajax({
                    url:  baseurl+"/update",
                    data:  {title: event.title, start: startR, end: endR, id: event.id},
                    type: "POST",
                    success: function (response) {


                    }
                });
            },
            eventDrop: function (event, delta) {

                var startD = moment( event.start ).format('Y-MM-D HH:mm:ss');
                var endD = moment(event.end).format('Y-MM-D HH:mm:ss');
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                jQuery.ajax({
                    url: baseurl +"/update",
                    data:  {title: event.title, start: startD, end: endD, id: event.id},
                    type: "POST",
                    success: function (response) {


                    }
                });
            },
            eventClick: function (event) {
                var deleteMsg = confirm("Do you really want to delete?");
                if (deleteMsg) {
                    jQuery.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url:  baseurl+"/delete",
                        data: {id: event.id},
                        type: "POST",
                        success: function (response) {
                            if(parseInt(response) > 0) {
                                $('#calendar').fullCalendar('removeEvents', event.id);

                            }
                        }
                    });
                }
            },

        });
    }
    $('#save-event').on('click', function() {
        var title = $('#title').val();

        if (title) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: baseurl +"/create",
                data: {title: title, start: startS, end:endS},
                type: "POST",
                success: function (response) {

                    $('#calendar').fullCalendar('renderEvent',
                        {
                            id: response.id,
                            title: title,
                            start: response.start,
                            end: response.end,
                            color: '#'+response.color
                        },
                        true
                    );
                    $('#calendar').fullCalendar('unselect');
                }
            });

        }
        $('#calendar').fullCalendar('unselect');
        $('.modal').find('select').val('');
        $('.modal').modal('hide');
    })


});
