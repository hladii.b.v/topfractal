$(document).ready(function () {
    var breakpointMobile = window.matchMedia('screen and (max-width: 300)');
    ymaps.ready(init);
    function init() {
        //var json_path = '/assets/files/maps/map-moscow.local.json';
        var map = new ymaps.Map("map", {
                center: [55.758291068983176, 37.645159499999984],
                zoom: 15,
                controls: ['typeSelector']
            }, {
                searchControlProvider: 'yandex#search',
                maxZoom: 23,
                minZoom: 12,
                flying:false
            }); //                
            map.behaviors.disable('scrollZoom');
            if (window.innerWidth < 1250) {
                map.behaviors.disable('drag');
            }
            // BallonLayout
            MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="map-popover top">' +
                    '<div class="arrow"></div>' +
                    '<button class="map-popover__close">' +
                        '<svg class="ico ico-close">' +
                            '<use xlink:href="#ico-close"></use>' +
                        '</svg>' +
                    '</button>' +
                    '<div class="map-popover__inner">' +
                        '$[[options.contentLayout observeSize maxHeight=300]]' +
                    '</div>' +
                '</div>', {
                    /**
                     * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
                     * @function
                     * @name build
                     */
                    build: function () {
                        this.constructor.superclass.build.call(this);
                        this._$element = $('.map-popover', this.getParentElement());
                        this.applyElementOffset();
                        this._$element.find('.map-popover__close')
                            .on('click', $.proxy(this.onCloseClick, this));
                    },
                    /**
                     * Удаляет содержимое макета из DOM.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
                     * @function
                     * @name clear
                     */
                    clear: function () {
                        this._$element.find('.close')
                            .off('click');
                        this.constructor.superclass.clear.call(this);
                    },
                    /**
                     * Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                     * @function
                     * @name onSublayoutSizeChange
                     */
                    onSublayoutSizeChange: function () {
                        MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);
                        if(!this._isElement(this._$element)) {
                            return;
                        }
                        this.applyElementOffset();
                        this.events.fire('shapechange');
                    },
                    /**
                     * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                     * @function
                     * @name applyElementOffset
                     */
                    applyElementOffset: function () {
                        this._$element.css({
                            left: -40,
                            top: -40
                        });
                    },
                    /**
                     * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                     * @function
                     * @name onCloseClick
                     */
                    onCloseClick: function (e) {
                        e.preventDefault();
                        this.events.fire('userclose');
                    },
                    /**
                     * Используется для автопозиционирования (balloonAutoPan).
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ILayout.xml#getClientBounds
                     * @function
                     * @name getClientBounds
                     * @returns {Number[][]} Координаты левого верхнего и правого нижнего углов шаблона относительно точки привязки.
                     */
                    getShape: function () {
                        if(!this._isElement(this._$element)) {
                            return MyBalloonLayout.superclass.getShape.call(this);
                        }
                        var position = this._$element.position();
                        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                            [position.left, position.top], [
                                position.left + this._$element[0].offsetWidth,
                                position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight
                            ]
                        ]));
                    },
                    /**
                     * Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
                     * @function
                     * @private
                     * @name _isElement
                     * @param {jQuery} [element] Элемент.
                     * @returns {Boolean} Флаг наличия.
                     */
                    _isElement: function (element) {
                        return element && element[0] && element.find('.arrow')[0];
                    }
                }
            ),
            // Создание вложенного макета содержимого балуна.
            MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="map-popover__content">$[properties.balloonContent]</div>'
            );

        /*
         * Filter Init from url param
         */
        function filterInit() {
            var filterval = getURLParameter('v1');
            if (filterval) {
                $('button.filter-list__btn.js-map-filter').each(function(i) {
                    if ($(this).data('direction') == filterval) {
                        var element = $(this)[0];
                        setTimeout(function(){ element.click()}, 100);
                        return false;
                    }
                });
            }
        }
        filterInit();
        /*
        *
        * Data Ballon Layout
        * */
        function mapPoint(id) {
            if (window['clinics_data']) {
                var data = window['clinics_data'];
                //filter
                if (id) {
                    var result = [];
                    var myGeoObjectsFilter = data.map(function(item) {
                        for (var i = 0; i < item.clinics.length; i++) {
                            var clinicItem = item.clinics[i];
                            if (clinicItem.divId === id) {
                                if (id == 14 && item.id == 611) {
                                    continue;
                                }
                                result.push(item);
                            }
                        }
                    });
                    data = result;
                }

                var myGeoObjects = data.map(function(item) {
                    var directionList = '';
                    for (var i = 0; i < item.clinics.length; i++) {
                        var clinicItem = item.clinics[i];
                        directionList += '<a href="' + clinicItem.url + '">' + clinicItem.name + '</a>';
                    }
                    return new ymaps.GeoObject({
                        geometry: {
                            type: 'Point',
                            coordinates: [item.geoshirota, item.geodolgota]
                        },
                        properties: {
                            clusterCaption: item.name,
                            balloonContent: [
                                '<div class="map-card">'+
                                    '<div class="map-card__img">' +
                                        '<img src="'+ item.icon +'" alt="">' +
                                    '</div>'+
                                    '<div class="map-card__info">'+
                                        '<div class="map-card__title">'+ item.name +'</div>'+
                                        '<div class="map-card__contacts">'+ item.address +'</div>'+
                                        '<div class="map-card__contacts call_phone_2_disabled">'+ item.phone +'</div>'+
                                        '<div class="map-card__contacts">'+ item.worktime +'</div>'+
                                        '<div class="map-card__directions">'+ directionList + '</div>'+
                                    '</div>'+
                                '</div>'
                            ].join('')
                        }
                    }, {
                        iconLayout: 'default#image',
                        iconImageHref: '/assets/images/v5/map-mark.svg',
                        iconImageSize: [72, 72],
                        iconImageOffset: [-31, -31],
                        hideIconOnBalloonOpen: false,
                        balloonShadow: false,
                        balloonLayout: MyBalloonLayout,
                        balloonContentLayout: MyBalloonContentLayout,
                        balloonPanelMaxMapArea: 0
                    });
                });
                var clusterIcons = [
                        {
                            href: '/assets/images/v5/map-mark-cluster.svg',
                            size: [72, 72],
                            offset: [-31, -31]
                        }
                    ],
                    clusterNumbers = [100],
                    clusterContent = ymaps.templateLayoutFactory.createClass(
                        '<div style="color: #fff;">{{ properties.geoObjects.length }}</div>'
                    );;
                /*
                *
                * Создадим кластеризатор после получения и добавления точек
                * */
                var clusterer = new ymaps.Clusterer({
                    preset: 'islands#yellowIcon',
                    hasBalloon: false,
                    clusterIcons: clusterIcons,
                    clusterNumbers: clusterNumbers,
                    clusterIconContentLayout: clusterContent
                });
                //удаление всех точек
                clusterer.removeAll();
                map.geoObjects.removeAll();
                //добавление отфильтрованных точек
                clusterer.add(myGeoObjects);
                map.geoObjects.add(clusterer);
                //map.setBounds(clusterer.getBounds(), {
                //    checkZoomRange: true
                //});
                /*
                *
                * Click hint on mobile
                * */
                if (breakpointMobile.matches) {
                    map.geoObjects.events.add("click", function (event) {
                        var layoutCompany = $('.js-map-company-layout'),
                            dataHint = event.get('target').properties._data;
                        if (!dataHint.geoObjects) {
                            $('body, html').addClass('overflow');
                            layoutCompany.find('[data-action="content"]').html(dataHint.balloonContent);
                            layoutCompany.addClass('show');
                            map.geoObjects.options.set('hasBalloon', false);
                        }
                    });
                    return false;
                }
            }
        }
        mapPoint();

        function mapPopup(id) {
            if (window['clinics_data']) {
                var data = window['clinics_data'];
                var myGeoObjectCurrent;
                data.map(function(item) {
                    if(item.id === id) {
                        myGeoObjectCurrent = item;
                    }
                });
                var directionListPopup = '';
                for (var i = 0; i < myGeoObjectCurrent.clinics.length; i++) {
                    var clinicPopupItem = myGeoObjectCurrent.clinics[i];
                    directionListPopup += '<a href="' + clinicPopupItem.url + '">' + clinicPopupItem.name + '</a>';
                }
                var myGeoObjectPopup = new ymaps.GeoObject({
                        geometry: {
                            type: 'Point',
                            coordinates: [myGeoObjectCurrent.geoshirota, myGeoObjectCurrent.geodolgota]
                        },
                        properties: {
                            clusterCaption: myGeoObjectCurrent.name,
                            balloonContent: [
                                '<div class="map-card">'+
                                '<div class="map-card__img">' +
                                '<img src="'+ myGeoObjectCurrent.icon +'" alt="">' +
                                '</div>'+
                                '<div class="map-card__info">'+
                                '<div class="map-card__title">'+ myGeoObjectCurrent.name +'</div>'+
                                '<div class="map-card__contacts">'+ myGeoObjectCurrent.address +'</div>'+
                                '<div class="map-card__contacts call_phone_2_disabled">'+ myGeoObjectCurrent.phone +'</div>'+
                                '<div class="map-card__contacts">'+ myGeoObjectCurrent.work +'</div>'+
                                '<div class="map-card__directions">'+ directionListPopup + '</div>'+
                                '</div>'+
                                '</div>'
                            ].join('')
                        }
                    }, {
                        iconLayout: 'default#image',
                        iconImageHref: '/assets/images/v5/map-mark.svg',
                        iconImageSize: [72, 72],
                        iconImageOffset: [-31, -31],
                        hideIconOnBalloonOpen: false,
                        balloonShadow: false,
                        balloonLayout: MyBalloonLayout,
                        balloonContentLayout: MyBalloonContentLayout,
                        balloonPanelMaxMapArea: 0
                    });

                //удаление всех точек
                map.geoObjects.removeAll();
                //добавление текущей точки
                map.geoObjects.add(myGeoObjectPopup);
                map.setCenter([myGeoObjectCurrent.geoshirota, myGeoObjectCurrent.geodolgota], 12);
            }
        }


        /*
        *
        * Закрытие балуна, если клик был на карте, а не на геообъекте
        * */
        map.events.add('balloonopen', function (e) {
            var balloon = e.get('balloon');
            map.events.add('click', function (e) {
                if(e.get('target') === map) {
                    map.balloon.close();
                }
            });
        });

        /*
        *
        * Элементы управления
        * */
        var zoomControl = new ymaps.control.ZoomControl({
            options: {
                size: "large",
                position: {
                    top: 60,
                    right: 20
                }
            }
        });
        map.controls.add(zoomControl);
        map.behaviors.disable('scrollZoom');
        /*
        *
        * Button open popup
        * */
        $('.js-map-popup').magnificPopup({
            type: 'inline',
            fixedContentPos: true,
            fixedBgPos: true,
            overflowY: 'auto',
            showCloseBtn: false,
            closeBtnInside: false,
            preloader: false,
            modal: false,
            midClick: true,
            removalDelay: 100,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            callbacks: {
                open: function () {
                    //$('body').addClass('blur-page overflow');
                    $('body').addClass('blur-page');
                    $('html').addClass('overflow');

                    var idAddress = $(this.ev).attr('data-id');
                    mapPopup(idAddress);
                },
                close: function () {
                    //$('body').removeClass('blur-page overflow');
                    $('body').removeClass('blur-page');
                    $('html').removeClass('overflow');
                }
            }
        });
    }
});
