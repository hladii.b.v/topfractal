// TODO svgfixer.js
// https://gist.github.com/leonderijke/c5cf7c5b2e424c0061d2
'use strict';

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

function escapeHTML(string){
    var pre = document.createElement('pre');
    var text = document.createTextNode(string);
    pre.appendChild(text);
    return pre.innerHTML;
}

function validateEmail(email) {
    var re = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
    return re.test(email);
}

/**
 * Array.max
 * Расширение объекта Array - добавление функции max
 * Получение максимального элемента одномерного массива
 **/
Array.max = function (array) {
    return Math.max.apply(Math, array);
};

/**
 * Array.min
 * Расширение объекта Array - добавление функции min
 * Получение минимального элемента одномерного массива
 **/
Array.min = function (array) {
    return Math.min.apply(Math, array);
};

/**
 * String.capitalize
 * Расширение объекта String - добавление функции capitalize
 * Преобразование первой буквы каждого слова в заглавную.
 **/
String.prototype.capitalizeWord = function() {
     return this.charAt(0).toUpperCase() + this.substr(1).toLowerCase();
};

/**
 * addLoadListener()
 * Добавление последовательно вызывающихся обработчиков сообщений
 * onload.
 */
function addLoadListener(func) {
    if (typeof window.addEventListener != 'undefined') {
        window.addEventListener('load', func, false);
    }
    else if (typeof document.addEventListener != 'undefined') {
        document.addEventListener('load', func, false);
    }
    else if (typeof window.attachEvent != 'undefined') {
        window.attachEvent('onload', func);
    } else {
        var oldfunc = window.onload;
        if (typeof window.onload != 'function') {
            window.onload = func;
        } else {
            window.onload = function() {
                oldfunc();
                func();
            };
        }
    }
}

function loadScript(url, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function() {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function() {
            callback();
        };
    }

    script.src = url;

    var head = document.getElementsByTagName ("head")[0] || document.documentElement;
    head.insertBefore(script, head.firstChild);
}

/**
 * String.capitalize
 * Расширение объекта String - добавление функции capitalize
 * Преобразование первой буквы каждого слова в заглавную.
 **/
String.prototype.capitalizeWord = function() {
     return this.charAt(0).toUpperCase() + this.substr(1).toLowerCase();
};

function isExternal(url) {
    var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);
    if (typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== location.protocol) return true;
    if (typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(":("+{"http:":80,"https:":443}[location.protocol]+")?$"), "") !== location.host) return true;
    return false;
}

function hideCallphone(jq) {
    jq(document).ready(function() {
        // Clickable phonenums
        if (!$pageProps.isMobile) {
            jq('a.callphone').off().unbind().attr('title', '');
            jq('a.callphone').on('click', function() {
                return false;
            });
            jq('a.callphone').removeAttr('href').addClass('noevent');
        }
    });
}

function callphoneMobileAnalytics () {
    window.dataLayer.push({'event':'mobile','eventAction':'buttontel'});
    return true;
}

function bindCallphoneAnalytics(jq) {
    jq(document).ready(function() {
        if (!!$pageProps.isMobile) {
            jq('a.callphone').off('click', callphoneMobileAnalytics).on('click', callphoneMobileAnalytics);
        }
    });
}

function setCookie(name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

var headerAdjustedScroll = function(elm, smooth, hist, evt) {
    //class="header-main header-scroll"
    var designHeader = document.querySelector('.header-main');
    if (designHeader && elm) {
        var designOffset = designHeader.offsetHeight + 20;
    } else {
        return false;
    }
    if (!$(elm).length) {
        return false;
    }
    // Javascript-скроллинг по документу в любом случае не приводит к изменениям hash
    // Нативный скроллинг по документу приводит к изменению hash и перемотке
    // Это означает, что нативный скроллинг нужно отменить в любом случае (preventDefault),
    // но изменение истории нужно оставить настраиваемым
    // Если функция вызывается из обработчика события, то ей нужно передать event
    // По event определяется event.target
    // Если hist===false, то нужно выполнить preventDefault (click)
    if (evt) {
        evt.preventDefault();
    }
    if (smooth) {
        $('html, body').stop().animate({
                scrollTop: $(elm).offset().top - designOffset
            }, 600);
    } else {
        $('html,body').scrollTop($(elm).offset().top - designOffset);
    }
    if (evt && hist===true) {
        // Вызвано обработчиком
        history.pushState({}, '', evt.href);
    }
    if (evt) {
        return false;
    }
    return true;
};

var headerScrollCheck = function(e) {
    var pagehash = window.location.hash;
    var rightNavigation = ((window.performance && window.performance.navigation.type === window.performance.navigation.TYPE_NAVIGATE) ||
                           (typeof window.PerformanceNavigationTiming !== 'undefined' && window.performance.getEntriesByType("navigation")[0].type === 'navigate'));
    if (pagehash !== '' && rightNavigation) { // возможно чуть позже будет скроллинг
        window['medirf_pending_scroll'] = false;
        var target = document.querySelector(pagehash); // содержит #
        if (typeof target !== 'undefined') { // точно будет скроллинг
            window['medirf_pending_scroll'] = pagehash;
            window['medirf_pending_scroll_func'] = function(evt) {
                $(window).off('scroll', medirf_pending_scroll_func);
                if (window['medirf_pending_scroll']) {
                    headerAdjustedScroll(window['medirf_pending_scroll'], false, false, evt);
                    window['medirf_pending_scroll'] = false;
                    return true;
                }
            }
            $(window).on('scroll', medirf_pending_scroll_func);
        }
    } else {
        window['medirf_pending_scroll'] = false;
    }
};

var headerScrollFixed = function() {
    var scrollTopWindow = window.pageYOffset,
        headerTop = document.querySelector('.header-top'),
        headerMain = document.querySelector('.header-main');
    //scroll header
    if (window.innerWidth > windowWidthLaptop) {
        if (scrollTopWindow > headerTop.offsetHeight) {
            headerMain.classList.add('header-scroll');
            document.body.style.paddingTop = headerTop.offsetHeight + headerMain.offsetHeight + 'px';
        } else {
            headerMain.classList.remove('header-scroll');
            document.body.style.paddingTop = '';
        }
    }
};

//closest

(function (ELEMENT) {
    ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
    ELEMENT.closest = ELEMENT.closest || function closest(selector) {
        if (!this) return null;
        if (this.matches(selector)) return this;
        if (!this.parentElement) {
            return null;
        } else return this.parentElement.closest(selector);
    };
})(Element.prototype);

var windowWidthLaptop = 1250,
    windowWidthTablet = 1040,
    windowWidthMobile = 767;

document.addEventListener('DOMContentLoaded', function () {

    if ($('html.ie11 .page-inner-header_img').length) {
        $('html.ie11.has_kdpv').removeClass('has_kdpv').addClass('no_kdpv');
        $('html.ie11 header.page-inner-header.page-inner-header_img').removeClass('page-inner-header_img');
        $('html.ie11 .page-inner-header__img').remove();
    }

    var header = document.querySelector('.header-main');
    headerScrollCheck();
    headerScrollFixed();

    // IAV content transforms (temp)
    // Mid block right column images
    $('.outside').each(function(index) {
        if ($(this).hasClass('inner_after') || $(this).hasClass('inner_before')) {
            if ($(this).hasClass('inner_after')) {
                // take previous block level element (display) and wrap this and previous with <div class="outside_inner_after_container"></div>
                var siblings = $(this).prevAll(); //var siblings = $(this).prevAll().reverse();
                $(this).wrap('<div class="outside_inner_after_container" />');
            } else if ($(this).hasClass('inner_before')) {
                // take next block level element (display) and wrap this and next with <div class="outside_inner_before_container"></div>
                var siblings = $(this).nextAll();//var siblings = $(this).nextAll().reverse();
                $(this).wrap('<div class="outside_inner_before_container" />');
            }
            var sideElement = $(this).parent();
            var sibfilter = [];
            if (siblings.length) {
                siblings.each(function(siblidx){
                    var tagName = $(this).prop("tagName");
                    if (tagName == 'HR') {
                        return false;
                    }
                    var displayMode = $(this).css('display');
                    if (displayMode == 'inline') {
                        sibfilter.push($(this)[0]);
                        return true;
                    }
                    sibfilter.push($(this)[0]);
                    return false;
                });
            }

            if (sibfilter.length) {
                if ($(this).hasClass('inner_after')) {
                    sideElement.prepend(sibfilter.slice().reverse());
                } else if ($(this).hasClass('inner_before')) {
                    sideElement.append(sibfilter);
                }
            }

        }
    });
    // -Mid block right column images

    // Auto collapsing blocks
    $('hr.expand_text_mobile_wrap').filter(":even").addClass('opentag');
    $('hr.expand_text_mobile_wrap').filter(":odd").addClass('closetag');
    $('hr.expand_text_mobile_wrap.opentag').each(function(index){
        $(this).nextUntil('hr.expand_text_mobile_wrap.closetag').wrapAll('<div class="toggle-text-mobile auto_injected" />');
    });
    $('hr.expand_text_mobile_wrap').remove();
    $('div.toggle-text-mobile.auto_injected > h2:first-child,'+
      'div.toggle-text-mobile.auto_injected > h3:first-child,'+
      'div.toggle-text-mobile.auto_injected > h4:first-child,'+
      'div.toggle-text-mobile.auto_injected > h5:first-child').wrap('<div class="toggle-text-mobile__btn" />');
    $('div.toggle-text-mobile.auto_injected > .toggle-text-mobile__btn').each(function() {
        $(this).nextAll().wrapAll('<div class="toggle-text-mobile__content clearfix" />');
    });
    // -Auto collapsing blocks

    // Content slider auto container
    $('hr.content_slider_wrap_outside').filter(":even").addClass('opentag');
    if ($('hr.content_slider_wrap_outside.opentag').length) {
        $('.page-inner-content').css('visibility', 'hidden');
    }
    $('hr.content_slider_wrap_outside').filter(":odd").addClass('closetag');
    $('hr.content_slider_wrap_elements').filter(":even").addClass('opentag');
    $('hr.content_slider_wrap_elements').filter(":odd").addClass('closetag');
    $('hr.content_slider_wrap_outside.opentag').each(function(index){
        $(this).nextUntil('hr.content_slider_wrap_outside.closetag').wrapAll('<div class="content-slider-right auto_injected" data-slider-scroll />');
    });
    $('hr.content_slider_wrap_elements.opentag').each(function(index){
        $(this).nextUntil('hr.content_slider_wrap_elements.closetag').wrapAll('<div class="content-slider-right__slider auto_injected" data-slider-scroll-img />');
    });
    $('hr.content_slider_wrap_outside').remove();
    $('hr.content_slider_wrap_elements').remove();

    $('.content-slider-right__slider.auto_injected > figure, .content-slider-right__slider.auto_injected > picture, .content-slider-right__slider.auto_injected > div, .content-slider-right__slider.auto_injected > span').addClass('slider-right__img').wrap('<div class="slider-right__item"></div>');
    $('.content-slider-right.auto_injected').wrapInner('<div class="content-slider-right__text" data-slider-scroll-fixed></div>');
    $('.content-slider-right__slider.auto_injected').each(function() { // в конец content-slider-right__slider
        $(this).appendTo($(this).closest('div.content-slider-right.auto_injected'));
    });
    $('.content-slider-right.auto_injected > .content-slider-right__text').wrapInner('<div data-slider-scroll-element></div>');
    $('.content-slider-right.auto_injected > .content-slider-right__slider').wrapInner('<div class="slider-right"></div>');

    // Перенос элементов в новый div.content
    jQuery.fn.reverse = [].reverse;
    $('.content-slider-right.auto_injected').each(function(index) {
        var parentElement = $(this).parent('.content');
        var siblings = $(this).prevAll().reverse();
        if (siblings.length) {
            var parentTargetBefore = $('<div class="content"></div>').insertBefore(parentElement);
            siblings.appendTo(parentTargetBefore);
        }
        siblings = $(this).nextAll().reverse();
        if (siblings.length) {
            var parentTargetAfter = $('<div class="content"></div>').insertAfter(parentElement);
            siblings.appendTo(parentTargetAfter);
        }
        $(this).unwrap();
    });

    $('.page-inner-content').css('visibility', 'visible');
    // -Content slider auto container

    // /IAV content transforms (temp)

    // Objectfit polyfill
    objectFitImages(null, {watchMQ: true});

    $('a[href*="#"]:not(.natevt,.scroll-to,.js-tab-btn)').on('click', function(evt) {
       // Ссылка на внешнем сайте - переход
       // Ссылка на этом сайте - headerAdjustedScroll
        var href = $(this).attr('href');
        var url = href.toString().toLowerCase();
        if (typeof window['isExternal'] == 'function' && window.isExternal(url)) {
            return true;
        }
        if (href) {
            href = href.split('#', 2);
            var qs = '';
            if (window.location.search != '?') {
                qs = window.location.search;
            }
            if (href[1] && (!href[0] || href[0] == (window.location.pathname+qs))) {
                href = '#'+href[1];
                return headerAdjustedScroll(href, false, true, evt);
            }
        }
        return true;
    });
    $('body').on('click', '.scroll-to', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        if (href) {
            href = href.split('#', 2);
            if (href[1]) {
                href = '#'+href[1];
                return headerAdjustedScroll(href , true, true, e);
            }
        }
        return true;
    });
    /*
    $('a[href*="#"],a[href=""]').each(function(idx) {
        var href = $(this).attr('href');
        if (href) {
            href = href.split('#', 2);
            if (!href[0].length && href[1] != 'void') {
                var qs = '';
                if (window.location.search != '?') {
                    qs = window.location.search;
                }
                $(this).attr('href', window.location.pathname+qs+'#'+href[1]);
            }
        } else {
            $(this).attr('href', window.location.pathname+'#');
        }
        return true;
    });
    */


    /*--menu mobile--*/
    //open
    $('.js-menu-open').on('click', function (e) {
        e.preventDefault();
        var topPosition = header.offsetHeight;
        $('.nav-main').css('top', topPosition + 'px');
        fixBody();
    });
    //menu close
    $('.open-menu__btn-overlay').on('click', function () {
        unfixBody();
    });
    //resize menu
    function resizeMenu() {
        if (window.innerWidth > windowWidthLaptop) {
            $('.nav-main').css('top', '');
            $('html').removeClass('open-menu');
            document.body.style.top = '';
        } else if (window.innerWidth > windowWidthMobile) {
            $('.nav-main').css('top', '61px');
        }
    }
    function fixBody() {
        var scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
        $('html').toggleClass('open-menu');
        if ($('html').hasClass('open-menu')) {
            document.body.setAttribute('data-body-scroll-fix', scrollPosition);
            document.body.style.top = '-' + scrollPosition + 'px';
        } else {
            document.body.style.top = '0';
            window.scroll(0, scrollPosition);
        }
    }
    function unfixBody() {
        $('html').removeClass('open-menu');
        var scrollPosition = document.body.getAttribute('data-body-scroll-fix');
        document.body.style.top = '0';
        window.scroll(0, scrollPosition);
    }
    /*--end menu mobile--*/

    /*--search header--*/
    var searchFormHeader = $('.search-header-popup');
    //open
    $('.js-open-search-header').on('click', function () {
        searchFormHeader.addClass('open');
        $('.menu-catalog').addClass('search_shown');
        searchFormHeader.find('.form-field__field').focus();
    });
    //close
    $('.js-close-search-header').on('click', function () {
        searchFormHeader.removeClass('open');
        $('.menu-catalog').removeClass('search_shown');
    });
    $('body').on('click', function () {
        if (event) {
        if (!$(event.target).closest('.search-header-popup').length && !$(event.target).closest('.js-open-search-header').length) {
            searchFormHeader.removeClass('open');
            $('.menu-catalog').removeClass('search_shown');
        }
        }
    });
    /*--end search header--*/

    /*--breadcrumbs btn--*/
    //open
    $('.js-breadcrumbs-btn').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('open');
    });
    //close
    $('body').on('click', function () {
        if (event) {
        if (!$(event.target).closest('.breadcrumbs-menu').length && !$(event.target).is('.js-breadcrumbs-btn')) {
            $('.js-breadcrumbs-btn').parent().removeClass('open');
        }
        }
    });
    /*--end breadcrumbs btn--*/

    //city
    var btnCity = document.querySelectorAll('.js-btn-city-mobile');
    if (btnCity) {
        var _loop = function _loop(i) {
            var item = btnCity[i],
                parent = item.parentNode;
            item.addEventListener('click', function (e) {
                e.preventDefault();
                parent.classList.toggle('open');
            });
        };

        for (var i = 0; i < btnCity.length; i++) {
            _loop(i);
        }
    }

    //toggle
    $('.toggle__btn').on('click', function (e) {
        //e.preventDefault(); FIXME
        var parent = $(this).parents('.toggle'),
            content = parent.find('.toggle__content');
        if (content) {
            content.slideToggle(300);
        }
    });
    $('.toggle').each(function () {
        if (!$(this).find('.toggle__content').length) {
            $(this).addClass('toggle_empty');
        }
    });

    //toggle text mobile
    $('.toggle-text-mobile__btn').on('click', function () {
        var parent = $(this).parents('.toggle-text-mobile'),
            content = parent.find('.toggle-text-mobile__content');
        if (window.innerWidth < windowWidthMobile) {
            //content.slideToggle(200);
            parent.toggleClass('open').toggleClass('closed');
        }
    });

    //tab
    /*
    $('.js-tab-btn').on('click', function (e) {
        e.preventDefault();
        var itemHref = $(this).attr('href'),
            itemHrefDom = $(this).get(0),
            itemParentNav = $(this).closest('[data-tab-nav]'),
            dataTabNav = itemParentNav.attr('data-tab-nav'),
            siblingsLink = itemParentNav.find('.js-tab-btn');

        itemHref = itemHrefDom.hash;

        siblingsLink.removeClass('active');

        var tabWrap = $('[data-tab-wrap="' + dataTabNav + '"]'),
            siblingsContent = tabWrap.find('[data-tab-content]');
        siblingsContent.removeClass('active');

        //add .active current
        $(this).addClass('active');
        $(itemHref).addClass('active');

        //text more
        textMore(windowWidthLaptop, windowWidthMobile);

        //for slider
        if (document.querySelector(itemHref + ' .slick-slider')) {
            console.log(itemHref);
            $(itemHref + ' .slick-slider').slick('refresh');
        }
    });
    */


    //management modal
    var managementModalOpen = document.querySelectorAll('.js-management-open');
    if (managementModalOpen) {
        var _loop2 = function _loop2(i) {
            var item = managementModalOpen[i];
            item.addEventListener('click', function (e) {
                e.preventDefault();
                var hash = item.getAttribute('href');
                document.querySelector(hash).classList.add('open');
                document.body.style.overflow = 'hidden';
            });
        };

        for (var i = 0; i < managementModalOpen.length; i++) {
            _loop2(i);
        }
    }
    var managementModalClose = document.querySelectorAll('.js-management-close');
    if (managementModalClose) {
        var _loop3 = function _loop3(i) {
            var item = managementModalClose[i];
            item.addEventListener('click', function (e) {
                e.preventDefault();
                var parent = item.closest('.management-modal');
                parent.classList.remove('open');
                document.body.style.overflow = '';
            });
        };

        for (var i = 0; i < managementModalClose.length; i++) {
            _loop3(i);
        }
    }

    //form field
    var fieldForm = document.querySelectorAll('.form-field__field');
    if (fieldForm) {
        var _loop4 = function _loop4(i) {
            var item = fieldForm[i],
                predictive = item.closest('[data-predictive]'),
                predictiveList = void 0;

            if (predictive) {
                predictiveList = predictive.querySelector('.search-predictive');
            }

            item.addEventListener('input', function () {
                if (item.value !== '') {
                    //for placeholder
                    item.closest('.form-field').classList.add('field-not-empty');

                    //for predictive (search)
                    if (predictive) {
                        predictiveList.classList.add('open');
                    }
                } else {
                    //for placeholder
                    item.closest('.form-field').classList.remove('field-not-empty');

                    //for predictive (search)
                    if (predictive) {
                        predictiveList.classList.remove('open');
                    }
                }
            });

            item.addEventListener('change', function () {
                if (predictive) {
                    predictiveList.classList.remove('open');
                }
            });
        };

        for (var i = 0; i < fieldForm.length; i++) {
            _loop4(i);
        }
    }

    //predictive (search)
    var predictive = document.querySelectorAll('.js-search-predictive');
    if (predictive) {
        var _loop5 = function _loop5(i) {
            var item = predictive[i],
                val = item.querySelector('[data-predictive-val]'),
                parent = item.closest('[data-predictive]'),
                id = parent.getAttribute('data-predictive');
            item.addEventListener('click', function (e) {
                document.getElementById(id).value = val.textContent;
            });
        };

        for (var i = 0; i < predictive.length; i++) {
            _loop5(i);
        }
    }

    //play video youtube
    var playVideo = document.querySelectorAll('.js-play-youtube');
    if (playVideo) {
        var _loop6 = function _loop6(i) {
            var item = playVideo[i],
                iframe = item.querySelector('iframe');
            item.addEventListener('click', function () {
                var src = iframe.getAttribute('src');
                iframe.setAttribute('src', src + '?autoplay=1');
                item.classList.add('video_playing');
            });
        };

        for (var i = 0; i < playVideo.length; i++) {
            _loop6(i);
        }
    }

    //video index
    var videoIndex = document.querySelector('.js-video-index');
    if (videoIndex) {
        var video = videoIndex.querySelector('video');
        if (window.innerWidth < windowWidthLaptop) {
            videoIndex.addEventListener('click', function () {
                videoIndex.classList.add('video_playing');
                video.play();
                video.setAttribute('controls', true);
            });
        }
        window.addEventListener('scroll', function () {
            //scroll to video (index)
            if (window.innerWidth > windowWidthLaptop) {

                var videoIndexTop = videoIndex.getBoundingClientRect().top,
                    heightDocument = document.documentElement.clientHeight + 50;

                if (videoIndexTop < heightDocument && !videoIndex.classList.contains('video_playing')) {
                    videoIndex.classList.add('video_playing');
                    var promise = video.play();
                    if (promise !== undefined) {
                        promise.then(function (_) {
                            video.play();
                        }).catch(function (error) {
                            video.play();
                        });
                    }
                }
            }
        });
    }

    //open catalog (mobile)
    var catalogBtnOpen = document.querySelectorAll('.js-catalog-btn');
    var catalogBtnBack = document.querySelectorAll('.js-catalog-btn-back');
    if (catalogBtnOpen) {
        var _loop7 = function _loop7(i) {
            var item = catalogBtnOpen[i];
            item.addEventListener('click', function (e) {
                if (window.innerWidth < windowWidthLaptop) {
                    e.preventDefault();
                    var subMenu = item.nextElementSibling;
                    subMenu.classList.add('open');
                    document.querySelector('.nav-main').scrollTop = 0;
                    document.querySelector('.nav-main').classList.add('nav-main_open-sub');

                    if (item.closest('.menu-catalog__sub.open')) {
                        item.closest('.menu-catalog__sub.open').scrollTop = 0;
                        item.closest('.menu-catalog__sub.open').style.overflow = 'hidden';
                    }
                }
            });
        };

        for (var i = 0; i < catalogBtnOpen.length; i++) {
            _loop7(i);
        }
    }
    if (catalogBtnBack) {
        var _loop8 = function _loop8(i) {
            var item = catalogBtnBack[i];
            item.addEventListener('click', function (e) {
                if (window.innerWidth < windowWidthLaptop) {
                    e.preventDefault();
                    var subMenu = item.closest('[data-menu-sub]');
                    subMenu.classList.remove('open');
                    document.querySelector('.nav-main').classList.remove('nav-main_open-sub');

                    if (item.closest('.menu-catalog__sub.open')) {
                        item.closest('.menu-catalog__sub.open').style.overflow = '';
                    }
                }
            });
        };

        for (var i = 0; i < catalogBtnBack.length; i++) {
            _loop8(i);
        }
    }

    //cookies
    var cookiesClose = document.querySelector('.js-cookies-close');
    if (cookiesClose) {
        cookiesClose.addEventListener('click', function (e) {
            e.preventDefault();
            cookiesClose.closest('.cookies').classList.add('hidden');
        });
    }

    //calc eye
    var calcCorrectionSubmit = document.querySelectorAll('.js-calc-correction');
    if (calcCorrectionSubmit) {
        var _loop9 = function _loop9(i) {
            var item = calcCorrectionSubmit[i],
                result = document.querySelector('.calc-correction-result');
            item.addEventListener('click', function (e) {
                e.preventDefault();
                result.classList.add('open');
            });
        };

        for (var i = 0; i < calcCorrectionSubmit.length; i++) {
            _loop9(i);
        }
    }

    //switch doctor
    var switchDoctor = document.querySelectorAll('.js-switch-doctor');
    if (switchDoctor) {
        var _loop10 = function _loop10(i) {
            var item = switchDoctor[i],
                data = item.getAttribute('data-switch');
            item.addEventListener('click', function (e) {
                e.preventDefault();
                document.querySelector('.js-switch-doctor.active').classList.remove('active');
                item.classList.add('active');
                var doctors = document.querySelectorAll('.doctors-item');
                for (var j = 0; j < doctors.length; j++) {
                    if (data == 'list') {
                        doctors[j].classList.remove('doctors-item_tile');
                        doctors[j].classList.add('doctors-item_line');
                    } else {
                        doctors[j].classList.remove('doctors-item_line');
                        doctors[j].classList.add('doctors-item_tile');
                    }
                }
            });
        };

        for (var i = 0; i < switchDoctor.length; i++) {
            _loop10(i);
        }
    }

    //doctor clinic (more)
    if (window.innerWidth < windowWidthMobile) {
        var clinics = document.querySelectorAll('.doctor-clinic__item');
        if (clinics.length > 2) {
            for (var i = 0; i < clinics.length; i++) {
                if (i > 2) {
                    clinics[i].classList.add('hidden-mobile');
                }
            }
            var clinicsBtn = document.querySelector('.js-doctor-clinic');
            clinicsBtn.classList.remove('hidden');
            clinicsBtn.addEventListener('click', function () {
                for (var _i = 0; _i < clinics.length; _i++) {
                    clinics[_i].classList.remove('hidden-mobile');
                }
                clinicsBtn.classList.add('hidden');
            });
        }
    }

    //table
    var table = document.querySelectorAll('.table');
    if (table) {
        for (var _i2 = 0; _i2 < table.length; _i2++) {
            var _item = table[_i2],
                th = _item.querySelectorAll('th'),
                tr = _item.querySelectorAll('tr');
            for (var j = 0; j < tr.length; j++) {
                var td = tr[j].querySelectorAll('td');
                for (var k = 0; k < td.length; k++) {
                    td[k].setAttribute('data-name', th[k].textContent);
                }
            }
        }
    }

    //book
    var bookContents = document.querySelectorAll('.js-book-contents');
    if (bookContents) {
        for (var _i3 = 0; _i3 < bookContents.length; _i3++) {
            var _item2 = bookContents[_i3];
            _item2.addEventListener('click', function (e) {
                e.preventDefault();
                document.documentElement.classList.add('page-slide');
            });
        }
    }
    var bookContentsClose = document.querySelectorAll('.js-book-contents-close');
    if (bookContentsClose) {
        for (var _i4 = 0; _i4 < bookContentsClose.length; _i4++) {
            var _item3 = bookContentsClose[_i4];
            _item3.addEventListener('click', function (e) {
                e.preventDefault();
                document.documentElement.classList.remove('page-slide');
            });
        }
    }

    //range slider
    /*
    $('.range-slider-eye').ionRangeSlider({
        min: -10,
        max: 10,
        from: 0,
        grid: false,
        hide_min_max: true,
        hide_from_to: true,
        step: 0.25,
        onChange: function onChange(data) {
            var parent = data.input.parents('.range-bar'),
                input = parent.find('.range-bar__input input'),
                val = data.from,
                resultMess = parent.find('.range-bar__mess');
            if (data.from > 0) {
                val = '+' + val;
                resultMess.text('Дальнозоркость');
            } else if (data.from < 0) {
                resultMess.text('Близорукость');
            } else {
                resultMess.text('Отклонений нет');
            }
            input.val(val);
        }
    });
    $('.range-bar__input input').on('input', function () {
        var val = $(this).val(),
            slider = $(this).parents('.range-bar').find('.range-slider-eye').data('ionRangeSlider');
        if (Number(val) > 10) val = '10';
        if (Number(val) < -10) val = '-10';
        val = val.replace(/[^-.,0-9]/g, '');
        val = val.replace(/,/g, '.');
        slider.update({
            from: val
        });
    });
    */

    //bar rating
    /*$('.rating-readonly').barrating({
        theme: 'css-stars',
        readonly: true
    });*/

    //select custom
    /*$('.select-sort select').customSelect();*/
    var select = $('.select select');
    select.customSelect({
        placeholder: '<span></span>',
        includeValue: true,
        keyboard: true
    });

    $('body').on('click', '.custom-select__dropdown button', function () {
        var select = $(this).parents('.select'),
            value = select.find('select')[0].value;
        if (value != '') {
            select.addClass('select_selected');
        }
    });

    //price select
    $('body').on('click', '.js-select-price .custom-select__dropdown button', function () {
        var value = $(this).parents('.select').find('select')[0].value;
        if (value != '') {
            $('.clinic-contacts-price').addClass('open');
        }
    });

    //info hover
    $('.info-hover').on('click', function () {
        if (window.innerWidth < windowWidthLaptop) {
            $(this).toggleClass('open');
        }
    });

    function fixBodyPopup() {
        var scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
        $('html').css('overflow', '');
        $('html').addClass('open-popup');
        $('body').addClass('blur-page');
        document.body.setAttribute('data-body-scroll-fix', scrollPosition);
        document.body.style.top = '-' + scrollPosition + 'px';
    }
    function unfixBodyPopup() {
        $('html').removeClass('open-popup');
        $('body').removeClass('blur-page');
        var scrollPosition = document.body.getAttribute('data-body-scroll-fix');
        document.body.style.top = '';
        window.scroll(0, scrollPosition);
    }

    //popup (gallery)
    $('[data-gallery-popup]').each(function () {
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Загрузка изображения...',
            mainClass: 'mfp-images',
            gallery: {
                enabled: true,
                tCounter: '',
                arrowMarkup: '<button class="btn-arrow mfp-arrow mfp-arrow-%dir%">\n                                    %title%\n                                    <svg class="ico ico-arrow">\n                                        <use xlink:href="#ico-arrow"></use>\n                                    </svg>\n                                </button>',
                tPrev: 'Предыдущий слайд',
                tNext: 'Следующий слайд'
            },
            image: {
                titleSrc: function titleSrc(item) {
                    return item.el.attr('title');
                }
            },
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">\n                                <svg class="ico ico-close">\n                                    <use xlink:href="#ico-close"></use>\n                               </svg>\n                           </button>',
            callbacks: {
                open: function open() {
                    fixBodyPopup();
                },
                close: function close() {
                    unfixBodyPopup();
                },
                imageLoadComplete: function loadComplete() {
                    var img = this.content.find('img');
                    // avoid fractional image sizes
                    var imgHeight = parseInt(img.css('height'));
                    var imgWidth = parseFloat(img.css('width'));
                    if (imgHeight && Math.ceil(imgWidth) != Math.floor(imgWidth)) {
                        img.css({'width': Math.floor(imgWidth)+'px','height': imgHeight+'px'}); // img.css('width', Math.floor(imgWidth));
                    } else {
                        img.css({'width': 'auto', 'height': 'auto'}); //img.css('max-width', '100%');
                    }
                },
                resize: debounce(function () {
                    var img = this.content.find('img');
                    // avoid fractional image sizes
                    var imgHeight = parseInt(img.css('height'));
                    var imgWidth = parseFloat(img.css('width'));
                    if (imgHeight && Math.ceil(imgWidth) != Math.floor(imgWidth)) {
                        img.css({'width': Math.floor(imgWidth)+'px','height': imgHeight+'px'}); // img.css('width', Math.floor(imgWidth));
                    } else {
                        img.css({'max-width': '100%', 'width': 'auto', 'height': 'auto'}); //img.css('max-width', '100%');
                        //img.css('max-width', 'unset'); //img.css('max-width', '100%');
                    }
                }, 250)
            }
        });
    });

    //popup (single img)
    $('[data-photo-popup]').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-images',
        image: {
            verticalFit: true
        },
        closeMarkup: '<button title="%title%" type="button" class="mfp-close">\n                                <svg class="ico ico-close">\n                                    <use xlink:href="#ico-close"></use>\n                               </svg>\n                           </button>',
        callbacks: {
            open: function open() {
                fixBodyPopup();
            },
            close: function close() {
                unfixBodyPopup();
            }
        }
    });

    //popup (form)
    $('.js-form-popup').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        showCloseBtn: false,
        closeBtnInside: false,
        preloader: false,
        modal: false,
        midClick: true,
        removalDelay: 100,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        callbacks: {
            open: function open() {
                fixBodyPopup();
            },
            close: function close() {
                unfixBodyPopup();
            }
        }
    });

/*
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        showCloseBtn: false,
        closeBtnInside: false,
        preloader: false,
        modal: false,
        midClick: true,
        removalDelay: 100,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        callbacks: {
            open: function open() {
                fixBodyPopup();
            },
            close: function close() {
                unfixBodyPopup();
            }
        }

*/
    $('#get_callback_form, .callback_form').magnificPopup({
        type: 'ajax',
        cursor: 'mfp-ajax-cur',
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        showCloseBtn: false,
        closeBtnInside: false,
        preloader: false,
        modal: false,
        midClick: true,
        removalDelay: 100,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        callbacks: {
            open: function open() {
                fixBodyPopup();
            },
            ajaxContentAdded: function contentLoaded() {
                $('#callback .popup__close').on('click', function (e) {
                    $.magnificPopup.close();
                });

                //form field
                var fieldForm = document.querySelectorAll('.form-field__field');
                if (fieldForm) {
                    var _loop4 = function _loop4(i) {
                        var item = fieldForm[i],
                            predictive = item.closest('[data-predictive]'),
                            predictiveList = void 0;

                        if (predictive) {
                            predictiveList = predictive.querySelector('.search-predictive');
                        }

                        item.addEventListener('input', function () {
                            if (item.value !== '') {
                                //for placeholder
                                item.closest('.form-field').classList.add('field-not-empty');

                                //for predictive (search)
                                if (predictive) {
                                    predictiveList.classList.add('open');
                                }
                            } else {
                                //for placeholder
                                item.closest('.form-field').classList.remove('field-not-empty');

                                //for predictive (search)
                                if (predictive) {
                                    predictiveList.classList.remove('open');
                                }
                            }
                        });

                        item.addEventListener('change', function () {
                            if (predictive) {
                                predictiveList.classList.remove('open');
                            }
                        });
                    };

                    for (var i = 0; i < fieldForm.length; i++) {
                        _loop4(i);
                    }
                }

            },
            close: function close() {
                unfixBodyPopup();
            }
        }

    });

    $('.js-popup').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        showCloseBtn: false,
        closeBtnInside: false,
        preloader: false,
        modal: false,
        midClick: true,
        removalDelay: 100,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        callbacks: {
            open: function open() {
                fixBodyPopup();
            },
            close: function close() {
                unfixBodyPopup();
            }
        }
    });
    $('.js-popup-close').on('click', function (e) {
        $.magnificPopup.close();
    });

    //datepicker
    $.datetimepicker.setLocale('ru');
    $('.datetimepicker').datetimepicker({
        i18n: {
            de: {
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентабрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                dayOfWeek: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]
            }
        },
        dayOfWeekStart: 1,
        timepicker: false,
        format: 'd.m.Y',
        lang: 'ru'
    });
    $('.datetimepicker').on('change', function () {
        //for placeholder
        if ($(this).val() !== '') {
            $(this).parents('.form-field').addClass('field-not-empty');
        } else {
            $(this).parents('.form-field').addClass('field-not-empty');
        }
    });

    Inputmask({
        'mask': '+7 (999) 999-99-99',
        'clearIncomplete': true,
        'autoUnmask': true,
        'showMaskOnHover': false
    }).mask(document.querySelectorAll("input[type=tel]"));

    //validate
    jQuery.extend(jQuery.validator.messages, {
        required: "Поле обязательно для заполнения",
        email: "Введите корректный e-mail",
        number: "Допустимо только число",
        digits: "Допустимы только цифры",
        remote: "Please fix this field.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Максимальное количество символов {0}."),
        minlength: jQuery.validator.format("Минимальное количество символов {0}."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}."),
        pwcheck: "Пароль некорректный"
    });
    $(".js-validate-callback").validate({
        rules: {
            name: {
                required: true
            },
            phone: {
                required: true
            },
            agree: {
                required: true
            }
        }
    });
    $(".js-validate-feedback").validate({
        rules: {
            'feedback-phone': {
                required: true
            },
            'feedback-email': {
                required: true
            },
            'feedback-agree': {
                required: true
            }
        }
    });
    $(".js-validate-coupon").validate({
        rules: {
            'coupon-surname': {
                required: true
            },
            'coupon-name': {
                required: true
            },
            'coupon-date-birth': {
                required: true
            },
            'coupon-email': {
                required: true
            }
        }
    });

    $(".js-validate-search").validate({
        rules: {
            'search': {
                required: true
            }
        }
    });
    $(".js-validate-search-site").validate({
        rules: {
            'site': {
                required: true
            }
        }
    });
    $(".js-validate-search-mobile").validate({
        rules: {
            'search-mobile': {
                required: true
            }
        }
    });
    $(".js-validate-search-doctors").validate({
        rules: {
            'doctors': {
                required: true
            }
        }
    });
    $(".js-validate-search-feedback").validate({
        rules: {
            'search-feedback': {
                required: true
            }
        }
    });

    //sliders
    var btnPrev = '<button class="btn-arrow btn-arrow_prev">\n                        \u041F\u0440\u0435\u0434\u044B\u0434\u0443\u0449\u0438\u0439 \u0441\u043B\u0430\u0439\u0434\n                        <svg class="ico ico-arrow">\n                            <use xlink:href="#ico-arrow"></use>\n                        </svg>\n                    </button>',
        btnNext = '<button class="btn-arrow btn-arrow_next">\n                        \u0421\u043B\u0435\u0434\u0443\u044E\u0449\u0438\u0439 \u0441\u043B\u0430\u0439\u0434\n                        <svg class="ico ico-arrow">\n                            <use xlink:href="#ico-arrow"></use>\n                        </svg>\n                    </button>';

    $('.slider-simple').each(function (i, slider) {
        var $slider = $(slider),
            container = $slider.find('.swiper-container'),
            arrowPrev = $slider.find('.swiper-arrow-prev'),
            arrowNext = $slider.find('.swiper-arrow-next');

        var objSlider = new Swiper(container, {
            slidesPerView: 'auto',
            spaceBetween: 16,
            navigation: {
                nextEl: arrowNext,
                prevEl: arrowPrev
            },
            freeMode: true,
            watchOverflow: true,
            speed: 500,
            loop: true,
            centeredSlides: true,
            roundLengths: true

        });
    });

    $('.stock-slider-one').each(function (i, slider) {
        var $slider = $(slider),
            container = $slider.find('.swiper-container'),
            arrowPrev = $slider.find('.swiper-arrow-prev'),
            arrowNext = $slider.find('.swiper-arrow-next');

        var objSlider = new Swiper(container, {
            slidesPerView: 1,
            spaceBetween: 0,
            navigation: {
                nextEl: arrowNext,
                prevEl: arrowPrev
            },
            freeMode: true,
            watchOverflow: true,
            roundLengths: true,
            speed: 500,
            breakpoints: {
                1250: {
                    slidesPerView: 'auto',
                    spaceBetween: 16
                }
            }

        });
    });

    $('html.spbru .stock-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: true,
        swipeToSlide: true,
        responsive: [{
            breakpoint: windowWidthLaptop,
            settings: {
                slidesToShow: 1,
                variableWidth: true
            }
        }]
    });
    $('html.mskru .stock-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: true,
        swipeToSlide: true,
        responsive: [{
            breakpoint: windowWidthMobile,
            settings: {
                slidesToShow: 2,
                variableWidth: true
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 1,
                variableWidth: true
            }
        }]
    });


    $('.js-doctors-slider-banner-mobile').each(function (i, slider) {
        var $slider = $(slider),
            container = $slider.find('.swiper-container');
        var sliderDoctors = new Swiper(container, {
            slidesPerView: 1,
            spaceBetween: 20,
            loop: false
        });
    });

    function sliderDoctorAdaptive() {
        var sliderDoctors = void 0,
            sliderThumbs = void 0;
        var $slider = $('.js-doctors-slider-banner'),
            container = $slider.find('.swiper-container'),
            arrowPrev = $slider.find('.swiper-arrow-prev'),
            arrowNext = $slider.find('.swiper-arrow-next'),
            thumbs = $('.js-doctors-slider-banner-img').find('.swiper-container');

        function initSlider() {
            // if (window.innerWidth < windowWidthMobile && container.hasClass('swiper-container-initialized')) {
            //     sliderDoctors.destroy();
            //     sliderThumbs.destroy();
            // } else if (window.innerWidth > windowWidthMobile && !container.hasClass('swiper-container-initialized')) {
                sliderThumbs = new Swiper(thumbs, {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    loop: true,
                    loopedSlides: 1,
                    allowTouchMove: false
                });
                sliderDoctors = new Swiper(container, {
                    slidesPerView: 1,
                    spaceBetween: 20,
                    loop: true,
                    loopedSlides: 1,
                    navigation: {
                        nextEl: arrowNext,
                        prevEl: arrowPrev
                    },
                    thumbs: {
                        swiper: sliderThumbs
                    }
                });
            //}
        }

        initSlider();
        $(window).on('resize', function () {
            initSlider();
        });
        $(window).on('orientationchange', function () {
            initSlider();
        });
    }
    sliderDoctorAdaptive();

    function galleryLargeAdaptive() {
        var galleryLarge = void 0,
            galleryLargeThumbs = void 0;
        var $slider = $('.gallery-large'),
            container = $slider.find('.swiper-container'),
            arrowPrev = $slider.find('.swiper-arrow-prev'),
            arrowNext = $slider.find('.swiper-arrow-next'),
            thumbs = $('.gallery-large-thumbs').find('.swiper-container');
        
        function initSlider() {
            if (window.innerWidth < windowWidthTablet && !container.hasClass('swiper-container-initialized')) {
                if (container.hasClass('swiper-container-initialized')) {
                    galleryLarge.destroy();
                }
                galleryLarge = new Swiper(container, {
                    spaceBetween: 16,
                    slidesPerView: 'auto',
                    freeMode: true,
                    loop: true,
                    watchOverflow: true
                });
                if (thumbs.hasClass('swiper-container-initialized')) {
                    galleryLargeThumbs.destroy();
                }
            } else if (window.innerWidth > windowWidthTablet) {
                if (container.hasClass('swiper-container-initialized')) {
                   // galleryLarge.destroy();
                }
                if (thumbs.hasClass('swiper-container-initialized')) {
                    //galleryLargeThumbs.destroy();
                }
                galleryLargeThumbs = new Swiper(thumbs, {
                    spaceBetween: 18,
                    slidesPerView: 7,
                    freeMode: true,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                    breakpoints: {
                        1040: {
                            centeredSlides: true
                        }
                    }
                });
                galleryLarge = new Swiper(container, {
                    spaceBetween: 16,
                    slidesPerView: 1,
                    loop: true,
                    navigation: {
                        nextEl: arrowNext,
                        prevEl: arrowPrev
                    },
                    thumbs: {
                        swiper: galleryLargeThumbs
                    },
                    watchOverflow: true,
                    breakpoints: {
                        1040: {
                            slidesPerView: 'auto',
                            freeMode: true
                        }
                    }
                });
            }
        }
        initSlider();
        $(window).on('resize pageshow', function () {
            initSlider();
        });
        $(window).on('orientationchange', function () {
            initSlider();
        });
    }
    galleryLargeAdaptive();

    $('.advantages-ico-slider').each(function (i, slider) {
        var $slider = $(slider);
        var objSlider = new Swiper($slider, {
            slidesPerView: 4,
            spaceBetween: 24,
            freeMode: true,
            watchOverflow: true,
            speed: 500,
            breakpoints: {
                1250: {
                    slidesPerView: 'auto',
                    freeMode: true
                },
                768: {
                    slidesPerView: 'auto',
                    freeMode: true,
                    spaceBetween: 16
                }
            }
        });
    });

    var secondsMainSlider = 3;
    if (window.innerWidth < windowWidthMobile) {
        secondsMainSlider = 6;
    }
    var millisecondMainSlider = secondsMainSlider * 1000;

    $('.main-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var dotList = slick.$dots[0],
            dots = dotList.childNodes,
            newSlide = nextSlide == dots.length ? 0 : nextSlide,
            itemInnerWidth = $('.slick-dots__btn').innerWidth(),
            itemWidthMargin = ($(dots[newSlide]).outerWidth(true) - $(dots[newSlide]).innerWidth()) / 2;

        //clear prev slide
        $(dots[currentSlide]).attr('style', '');
        $(dots[currentSlide]).find('.slick-dots__load').attr('style', '');

        //current slide

        var current = currentSlide,
            next = newSlide;
        if (currentSlide === dots.length - 1) {
            current = 0;
        }
        if (newSlide === 0) {
            next = dots.length - 1;
        }
        if (current < next) {
            $(dots).each(function () {
                if ($(this).index() >= current && $(this).index() < next) {
                    $(this).css('position', 'absolute');
                    $(this).css('opacity', '0');
                }
            });
        } else {
            $(dots).each(function () {
                //console.log($(this).index() <= current, $(this).index() < next, $(this).index() , next, current);
                if ($(this).index() >= current || $(this).index() < next) {
                    //console.log($(this).index());
                    $(this).css('position', 'absolute');
                    $(this).css('opacity', '0');
                }
            });
        }

        $(dotList).css('padding-right', itemInnerWidth + itemWidthMargin + 'px');

        //load slide
        $(dots[newSlide]).find('.slick-dots__load').css({
            'transition': secondsMainSlider - 0.5 + 's linear 1s',
            'width': 'calc(100% + ' + (itemInnerWidth + itemWidthMargin) + 'px)'
        });

        function changeSliderEnd() {
            $(dots).attr('style', '');
            $(dotList).attr('style', '');
        }

        setTimeout(changeSliderEnd, 1400);
    });
    $('.main-slider').slick({
        slidesToShow: 1,
        infinite: true,
        dots: true,
        appendDots: '.main-slider-wrap__dots',
        appendArrows: '.main-slider-wrap__btn',
        prevArrow: btnPrev,
        nextArrow: btnNext,
        fade: true,
        autoplay: true,
        autoplaySpeed: millisecondMainSlider,
        swipeToSlide: true,
        customPaging: function customPaging(slick, index) {
            return '<button class="slick-dots__btn" type="button">' + index + '</button>\n                    <span class="slick-dots__load"></span>';
        }
    });
    $('.main-slider-wrap__dots .slick-active .slick-dots__load').css({
        'transition': secondsMainSlider - 0.5 + 's linear 1s',
        'width': 'calc(100% + ' + 10 + 'px)'
    });

    $('.result-work-slider').slick({
        slidesToShow: 2,
        dots: false,
        infinite: true,
        appendArrows: '.result-work-wrap__btn',
        prevArrow: btnPrev,
        nextArrow: btnNext,
        swipeToSlide: true,
        responsive: [{
            breakpoint: windowWidthLaptop,
            settings: {
                variableWidth: true,
                arrows: false
            }
        }]
    });

    $('.doctor-slider').each(function (i, slider) {
        var $slider = $(slider),
            container = $slider.find('.swiper-container'),
            arrowPrev = $slider.find('.swiper-arrow-prev'),
            arrowNext = $slider.find('.swiper-arrow-next');

            //            slidesPerColumn: 2,
        var objSlider = new Swiper(container, {
            slidesPerView: 4,
            spaceBetween: 10,
            navigation: {
                nextEl: arrowNext,
                prevEl: arrowPrev
            },
            loop: false,
            freeMode: true,
            watchOverflow: true,
            roundLengths: true,
            speed: 500,
            breakpoints: {
                1250: {
                    slidesPerView: 'auto'
                }
            }

        });
    });

    //slider right
    function sliderRightContent() {
        if (window.innerWidth < windowWidthMobile) {
            if (!$('.slider-right').hasClass('slick-slider')) {
                $('.slider-right').slick({
                    dots: false,
                    infinite: true,
                    arrows: false,
                    variableWidth: true,
                    swipeToSlide: true
                });
            }
        } else {
            if ($('.slider-right').hasClass('slick-slider')) {
                $('.slider-right').slick('unslick');
            }
            /*let heightText = $('.content-slider-right__text').outerHeight();
            $('.content-slider-right').css('height', heightText + 'px'); */
        }
    }

    sliderRightContent();

    //links page
    function linksPageSliderAdaptive() {
        if (window.innerWidth < windowWidthMobile) {
            if (!$('.links-page').hasClass('slick-slider')) {
                $('.links-page').slick({
                    variableWidth: true,
                    dots: false,
                    infinite: false,
                    arrows: false
                });
            }
        } else {
            if ($('.links-page').hasClass('slick-slider')) {
                $('.links-page').slick('unslick');
            }
        }
    }

    linksPageSliderAdaptive();

    navPageScroll();
    scrollFixBlock();
    textMore(windowWidthLaptop, windowWidthMobile);

    window.addEventListener('resize', debounce(function () {
        resizeMenu();
        linksPageSliderAdaptive();
        sliderRightContent();
        if (window.innerWidth < windowWidthLaptop) {
            document.body.style.paddingTop = '';
        }
    }, 100));

    window.addEventListener('scroll', debounce(headerScrollFixed, 20));

    //filter without pagination
    /*$('.js-filter').on('click', function(e) {
        e.preventDefault();
        let filterBtnData = $(this).attr('data-filter');
        if($(this).hasClass('active')) {
            $('.js-filter.active').removeClass('active');
            $('[data-filter-item]').removeClass('hidden');
        } else {
            $('.js-filter.active').removeClass('active');
            $(this).addClass('active');
             $('[data-filter-item]').addClass('hidden');
            $('[data-filter-item="' + filterBtnData + '"]').removeClass('hidden');
        }
    });*/

    /*--filter with pagination--*/
    /*
    var pageShow = 12;
    var optionsFilter = {
        valueNames: [{ data: ['filter'] }],
        page: pageShow,
        pagination: {
            innerWindow: 2,
            left: 0,
            right: 0,
            paginationClass: "pagination__list"
        }
    };
    var userList = new List('filter', optionsFilter);
    function updateList(val) {
        userList.filter(function (item) {
            var genderFilter = false;
            if (val === "all") {
                genderFilter = true;
            } else {
                genderFilter = item.values().filter == val;
            }
            return genderFilter;
        });
        userList.update();
    }
    $('.js-filter').on('click', function (e) {
        e.preventDefault();
        var filterBtnData = $(this).attr('data-filter');
        if ($(this).hasClass('active')) {
            filterBtnData = 'all';
            $('.js-filter.active').removeClass('active');
            $('.filter-select-clinics').addClass('hidden');
        } else {
            $('.js-filter.active').removeClass('active');
            $(this).addClass('active');
            $('.filter-select-clinics').removeClass('hidden');
        }
        updateList(filterBtnData);
    });
    var paginationBack = $('.js-pagination-back'),
        paginationNext = $('.js-pagination-next');
    if (pageShow < $('.list > *').length) {
        $('.pagination-filter').hide();
    }
    if ($('.pagination-filter li:first-child').hasClass('active')) {
        paginationBack.hide();
    }
    paginationBack.on('click', function (e) {
        e.preventDefault();
        var list = $('.pagination-filter li');
        $.each(list, function (position, element) {
            if ($(element).is('.active')) {
                $(list[position - 1]).trigger('click');
            }
        });
    });
    paginationNext.on('click', function (e) {
        e.preventDefault();
        var list = $('.pagination-filter li');
        $.each(list, function (position, element) {
            if ($(element).is('.active')) {
                $(list[position + 1]).trigger('click');
            }
        });
    });
    userList.on('updated', function (list) {
        if (list.matchingItems.length > 0) {
            $('.no-result').hide();
        } else {
            $('.no-result').show();
            $('.pagination-filter').hide();
        }
        if (list.page >= list.matchingItems.length) {
            $('.pagination-filter').hide();
        } else {
            $('.pagination-filter').show();
        }
        function paginationChange() {
            //first page
            var index = $('.pagination-filter li.active').index();
            paginationBack.show();
            if (index == 0) {
                paginationBack.hide();
            }
            //last page
            if (index >= $('.pagination-filter li').length - 1) {
                paginationNext.hide();
            } else {
                paginationNext.show();
            }

            $('.pagination-filter li').on('click', function (e) {

                e.preventDefault();

                var spaced = header.offsetHeight + 20;
                $('html, body').stop().animate({
                    scrollTop: $('#filter').offset().top - spaced
                }, 600);
            });
        }
        setTimeout(paginationChange, 10);
    });
    $('.pagination-filter li').on('click', function (e) {

        e.preventDefault();

        var spaced = header.offsetHeight + 20;
        $('html, body').stop().animate({
            scrollTop: $('#filter').offset().top - spaced
        }, 600);
    });
    */
    /*--end filter with pagination--*/

    scrollSlider();

    if (!!$pageProps.isMobile) {
        $('a.callphone').on('click', function() {
            if (window.dataLayer) {
                window.dataLayer.push({'event':'mobile','eventAction':'buttontel', 'eventURL':window.location.href});
            }
            if (_gaq) {
                _gaq.push(['_trackEvent', 'Callphone', 'Clicked', window.location.href+$(this).attr('href')]);
            }
            if (window.medianalytics) {
                medianalytics('medi.send', 'event', 'Callphone', 'Clicked', window.location.href+$(this).attr('href'));
            }
            if (typeof window.ym == 'function') {
                window.ym(53003266, 'reachGoal', 'call_button');
            }
        });
    } else {
        $('a.callphone').removeAttr('href').unbind().addClass('noevent');
    }

    // Code run in every page, in case the previous page left an event to be tracked:
    var medirfTE = document.cookie.match('(?:;\\s*|^)ev=([^!]*)!([^!]*)!([^!]+)!([^!]+)(?:;|\s*$)');
    if (medirfTE && medirfTE.length > 2) {
        if (typeof _gaq != 'undefined') {
            _gaq.push(['_trackEvent', unescape(ev[1]), unescape(ev[2]),
                    unescape(ev[3]), parseInt(ev[4])]);
        }
        document.cookie='ev=; path=/; expires='+new Date(new Date().getTime()-1000).toUTCString();
    }
    var medirfTE = document.cookie.match('(?:;\\s*|^)eva=([^!]*)!([^!]*)!([^!]+)!([^!]+)(?:;|\s*$)');
    if (medirfTE && medirfTE.length > 2) {
        if (typeof window.medianalytics == 'function') {
            window.medianalytics('medi.send', 'event', unescape(ev[1]), unescape(ev[2]), unescape(ev[3]), parseInt(ev[4]), {'nonInteraction': 1});
        }
        document.cookie='eva=; path=/; expires='+new Date(new Date().getTime()-1000).toUTCString();
    }
    var medirfTE = document.cookie.match('(?:;\\s*|^)evy=([^!]*)!([^!]*)!([^!]+)!([^!]+)(?:;|\s*$)');
    if (medirfTE && medirfTE.length > 2) {
        if (typeof window.ym == 'function') {
            window.ym(53003266, unescape(ev[1]), unescape(ev[2]));
        }
        document.cookie='evy=; path=/; expires='+new Date(new Date().getTime()-1000).toUTCString();
    }

});

// Function to set the event to be tracked:
function goog_trackDelayedEventGAQ(category, action, label, value) {
    document.cookie='ev='+escape(category)+'!'+escape(action)+'!'+escape(label)+'!'+value+'; path=/; expires='+new Date(new Date().getTime()+60000).toUTCString();
}
function goog_trackDelayedEventAJS(category, action, label, value) {
    document.cookie='eva='+escape(category)+'!'+escape(action)+'!'+escape(label)+'!'+value+'; path=/; expires='+new Date(new Date().getTime()+60000).toUTCString();
}
function yndx_trackDelayedEvent(category, name) {
    document.cookie='evy='+escape(category)+'!'+escape(name)+'; path=/; expires='+new Date(new Date().getTime()+60000).toUTCString();
}

window.addEventListener('load', function () {
    if ($('html').hasClass('animate_header')) {
        animateLine();
        animateLoad();
    }
});

function scrollFixBlock() {
    var fixBlock = document.querySelectorAll('[data-fixed-block]');
    if (fixBlock) {
        var scrollFix = function scrollFix() {
            for (var i = 0; i < fixBlock.length; i++) {
                var _item4 = fixBlock[i],
                    spaced = 30,
                    itemWidth = _item4.offsetWidth,
                    headerHeight = document.querySelector('.header-main').offsetHeight,
                    fixElement = _item4.querySelector('[data-fixed-element]'),
                    scrollWindow = window.pageYOffset,
                    offsetTopBlock = _item4.getBoundingClientRect().top + pageYOffset,
                    topOffset = offsetTopBlock + _item4.closest('[data-fixed-section]').offsetHeight,
                    maxOffset = parseInt(scrollWindow + fixElement.offsetHeight + headerHeight + spaced),
                    attrSection = _item4.closest('[data-fixed-section]').getAttribute('data-fixed-section');

                if (window.innerWidth > windowWidthMobile || attrSection == 'mobile') {
                    var positionBlock = offsetTopBlock - headerHeight - spaced;
                    if (positionBlock <= scrollWindow) {
                        fixElement.style.position = 'fixed';
                        fixElement.style.top = headerHeight + spaced + 'px';
                        fixElement.style.width = itemWidth + 'px';
                        if (topOffset <= maxOffset) {
                            fixElement.style.position = 'absolute';
                            fixElement.style.top = 'auto';
                            fixElement.style.bottom = '0';
                        } else {
                            fixElement.style.position = 'fixed';
                            fixElement.style.top = headerHeight + spaced + 'px';
                            fixElement.style.bottom = '';
                        }
                    } else {
                        fixElement.style.position = '';
                        fixElement.style.top = '';
                        fixElement.style.width = '';
                    }
                } else {
                    fixElement.style.position = '';
                    fixElement.style.top = '';
                    fixElement.style.width = '';
                }
            }
        };

        scrollFix();
        window.addEventListener("resize", function () {
            scrollFix();
        });
        window.addEventListener("scroll", function () {
            scrollFix();
        });
    }
}

function navPageScroll() {
    var navScrollLink = document.querySelectorAll('.js-nav-scroll-link'),
        headerHeight = document.querySelector('.header-main').offsetHeight;
    if (navScrollLink) {
        var navScroll = function navScroll() {
            for (var i = 0; i < navScrollLink.length; i++) {
                var navScrollItem = navScrollLink[i],
                    navScrollItemContent = document.querySelectorAll('.nav-scroll-section')[i],
                    navScrollItemContentTop = navScrollItemContent.getBoundingClientRect().top;
                if (navScrollItemContentTop < headerHeight) {
                    document.querySelector('.js-nav-scroll-link.active').classList.remove('active');
                    navScrollItem.classList.add("active");
                }
            }
        };

        var navClick = function navClick() {
            var _loop11 = function _loop11(i) {
                var navScrollItem = navScrollLink[i];
                navScrollItem.addEventListener('click', function (e) {
                    e.preventDefault();
                    var spaced = headerHeight - 20;
                    if (window.innerWidth < windowWidthMobile) {
                        spaced = headerHeight - 10;
                    }
                    $('html, body').stop().animate({
                        scrollTop: $('.nav-scroll-section').eq(i).offset().top - spaced
                    }, 300);
                });
            };

            for (var i = 0; i < navScrollLink.length; i++) {
                _loop11(i);
            }
        };

        navScroll();
        navClick();
        window.addEventListener('resize', function () {
            navScroll();
        });
        window.addEventListener('scroll', function () {
            navScroll();
        });
    }
}

function animateLoad() {
    if (window.innerWidth > windowWidthMobile) {
        var elementsAnimate = document.querySelectorAll('[data-animate-load]');
        for (var i = 0; i < elementsAnimate.length; i++) {
            var itemAnimate = elementsAnimate[i];
            itemAnimate.classList.add('is-animate');
        }
    }
}

function animateLine() {
    if (window.innerWidth > windowWidthMobile) {
        var _animateLine = document.querySelectorAll('.animate-line');
        for (var i = 0; i < _animateLine.length; i++) {
            var itemAnimateLine = _animateLine[i],
                html = itemAnimateLine.innerHTML,
                newHtml = '<div>' + html + '</div>';
            itemAnimateLine.innerHTML = newHtml;
            itemAnimateLine.classList.add('is-animate-line');
        }
        var animateLineWrap = document.querySelectorAll('.animate-line-wrap');
        for (var j = 0; j < animateLineWrap.length; j++) {
            var itemAnimateWrap = animateLineWrap[j],
                animaleLineInner = itemAnimateWrap.querySelectorAll('.animate-line');
            for (var k = 0; k < animaleLineInner.length; k++) {
                var itemAnimaleLineInner = animaleLineInner[k].firstChild;
                itemAnimaleLineInner.style.animationDelay = (k + 1) / 4 + 0.5 + 's';
                //itemAnimaleLineInner.style.animationDelay = '0s';
            }
        }
    }
}

function textMore(laptop, mobile) {
    var textMore = document.querySelectorAll('.text-more');

    var _loop12 = function _loop12(i) {
        var item = textMore[i],
            parent = item.closest('.text-more'),
            textBlock = parent.querySelector('.text-more__text'),
            btn = parent.querySelector('.text-more__btn'),
            heightTextCut = void 0;
        function textMoreLoad() {
            var lineHeight = $('.text-more__text').css('line-height'),
                numberLines = item.getAttribute('data-text-more-line'),
                media = item.getAttribute('data-text-more-media'),
                windowWidth = 0;
            if (numberLines) {
                numberLines = numberLines;
            } else {
                numberLines = 3;
            }
            heightTextCut = Number(numberLines) * parseInt(lineHeight);
            switch (media) {
                case 'laptop':
                    windowWidth = laptop;
                    break;
                case 'mobile':
                    windowWidth = mobile;
                    break;
                default:
                    windowWidth = 0;
            }
            if (windowWidth > window.innerWidth) {
                if (heightTextCut < textBlock.scrollHeight) {
                    btn.style.display = 'block';
                    textBlock.style.height = heightTextCut + 'px';
                } else {
                    btn.style.display = '';
                    textBlock.style.height = '';
                }
            } else if (!windowWidth) {
                if (heightTextCut < textBlock.scrollHeight) {
                    btn.style.display = 'block';
                    textBlock.style.height = heightTextCut + 'px';
                }
            } else {
                btn.style.display = 'none';
                textBlock.style.height = '';
            }
        }

        textMoreLoad();
        window.addEventListener('resize', function () {
            textMoreLoad();
            if ($('.text-more').hasClass('open')) {
                $('.text-more__btn').click(); // закрыть
            }
        });

        //text more event
        btn.onclick = function (e) {
            e.preventDefault();
            if (parent.classList.contains('open')) {
                parent.classList.remove('open');
                textBlock.style.height = heightTextCut + 'px';
            } else {
                parent.classList.add('open');
                textBlock.style.height = '';
            }
        };
    };

    for (var i = 0; i < textMore.length; i++) {
        _loop12(i);
    }
}

function scrollSlider() {
    var scrollBlock = document.querySelectorAll('[data-slider-scroll-fixed]');
    if (scrollBlock) {
        var scrollFix = function scrollFix() {
            for (var i = 0; i < scrollBlock.length; i++) {
                var _item5 = scrollBlock[i],
                    itemWidth = _item5.offsetWidth,
                    heightnWindow = document.documentElement.clientHeight,
                    fixElement = _item5.querySelector('[data-slider-scroll-element]'),
                    scrollWindow = window.pageYOffset + heightnWindow,
                    positionBottomBlock = _item5.getBoundingClientRect().bottom + pageYOffset,
                    bottomOffset = window.pageYOffset + _item5.closest('[data-slider-scroll]').getBoundingClientRect().bottom;

                if (window.innerWidth > windowWidthMobile) {
                    if (positionBottomBlock < scrollWindow) {
                        fixElement.style.position = 'fixed';
                        fixElement.style.bottom = 0;
                        fixElement.style.width = itemWidth + 'px';
                        if (bottomOffset <= scrollWindow) {
                            fixElement.style.position = 'absolute';
                            fixElement.style.bottom = 0;
                        } else {
                            if (fixElement.getBoundingClientRect().top < _item5.getBoundingClientRect().top) {
                                fixElement.style.position = '';
                                fixElement.style.bottom = '';
                            } else {
                                fixElement.style.position = 'fixed';
                                fixElement.style.bottom = 0;
                            }
                        }
                    } else {
                        fixElement.style.position = '';
                        fixElement.style.bottom = '';
                        fixElement.style.width = '';
                    }
                } else {
                    fixElement.style.position = '';
                    fixElement.style.bottom = '';
                    fixElement.style.width = '';
                }
            }
        };

        scrollFix();
        window.addEventListener("resize", function () {
            scrollFix();
        });
        window.addEventListener("scroll", function () {
            scrollFix();
        });
    }
}
