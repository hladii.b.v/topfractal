/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
window.matchMedia = window.matchMedia || (function (e, f) {
  var c, a = e.documentElement,
    b = a.firstElementChild || a.firstChild,
    d = e.createElement("body"),
    g = e.createElement("div");
  g.id = "mq-test-1";
  g.style.cssText = "position:absolute;top:-100em";
  d.appendChild(g);
  return function (h) {
    g.innerHTML = '&shy;<style media="' + h + '"> #mq-test-1 { width: 42px; }</style>';
    a.insertBefore(d, b);
    c = g.offsetWidth == 42;
    a.removeChild(d);
    return {
      matches: c,
      media: h
    }
  }
})(document);
/*!
 * dependencyLibs/inputmask.dependencyLib.js
 * https://github.com/RobinHerbots/Inputmask
 * Copyright (c) 2010 - 2017 Robin Herbots
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 4.0.1-37
 */

! function (factory) {
  "function" == typeof define && define.amd ? define(["../global/window", "../global/document"], factory) : "object" == typeof exports ? module.exports = factory(require("../global/window"), require("../global/document")) : window.dependencyLib = factory(window, document);
}(function (window, document) {
  function indexOf(list, elem) {
    for (var i = 0, len = list.length; i < len; i++)
      if (list[i] === elem) return i;
    return -1;
  }

  function type(obj) {
    return null == obj ? obj + "" : "object" == typeof obj || "function" == typeof obj ? class2type[class2type.toString.call(obj)] || "object" : typeof obj;
  }

  function isWindow(obj) {
    return null != obj && obj === obj.window;
  }

  function isArraylike(obj) {
    var length = "length" in obj && obj.length,
      ltype = type(obj);
    return "function" !== ltype && !isWindow(obj) && (!(1 !== obj.nodeType || !length) || ("array" === ltype || 0 === length || "number" == typeof length && length > 0 && length - 1 in obj));
  }

  function isValidElement(elem) {
    return elem instanceof Element;
  }

  function DependencyLib(elem) {
    return elem instanceof DependencyLib ? elem : this instanceof DependencyLib ? void(void 0 !== elem && null !== elem && elem !== window && (this[0] = elem.nodeName ? elem : void 0 !== elem[0] && elem[0].nodeName ? elem[0] : document.querySelector(elem),
      void 0 !== this[0] && null !== this[0] && (this[0].eventRegistry = this[0].eventRegistry || {}))) : new DependencyLib(elem);
  }
  for (var class2type = {}, classTypes = "Boolean Number String Function Array Date RegExp Object Error".split(" "), nameNdx = 0; nameNdx < classTypes.length; nameNdx++) class2type["[object " + classTypes[nameNdx] + "]"] = classTypes[nameNdx].toLowerCase();
  return DependencyLib.prototype = {
    on: function (events, handler) {
      if (isValidElement(this[0]))
        for (var eventRegistry = this[0].eventRegistry, elem = this[0], _events = events.split(" "), endx = 0; endx < _events.length; endx++) {
          var nsEvent = _events[endx].split("."),
            ev = nsEvent[0],
            namespace = nsEvent[1] || "global";
          ! function (ev, namespace) {
            elem.addEventListener ? elem.addEventListener(ev, handler, !1) : elem.attachEvent && elem.attachEvent("on" + ev, handler),
              eventRegistry[ev] = eventRegistry[ev] || {}, eventRegistry[ev][namespace] = eventRegistry[ev][namespace] || [],
              eventRegistry[ev][namespace].push(handler);
          }(ev, namespace);
        }
      return this;
    },
    off: function (events, handler) {
      if (isValidElement(this[0]))
        for (var eventRegistry = this[0].eventRegistry, elem = this[0], _events = events.split(" "), endx = 0; endx < _events.length; endx++)
          for (var nsEvent = _events[endx].split("."), offEvents = function (ev, namespace) {
              var hndx, hndL, evts = [];
              if (ev.length > 0)
                if (void 0 === handler)
                  for (hndx = 0, hndL = eventRegistry[ev][namespace].length; hndx < hndL; hndx++) evts.push({
                    ev: ev,
                    namespace: namespace && namespace.length > 0 ? namespace : "global",
                    handler: eventRegistry[ev][namespace][hndx]
                  });
                else evts.push({
                  ev: ev,
                  namespace: namespace && namespace.length > 0 ? namespace : "global",
                  handler: handler
                });
              else if (namespace.length > 0)
                for (var evNdx in eventRegistry)
                  for (var nmsp in eventRegistry[evNdx])
                    if (nmsp === namespace)
                      if (void 0 === handler)
                        for (hndx = 0,
                          hndL = eventRegistry[evNdx][nmsp].length; hndx < hndL; hndx++) evts.push({
                          ev: evNdx,
                          namespace: nmsp,
                          handler: eventRegistry[evNdx][nmsp][hndx]
                        });
                      else evts.push({
                        ev: evNdx,
                        namespace: nmsp,
                        handler: handler
                      });
              return evts;
            }(nsEvent[0], nsEvent[1]), i = 0, offEventsL = offEvents.length; i < offEventsL; i++) ! function (ev, namespace, handler) {
            if (ev in eventRegistry == 1)
              if (elem.removeEventListener ? elem.removeEventListener(ev, handler, !1) : elem.detachEvent && elem.detachEvent("on" + ev, handler),
                "global" === namespace)
                for (var nmsp in eventRegistry[ev]) eventRegistry[ev][nmsp].splice(eventRegistry[ev][nmsp].indexOf(handler), 1);
              else eventRegistry[ev][namespace].splice(eventRegistry[ev][namespace].indexOf(handler), 1);
          }(offEvents[i].ev, offEvents[i].namespace, offEvents[i].handler);
      return this;
    },
    trigger: function (events) {
      if (isValidElement(this[0]))
        for (var eventRegistry = this[0].eventRegistry, elem = this[0], _events = "string" == typeof events ? events.split(" ") : [events.type], endx = 0; endx < _events.length; endx++) {
          var nsEvent = _events[endx].split("."),
            ev = nsEvent[0],
            namespace = nsEvent[1] || "global";
          if (void 0 !== document && "global" === namespace) {
            var evnt, i, params = {
              bubbles: !0,
              cancelable: !0,
              detail: Array.prototype.slice.call(arguments, 1)
            };
            if (document.createEvent) {
              try {
                evnt = new CustomEvent(ev, params);
              } catch (e) {
                evnt = document.createEvent("CustomEvent"), evnt.initCustomEvent(ev, params.bubbles, params.cancelable, params.detail);
              }
              events.type && DependencyLib.extend(evnt, events), elem.dispatchEvent(evnt);
            } else evnt = document.createEventObject(), evnt.eventType = ev, events.type && DependencyLib.extend(evnt, events),
              elem.fireEvent("on" + evnt.eventType, evnt);
          } else if (void 0 !== eventRegistry[ev])
            if (arguments[0] = arguments[0].type ? arguments[0] : DependencyLib.Event(arguments[0]),
              "global" === namespace)
              for (var nmsp in eventRegistry[ev])
                for (i = 0; i < eventRegistry[ev][nmsp].length; i++) eventRegistry[ev][nmsp][i].apply(elem, arguments);
            else
              for (i = 0; i < eventRegistry[ev][namespace].length; i++) eventRegistry[ev][namespace][i].apply(elem, arguments);
        }
      return this;
    }
  }, DependencyLib.isFunction = function (obj) {
    return "function" === type(obj);
  }, DependencyLib.noop = function () {}, DependencyLib.isArray = Array.isArray, DependencyLib.inArray = function (elem, arr, i) {
    return null == arr ? -1 : indexOf(arr, elem);
  }, DependencyLib.valHooks = void 0, DependencyLib.isPlainObject = function (obj) {
    return "object" === type(obj) && !obj.nodeType && !isWindow(obj) && !(obj.constructor && !class2type.hasOwnProperty.call(obj.constructor.prototype, "isPrototypeOf"));
  }, DependencyLib.extend = function () {
    var options, name, src, copy, copyIsArray, clone, target = arguments[0] || {},
      i = 1,
      length = arguments.length,
      deep = !1;
    for ("boolean" == typeof target && (deep = target, target = arguments[i] || {},
        i++), "object" == typeof target || DependencyLib.isFunction(target) || (target = {}),
      i === length && (target = this, i--); i < length; i++)
      if (null != (options = arguments[i]))
        for (name in options) src = target[name],
          copy = options[name], target !== copy && (deep && copy && (DependencyLib.isPlainObject(copy) || (copyIsArray = DependencyLib.isArray(copy))) ? (copyIsArray ? (copyIsArray = !1,
              clone = src && DependencyLib.isArray(src) ? src : []) : clone = src && DependencyLib.isPlainObject(src) ? src : {},
            target[name] = DependencyLib.extend(deep, clone, copy)) : void 0 !== copy && (target[name] = copy));
    return target;
  }, DependencyLib.each = function (obj, callback) {
    var i = 0;
    if (isArraylike(obj))
      for (var length = obj.length; i < length && !1 !== callback.call(obj[i], i, obj[i]); i++);
    else
      for (i in obj)
        if (!1 === callback.call(obj[i], i, obj[i])) break;
    return obj;
  }, DependencyLib.map = function (elems, callback) {
    var value, i = 0,
      length = elems.length,
      isArray = isArraylike(elems),
      ret = [];
    if (isArray)
      for (; i < length; i++) null != (value = callback(elems[i], i)) && ret.push(value);
    else
      for (i in elems) null != (value = callback(elems[i], i)) && ret.push(value);
    return [].concat(ret);
  }, DependencyLib.data = function (owner, key, value) {
    if (void 0 === value) return owner.__data ? owner.__data[key] : null;
    owner.__data = owner.__data || {}, owner.__data[key] = value;
  }, "function" == typeof window.CustomEvent ? DependencyLib.Event = window.CustomEvent : (DependencyLib.Event = function (event, params) {
    params = params || {
      bubbles: !1,
      cancelable: !1,
      detail: void 0
    };
    var evt = document.createEvent("CustomEvent");
    return evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail),
      evt;
  }, DependencyLib.Event.prototype = window.Event.prototype), DependencyLib;
});

/*!
 * inputmask.js
 * https://github.com/RobinHerbots/Inputmask
 * Copyright (c) 2010 - 2017 Robin Herbots
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 4.0.1-37
 */

! function (factory) {
  "function" == typeof define && define.amd ? define(["./dependencyLibs/inputmask.dependencyLib", "./global/window", "./global/document"], factory) : "object" == typeof exports ? module.exports = factory(require("./dependencyLibs/inputmask.dependencyLib"), require("./global/window"), require("./global/document")) : window.Inputmask = factory(window.dependencyLib || jQuery, window, document);
}(function ($, window, document, undefined) {
  function Inputmask(alias, options, internal) {
    if (!(this instanceof Inputmask)) return new Inputmask(alias, options, internal);
    this.el = undefined, this.events = {}, this.maskset = undefined, this.refreshValue = !1,
      !0 !== internal && ($.isPlainObject(alias) ? options = alias : (options = options || {},
          options.alias = alias), this.opts = $.extend(!0, {}, this.defaults, options), this.noMasksCache = options && options.definitions !== undefined,
        this.userOptions = options || {}, this.isRTL = this.opts.numericInput, resolveAlias(this.opts.alias, options, this.opts));
  }

  function resolveAlias(aliasStr, options, opts) {
    var aliasDefinition = Inputmask.prototype.aliases[aliasStr];
    return aliasDefinition ? (aliasDefinition.alias && resolveAlias(aliasDefinition.alias, undefined, opts),
      $.extend(!0, opts, aliasDefinition), $.extend(!0, opts, options), !0) : (null === opts.mask && (opts.mask = aliasStr),
      !1);
  }

  function generateMaskSet(opts, nocache) {
    function generateMask(mask, metadata, opts) {
      var regexMask = !1;
      if (null !== mask && "" !== mask || (regexMask = null !== opts.regex, regexMask ? (mask = opts.regex,
          mask = mask.replace(/^(\^)(.*)(\$)$/, "$2")) : (regexMask = !0, mask = ".*")), 1 === mask.length && !1 === opts.greedy && 0 !== opts.repeat && (opts.placeholder = ""),
        opts.repeat > 0 || "*" === opts.repeat || "+" === opts.repeat) {
        var repeatStart = "*" === opts.repeat ? 0 : "+" === opts.repeat ? 1 : opts.repeat;
        mask = opts.groupmarker.start + mask + opts.groupmarker.end + opts.quantifiermarker.start + repeatStart + "," + opts.repeat + opts.quantifiermarker.end;
      }
      var masksetDefinition, maskdefKey = regexMask ? "regex_" + opts.regex : opts.numericInput ? mask.split("").reverse().join("") : mask;
      return Inputmask.prototype.masksCache[maskdefKey] === undefined || !0 === nocache ? (masksetDefinition = {
          mask: mask,
          maskToken: Inputmask.prototype.analyseMask(mask, regexMask, opts),
          validPositions: {},
          _buffer: undefined,
          buffer: undefined,
          tests: {},
          metadata: metadata,
          maskLength: undefined
        }, !0 !== nocache && (Inputmask.prototype.masksCache[maskdefKey] = masksetDefinition,
          masksetDefinition = $.extend(!0, {}, Inputmask.prototype.masksCache[maskdefKey]))) : masksetDefinition = $.extend(!0, {}, Inputmask.prototype.masksCache[maskdefKey]),
        masksetDefinition;
    }
    if ($.isFunction(opts.mask) && (opts.mask = opts.mask(opts)), $.isArray(opts.mask)) {
      if (opts.mask.length > 1) {
        opts.keepStatic = null === opts.keepStatic || opts.keepStatic;
        var altMask = opts.groupmarker.start;
        return $.each(opts.numericInput ? opts.mask.reverse() : opts.mask, function (ndx, msk) {
          altMask.length > 1 && (altMask += opts.groupmarker.end + opts.alternatormarker + opts.groupmarker.start),
            msk.mask === undefined || $.isFunction(msk.mask) ? altMask += msk : altMask += msk.mask;
        }), altMask += opts.groupmarker.end, generateMask(altMask, opts.mask, opts);
      }
      opts.mask = opts.mask.pop();
    }
    return opts.mask && opts.mask.mask !== undefined && !$.isFunction(opts.mask.mask) ? generateMask(opts.mask.mask, opts.mask, opts) : generateMask(opts.mask, opts.mask, opts);
  }

  function maskScope(actionObj, maskset, opts) {
    function getMaskTemplate(baseOnInput, minimalPos, includeMode) {
      minimalPos = minimalPos || 0;
      var ndxIntlzr, test, testPos, maskTemplate = [],
        pos = 0,
        lvp = getLastValidPosition();
      do {
        !0 === baseOnInput && getMaskSet().validPositions[pos] ? (testPos = getMaskSet().validPositions[pos],
            test = testPos.match, ndxIntlzr = testPos.locator.slice(), maskTemplate.push(!0 === includeMode ? testPos.input : !1 === includeMode ? test.nativeDef : getPlaceholder(pos, test))) : (testPos = getTestTemplate(pos, ndxIntlzr, pos - 1),
            test = testPos.match, ndxIntlzr = testPos.locator.slice(), (!1 === opts.jitMasking || pos < lvp || "number" == typeof opts.jitMasking && isFinite(opts.jitMasking) && opts.jitMasking > pos) && maskTemplate.push(!1 === includeMode ? test.nativeDef : getPlaceholder(pos, test))),
          pos++;
      } while ((maxLength === undefined || pos < maxLength) && (null !== test.fn || "" !== test.def) || minimalPos > pos);
      return "" === maskTemplate[maskTemplate.length - 1] && maskTemplate.pop(), getMaskSet().maskLength = pos + 1,
        maskTemplate;
    }

    function getMaskSet() {
      return maskset;
    }

    function resetMaskSet(soft) {
      var maskset = getMaskSet();
      maskset.buffer = undefined, !0 !== soft && (maskset.validPositions = {}, maskset.p = 0);
    }

    function getLastValidPosition(closestTo, strict, validPositions) {
      var before = -1,
        after = -1,
        valids = validPositions || getMaskSet().validPositions;
      closestTo === undefined && (closestTo = -1);
      for (var posNdx in valids) {
        var psNdx = parseInt(posNdx);
        valids[psNdx] && (strict || !0 !== valids[psNdx].generatedInput) && (psNdx <= closestTo && (before = psNdx),
          psNdx >= closestTo && (after = psNdx));
      }
      return -1 !== before && closestTo - before > 1 || after < closestTo ? before : after;
    }

    function stripValidPositions(start, end, nocheck, strict) {
      var i, startPos = start,
        positionsClone = $.extend(!0, {}, getMaskSet().validPositions),
        needsValidation = !1;
      for (getMaskSet().p = start, i = end - 1; i >= startPos; i--) getMaskSet().validPositions[i] !== undefined && (!0 !== nocheck && (!getMaskSet().validPositions[i].match.optionality && function (pos) {
        var posMatch = getMaskSet().validPositions[pos];
        if (posMatch !== undefined && null === posMatch.match.fn) {
          var prevMatch = getMaskSet().validPositions[pos - 1],
            nextMatch = getMaskSet().validPositions[pos + 1];
          return prevMatch !== undefined && nextMatch !== undefined;
        }
        return !1;
      }(i) || !1 === opts.canClearPosition(getMaskSet(), i, getLastValidPosition(undefined, !0), strict, opts)) || delete getMaskSet().validPositions[i]);
      for (resetMaskSet(!0), i = startPos + 1; i <= getLastValidPosition();) {
        for (; getMaskSet().validPositions[startPos] !== undefined;) startPos++;
        if (i < startPos && (i = startPos + 1), getMaskSet().validPositions[i] === undefined && isMask(i)) i++;
        else {
          var t = getTestTemplate(i);
          !1 === needsValidation && positionsClone[startPos] && positionsClone[startPos].match.def === t.match.def ? (getMaskSet().validPositions[startPos] = $.extend(!0, {}, positionsClone[startPos]),
            getMaskSet().validPositions[startPos].input = t.input, delete getMaskSet().validPositions[i],
            i++) : positionCanMatchDefinition(startPos, t.match.def) ? !1 !== isValid(startPos, t.input || getPlaceholder(i), !0) && (delete getMaskSet().validPositions[i],
            i++, needsValidation = !0) : isMask(i) || (i++, startPos--), startPos++;
        }
      }
      resetMaskSet(!0);
    }

    function determineTestTemplate(tests, guessNextBest) {
      for (var testPos, testPositions = tests, lvp = getLastValidPosition(), lvTest = getMaskSet().validPositions[lvp] || getTests(0)[0], lvTestAltArr = lvTest.alternation !== undefined ? lvTest.locator[lvTest.alternation].toString().split(",") : [], ndx = 0; ndx < testPositions.length && (testPos = testPositions[ndx],
          !(testPos.match && (opts.greedy && !0 !== testPos.match.optionalQuantifier || (!1 === testPos.match.optionality || !1 === testPos.match.newBlockMarker) && !0 !== testPos.match.optionalQuantifier) && (lvTest.alternation === undefined || lvTest.alternation !== testPos.alternation || testPos.locator[lvTest.alternation] !== undefined && checkAlternationMatch(testPos.locator[lvTest.alternation].toString().split(","), lvTestAltArr))) || !0 === guessNextBest && (null !== testPos.match.fn || /[0-9a-bA-Z]/.test(testPos.match.def))); ndx++);
      return testPos;
    }

    function getTestTemplate(pos, ndxIntlzr, tstPs) {
      return getMaskSet().validPositions[pos] || determineTestTemplate(getTests(pos, ndxIntlzr ? ndxIntlzr.slice() : ndxIntlzr, tstPs));
    }

    function getTest(pos) {
      return getMaskSet().validPositions[pos] ? getMaskSet().validPositions[pos] : getTests(pos)[0];
    }

    function positionCanMatchDefinition(pos, def) {
      for (var valid = !1, tests = getTests(pos), tndx = 0; tndx < tests.length; tndx++)
        if (tests[tndx].match && tests[tndx].match.def === def) {
          valid = !0;
          break;
        }
      return valid;
    }

    function getTests(pos, ndxIntlzr, tstPs) {
      function resolveTestFromToken(maskToken, ndxInitializer, loopNdx, quantifierRecurse) {
        function handleMatch(match, loopNdx, quantifierRecurse) {
          function isFirstMatch(latestMatch, tokenGroup) {
            var firstMatch = 0 === $.inArray(latestMatch, tokenGroup.matches);
            return firstMatch || $.each(tokenGroup.matches, function (ndx, match) {
              if (!0 === match.isQuantifier && (firstMatch = isFirstMatch(latestMatch, tokenGroup.matches[ndx - 1]))) return !1;
            }), firstMatch;
          }

          function resolveNdxInitializer(pos, alternateNdx, targetAlternation) {
            var bestMatch, indexPos;
            if (getMaskSet().validPositions[pos - 1] && targetAlternation && getMaskSet().tests[pos])
              for (var vpAlternation = getMaskSet().validPositions[pos - 1].locator, tpAlternation = getMaskSet().tests[pos][0].locator, i = 0; i < targetAlternation; i++)
                if (vpAlternation[i] !== tpAlternation[i]) return vpAlternation.slice(targetAlternation + 1);
            return (getMaskSet().tests[pos] || getMaskSet().validPositions[pos]) && $.each(getMaskSet().tests[pos] || [getMaskSet().validPositions[pos]], function (ndx, lmnt) {
              var alternation = targetAlternation !== undefined ? targetAlternation : lmnt.alternation,
                ndxPos = lmnt.locator[alternation] !== undefined ? lmnt.locator[alternation].toString().indexOf(alternateNdx) : -1;
              (indexPos === undefined || ndxPos < indexPos) && -1 !== ndxPos && (bestMatch = lmnt,
                indexPos = ndxPos);
            }), bestMatch ? bestMatch.locator.slice((targetAlternation !== undefined ? targetAlternation : bestMatch.alternation) + 1) : targetAlternation !== undefined ? resolveNdxInitializer(pos, alternateNdx) : undefined;
          }
          if (testPos > 1e4) throw "Inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + getMaskSet().mask;
          if (testPos === pos && match.matches === undefined) return matches.push({
            match: match,
            locator: loopNdx.reverse(),
            cd: cacheDependency
          }), !0;
          if (match.matches !== undefined) {
            if (match.isGroup && quantifierRecurse !== match) {
              if (match = handleMatch(maskToken.matches[$.inArray(match, maskToken.matches) + 1], loopNdx)) return !0;
            } else if (match.isOptional) {
              var optionalToken = match;
              if (match = resolveTestFromToken(match, ndxInitializer, loopNdx, quantifierRecurse)) {
                if (latestMatch = matches[matches.length - 1].match, !isFirstMatch(latestMatch, optionalToken)) return !0;
                insertStop = !0, testPos = pos;
              }
            } else if (match.isAlternator) {
              var maltMatches, alternateToken = match,
                malternateMatches = [],
                currentMatches = matches.slice(),
                loopNdxCnt = loopNdx.length,
                altIndex = ndxInitializer.length > 0 ? ndxInitializer.shift() : -1;
              if (-1 === altIndex || "string" == typeof altIndex) {
                var amndx, currentPos = testPos,
                  ndxInitializerClone = ndxInitializer.slice(),
                  altIndexArr = [];
                if ("string" == typeof altIndex) altIndexArr = altIndex.split(",");
                else
                  for (amndx = 0; amndx < alternateToken.matches.length; amndx++) altIndexArr.push(amndx);
                for (var ndx = 0; ndx < altIndexArr.length; ndx++) {
                  if (amndx = parseInt(altIndexArr[ndx]), matches = [], ndxInitializer = resolveNdxInitializer(testPos, amndx, loopNdxCnt) || ndxInitializerClone.slice(),
                    !0 !== (match = handleMatch(alternateToken.matches[amndx] || maskToken.matches[amndx], [amndx].concat(loopNdx), quantifierRecurse) || match) && match !== undefined && altIndexArr[altIndexArr.length - 1] < alternateToken.matches.length) {
                    var ntndx = $.inArray(match, maskToken.matches) + 1;
                    maskToken.matches.length > ntndx && (match = handleMatch(maskToken.matches[ntndx], [ntndx].concat(loopNdx.slice(1, loopNdx.length)), quantifierRecurse)) && (altIndexArr.push(ntndx.toString()),
                      $.each(matches, function (ndx, lmnt) {
                        lmnt.alternation = loopNdx.length - 1;
                      }));
                  }
                  maltMatches = matches.slice(), testPos = currentPos, matches = [];
                  for (var ndx1 = 0; ndx1 < maltMatches.length; ndx1++) {
                    var altMatch = maltMatches[ndx1],
                      dropMatch = !1;
                    altMatch.alternation = altMatch.alternation || loopNdxCnt;
                    for (var ndx2 = 0; ndx2 < malternateMatches.length; ndx2++) {
                      var altMatch2 = malternateMatches[ndx2];
                      if ("string" != typeof altIndex || -1 !== $.inArray(altMatch.locator[altMatch.alternation].toString(), altIndexArr)) {
                        if (function (source, target) {
                            return source.match.nativeDef === target.match.nativeDef || source.match.def === target.match.nativeDef || source.match.nativeDef === target.match.def;
                          }(altMatch, altMatch2)) {
                          dropMatch = !0, altMatch.alternation === altMatch2.alternation && -1 === altMatch2.locator[altMatch2.alternation].toString().indexOf(altMatch.locator[altMatch.alternation]) && (altMatch2.locator[altMatch2.alternation] = altMatch2.locator[altMatch2.alternation] + "," + altMatch.locator[altMatch.alternation],
                            altMatch2.alternation = altMatch.alternation), altMatch.match.nativeDef === altMatch2.match.def && (altMatch.locator[altMatch.alternation] = altMatch2.locator[altMatch2.alternation],
                            malternateMatches.splice(malternateMatches.indexOf(altMatch2), 1, altMatch));
                          break;
                        }
                        if (altMatch.match.def === altMatch2.match.def) {
                          dropMatch = !1;
                          break;
                        }
                        if (function (source, target) {
                            return null === source.match.fn && null !== target.match.fn && target.match.fn.test(source.match.def, getMaskSet(), pos, !1, opts, !1);
                          }(altMatch, altMatch2) || function (source, target) {
                            return null !== source.match.fn && null !== target.match.fn && target.match.fn.test(source.match.def.replace(/[\[\]]/g, ""), getMaskSet(), pos, !1, opts, !1);
                          }(altMatch, altMatch2)) {
                          altMatch.alternation === altMatch2.alternation && -1 === altMatch.locator[altMatch.alternation].toString().indexOf(altMatch2.locator[altMatch2.alternation].toString().split("")[0]) && (altMatch.na = altMatch.na || altMatch.locator[altMatch.alternation].toString(),
                            -1 === altMatch.na.indexOf(altMatch.locator[altMatch.alternation].toString().split("")[0]) && (altMatch.na = altMatch.na + "," + altMatch.locator[altMatch2.alternation].toString().split("")[0]),
                            dropMatch = !0, altMatch.locator[altMatch.alternation] = altMatch2.locator[altMatch2.alternation].toString().split("")[0] + "," + altMatch.locator[altMatch.alternation],
                            malternateMatches.splice(malternateMatches.indexOf(altMatch2), 0, altMatch));
                          break;
                        }
                      }
                    }
                    dropMatch || malternateMatches.push(altMatch);
                  }
                }
                "string" == typeof altIndex && (malternateMatches = $.map(malternateMatches, function (lmnt, ndx) {
                    if (isFinite(ndx)) {
                      var alternation = lmnt.alternation,
                        altLocArr = lmnt.locator[alternation].toString().split(",");
                      lmnt.locator[alternation] = undefined, lmnt.alternation = undefined;
                      for (var alndx = 0; alndx < altLocArr.length; alndx++) - 1 !== $.inArray(altLocArr[alndx], altIndexArr) && (lmnt.locator[alternation] !== undefined ? (lmnt.locator[alternation] += ",",
                          lmnt.locator[alternation] += altLocArr[alndx]) : lmnt.locator[alternation] = parseInt(altLocArr[alndx]),
                        lmnt.alternation = alternation);
                      if (lmnt.locator[alternation] !== undefined) return lmnt;
                    }
                  })), matches = currentMatches.concat(malternateMatches), testPos = pos, insertStop = matches.length > 0,
                  match = malternateMatches.length > 0, ndxInitializer = ndxInitializerClone.slice();
              } else match = handleMatch(alternateToken.matches[altIndex] || maskToken.matches[altIndex], [altIndex].concat(loopNdx), quantifierRecurse);
              if (match) return !0;
            } else if (match.isQuantifier && quantifierRecurse !== maskToken.matches[$.inArray(match, maskToken.matches) - 1])
              for (var qt = match, qndx = ndxInitializer.length > 0 ? ndxInitializer.shift() : 0; qndx < (isNaN(qt.quantifier.max) ? qndx + 1 : qt.quantifier.max) && testPos <= pos; qndx++) {
                var tokenGroup = maskToken.matches[$.inArray(qt, maskToken.matches) - 1];
                if (match = handleMatch(tokenGroup, [qndx].concat(loopNdx), tokenGroup)) {
                  if (latestMatch = matches[matches.length - 1].match, latestMatch.optionalQuantifier = qndx > qt.quantifier.min - 1,
                    isFirstMatch(latestMatch, tokenGroup)) {
                    if (qndx > qt.quantifier.min - 1) {
                      insertStop = !0, testPos = pos;
                      break;
                    }
                    return !0;
                  }
                  return !0;
                }
              } else if (match = resolveTestFromToken(match, ndxInitializer, loopNdx, quantifierRecurse)) return !0;
          } else testPos++;
        }
        for (var tndx = ndxInitializer.length > 0 ? ndxInitializer.shift() : 0; tndx < maskToken.matches.length; tndx++)
          if (!0 !== maskToken.matches[tndx].isQuantifier) {
            var match = handleMatch(maskToken.matches[tndx], [tndx].concat(loopNdx), quantifierRecurse);
            if (match && testPos === pos) return match;
            if (testPos > pos) break;
          }
      }

      function filterTests(tests) {
        if (opts.keepStatic && pos > 0 && tests.length > 1 + ("" === tests[tests.length - 1].match.def ? 1 : 0) && !0 !== tests[0].match.optionality && !0 !== tests[0].match.optionalQuantifier && null === tests[0].match.fn && !/[0-9a-bA-Z]/.test(tests[0].match.def)) {
          if (getMaskSet().validPositions[pos - 1] === undefined) return [determineTestTemplate(tests)];
          if (getMaskSet().validPositions[pos - 1].alternation === tests[0].alternation) return [determineTestTemplate(tests)];
          if (getMaskSet().validPositions[pos - 1]) return [determineTestTemplate(tests)];
        }
        return tests;
      }
      var latestMatch, maskTokens = getMaskSet().maskToken,
        testPos = ndxIntlzr ? tstPs : 0,
        ndxInitializer = ndxIntlzr ? ndxIntlzr.slice() : [0],
        matches = [],
        insertStop = !1,
        cacheDependency = ndxIntlzr ? ndxIntlzr.join("") : "";
      if (pos > -1) {
        if (ndxIntlzr === undefined) {
          for (var test, previousPos = pos - 1;
            (test = getMaskSet().validPositions[previousPos] || getMaskSet().tests[previousPos]) === undefined && previousPos > -1;) previousPos--;
          test !== undefined && previousPos > -1 && (ndxInitializer = function (tests) {
            var locator = [];
            return $.isArray(tests) || (tests = [tests]), tests.length > 0 && (tests[0].alternation === undefined ? (locator = determineTestTemplate(tests.slice()).locator.slice(),
              0 === locator.length && (locator = tests[0].locator.slice())) : $.each(tests, function (ndx, tst) {
              if ("" !== tst.def)
                if (0 === locator.length) locator = tst.locator.slice();
                else
                  for (var i = 0; i < locator.length; i++) tst.locator[i] && -1 === locator[i].toString().indexOf(tst.locator[i]) && (locator[i] += "," + tst.locator[i]);
            })), locator;
          }(test), cacheDependency = ndxInitializer.join(""), testPos = previousPos);
        }
        if (getMaskSet().tests[pos] && getMaskSet().tests[pos][0].cd === cacheDependency) return filterTests(getMaskSet().tests[pos]);
        for (var mtndx = ndxInitializer.shift(); mtndx < maskTokens.length; mtndx++) {
          if (resolveTestFromToken(maskTokens[mtndx], ndxInitializer, [mtndx]) && testPos === pos || testPos > pos) break;
        }
      }
      return (0 === matches.length || insertStop) && matches.push({
        match: {
          fn: null,
          cardinality: 0,
          optionality: !0,
          casing: null,
          def: "",
          placeholder: ""
        },
        locator: [],
        cd: cacheDependency
      }), ndxIntlzr !== undefined && getMaskSet().tests[pos] ? filterTests($.extend(!0, [], matches)) : (getMaskSet().tests[pos] = $.extend(!0, [], matches),
        filterTests(getMaskSet().tests[pos]));
    }

    function getBufferTemplate() {
      return getMaskSet()._buffer === undefined && (getMaskSet()._buffer = getMaskTemplate(!1, 1),
          getMaskSet().buffer === undefined && (getMaskSet().buffer = getMaskSet()._buffer.slice())),
        getMaskSet()._buffer;
    }

    function getBuffer(noCache) {
      return getMaskSet().buffer !== undefined && !0 !== noCache || (getMaskSet().buffer = getMaskTemplate(!0, getLastValidPosition(), !0)),
        getMaskSet().buffer;
    }

    function refreshFromBuffer(start, end, buffer) {
      var i, p;
      if (!0 === start) resetMaskSet(), start = 0, end = buffer.length;
      else
        for (i = start; i < end; i++) delete getMaskSet().validPositions[i];
      for (p = start, i = start; i < end; i++)
        if (resetMaskSet(!0), buffer[i] !== opts.skipOptionalPartCharacter) {
          var valResult = isValid(p, buffer[i], !0, !0);
          !1 !== valResult && (resetMaskSet(!0), p = valResult.caret !== undefined ? valResult.caret : valResult.pos + 1);
        }
    }

    function casing(elem, test, pos) {
      switch (opts.casing || test.casing) {
        case "upper":
          elem = elem.toUpperCase();
          break;

        case "lower":
          elem = elem.toLowerCase();
          break;

        case "title":
          var posBefore = getMaskSet().validPositions[pos - 1];
          elem = 0 === pos || posBefore && posBefore.input === String.fromCharCode(Inputmask.keyCode.SPACE) ? elem.toUpperCase() : elem.toLowerCase();
          break;

        default:
          if ($.isFunction(opts.casing)) {
            var args = Array.prototype.slice.call(arguments);
            args.push(getMaskSet().validPositions), elem = opts.casing.apply(this, args);
          }
      }
      return elem;
    }

    function checkAlternationMatch(altArr1, altArr2, na) {
      for (var naNdx, altArrC = opts.greedy ? altArr2 : altArr2.slice(0, 1), isMatch = !1, naArr = na !== undefined ? na.split(",") : [], i = 0; i < naArr.length; i++) - 1 !== (naNdx = altArr1.indexOf(naArr[i])) && altArr1.splice(naNdx, 1);
      for (var alndx = 0; alndx < altArr1.length; alndx++)
        if (-1 !== $.inArray(altArr1[alndx], altArrC)) {
          isMatch = !0;
          break;
        }
      return isMatch;
    }

    function isValid(pos, c, strict, fromSetValid, fromAlternate, validateOnly) {
      function isSelection(posObj) {
        var selection = isRTL ? posObj.begin - posObj.end > 1 || posObj.begin - posObj.end == 1 : posObj.end - posObj.begin > 1 || posObj.end - posObj.begin == 1;
        return selection && 0 === posObj.begin && posObj.end === getMaskSet().maskLength ? "full" : selection;
      }

      function _isValid(position, c, strict) {
        var rslt = !1;
        return $.each(getTests(position), function (ndx, tst) {
          for (var test = tst.match, loopend = c ? 1 : 0, chrs = "", i = test.cardinality; i > loopend; i--) chrs += getBufferElement(position - (i - 1));
          if (c && (chrs += c), getBuffer(!0), !1 !== (rslt = null != test.fn ? test.fn.test(chrs, getMaskSet(), position, strict, opts, isSelection(pos)) : (c === test.def || c === opts.skipOptionalPartCharacter) && "" !== test.def && {
              c: getPlaceholder(position, test, !0) || test.def,
              pos: position
            })) {
            var elem = rslt.c !== undefined ? rslt.c : c;
            elem = elem === opts.skipOptionalPartCharacter && null === test.fn ? getPlaceholder(position, test, !0) || test.def : elem;
            var validatedPos = position,
              possibleModifiedBuffer = getBuffer();
            if (rslt.remove !== undefined && ($.isArray(rslt.remove) || (rslt.remove = [rslt.remove]),
                $.each(rslt.remove.sort(function (a, b) {
                  return b - a;
                }), function (ndx, lmnt) {
                  stripValidPositions(lmnt, lmnt + 1, !0);
                })), rslt.insert !== undefined && ($.isArray(rslt.insert) || (rslt.insert = [rslt.insert]),
                $.each(rslt.insert.sort(function (a, b) {
                  return a - b;
                }), function (ndx, lmnt) {
                  isValid(lmnt.pos, lmnt.c, !0, fromSetValid);
                })), rslt.refreshFromBuffer) {
              var refresh = rslt.refreshFromBuffer;
              if (refreshFromBuffer(!0 === refresh ? refresh : refresh.start, refresh.end, possibleModifiedBuffer),
                rslt.pos === undefined && rslt.c === undefined) return rslt.pos = getLastValidPosition(),
                !1;
              if ((validatedPos = rslt.pos !== undefined ? rslt.pos : position) !== position) return rslt = $.extend(rslt, isValid(validatedPos, elem, !0, fromSetValid)),
                !1;
            } else if (!0 !== rslt && rslt.pos !== undefined && rslt.pos !== position && (validatedPos = rslt.pos,
                refreshFromBuffer(position, validatedPos, getBuffer().slice()), validatedPos !== position)) return rslt = $.extend(rslt, isValid(validatedPos, elem, !0)),
              !1;
            return (!0 === rslt || rslt.pos !== undefined || rslt.c !== undefined) && (ndx > 0 && resetMaskSet(!0),
              setValidPosition(validatedPos, $.extend({}, tst, {
                input: casing(elem, test, validatedPos)
              }), fromSetValid, isSelection(pos)) || (rslt = !1), !1);
          }
        }), rslt;
      }

      function setValidPosition(pos, validTest, fromSetValid, isSelection) {
        if (isSelection || opts.insertMode && getMaskSet().validPositions[pos] !== undefined && fromSetValid === undefined) {
          var i, positionsClone = $.extend(!0, {}, getMaskSet().validPositions),
            lvp = getLastValidPosition(undefined, !0);
          for (i = pos; i <= lvp; i++) delete getMaskSet().validPositions[i];
          getMaskSet().validPositions[pos] = $.extend(!0, {}, validTest);
          var j, valid = !0,
            vps = getMaskSet().validPositions,
            needsValidation = !1,
            initialLength = getMaskSet().maskLength;
          for (i = j = pos; i <= lvp; i++) {
            var t = positionsClone[i];
            if (t !== undefined)
              for (var posMatch = j; posMatch < getMaskSet().maskLength && (null === t.match.fn && vps[i] && (!0 === vps[i].match.optionalQuantifier || !0 === vps[i].match.optionality) || null != t.match.fn);) {
                if (posMatch++, !1 === needsValidation && positionsClone[posMatch] && positionsClone[posMatch].match.def === t.match.def) getMaskSet().validPositions[posMatch] = $.extend(!0, {}, positionsClone[posMatch]),
                  getMaskSet().validPositions[posMatch].input = t.input, fillMissingNonMask(posMatch),
                  j = posMatch, valid = !0;
                else if (positionCanMatchDefinition(posMatch, t.match.def)) {
                  var result = isValid(posMatch, t.input, !0, !0);
                  valid = !1 !== result, j = result.caret || result.insert ? getLastValidPosition() : posMatch,
                    needsValidation = !0;
                } else if (!(valid = !0 === t.generatedInput) && posMatch >= getMaskSet().maskLength - 1) break;
                if (getMaskSet().maskLength < initialLength && (getMaskSet().maskLength = initialLength),
                  valid) break;
              }
            if (!valid) break;
          }
          if (!valid) return getMaskSet().validPositions = $.extend(!0, {}, positionsClone),
            resetMaskSet(!0), !1;
        } else getMaskSet().validPositions[pos] = $.extend(!0, {}, validTest);
        return resetMaskSet(!0), !0;
      }

      function fillMissingNonMask(maskPos) {
        for (var pndx = maskPos - 1; pndx > -1 && !getMaskSet().validPositions[pndx]; pndx--);
        var testTemplate, testsFromPos;
        for (pndx++; pndx < maskPos; pndx++) getMaskSet().validPositions[pndx] === undefined && (!1 === opts.jitMasking || opts.jitMasking > pndx) && (testsFromPos = getTests(pndx, getTestTemplate(pndx - 1).locator, pndx - 1).slice(),
          "" === testsFromPos[testsFromPos.length - 1].match.def && testsFromPos.pop(), (testTemplate = determineTestTemplate(testsFromPos)) && (testTemplate.match.def === opts.radixPointDefinitionSymbol || !isMask(pndx, !0) || $.inArray(opts.radixPoint, getBuffer()) < pndx && testTemplate.match.fn && testTemplate.match.fn.test(getPlaceholder(pndx), getMaskSet(), pndx, !1, opts)) && !1 !== (result = _isValid(pndx, getPlaceholder(pndx, testTemplate.match, !0) || (null == testTemplate.match.fn ? testTemplate.match.def : "" !== getPlaceholder(pndx) ? getPlaceholder(pndx) : getBuffer()[pndx]), !0)) && (getMaskSet().validPositions[result.pos || pndx].generatedInput = !0));
      }
      strict = !0 === strict;
      var maskPos = pos;
      pos.begin !== undefined && (maskPos = isRTL && !isSelection(pos) ? pos.end : pos.begin);
      var result = !0,
        positionsClone = $.extend(!0, {}, getMaskSet().validPositions);
      if ($.isFunction(opts.preValidation) && !strict && !0 !== fromSetValid && !0 !== validateOnly && (result = opts.preValidation(getBuffer(), maskPos, c, isSelection(pos), opts)),
        !0 === result) {
        if (fillMissingNonMask(maskPos), isSelection(pos) && (handleRemove(undefined, Inputmask.keyCode.DELETE, pos, !0, !0),
            maskPos = getMaskSet().p), maskPos < getMaskSet().maskLength && (maxLength === undefined || maskPos < maxLength) && (result = _isValid(maskPos, c, strict),
            (!strict || !0 === fromSetValid) && !1 === result && !0 !== validateOnly)) {
          var currentPosValid = getMaskSet().validPositions[maskPos];
          if (!currentPosValid || null !== currentPosValid.match.fn || currentPosValid.match.def !== c && c !== opts.skipOptionalPartCharacter) {
            if ((opts.insertMode || getMaskSet().validPositions[seekNext(maskPos)] === undefined) && !isMask(maskPos, !0))
              for (var nPos = maskPos + 1, snPos = seekNext(maskPos); nPos <= snPos; nPos++)
                if (!1 !== (result = _isValid(nPos, c, strict))) {
                  ! function (originalPos, newPos) {
                    var vp = getMaskSet().validPositions[newPos];
                    if (vp)
                      for (var targetLocator = vp.locator, tll = targetLocator.length, ps = originalPos; ps < newPos; ps++)
                        if (getMaskSet().validPositions[ps] === undefined && !isMask(ps, !0)) {
                          var tests = getTests(ps).slice(),
                            bestMatch = determineTestTemplate(tests, !0),
                            equality = -1;
                          "" === tests[tests.length - 1].match.def && tests.pop(), $.each(tests, function (ndx, tst) {
                              for (var i = 0; i < tll; i++) {
                                if (tst.locator[i] === undefined || !checkAlternationMatch(tst.locator[i].toString().split(","), targetLocator[i].toString().split(","), tst.na)) {
                                  var targetAI = targetLocator[i],
                                    bestMatchAI = bestMatch.locator[i],
                                    tstAI = tst.locator[i];
                                  targetAI - bestMatchAI > Math.abs(targetAI - tstAI) && (bestMatch = tst);
                                  break;
                                }
                                equality < i && (equality = i, bestMatch = tst);
                              }
                            }), bestMatch = $.extend({}, bestMatch, {
                              input: getPlaceholder(ps, bestMatch.match, !0) || bestMatch.match.def
                            }), bestMatch.generatedInput = !0, setValidPosition(ps, bestMatch, !0), getMaskSet().validPositions[newPos] = undefined,
                            _isValid(newPos, vp.input, !0);
                        }
                  }(maskPos, result.pos !== undefined ? result.pos : nPos), maskPos = nPos;
                  break;
                }
          } else result = {
            caret: seekNext(maskPos)
          };
        }!1 === result && opts.keepStatic && !strict && !0 !== fromAlternate && (result = function (pos, c, strict) {
          var lastAlt, alternation, altPos, prevAltPos, i, validPos, altNdxs, decisionPos, validPsClone = $.extend(!0, {}, getMaskSet().validPositions),
            isValidRslt = !1,
            lAltPos = getLastValidPosition();
          for (prevAltPos = getMaskSet().validPositions[lAltPos]; lAltPos >= 0; lAltPos--)
            if ((altPos = getMaskSet().validPositions[lAltPos]) && altPos.alternation !== undefined) {
              if (lastAlt = lAltPos, alternation = getMaskSet().validPositions[lastAlt].alternation,
                prevAltPos.locator[altPos.alternation] !== altPos.locator[altPos.alternation]) break;
              prevAltPos = altPos;
            }
          if (alternation !== undefined) {
            decisionPos = parseInt(lastAlt);
            var decisionTaker = prevAltPos.locator[prevAltPos.alternation || alternation] !== undefined ? prevAltPos.locator[prevAltPos.alternation || alternation] : altNdxs[0];
            decisionTaker.length > 0 && (decisionTaker = decisionTaker.split(",")[0]);
            var possibilityPos = getMaskSet().validPositions[decisionPos],
              prevPos = getMaskSet().validPositions[decisionPos - 1];
            $.each(getTests(decisionPos, prevPos ? prevPos.locator : undefined, decisionPos - 1), function (ndx, test) {
              altNdxs = test.locator[alternation] ? test.locator[alternation].toString().split(",") : [];
              for (var mndx = 0; mndx < altNdxs.length; mndx++) {
                var validInputs = [],
                  staticInputsBeforePos = 0,
                  staticInputsBeforePosAlternate = 0,
                  verifyValidInput = !1;
                if (decisionTaker < altNdxs[mndx] && (test.na === undefined || -1 === $.inArray(altNdxs[mndx], test.na.split(",")) || -1 === $.inArray(decisionTaker.toString(), altNdxs))) {
                  getMaskSet().validPositions[decisionPos] = $.extend(!0, {}, test);
                  var possibilities = getMaskSet().validPositions[decisionPos].locator;
                  for (getMaskSet().validPositions[decisionPos].locator[alternation] = parseInt(altNdxs[mndx]),
                    null == test.match.fn ? (possibilityPos.input !== test.match.def && (verifyValidInput = !0,
                        !0 !== possibilityPos.generatedInput && validInputs.push(possibilityPos.input)),
                      staticInputsBeforePosAlternate++, getMaskSet().validPositions[decisionPos].generatedInput = !/[0-9a-bA-Z]/.test(test.match.def),
                      getMaskSet().validPositions[decisionPos].input = test.match.def) : getMaskSet().validPositions[decisionPos].input = possibilityPos.input,
                    i = decisionPos + 1; i < getLastValidPosition(undefined, !0) + 1; i++) validPos = getMaskSet().validPositions[i],
                    validPos && !0 !== validPos.generatedInput && /[0-9a-bA-Z]/.test(validPos.input) ? validInputs.push(validPos.input) : i < pos && staticInputsBeforePos++,
                    delete getMaskSet().validPositions[i];
                  for (verifyValidInput && validInputs[0] === test.match.def && validInputs.shift(),
                    resetMaskSet(!0), isValidRslt = !0; validInputs.length > 0;) {
                    var input = validInputs.shift();
                    if (input !== opts.skipOptionalPartCharacter && !(isValidRslt = isValid(getLastValidPosition(undefined, !0) + 1, input, !1, fromSetValid, !0))) break;
                  }
                  if (isValidRslt) {
                    getMaskSet().validPositions[decisionPos].locator = possibilities;
                    var targetLvp = getLastValidPosition(pos) + 1;
                    for (i = decisionPos + 1; i < getLastValidPosition() + 1; i++)((validPos = getMaskSet().validPositions[i]) === undefined || null == validPos.match.fn) && i < pos + (staticInputsBeforePosAlternate - staticInputsBeforePos) && staticInputsBeforePosAlternate++;
                    pos += staticInputsBeforePosAlternate - staticInputsBeforePos, isValidRslt = isValid(pos > targetLvp ? targetLvp : pos, c, strict, fromSetValid, !0);
                  }
                  if (isValidRslt) return !1;
                  resetMaskSet(), getMaskSet().validPositions = $.extend(!0, {}, validPsClone);
                }
              }
            });
          }
          return isValidRslt;
        }(maskPos, c, strict)), !0 === result && (result = {
          pos: maskPos
        });
      }
      if ($.isFunction(opts.postValidation) && !1 !== result && !strict && !0 !== fromSetValid && !0 !== validateOnly) {
        var postResult = opts.postValidation(getBuffer(!0), result, opts);
        if (postResult.refreshFromBuffer && postResult.buffer) {
          var refresh = postResult.refreshFromBuffer;
          refreshFromBuffer(!0 === refresh ? refresh : refresh.start, refresh.end, postResult.buffer);
        }
        result = !0 === postResult ? result : postResult;
      }
      return result && result.pos === undefined && (result.pos = maskPos), !1 !== result && !0 !== validateOnly || (resetMaskSet(!0),
        getMaskSet().validPositions = $.extend(!0, {}, positionsClone)), result;
    }

    function isMask(pos, strict) {
      var test = getTestTemplate(pos).match;
      if ("" === test.def && (test = getTest(pos).match), null != test.fn) return test.fn;
      if (!0 !== strict && pos > -1) {
        var tests = getTests(pos);
        return tests.length > 1 + ("" === tests[tests.length - 1].match.def ? 1 : 0);
      }
      return !1;
    }

    function seekNext(pos, newBlock) {
      var maskL = getMaskSet().maskLength;
      if (pos >= maskL) return maskL;
      var position = pos;
      for (getTests(maskL + 1).length > 1 && (getMaskTemplate(!0, maskL + 1, !0), maskL = getMaskSet().maskLength); ++position < maskL && (!0 === newBlock && (!0 !== getTest(position).match.newBlockMarker || !isMask(position)) || !0 !== newBlock && !isMask(position)););
      return position;
    }

    function seekPrevious(pos, newBlock) {
      var tests, position = pos;
      if (position <= 0) return 0;
      for (; --position > 0 && (!0 === newBlock && !0 !== getTest(position).match.newBlockMarker || !0 !== newBlock && !isMask(position) && (tests = getTests(position),
          tests.length < 2 || 2 === tests.length && "" === tests[1].match.def)););
      return position;
    }

    function getBufferElement(position) {
      return getMaskSet().validPositions[position] === undefined ? getPlaceholder(position) : getMaskSet().validPositions[position].input;
    }

    function writeBuffer(input, buffer, caretPos, event, triggerInputEvent) {
      if (event && $.isFunction(opts.onBeforeWrite)) {
        var result = opts.onBeforeWrite.call(inputmask, event, buffer, caretPos, opts);
        if (result) {
          if (result.refreshFromBuffer) {
            var refresh = result.refreshFromBuffer;
            refreshFromBuffer(!0 === refresh ? refresh : refresh.start, refresh.end, result.buffer || buffer),
              buffer = getBuffer(!0);
          }
          caretPos !== undefined && (caretPos = result.caret !== undefined ? result.caret : caretPos);
        }
      }
      input !== undefined && (input.inputmask._valueSet(buffer.join("")), caretPos === undefined || event !== undefined && "blur" === event.type ? renderColorMask(input, caretPos, 0 === buffer.length) : android && event && "input" === event.type ? setTimeout(function () {
        caret(input, caretPos);
      }, 0) : caret(input, caretPos), !0 === triggerInputEvent && (skipInputEvent = !0,
        $(input).trigger("input")));
    }

    function getPlaceholder(pos, test, returnPL) {
      if (test = test || getTest(pos).match, test.placeholder !== undefined || !0 === returnPL) return $.isFunction(test.placeholder) ? test.placeholder(opts) : test.placeholder;
      if (null === test.fn) {
        if (pos > -1 && getMaskSet().validPositions[pos] === undefined) {
          var prevTest, tests = getTests(pos),
            staticAlternations = [];
          if (tests.length > 1 + ("" === tests[tests.length - 1].match.def ? 1 : 0))
            for (var i = 0; i < tests.length; i++)
              if (!0 !== tests[i].match.optionality && !0 !== tests[i].match.optionalQuantifier && (null === tests[i].match.fn || prevTest === undefined || !1 !== tests[i].match.fn.test(prevTest.match.def, getMaskSet(), pos, !0, opts)) && (staticAlternations.push(tests[i]),
                  null === tests[i].match.fn && (prevTest = tests[i]), staticAlternations.length > 1 && /[0-9a-bA-Z]/.test(staticAlternations[0].match.def))) return opts.placeholder.charAt(pos % opts.placeholder.length);
        }
        return test.def;
      }
      return opts.placeholder.charAt(pos % opts.placeholder.length);
    }

    function checkVal(input, writeOut, strict, nptvl, initiatingEvent) {
      function isTemplateMatch(ndx, charCodes) {
        return -1 !== getBufferTemplate().slice(ndx, seekNext(ndx)).join("").indexOf(charCodes) && !isMask(ndx) && getTest(ndx).match.nativeDef === charCodes.charAt(charCodes.length - 1);
      }
      var inputValue = nptvl.slice(),
        charCodes = "",
        initialNdx = -1,
        result = undefined;
      if (resetMaskSet(), strict || !0 === opts.autoUnmask) initialNdx = seekNext(initialNdx);
      else {
        var staticInput = getBufferTemplate().slice(0, seekNext(-1)).join(""),
          matches = inputValue.join("").match(new RegExp("^" + Inputmask.escapeRegex(staticInput), "g"));
        matches && matches.length > 0 && (inputValue.splice(0, matches.length * staticInput.length),
          initialNdx = seekNext(initialNdx));
      }
      if (-1 === initialNdx ? (getMaskSet().p = seekNext(initialNdx), initialNdx = 0) : getMaskSet().p = initialNdx,
        $.each(inputValue, function (ndx, charCode) {
          if (charCode !== undefined)
            if (getMaskSet().validPositions[ndx] === undefined && inputValue[ndx] === getPlaceholder(ndx) && isMask(ndx, !0) && !1 === isValid(ndx, inputValue[ndx], !0, undefined, undefined, !0)) getMaskSet().p++;
            else {
              var keypress = new $.Event("_checkval");
              keypress.which = charCode.charCodeAt(0), charCodes += charCode;
              var lvp = getLastValidPosition(undefined, !0),
                lvTest = getMaskSet().validPositions[lvp],
                nextTest = getTestTemplate(lvp + 1, lvTest ? lvTest.locator.slice() : undefined, lvp);
              if (!isTemplateMatch(initialNdx, charCodes) || strict || opts.autoUnmask) {
                var pos = strict ? ndx : null == nextTest.match.fn && nextTest.match.optionality && lvp + 1 < getMaskSet().p ? lvp + 1 : getMaskSet().p;
                result = EventHandlers.keypressEvent.call(input, keypress, !0, !1, strict, pos),
                  initialNdx = pos + 1, charCodes = "";
              } else result = EventHandlers.keypressEvent.call(input, keypress, !0, !1, !0, lvp + 1);
              if (!1 !== result && !strict && $.isFunction(opts.onBeforeWrite)) {
                var origResult = result;
                if (result = opts.onBeforeWrite.call(inputmask, keypress, getBuffer(), result.forwardPosition, opts),
                  (result = $.extend(origResult, result)) && result.refreshFromBuffer) {
                  var refresh = result.refreshFromBuffer;
                  refreshFromBuffer(!0 === refresh ? refresh : refresh.start, refresh.end, result.buffer),
                    resetMaskSet(!0), result.caret && (getMaskSet().p = result.caret, result.forwardPosition = result.caret);
                }
              }
            }
        }), writeOut) {
        var caretPos = undefined;
        document.activeElement === input && result && (caretPos = opts.numericInput ? seekPrevious(result.forwardPosition) : result.forwardPosition),
          writeBuffer(input, getBuffer(), caretPos, initiatingEvent || new $.Event("checkval"), initiatingEvent && "input" === initiatingEvent.type);
      }
    }

    function unmaskedvalue(input) {
      if (input) {
        if (input.inputmask === undefined) return input.value;
        input.inputmask && input.inputmask.refreshValue && EventHandlers.setValueEvent.call(input);
      }
      var umValue = [],
        vps = getMaskSet().validPositions;
      for (var pndx in vps) vps[pndx].match && null != vps[pndx].match.fn && umValue.push(vps[pndx].input);
      var unmaskedValue = 0 === umValue.length ? "" : (isRTL ? umValue.reverse() : umValue).join("");
      if ($.isFunction(opts.onUnMask)) {
        var bufferValue = (isRTL ? getBuffer().slice().reverse() : getBuffer()).join("");
        unmaskedValue = opts.onUnMask.call(inputmask, bufferValue, unmaskedValue, opts);
      }
      return unmaskedValue;
    }

    function caret(input, begin, end, notranslate) {
      function translatePosition(pos) {
        if (!0 !== notranslate && isRTL && "number" == typeof pos && (!opts.greedy || "" !== opts.placeholder)) {
          pos = getBuffer().join("").length - pos;
        }
        return pos;
      }
      var range;
      if (begin === undefined) return input.setSelectionRange ? (begin = input.selectionStart,
        end = input.selectionEnd) : window.getSelection ? (range = window.getSelection().getRangeAt(0),
        range.commonAncestorContainer.parentNode !== input && range.commonAncestorContainer !== input || (begin = range.startOffset,
          end = range.endOffset)) : document.selection && document.selection.createRange && (range = document.selection.createRange(),
        begin = 0 - range.duplicate().moveStart("character", -input.inputmask._valueGet().length),
        end = begin + range.text.length), {
        begin: translatePosition(begin),
        end: translatePosition(end)
      };
      if (begin.begin !== undefined && (end = begin.end, begin = begin.begin), "number" == typeof begin) {
        begin = translatePosition(begin), end = translatePosition(end), end = "number" == typeof end ? end : begin;
        var scrollCalc = parseInt(((input.ownerDocument.defaultView || window).getComputedStyle ? (input.ownerDocument.defaultView || window).getComputedStyle(input, null) : input.currentStyle).fontSize) * end;
        if (input.scrollLeft = scrollCalc > input.scrollWidth ? scrollCalc : 0, mobile || !1 !== opts.insertMode || begin !== end || end++,
          input.setSelectionRange) input.selectionStart = begin, input.selectionEnd = end;
        else if (window.getSelection) {
          if (range = document.createRange(), input.firstChild === undefined || null === input.firstChild) {
            var textNode = document.createTextNode("");
            input.appendChild(textNode);
          }
          range.setStart(input.firstChild, begin < input.inputmask._valueGet().length ? begin : input.inputmask._valueGet().length),
            range.setEnd(input.firstChild, end < input.inputmask._valueGet().length ? end : input.inputmask._valueGet().length),
            range.collapse(!0);
          var sel = window.getSelection();
          sel.removeAllRanges(), sel.addRange(range);
        } else input.createTextRange && (range = input.createTextRange(), range.collapse(!0),
          range.moveEnd("character", end), range.moveStart("character", begin), range.select());
        renderColorMask(input, {
          begin: begin,
          end: end
        });
      }
    }

    function determineLastRequiredPosition(returnDefinition) {
      var pos, testPos, buffer = getBuffer(),
        bl = buffer.length,
        lvp = getLastValidPosition(),
        positions = {},
        lvTest = getMaskSet().validPositions[lvp],
        ndxIntlzr = lvTest !== undefined ? lvTest.locator.slice() : undefined;
      for (pos = lvp + 1; pos < buffer.length; pos++) testPos = getTestTemplate(pos, ndxIntlzr, pos - 1),
        ndxIntlzr = testPos.locator.slice(), positions[pos] = $.extend(!0, {}, testPos);
      var lvTestAlt = lvTest && lvTest.alternation !== undefined ? lvTest.locator[lvTest.alternation] : undefined;
      for (pos = bl - 1; pos > lvp && (testPos = positions[pos], (testPos.match.optionality || testPos.match.optionalQuantifier && testPos.match.newBlockMarker || lvTestAlt && (lvTestAlt !== positions[pos].locator[lvTest.alternation] && null != testPos.match.fn || null === testPos.match.fn && testPos.locator[lvTest.alternation] && checkAlternationMatch(testPos.locator[lvTest.alternation].toString().split(","), lvTestAlt.toString().split(",")) && "" !== getTests(pos)[0].def)) && buffer[pos] === getPlaceholder(pos, testPos.match)); pos--) bl--;
      return returnDefinition ? {
        l: bl,
        def: positions[bl] ? positions[bl].match : undefined
      } : bl;
    }

    function clearOptionalTail(buffer) {
      for (var validPos, rl = determineLastRequiredPosition(), bl = buffer.length, lv = getMaskSet().validPositions[getLastValidPosition()]; rl < bl && !isMask(rl, !0) && (validPos = lv !== undefined ? getTestTemplate(rl, lv.locator.slice(""), lv) : getTest(rl)) && !0 !== validPos.match.optionality && (!0 !== validPos.match.optionalQuantifier && !0 !== validPos.match.newBlockMarker || rl + 1 === bl && "" === (lv !== undefined ? getTestTemplate(rl + 1, lv.locator.slice(""), lv) : getTest(rl + 1)).match.def);) rl++;
      for (;
        (validPos = getMaskSet().validPositions[rl - 1]) && validPos && validPos.match.optionality && validPos.input === opts.skipOptionalPartCharacter;) rl--;
      return buffer.splice(rl), buffer;
    }

    function isComplete(buffer) {
      if ($.isFunction(opts.isComplete)) return opts.isComplete(buffer, opts);
      if ("*" === opts.repeat) return undefined;
      var complete = !1,
        lrp = determineLastRequiredPosition(!0),
        aml = seekPrevious(lrp.l);
      if (lrp.def === undefined || lrp.def.newBlockMarker || lrp.def.optionality || lrp.def.optionalQuantifier) {
        complete = !0;
        for (var i = 0; i <= aml; i++) {
          var test = getTestTemplate(i).match;
          if (null !== test.fn && getMaskSet().validPositions[i] === undefined && !0 !== test.optionality && !0 !== test.optionalQuantifier || null === test.fn && buffer[i] !== getPlaceholder(i, test)) {
            complete = !1;
            break;
          }
        }
      }
      return complete;
    }

    function handleRemove(input, k, pos, strict, fromIsValid) {
      if ((opts.numericInput || isRTL) && (k === Inputmask.keyCode.BACKSPACE ? k = Inputmask.keyCode.DELETE : k === Inputmask.keyCode.DELETE && (k = Inputmask.keyCode.BACKSPACE),
          isRTL)) {
        var pend = pos.end;
        pos.end = pos.begin, pos.begin = pend;
      }
      k === Inputmask.keyCode.BACKSPACE && (pos.end - pos.begin < 1 || !1 === opts.insertMode) ? (pos.begin = seekPrevious(pos.begin),
          getMaskSet().validPositions[pos.begin] !== undefined && getMaskSet().validPositions[pos.begin].input === opts.groupSeparator && pos.begin--) : k === Inputmask.keyCode.DELETE && pos.begin === pos.end && (pos.end = isMask(pos.end, !0) && getMaskSet().validPositions[pos.end] && getMaskSet().validPositions[pos.end].input !== opts.radixPoint ? pos.end + 1 : seekNext(pos.end) + 1,
          getMaskSet().validPositions[pos.begin] !== undefined && getMaskSet().validPositions[pos.begin].input === opts.groupSeparator && pos.end++),
        stripValidPositions(pos.begin, pos.end, !1, strict), !0 !== strict && function () {
          if (opts.keepStatic) {
            for (var validInputs = [], lastAlt = getLastValidPosition(-1, !0), positionsClone = $.extend(!0, {}, getMaskSet().validPositions), prevAltPos = getMaskSet().validPositions[lastAlt]; lastAlt >= 0; lastAlt--) {
              var altPos = getMaskSet().validPositions[lastAlt];
              if (altPos) {
                if (!0 !== altPos.generatedInput && /[0-9a-bA-Z]/.test(altPos.input) && validInputs.push(altPos.input),
                  delete getMaskSet().validPositions[lastAlt], altPos.alternation !== undefined && altPos.locator[altPos.alternation] !== prevAltPos.locator[altPos.alternation]) break;
                prevAltPos = altPos;
              }
            }
            if (lastAlt > -1)
              for (getMaskSet().p = seekNext(getLastValidPosition(-1, !0)); validInputs.length > 0;) {
                var keypress = new $.Event("keypress");
                keypress.which = validInputs.pop().charCodeAt(0), EventHandlers.keypressEvent.call(input, keypress, !0, !1, !1, getMaskSet().p);
              } else getMaskSet().validPositions = $.extend(!0, {}, positionsClone);
          }
        }();
      var lvp = getLastValidPosition(pos.begin, !0);
      if (lvp < pos.begin) getMaskSet().p = seekNext(lvp);
      else if (!0 !== strict && (getMaskSet().p = pos.begin,
          !0 !== fromIsValid))
        for (; getMaskSet().p < lvp && getMaskSet().validPositions[getMaskSet().p] === undefined;) getMaskSet().p++;
    }

    function initializeColorMask(input) {
      function findCaretPos(clientx) {
        var caretPos, e = document.createElement("span");
        for (var style in computedStyle) isNaN(style) && -1 !== style.indexOf("font") && (e.style[style] = computedStyle[style]);
        e.style.textTransform = computedStyle.textTransform, e.style.letterSpacing = computedStyle.letterSpacing,
          e.style.position = "absolute", e.style.height = "auto", e.style.width = "auto",
          e.style.visibility = "hidden", e.style.whiteSpace = "nowrap", document.body.appendChild(e);
        var itl, inputText = input.inputmask._valueGet(),
          previousWidth = 0;
        for (caretPos = 0, itl = inputText.length; caretPos <= itl; caretPos++) {
          if (e.innerHTML += inputText.charAt(caretPos) || "_", e.offsetWidth >= clientx) {
            var offset1 = clientx - previousWidth,
              offset2 = e.offsetWidth - clientx;
            e.innerHTML = inputText.charAt(caretPos), offset1 -= e.offsetWidth / 3, caretPos = offset1 < offset2 ? caretPos - 1 : caretPos;
            break;
          }
          previousWidth = e.offsetWidth;
        }
        return document.body.removeChild(e), caretPos;
      }
      var computedStyle = (input.ownerDocument.defaultView || window).getComputedStyle(input, null),
        template = document.createElement("div");
      template.style.width = computedStyle.width, template.style.textAlign = computedStyle.textAlign,
        colorMask = document.createElement("div"), colorMask.className = "im-colormask",
        input.parentNode.insertBefore(colorMask, input), input.parentNode.removeChild(input),
        colorMask.appendChild(template), colorMask.appendChild(input), input.style.left = template.offsetLeft + "px",
        $(input).on("click", function (e) {
          return caret(input, findCaretPos(e.clientX)), EventHandlers.clickEvent.call(input, [e]);
        }), $(input).on("keydown", function (e) {
          e.shiftKey || !1 === opts.insertMode || setTimeout(function () {
            renderColorMask(input);
          }, 0);
        });
    }

    function renderColorMask(input, caretPos, clear) {
      function handleStatic() {
        isStatic || null !== test.fn && testPos.input !== undefined ? isStatic && (null !== test.fn && testPos.input !== undefined || "" === test.def) && (isStatic = !1,
          maskTemplate += "</span>") : (isStatic = !0, maskTemplate += "<span class='im-static'>");
      }

      function handleCaret(force) {
        !0 !== force && pos !== caretPos.begin || document.activeElement !== input || (maskTemplate += "<span class='im-caret' style='border-right-width: 1px;border-right-style: solid;'></span>");
      }
      var test, testPos, ndxIntlzr, maskTemplate = "",
        isStatic = !1,
        pos = 0;
      if (colorMask !== undefined) {
        var buffer = getBuffer();
        if (caretPos === undefined ? caretPos = caret(input) : caretPos.begin === undefined && (caretPos = {
            begin: caretPos,
            end: caretPos
          }), !0 !== clear) {
          var lvp = getLastValidPosition();
          do {
            handleCaret(), getMaskSet().validPositions[pos] ? (testPos = getMaskSet().validPositions[pos],
              test = testPos.match, ndxIntlzr = testPos.locator.slice(), handleStatic(), maskTemplate += buffer[pos]) : (testPos = getTestTemplate(pos, ndxIntlzr, pos - 1),
              test = testPos.match, ndxIntlzr = testPos.locator.slice(), (!1 === opts.jitMasking || pos < lvp || "number" == typeof opts.jitMasking && isFinite(opts.jitMasking) && opts.jitMasking > pos) && (handleStatic(),
                maskTemplate += getPlaceholder(pos, test))), pos++;
          } while ((maxLength === undefined || pos < maxLength) && (null !== test.fn || "" !== test.def) || lvp > pos || isStatic); -
          1 === maskTemplate.indexOf("im-caret") && handleCaret(!0), isStatic && handleStatic();
        }
        var template = colorMask.getElementsByTagName("div")[0];
        template.innerHTML = maskTemplate, input.inputmask.positionColorMask(input, template);
      }
    }
    maskset = maskset || this.maskset, opts = opts || this.opts;
    var undoValue, $el, maxLength, colorMask, inputmask = this,
      el = this.el,
      isRTL = this.isRTL,
      skipKeyPressEvent = !1,
      skipInputEvent = !1,
      ignorable = !1,
      mouseEnter = !1,
      EventRuler = {
        on: function (input, eventName, eventHandler) {
          var ev = function (e) {
            if (this.inputmask === undefined && "FORM" !== this.nodeName) {
              var imOpts = $.data(this, "_inputmask_opts");
              imOpts ? new Inputmask(imOpts).mask(this) : EventRuler.off(this);
            } else {
              if ("setvalue" === e.type || "FORM" === this.nodeName || !(this.disabled || this.readOnly && !("keydown" === e.type && e.ctrlKey && 67 === e.keyCode || !1 === opts.tabThrough && e.keyCode === Inputmask.keyCode.TAB))) {
                switch (e.type) {
                  case "input":
                    if (!0 === skipInputEvent) return skipInputEvent = !1, e.preventDefault();
                    break;

                  case "keydown":
                    skipKeyPressEvent = !1, skipInputEvent = !1;
                    break;

                  case "keypress":
                    if (!0 === skipKeyPressEvent) return e.preventDefault();
                    skipKeyPressEvent = !0;
                    break;

                  case "click":
                    if (iemobile || iphone) {
                      var that = this,
                        args = arguments;
                      return setTimeout(function () {
                        eventHandler.apply(that, args);
                      }, 0), !1;
                    }
                }
                var returnVal = eventHandler.apply(this, arguments);
                return !1 === returnVal && (e.preventDefault(), e.stopPropagation()), returnVal;
              }
              e.preventDefault();
            }
          };
          input.inputmask.events[eventName] = input.inputmask.events[eventName] || [], input.inputmask.events[eventName].push(ev),
            -1 !== $.inArray(eventName, ["submit", "reset"]) ? null !== input.form && $(input.form).on(eventName, ev) : $(input).on(eventName, ev);
        },
        off: function (input, event) {
          if (input.inputmask && input.inputmask.events) {
            var events;
            event ? (events = [], events[event] = input.inputmask.events[event]) : events = input.inputmask.events,
              $.each(events, function (eventName, evArr) {
                for (; evArr.length > 0;) {
                  var ev = evArr.pop(); -
                  1 !== $.inArray(eventName, ["submit", "reset"]) ? null !== input.form && $(input.form).off(eventName, ev) : $(input).off(eventName, ev);
                }
                delete input.inputmask.events[eventName];
              });
          }
        }
      },
      EventHandlers = {
        keydownEvent: function (e) {
          var input = this,
            $input = $(input),
            k = e.keyCode,
            pos = caret(input);
          if (k === Inputmask.keyCode.BACKSPACE || k === Inputmask.keyCode.DELETE || iphone && k === Inputmask.keyCode.BACKSPACE_SAFARI || e.ctrlKey && k === Inputmask.keyCode.X && ! function (eventName) {
              var el = document.createElement("input"),
                evName = "on" + eventName,
                isSupported = evName in el;
              return isSupported || (el.setAttribute(evName, "return;"), isSupported = "function" == typeof el[evName]),
                el = null, isSupported;
            }("cut")) e.preventDefault(), handleRemove(input, k, pos), writeBuffer(input, getBuffer(!0), getMaskSet().p, e, input.inputmask._valueGet() !== getBuffer().join("")),
            input.inputmask._valueGet() === getBufferTemplate().join("") ? $input.trigger("cleared") : !0 === isComplete(getBuffer()) && $input.trigger("complete");
          else if (k === Inputmask.keyCode.END || k === Inputmask.keyCode.PAGE_DOWN) {
            e.preventDefault();
            var caretPos = seekNext(getLastValidPosition());
            opts.insertMode || caretPos !== getMaskSet().maskLength || e.shiftKey || caretPos--,
              caret(input, e.shiftKey ? pos.begin : caretPos, caretPos, !0);
          } else k === Inputmask.keyCode.HOME && !e.shiftKey || k === Inputmask.keyCode.PAGE_UP ? (e.preventDefault(),
            caret(input, 0, e.shiftKey ? pos.begin : 0, !0)) : (opts.undoOnEscape && k === Inputmask.keyCode.ESCAPE || 90 === k && e.ctrlKey) && !0 !== e.altKey ? (checkVal(input, !0, !1, undoValue.split("")),
            $input.trigger("click")) : k !== Inputmask.keyCode.INSERT || e.shiftKey || e.ctrlKey ? !0 === opts.tabThrough && k === Inputmask.keyCode.TAB ? (!0 === e.shiftKey ? (null === getTest(pos.begin).match.fn && (pos.begin = seekNext(pos.begin)),
              pos.end = seekPrevious(pos.begin, !0), pos.begin = seekPrevious(pos.end, !0)) : (pos.begin = seekNext(pos.begin, !0),
              pos.end = seekNext(pos.begin, !0), pos.end < getMaskSet().maskLength && pos.end--),
            pos.begin < getMaskSet().maskLength && (e.preventDefault(), caret(input, pos.begin, pos.end))) : e.shiftKey || !1 === opts.insertMode && (k === Inputmask.keyCode.RIGHT ? setTimeout(function () {
            var caretPos = caret(input);
            caret(input, caretPos.begin);
          }, 0) : k === Inputmask.keyCode.LEFT && setTimeout(function () {
            var caretPos = caret(input);
            caret(input, isRTL ? caretPos.begin + 1 : caretPos.begin - 1);
          }, 0)) : (opts.insertMode = !opts.insertMode, caret(input, opts.insertMode || pos.begin !== getMaskSet().maskLength ? pos.begin : pos.begin - 1));
          opts.onKeyDown.call(this, e, getBuffer(), caret(input).begin, opts), ignorable = -1 !== $.inArray(k, opts.ignorables);
        },
        keypressEvent: function (e, checkval, writeOut, strict, ndx) {
          var input = this,
            $input = $(input),
            k = e.which || e.charCode || e.keyCode;
          if (!(!0 === checkval || e.ctrlKey && e.altKey) && (e.ctrlKey || e.metaKey || ignorable)) return k === Inputmask.keyCode.ENTER && undoValue !== getBuffer().join("") && (undoValue = getBuffer().join(""),
            setTimeout(function () {
              $input.trigger("change");
            }, 0)), !0;
          if (k) {
            46 === k && !1 === e.shiftKey && "" !== opts.radixPoint && (k = opts.radixPoint.charCodeAt(0));
            var forwardPosition, pos = checkval ? {
                begin: ndx,
                end: ndx
              } : caret(input),
              c = String.fromCharCode(k);
            getMaskSet().writeOutBuffer = !0;
            var valResult = isValid(pos, c, strict);
            if (!1 !== valResult && (resetMaskSet(!0), forwardPosition = valResult.caret !== undefined ? valResult.caret : checkval ? valResult.pos + 1 : seekNext(valResult.pos),
                getMaskSet().p = forwardPosition), !1 !== writeOut && (setTimeout(function () {
                opts.onKeyValidation.call(input, k, valResult, opts);
              }, 0), getMaskSet().writeOutBuffer && !1 !== valResult)) {
              var buffer = getBuffer();
              writeBuffer(input, buffer, opts.numericInput && valResult.caret === undefined ? seekPrevious(forwardPosition) : forwardPosition, e, !0 !== checkval),
                !0 !== checkval && setTimeout(function () {
                  !0 === isComplete(buffer) && $input.trigger("complete");
                }, 0);
            }
            if (e.preventDefault(), checkval) return !1 !== valResult && (valResult.forwardPosition = forwardPosition),
              valResult;
          }
        },
        pasteEvent: function (e) {
          var tempValue, input = this,
            ev = e.originalEvent || e,
            $input = $(input),
            inputValue = input.inputmask._valueGet(!0),
            caretPos = caret(input);
          isRTL && (tempValue = caretPos.end, caretPos.end = caretPos.begin, caretPos.begin = tempValue);
          var valueBeforeCaret = inputValue.substr(0, caretPos.begin),
            valueAfterCaret = inputValue.substr(caretPos.end, inputValue.length);
          if (valueBeforeCaret === (isRTL ? getBufferTemplate().reverse() : getBufferTemplate()).slice(0, caretPos.begin).join("") && (valueBeforeCaret = ""),
            valueAfterCaret === (isRTL ? getBufferTemplate().reverse() : getBufferTemplate()).slice(caretPos.end).join("") && (valueAfterCaret = ""),
            isRTL && (tempValue = valueBeforeCaret, valueBeforeCaret = valueAfterCaret, valueAfterCaret = tempValue),
            window.clipboardData && window.clipboardData.getData) inputValue = valueBeforeCaret + window.clipboardData.getData("Text") + valueAfterCaret;
          else {
            if (!ev.clipboardData || !ev.clipboardData.getData) return !0;
            inputValue = valueBeforeCaret + ev.clipboardData.getData("text/plain") + valueAfterCaret;
          }
          var pasteValue = inputValue;
          if ($.isFunction(opts.onBeforePaste)) {
            if (!1 === (pasteValue = opts.onBeforePaste.call(inputmask, inputValue, opts))) return e.preventDefault();
            pasteValue || (pasteValue = inputValue);
          }
          return checkVal(input, !1, !1, isRTL ? pasteValue.split("").reverse() : pasteValue.toString().split("")),
            writeBuffer(input, getBuffer(), seekNext(getLastValidPosition()), e, undoValue !== getBuffer().join("")),
            !0 === isComplete(getBuffer()) && $input.trigger("complete"), e.preventDefault();
        },
        inputFallBackEvent: function (e) {
          var input = this,
            inputValue = input.inputmask._valueGet();
          if (getBuffer().join("") !== inputValue) {
            var caretPos = caret(input);
            if (inputValue = function (input, inputValue, caretPos) {
                return "." === inputValue.charAt(caretPos.begin - 1) && "" !== opts.radixPoint && (inputValue = inputValue.split(""),
                    inputValue[caretPos.begin - 1] = opts.radixPoint.charAt(0), inputValue = inputValue.join("")),
                  inputValue;
              }(input, inputValue, caretPos), inputValue = function (input, inputValue, caretPos) {
                if (iemobile) {
                  var inputChar = inputValue.replace(getBuffer().join(""), "");
                  if (1 === inputChar.length) {
                    var iv = inputValue.split("");
                    iv.splice(caretPos.begin, 0, inputChar), inputValue = iv.join("");
                  }
                }
                return inputValue;
              }(input, inputValue, caretPos), caretPos.begin > inputValue.length && (caret(input, inputValue.length),
                caretPos = caret(input)), getBuffer().join("") !== inputValue) {
              var buffer = getBuffer().join(""),
                offset = inputValue.length > buffer.length ? -1 : 0,
                frontPart = inputValue.substr(0, caretPos.begin),
                backPart = inputValue.substr(caretPos.begin),
                frontBufferPart = buffer.substr(0, caretPos.begin + offset),
                backBufferPart = buffer.substr(caretPos.begin + offset),
                selection = caretPos,
                entries = "",
                isEntry = !1;
              if (frontPart !== frontBufferPart) {
                for (var fpl = (isEntry = frontPart.length >= frontBufferPart.length) ? frontPart.length : frontBufferPart.length, i = 0; frontPart.charAt(i) === frontBufferPart.charAt(i) && i < fpl; i++);
                isEntry && (0 === offset && (selection.begin = i), entries += frontPart.slice(i, selection.end));
              }
              if (backPart !== backBufferPart && (backPart.length > backBufferPart.length ? entries += backPart.slice(0, 1) : backPart.length < backBufferPart.length && (selection.end += backBufferPart.length - backPart.length,
                  isEntry || "" === opts.radixPoint || "" !== backPart || frontPart.charAt(selection.begin + offset - 1) !== opts.radixPoint || (selection.begin--,
                    entries = opts.radixPoint))), writeBuffer(input, getBuffer(), {
                  begin: selection.begin + offset,
                  end: selection.end + offset
                }), entries.length > 0) $.each(entries.split(""), function (ndx, entry) {
                var keypress = new $.Event("keypress");
                keypress.which = entry.charCodeAt(0), ignorable = !1, EventHandlers.keypressEvent.call(input, keypress);
              });
              else {
                selection.begin === selection.end - 1 && (selection.begin = seekPrevious(selection.begin + 1),
                  selection.begin === selection.end - 1 ? caret(input, selection.begin) : caret(input, selection.begin, selection.end));
                var keydown = new $.Event("keydown");
                keydown.keyCode = Inputmask.keyCode.DELETE, EventHandlers.keydownEvent.call(input, keydown);
              }
              e.preventDefault();
            }
          }
        },
        setValueEvent: function (e) {
          this.inputmask.refreshValue = !1;
          var input = this,
            value = input.inputmask._valueGet(!0);
          $.isFunction(opts.onBeforeMask) && (value = opts.onBeforeMask.call(inputmask, value, opts) || value),
            value = value.split(""), checkVal(input, !0, !1, isRTL ? value.reverse() : value),
            undoValue = getBuffer().join(""), (opts.clearMaskOnLostFocus || opts.clearIncomplete) && input.inputmask._valueGet() === getBufferTemplate().join("") && input.inputmask._valueSet("");
        },
        focusEvent: function (e) {
          var input = this,
            nptValue = input.inputmask._valueGet();
          opts.showMaskOnFocus && (!opts.showMaskOnHover || opts.showMaskOnHover && "" === nptValue) && (input.inputmask._valueGet() !== getBuffer().join("") ? writeBuffer(input, getBuffer(), seekNext(getLastValidPosition())) : !1 === mouseEnter && caret(input, seekNext(getLastValidPosition()))),
            !0 === opts.positionCaretOnTab && !1 === mouseEnter && "" !== nptValue && (writeBuffer(input, getBuffer(), caret(input)),
              EventHandlers.clickEvent.apply(input, [e, !0])), undoValue = getBuffer().join("");
        },
        mouseleaveEvent: function (e) {
          var input = this;
          if (mouseEnter = !1, opts.clearMaskOnLostFocus && document.activeElement !== input) {
            var buffer = getBuffer().slice(),
              nptValue = input.inputmask._valueGet();
            nptValue !== input.getAttribute("placeholder") && "" !== nptValue && (-1 === getLastValidPosition() && nptValue === getBufferTemplate().join("") ? buffer = [] : clearOptionalTail(buffer),
              writeBuffer(input, buffer));
          }
        },
        clickEvent: function (e, tabbed) {
          function doRadixFocus(clickPos) {
            if ("" !== opts.radixPoint) {
              var vps = getMaskSet().validPositions;
              if (vps[clickPos] === undefined || vps[clickPos].input === getPlaceholder(clickPos)) {
                if (clickPos < seekNext(-1)) return !0;
                var radixPos = $.inArray(opts.radixPoint, getBuffer());
                if (-1 !== radixPos) {
                  for (var vp in vps)
                    if (radixPos < vp && vps[vp].input !== getPlaceholder(vp)) return !1;
                  return !0;
                }
              }
            }
            return !1;
          }
          var input = this;
          setTimeout(function () {
            if (document.activeElement === input) {
              var selectedCaret = caret(input);
              if (tabbed && (isRTL ? selectedCaret.end = selectedCaret.begin : selectedCaret.begin = selectedCaret.end),
                selectedCaret.begin === selectedCaret.end) switch (opts.positionCaretOnClick) {
                case "none":
                  break;

                case "radixFocus":
                  if (doRadixFocus(selectedCaret.begin)) {
                    var radixPos = getBuffer().join("").indexOf(opts.radixPoint);
                    caret(input, opts.numericInput ? seekNext(radixPos) : radixPos);
                    break;
                  }

                  default:
                    var clickPosition = selectedCaret.begin,
                      lvclickPosition = getLastValidPosition(clickPosition, !0),
                      lastPosition = seekNext(lvclickPosition);
                    if (clickPosition < lastPosition) caret(input, isMask(clickPosition, !0) || isMask(clickPosition - 1, !0) ? clickPosition : seekNext(clickPosition));
                    else {
                      var lvp = getMaskSet().validPositions[lvclickPosition],
                        tt = getTestTemplate(lastPosition, lvp ? lvp.match.locator : undefined, lvp),
                        placeholder = getPlaceholder(lastPosition, tt.match);
                      if ("" !== placeholder && getBuffer()[lastPosition] !== placeholder && !0 !== tt.match.optionalQuantifier && !0 !== tt.match.newBlockMarker || !isMask(lastPosition, !0) && tt.match.def === placeholder) {
                        var newPos = seekNext(lastPosition);
                        (clickPosition >= newPos || clickPosition === lastPosition) && (lastPosition = newPos);
                      }
                      caret(input, lastPosition);
                    }
              }
            }
          }, 0);
        },
        dblclickEvent: function (e) {
          var input = this;
          setTimeout(function () {
            caret(input, 0, seekNext(getLastValidPosition()));
          }, 0);
        },
        cutEvent: function (e) {
          var input = this,
            $input = $(input),
            pos = caret(input),
            ev = e.originalEvent || e,
            clipboardData = window.clipboardData || ev.clipboardData,
            clipData = isRTL ? getBuffer().slice(pos.end, pos.begin) : getBuffer().slice(pos.begin, pos.end);
          clipboardData.setData("text", isRTL ? clipData.reverse().join("") : clipData.join("")),
            document.execCommand && document.execCommand("copy"), handleRemove(input, Inputmask.keyCode.DELETE, pos),
            writeBuffer(input, getBuffer(), getMaskSet().p, e, undoValue !== getBuffer().join("")),
            input.inputmask._valueGet() === getBufferTemplate().join("") && $input.trigger("cleared");
        },
        blurEvent: function (e) {
          var $input = $(this),
            input = this;
          if (input.inputmask) {
            var nptValue = input.inputmask._valueGet(),
              buffer = getBuffer().slice();
            "" !== nptValue && (opts.clearMaskOnLostFocus && (-1 === getLastValidPosition() && nptValue === getBufferTemplate().join("") ? buffer = [] : clearOptionalTail(buffer)),
              !1 === isComplete(buffer) && (setTimeout(function () {
                $input.trigger("incomplete");
              }, 0), opts.clearIncomplete && (resetMaskSet(), buffer = opts.clearMaskOnLostFocus ? [] : getBufferTemplate().slice())),
              writeBuffer(input, buffer, undefined, e)), undoValue !== getBuffer().join("") && (undoValue = buffer.join(""),
              $input.trigger("change"));
          }
        },
        mouseenterEvent: function (e) {
          var input = this;
          mouseEnter = !0, document.activeElement !== input && opts.showMaskOnHover && input.inputmask._valueGet() !== getBuffer().join("") && writeBuffer(input, getBuffer());
        },
        submitEvent: function (e) {
          undoValue !== getBuffer().join("") && $el.trigger("change"), opts.clearMaskOnLostFocus && -1 === getLastValidPosition() && el.inputmask._valueGet && el.inputmask._valueGet() === getBufferTemplate().join("") && el.inputmask._valueSet(""),
            opts.removeMaskOnSubmit && (el.inputmask._valueSet(el.inputmask.unmaskedvalue(), !0),
              setTimeout(function () {
                writeBuffer(el, getBuffer());
              }, 0));
        },
        resetEvent: function (e) {
          el.inputmask.refreshValue = !0, setTimeout(function () {
            $el.trigger("setvalue");
          }, 0);
        }
      };
    Inputmask.prototype.positionColorMask = function (input, template) {
      input.style.left = template.offsetLeft + "px";
    };
    var valueBuffer;
    if (actionObj !== undefined) switch (actionObj.action) {
      case "isComplete":
        return el = actionObj.el, isComplete(getBuffer());

      case "unmaskedvalue":
        return el !== undefined && actionObj.value === undefined || (valueBuffer = actionObj.value,
            valueBuffer = ($.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(inputmask, valueBuffer, opts) || valueBuffer : valueBuffer).split(""),
            checkVal(undefined, !1, !1, isRTL ? valueBuffer.reverse() : valueBuffer), $.isFunction(opts.onBeforeWrite) && opts.onBeforeWrite.call(inputmask, undefined, getBuffer(), 0, opts)),
          unmaskedvalue(el);

      case "mask":
        ! function (elem) {
          EventRuler.off(elem);
          var isSupported = function (input, opts) {
            var elementType = input.getAttribute("type"),
              isSupported = "INPUT" === input.tagName && -1 !== $.inArray(elementType, opts.supportsInputType) || input.isContentEditable || "TEXTAREA" === input.tagName;
            if (!isSupported)
              if ("INPUT" === input.tagName) {
                var el = document.createElement("input");
                el.setAttribute("type", elementType), isSupported = "text" === el.type, el = null;
              } else isSupported = "partial";
            return !1 !== isSupported ? function (npt) {
              function getter() {
                return this.inputmask ? this.inputmask.opts.autoUnmask ? this.inputmask.unmaskedvalue() : -1 !== getLastValidPosition() || !0 !== opts.nullable ? document.activeElement === this && opts.clearMaskOnLostFocus ? (isRTL ? clearOptionalTail(getBuffer().slice()).reverse() : clearOptionalTail(getBuffer().slice())).join("") : valueGet.call(this) : "" : valueGet.call(this);
              }

              function setter(value) {
                valueSet.call(this, value), this.inputmask && $(this).trigger("setvalue");
              }
              var valueGet, valueSet;
              if (!npt.inputmask.__valueGet) {
                if (!0 !== opts.noValuePatching) {
                  if (Object.getOwnPropertyDescriptor) {
                    "function" != typeof Object.getPrototypeOf && (Object.getPrototypeOf = "object" == typeof "test".__proto__ ? function (object) {
                      return object.__proto__;
                    } : function (object) {
                      return object.constructor.prototype;
                    });
                    var valueProperty = Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(npt), "value") : undefined;
                    valueProperty && valueProperty.get && valueProperty.set ? (valueGet = valueProperty.get,
                      valueSet = valueProperty.set, Object.defineProperty(npt, "value", {
                        get: getter,
                        set: setter,
                        configurable: !0
                      })) : "INPUT" !== npt.tagName && (valueGet = function () {
                      return this.textContent;
                    }, valueSet = function (value) {
                      this.textContent = value;
                    }, Object.defineProperty(npt, "value", {
                      get: getter,
                      set: setter,
                      configurable: !0
                    }));
                  } else document.__lookupGetter__ && npt.__lookupGetter__("value") && (valueGet = npt.__lookupGetter__("value"),
                    valueSet = npt.__lookupSetter__("value"), npt.__defineGetter__("value", getter),
                    npt.__defineSetter__("value", setter));
                  npt.inputmask.__valueGet = valueGet, npt.inputmask.__valueSet = valueSet;
                }
                npt.inputmask._valueGet = function (overruleRTL) {
                  return isRTL && !0 !== overruleRTL ? valueGet.call(this.el).split("").reverse().join("") : valueGet.call(this.el);
                }, npt.inputmask._valueSet = function (value, overruleRTL) {
                  valueSet.call(this.el, null === value || value === undefined ? "" : !0 !== overruleRTL && isRTL ? value.split("").reverse().join("") : value);
                }, valueGet === undefined && (valueGet = function () {
                  return this.value;
                }, valueSet = function (value) {
                  this.value = value;
                }, function (type) {
                  if ($.valHooks && ($.valHooks[type] === undefined || !0 !== $.valHooks[type].inputmaskpatch)) {
                    var valhookGet = $.valHooks[type] && $.valHooks[type].get ? $.valHooks[type].get : function (elem) {
                        return elem.value;
                      },
                      valhookSet = $.valHooks[type] && $.valHooks[type].set ? $.valHooks[type].set : function (elem, value) {
                        return elem.value = value, elem;
                      };
                    $.valHooks[type] = {
                      get: function (elem) {
                        if (elem.inputmask) {
                          if (elem.inputmask.opts.autoUnmask) return elem.inputmask.unmaskedvalue();
                          var result = valhookGet(elem);
                          return -1 !== getLastValidPosition(undefined, undefined, elem.inputmask.maskset.validPositions) || !0 !== opts.nullable ? result : "";
                        }
                        return valhookGet(elem);
                      },
                      set: function (elem, value) {
                        var result, $elem = $(elem);
                        return result = valhookSet(elem, value), elem.inputmask && $elem.trigger("setvalue"),
                          result;
                      },
                      inputmaskpatch: !0
                    };
                  }
                }(npt.type), function (npt) {
                  EventRuler.on(npt, "mouseenter", function (event) {
                    var $input = $(this);
                    this.inputmask._valueGet() !== getBuffer().join("") && $input.trigger("setvalue");
                  });
                }(npt));
              }
            }(input) : input.inputmask = undefined, isSupported;
          }(elem, opts);
          if (!1 !== isSupported && (el = elem, $el = $(el), maxLength = el !== undefined ? el.maxLength : undefined,
              -1 === maxLength && (maxLength = undefined), !0 === opts.colorMask && initializeColorMask(el),
              android && (el.hasOwnProperty("inputmode") && (el.inputmode = opts.inputmode, el.setAttribute("inputmode", opts.inputmode)),
                "rtfm" === opts.androidHack && (!0 !== opts.colorMask && initializeColorMask(el),
                  el.type = "password")), !0 === isSupported && (EventRuler.on(el, "submit", EventHandlers.submitEvent),
                EventRuler.on(el, "reset", EventHandlers.resetEvent), EventRuler.on(el, "mouseenter", EventHandlers.mouseenterEvent),
                EventRuler.on(el, "blur", EventHandlers.blurEvent), EventRuler.on(el, "focus", EventHandlers.focusEvent),
                EventRuler.on(el, "mouseleave", EventHandlers.mouseleaveEvent), !0 !== opts.colorMask && EventRuler.on(el, "click", EventHandlers.clickEvent),
                EventRuler.on(el, "dblclick", EventHandlers.dblclickEvent), EventRuler.on(el, "paste", EventHandlers.pasteEvent),
                EventRuler.on(el, "dragdrop", EventHandlers.pasteEvent), EventRuler.on(el, "drop", EventHandlers.pasteEvent),
                EventRuler.on(el, "cut", EventHandlers.cutEvent), EventRuler.on(el, "complete", opts.oncomplete),
                EventRuler.on(el, "incomplete", opts.onincomplete), EventRuler.on(el, "cleared", opts.oncleared),
                android || !0 === opts.inputEventOnly ? el.removeAttribute("maxLength") : (EventRuler.on(el, "keydown", EventHandlers.keydownEvent),
                  EventRuler.on(el, "keypress", EventHandlers.keypressEvent)), EventRuler.on(el, "compositionstart", $.noop),
                EventRuler.on(el, "compositionupdate", $.noop), EventRuler.on(el, "compositionend", $.noop),
                EventRuler.on(el, "keyup", $.noop), EventRuler.on(el, "input", EventHandlers.inputFallBackEvent),
                EventRuler.on(el, "beforeinput", $.noop)), EventRuler.on(el, "setvalue", EventHandlers.setValueEvent),
              undoValue = getBufferTemplate().join(""), "" !== el.inputmask._valueGet(!0) || !1 === opts.clearMaskOnLostFocus || document.activeElement === el)) {
            var initialValue = $.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(inputmask, el.inputmask._valueGet(!0), opts) || el.inputmask._valueGet(!0) : el.inputmask._valueGet(!0);
            "" !== initialValue && checkVal(el, !0, !1, isRTL ? initialValue.split("").reverse() : initialValue.split(""));
            var buffer = getBuffer().slice();
            undoValue = buffer.join(""), !1 === isComplete(buffer) && opts.clearIncomplete && resetMaskSet(),
              opts.clearMaskOnLostFocus && document.activeElement !== el && (-1 === getLastValidPosition() ? buffer = [] : clearOptionalTail(buffer)),
              writeBuffer(el, buffer), document.activeElement === el && caret(el, seekNext(getLastValidPosition()));
          }
        }(el);
        break;

      case "format":
        return valueBuffer = ($.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(inputmask, actionObj.value, opts) || actionObj.value : actionObj.value).split(""),
          checkVal(undefined, !0, !1, isRTL ? valueBuffer.reverse() : valueBuffer), actionObj.metadata ? {
            value: isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join(""),
            metadata: maskScope.call(this, {
              action: "getmetadata"
            }, maskset, opts)
          } : isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join("");

      case "isValid":
        actionObj.value ? (valueBuffer = actionObj.value.split(""), checkVal(undefined, !0, !0, isRTL ? valueBuffer.reverse() : valueBuffer)) : actionObj.value = getBuffer().join("");
        for (var buffer = getBuffer(), rl = determineLastRequiredPosition(), lmib = buffer.length - 1; lmib > rl && !isMask(lmib); lmib--);
        return buffer.splice(rl, lmib + 1 - rl), isComplete(buffer) && actionObj.value === getBuffer().join("");

      case "getemptymask":
        return getBufferTemplate().join("");

      case "remove":
        if (el && el.inputmask) {
          $el = $(el), el.inputmask._valueSet(opts.autoUnmask ? unmaskedvalue(el) : el.inputmask._valueGet(!0)),
            EventRuler.off(el);
          Object.getOwnPropertyDescriptor && Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(el), "value") && el.inputmask.__valueGet && Object.defineProperty(el, "value", {
            get: el.inputmask.__valueGet,
            set: el.inputmask.__valueSet,
            configurable: !0
          }) : document.__lookupGetter__ && el.__lookupGetter__("value") && el.inputmask.__valueGet && (el.__defineGetter__("value", el.inputmask.__valueGet),
            el.__defineSetter__("value", el.inputmask.__valueSet)), el.inputmask = undefined;
        }
        return el;

      case "getmetadata":
        if ($.isArray(maskset.metadata)) {
          var maskTarget = getMaskTemplate(!0, 0, !1).join("");
          return $.each(maskset.metadata, function (ndx, mtdt) {
            if (mtdt.mask === maskTarget) return maskTarget = mtdt, !1;
          }), maskTarget;
        }
        return maskset.metadata;
    }
  }
  var ua = navigator.userAgent,
    mobile = /mobile/i.test(ua),
    iemobile = /iemobile/i.test(ua),
    iphone = /iphone/i.test(ua) && !iemobile,
    android = /android/i.test(ua) && !iemobile;
  return Inputmask.prototype = {
    dataAttribute: "data-inputmask",
    defaults: {
      placeholder: "_",
      optionalmarker: {
        start: "[",
        end: "]"
      },
      quantifiermarker: {
        start: "{",
        end: "}"
      },
      groupmarker: {
        start: "(",
        end: ")"
      },
      alternatormarker: "|",
      escapeChar: "\\",
      mask: null,
      regex: null,
      oncomplete: $.noop,
      onincomplete: $.noop,
      oncleared: $.noop,
      repeat: 0,
      greedy: !0,
      autoUnmask: !1,
      removeMaskOnSubmit: !1,
      clearMaskOnLostFocus: !0,
      insertMode: !0,
      clearIncomplete: !1,
      alias: null,
      onKeyDown: $.noop,
      onBeforeMask: null,
      onBeforePaste: function (pastedValue, opts) {
        return $.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(this, pastedValue, opts) : pastedValue;
      },
      onBeforeWrite: null,
      onUnMask: null,
      showMaskOnFocus: !0,
      showMaskOnHover: !0,
      onKeyValidation: $.noop,
      skipOptionalPartCharacter: " ",
      numericInput: !1,
      rightAlign: !1,
      undoOnEscape: !0,
      radixPoint: "",
      radixPointDefinitionSymbol: undefined,
      groupSeparator: "",
      keepStatic: null,
      positionCaretOnTab: !0,
      tabThrough: !1,
      supportsInputType: ["text", "tel", "password"],
      ignorables: [8, 9, 13, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 0, 229],
      isComplete: null,
      canClearPosition: $.noop,
      preValidation: null,
      postValidation: null,
      staticDefinitionSymbol: undefined,
      jitMasking: !1,
      nullable: !0,
      inputEventOnly: !1,
      noValuePatching: !1,
      positionCaretOnClick: "lvp",
      casing: null,
      inputmode: "verbatim",
      colorMask: !1,
      androidHack: !1,
      importDataAttributes: !0
    },
    definitions: {
      "9": {
        validator: "[0-9\uff11-\uff19]",
        cardinality: 1,
        definitionSymbol: "*"
      },
      a: {
        validator: "[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",
        cardinality: 1,
        definitionSymbol: "*"
      },
      "*": {
        validator: "[0-9\uff11-\uff19A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",
        cardinality: 1
      }
    },
    aliases: {},
    masksCache: {},
    mask: function (elems) {
      function importAttributeOptions(npt, opts, userOptions, dataAttribute) {
        function importOption(option, optionData) {
          null !== (optionData = optionData !== undefined ? optionData : npt.getAttribute(dataAttribute + "-" + option)) && ("string" == typeof optionData && (0 === option.indexOf("on") ? optionData = window[optionData] : "false" === optionData ? optionData = !1 : "true" === optionData && (optionData = !0)),
            userOptions[option] = optionData);
        }
        if (!0 === opts.importDataAttributes) {
          var option, dataoptions, optionData, p, attrOptions = npt.getAttribute(dataAttribute);
          if (attrOptions && "" !== attrOptions && (attrOptions = attrOptions.replace(new RegExp("'", "g"), '"'),
              dataoptions = JSON.parse("{" + attrOptions + "}")), dataoptions) {
            optionData = undefined;
            for (p in dataoptions)
              if ("alias" === p.toLowerCase()) {
                optionData = dataoptions[p];
                break;
              }
          }
          importOption("alias", optionData), userOptions.alias && resolveAlias(userOptions.alias, userOptions, opts);
          for (option in opts) {
            if (dataoptions) {
              optionData = undefined;
              for (p in dataoptions)
                if (p.toLowerCase() === option.toLowerCase()) {
                  optionData = dataoptions[p];
                  break;
                }
            }
            importOption(option, optionData);
          }
        }
        return $.extend(!0, opts, userOptions), ("rtl" === npt.dir || opts.rightAlign) && (npt.style.textAlign = "right"),
          ("rtl" === npt.dir || opts.numericInput) && (npt.dir = "ltr", npt.removeAttribute("dir"),
            opts.isRTL = !0), opts;
      }
      var that = this;
      return "string" == typeof elems && (elems = document.getElementById(elems) || document.querySelectorAll(elems)),
        elems = elems.nodeName ? [elems] : elems, $.each(elems, function (ndx, el) {
          var scopedOpts = $.extend(!0, {}, that.opts);
          importAttributeOptions(el, scopedOpts, $.extend(!0, {}, that.userOptions), that.dataAttribute);
          var maskset = generateMaskSet(scopedOpts, that.noMasksCache);
          maskset !== undefined && (el.inputmask !== undefined && (el.inputmask.opts.autoUnmask = !0,
              el.inputmask.remove()), el.inputmask = new Inputmask(undefined, undefined, !0),
            el.inputmask.opts = scopedOpts, el.inputmask.noMasksCache = that.noMasksCache, el.inputmask.userOptions = $.extend(!0, {}, that.userOptions),
            el.inputmask.isRTL = scopedOpts.isRTL || scopedOpts.numericInput, el.inputmask.el = el,
            el.inputmask.maskset = maskset, $.data(el, "_inputmask_opts", scopedOpts), maskScope.call(el.inputmask, {
              action: "mask"
            }));
        }), elems && elems[0] ? elems[0].inputmask || this : this;
    },
    option: function (options, noremask) {
      return "string" == typeof options ? this.opts[options] : "object" == typeof options ? ($.extend(this.userOptions, options),
        this.el && !0 !== noremask && this.mask(this.el), this) : void 0;
    },
    unmaskedvalue: function (value) {
      return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache),
        maskScope.call(this, {
          action: "unmaskedvalue",
          value: value
        });
    },
    remove: function () {
      return maskScope.call(this, {
        action: "remove"
      });
    },
    getemptymask: function () {
      return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache),
        maskScope.call(this, {
          action: "getemptymask"
        });
    },
    hasMaskedValue: function () {
      return !this.opts.autoUnmask;
    },
    isComplete: function () {
      return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache),
        maskScope.call(this, {
          action: "isComplete"
        });
    },
    getmetadata: function () {
      return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache),
        maskScope.call(this, {
          action: "getmetadata"
        });
    },
    isValid: function (value) {
      return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache),
        maskScope.call(this, {
          action: "isValid",
          value: value
        });
    },
    format: function (value, metadata) {
      return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache),
        maskScope.call(this, {
          action: "format",
          value: value,
          metadata: metadata
        });
    },
    analyseMask: function (mask, regexMask, opts) {
      function MaskToken(isGroup, isOptional, isQuantifier, isAlternator) {
        this.matches = [], this.openGroup = isGroup || !1, this.alternatorGroup = !1, this.isGroup = isGroup || !1,
          this.isOptional = isOptional || !1, this.isQuantifier = isQuantifier || !1, this.isAlternator = isAlternator || !1,
          this.quantifier = {
            min: 1,
            max: 1
          };
      }

      function insertTestDefinition(mtoken, element, position) {
        position = position !== undefined ? position : mtoken.matches.length;
        var prevMatch = mtoken.matches[position - 1];
        if (regexMask) 0 === element.indexOf("[") || escaped && /\\d|\\s|\\w]/i.test(element) || "." === element ? mtoken.matches.splice(position++, 0, {
          fn: new RegExp(element, opts.casing ? "i" : ""),
          cardinality: 1,
          optionality: mtoken.isOptional,
          newBlockMarker: prevMatch === undefined || prevMatch.def !== element,
          casing: null,
          def: element,
          placeholder: undefined,
          nativeDef: element
        }) : (escaped && (element = element[element.length - 1]), $.each(element.split(""), function (ndx, lmnt) {
          prevMatch = mtoken.matches[position - 1], mtoken.matches.splice(position++, 0, {
            fn: null,
            cardinality: 0,
            optionality: mtoken.isOptional,
            newBlockMarker: prevMatch === undefined || prevMatch.def !== lmnt && null !== prevMatch.fn,
            casing: null,
            def: opts.staticDefinitionSymbol || lmnt,
            placeholder: opts.staticDefinitionSymbol !== undefined ? lmnt : undefined,
            nativeDef: lmnt
          });
        })), escaped = !1;
        else {
          var maskdef = (opts.definitions ? opts.definitions[element] : undefined) || Inputmask.prototype.definitions[element];
          if (maskdef && !escaped) {
            for (var prevalidators = maskdef.prevalidator, prevalidatorsL = prevalidators ? prevalidators.length : 0, i = 1; i < maskdef.cardinality; i++) {
              var prevalidator = prevalidatorsL >= i ? prevalidators[i - 1] : [],
                validator = prevalidator.validator,
                cardinality = prevalidator.cardinality;
              mtoken.matches.splice(position++, 0, {
                fn: validator ? "string" == typeof validator ? new RegExp(validator, opts.casing ? "i" : "") : new function () {
                  this.test = validator;
                }() : new RegExp("."),
                cardinality: cardinality || 1,
                optionality: mtoken.isOptional,
                newBlockMarker: prevMatch === undefined || prevMatch.def !== (maskdef.definitionSymbol || element),
                casing: maskdef.casing,
                def: maskdef.definitionSymbol || element,
                placeholder: maskdef.placeholder,
                nativeDef: element
              }), prevMatch = mtoken.matches[position - 1];
            }
            mtoken.matches.splice(position++, 0, {
              fn: maskdef.validator ? "string" == typeof maskdef.validator ? new RegExp(maskdef.validator, opts.casing ? "i" : "") : new function () {
                this.test = maskdef.validator;
              }() : new RegExp("."),
              cardinality: maskdef.cardinality,
              optionality: mtoken.isOptional,
              newBlockMarker: prevMatch === undefined || prevMatch.def !== (maskdef.definitionSymbol || element),
              casing: maskdef.casing,
              def: maskdef.definitionSymbol || element,
              placeholder: maskdef.placeholder,
              nativeDef: element
            });
          } else mtoken.matches.splice(position++, 0, {
            fn: null,
            cardinality: 0,
            optionality: mtoken.isOptional,
            newBlockMarker: prevMatch === undefined || prevMatch.def !== element && null !== prevMatch.fn,
            casing: null,
            def: opts.staticDefinitionSymbol || element,
            placeholder: opts.staticDefinitionSymbol !== undefined ? element : undefined,
            nativeDef: element
          }), escaped = !1;
        }
      }

      function verifyGroupMarker(maskToken) {
        maskToken && maskToken.matches && $.each(maskToken.matches, function (ndx, token) {
          var nextToken = maskToken.matches[ndx + 1];
          (nextToken === undefined || nextToken.matches === undefined || !1 === nextToken.isQuantifier) && token && token.isGroup && (token.isGroup = !1,
              regexMask || (insertTestDefinition(token, opts.groupmarker.start, 0), !0 !== token.openGroup && insertTestDefinition(token, opts.groupmarker.end))),
            verifyGroupMarker(token);
        });
      }

      function defaultCase() {
        if (openenings.length > 0) {
          if (currentOpeningToken = openenings[openenings.length - 1], insertTestDefinition(currentOpeningToken, m),
            currentOpeningToken.isAlternator) {
            alternator = openenings.pop();
            for (var mndx = 0; mndx < alternator.matches.length; mndx++) alternator.matches[mndx].isGroup = !1;
            openenings.length > 0 ? (currentOpeningToken = openenings[openenings.length - 1],
              currentOpeningToken.matches.push(alternator)) : currentToken.matches.push(alternator);
          }
        } else insertTestDefinition(currentToken, m);
      }

      function reverseTokens(maskToken) {
        maskToken.matches = maskToken.matches.reverse();
        for (var match in maskToken.matches)
          if (maskToken.matches.hasOwnProperty(match)) {
            var intMatch = parseInt(match);
            if (maskToken.matches[match].isQuantifier && maskToken.matches[intMatch + 1] && maskToken.matches[intMatch + 1].isGroup) {
              var qt = maskToken.matches[match];
              maskToken.matches.splice(match, 1), maskToken.matches.splice(intMatch + 1, 0, qt);
            }
            maskToken.matches[match].matches !== undefined ? maskToken.matches[match] = reverseTokens(maskToken.matches[match]) : maskToken.matches[match] = function (st) {
              return st === opts.optionalmarker.start ? st = opts.optionalmarker.end : st === opts.optionalmarker.end ? st = opts.optionalmarker.start : st === opts.groupmarker.start ? st = opts.groupmarker.end : st === opts.groupmarker.end && (st = opts.groupmarker.start),
                st;
            }(maskToken.matches[match]);
          }
        return maskToken;
      }
      var match, m, openingToken, currentOpeningToken, alternator, lastMatch, groupToken, tokenizer = /(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?\})|[^.?*+^${[]()|\\]+|./g,
        regexTokenizer = /\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g,
        escaped = !1,
        currentToken = new MaskToken(),
        openenings = [],
        maskTokens = [];
      for (regexMask && (opts.optionalmarker.start = undefined, opts.optionalmarker.end = undefined); match = regexMask ? regexTokenizer.exec(mask) : tokenizer.exec(mask);) {
        if (m = match[0], regexMask) switch (m.charAt(0)) {
          case "?":
            m = "{0,1}";
            break;

          case "+":
          case "*":
            m = "{" + m + "}";
        }
        if (escaped) defaultCase();
        else switch (m.charAt(0)) {
          case opts.escapeChar:
            escaped = !0, regexMask && defaultCase();
            break;

          case opts.optionalmarker.end:
          case opts.groupmarker.end:
            if (openingToken = openenings.pop(), openingToken.openGroup = !1, openingToken !== undefined)
              if (openenings.length > 0) {
                if (currentOpeningToken = openenings[openenings.length - 1], currentOpeningToken.matches.push(openingToken),
                  currentOpeningToken.isAlternator) {
                  alternator = openenings.pop();
                  for (var mndx = 0; mndx < alternator.matches.length; mndx++) alternator.matches[mndx].isGroup = !1,
                    alternator.matches[mndx].alternatorGroup = !1;
                  openenings.length > 0 ? (currentOpeningToken = openenings[openenings.length - 1],
                    currentOpeningToken.matches.push(alternator)) : currentToken.matches.push(alternator);
                }
              } else currentToken.matches.push(openingToken);
            else defaultCase();
            break;

          case opts.optionalmarker.start:
            openenings.push(new MaskToken(!1, !0));
            break;

          case opts.groupmarker.start:
            openenings.push(new MaskToken(!0));
            break;

          case opts.quantifiermarker.start:
            var quantifier = new MaskToken(!1, !1, !0);
            m = m.replace(/[{}]/g, "");
            var mq = m.split(","),
              mq0 = isNaN(mq[0]) ? mq[0] : parseInt(mq[0]),
              mq1 = 1 === mq.length ? mq0 : isNaN(mq[1]) ? mq[1] : parseInt(mq[1]);
            if ("*" !== mq1 && "+" !== mq1 || (mq0 = "*" === mq1 ? 0 : 1), quantifier.quantifier = {
                min: mq0,
                max: mq1
              }, openenings.length > 0) {
              var matches = openenings[openenings.length - 1].matches;
              match = matches.pop(), match.isGroup || (groupToken = new MaskToken(!0), groupToken.matches.push(match),
                match = groupToken), matches.push(match), matches.push(quantifier);
            } else match = currentToken.matches.pop(), match.isGroup || (regexMask && null === match.fn && "." === match.def && (match.fn = new RegExp(match.def, opts.casing ? "i" : "")),
                groupToken = new MaskToken(!0), groupToken.matches.push(match), match = groupToken),
              currentToken.matches.push(match), currentToken.matches.push(quantifier);
            break;

          case opts.alternatormarker:
            if (openenings.length > 0) {
              currentOpeningToken = openenings[openenings.length - 1];
              var subToken = currentOpeningToken.matches[currentOpeningToken.matches.length - 1];
              lastMatch = currentOpeningToken.openGroup && (subToken.matches === undefined || !1 === subToken.isGroup && !1 === subToken.isAlternator) ? openenings.pop() : currentOpeningToken.matches.pop();
            } else lastMatch = currentToken.matches.pop();
            if (lastMatch.isAlternator) openenings.push(lastMatch);
            else if (lastMatch.alternatorGroup ? (alternator = openenings.pop(),
                lastMatch.alternatorGroup = !1) : alternator = new MaskToken(!1, !1, !1, !0), alternator.matches.push(lastMatch),
              openenings.push(alternator), lastMatch.openGroup) {
              lastMatch.openGroup = !1;
              var alternatorGroup = new MaskToken(!0);
              alternatorGroup.alternatorGroup = !0, openenings.push(alternatorGroup);
            }
            break;

          default:
            defaultCase();
        }
      }
      for (; openenings.length > 0;) openingToken = openenings.pop(), currentToken.matches.push(openingToken);
      return currentToken.matches.length > 0 && (verifyGroupMarker(currentToken), maskTokens.push(currentToken)),
        (opts.numericInput || opts.isRTL) && reverseTokens(maskTokens[0]), maskTokens;
    }
  }, Inputmask.extendDefaults = function (options) {
    $.extend(!0, Inputmask.prototype.defaults, options);
  }, Inputmask.extendDefinitions = function (definition) {
    $.extend(!0, Inputmask.prototype.definitions, definition);
  }, Inputmask.extendAliases = function (alias) {
    $.extend(!0, Inputmask.prototype.aliases, alias);
  }, Inputmask.format = function (value, options, metadata) {
    return Inputmask(options).format(value, metadata);
  }, Inputmask.unmask = function (value, options) {
    return Inputmask(options).unmaskedvalue(value);
  }, Inputmask.isValid = function (value, options) {
    return Inputmask(options).isValid(value);
  }, Inputmask.remove = function (elems) {
    $.each(elems, function (ndx, el) {
      el.inputmask && el.inputmask.remove();
    });
  }, Inputmask.escapeRegex = function (str) {
    var specials = ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"];
    return str.replace(new RegExp("(\\" + specials.join("|\\") + ")", "gim"), "\\$1");
  }, Inputmask.keyCode = {
    ALT: 18,
    BACKSPACE: 8,
    BACKSPACE_SAFARI: 127,
    CAPS_LOCK: 20,
    COMMA: 188,
    COMMAND: 91,
    COMMAND_LEFT: 91,
    COMMAND_RIGHT: 93,
    CONTROL: 17,
    DELETE: 46,
    DOWN: 40,
    END: 35,
    ENTER: 13,
    ESCAPE: 27,
    HOME: 36,
    INSERT: 45,
    LEFT: 37,
    MENU: 93,
    NUMPAD_ADD: 107,
    NUMPAD_DECIMAL: 110,
    NUMPAD_DIVIDE: 111,
    NUMPAD_ENTER: 108,
    NUMPAD_MULTIPLY: 106,
    NUMPAD_SUBTRACT: 109,
    PAGE_DOWN: 34,
    PAGE_UP: 33,
    PERIOD: 190,
    RIGHT: 39,
    SHIFT: 16,
    SPACE: 32,
    TAB: 9,
    UP: 38,
    WINDOWS: 91,
    X: 88
  }, Inputmask;
});
/*!
 * inputmask.extensions.js
 * https://github.com/RobinHerbots/Inputmask
 * Copyright (c) 2010 - 2017 Robin Herbots
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 4.0.1-37
 */

! function (factory) {
  "function" == typeof define && define.amd ? define(["./dependencyLibs/inputmask.dependencyLib", "./inputmask"], factory) : "object" == typeof exports ? module.exports = factory(require("./dependencyLibs/inputmask.dependencyLib"), require("./inputmask")) : factory(window.dependencyLib || jQuery, window.Inputmask);
}(function ($, Inputmask) {
  return Inputmask.extendDefinitions({
    A: {
      validator: "[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",
      cardinality: 1,
      casing: "upper"
    },
    "&": {
      validator: "[0-9A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",
      cardinality: 1,
      casing: "upper"
    },
    "#": {
      validator: "[0-9A-Fa-f]",
      cardinality: 1,
      casing: "upper"
    }
  }), Inputmask.extendAliases({
    url: {
      definitions: {
        i: {
          validator: ".",
          cardinality: 1
        }
      },
      mask: "(\\http://)|(\\http\\s://)|(ftp://)|(ftp\\s://)i{+}",
      insertMode: !1,
      autoUnmask: !1,
      inputmode: "url"
    },
    ip: {
      mask: "i[i[i]].i[i[i]].i[i[i]].i[i[i]]",
      definitions: {
        i: {
          validator: function (chrs, maskset, pos, strict, opts) {
            return pos - 1 > -1 && "." !== maskset.buffer[pos - 1] ? (chrs = maskset.buffer[pos - 1] + chrs,
                chrs = pos - 2 > -1 && "." !== maskset.buffer[pos - 2] ? maskset.buffer[pos - 2] + chrs : "0" + chrs) : chrs = "00" + chrs,
              new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]").test(chrs);
          },
          cardinality: 1
        }
      },
      onUnMask: function (maskedValue, unmaskedValue, opts) {
        return maskedValue;
      },
      inputmode: "numeric"
    },
    email: {
      mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]",
      greedy: !1,
      onBeforePaste: function (pastedValue, opts) {
        return pastedValue = pastedValue.toLowerCase(), pastedValue.replace("mailto:", "");
      },
      definitions: {
        "*": {
          validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",
          cardinality: 1,
          casing: "lower"
        },
        "-": {
          validator: "[0-9A-Za-z-]",
          cardinality: 1,
          casing: "lower"
        }
      },
      onUnMask: function (maskedValue, unmaskedValue, opts) {
        return maskedValue;
      },
      inputmode: "email"
    },
    mac: {
      mask: "##:##:##:##:##:##"
    },
    vin: {
      mask: "V{13}9{4}",
      definitions: {
        V: {
          validator: "[A-HJ-NPR-Za-hj-npr-z\\d]",
          cardinality: 1,
          casing: "upper"
        }
      },
      clearIncomplete: !0,
      autoUnmask: !0
    }
  }), Inputmask;
});
/*!
 * inputmask.phone.extensions.js
 * https://github.com/RobinHerbots/Inputmask
 * Copyright (c) 2010 - 2017 Robin Herbots
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 4.0.1-37
 */

! function (factory) {
  "function" == typeof define && define.amd ? define(["./dependencyLibs/inputmask.dependencyLib", "./inputmask"], factory) : "object" == typeof exports ? module.exports = factory(require("./dependencyLibs/inputmask.dependencyLib"), require("./inputmask")) : factory(window.dependencyLib || jQuery, window.Inputmask);
}(function ($, Inputmask) {
  function maskSort(a, b) {
    var maska = (a.mask || a).replace(/#/g, "9").replace(/\)/, "9").replace(/[+()#-]/g, ""),
      maskb = (b.mask || b).replace(/#/g, "9").replace(/\)/, "9").replace(/[+()#-]/g, ""),
      maskas = (a.mask || a).split("#")[0],
      maskbs = (b.mask || b).split("#")[0];
    return 0 === maskbs.indexOf(maskas) ? -1 : 0 === maskas.indexOf(maskbs) ? 1 : maska.localeCompare(maskb);
  }
  var analyseMaskBase = Inputmask.prototype.analyseMask;
  return Inputmask.prototype.analyseMask = function (mask, regexMask, opts) {
    function reduceVariations(masks, previousVariation, previousmaskGroup) {
      previousVariation = previousVariation || "", previousmaskGroup = previousmaskGroup || maskGroups,
        "" !== previousVariation && (previousmaskGroup[previousVariation] = {});
      for (var variation = "", maskGroup = previousmaskGroup[previousVariation] || previousmaskGroup, i = masks.length - 1; i >= 0; i--) mask = masks[i].mask || masks[i],
        variation = mask.substr(0, 1), maskGroup[variation] = maskGroup[variation] || [],
        maskGroup[variation].unshift(mask.substr(1)), masks.splice(i, 1);
      for (var ndx in maskGroup) maskGroup[ndx].length > 500 && reduceVariations(maskGroup[ndx].slice(), ndx, maskGroup);
    }

    function rebuild(maskGroup) {
      var mask = "",
        submasks = [];
      for (var ndx in maskGroup) $.isArray(maskGroup[ndx]) ? 1 === maskGroup[ndx].length ? submasks.push(ndx + maskGroup[ndx]) : submasks.push(ndx + opts.groupmarker.start + maskGroup[ndx].join(opts.groupmarker.end + opts.alternatormarker + opts.groupmarker.start) + opts.groupmarker.end) : submasks.push(ndx + rebuild(maskGroup[ndx]));
      return 1 === submasks.length ? mask += submasks[0] : mask += opts.groupmarker.start + submasks.join(opts.groupmarker.end + opts.alternatormarker + opts.groupmarker.start) + opts.groupmarker.end,
        mask;
    }
    var maskGroups = {};
    return opts.phoneCodes && (opts.phoneCodes && opts.phoneCodes.length > 1e3 && (mask = mask.substr(1, mask.length - 2),
      reduceVariations(mask.split(opts.groupmarker.end + opts.alternatormarker + opts.groupmarker.start)),
      mask = rebuild(maskGroups)), mask = mask.replace(/9/g, "\\9")), analyseMaskBase.call(this, mask, regexMask, opts);
  }, Inputmask.extendAliases({
    abstractphone: {
      groupmarker: {
        start: "<",
        end: ">"
      },
      countrycode: "",
      phoneCodes: [],
      mask: function (opts) {
        return opts.definitions = {
          "#": Inputmask.prototype.definitions[9]
        }, opts.phoneCodes.sort(maskSort);
      },
      keepStatic: !0,
      onBeforeMask: function (value, opts) {
        var processedValue = value.replace(/^0{1,2}/, "").replace(/[\s]/g, "");
        return (processedValue.indexOf(opts.countrycode) > 1 || -1 === processedValue.indexOf(opts.countrycode)) && (processedValue = "+" + opts.countrycode + processedValue),
          processedValue;
      },
      onUnMask: function (maskedValue, unmaskedValue, opts) {
        return maskedValue.replace(/[()#-]/g, "");
      },
      inputmode: "tel"
    }
  }), Inputmask;
});

function _extends() {
  return (_extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key])
    }
    return target
  }).apply(this, arguments)
}
var CustomSelect = function ($) {
  var defaults = {
      block: "custom-select",
      hideCallback: !1,
      includeValue: !1,
      keyboard: !0,
      modifier: !1,
      placeholder: !1,
      search: !1,
      showCallback: !1,
      transition: 0
    },
    CustomSelect = function () {
      function CustomSelect(select, options) {
        this._$select = $(select), this._options = _extends({}, defaults, "object" == typeof options ? options : {}), this._activeModifier = this._options.block + "--active", this._dropupModifier = this._options.block + "--dropup", this._optionSelectedModifier = this._options.block + "__option--selected", this._keydown = this._keydown.bind(this), this._dropup = this._dropup.bind(this), this._outside = this._outside.bind(this), this._init()
      }
      var _proto = CustomSelect.prototype;
      return _proto.reset = function () {
        this._$dropdown.hide().empty(), this._$value.off("click"), this._fill()
      }, _proto._init = function () {
        this._$element = $('<div class="' + this._options.block + '">\n           <button class="' + this._options.block + "__option " + this._options.block + '__option--value" type="button"></button>\n           <div class="' + this._options.block + '__dropdown" style="display: none;"></div>\n         </div>'), this._$select.hide().after(this._$element), this._options.modifier && this._$element.addClass(this._options.modifier), this._$value = this._$element.find("." + this._options.block + "__option--value"), this._$dropdown = this._$element.find("." + this._options.block + "__dropdown"), this._fill()
      }, _proto._fill = function () {
        var _this = this;
        this._$values = this._$select.find("option"), this._values = [];
        var placeholder = this._options.placeholder;
        $.each(this._$values, function (i, option) {
          var el = $(option).text().trim();
          _this._values.push(el)
        }), placeholder && (this._$select.find("[selected]").length ? placeholder = !1 : (this._$value.html(placeholder), this._$select.prop("selectedIndex", -1))), $.each(this._values, function (i, el) {
          var cssClass = _this._$values.eq(i).attr("class"),
            $option = $('<button class="' + _this._options.block + '__option" type="button">' + el + "</button>"),
            $selected = _this._$select.find(":selected");
          _this._$values.eq(i).attr("disabled") && $option.prop("disabled", !0), !$selected.length && 0 === i || el === $selected.text().trim() ? (placeholder || _this._$value.text(el).removeClass(_this._$value.data("class")).removeData("class").addClass(cssClass).data("class", cssClass), (_this._options.includeValue || placeholder) && ($option.addClass(cssClass), $option.toggleClass(_this._optionSelectedModifier, _this._$values.eq(i).is("[selected]")), _this._$dropdown.append($option))) : ($option.addClass(cssClass), _this._$dropdown.append($option))
        }), this._$options = this._$dropdown.find("." + this._options.block + "__option"), this._options.search && this._search(), this._$value.one("click", function (event) {
          _this._show(event)
        }), this._$value.prop("disabled", !this._$options.length), this._$options.on("click", function (event) {
          _this._select(event)
        })
      }, _proto._show = function (event) {
        var _this2 = this;
        event.preventDefault(), this._dropup(), $(window).on("resize scroll", this._dropup), this._$element.addClass(this._activeModifier), this._$dropdown.slideDown(this._options.transition, function () {
          _this2._options.search && (_this2._$input.focus(), _this2._options.includeValue && _this2._scroll()), "function" == typeof _this2._options.showCallback && _this2._options.showCallback.call(_this2._$element[0])
        }), setTimeout(function () {
          $(document).on("touchstart click", _this2._outside)
        }, 0), this._$value.one("click", function (event) {
          event.preventDefault(), _this2._hide()
        }), this._options.keyboard && (this._options.index = -1, $(window).on("keydown", this._keydown))
      }, _proto._hide = function () {
        var _this3 = this;
        this._options.search && (this._$input.val("").blur(), this._$options.show(), this._$wrap.scrollTop(0)), this._$dropdown.slideUp(this._options.transition, function () {
          _this3._$element.removeClass(_this3._activeModifier).removeClass(_this3._dropupModifier), "function" == typeof _this3._options.hideCallback && _this3._options.hideCallback.call(_this3._$element[0]), _this3._$value.off("click").one("click", function (event) {
            _this3._show(event)
          }), $(document).off("touchstart click", _this3._outside), $(window).off("resize scroll", _this3._dropup)
        }), this._options.keyboard && (this._$options.blur(), $(window).off("keydown", this._keydown))
      }, _proto._scroll = function () {
        var _this4 = this;
        $.each(this._$options, function (i, option) {
          var $option = $(option);
          if ($option.text() === _this4._$value.text()) {
            var top = $option.position().top,
              center = _this4._$wrap.outerHeight() / 2 - $option.outerHeight() / 2;
            return center < top && _this4._$wrap.scrollTop(top - center), !1
          }
        })
      }, _proto._select = function (event) {
        var _this5 = this;
        event.preventDefault();
        var choice = $(event.currentTarget).text().trim(),
          values = this._values.concat();
        if (this._$value.text(choice).removeClass(this._$value.data("class")), this._$values.prop("selected", !1), $.each(values, function (i, el) {
            _this5._options.includeValue || el !== choice || values.splice(i, 1), $.each(_this5._$values, function (i, option) {
              var $option = $(option);
              if ($option.text().trim() === choice) {
                var cssClass = $option.attr("class");
                $option.prop("selected", !0), _this5._$value.addClass(cssClass).data("class", cssClass)
              }
            })
          }), this._hide(), this._options.includeValue) this._$options.removeClass(this._optionSelectedModifier), $.each(this._$options, function (i, option) {
          var $option = $(option);
          if ($option.text().trim() === choice) return $option.addClass(_this5._optionSelectedModifier), !1
        });
        else {
          if (this._$options.length > values.length) {
            var last = this._$options.eq(values.length);
            last.remove(), this._$options = this._$options.not(last), this._$options.length || this._$value.prop("disabled", !0)
          }
          $.each(this._$options, function (i, option) {
            var $option = $(option);
            $option.text(values[i]), $option.attr("class", _this5._options.block + "__option"), $.each(_this5._$values, function () {
              var $this = $(this);
              $this.text().trim() === values[i] && $option.addClass($this.attr("class"))
            })
          })
        }
        void 0 !== event.originalEvent && this._$select.trigger("change")
      }, _proto._search = function () {
        var _this6 = this;
        this._$input = $('<input class="' + this._options.block + '__input" autocomplete="off">'), this._$dropdown.prepend(this._$input), this._$options.wrapAll('<div class="' + this._options.block + '__option-wrap"></div>'), this._$wrap = this._$element.find("." + this._options.block + "__option-wrap"), this._$input.on("focus", function () {
          _this6._options.index = -1
        }), this._$input.on("keyup", function () {
          var query = _this6._$input.val().trim();
          query.length ? (_this6._$wrap.scrollTop(0), setTimeout(function () {
            query === _this6._$input.val().trim() && $.each(_this6._$options, function (i, option) {
              var $option = $(option),
                match = -1 !== $option.text().trim().toLowerCase().indexOf(query.toLowerCase());
              $option.toggle(match)
            })
          }, 300)) : _this6._$options.show()
        })
      }, _proto._dropup = function () {
        var bottom = this._$element[0].getBoundingClientRect().bottom,
          up = $(window).height() - bottom < this._$dropdown.height();
        this._$element.toggleClass(this._dropupModifier, up)
      }, _proto._outside = function (event) {
        var $target = $(event.target);
        $target.parents().is(this._$element) || $target.is(this._$element) || this._hide()
      }, _proto._keydown = function (event) {
        var $visible = this._$options.filter(":visible").not("[disabled]");
        switch (event.which) {
          case 40:
            event.preventDefault(), $visible.eq(this._options.index + 1).length ? this._options.index += 1 : this._options.index = 0, $visible.eq(this._options.index).focus();
            break;
          case 38:
            event.preventDefault(), $visible.eq(this._options.index - 1).length && 0 <= this._options.index - 1 ? this._options.index -= 1 : this._options.index = $visible.length - 1, $visible.eq(this._options.index).focus();
            break;
          case 13:
          case 32:
            if (!this._$input || !this._$input.is(":focus")) {
              event.preventDefault();
              var $option = this._$options.add(this._$value).filter(":focus");
              $option.trigger("click"), $option.is(this._$value) || this._$select.trigger("change"), this._$value.focus()
            }
            break;
          case 27:
            event.preventDefault(), this._hide(), this._$value.focus()
        }
      }, CustomSelect._jQueryPlugin = function (config) {
        return this.each(function () {
          var $this = $(this),
            data = $this.data("custom-select");
          data ? "reset" === config && data.reset() : "string" != typeof config && (data = new CustomSelect(this, config), $this.data("custom-select", data))
        })
      }, CustomSelect
    }();
  return $.fn.customSelect = CustomSelect._jQueryPlugin, $.fn.customSelect.noConflict = function () {
    return $.fn.customSelect
  }, CustomSelect
}($);
//# sourceMappingURL=jquery.custom-select.min.js.map
var DateFormatter;
! function () {
  "use strict";
  var D, s, r, a, n;
  D = function (e, t) {
    return "string" == typeof e && "string" == typeof t && e.toLowerCase() === t.toLowerCase()
  }, s = function (e, t, a) {
    var n = a || "0",
      r = e.toString();
    return r.length < t ? s(n + r, t) : r
  }, r = function (e) {
    var t, a;
    for (e = e || {}, t = 1; t < arguments.length; t++)
      if (a = arguments[t])
        for (var n in a) a.hasOwnProperty(n) && ("object" == typeof a[n] ? r(e[n], a[n]) : e[n] = a[n]);
    return e
  }, a = function (e, t) {
    for (var a = 0; a < t.length; a++)
      if (t[a].toLowerCase() === e.toLowerCase()) return a;
    return -1
  }, n = {
    dateSettings: {
      days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      meridiem: ["AM", "PM"],
      ordinal: function (e) {
        var t = e % 10,
          a = {
            1: "st",
            2: "nd",
            3: "rd"
          };
        return 1 !== Math.floor(e % 100 / 10) && a[t] ? a[t] : "th"
      }
    },
    separators: /[ \-+\/\.T:@]/g,
    validParts: /[dDjlNSwzWFmMntLoYyaABgGhHisueTIOPZcrU]/g,
    intParts: /[djwNzmnyYhHgGis]/g,
    tzParts: /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
    tzClip: /[^-+\dA-Z]/g
  }, (DateFormatter = function (e) {
    var t = this,
      a = r(n, e);
    t.dateSettings = a.dateSettings, t.separators = a.separators, t.validParts = a.validParts, t.intParts = a.intParts, t.tzParts = a.tzParts, t.tzClip = a.tzClip
  }).prototype = {
    constructor: DateFormatter,
    getMonth: function (e) {
      var t;
      return 0 === (t = a(e, this.dateSettings.monthsShort) + 1) && (t = a(e, this.dateSettings.months) + 1), t
    },
    parseDate: function (e, t) {
      var a, n, r, o, i, s, d, u, l, f, c = this,
        m = !1,
        h = !1,
        g = c.dateSettings,
        p = {
          date: null,
          year: null,
          month: null,
          day: null,
          hour: 0,
          min: 0,
          sec: 0
        };
      if (!e) return null;
      if (e instanceof Date) return e;
      if ("U" === t) return (r = parseInt(e)) ? new Date(1e3 * r) : e;
      switch (typeof e) {
        case "number":
          return new Date(e);
        case "string":
          break;
        default:
          return null
      }
      if (!(a = t.match(c.validParts)) || 0 === a.length) throw new Error("Invalid date format definition.");
      for (n = e.replace(c.separators, "\0").split("\0"), r = 0; r < n.length; r++) switch (o = n[r], i = parseInt(o), a[r]) {
        case "y":
        case "Y":
          if (!i) return null;
          l = o.length, p.year = 2 === l ? parseInt((i < 70 ? "20" : "19") + o) : i, m = !0;
          break;
        case "m":
        case "n":
        case "M":
        case "F":
          if (isNaN(i)) {
            if (!(0 < (s = c.getMonth(o)))) return null;
            p.month = s
          } else {
            if (!(1 <= i && i <= 12)) return null;
            p.month = i
          }
          m = !0;
          break;
        case "d":
        case "j":
          if (!(1 <= i && i <= 31)) return null;
          p.day = i, m = !0;
          break;
        case "g":
        case "h":
          if (f = n[d = -1 < a.indexOf("a") ? a.indexOf("a") : -1 < a.indexOf("A") ? a.indexOf("A") : -1], -1 < d) u = D(f, g.meridiem[0]) ? 0 : D(f, g.meridiem[1]) ? 12 : -1, 1 <= i && i <= 12 && -1 < u ? p.hour = i + u - 1 : 0 <= i && i <= 23 && (p.hour = i);
          else {
            if (!(0 <= i && i <= 23)) return null;
            p.hour = i
          }
          h = !0;
          break;
        case "G":
        case "H":
          if (!(0 <= i && i <= 23)) return null;
          p.hour = i, h = !0;
          break;
        case "i":
          if (!(0 <= i && i <= 59)) return null;
          p.min = i, h = !0;
          break;
        case "s":
          if (!(0 <= i && i <= 59)) return null;
          p.sec = i, h = !0
      }
      if (!0 === m && p.year && p.month && p.day) p.date = new Date(p.year, p.month - 1, p.day, p.hour, p.min, p.sec, 0);
      else {
        if (!0 !== h) return null;
        p.date = new Date(0, 0, 0, p.hour, p.min, p.sec, 0)
      }
      return p.date
    },
    guessDate: function (e, t) {
      if ("string" != typeof e) return e;
      var a, n, r, o, i, s, d = e.replace(this.separators, "\0").split("\0"),
        u = t.match(this.validParts),
        l = new Date,
        f = 0;
      if (!/^[djmn]/g.test(u[0])) return e;
      for (r = 0; r < d.length; r++) {
        if (f = 2, i = d[r], s = parseInt(i.substr(0, 2)), isNaN(s)) return null;
        switch (r) {
          case 0:
            "m" === u[0] || "n" === u[0] ? l.setMonth(s - 1) : l.setDate(s);
            break;
          case 1:
            "m" === u[0] || "n" === u[0] ? l.setDate(s) : l.setMonth(s - 1);
            break;
          case 2:
            if (n = l.getFullYear(), f = (a = i.length) < 4 ? a : 4, !(n = parseInt(a < 4 ? n.toString().substr(0, 4 - a) + i : i.substr(0, 4)))) return null;
            l.setFullYear(n);
            break;
          case 3:
            l.setHours(s);
            break;
          case 4:
            l.setMinutes(s);
            break;
          case 5:
            l.setSeconds(s)
        }
        0 < (o = i.substr(f)).length && d.splice(r + 1, 0, o)
      }
      return l
    },
    parseFormat: function (e, n) {
      var a, t = this,
        r = t.dateSettings,
        o = /\\?(.?)/gi,
        i = function (e, t) {
          return a[e] ? a[e]() : t
        };
      return a = {
        d: function () {
          return s(a.j(), 2)
        },
        D: function () {
          return r.daysShort[a.w()]
        },
        j: function () {
          return n.getDate()
        },
        l: function () {
          return r.days[a.w()]
        },
        N: function () {
          return a.w() || 7
        },
        w: function () {
          return n.getDay()
        },
        z: function () {
          var e = new Date(a.Y(), a.n() - 1, a.j()),
            t = new Date(a.Y(), 0, 1);
          return Math.round((e - t) / 864e5)
        },
        W: function () {
          var e = new Date(a.Y(), a.n() - 1, a.j() - a.N() + 3),
            t = new Date(e.getFullYear(), 0, 4);
          return s(1 + Math.round((e - t) / 864e5 / 7), 2)
        },
        F: function () {
          return r.months[n.getMonth()]
        },
        m: function () {
          return s(a.n(), 2)
        },
        M: function () {
          return r.monthsShort[n.getMonth()]
        },
        n: function () {
          return n.getMonth() + 1
        },
        t: function () {
          return new Date(a.Y(), a.n(), 0).getDate()
        },
        L: function () {
          var e = a.Y();
          return e % 4 == 0 && e % 100 != 0 || e % 400 == 0 ? 1 : 0
        },
        o: function () {
          var e = a.n(),
            t = a.W();
          return a.Y() + (12 === e && t < 9 ? 1 : 1 === e && 9 < t ? -1 : 0)
        },
        Y: function () {
          return n.getFullYear()
        },
        y: function () {
          return a.Y().toString().slice(-2)
        },
        a: function () {
          return a.A().toLowerCase()
        },
        A: function () {
          var e = a.G() < 12 ? 0 : 1;
          return r.meridiem[e]
        },
        B: function () {
          var e = 3600 * n.getUTCHours(),
            t = 60 * n.getUTCMinutes(),
            a = n.getUTCSeconds();
          return s(Math.floor((e + t + a + 3600) / 86.4) % 1e3, 3)
        },
        g: function () {
          return a.G() % 12 || 12
        },
        G: function () {
          return n.getHours()
        },
        h: function () {
          return s(a.g(), 2)
        },
        H: function () {
          return s(a.G(), 2)
        },
        i: function () {
          return s(n.getMinutes(), 2)
        },
        s: function () {
          return s(n.getSeconds(), 2)
        },
        u: function () {
          return s(1e3 * n.getMilliseconds(), 6)
        },
        e: function () {
          return /\((.*)\)/.exec(String(n))[1] || "Coordinated Universal Time"
        },
        I: function () {
          return new Date(a.Y(), 0) - Date.UTC(a.Y(), 0) != new Date(a.Y(), 6) - Date.UTC(a.Y(), 6) ? 1 : 0
        },
        O: function () {
          var e = n.getTimezoneOffset(),
            t = Math.abs(e);
          return (0 < e ? "-" : "+") + s(100 * Math.floor(t / 60) + t % 60, 4)
        },
        P: function () {
          var e = a.O();
          return e.substr(0, 3) + ":" + e.substr(3, 2)
        },
        T: function () {
          return (String(n).match(t.tzParts) || [""]).pop().replace(t.tzClip, "") || "UTC"
        },
        Z: function () {
          return 60 * -n.getTimezoneOffset()
        },
        c: function () {
          return "Y-m-d\\TH:i:sP".replace(o, i)
        },
        r: function () {
          return "D, d M Y H:i:s O".replace(o, i)
        },
        U: function () {
          return n.getTime() / 1e3 || 0
        }
      }, i(e, e)
    },
    formatDate: function (e, t) {
      var a, n, r, o, i, s = "";
      if ("string" == typeof e && !(e = this.parseDate(e, t))) return null;
      if (e instanceof Date) {
        for (r = t.length, a = 0; a < r; a++) "S" !== (i = t.charAt(a)) && "\\" !== i && (0 < a && "\\" === t.charAt(a - 1) ? s += i : (o = this.parseFormat(i, e), a !== r - 1 && this.intParts.test(i) && "S" === t.charAt(a + 1) && (n = parseInt(o) || 0, o += this.dateSettings.ordinal(n)), s += o));
        return s
      }
      return ""
    }
  }
}();
var datetimepickerFactory = function (L) {
  "use strict";
  var s = {
      i18n: {
        ar: {
          months: ["كانون الثاني", "شباط", "آذار", "نيسان", "مايو", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"],
          dayOfWeekShort: ["ن", "ث", "ع", "خ", "ج", "س", "ح"],
          dayOfWeek: ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت", "الأحد"]
        },
        ro: {
          months: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
          dayOfWeekShort: ["Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sâ"],
          dayOfWeek: ["Duminică", "Luni", "Marţi", "Miercuri", "Joi", "Vineri", "Sâmbătă"]
        },
        id: {
          months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
          dayOfWeekShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
          dayOfWeek: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
        },
        is: {
          months: ["Janúar", "Febrúar", "Mars", "Apríl", "Maí", "Júní", "Júlí", "Ágúst", "September", "Október", "Nóvember", "Desember"],
          dayOfWeekShort: ["Sun", "Mán", "Þrið", "Mið", "Fim", "Fös", "Lau"],
          dayOfWeek: ["Sunnudagur", "Mánudagur", "Þriðjudagur", "Miðvikudagur", "Fimmtudagur", "Föstudagur", "Laugardagur"]
        },
        bg: {
          months: ["Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"],
          dayOfWeekShort: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
          dayOfWeek: ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"]
        },
        fa: {
          months: ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"],
          dayOfWeekShort: ["یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"],
          dayOfWeek: ["یک‌شنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنج‌شنبه", "جمعه", "شنبه", "یک‌شنبه"]
        },
        ru: {
          months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
          dayOfWeekShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
          dayOfWeek: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
        },
        uk: {
          months: ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"],
          dayOfWeekShort: ["Ндл", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Сбт"],
          dayOfWeek: ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота"]
        },
        en: {
          months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        },
        el: {
          months: ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"],
          dayOfWeekShort: ["Κυρ", "Δευ", "Τρι", "Τετ", "Πεμ", "Παρ", "Σαβ"],
          dayOfWeek: ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο"]
        },
        de: {
          months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
          dayOfWeekShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
          dayOfWeek: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"]
        },
        nl: {
          months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
          dayOfWeekShort: ["zo", "ma", "di", "wo", "do", "vr", "za"],
          dayOfWeek: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"]
        },
        tr: {
          months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
          dayOfWeekShort: ["Paz", "Pts", "Sal", "Çar", "Per", "Cum", "Cts"],
          dayOfWeek: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"]
        },
        fr: {
          months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
          dayOfWeekShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
          dayOfWeek: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]
        },
        es: {
          months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
          dayOfWeekShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
          dayOfWeek: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
        },
        th: {
          months: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
          dayOfWeekShort: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
          dayOfWeek: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์", "อาทิตย์"]
        },
        pl: {
          months: ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"],
          dayOfWeekShort: ["nd", "pn", "wt", "śr", "cz", "pt", "sb"],
          dayOfWeek: ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"]
        },
        pt: {
          months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
          dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
          dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
        },
        ch: {
          months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
          dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"]
        },
        se: {
          months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
          dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"]
        },
        km: {
          months: ["មករា​", "កុម្ភៈ", "មិនា​", "មេសា​", "ឧសភា​", "មិថុនា​", "កក្កដា​", "សីហា​", "កញ្ញា​", "តុលា​", "វិច្ឆិកា", "ធ្នូ​"],
          dayOfWeekShort: ["អាទិ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហ​​", "សុក្រ​", "សៅរ៍"],
          dayOfWeek: ["អាទិត្យ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហស្បតិ៍​", "សុក្រ​", "សៅរ៍"]
        },
        kr: {
          months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
          dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
          dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
        },
        it: {
          months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
          dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
          dayOfWeek: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"]
        },
        da: {
          months: ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
          dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
          dayOfWeek: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"]
        },
        no: {
          months: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
          dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
          dayOfWeek: ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"]
        },
        ja: {
          months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
          dayOfWeekShort: ["日", "月", "火", "水", "木", "金", "土"],
          dayOfWeek: ["日曜", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜"]
        },
        vi: {
          months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
          dayOfWeekShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
          dayOfWeek: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"]
        },
        sl: {
          months: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
          dayOfWeekShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"],
          dayOfWeek: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"]
        },
        cs: {
          months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
          dayOfWeekShort: ["Ne", "Po", "Út", "St", "Čt", "Pá", "So"]
        },
        hu: {
          months: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
          dayOfWeekShort: ["Va", "Hé", "Ke", "Sze", "Cs", "Pé", "Szo"],
          dayOfWeek: ["vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat"]
        },
        az: {
          months: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "Iyun", "Iyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
          dayOfWeekShort: ["B", "Be", "Ça", "Ç", "Ca", "C", "Ş"],
          dayOfWeek: ["Bazar", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"]
        },
        bs: {
          months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
          dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
          dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
        },
        ca: {
          months: ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"],
          dayOfWeekShort: ["Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds"],
          dayOfWeek: ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"]
        },
        "en-GB": {
          months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        },
        et: {
          months: ["Jaanuar", "Veebruar", "Märts", "Aprill", "Mai", "Juuni", "Juuli", "August", "September", "Oktoober", "November", "Detsember"],
          dayOfWeekShort: ["P", "E", "T", "K", "N", "R", "L"],
          dayOfWeek: ["Pühapäev", "Esmaspäev", "Teisipäev", "Kolmapäev", "Neljapäev", "Reede", "Laupäev"]
        },
        eu: {
          months: ["Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"],
          dayOfWeekShort: ["Ig.", "Al.", "Ar.", "Az.", "Og.", "Or.", "La."],
          dayOfWeek: ["Igandea", "Astelehena", "Asteartea", "Asteazkena", "Osteguna", "Ostirala", "Larunbata"]
        },
        fi: {
          months: ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"],
          dayOfWeekShort: ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"],
          dayOfWeek: ["sunnuntai", "maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai"]
        },
        gl: {
          months: ["Xan", "Feb", "Maz", "Abr", "Mai", "Xun", "Xul", "Ago", "Set", "Out", "Nov", "Dec"],
          dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Xov", "Ven", "Sab"],
          dayOfWeek: ["Domingo", "Luns", "Martes", "Mércores", "Xoves", "Venres", "Sábado"]
        },
        hr: {
          months: ["Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"],
          dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
          dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
        },
        ko: {
          months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
          dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
          dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
        },
        lt: {
          months: ["Sausio", "Vasario", "Kovo", "Balandžio", "Gegužės", "Birželio", "Liepos", "Rugpjūčio", "Rugsėjo", "Spalio", "Lapkričio", "Gruodžio"],
          dayOfWeekShort: ["Sek", "Pir", "Ant", "Tre", "Ket", "Pen", "Šeš"],
          dayOfWeek: ["Sekmadienis", "Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"]
        },
        lv: {
          months: ["Janvāris", "Februāris", "Marts", "Aprīlis ", "Maijs", "Jūnijs", "Jūlijs", "Augusts", "Septembris", "Oktobris", "Novembris", "Decembris"],
          dayOfWeekShort: ["Sv", "Pr", "Ot", "Tr", "Ct", "Pk", "St"],
          dayOfWeek: ["Svētdiena", "Pirmdiena", "Otrdiena", "Trešdiena", "Ceturtdiena", "Piektdiena", "Sestdiena"]
        },
        mk: {
          months: ["јануари", "февруари", "март", "април", "мај", "јуни", "јули", "август", "септември", "октомври", "ноември", "декември"],
          dayOfWeekShort: ["нед", "пон", "вто", "сре", "чет", "пет", "саб"],
          dayOfWeek: ["Недела", "Понеделник", "Вторник", "Среда", "Четврток", "Петок", "Сабота"]
        },
        mn: {
          months: ["1-р сар", "2-р сар", "3-р сар", "4-р сар", "5-р сар", "6-р сар", "7-р сар", "8-р сар", "9-р сар", "10-р сар", "11-р сар", "12-р сар"],
          dayOfWeekShort: ["Дав", "Мяг", "Лха", "Пүр", "Бсн", "Бям", "Ням"],
          dayOfWeek: ["Даваа", "Мягмар", "Лхагва", "Пүрэв", "Баасан", "Бямба", "Ням"]
        },
        "pt-BR": {
          months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
          dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
          dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
        },
        sk: {
          months: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
          dayOfWeekShort: ["Ne", "Po", "Ut", "St", "Št", "Pi", "So"],
          dayOfWeek: ["Nedeľa", "Pondelok", "Utorok", "Streda", "Štvrtok", "Piatok", "Sobota"]
        },
        sq: {
          months: ["Janar", "Shkurt", "Mars", "Prill", "Maj", "Qershor", "Korrik", "Gusht", "Shtator", "Tetor", "Nëntor", "Dhjetor"],
          dayOfWeekShort: ["Die", "Hën", "Mar", "Mër", "Enj", "Pre", "Shtu"],
          dayOfWeek: ["E Diel", "E Hënë", "E Martē", "E Mërkurë", "E Enjte", "E Premte", "E Shtunë"]
        },
        "sr-YU": {
          months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
          dayOfWeekShort: ["Ned", "Pon", "Uto", "Sre", "čet", "Pet", "Sub"],
          dayOfWeek: ["Nedelja", "Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota"]
        },
        sr: {
          months: ["јануар", "фебруар", "март", "април", "мај", "јун", "јул", "август", "септембар", "октобар", "новембар", "децембар"],
          dayOfWeekShort: ["нед", "пон", "уто", "сре", "чет", "пет", "суб"],
          dayOfWeek: ["Недеља", "Понедељак", "Уторак", "Среда", "Четвртак", "Петак", "Субота"]
        },
        sv: {
          months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
          dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
          dayOfWeek: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"]
        },
        "zh-TW": {
          months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
          dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
          dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
        },
        zh: {
          months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
          dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
          dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
        },
        ug: {
          months: ["1-ئاي", "2-ئاي", "3-ئاي", "4-ئاي", "5-ئاي", "6-ئاي", "7-ئاي", "8-ئاي", "9-ئاي", "10-ئاي", "11-ئاي", "12-ئاي"],
          dayOfWeek: ["يەكشەنبە", "دۈشەنبە", "سەيشەنبە", "چارشەنبە", "پەيشەنبە", "جۈمە", "شەنبە"]
        },
        he: {
          months: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
          dayOfWeekShort: ["א'", "ב'", "ג'", "ד'", "ה'", "ו'", "שבת"],
          dayOfWeek: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת", "ראשון"]
        },
        hy: {
          months: ["Հունվար", "Փետրվար", "Մարտ", "Ապրիլ", "Մայիս", "Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր"],
          dayOfWeekShort: ["Կի", "Երկ", "Երք", "Չոր", "Հնգ", "Ուրբ", "Շբթ"],
          dayOfWeek: ["Կիրակի", "Երկուշաբթի", "Երեքշաբթի", "Չորեքշաբթի", "Հինգշաբթի", "Ուրբաթ", "Շաբաթ"]
        },
        kg: {
          months: ["Үчтүн айы", "Бирдин айы", "Жалган Куран", "Чын Куран", "Бугу", "Кулжа", "Теке", "Баш Оона", "Аяк Оона", "Тогуздун айы", "Жетинин айы", "Бештин айы"],
          dayOfWeekShort: ["Жек", "Дүй", "Шей", "Шар", "Бей", "Жум", "Ише"],
          dayOfWeek: ["Жекшемб", "Дүйшөмб", "Шейшемб", "Шаршемб", "Бейшемби", "Жума", "Ишенб"]
        },
        rm: {
          months: ["Schaner", "Favrer", "Mars", "Avrigl", "Matg", "Zercladur", "Fanadur", "Avust", "Settember", "October", "November", "December"],
          dayOfWeekShort: ["Du", "Gli", "Ma", "Me", "Gie", "Ve", "So"],
          dayOfWeek: ["Dumengia", "Glindesdi", "Mardi", "Mesemna", "Gievgia", "Venderdi", "Sonda"]
        },
        ka: {
          months: ["იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი", "დეკემბერი"],
          dayOfWeekShort: ["კვ", "ორშ", "სამშ", "ოთხ", "ხუთ", "პარ", "შაბ"],
          dayOfWeek: ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"]
        }
      },
      ownerDocument: document,
      contentWindow: window,
      value: "",
      rtl: !1,
      format: "Y/m/d H:i",
      formatTime: "H:i",
      formatDate: "Y/m/d",
      startDate: !1,
      step: 60,
      monthChangeSpinner: !0,
      closeOnDateSelect: !1,
      closeOnTimeSelect: !0,
      closeOnWithoutClick: !0,
      closeOnInputClick: !0,
      openOnFocus: !0,
      timepicker: !0,
      datepicker: !0,
      weeks: !1,
      defaultTime: !1,
      defaultDate: !1,
      minDate: !1,
      maxDate: !1,
      minTime: !1,
      maxTime: !1,
      minDateTime: !1,
      maxDateTime: !1,
      allowTimes: [],
      opened: !1,
      initTime: !0,
      inline: !1,
      theme: "",
      touchMovedThreshold: 5,
      onSelectDate: function () {},
      onSelectTime: function () {},
      onChangeMonth: function () {},
      onGetWeekOfYear: function () {},
      onChangeYear: function () {},
      onChangeDateTime: function () {},
      onShow: function () {},
      onClose: function () {},
      onGenerate: function () {},
      withoutCopyright: !0,
      inverseButton: !1,
      hours12: !1,
      next: "xdsoft_next",
      prev: "xdsoft_prev",
      dayOfWeekStart: 0,
      parentID: "body",
      timeHeightInTimePicker: 25,
      timepickerScrollbar: !0,
      todayButton: !0,
      prevButton: !0,
      nextButton: !0,
      defaultSelect: !0,
      scrollMonth: !0,
      scrollTime: !0,
      scrollInput: !0,
      lazyInit: !1,
      mask: !1,
      validateOnBlur: !0,
      allowBlank: !0,
      yearStart: 1950,
      yearEnd: 2050,
      monthStart: 0,
      monthEnd: 11,
      style: "",
      id: "",
      fixed: !1,
      roundTime: "round",
      className: "",
      weekends: [],
      highlightedDates: [],
      highlightedPeriods: [],
      allowDates: [],
      allowDateRe: null,
      disabledDates: [],
      disabledWeekDays: [],
      yearOffset: 0,
      beforeShowDay: null,
      enterLikeTab: !0,
      showApplyButton: !1,
      insideParent: !1
    },
    E = null,
    n = null,
    R = "en",
    a = {
      meridiem: ["AM", "PM"]
    },
    r = function () {
      var e = s.i18n[R],
        t = {
          days: e.dayOfWeek,
          daysShort: e.dayOfWeekShort,
          months: e.months,
          monthsShort: L.map(e.months, function (e) {
            return e.substring(0, 3)
          })
        };
      "function" == typeof DateFormatter && (E = n = new DateFormatter({
        dateSettings: L.extend({}, a, t)
      }))
    },
    o = {
      moment: {
        default_options: {
          format: "YYYY/MM/DD HH:mm",
          formatDate: "YYYY/MM/DD",
          formatTime: "HH:mm"
        },
        formatter: {
          parseDate: function (e, t) {
            if (i(t)) return n.parseDate(e, t);
            var a = moment(e, t);
            return !!a.isValid() && a.toDate()
          },
          formatDate: function (e, t) {
            return i(t) ? n.formatDate(e, t) : moment(e).format(t)
          },
          formatMask: function (e) {
            return e.replace(/Y{4}/g, "9999").replace(/Y{2}/g, "99").replace(/M{2}/g, "19").replace(/D{2}/g, "39").replace(/H{2}/g, "29").replace(/m{2}/g, "59").replace(/s{2}/g, "59")
          }
        }
      }
    };
  L.datetimepicker = {
    setLocale: function (e) {
      var t = s.i18n[e] ? e : "en";
      R !== t && (R = t, r())
    },
    setDateFormatter: function (e) {
      if ("string" == typeof e && o.hasOwnProperty(e)) {
        var t = o[e];
        L.extend(s, t.default_options), E = t.formatter
      } else E = e
    }
  };
  var t = {
      RFC_2822: "D, d M Y H:i:s O",
      ATOM: "Y-m-dTH:i:sP",
      ISO_8601: "Y-m-dTH:i:sO",
      RFC_822: "D, d M y H:i:s O",
      RFC_850: "l, d-M-y H:i:s T",
      RFC_1036: "D, d M y H:i:s O",
      RFC_1123: "D, d M Y H:i:s O",
      RSS: "D, d M Y H:i:s O",
      W3C: "Y-m-dTH:i:sP"
    },
    i = function (e) {
      return -1 !== Object.values(t).indexOf(e)
    };

  function m(e, t, a) {
    this.date = e, this.desc = t, this.style = a
  }
  L.extend(L.datetimepicker, t), r(), window.getComputedStyle || (window.getComputedStyle = function (a) {
    return this.el = a, this.getPropertyValue = function (e) {
      var t = /(-([a-z]))/g;
      return "float" === e && (e = "styleFloat"), t.test(e) && (e = e.replace(t, function (e, t, a) {
        return a.toUpperCase()
      })), a.currentStyle[e] || null
    }, this
  }), Array.prototype.indexOf || (Array.prototype.indexOf = function (e, t) {
    var a, n;
    for (a = t || 0, n = this.length; a < n; a += 1)
      if (this[a] === e) return a;
    return -1
  }), Date.prototype.countDaysInMonth = function () {
    return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate()
  }, L.fn.xdsoftScroller = function (p, D) {
    return this.each(function () {
      var o, i, s, d, u, l = L(this),
        a = function (e) {
          var t, a = {
            x: 0,
            y: 0
          };
          return "touchstart" === e.type || "touchmove" === e.type || "touchend" === e.type || "touchcancel" === e.type ? (t = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0], a.x = t.clientX, a.y = t.clientY) : "mousedown" !== e.type && "mouseup" !== e.type && "mousemove" !== e.type && "mouseover" !== e.type && "mouseout" !== e.type && "mouseenter" !== e.type && "mouseleave" !== e.type || (a.x = e.clientX, a.y = e.clientY), a
        },
        f = 100,
        n = !1,
        r = 0,
        c = 0,
        m = 0,
        t = !1,
        h = 0,
        g = function () {};
      "hide" !== D ? (L(this).hasClass("xdsoft_scroller_box") || (o = l.children().eq(0), i = l[0].clientHeight, s = o[0].offsetHeight, d = L('<div class="xdsoft_scrollbar"></div>'), u = L('<div class="xdsoft_scroller"></div>'), d.append(u), l.addClass("xdsoft_scroller_box").append(d), g = function (e) {
        var t = a(e).y - r + h;
        t < 0 && (t = 0), t + u[0].offsetHeight > m && (t = m - u[0].offsetHeight), l.trigger("scroll_element.xdsoft_scroller", [f ? t / f : 0])
      }, u.on("touchstart.xdsoft_scroller mousedown.xdsoft_scroller", function (e) {
        i || l.trigger("resize_scroll.xdsoft_scroller", [D]), r = a(e).y, h = parseInt(u.css("margin-top"), 10), m = d[0].offsetHeight, "mousedown" === e.type || "touchstart" === e.type ? (p.ownerDocument && L(p.ownerDocument.body).addClass("xdsoft_noselect"), L([p.ownerDocument.body, p.contentWindow]).on("touchend mouseup.xdsoft_scroller", function e() {
          L([p.ownerDocument.body, p.contentWindow]).off("touchend mouseup.xdsoft_scroller", e).off("mousemove.xdsoft_scroller", g).removeClass("xdsoft_noselect")
        }), L(p.ownerDocument.body).on("mousemove.xdsoft_scroller", g)) : (t = !0, e.stopPropagation(), e.preventDefault())
      }).on("touchmove", function (e) {
        t && (e.preventDefault(), g(e))
      }).on("touchend touchcancel", function () {
        t = !1, h = 0
      }), l.on("scroll_element.xdsoft_scroller", function (e, t) {
        i || l.trigger("resize_scroll.xdsoft_scroller", [t, !0]), t = 1 < t ? 1 : t < 0 || isNaN(t) ? 0 : t, u.css("margin-top", f * t), setTimeout(function () {
          o.css("marginTop", -parseInt((o[0].offsetHeight - i) * t, 10))
        }, 10)
      }).on("resize_scroll.xdsoft_scroller", function (e, t, a) {
        var n, r;
        i = l[0].clientHeight, s = o[0].offsetHeight, r = (n = i / s) * d[0].offsetHeight, 1 < n ? u.hide() : (u.show(), u.css("height", parseInt(10 < r ? r : 10, 10)), f = d[0].offsetHeight - u[0].offsetHeight, !0 !== a && l.trigger("scroll_element.xdsoft_scroller", [t || Math.abs(parseInt(o.css("marginTop"), 10)) / (s - i)]))
      }), l.on("mousewheel", function (e) {
        var t = Math.abs(parseInt(o.css("marginTop"), 10));
        return (t -= 20 * e.deltaY) < 0 && (t = 0), l.trigger("scroll_element.xdsoft_scroller", [t / (s - i)]), e.stopPropagation(), !1
      }), l.on("touchstart", function (e) {
        n = a(e), c = Math.abs(parseInt(o.css("marginTop"), 10))
      }), l.on("touchmove", function (e) {
        if (n) {
          e.preventDefault();
          var t = a(e);
          l.trigger("scroll_element.xdsoft_scroller", [(c - (t.y - n.y)) / (s - i)])
        }
      }), l.on("touchend touchcancel", function () {
        n = !1, c = 0
      })), l.trigger("resize_scroll.xdsoft_scroller", [D])) : l.find(".xdsoft_scrollbar").hide()
    })
  }, L.fn.datetimepicker = function (H, a) {
    var n, r, o = this,
      p = 17,
      D = 13,
      y = 27,
      v = 37,
      b = 38,
      k = 39,
      x = 40,
      T = 9,
      S = 116,
      M = 65,
      w = 67,
      j = 86,
      J = 90,
      z = 89,
      I = !1,
      N = L.isPlainObject(H) || !H ? L.extend(!0, {}, s, H) : L.extend(!0, {}, s),
      i = 0;
    return n = function (O) {
      var t, n, a, r, W, h, _ = L('<div class="xdsoft_datetimepicker xdsoft_noselect"></div>'),
        e = L('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
        g = L('<div class="xdsoft_datepicker active"></div>'),
        F = L('<div class="xdsoft_monthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button><div class="xdsoft_label xdsoft_month"><span></span><i></i></div><div class="xdsoft_label xdsoft_year"><span></span><i></i></div><button type="button" class="xdsoft_next"></button></div>'),
        C = L('<div class="xdsoft_calendar"></div>'),
        o = L('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),
        u = o.find(".xdsoft_time_box").eq(0),
        P = L('<div class="xdsoft_time_variant"></div>'),
        i = L('<button type="button" class="xdsoft_save_selected blue-gradient-button">Save Selected</button>'),
        Y = L('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>'),
        A = L('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>'),
        s = !1,
        d = 0;
      N.id && _.attr("id", N.id), N.style && _.attr("style", N.style), N.weeks && _.addClass("xdsoft_showweeks"), N.rtl && _.addClass("xdsoft_rtl"), _.addClass("xdsoft_" + N.theme), _.addClass(N.className), F.find(".xdsoft_month span").after(Y), F.find(".xdsoft_year span").after(A), F.find(".xdsoft_month,.xdsoft_year").on("touchstart mousedown.xdsoft", function (e) {
        var t, a, n = L(this).find(".xdsoft_select").eq(0),
          r = 0,
          o = 0,
          i = n.is(":visible");
        for (F.find(".xdsoft_select").hide(), W.currentTime && (r = W.currentTime[L(this).hasClass("xdsoft_month") ? "getMonth" : "getFullYear"]()), n[i ? "hide" : "show"](), t = n.find("div.xdsoft_option"), a = 0; a < t.length && t.eq(a).data("value") !== r; a += 1) o += t[0].offsetHeight;
        return n.xdsoftScroller(N, o / (n.children()[0].offsetHeight - n[0].clientHeight)), e.stopPropagation(), !1
      });
      var l = function (e) {
        var t = e.originalEvent,
          a = t.touches ? t.touches[0] : t;
        this.touchStartPosition = this.touchStartPosition || a;
        var n = Math.abs(this.touchStartPosition.clientX - a.clientX),
          r = Math.abs(this.touchStartPosition.clientY - a.clientY);
        Math.sqrt(n * n + r * r) > N.touchMovedThreshold && (this.touchMoved = !0)
      };

      function f() {
        var e, t = !1;
        return N.startDate ? t = W.strToDate(N.startDate) : (t = N.value || (O && O.val && O.val() ? O.val() : "")) ? (t = W.strToDateTime(t), N.yearOffset && (t = new Date(t.getFullYear() - N.yearOffset, t.getMonth(), t.getDate(), t.getHours(), t.getMinutes(), t.getSeconds(), t.getMilliseconds()))) : N.defaultDate && (t = W.strToDateTime(N.defaultDate), N.defaultTime && (e = W.strtotime(N.defaultTime), t.setHours(e.getHours()), t.setMinutes(e.getMinutes()))), t && W.isValidDate(t) ? _.data("changed", !0) : t = "", t || 0
      }

      function c(m) {
        var h = function (e, t) {
            var a = e.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, "\\$1").replace(/_/g, "{digit+}").replace(/([0-9]{1})/g, "{digit$1}").replace(/\{digit([0-9]{1})\}/g, "[0-$1_]{1}").replace(/\{digit[\+]\}/g, "[0-9_]{1}");
            return new RegExp(a).test(t)
          },
          g = function (e, t) {
            if (!(e = "string" == typeof e || e instanceof String ? m.ownerDocument.getElementById(e) : e)) return !1;
            if (e.createTextRange) {
              var a = e.createTextRange();
              return a.collapse(!0), a.moveEnd("character", t), a.moveStart("character", t), a.select(), !0
            }
            return !!e.setSelectionRange && (e.setSelectionRange(t, t), !0)
          };
        m.mask && O.off("keydown.xdsoft"), !0 === m.mask && (E.formatMask ? m.mask = E.formatMask(m.format) : m.mask = m.format.replace(/Y/g, "9999").replace(/F/g, "9999").replace(/m/g, "19").replace(/d/g, "39").replace(/H/g, "29").replace(/i/g, "59").replace(/s/g, "59")), "string" === L.type(m.mask) && (h(m.mask, O.val()) || (O.val(m.mask.replace(/[0-9]/g, "_")), g(O[0], 0)), O.on("paste.xdsoft", function (e) {
          var t = (e.clipboardData || e.originalEvent.clipboardData || window.clipboardData).getData("text"),
            a = this.value,
            n = this.selectionStart;
          return a = a.substr(0, n) + t + a.substr(n + t.length), n += t.length, h(m.mask, a) ? (this.value = a, g(this, n)) : "" === L.trim(a) ? this.value = m.mask.replace(/[0-9]/g, "_") : O.trigger("error_input.xdsoft"), e.preventDefault(), !1
        }), O.on("keydown.xdsoft", function (e) {
          var t, a = this.value,
            n = e.which,
            r = this.selectionStart,
            o = this.selectionEnd,
            i = r !== o;
          if (48 <= n && n <= 57 || 96 <= n && n <= 105 || 8 === n || 46 === n) {
            for (t = 8 === n || 46 === n ? "_" : String.fromCharCode(96 <= n && n <= 105 ? n - 48 : n), 8 === n && r && !i && (r -= 1);;) {
              var s = m.mask.substr(r, 1),
                d = r < m.mask.length,
                u = 0 < r;
              if (!(/[^0-9_]/.test(s) && d && u)) break;
              r += 8 !== n || i ? 1 : -1
            }
            if (e.metaKey && (i = !(r = 0)), i) {
              var l = o - r,
                f = m.mask.replace(/[0-9]/g, "_"),
                c = f.substr(r, l).substr(1);
              a = a.substr(0, r) + (t + c) + a.substr(r + l)
            } else {
              a = a.substr(0, r) + t + a.substr(r + 1)
            }
            if ("" === L.trim(a)) a = f;
            else if (r === m.mask.length) return e.preventDefault(), !1;
            for (r += 8 === n ? 0 : 1;
              /[^0-9_]/.test(m.mask.substr(r, 1)) && r < m.mask.length && 0 < r;) r += 8 === n ? 0 : 1;
            h(m.mask, a) ? (this.value = a, g(this, r)) : "" === L.trim(a) ? this.value = m.mask.replace(/[0-9]/g, "_") : O.trigger("error_input.xdsoft")
          } else if (-1 !== [M, w, j, J, z].indexOf(n) && I || -1 !== [y, b, x, v, k, S, p, T, D].indexOf(n)) return !0;
          return e.preventDefault(), !1
        }))
      }
      F.find(".xdsoft_select").xdsoftScroller(N).on("touchstart mousedown.xdsoft", function (e) {
        var t = e.originalEvent;
        this.touchMoved = !1, this.touchStartPosition = t.touches ? t.touches[0] : t, e.stopPropagation(), e.preventDefault()
      }).on("touchmove", ".xdsoft_option", l).on("touchend mousedown.xdsoft", ".xdsoft_option", function () {
        if (!this.touchMoved) {
          void 0 !== W.currentTime && null !== W.currentTime || (W.currentTime = W.now());
          var e = W.currentTime.getFullYear();
          W && W.currentTime && W.currentTime[L(this).parent().parent().hasClass("xdsoft_monthselect") ? "setMonth" : "setFullYear"](L(this).data("value")), L(this).parent().parent().hide(), _.trigger("xchange.xdsoft"), N.onChangeMonth && L.isFunction(N.onChangeMonth) && N.onChangeMonth.call(_, W.currentTime, _.data("input")), e !== W.currentTime.getFullYear() && L.isFunction(N.onChangeYear) && N.onChangeYear.call(_, W.currentTime, _.data("input"))
        }
      }), _.getValue = function () {
        return W.getCurrentTime()
      }, _.setOptions = function (e) {
        var l = {};
        N = L.extend(!0, {}, N, e), e.allowTimes && L.isArray(e.allowTimes) && e.allowTimes.length && (N.allowTimes = L.extend(!0, [], e.allowTimes)), e.weekends && L.isArray(e.weekends) && e.weekends.length && (N.weekends = L.extend(!0, [], e.weekends)), e.allowDates && L.isArray(e.allowDates) && e.allowDates.length && (N.allowDates = L.extend(!0, [], e.allowDates)), e.allowDateRe && "[object String]" === Object.prototype.toString.call(e.allowDateRe) && (N.allowDateRe = new RegExp(e.allowDateRe)), e.highlightedDates && L.isArray(e.highlightedDates) && e.highlightedDates.length && (L.each(e.highlightedDates, function (e, t) {
          var a, n = L.map(t.split(","), L.trim),
            r = new m(E.parseDate(n[0], N.formatDate), n[1], n[2]),
            o = E.formatDate(r.date, N.formatDate);
          void 0 !== l[o] ? (a = l[o].desc) && a.length && r.desc && r.desc.length && (l[o].desc = a + "\n" + r.desc) : l[o] = r
        }), N.highlightedDates = L.extend(!0, [], l)), e.highlightedPeriods && L.isArray(e.highlightedPeriods) && e.highlightedPeriods.length && (l = L.extend(!0, [], N.highlightedDates), L.each(e.highlightedPeriods, function (e, t) {
          var a, n, r, o, i, s, d;
          if (L.isArray(t)) a = t[0], n = t[1], r = t[2], d = t[3];
          else {
            var u = L.map(t.split(","), L.trim);
            a = E.parseDate(u[0], N.formatDate), n = E.parseDate(u[1], N.formatDate), r = u[2], d = u[3]
          }
          for (; a <= n;) o = new m(a, r, d), i = E.formatDate(a, N.formatDate), a.setDate(a.getDate() + 1), void 0 !== l[i] ? (s = l[i].desc) && s.length && o.desc && o.desc.length && (l[i].desc = s + "\n" + o.desc) : l[i] = o
        }), N.highlightedDates = L.extend(!0, [], l)), e.disabledDates && L.isArray(e.disabledDates) && e.disabledDates.length && (N.disabledDates = L.extend(!0, [], e.disabledDates)), e.disabledWeekDays && L.isArray(e.disabledWeekDays) && e.disabledWeekDays.length && (N.disabledWeekDays = L.extend(!0, [], e.disabledWeekDays)), !N.open && !N.opened || N.inline || O.trigger("open.xdsoft"), N.inline && (s = !0, _.addClass("xdsoft_inline"), O.after(_).hide()), N.inverseButton && (N.next = "xdsoft_prev", N.prev = "xdsoft_next"), N.datepicker ? g.addClass("active") : g.removeClass("active"), N.timepicker ? o.addClass("active") : o.removeClass("active"), N.value && (W.setCurrentTime(N.value), O && O.val && O.val(W.str)), isNaN(N.dayOfWeekStart) ? N.dayOfWeekStart = 0 : N.dayOfWeekStart = parseInt(N.dayOfWeekStart, 10) % 7, N.timepickerScrollbar || u.xdsoftScroller(N, "hide"), N.minDate && /^[\+\-](.*)$/.test(N.minDate) && (N.minDate = E.formatDate(W.strToDateTime(N.minDate), N.formatDate)), N.maxDate && /^[\+\-](.*)$/.test(N.maxDate) && (N.maxDate = E.formatDate(W.strToDateTime(N.maxDate), N.formatDate)), N.minDateTime && /^\+(.*)$/.test(N.minDateTime) && (N.minDateTime = W.strToDateTime(N.minDateTime).dateFormat(N.formatDate)), N.maxDateTime && /^\+(.*)$/.test(N.maxDateTime) && (N.maxDateTime = W.strToDateTime(N.maxDateTime).dateFormat(N.formatDate)), i.toggle(N.showApplyButton), F.find(".xdsoft_today_button").css("visibility", N.todayButton ? "visible" : "hidden"), F.find("." + N.prev).css("visibility", N.prevButton ? "visible" : "hidden"), F.find("." + N.next).css("visibility", N.nextButton ? "visible" : "hidden"), c(N), N.validateOnBlur && O.off("blur.xdsoft").on("blur.xdsoft", function () {
          if (N.allowBlank && (!L.trim(L(this).val()).length || "string" == typeof N.mask && L.trim(L(this).val()) === N.mask.replace(/[0-9]/g, "_"))) L(this).val(null), _.data("xdsoft_datetime").empty();
          else {
            var e = E.parseDate(L(this).val(), N.format);
            if (e) L(this).val(E.formatDate(e, N.format));
            else {
              var t = +[L(this).val()[0], L(this).val()[1]].join(""),
                a = +[L(this).val()[2], L(this).val()[3]].join("");
              !N.datepicker && N.timepicker && 0 <= t && t < 24 && 0 <= a && a < 60 ? L(this).val([t, a].map(function (e) {
                return 9 < e ? e : "0" + e
              }).join(":")) : L(this).val(E.formatDate(W.now(), N.format))
            }
            _.data("xdsoft_datetime").setCurrentTime(L(this).val())
          }
          _.trigger("changedatetime.xdsoft"), _.trigger("close.xdsoft")
        }), N.dayOfWeekStartPrev = 0 === N.dayOfWeekStart ? 6 : N.dayOfWeekStart - 1, _.trigger("xchange.xdsoft").trigger("afterOpen.xdsoft")
      }, _.data("options", N).on("touchstart mousedown.xdsoft", function (e) {
        return e.stopPropagation(), e.preventDefault(), A.hide(), Y.hide(), !1
      }), u.append(P), u.xdsoftScroller(N), _.on("afterOpen.xdsoft", function () {
        u.xdsoftScroller(N)
      }), _.append(g).append(o), !0 !== N.withoutCopyright && _.append(e), g.append(F).append(C).append(i), N.insideParent ? L(O).parent().append(_) : L(N.parentID).append(_), W = new function () {
        var r = this;
        r.now = function (e) {
          var t, a, n = new Date;
          return !e && N.defaultDate && (t = r.strToDateTime(N.defaultDate), n.setFullYear(t.getFullYear()), n.setMonth(t.getMonth()), n.setDate(t.getDate())), n.setFullYear(n.getFullYear()), !e && N.defaultTime && (a = r.strtotime(N.defaultTime), n.setHours(a.getHours()), n.setMinutes(a.getMinutes()), n.setSeconds(a.getSeconds()), n.setMilliseconds(a.getMilliseconds())), n
        }, r.isValidDate = function (e) {
          return "[object Date]" === Object.prototype.toString.call(e) && !isNaN(e.getTime())
        }, r.setCurrentTime = function (e, t) {
          "string" == typeof e ? r.currentTime = r.strToDateTime(e) : r.isValidDate(e) ? r.currentTime = e : e || t || !N.allowBlank || N.inline ? r.currentTime = r.now() : r.currentTime = null, _.trigger("xchange.xdsoft")
        }, r.empty = function () {
          r.currentTime = null
        }, r.getCurrentTime = function () {
          return r.currentTime
        }, r.nextMonth = function () {
          void 0 !== r.currentTime && null !== r.currentTime || (r.currentTime = r.now());
          var e, t = r.currentTime.getMonth() + 1;
          return 12 === t && (r.currentTime.setFullYear(r.currentTime.getFullYear() + 1), t = 0), e = r.currentTime.getFullYear(), r.currentTime.setDate(Math.min(new Date(r.currentTime.getFullYear(), t + 1, 0).getDate(), r.currentTime.getDate())), r.currentTime.setMonth(t), N.onChangeMonth && L.isFunction(N.onChangeMonth) && N.onChangeMonth.call(_, W.currentTime, _.data("input")), e !== r.currentTime.getFullYear() && L.isFunction(N.onChangeYear) && N.onChangeYear.call(_, W.currentTime, _.data("input")), _.trigger("xchange.xdsoft"), t
        }, r.prevMonth = function () {
          void 0 !== r.currentTime && null !== r.currentTime || (r.currentTime = r.now());
          var e = r.currentTime.getMonth() - 1;
          return -1 === e && (r.currentTime.setFullYear(r.currentTime.getFullYear() - 1), e = 11), r.currentTime.setDate(Math.min(new Date(r.currentTime.getFullYear(), e + 1, 0).getDate(), r.currentTime.getDate())), r.currentTime.setMonth(e), N.onChangeMonth && L.isFunction(N.onChangeMonth) && N.onChangeMonth.call(_, W.currentTime, _.data("input")), _.trigger("xchange.xdsoft"), e
        }, r.getWeekOfYear = function (e) {
          if (N.onGetWeekOfYear && L.isFunction(N.onGetWeekOfYear)) {
            var t = N.onGetWeekOfYear.call(_, e);
            if (void 0 !== t) return t
          }
          var a = new Date(e.getFullYear(), 0, 1);
          return 4 !== a.getDay() && a.setMonth(0, 1 + (4 - a.getDay() + 7) % 7), Math.ceil(((e - a) / 864e5 + a.getDay() + 1) / 7)
        }, r.strToDateTime = function (e) {
          var t, a, n = [];
          return e && e instanceof Date && r.isValidDate(e) ? e : ((n = /^([+-]{1})(.*)$/.exec(e)) && (n[2] = E.parseDate(n[2], N.formatDate)), a = n && n[2] ? (t = n[2].getTime() - 6e4 * n[2].getTimezoneOffset(), new Date(r.now(!0).getTime() + parseInt(n[1] + "1", 10) * t)) : e ? E.parseDate(e, N.format) : r.now(), r.isValidDate(a) || (a = r.now()), a)
        }, r.strToDate = function (e) {
          if (e && e instanceof Date && r.isValidDate(e)) return e;
          var t = e ? E.parseDate(e, N.formatDate) : r.now(!0);
          return r.isValidDate(t) || (t = r.now(!0)), t
        }, r.strtotime = function (e) {
          if (e && e instanceof Date && r.isValidDate(e)) return e;
          var t = e ? E.parseDate(e, N.formatTime) : r.now(!0);
          return r.isValidDate(t) || (t = r.now(!0)), t
        }, r.str = function () {
          var e = N.format;
          return N.yearOffset && (e = (e = e.replace("Y", r.currentTime.getFullYear() + N.yearOffset)).replace("y", String(r.currentTime.getFullYear() + N.yearOffset).substring(2, 4))), E.formatDate(r.currentTime, e)
        }, r.currentTime = this.now()
      }, i.on("touchend click", function (e) {
        e.preventDefault(), _.data("changed", !0), W.setCurrentTime(f()), O.val(W.str()), _.trigger("close.xdsoft")
      }), F.find(".xdsoft_today_button").on("touchend mousedown.xdsoft", function () {
        _.data("changed", !0), W.setCurrentTime(0, !0), _.trigger("afterOpen.xdsoft")
      }).on("dblclick.xdsoft", function () {
        var e, t, a = W.getCurrentTime();
        a = new Date(a.getFullYear(), a.getMonth(), a.getDate()), e = W.strToDate(N.minDate), a < (e = new Date(e.getFullYear(), e.getMonth(), e.getDate())) || (t = W.strToDate(N.maxDate), (t = new Date(t.getFullYear(), t.getMonth(), t.getDate())) < a || (O.val(W.str()), O.trigger("change"), _.trigger("close.xdsoft")))
      }), F.find(".xdsoft_prev,.xdsoft_next").on("touchend mousedown.xdsoft", function () {
        var a = L(this),
          n = 0,
          r = !1;
        ! function e(t) {
          a.hasClass(N.next) ? W.nextMonth() : a.hasClass(N.prev) && W.prevMonth(), N.monthChangeSpinner && (r || (n = setTimeout(e, t || 100)))
        }(500), L([N.ownerDocument.body, N.contentWindow]).on("touchend mouseup.xdsoft", function e() {
          clearTimeout(n), r = !0, L([N.ownerDocument.body, N.contentWindow]).off("touchend mouseup.xdsoft", e)
        })
      }), o.find(".xdsoft_prev,.xdsoft_next").on("touchend mousedown.xdsoft", function () {
        var o = L(this),
          i = 0,
          s = !1,
          d = 110;
        ! function e(t) {
          var a = u[0].clientHeight,
            n = P[0].offsetHeight,
            r = Math.abs(parseInt(P.css("marginTop"), 10));
          o.hasClass(N.next) && n - a - N.timeHeightInTimePicker >= r ? P.css("marginTop", "-" + (r + N.timeHeightInTimePicker) + "px") : o.hasClass(N.prev) && 0 <= r - N.timeHeightInTimePicker && P.css("marginTop", "-" + (r - N.timeHeightInTimePicker) + "px"), u.trigger("scroll_element.xdsoft_scroller", [Math.abs(parseInt(P[0].style.marginTop, 10) / (n - a))]), d = 10 < d ? 10 : d - 10, s || (i = setTimeout(e, t || d))
        }(500), L([N.ownerDocument.body, N.contentWindow]).on("touchend mouseup.xdsoft", function e() {
          clearTimeout(i), s = !0, L([N.ownerDocument.body, N.contentWindow]).off("touchend mouseup.xdsoft", e)
        })
      }), t = 0, _.on("xchange.xdsoft", function (e) {
        clearTimeout(t), t = setTimeout(function () {
          void 0 !== W.currentTime && null !== W.currentTime || (W.currentTime = W.now());
          for (var e, t, a, n, r, o, i, s, d, u, l = "", f = new Date(W.currentTime.getFullYear(), W.currentTime.getMonth(), 1, 12, 0, 0), c = 0, m = W.now(), h = !1, g = !1, p = !1, D = !1, y = [], v = !0, b = ""; f.getDay() !== N.dayOfWeekStart;) f.setDate(f.getDate() - 1);
          for (l += "<table><thead><tr>", N.weeks && (l += "<th></th>"), e = 0; e < 7; e += 1) l += "<th>" + N.i18n[R].dayOfWeekShort[(e + N.dayOfWeekStart) % 7] + "</th>";
          for (l += "</tr></thead>", l += "<tbody>", !1 !== N.maxDate && (h = W.strToDate(N.maxDate), h = new Date(h.getFullYear(), h.getMonth(), h.getDate(), 23, 59, 59, 999)), !1 !== N.minDate && (g = W.strToDate(N.minDate), g = new Date(g.getFullYear(), g.getMonth(), g.getDate())), !1 !== N.minDateTime && (p = W.strToDate(N.minDateTime), p = new Date(p.getFullYear(), p.getMonth(), p.getDate(), p.getHours(), p.getMinutes(), p.getSeconds())), !1 !== N.maxDateTime && (D = W.strToDate(N.maxDateTime), D = new Date(D.getFullYear(), D.getMonth(), D.getDate(), D.getHours(), D.getMinutes(), D.getSeconds())), !1 !== D && (u = 31 * (12 * D.getFullYear() + D.getMonth()) + D.getDate()); c < W.currentTime.countDaysInMonth() || f.getDay() !== N.dayOfWeekStart || W.currentTime.getMonth() === f.getMonth();) {
            y = [], c += 1, a = f.getDay(), n = f.getDate(), r = f.getFullYear(), M = f.getMonth(), o = W.getWeekOfYear(f), d = "", y.push("xdsoft_date"), i = N.beforeShowDay && L.isFunction(N.beforeShowDay.call) ? N.beforeShowDay.call(_, f) : null, N.allowDateRe && "[object RegExp]" === Object.prototype.toString.call(N.allowDateRe) && (N.allowDateRe.test(E.formatDate(f, N.formatDate)) || y.push("xdsoft_disabled")), N.allowDates && 0 < N.allowDates.length && -1 === N.allowDates.indexOf(E.formatDate(f, N.formatDate)) && y.push("xdsoft_disabled");
            var k = 31 * (12 * f.getFullYear() + f.getMonth()) + f.getDate();
            (!1 !== h && h < f || !1 !== p && f < p || !1 !== g && f < g || !1 !== D && u < k || i && !1 === i[0]) && y.push("xdsoft_disabled"), -1 !== N.disabledDates.indexOf(E.formatDate(f, N.formatDate)) && y.push("xdsoft_disabled"), -1 !== N.disabledWeekDays.indexOf(a) && y.push("xdsoft_disabled"), O.is("[disabled]") && y.push("xdsoft_disabled"), i && "" !== i[1] && y.push(i[1]), W.currentTime.getMonth() !== M && y.push("xdsoft_other_month"), (N.defaultSelect || _.data("changed")) && E.formatDate(W.currentTime, N.formatDate) === E.formatDate(f, N.formatDate) && y.push("xdsoft_current"), E.formatDate(m, N.formatDate) === E.formatDate(f, N.formatDate) && y.push("xdsoft_today"), 0 !== f.getDay() && 6 !== f.getDay() && -1 === N.weekends.indexOf(E.formatDate(f, N.formatDate)) || y.push("xdsoft_weekend"), void 0 !== N.highlightedDates[E.formatDate(f, N.formatDate)] && (t = N.highlightedDates[E.formatDate(f, N.formatDate)], y.push(void 0 === t.style ? "xdsoft_highlighted_default" : t.style), d = void 0 === t.desc ? "" : t.desc), N.beforeShowDay && L.isFunction(N.beforeShowDay) && y.push(N.beforeShowDay(f)), v && (l += "<tr>", v = !1, N.weeks && (l += "<th>" + o + "</th>")), l += '<td data-date="' + n + '" data-month="' + M + '" data-year="' + r + '" class="xdsoft_date xdsoft_day_of_week' + f.getDay() + " " + y.join(" ") + '" title="' + d + '"><div>' + n + "</div></td>", f.getDay() === N.dayOfWeekStartPrev && (l += "</tr>", v = !0), f.setDate(n + 1)
          }
          l += "</tbody></table>", C.html(l), F.find(".xdsoft_label span").eq(0).text(N.i18n[R].months[W.currentTime.getMonth()]), F.find(".xdsoft_label span").eq(1).text(W.currentTime.getFullYear() + N.yearOffset), M = b = "";
          var x = 0;
          if (!1 !== N.minTime) {
            var T = W.strtotime(N.minTime);
            x = 60 * T.getHours() + T.getMinutes()
          }
          var S = 1440;
          if (!1 !== N.maxTime) {
            T = W.strtotime(N.maxTime);
            S = 60 * T.getHours() + T.getMinutes()
          }
          if (!1 !== N.minDateTime) {
            T = W.strToDateTime(N.minDateTime);
            if (E.formatDate(W.currentTime, N.formatDate) === E.formatDate(T, N.formatDate)) {
              var M = 60 * T.getHours() + T.getMinutes();
              x < M && (x = M)
            }
          }
          if (!1 !== N.maxDateTime) {
            T = W.strToDateTime(N.maxDateTime);
            if (E.formatDate(W.currentTime, N.formatDate) === E.formatDate(T, N.formatDate))(M = 60 * T.getHours() + T.getMinutes()) < S && (S = M)
          }
          if (s = function (e, t) {
              var a, n = W.now(),
                r = N.allowTimes && L.isArray(N.allowTimes) && N.allowTimes.length;
              n.setHours(e), e = parseInt(n.getHours(), 10), n.setMinutes(t), t = parseInt(n.getMinutes(), 10), y = [];
              var o = 60 * e + t;
              (O.is("[disabled]") || S <= o || o < x) && y.push("xdsoft_disabled"), (a = new Date(W.currentTime)).setHours(parseInt(W.currentTime.getHours(), 10)), r || a.setMinutes(Math[N.roundTime](W.currentTime.getMinutes() / N.step) * N.step), (N.initTime || N.defaultSelect || _.data("changed")) && a.getHours() === parseInt(e, 10) && (!r && 59 < N.step || a.getMinutes() === parseInt(t, 10)) && (N.defaultSelect || _.data("changed") ? y.push("xdsoft_current") : N.initTime && y.push("xdsoft_init_time")), parseInt(m.getHours(), 10) === parseInt(e, 10) && parseInt(m.getMinutes(), 10) === parseInt(t, 10) && y.push("xdsoft_today"), b += '<div class="xdsoft_time ' + y.join(" ") + '" data-hour="' + e + '" data-minute="' + t + '">' + E.formatDate(n, N.formatTime) + "</div>"
            }, N.allowTimes && L.isArray(N.allowTimes) && N.allowTimes.length)
            for (c = 0; c < N.allowTimes.length; c += 1) s(W.strtotime(N.allowTimes[c]).getHours(), M = W.strtotime(N.allowTimes[c]).getMinutes());
          else
            for (e = c = 0; c < (N.hours12 ? 12 : 24); c += 1)
              for (e = 0; e < 60; e += N.step) {
                var w = 60 * c + e;
                w < x || (S <= w || s((c < 10 ? "0" : "") + c, M = (e < 10 ? "0" : "") + e))
              }
          for (P.html(b), H = "", c = parseInt(N.yearStart, 10); c <= parseInt(N.yearEnd, 10); c += 1) H += '<div class="xdsoft_option ' + (W.currentTime.getFullYear() === c ? "xdsoft_current" : "") + '" data-value="' + c + '">' + (c + N.yearOffset) + "</div>";
          for (A.children().eq(0).html(H), c = parseInt(N.monthStart, 10), H = ""; c <= parseInt(N.monthEnd, 10); c += 1) H += '<div class="xdsoft_option ' + (W.currentTime.getMonth() === c ? "xdsoft_current" : "") + '" data-value="' + c + '">' + N.i18n[R].months[c] + "</div>";
          Y.children().eq(0).html(H), L(_).trigger("generate.xdsoft")
        }, 10), e.stopPropagation()
      }).on("afterOpen.xdsoft", function () {
        var e, t, a, n;
        N.timepicker && (P.find(".xdsoft_current").length ? e = ".xdsoft_current" : P.find(".xdsoft_init_time").length && (e = ".xdsoft_init_time"), e ? (t = u[0].clientHeight, (a = P[0].offsetHeight) - t < (n = P.find(e).index() * N.timeHeightInTimePicker + 1) && (n = a - t), u.trigger("scroll_element.xdsoft_scroller", [parseInt(n, 10) / (a - t)])) : u.trigger("scroll_element.xdsoft_scroller", [0]))
      }), n = 0, C.on("touchend click.xdsoft", "td", function (e) {
        e.stopPropagation(), n += 1;
        var t = L(this),
          a = W.currentTime;
        if (null == a && (W.currentTime = W.now(), a = W.currentTime), t.hasClass("xdsoft_disabled")) return !1;
        a.setDate(1), a.setFullYear(t.data("year")), a.setMonth(t.data("month")), a.setDate(t.data("date")), _.trigger("select.xdsoft", [a]), O.val(W.str()), N.onSelectDate && L.isFunction(N.onSelectDate) && N.onSelectDate.call(_, W.currentTime, _.data("input"), e), _.data("changed", !0), _.trigger("xchange.xdsoft"), _.trigger("changedatetime.xdsoft"), (1 < n || !0 === N.closeOnDateSelect || !1 === N.closeOnDateSelect && !N.timepicker) && !N.inline && _.trigger("close.xdsoft"), setTimeout(function () {
          n = 0
        }, 200)
      }), P.on("touchstart", "div", function (e) {
        this.touchMoved = !1
      }).on("touchmove", "div", l).on("touchend click.xdsoft", "div", function (e) {
        if (!this.touchMoved) {
          e.stopPropagation();
          var t = L(this),
            a = W.currentTime;
          if (null == a && (W.currentTime = W.now(), a = W.currentTime), t.hasClass("xdsoft_disabled")) return !1;
          a.setHours(t.data("hour")), a.setMinutes(t.data("minute")), _.trigger("select.xdsoft", [a]), _.data("input").val(W.str()), N.onSelectTime && L.isFunction(N.onSelectTime) && N.onSelectTime.call(_, W.currentTime, _.data("input"), e), _.data("changed", !0), _.trigger("xchange.xdsoft"), _.trigger("changedatetime.xdsoft"), !0 !== N.inline && !0 === N.closeOnTimeSelect && _.trigger("close.xdsoft")
        }
      }), g.on("mousewheel.xdsoft", function (e) {
        return !N.scrollMonth || (e.deltaY < 0 ? W.nextMonth() : W.prevMonth(), !1)
      }), O.on("mousewheel.xdsoft", function (e) {
        return !N.scrollInput || (!N.datepicker && N.timepicker ? (0 <= (a = P.find(".xdsoft_current").length ? P.find(".xdsoft_current").eq(0).index() : 0) + e.deltaY && a + e.deltaY < P.children().length && (a += e.deltaY), P.children().eq(a).length && P.children().eq(a).trigger("mousedown"), !1) : N.datepicker && !N.timepicker ? (g.trigger(e, [e.deltaY, e.deltaX, e.deltaY]), O.val && O.val(W.str()), _.trigger("changedatetime.xdsoft"), !1) : void 0)
      }), _.on("changedatetime.xdsoft", function (e) {
        if (N.onChangeDateTime && L.isFunction(N.onChangeDateTime)) {
          var t = _.data("input");
          N.onChangeDateTime.call(_, W.currentTime, t, e), delete N.value, t.trigger("change")
        }
      }).on("generate.xdsoft", function () {
        N.onGenerate && L.isFunction(N.onGenerate) && N.onGenerate.call(_, W.currentTime, _.data("input")), s && (_.trigger("afterOpen.xdsoft"), s = !1)
      }).on("click.xdsoft", function (e) {
        e.stopPropagation()
      }), a = 0, h = function (e, t) {
        do {
          if (!(e = e.parentNode) || !1 === t(e)) break
        } while ("HTML" !== e.nodeName)
      }, r = function () {
        var e, t, a, n, r, o, i, s, d, u, l, f, c;
        if (e = (s = _.data("input")).offset(), t = s[0], u = "top", a = e.top + t.offsetHeight - 1, n = e.left, r = "absolute", d = L(N.contentWindow).width(), f = L(N.contentWindow).height(), c = L(N.contentWindow).scrollTop(), N.ownerDocument.documentElement.clientWidth - e.left < g.parent().outerWidth(!0)) {
          var m = g.parent().outerWidth(!0) - t.offsetWidth;
          n -= m
        }
        "rtl" === s.parent().css("direction") && (n -= _.outerWidth() - s.outerWidth()), N.fixed ? (a -= c, n -= L(N.contentWindow).scrollLeft(), r = "fixed") : (i = !1, h(t, function (e) {
          return null !== e && ("fixed" === N.contentWindow.getComputedStyle(e).getPropertyValue("position") ? !(i = !0) : void 0)
        }), i && !N.insideParent ? (r = "fixed", a + _.outerHeight() > f + c ? (u = "bottom", a = f + c - e.top) : a -= c) : a + _[0].offsetHeight > f + c && (a = e.top - _[0].offsetHeight + 1), a < 0 && (a = 0), n + t.offsetWidth > d && (n = d - t.offsetWidth)), o = _[0], h(o, function (e) {
          if ("relative" === N.contentWindow.getComputedStyle(e).getPropertyValue("position") && d >= e.offsetWidth) return n -= (d - e.offsetWidth) / 2, !1
        }), l = {
          position: r,
          left: N.insideParent ? t.offsetLeft : n,
          top: "",
          bottom: ""
        }, N.insideParent ? l[u] = t.offsetTop + t.offsetHeight : l[u] = a, _.css(l)
      }, _.on("open.xdsoft", function (e) {
        var t = !0;
        N.onShow && L.isFunction(N.onShow) && (t = N.onShow.call(_, W.currentTime, _.data("input"), e)), !1 !== t && (_.show(), r(), L(N.contentWindow).off("resize.xdsoft", r).on("resize.xdsoft", r), N.closeOnWithoutClick && L([N.ownerDocument.body, N.contentWindow]).on("touchstart mousedown.xdsoft", function e() {
          _.trigger("close.xdsoft"), L([N.ownerDocument.body, N.contentWindow]).off("touchstart mousedown.xdsoft", e)
        }))
      }).on("close.xdsoft", function (e) {
        var t = !0;
        F.find(".xdsoft_month,.xdsoft_year").find(".xdsoft_select").hide(), N.onClose && L.isFunction(N.onClose) && (t = N.onClose.call(_, W.currentTime, _.data("input"), e)), !1 === t || N.opened || N.inline || _.hide(), e.stopPropagation()
      }).on("toggle.xdsoft", function () {
        _.is(":visible") ? _.trigger("close.xdsoft") : _.trigger("open.xdsoft")
      }).data("input", O), d = 0, _.data("xdsoft_datetime", W), _.setOptions(N), W.setCurrentTime(f()), O.data("xdsoft_datetimepicker", _).on("open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart", function () {
        O.is(":disabled") || O.data("xdsoft_datetimepicker").is(":visible") && N.closeOnInputClick || N.openOnFocus && (clearTimeout(d), d = setTimeout(function () {
          O.is(":disabled") || (s = !0, W.setCurrentTime(f(), !0), N.mask && c(N), _.trigger("open.xdsoft"))
        }, 100))
      }).on("keydown.xdsoft", function (e) {
        var t, a = e.which;
        return -1 !== [D].indexOf(a) && N.enterLikeTab ? (t = L("input:visible,textarea:visible,button:visible,a:visible"), _.trigger("close.xdsoft"), t.eq(t.index(this) + 1).focus(), !1) : -1 !== [T].indexOf(a) ? (_.trigger("close.xdsoft"), !0) : void 0
      }).on("blur.xdsoft", function () {
        _.trigger("close.xdsoft")
      })
    }, r = function (e) {
      var t = e.data("xdsoft_datetimepicker");
      t && (t.data("xdsoft_datetime", null), t.remove(), e.data("xdsoft_datetimepicker", null).off(".xdsoft"), L(N.contentWindow).off("resize.xdsoft"), L([N.contentWindow, N.ownerDocument.body]).off("mousedown.xdsoft touchstart"), e.unmousewheel && e.unmousewheel())
    }, L(N.ownerDocument).off("keydown.xdsoftctrl keyup.xdsoftctrl").off("keydown.xdsoftcmd keyup.xdsoftcmd").on("keydown.xdsoftctrl", function (e) {
      e.keyCode === p && (I = !0)
    }).on("keyup.xdsoftctrl", function (e) {
      e.keyCode === p && (I = !1)
    }).on("keydown.xdsoftcmd", function (e) {
      91 === e.keyCode && !0
    }).on("keyup.xdsoftcmd", function (e) {
      91 === e.keyCode && !1
    }), this.each(function () {
      var t, e = L(this).data("xdsoft_datetimepicker");
      if (e) {
        if ("string" === L.type(H)) switch (H) {
          case "show":
            L(this).select().focus(), e.trigger("open.xdsoft");
            break;
          case "hide":
            e.trigger("close.xdsoft");
            break;
          case "toggle":
            e.trigger("toggle.xdsoft");
            break;
          case "destroy":
            r(L(this));
            break;
          case "reset":
            this.value = this.defaultValue, this.value && e.data("xdsoft_datetime").isValidDate(E.parseDate(this.value, N.format)) || e.data("changed", !1), e.data("xdsoft_datetime").setCurrentTime(this.value);
            break;
          case "validate":
            e.data("input").trigger("blur.xdsoft");
            break;
          default:
            e[H] && L.isFunction(e[H]) && (o = e[H](a))
        } else e.setOptions(H);
        return 0
      }
      "string" !== L.type(H) && (!N.lazyInit || N.open || N.inline ? n(L(this)) : (t = L(this)).on("open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart", function e() {
        t.is(":disabled") || t.data("xdsoft_datetimepicker") || (clearTimeout(i), i = setTimeout(function () {
          t.data("xdsoft_datetimepicker") || n(t), t.off("open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart", e).trigger("open.xdsoft")
        }, 100))
      }))
    }), o
  }, L.fn.datetimepicker.defaults = s
};
! function (e) {
  "function" == typeof define && define.amd ? define(["jquery", "jquery-mousewheel"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(datetimepickerFactory),
function (e) {
  "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
}(function (c) {
  var m, h, e = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
    t = "onwheel" in document || 9 <= document.documentMode ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
    g = Array.prototype.slice;
  if (c.event.fixHooks)
    for (var a = e.length; a;) c.event.fixHooks[e[--a]] = c.event.mouseHooks;
  var p = c.event.special.mousewheel = {
    version: "3.1.12",
    setup: function () {
      if (this.addEventListener)
        for (var e = t.length; e;) this.addEventListener(t[--e], n, !1);
      else this.onmousewheel = n;
      c.data(this, "mousewheel-line-height", p.getLineHeight(this)), c.data(this, "mousewheel-page-height", p.getPageHeight(this))
    },
    teardown: function () {
      if (this.removeEventListener)
        for (var e = t.length; e;) this.removeEventListener(t[--e], n, !1);
      else this.onmousewheel = null;
      c.removeData(this, "mousewheel-line-height"), c.removeData(this, "mousewheel-page-height")
    },
    getLineHeight: function (e) {
      var t = c(e),
        a = t["offsetParent" in c.fn ? "offsetParent" : "parent"]();
      return a.length || (a = c("body")), parseInt(a.css("fontSize"), 10) || parseInt(t.css("fontSize"), 10) || 16
    },
    getPageHeight: function (e) {
      return c(e).height()
    },
    settings: {
      adjustOldDeltas: !0,
      normalizeOffset: !0
    }
  };

  function n(e) {
    var t, a = e || window.event,
      n = g.call(arguments, 1),
      r = 0,
      o = 0,
      i = 0,
      s = 0,
      d = 0;
    if ((e = c.event.fix(a)).type = "mousewheel", "detail" in a && (i = -1 * a.detail), "wheelDelta" in a && (i = a.wheelDelta), "wheelDeltaY" in a && (i = a.wheelDeltaY), "wheelDeltaX" in a && (o = -1 * a.wheelDeltaX), "axis" in a && a.axis === a.HORIZONTAL_AXIS && (o = -1 * i, i = 0), r = 0 === i ? o : i, "deltaY" in a && (r = i = -1 * a.deltaY), "deltaX" in a && (o = a.deltaX, 0 === i && (r = -1 * o)), 0 !== i || 0 !== o) {
      if (1 === a.deltaMode) {
        var u = c.data(this, "mousewheel-line-height");
        r *= u, i *= u, o *= u
      } else if (2 === a.deltaMode) {
        var l = c.data(this, "mousewheel-page-height");
        r *= l, i *= l, o *= l
      }
      if (t = Math.max(Math.abs(i), Math.abs(o)), (!h || t < h) && y(a, h = t) && (h /= 40), y(a, t) && (r /= 40, o /= 40, i /= 40), r = Math[1 <= r ? "floor" : "ceil"](r / h), o = Math[1 <= o ? "floor" : "ceil"](o / h), i = Math[1 <= i ? "floor" : "ceil"](i / h), p.settings.normalizeOffset && this.getBoundingClientRect) {
        var f = this.getBoundingClientRect();
        s = e.clientX - f.left, d = e.clientY - f.top
      }
      return e.deltaX = o, e.deltaY = i, e.deltaFactor = h, e.offsetX = s, e.offsetY = d, e.deltaMode = 0, n.unshift(e, r, o, i), m && clearTimeout(m), m = setTimeout(D, 200), (c.event.dispatch || c.event.handle).apply(this, n)
    }
  }

  function D() {
    h = null
  }

  function y(e, t) {
    return p.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0
  }
  c.fn.extend({
    mousewheel: function (e) {
      return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
    },
    unmousewheel: function (e) {
      return this.unbind("mousewheel", e)
    }
  })
});
/*! Magnific Popup - v1.1.0 - 2016-02-20
 * http://dimsemenov.com/plugins/magnific-popup/
 * Copyright (c) 2016 Dmitry Semenov; */
;
(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module. 
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node/CommonJS 
    factory(require('jquery'));
  } else {
    // Browser globals 
    factory(window.jQuery || window.Zepto);
  }
}(function ($) {

  /*>>core*/
  /**
   * 
   * Magnific Popup Core JS file
   * 
   */


  /**
   * Private static constants
   */
  var CLOSE_EVENT = 'Close',
    BEFORE_CLOSE_EVENT = 'BeforeClose',
    AFTER_CLOSE_EVENT = 'AfterClose',
    BEFORE_APPEND_EVENT = 'BeforeAppend',
    MARKUP_PARSE_EVENT = 'MarkupParse',
    OPEN_EVENT = 'Open',
    CHANGE_EVENT = 'Change',
    NS = 'mfp',
    EVENT_NS = '.' + NS,
    READY_CLASS = 'mfp-ready',
    REMOVING_CLASS = 'mfp-removing',
    PREVENT_CLOSE_CLASS = 'mfp-prevent-close';


  /**
   * Private vars 
   */
  /*jshint -W079 */
  var mfp, // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
    MagnificPopup = function () {},
    _isJQ = !!(window.jQuery),
    _prevStatus,
    _window = $(window),
    _document,
    _prevContentType,
    _wrapClasses,
    _currPopupType;


  /**
   * Private functions
   */
  var _mfpOn = function (name, f) {
      mfp.ev.on(NS + name + EVENT_NS, f);
    },
    _getEl = function (className, appendTo, html, raw) {
      var el = document.createElement('div');
      el.className = 'mfp-' + className;
      if (html) {
        el.innerHTML = html;
      }
      if (!raw) {
        el = $(el);
        if (appendTo) {
          el.appendTo(appendTo);
        }
      } else if (appendTo) {
        appendTo.appendChild(el);
      }
      return el;
    },
    _mfpTrigger = function (e, data) {
      mfp.ev.triggerHandler(NS + e, data);

      if (mfp.st.callbacks) {
        // converts "mfpEventName" to "eventName" callback and triggers it if it's present
        e = e.charAt(0).toLowerCase() + e.slice(1);
        if (mfp.st.callbacks[e]) {
          mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
        }
      }
    },
    _getCloseBtn = function (type) {
      if (type !== _currPopupType || !mfp.currTemplate.closeBtn) {
        mfp.currTemplate.closeBtn = $(mfp.st.closeMarkup.replace('%title%', mfp.st.tClose));
        _currPopupType = type;
      }
      return mfp.currTemplate.closeBtn;
    },
    // Initialize Magnific Popup only when called at least once
    _checkInstance = function () {
      if (!$.magnificPopup.instance) {
        /*jshint -W020 */
        mfp = new MagnificPopup();
        mfp.init();
        $.magnificPopup.instance = mfp;
      }
    },
    // CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
    supportsTransitions = function () {
      var s = document.createElement('p').style, // 's' for style. better to create an element if body yet to exist
        v = ['ms', 'O', 'Moz', 'Webkit']; // 'v' for vendor

      if (s['transition'] !== undefined) {
        return true;
      }

      while (v.length) {
        if (v.pop() + 'Transition' in s) {
          return true;
        }
      }

      return false;
    };



  /**
   * Public functions
   */
  MagnificPopup.prototype = {

    constructor: MagnificPopup,

    /**
     * Initializes Magnific Popup plugin. 
     * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
     */
    init: function () {
      var appVersion = navigator.appVersion;
      mfp.isLowIE = mfp.isIE8 = document.all && !document.addEventListener;
      mfp.isAndroid = (/android/gi).test(appVersion);
      mfp.isIOS = (/iphone|ipad|ipod/gi).test(appVersion);
      mfp.supportsTransition = supportsTransitions();

      // We disable fixed positioned lightbox on devices that don't handle it nicely.
      // If you know a better way of detecting this - let me know.
      mfp.probablyMobile = (mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent));
      _document = $(document);

      mfp.popupsCache = {};
    },

    /**
     * Opens popup
     * @param  data [description]
     */
    open: function (data) {

      var i;

      if (data.isObj === false) {
        // convert jQuery collection to array to avoid conflicts later
        mfp.items = data.items.toArray();

        mfp.index = 0;
        var items = data.items,
          item;
        for (i = 0; i < items.length; i++) {
          item = items[i];
          if (item.parsed) {
            item = item.el[0];
          }
          if (item === data.el[0]) {
            mfp.index = i;
            break;
          }
        }
      } else {
        mfp.items = $.isArray(data.items) ? data.items : [data.items];
        mfp.index = data.index || 0;
      }

      // if popup is already opened - we just update the content
      if (mfp.isOpen) {
        mfp.updateItemHTML();
        return;
      }

      mfp.types = [];
      _wrapClasses = '';
      if (data.mainEl && data.mainEl.length) {
        mfp.ev = data.mainEl.eq(0);
      } else {
        mfp.ev = _document;
      }

      if (data.key) {
        if (!mfp.popupsCache[data.key]) {
          mfp.popupsCache[data.key] = {};
        }
        mfp.currTemplate = mfp.popupsCache[data.key];
      } else {
        mfp.currTemplate = {};
      }



      mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data);
      mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;

      if (mfp.st.modal) {
        mfp.st.closeOnContentClick = false;
        mfp.st.closeOnBgClick = false;
        mfp.st.showCloseBtn = false;
        mfp.st.enableEscapeKey = false;
      }


      // Building markup
      // main containers are created only once
      if (!mfp.bgOverlay) {

        // Dark overlay
        mfp.bgOverlay = _getEl('bg').on('click' + EVENT_NS, function () {
          mfp.close();
        });

        mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click' + EVENT_NS, function (e) {
          if (mfp._checkIfClose(e.target)) {
            mfp.close();
          }
        });

        mfp.container = _getEl('container', mfp.wrap);
      }

      mfp.contentContainer = _getEl('content');
      if (mfp.st.preloader) {
        mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
      }


      // Initializing modules
      var modules = $.magnificPopup.modules;
      for (i = 0; i < modules.length; i++) {
        var n = modules[i];
        n = n.charAt(0).toUpperCase() + n.slice(1);
        mfp['init' + n].call(mfp);
      }
      _mfpTrigger('BeforeOpen');


      if (mfp.st.showCloseBtn) {
        // Close button
        if (!mfp.st.closeBtnInside) {
          mfp.wrap.append(_getCloseBtn());
        } else {
          _mfpOn(MARKUP_PARSE_EVENT, function (e, template, values, item) {
            values.close_replaceWith = _getCloseBtn(item.type);
          });
          _wrapClasses += ' mfp-close-btn-in';
        }
      }

      if (mfp.st.alignTop) {
        _wrapClasses += ' mfp-align-top';
      }



      if (mfp.fixedContentPos) {
        mfp.wrap.css({
          overflow: mfp.st.overflowY,
          overflowX: 'hidden',
          overflowY: mfp.st.overflowY
        });
      } else {
        mfp.wrap.css({
          top: _window.scrollTop(),
          position: 'absolute'
        });
      }
      if (mfp.st.fixedBgPos === false || (mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos)) {
        mfp.bgOverlay.css({
          height: _document.height(),
          position: 'absolute'
        });
      }



      if (mfp.st.enableEscapeKey) {
        // Close on ESC key
        _document.on('keyup' + EVENT_NS, function (e) {
          if (e.keyCode === 27) {
            mfp.close();
          }
        });
      }

      _window.on('resize' + EVENT_NS, function () {
        mfp.updateSize();
      });


      if (!mfp.st.closeOnContentClick) {
        _wrapClasses += ' mfp-auto-cursor';
      }

      if (_wrapClasses)
        mfp.wrap.addClass(_wrapClasses);


      // this triggers recalculation of layout, so we get it once to not to trigger twice
      var windowHeight = mfp.wH = _window.height();


      var windowStyles = {};

      if (mfp.fixedContentPos) {
        if (mfp._hasScrollBar(windowHeight)) {
          var s = mfp._getScrollbarSize();
          if (s) {
            windowStyles.marginRight = s;
          }
        }
      }

      if (mfp.fixedContentPos) {
        if (!mfp.isIE7) {
          windowStyles.overflow = 'hidden';
        } else {
          // ie7 double-scroll bug
          $('body, html').css('overflow', 'hidden');
        }
      }



      var classesToadd = mfp.st.mainClass;
      if (mfp.isIE7) {
        classesToadd += ' mfp-ie7';
      }
      if (classesToadd) {
        mfp._addClassToMFP(classesToadd);
      }

      // add content
      mfp.updateItemHTML();

      _mfpTrigger('BuildControls');

      // remove scrollbar, add margin e.t.c
      $('html').css(windowStyles);

      // add everything to DOM
      mfp.bgOverlay.add(mfp.wrap).prependTo(mfp.st.prependTo || $(document.body));

      // Save last focused element
      mfp._lastFocusedEl = document.activeElement;

      // Wait for next cycle to allow CSS transition
      setTimeout(function () {

        if (mfp.content) {
          mfp._addClassToMFP(READY_CLASS);
          mfp._setFocus();
        } else {
          // if content is not defined (not loaded e.t.c) we add class only for BG
          mfp.bgOverlay.addClass(READY_CLASS);
        }

        // Trap the focus in popup
        _document.on('focusin' + EVENT_NS, mfp._onFocusIn);

      }, 16);

      mfp.isOpen = true;
      mfp.updateSize(windowHeight);
      _mfpTrigger(OPEN_EVENT);

      return data;
    },

    /**
     * Closes the popup
     */
    close: function () {
      if (!mfp.isOpen) return;
      _mfpTrigger(BEFORE_CLOSE_EVENT);

      mfp.isOpen = false;
      // for CSS3 animation
      if (mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition) {
        mfp._addClassToMFP(REMOVING_CLASS);
        setTimeout(function () {
          mfp._close();
        }, mfp.st.removalDelay);
      } else {
        mfp._close();
      }
    },

    /**
     * Helper for close() function
     */
    _close: function () {
      _mfpTrigger(CLOSE_EVENT);

      var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';

      mfp.bgOverlay.detach();
      mfp.wrap.detach();
      mfp.container.empty();

      if (mfp.st.mainClass) {
        classesToRemove += mfp.st.mainClass + ' ';
      }

      mfp._removeClassFromMFP(classesToRemove);

      if (mfp.fixedContentPos) {
        var windowStyles = {
          marginRight: ''
        };
        if (mfp.isIE7) {
          $('body, html').css('overflow', '');
        } else {
          windowStyles.overflow = '';
        }
        $('html').css(windowStyles);
      }

      _document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
      mfp.ev.off(EVENT_NS);

      // clean up DOM elements that aren't removed
      mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
      mfp.bgOverlay.attr('class', 'mfp-bg');
      mfp.container.attr('class', 'mfp-container');

      // remove close button from target element
      if (mfp.st.showCloseBtn &&
        (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
        if (mfp.currTemplate.closeBtn)
          mfp.currTemplate.closeBtn.detach();
      }


      if (mfp.st.autoFocusLast && mfp._lastFocusedEl) {
        $(mfp._lastFocusedEl).focus(); // put tab focus back
      }
      mfp.currItem = null;
      mfp.content = null;
      mfp.currTemplate = null;
      mfp.prevHeight = 0;

      _mfpTrigger(AFTER_CLOSE_EVENT);
    },

    updateSize: function (winHeight) {

      if (mfp.isIOS) {
        // fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
        var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
        var height = window.innerHeight * zoomLevel;
        mfp.wrap.css('height', height);
        mfp.wH = height;
      } else {
        mfp.wH = winHeight || _window.height();
      }
      // Fixes #84: popup incorrectly positioned with position:relative on body
      if (!mfp.fixedContentPos) {
        mfp.wrap.css('height', mfp.wH);
      }

      _mfpTrigger('Resize');

    },

    /**
     * Set content of popup based on current index
     */
    updateItemHTML: function () {
      var item = mfp.items[mfp.index];

      // Detach and perform modifications
      mfp.contentContainer.detach();

      if (mfp.content)
        mfp.content.detach();

      if (!item.parsed) {
        item = mfp.parseEl(mfp.index);
      }

      var type = item.type;

      _mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
      // BeforeChange event works like so:
      // _mfpOn('BeforeChange', function(e, prevType, newType) { });

      mfp.currItem = item;

      if (!mfp.currTemplate[type]) {
        var markup = mfp.st[type] ? mfp.st[type].markup : false;

        // allows to modify markup
        _mfpTrigger('FirstMarkupParse', markup);

        if (markup) {
          mfp.currTemplate[type] = $(markup);
        } else {
          // if there is no markup found we just define that template is parsed
          mfp.currTemplate[type] = true;
        }
      }

      if (_prevContentType && _prevContentType !== item.type) {
        mfp.container.removeClass('mfp-' + _prevContentType + '-holder');
      }

      var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
      mfp.appendContent(newContent, type);

      item.preloaded = true;

      _mfpTrigger(CHANGE_EVENT, item);
      _prevContentType = item.type;

      // Append container back after its content changed
      mfp.container.prepend(mfp.contentContainer);

      _mfpTrigger('AfterChange');
    },


    /**
     * Set HTML content of popup
     */
    appendContent: function (newContent, type) {
      mfp.content = newContent;

      if (newContent) {
        if (mfp.st.showCloseBtn && mfp.st.closeBtnInside &&
          mfp.currTemplate[type] === true) {
          // if there is no markup, we just append close button element inside
          if (!mfp.content.find('.mfp-close').length) {
            mfp.content.append(_getCloseBtn());
          }
        } else {
          mfp.content = newContent;
        }
      } else {
        mfp.content = '';
      }

      _mfpTrigger(BEFORE_APPEND_EVENT);
      mfp.container.addClass('mfp-' + type + '-holder');

      mfp.contentContainer.append(mfp.content);
    },


    /**
     * Creates Magnific Popup data object based on given data
     * @param  {int} index Index of item to parse
     */
    parseEl: function (index) {
      var item = mfp.items[index],
        type;

      if (item.tagName) {
        item = {
          el: $(item)
        };
      } else {
        type = item.type;
        item = {
          data: item,
          src: item.src
        };
      }

      if (item.el) {
        var types = mfp.types;

        // check for 'mfp-TYPE' class
        for (var i = 0; i < types.length; i++) {
          if (item.el.hasClass('mfp-' + types[i])) {
            type = types[i];
            break;
          }
        }

        item.src = item.el.attr('data-mfp-src');
        if (!item.src) {
          item.src = item.el.attr('href');
        }
      }

      item.type = type || mfp.st.type || 'inline';
      item.index = index;
      item.parsed = true;
      mfp.items[index] = item;
      _mfpTrigger('ElementParse', item);

      return mfp.items[index];
    },


    /**
     * Initializes single popup or a group of popups
     */
    addGroup: function (el, options) {
      var eHandler = function (e) {
        e.mfpEl = this;
        mfp._openClick(e, el, options);
      };

      if (!options) {
        options = {};
      }

      var eName = 'click.magnificPopup';
      options.mainEl = el;

      if (options.items) {
        options.isObj = true;
        el.off(eName).on(eName, eHandler);
      } else {
        options.isObj = false;
        if (options.delegate) {
          el.off(eName).on(eName, options.delegate, eHandler);
        } else {
          options.items = el;
          el.off(eName).on(eName, eHandler);
        }
      }
    },
    _openClick: function (e, el, options) {
      var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;


      if (!midClick && (e.which === 2 || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey)) {
        return;
      }

      var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;

      if (disableOn) {
        if ($.isFunction(disableOn)) {
          if (!disableOn.call(mfp)) {
            return true;
          }
        } else { // else it's number
          if (_window.width() < disableOn) {
            return true;
          }
        }
      }

      if (e.type) {
        e.preventDefault();

        // This will prevent popup from closing if element is inside and popup is already opened
        if (mfp.isOpen) {
          e.stopPropagation();
        }
      }

      options.el = $(e.mfpEl);
      if (options.delegate) {
        options.items = el.find(options.delegate);
      }
      mfp.open(options);
    },


    /**
     * Updates text on preloader
     */
    updateStatus: function (status, text) {

      if (mfp.preloader) {
        if (_prevStatus !== status) {
          mfp.container.removeClass('mfp-s-' + _prevStatus);
        }

        if (!text && status === 'loading') {
          text = mfp.st.tLoading;
        }

        var data = {
          status: status,
          text: text
        };
        // allows to modify status
        _mfpTrigger('UpdateStatus', data);

        status = data.status;
        text = data.text;

        mfp.preloader.html(text);

        mfp.preloader.find('a').on('click', function (e) {
          e.stopImmediatePropagation();
        });

        mfp.container.addClass('mfp-s-' + status);
        _prevStatus = status;
      }
    },


    /*
    	"Private" helpers that aren't private at all
     */
    // Check to close popup or not
    // "target" is an element that was clicked
    _checkIfClose: function (target) {

      if ($(target).hasClass(PREVENT_CLOSE_CLASS)) {
        return;
      }

      var closeOnContent = mfp.st.closeOnContentClick;
      var closeOnBg = mfp.st.closeOnBgClick;

      if (closeOnContent && closeOnBg) {
        return true;
      } else {

        // We close the popup if click is on close button or on preloader. Or if there is no content.
        if (!mfp.content || $(target).hasClass('mfp-close') || (mfp.preloader && target === mfp.preloader[0])) {
          return true;
        }

        // if click is outside the content
        if ((target !== mfp.content[0] && !$.contains(mfp.content[0], target))) {
          if (closeOnBg) {
            // last check, if the clicked element is in DOM, (in case it's removed onclick)
            if ($.contains(document, target)) {
              return true;
            }
          }
        } else if (closeOnContent) {
          return true;
        }

      }
      return false;
    },
    _addClassToMFP: function (cName) {
      mfp.bgOverlay.addClass(cName);
      mfp.wrap.addClass(cName);
    },
    _removeClassFromMFP: function (cName) {
      this.bgOverlay.removeClass(cName);
      mfp.wrap.removeClass(cName);
    },
    _hasScrollBar: function (winHeight) {
      return ((mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height()));
    },
    _setFocus: function () {
      (mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).focus();
    },
    _onFocusIn: function (e) {
      if (e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target)) {
        mfp._setFocus();
        return false;
      }
    },
    _parseMarkup: function (template, values, item) {
      var arr;
      if (item.data) {
        values = $.extend(item.data, values);
      }
      _mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item]);

      $.each(values, function (key, value) {
        if (value === undefined || value === false) {
          return true;
        }
        arr = key.split('_');
        if (arr.length > 1) {
          var el = template.find(EVENT_NS + '-' + arr[0]);

          if (el.length > 0) {
            var attr = arr[1];
            if (attr === 'replaceWith') {
              if (el[0] !== value[0]) {
                el.replaceWith(value);
              }
            } else if (attr === 'img') {
              if (el.is('img')) {
                el.attr('src', value);
              } else {
                el.replaceWith($('<img>').attr('src', value).attr('class', el.attr('class')));
              }
            } else {
              el.attr(arr[1], value);
            }
          }

        } else {
          template.find(EVENT_NS + '-' + key).html(value);
        }
      });
    },

    _getScrollbarSize: function () {
      // thx David
      if (mfp.scrollbarSize === undefined) {
        var scrollDiv = document.createElement("div");
        scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
        document.body.appendChild(scrollDiv);
        mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        document.body.removeChild(scrollDiv);
      }
      return mfp.scrollbarSize;
    }

  }; /* MagnificPopup core prototype end */




  /**
   * Public static functions
   */
  $.magnificPopup = {
    instance: null,
    proto: MagnificPopup.prototype,
    modules: [],

    open: function (options, index) {
      _checkInstance();

      if (!options) {
        options = {};
      } else {
        options = $.extend(true, {}, options);
      }

      options.isObj = true;
      options.index = index || 0;
      return this.instance.open(options);
    },

    close: function () {
      return $.magnificPopup.instance && $.magnificPopup.instance.close();
    },

    registerModule: function (name, module) {
      if (module.options) {
        $.magnificPopup.defaults[name] = module.options;
      }
      $.extend(this.proto, module.proto);
      this.modules.push(name);
    },

    defaults: {

      // Info about options is in docs:
      // http://dimsemenov.com/plugins/magnific-popup/documentation.html#options

      disableOn: 0,

      key: null,

      midClick: false,

      mainClass: '',

      preloader: true,

      focus: '', // CSS selector of input to focus after popup is opened

      closeOnContentClick: false,

      closeOnBgClick: true,

      closeBtnInside: true,

      showCloseBtn: true,

      enableEscapeKey: true,

      modal: false,

      alignTop: false,

      removalDelay: 0,

      prependTo: null,

      fixedContentPos: 'auto',

      fixedBgPos: 'auto',

      overflowY: 'auto',

      closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',

      tClose: 'Close (Esc)',

      tLoading: 'Loading...',

      autoFocusLast: true

    }
  };



  $.fn.magnificPopup = function (options) {
    _checkInstance();

    var jqEl = $(this);

    // We call some API method of first param is a string
    if (typeof options === "string") {

      if (options === 'open') {
        var items,
          itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
          index = parseInt(arguments[1], 10) || 0;

        if (itemOpts.items) {
          items = itemOpts.items[index];
        } else {
          items = jqEl;
          if (itemOpts.delegate) {
            items = items.find(itemOpts.delegate);
          }
          items = items.eq(index);
        }
        mfp._openClick({
          mfpEl: items
        }, jqEl, itemOpts);
      } else {
        if (mfp.isOpen)
          mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
      }

    } else {
      // clone options obj
      options = $.extend(true, {}, options);

      /*
       * As Zepto doesn't support .data() method for objects
       * and it works only in normal browsers
       * we assign "options" object directly to the DOM element. FTW!
       */
      if (_isJQ) {
        jqEl.data('magnificPopup', options);
      } else {
        jqEl[0].magnificPopup = options;
      }

      mfp.addGroup(jqEl, options);

    }
    return jqEl;
  };

  /*>>core*/

  /*>>inline*/

  var INLINE_NS = 'inline',
    _hiddenClass,
    _inlinePlaceholder,
    _lastInlineElement,
    _putInlineElementsBack = function () {
      if (_lastInlineElement) {
        _inlinePlaceholder.after(_lastInlineElement.addClass(_hiddenClass)).detach();
        _lastInlineElement = null;
      }
    };

  $.magnificPopup.registerModule(INLINE_NS, {
    options: {
      hiddenClass: 'hide', // will be appended with `mfp-` prefix
      markup: '',
      tNotFound: 'Content not found'
    },
    proto: {

      initInline: function () {
        mfp.types.push(INLINE_NS);

        _mfpOn(CLOSE_EVENT + '.' + INLINE_NS, function () {
          _putInlineElementsBack();
        });
      },

      getInline: function (item, template) {

        _putInlineElementsBack();

        if (item.src) {
          var inlineSt = mfp.st.inline,
            el = $(item.src);

          if (el.length) {

            // If target element has parent - we replace it with placeholder and put it back after popup is closed
            var parent = el[0].parentNode;
            if (parent && parent.tagName) {
              if (!_inlinePlaceholder) {
                _hiddenClass = inlineSt.hiddenClass;
                _inlinePlaceholder = _getEl(_hiddenClass);
                _hiddenClass = 'mfp-' + _hiddenClass;
              }
              // replace target inline element with placeholder
              _lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
            }

            mfp.updateStatus('ready');
          } else {
            mfp.updateStatus('error', inlineSt.tNotFound);
            el = $('<div>');
          }

          item.inlineElement = el;
          return el;
        }

        mfp.updateStatus('ready');
        mfp._parseMarkup(template, {}, item);
        return template;
      }
    }
  });

  /*>>inline*/

  /*>>ajax*/
  var AJAX_NS = 'ajax',
    _ajaxCur,
    _removeAjaxCursor = function () {
      if (_ajaxCur) {
        $(document.body).removeClass(_ajaxCur);
      }
    },
    _destroyAjaxRequest = function () {
      _removeAjaxCursor();
      if (mfp.req) {
        mfp.req.abort();
      }
    };

  $.magnificPopup.registerModule(AJAX_NS, {

    options: {
      settings: null,
      cursor: 'mfp-ajax-cur',
      tError: '<a href="%url%">The content</a> could not be loaded.'
    },

    proto: {
      initAjax: function () {
        mfp.types.push(AJAX_NS);
        _ajaxCur = mfp.st.ajax.cursor;

        _mfpOn(CLOSE_EVENT + '.' + AJAX_NS, _destroyAjaxRequest);
        _mfpOn('BeforeChange.' + AJAX_NS, _destroyAjaxRequest);
      },
      getAjax: function (item) {

        if (_ajaxCur) {
          $(document.body).addClass(_ajaxCur);
        }

        mfp.updateStatus('loading');

        var opts = $.extend({
          url: item.src,
          success: function (data, textStatus, jqXHR) {
            var temp = {
              data: data,
              xhr: jqXHR
            };

            _mfpTrigger('ParseAjax', temp);

            mfp.appendContent($(temp.data), AJAX_NS);

            item.finished = true;

            _removeAjaxCursor();

            mfp._setFocus();

            setTimeout(function () {
              mfp.wrap.addClass(READY_CLASS);
            }, 16);

            mfp.updateStatus('ready');

            _mfpTrigger('AjaxContentAdded');
          },
          error: function () {
            _removeAjaxCursor();
            item.finished = item.loadError = true;
            mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
          }
        }, mfp.st.ajax.settings);

        mfp.req = $.ajax(opts);

        return '';
      }
    }
  });

  /*>>ajax*/

  /*>>image*/
  var _imgInterval,
    _getTitle = function (item) {
      if (item.data && item.data.title !== undefined)
        return item.data.title;

      var src = mfp.st.image.titleSrc;

      if (src) {
        if ($.isFunction(src)) {
          return src.call(mfp, item);
        } else if (item.el) {
          return item.el.attr(src) || '';
        }
      }
      return '';
    };

  $.magnificPopup.registerModule('image', {

    options: {
      markup: '<div class="mfp-figure">' +
        '<div class="mfp-close"></div>' +
        '<figure>' +
        '<div class="mfp-img"></div>' +
        '<figcaption>' +
        '<div class="mfp-bottom-bar">' +
        '<div class="mfp-title"></div>' +
        '<div class="mfp-counter"></div>' +
        '</div>' +
        '</figcaption>' +
        '</figure>' +
        '</div>',
      cursor: 'mfp-zoom-out-cur',
      titleSrc: 'title',
      verticalFit: true,
      tError: '<a href="%url%">The image</a> could not be loaded.'
    },

    proto: {
      initImage: function () {
        var imgSt = mfp.st.image,
          ns = '.image';

        mfp.types.push('image');

        _mfpOn(OPEN_EVENT + ns, function () {
          if (mfp.currItem.type === 'image' && imgSt.cursor) {
            $(document.body).addClass(imgSt.cursor);
          }
        });

        _mfpOn(CLOSE_EVENT + ns, function () {
          if (imgSt.cursor) {
            $(document.body).removeClass(imgSt.cursor);
          }
          _window.off('resize' + EVENT_NS);
        });

        _mfpOn('Resize' + ns, mfp.resizeImage);
        if (mfp.isLowIE) {
          _mfpOn('AfterChange', mfp.resizeImage);
        }
      },
      resizeImage: function () {
        var item = mfp.currItem;
        if (!item || !item.img) return;

        if (mfp.st.image.verticalFit) {
          var decr = 0;
          // fix box-sizing in ie7/8
          if (mfp.isLowIE) {
            decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'), 10);
          }
          item.img.css('max-height', mfp.wH - decr);
        }
      },
      _onImageHasSize: function (item) {
        if (item.img) {

          item.hasSize = true;

          if (_imgInterval) {
            clearInterval(_imgInterval);
          }

          item.isCheckingImgSize = false;

          _mfpTrigger('ImageHasSize', item);

          if (item.imgHidden) {
            if (mfp.content)
              mfp.content.removeClass('mfp-loading');

            item.imgHidden = false;
          }

        }
      },

      /**
       * Function that loops until the image has size to display elements that rely on it asap
       */
      findImageSize: function (item) {

        var counter = 0,
          img = item.img[0],
          mfpSetInterval = function (delay) {

            if (_imgInterval) {
              clearInterval(_imgInterval);
            }
            // decelerating interval that checks for size of an image
            _imgInterval = setInterval(function () {
              if (img.naturalWidth > 0) {
                mfp._onImageHasSize(item);
                return;
              }

              if (counter > 200) {
                clearInterval(_imgInterval);
              }

              counter++;
              if (counter === 3) {
                mfpSetInterval(10);
              } else if (counter === 40) {
                mfpSetInterval(50);
              } else if (counter === 100) {
                mfpSetInterval(500);
              }
            }, delay);
          };

        mfpSetInterval(1);
      },

      getImage: function (item, template) {

        var guard = 0,

          // image load complete handler
          onLoadComplete = function () {
            if (item) {
              if (item.img[0].complete) {
                item.img.off('.mfploader');

                if (item === mfp.currItem) {
                  mfp._onImageHasSize(item);

                  mfp.updateStatus('ready');
                }

                item.hasSize = true;
                item.loaded = true;

                _mfpTrigger('ImageLoadComplete');

              } else {
                // if image complete check fails 200 times (20 sec), we assume that there was an error.
                guard++;
                if (guard < 200) {
                  setTimeout(onLoadComplete, 100);
                } else {
                  onLoadError();
                }
              }
            }
          },

          // image error handler
          onLoadError = function () {
            if (item) {
              item.img.off('.mfploader');
              if (item === mfp.currItem) {
                mfp._onImageHasSize(item);
                mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
              }

              item.hasSize = true;
              item.loaded = true;
              item.loadError = true;
            }
          },
          imgSt = mfp.st.image;


        var el = template.find('.mfp-img');
        if (el.length) {
          var img = document.createElement('img');
          img.className = 'mfp-img';
          if (item.el && item.el.find('img').length) {
            img.alt = item.el.find('img').attr('alt');
          }
          item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
          img.src = item.src;

          // without clone() "error" event is not firing when IMG is replaced by new IMG
          // TODO: find a way to avoid such cloning
          if (el.is('img')) {
            item.img = item.img.clone();
          }

          img = item.img[0];
          if (img.naturalWidth > 0) {
            item.hasSize = true;
          } else if (!img.width) {
            item.hasSize = false;
          }
        }

        mfp._parseMarkup(template, {
          title: _getTitle(item),
          img_replaceWith: item.img
        }, item);

        mfp.resizeImage();

        if (item.hasSize) {
          if (_imgInterval) clearInterval(_imgInterval);

          if (item.loadError) {
            template.addClass('mfp-loading');
            mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
          } else {
            template.removeClass('mfp-loading');
            mfp.updateStatus('ready');
          }
          return template;
        }

        mfp.updateStatus('loading');
        item.loading = true;

        if (!item.hasSize) {
          item.imgHidden = true;
          template.addClass('mfp-loading');
          mfp.findImageSize(item);
        }

        return template;
      }
    }
  });

  /*>>image*/

  /*>>zoom*/
  var hasMozTransform,
    getHasMozTransform = function () {
      if (hasMozTransform === undefined) {
        hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
      }
      return hasMozTransform;
    };

  $.magnificPopup.registerModule('zoom', {

    options: {
      enabled: false,
      easing: 'ease-in-out',
      duration: 300,
      opener: function (element) {
        return element.is('img') ? element : element.find('img');
      }
    },

    proto: {

      initZoom: function () {
        var zoomSt = mfp.st.zoom,
          ns = '.zoom',
          image;

        if (!zoomSt.enabled || !mfp.supportsTransition) {
          return;
        }

        var duration = zoomSt.duration,
          getElToAnimate = function (image) {
            var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
              transition = 'all ' + (zoomSt.duration / 1000) + 's ' + zoomSt.easing,
              cssObj = {
                position: 'fixed',
                zIndex: 9999,
                left: 0,
                top: 0,
                '-webkit-backface-visibility': 'hidden'
              },
              t = 'transition';

            cssObj['-webkit-' + t] = cssObj['-moz-' + t] = cssObj['-o-' + t] = cssObj[t] = transition;

            newImg.css(cssObj);
            return newImg;
          },
          showMainContent = function () {
            mfp.content.css('visibility', 'visible');
          },
          openTimeout,
          animatedImg;

        _mfpOn('BuildControls' + ns, function () {
          if (mfp._allowZoom()) {

            clearTimeout(openTimeout);
            mfp.content.css('visibility', 'hidden');

            // Basically, all code below does is clones existing image, puts in on top of the current one and animated it

            image = mfp._getItemToZoom();

            if (!image) {
              showMainContent();
              return;
            }

            animatedImg = getElToAnimate(image);

            animatedImg.css(mfp._getOffset());

            mfp.wrap.append(animatedImg);

            openTimeout = setTimeout(function () {
              animatedImg.css(mfp._getOffset(true));
              openTimeout = setTimeout(function () {

                showMainContent();

                setTimeout(function () {
                  animatedImg.remove();
                  image = animatedImg = null;
                  _mfpTrigger('ZoomAnimationEnded');
                }, 16); // avoid blink when switching images

              }, duration); // this timeout equals animation duration

            }, 16); // by adding this timeout we avoid short glitch at the beginning of animation


            // Lots of timeouts...
          }
        });
        _mfpOn(BEFORE_CLOSE_EVENT + ns, function () {
          if (mfp._allowZoom()) {

            clearTimeout(openTimeout);

            mfp.st.removalDelay = duration;

            if (!image) {
              image = mfp._getItemToZoom();
              if (!image) {
                return;
              }
              animatedImg = getElToAnimate(image);
            }

            animatedImg.css(mfp._getOffset(true));
            mfp.wrap.append(animatedImg);
            mfp.content.css('visibility', 'hidden');

            setTimeout(function () {
              animatedImg.css(mfp._getOffset());
            }, 16);
          }

        });

        _mfpOn(CLOSE_EVENT + ns, function () {
          if (mfp._allowZoom()) {
            showMainContent();
            if (animatedImg) {
              animatedImg.remove();
            }
            image = null;
          }
        });
      },

      _allowZoom: function () {
        return mfp.currItem.type === 'image';
      },

      _getItemToZoom: function () {
        if (mfp.currItem.hasSize) {
          return mfp.currItem.img;
        } else {
          return false;
        }
      },

      // Get element postion relative to viewport
      _getOffset: function (isLarge) {
        var el;
        if (isLarge) {
          el = mfp.currItem.img;
        } else {
          el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
        }

        var offset = el.offset();
        var paddingTop = parseInt(el.css('padding-top'), 10);
        var paddingBottom = parseInt(el.css('padding-bottom'), 10);
        offset.top -= ($(window).scrollTop() - paddingTop);


        /*

        Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.

         */
        var obj = {
          width: el.width(),
          // fix Zepto height+padding issue
          height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
        };

        // I hate to do this, but there is no another option
        if (getHasMozTransform()) {
          obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
        } else {
          obj.left = offset.left;
          obj.top = offset.top;
        }
        return obj;
      }

    }
  });



  /*>>zoom*/

  /*>>iframe*/

  var IFRAME_NS = 'iframe',
    _emptyPage = '//about:blank',

    _fixIframeBugs = function (isShowing) {
      if (mfp.currTemplate[IFRAME_NS]) {
        var el = mfp.currTemplate[IFRAME_NS].find('iframe');
        if (el.length) {
          // reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
          if (!isShowing) {
            el[0].src = _emptyPage;
          }

          // IE8 black screen bug fix
          if (mfp.isIE8) {
            el.css('display', isShowing ? 'block' : 'none');
          }
        }
      }
    };

  $.magnificPopup.registerModule(IFRAME_NS, {

    options: {
      markup: '<div class="mfp-iframe-scaler">' +
        '<div class="mfp-close"></div>' +
        '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>' +
        '</div>',

      srcAction: 'iframe_src',

      // we don't care and support only one default type of URL by default
      patterns: {
        youtube: {
          index: 'youtube.com',
          id: 'v=',
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
        vimeo: {
          index: 'vimeo.com/',
          id: '/',
          src: '//player.vimeo.com/video/%id%?autoplay=1'
        },
        gmaps: {
          index: '//maps.google.',
          src: '%id%&output=embed'
        }
      }
    },

    proto: {
      initIframe: function () {
        mfp.types.push(IFRAME_NS);

        _mfpOn('BeforeChange', function (e, prevType, newType) {
          if (prevType !== newType) {
            if (prevType === IFRAME_NS) {
              _fixIframeBugs(); // iframe if removed
            } else if (newType === IFRAME_NS) {
              _fixIframeBugs(true); // iframe is showing
            }
          } // else {
          // iframe source is switched, don't do anything
          //}
        });

        _mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function () {
          _fixIframeBugs();
        });
      },

      getIframe: function (item, template) {
        var embedSrc = item.src;
        var iframeSt = mfp.st.iframe;

        $.each(iframeSt.patterns, function () {
          if (embedSrc.indexOf(this.index) > -1) {
            if (this.id) {
              if (typeof this.id === 'string') {
                embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id) + this.id.length, embedSrc.length);
              } else {
                embedSrc = this.id.call(this, embedSrc);
              }
            }
            embedSrc = this.src.replace('%id%', embedSrc);
            return false; // break;
          }
        });

        var dataObj = {};
        if (iframeSt.srcAction) {
          dataObj[iframeSt.srcAction] = embedSrc;
        }
        mfp._parseMarkup(template, dataObj, item);

        mfp.updateStatus('ready');

        return template;
      }
    }
  });



  /*>>iframe*/

  /*>>gallery*/
  /**
   * Get looped index depending on number of slides
   */
  var _getLoopedId = function (index) {
      var numSlides = mfp.items.length;
      if (index > numSlides - 1) {
        return index - numSlides;
      } else if (index < 0) {
        return numSlides + index;
      }
      return index;
    },
    _replaceCurrTotal = function (text, curr, total) {
      return text.replace(/%curr%/gi, curr + 1).replace(/%total%/gi, total);
    };

  $.magnificPopup.registerModule('gallery', {

    options: {
      enabled: false,
      arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
      preload: [0, 2],
      navigateByImgClick: true,
      arrows: true,

      tPrev: 'Previous (Left arrow key)',
      tNext: 'Next (Right arrow key)',
      tCounter: '%curr% of %total%'
    },

    proto: {
      initGallery: function () {

        var gSt = mfp.st.gallery,
          ns = '.mfp-gallery';

        mfp.direction = true; // true - next, false - prev

        if (!gSt || !gSt.enabled) return false;

        _wrapClasses += ' mfp-gallery';

        _mfpOn(OPEN_EVENT + ns, function () {

          if (gSt.navigateByImgClick) {
            mfp.wrap.on('click' + ns, '.mfp-img', function () {
              if (mfp.items.length > 1) {
                mfp.next();
                return false;
              }
            });
          }

          _document.on('keydown' + ns, function (e) {
            if (e.keyCode === 37) {
              mfp.prev();
            } else if (e.keyCode === 39) {
              mfp.next();
            }
          });
        });

        _mfpOn('UpdateStatus' + ns, function (e, data) {
          if (data.text) {
            data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
          }
        });

        _mfpOn(MARKUP_PARSE_EVENT + ns, function (e, element, values, item) {
          var l = mfp.items.length;
          values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
        });

        _mfpOn('BuildControls' + ns, function () {
          if (mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {
            var markup = gSt.arrowMarkup,
              arrowLeft = mfp.arrowLeft = $(markup.replace(/%title%/gi, gSt.tPrev).replace(/%dir%/gi, 'left')).addClass(PREVENT_CLOSE_CLASS),
              arrowRight = mfp.arrowRight = $(markup.replace(/%title%/gi, gSt.tNext).replace(/%dir%/gi, 'right')).addClass(PREVENT_CLOSE_CLASS);

            arrowLeft.click(function () {
              mfp.prev();
            });
            arrowRight.click(function () {
              mfp.next();
            });

            mfp.container.append(arrowLeft.add(arrowRight));
          }
        });

        _mfpOn(CHANGE_EVENT + ns, function () {
          if (mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);

          mfp._preloadTimeout = setTimeout(function () {
            mfp.preloadNearbyImages();
            mfp._preloadTimeout = null;
          }, 16);
        });


        _mfpOn(CLOSE_EVENT + ns, function () {
          _document.off(ns);
          mfp.wrap.off('click' + ns);
          mfp.arrowRight = mfp.arrowLeft = null;
        });

      },
      next: function () {
        mfp.direction = true;
        mfp.index = _getLoopedId(mfp.index + 1);
        mfp.updateItemHTML();
      },
      prev: function () {
        mfp.direction = false;
        mfp.index = _getLoopedId(mfp.index - 1);
        mfp.updateItemHTML();
      },
      goTo: function (newIndex) {
        mfp.direction = (newIndex >= mfp.index);
        mfp.index = newIndex;
        mfp.updateItemHTML();
      },
      preloadNearbyImages: function () {
        var p = mfp.st.gallery.preload,
          preloadBefore = Math.min(p[0], mfp.items.length),
          preloadAfter = Math.min(p[1], mfp.items.length),
          i;

        for (i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
          mfp._preloadItem(mfp.index + i);
        }
        for (i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
          mfp._preloadItem(mfp.index - i);
        }
      },
      _preloadItem: function (index) {
        index = _getLoopedId(index);

        if (mfp.items[index].preloaded) {
          return;
        }

        var item = mfp.items[index];
        if (!item.parsed) {
          item = mfp.parseEl(index);
        }

        _mfpTrigger('LazyLoad', item);

        if (item.type === 'image') {
          item.img = $('<img class="mfp-img" />').on('load.mfploader', function () {
            item.hasSize = true;
          }).on('error.mfploader', function () {
            item.hasSize = true;
            item.loadError = true;
            _mfpTrigger('LazyLoadError', item);
          }).attr('src', item.src);
        }


        item.preloaded = true;
      }
    }
  });

  /*>>gallery*/

  /*>>retina*/

  var RETINA_NS = 'retina';

  $.magnificPopup.registerModule(RETINA_NS, {
    options: {
      replaceSrc: function (item) {
        return item.src.replace(/\.\w+$/, function (m) {
          return '@2x' + m;
        });
      },
      ratio: 1 // Function or number.  Set to 1 to disable.
    },
    proto: {
      initRetina: function () {
        if (window.devicePixelRatio > 1) {

          var st = mfp.st.retina,
            ratio = st.ratio;

          ratio = !isNaN(ratio) ? ratio : ratio();

          if (ratio > 1) {
            _mfpOn('ImageHasSize' + '.' + RETINA_NS, function (e, item) {
              item.img.css({
                'max-width': item.img[0].naturalWidth / ratio,
                'width': '100%'
              });
            });
            _mfpOn('ElementParse' + '.' + RETINA_NS, function (e, item) {
              item.src = st.replaceSrc(item, ratio);
            });
          }
        }

      }
    }
  });

  /*>>retina*/
  _checkInstance();
}));
/*! jQuery Validation Plugin - v1.16.0 - 12/2/2016
 * http://jqueryvalidation.org/
 * Copyright (c) 2016 Jörn Zaefferer; Licensed MIT */
! function (a) {
  "function" == typeof define && define.amd ? define(["sources/libs/jquery"], a) : "object" == typeof module && module.exports ? module.exports = a(require("sources/libs/jquery")) : a(jQuery)
}(function (a) {
  a.extend(a.fn, {
    validate: function (b) {
      if (!this.length) return void(b && b.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));
      var c = a.data(this[0], "validator");
      return c ? c : (this.attr("novalidate", "novalidate"), c = new a.validator(b, this[0]), a.data(this[0], "validator", c), c.settings.onsubmit && (this.on("click.validate", ":submit", function (b) {
        c.settings.submitHandler && (c.submitButton = b.target), a(this).hasClass("cancel") && (c.cancelSubmit = !0), void 0 !== a(this).attr("formnovalidate") && (c.cancelSubmit = !0)
      }), this.on("submit.validate", function (b) {
        function d() {
          var d, e;
          return !c.settings.submitHandler || (c.submitButton && (d = a("<input type='hidden'/>").attr("name", c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)), e = c.settings.submitHandler.call(c, c.currentForm, b), c.submitButton && d.remove(), void 0 !== e && e)
        }
        return c.settings.debug && b.preventDefault(), c.cancelSubmit ? (c.cancelSubmit = !1, d()) : c.form() ? c.pendingRequest ? (c.formSubmitted = !0, !1) : d() : (c.focusInvalid(), !1)
      })), c)
    },
    valid: function () {
      var b, c, d;
      return a(this[0]).is("form") ? b = this.validate().form() : (d = [], b = !0, c = a(this[0].form).validate(), this.each(function () {
        b = c.element(this) && b, b || (d = d.concat(c.errorList))
      }), c.errorList = d), b
    },
    rules: function (b, c) {
      var d, e, f, g, h, i, j = this[0];
      if (null != j && null != j.form) {
        if (b) switch (d = a.data(j.form, "validator").settings, e = d.rules, f = a.validator.staticRules(j), b) {
          case "add":
            a.extend(f, a.validator.normalizeRule(c)), delete f.messages, e[j.name] = f, c.messages && (d.messages[j.name] = a.extend(d.messages[j.name], c.messages));
            break;
          case "remove":
            return c ? (i = {}, a.each(c.split(/\s/), function (b, c) {
              i[c] = f[c], delete f[c], "required" === c && a(j).removeAttr("aria-required")
            }), i) : (delete e[j.name], f)
        }
        return g = a.validator.normalizeRules(a.extend({}, a.validator.classRules(j), a.validator.attributeRules(j), a.validator.dataRules(j), a.validator.staticRules(j)), j), g.required && (h = g.required, delete g.required, g = a.extend({
          required: h
        }, g), a(j).attr("aria-required", "true")), g.remote && (h = g.remote, delete g.remote, g = a.extend(g, {
          remote: h
        })), g
      }
    }
  }), a.extend(a.expr.pseudos || a.expr[":"], {
    blank: function (b) {
      return !a.trim("" + a(b).val())
    },
    filled: function (b) {
      var c = a(b).val();
      return null !== c && !!a.trim("" + c)
    },
    unchecked: function (b) {
      return !a(b).prop("checked")
    }
  }), a.validator = function (b, c) {
    this.settings = a.extend(!0, {}, a.validator.defaults, b), this.currentForm = c, this.init()
  }, a.validator.format = function (b, c) {
    return 1 === arguments.length ? function () {
      var c = a.makeArray(arguments);
      return c.unshift(b), a.validator.format.apply(this, c)
    } : void 0 === c ? b : (arguments.length > 2 && c.constructor !== Array && (c = a.makeArray(arguments).slice(1)), c.constructor !== Array && (c = [c]), a.each(c, function (a, c) {
      b = b.replace(new RegExp("\\{" + a + "\\}", "g"), function () {
        return c
      })
    }), b)
  }, a.extend(a.validator, {
    defaults: {
      messages: {},
      groups: {},
      rules: {},
      errorClass: "error",
      pendingClass: "pending",
      validClass: "valid",
      errorElement: "label",
      focusCleanup: !1,
      focusInvalid: !0,
      errorContainer: a([]),
      errorLabelContainer: a([]),
      onsubmit: !0,
      ignore: ":hidden",
      ignoreTitle: !1,
      onfocusin: function (a) {
        this.lastActive = a, this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(a)))
      },
      onfocusout: function (a) {
        this.checkable(a) || !(a.name in this.submitted) && this.optional(a) || this.element(a)
      },
      onkeyup: function (b, c) {
        var d = [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225];
        9 === c.which && "" === this.elementValue(b) || a.inArray(c.keyCode, d) !== -1 || (b.name in this.submitted || b.name in this.invalid) && this.element(b)
      },
      onclick: function (a) {
        a.name in this.submitted ? this.element(a) : a.parentNode.name in this.submitted && this.element(a.parentNode)
      },
      highlight: function (b, c, d) {
        "radio" === b.type ? this.findByName(b.name).addClass(c).removeClass(d) : a(b).addClass(c).removeClass(d)
      },
      unhighlight: function (b, c, d) {
        "radio" === b.type ? this.findByName(b.name).removeClass(c).addClass(d) : a(b).removeClass(c).addClass(d)
      }
    },
    setDefaults: function (b) {
      a.extend(a.validator.defaults, b)
    },
    messages: {
      required: "This field is required.",
      remote: "Please fix this field.",
      email: "Please enter a valid email address.",
      url: "Please enter a valid URL.",
      date: "Please enter a valid date.",
      dateISO: "Please enter a valid date (ISO).",
      number: "Please enter a valid number.",
      digits: "Please enter only digits.",
      equalTo: "Please enter the same value again.",
      maxlength: a.validator.format("Please enter no more than {0} characters."),
      minlength: a.validator.format("Please enter at least {0} characters."),
      rangelength: a.validator.format("Please enter a value between {0} and {1} characters long."),
      range: a.validator.format("Please enter a value between {0} and {1}."),
      max: a.validator.format("Please enter a value less than or equal to {0}."),
      min: a.validator.format("Please enter a value greater than or equal to {0}."),
      step: a.validator.format("Please enter a multiple of {0}.")
    },
    autoCreateRanges: !1,
    prototype: {
      init: function () {
        function b(b) {
          !this.form && this.hasAttribute("contenteditable") && (this.form = a(this).closest("form")[0]);
          var c = a.data(this.form, "validator"),
            d = "on" + b.type.replace(/^validate/, ""),
            e = c.settings;
          e[d] && !a(this).is(e.ignore) && e[d].call(c, this, b)
        }
        this.labelContainer = a(this.settings.errorLabelContainer), this.errorContext = this.labelContainer.length && this.labelContainer || a(this.currentForm), this.containers = a(this.settings.errorContainer).add(this.settings.errorLabelContainer), this.submitted = {}, this.valueCache = {}, this.pendingRequest = 0, this.pending = {}, this.invalid = {}, this.reset();
        var c, d = this.groups = {};
        a.each(this.settings.groups, function (b, c) {
          "string" == typeof c && (c = c.split(/\s/)), a.each(c, function (a, c) {
            d[c] = b
          })
        }), c = this.settings.rules, a.each(c, function (b, d) {
          c[b] = a.validator.normalizeRule(d)
        }), a(this.currentForm).on("focusin.validate focusout.validate keyup.validate", ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']", b).on("click.validate", "select, option, [type='radio'], [type='checkbox']", b), this.settings.invalidHandler && a(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler), a(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true")
      },
      form: function () {
        return this.checkForm(), a.extend(this.submitted, this.errorMap), this.invalid = a.extend({}, this.errorMap), this.valid() || a(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid()
      },
      checkForm: function () {
        this.prepareForm();
        for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) this.check(b[a]);
        return this.valid()
      },
      element: function (b) {
        var c, d, e = this.clean(b),
          f = this.validationTargetFor(e),
          g = this,
          h = !0;
        return void 0 === f ? delete this.invalid[e.name] : (this.prepareElement(f), this.currentElements = a(f), d = this.groups[f.name], d && a.each(this.groups, function (a, b) {
          b === d && a !== f.name && (e = g.validationTargetFor(g.clean(g.findByName(a))), e && e.name in g.invalid && (g.currentElements.push(e), h = g.check(e) && h))
        }), c = this.check(f) !== !1, h = h && c, c ? this.invalid[f.name] = !1 : this.invalid[f.name] = !0, this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), a(b).attr("aria-invalid", !c)), h
      },
      showErrors: function (b) {
        if (b) {
          var c = this;
          a.extend(this.errorMap, b), this.errorList = a.map(this.errorMap, function (a, b) {
            return {
              message: a,
              element: c.findByName(b)[0]
            }
          }), this.successList = a.grep(this.successList, function (a) {
            return !(a.name in b)
          })
        }
        this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
      },
      resetForm: function () {
        a.fn.resetForm && a(this.currentForm).resetForm(), this.invalid = {}, this.submitted = {}, this.prepareForm(), this.hideErrors();
        var b = this.elements().removeData("previousValue").removeAttr("aria-invalid");
        this.resetElements(b)
      },
      resetElements: function (a) {
        var b;
        if (this.settings.unhighlight)
          for (b = 0; a[b]; b++) this.settings.unhighlight.call(this, a[b], this.settings.errorClass, ""), this.findByName(a[b].name).removeClass(this.settings.validClass);
        else a.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)
      },
      numberOfInvalids: function () {
        return this.objectLength(this.invalid)
      },
      objectLength: function (a) {
        var b, c = 0;
        for (b in a) a[b] && c++;
        return c
      },
      hideErrors: function () {
        this.hideThese(this.toHide)
      },
      hideThese: function (a) {
        a.not(this.containers).text(""), this.addWrapper(a).hide()
      },
      valid: function () {
        return 0 === this.size()
      },
      size: function () {
        return this.errorList.length
      },
      focusInvalid: function () {
        if (this.settings.focusInvalid) try {
          a(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
        } catch (b) {}
      },
      findLastActive: function () {
        var b = this.lastActive;
        return b && 1 === a.grep(this.errorList, function (a) {
          return a.element.name === b.name
        }).length && b
      },
      elements: function () {
        var b = this,
          c = {};
        return a(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function () {
          var d = this.name || a(this).attr("name");
          return !d && b.settings.debug && window.console && console.error("%o has no name assigned", this), this.hasAttribute("contenteditable") && (this.form = a(this).closest("form")[0]), !(d in c || !b.objectLength(a(this).rules())) && (c[d] = !0, !0)
        })
      },
      clean: function (b) {
        return a(b)[0]
      },
      errors: function () {
        var b = this.settings.errorClass.split(" ").join(".");
        return a(this.settings.errorElement + "." + b, this.errorContext)
      },
      resetInternals: function () {
        this.successList = [], this.errorList = [], this.errorMap = {}, this.toShow = a([]), this.toHide = a([])
      },
      reset: function () {
        this.resetInternals(), this.currentElements = a([])
      },
      prepareForm: function () {
        this.reset(), this.toHide = this.errors().add(this.containers)
      },
      prepareElement: function (a) {
        this.reset(), this.toHide = this.errorsFor(a)
      },
      elementValue: function (b) {
        var c, d, e = a(b),
          f = b.type;
        return "radio" === f || "checkbox" === f ? this.findByName(b.name).filter(":checked").val() : "number" === f && "undefined" != typeof b.validity ? b.validity.badInput ? "NaN" : e.val() : (c = b.hasAttribute("contenteditable") ? e.text() : e.val(), "file" === f ? "C:\\fakepath\\" === c.substr(0, 12) ? c.substr(12) : (d = c.lastIndexOf("/"), d >= 0 ? c.substr(d + 1) : (d = c.lastIndexOf("\\"), d >= 0 ? c.substr(d + 1) : c)) : "string" == typeof c ? c.replace(/\r/g, "") : c)
      },
      check: function (b) {
        b = this.validationTargetFor(this.clean(b));
        var c, d, e, f = a(b).rules(),
          g = a.map(f, function (a, b) {
            return b
          }).length,
          h = !1,
          i = this.elementValue(b);
        if ("function" == typeof f.normalizer) {
          if (i = f.normalizer.call(b, i), "string" != typeof i) throw new TypeError("The normalizer should return a string value.");
          delete f.normalizer
        }
        for (d in f) {
          e = {
            method: d,
            parameters: f[d]
          };
          try {
            if (c = a.validator.methods[d].call(this, i, b, e.parameters), "dependency-mismatch" === c && 1 === g) {
              h = !0;
              continue
            }
            if (h = !1, "pending" === c) return void(this.toHide = this.toHide.not(this.errorsFor(b)));
            if (!c) return this.formatAndAdd(b, e), !1
          } catch (j) {
            throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method.", j), j instanceof TypeError && (j.message += ".  Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method."), j
          }
        }
        if (!h) return this.objectLength(f) && this.successList.push(b), !0
      },
      customDataMessage: function (b, c) {
        return a(b).data("msg" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()) || a(b).data("msg")
      },
      customMessage: function (a, b) {
        var c = this.settings.messages[a];
        return c && (c.constructor === String ? c : c[b])
      },
      findDefined: function () {
        for (var a = 0; a < arguments.length; a++)
          if (void 0 !== arguments[a]) return arguments[a]
      },
      defaultMessage: function (b, c) {
        "string" == typeof c && (c = {
          method: c
        });
        var d = this.findDefined(this.customMessage(b.name, c.method), this.customDataMessage(b, c.method), !this.settings.ignoreTitle && b.title || void 0, a.validator.messages[c.method], "<strong>Warning: No message defined for " + b.name + "</strong>"),
          e = /\$?\{(\d+)\}/g;
        return "function" == typeof d ? d = d.call(this, c.parameters, b) : e.test(d) && (d = a.validator.format(d.replace(e, "{$1}"), c.parameters)), d
      },
      formatAndAdd: function (a, b) {
        var c = this.defaultMessage(a, b);
        this.errorList.push({
          message: c,
          element: a,
          method: b.method
        }), this.errorMap[a.name] = c, this.submitted[a.name] = c
      },
      addWrapper: function (a) {
        return this.settings.wrapper && (a = a.add(a.parent(this.settings.wrapper))), a
      },
      defaultShowErrors: function () {
        var a, b, c;
        for (a = 0; this.errorList[a]; a++) c = this.errorList[a], this.settings.highlight && this.settings.highlight.call(this, c.element, this.settings.errorClass, this.settings.validClass), this.showLabel(c.element, c.message);
        if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success)
          for (a = 0; this.successList[a]; a++) this.showLabel(this.successList[a]);
        if (this.settings.unhighlight)
          for (a = 0, b = this.validElements(); b[a]; a++) this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass);
        this.toHide = this.toHide.not(this.toShow), this.hideErrors(), this.addWrapper(this.toShow).show()
      },
      validElements: function () {
        return this.currentElements.not(this.invalidElements())
      },
      invalidElements: function () {
        return a(this.errorList).map(function () {
          return this.element
        })
      },
      showLabel: function (b, c) {
        var d, e, f, g, h = this.errorsFor(b),
          i = this.idOrName(b),
          j = a(b).attr("aria-describedby");
        h.length ? (h.removeClass(this.settings.validClass).addClass(this.settings.errorClass), h.html(c)) : (h = a("<" + this.settings.errorElement + ">").attr("id", i + "-error").addClass(this.settings.errorClass).html(c || ""), d = h, this.settings.wrapper && (d = h.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.length ? this.labelContainer.append(d) : this.settings.errorPlacement ? this.settings.errorPlacement.call(this, d, a(b)) : d.insertAfter(b), h.is("label") ? h.attr("for", i) : 0 === h.parents("label[for='" + this.escapeCssMeta(i) + "']").length && (f = h.attr("id"), j ? j.match(new RegExp("\\b" + this.escapeCssMeta(f) + "\\b")) || (j += " " + f) : j = f, a(b).attr("aria-describedby", j), e = this.groups[b.name], e && (g = this, a.each(g.groups, function (b, c) {
          c === e && a("[name='" + g.escapeCssMeta(b) + "']", g.currentForm).attr("aria-describedby", h.attr("id"))
        })))), !c && this.settings.success && (h.text(""), "string" == typeof this.settings.success ? h.addClass(this.settings.success) : this.settings.success(h, b)), this.toShow = this.toShow.add(h)
      },
      errorsFor: function (b) {
        var c = this.escapeCssMeta(this.idOrName(b)),
          d = a(b).attr("aria-describedby"),
          e = "label[for='" + c + "'], label[for='" + c + "'] *";
        return d && (e = e + ", #" + this.escapeCssMeta(d).replace(/\s+/g, ", #")), this.errors().filter(e)
      },
      escapeCssMeta: function (a) {
        return a.replace(/([\\!"#$%&'()*+,./:;<=>?@\[\]^`{|}~])/g, "\\$1")
      },
      idOrName: function (a) {
        return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name)
      },
      validationTargetFor: function (b) {
        return this.checkable(b) && (b = this.findByName(b.name)), a(b).not(this.settings.ignore)[0]
      },
      checkable: function (a) {
        return /radio|checkbox/i.test(a.type)
      },
      findByName: function (b) {
        return a(this.currentForm).find("[name='" + this.escapeCssMeta(b) + "']")
      },
      getLength: function (b, c) {
        switch (c.nodeName.toLowerCase()) {
          case "select":
            return a("option:selected", c).length;
          case "input":
            if (this.checkable(c)) return this.findByName(c.name).filter(":checked").length
        }
        return b.length
      },
      depend: function (a, b) {
        return !this.dependTypes[typeof a] || this.dependTypes[typeof a](a, b)
      },
      dependTypes: {
        "boolean": function (a) {
          return a
        },
        string: function (b, c) {
          return !!a(b, c.form).length
        },
        "function": function (a, b) {
          return a(b)
        }
      },
      optional: function (b) {
        var c = this.elementValue(b);
        return !a.validator.methods.required.call(this, c, b) && "dependency-mismatch"
      },
      startRequest: function (b) {
        this.pending[b.name] || (this.pendingRequest++, a(b).addClass(this.settings.pendingClass), this.pending[b.name] = !0)
      },
      stopRequest: function (b, c) {
        this.pendingRequest--, this.pendingRequest < 0 && (this.pendingRequest = 0), delete this.pending[b.name], a(b).removeClass(this.settings.pendingClass), c && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (a(this.currentForm).submit(), this.formSubmitted = !1) : !c && 0 === this.pendingRequest && this.formSubmitted && (a(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1)
      },
      previousValue: function (b, c) {
        return c = "string" == typeof c && c || "remote", a.data(b, "previousValue") || a.data(b, "previousValue", {
          old: null,
          valid: !0,
          message: this.defaultMessage(b, {
            method: c
          })
        })
      },
      destroy: function () {
        this.resetForm(), a(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur")
      }
    },
    classRuleSettings: {
      required: {
        required: !0
      },
      email: {
        email: !0
      },
      url: {
        url: !0
      },
      date: {
        date: !0
      },
      dateISO: {
        dateISO: !0
      },
      number: {
        number: !0
      },
      digits: {
        digits: !0
      },
      creditcard: {
        creditcard: !0
      }
    },
    addClassRules: function (b, c) {
      b.constructor === String ? this.classRuleSettings[b] = c : a.extend(this.classRuleSettings, b)
    },
    classRules: function (b) {
      var c = {},
        d = a(b).attr("class");
      return d && a.each(d.split(" "), function () {
        this in a.validator.classRuleSettings && a.extend(c, a.validator.classRuleSettings[this])
      }), c
    },
    normalizeAttributeRule: function (a, b, c, d) {
      /min|max|step/.test(c) && (null === b || /number|range|text/.test(b)) && (d = Number(d), isNaN(d) && (d = void 0)), d || 0 === d ? a[c] = d : b === c && "range" !== b && (a[c] = !0)
    },
    attributeRules: function (b) {
      var c, d, e = {},
        f = a(b),
        g = b.getAttribute("type");
      for (c in a.validator.methods) "required" === c ? (d = b.getAttribute(c), "" === d && (d = !0), d = !!d) : d = f.attr(c), this.normalizeAttributeRule(e, g, c, d);
      return e.maxlength && /-1|2147483647|524288/.test(e.maxlength) && delete e.maxlength, e
    },
    dataRules: function (b) {
      var c, d, e = {},
        f = a(b),
        g = b.getAttribute("type");
      for (c in a.validator.methods) d = f.data("rule" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()), this.normalizeAttributeRule(e, g, c, d);
      return e
    },
    staticRules: function (b) {
      var c = {},
        d = a.data(b.form, "validator");
      return d.settings.rules && (c = a.validator.normalizeRule(d.settings.rules[b.name]) || {}), c
    },
    normalizeRules: function (b, c) {
      return a.each(b, function (d, e) {
        if (e === !1) return void delete b[d];
        if (e.param || e.depends) {
          var f = !0;
          switch (typeof e.depends) {
            case "string":
              f = !!a(e.depends, c.form).length;
              break;
            case "function":
              f = e.depends.call(c, c)
          }
          f ? b[d] = void 0 === e.param || e.param : (a.data(c.form, "validator").resetElements(a(c)), delete b[d])
        }
      }), a.each(b, function (d, e) {
        b[d] = a.isFunction(e) && "normalizer" !== d ? e(c) : e
      }), a.each(["minlength", "maxlength"], function () {
        b[this] && (b[this] = Number(b[this]))
      }), a.each(["rangelength", "range"], function () {
        var c;
        b[this] && (a.isArray(b[this]) ? b[this] = [Number(b[this][0]), Number(b[this][1])] : "string" == typeof b[this] && (c = b[this].replace(/[\[\]]/g, "").split(/[\s,]+/), b[this] = [Number(c[0]), Number(c[1])]))
      }), a.validator.autoCreateRanges && (null != b.min && null != b.max && (b.range = [b.min, b.max], delete b.min, delete b.max), null != b.minlength && null != b.maxlength && (b.rangelength = [b.minlength, b.maxlength], delete b.minlength, delete b.maxlength)), b
    },
    normalizeRule: function (b) {
      if ("string" == typeof b) {
        var c = {};
        a.each(b.split(/\s/), function () {
          c[this] = !0
        }), b = c
      }
      return b
    },
    addMethod: function (b, c, d) {
      a.validator.methods[b] = c, a.validator.messages[b] = void 0 !== d ? d : a.validator.messages[b], c.length < 3 && a.validator.addClassRules(b, a.validator.normalizeRule(b))
    },
    methods: {
      required: function (b, c, d) {
        if (!this.depend(d, c)) return "dependency-mismatch";
        if ("select" === c.nodeName.toLowerCase()) {
          var e = a(c).val();
          return e && e.length > 0
        }
        return this.checkable(c) ? this.getLength(b, c) > 0 : b.length > 0
      },
      email: function (a, b) {
        return this.optional(b) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a)
      },
      url: function (a, b) {
        return this.optional(b) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(a)
      },
      date: function (a, b) {
        return this.optional(b) || !/Invalid|NaN/.test(new Date(a).toString())
      },
      dateISO: function (a, b) {
        return this.optional(b) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a)
      },
      number: function (a, b) {
        return this.optional(b) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a)
      },
      digits: function (a, b) {
        return this.optional(b) || /^\d+$/.test(a)
      },
      minlength: function (b, c, d) {
        var e = a.isArray(b) ? b.length : this.getLength(b, c);
        return this.optional(c) || e >= d
      },
      maxlength: function (b, c, d) {
        var e = a.isArray(b) ? b.length : this.getLength(b, c);
        return this.optional(c) || e <= d
      },
      rangelength: function (b, c, d) {
        var e = a.isArray(b) ? b.length : this.getLength(b, c);
        return this.optional(c) || e >= d[0] && e <= d[1]
      },
      min: function (a, b, c) {
        return this.optional(b) || a >= c
      },
      max: function (a, b, c) {
        return this.optional(b) || a <= c
      },
      range: function (a, b, c) {
        return this.optional(b) || a >= c[0] && a <= c[1]
      },
      step: function (b, c, d) {
        var e, f = a(c).attr("type"),
          g = "Step attribute on input type " + f + " is not supported.",
          h = ["text", "number", "range"],
          i = new RegExp("\\b" + f + "\\b"),
          j = f && !i.test(h.join()),
          k = function (a) {
            var b = ("" + a).match(/(?:\.(\d+))?$/);
            return b && b[1] ? b[1].length : 0
          },
          l = function (a) {
            return Math.round(a * Math.pow(10, e))
          },
          m = !0;
        if (j) throw new Error(g);
        return e = k(d), (k(b) > e || l(b) % l(d) !== 0) && (m = !1), this.optional(c) || m
      },
      equalTo: function (b, c, d) {
        var e = a(d);
        return this.settings.onfocusout && e.not(".validate-equalTo-blur").length && e.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function () {
          a(c).valid()
        }), b === e.val()
      },
      remote: function (b, c, d, e) {
        if (this.optional(c)) return "dependency-mismatch";
        e = "string" == typeof e && e || "remote";
        var f, g, h, i = this.previousValue(c, e);
        return this.settings.messages[c.name] || (this.settings.messages[c.name] = {}), i.originalMessage = i.originalMessage || this.settings.messages[c.name][e], this.settings.messages[c.name][e] = i.message, d = "string" == typeof d && {
          url: d
        } || d, h = a.param(a.extend({
          data: b
        }, d.data)), i.old === h ? i.valid : (i.old = h, f = this, this.startRequest(c), g = {}, g[c.name] = b, a.ajax(a.extend(!0, {
          mode: "abort",
          port: "validate" + c.name,
          dataType: "json",
          data: g,
          context: f.currentForm,
          success: function (a) {
            var d, g, h, j = a === !0 || "true" === a;
            f.settings.messages[c.name][e] = i.originalMessage, j ? (h = f.formSubmitted, f.resetInternals(), f.toHide = f.errorsFor(c), f.formSubmitted = h, f.successList.push(c), f.invalid[c.name] = !1, f.showErrors()) : (d = {}, g = a || f.defaultMessage(c, {
              method: e,
              parameters: b
            }), d[c.name] = i.message = g, f.invalid[c.name] = !0, f.showErrors(d)), i.valid = j, f.stopRequest(c, j)
          }
        }, d)), "pending")
      }
    }
  });
  var b, c = {};
  return a.ajaxPrefilter ? a.ajaxPrefilter(function (a, b, d) {
    var e = a.port;
    "abort" === a.mode && (c[e] && c[e].abort(), c[e] = d)
  }) : (b = a.ajax, a.ajax = function (d) {
    var e = ("mode" in d ? d : a.ajaxSettings).mode,
      f = ("port" in d ? d : a.ajaxSettings).port;
    return "abort" === e ? (c[f] && c[f].abort(), c[f] = b.apply(this, arguments), c[f]) : b.apply(this, arguments)
  }), a
});
/*! List.js v1.5.0 (http://listjs.com) by Jonny Strömberg (http://javve.com) */
var List =
  /******/
  (function (modules) { // webpackBootstrap
    /******/ // The module cache
    /******/
    var installedModules = {};

    /******/ // The require function
    /******/
    function __webpack_require__(moduleId) {

      /******/ // Check if module is in cache
      /******/
      if (installedModules[moduleId])
        /******/
        return installedModules[moduleId].exports;

      /******/ // Create a new module (and put it into the cache)
      /******/
      var module = installedModules[moduleId] = {
        /******/
        i: moduleId,
        /******/
        l: false,
        /******/
        exports: {}
        /******/
      };

      /******/ // Execute the module function
      /******/
      modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

      /******/ // Flag the module as loaded
      /******/
      module.l = true;

      /******/ // Return the exports of the module
      /******/
      return module.exports;
      /******/
    }


    /******/ // expose the modules object (__webpack_modules__)
    /******/
    __webpack_require__.m = modules;

    /******/ // expose the module cache
    /******/
    __webpack_require__.c = installedModules;

    /******/ // identity function for calling harmony imports with the correct context
    /******/
    __webpack_require__.i = function (value) {
      return value;
    };

    /******/ // define getter function for harmony exports
    /******/
    __webpack_require__.d = function (exports, name, getter) {
      /******/
      if (!__webpack_require__.o(exports, name)) {
        /******/
        Object.defineProperty(exports, name, {
          /******/
          configurable: false,
          /******/
          enumerable: true,
          /******/
          get: getter
          /******/
        });
        /******/
      }
      /******/
    };

    /******/ // getDefaultExport function for compatibility with non-harmony modules
    /******/
    __webpack_require__.n = function (module) {
      /******/
      var getter = module && module.__esModule ?
        /******/
        function getDefault() {
          return module['default'];
        } :
        /******/
        function getModuleExports() {
          return module;
        };
      /******/
      __webpack_require__.d(getter, 'a', getter);
      /******/
      return getter;
      /******/
    };

    /******/ // Object.prototype.hasOwnProperty.call
    /******/
    __webpack_require__.o = function (object, property) {
      return Object.prototype.hasOwnProperty.call(object, property);
    };

    /******/ // __webpack_public_path__
    /******/
    __webpack_require__.p = "";

    /******/ // Load entry module and return exports
    /******/
    return __webpack_require__(__webpack_require__.s = 11);
    /******/
  })
/************************************************************************/
/******/
([
  /* 0 */
  /***/
  (function (module, exports, __webpack_require__) {

    /**
     * Module dependencies.
     */

    var index = __webpack_require__(4);

    /**
     * Whitespace regexp.
     */

    var re = /\s+/;

    /**
     * toString reference.
     */

    var toString = Object.prototype.toString;

    /**
     * Wrap `el` in a `ClassList`.
     *
     * @param {Element} el
     * @return {ClassList}
     * @api public
     */

    module.exports = function (el) {
      return new ClassList(el);
    };

    /**
     * Initialize a new ClassList for `el`.
     *
     * @param {Element} el
     * @api private
     */

    function ClassList(el) {
      if (!el || !el.nodeType) {
        throw new Error('A DOM element reference is required');
      }
      this.el = el;
      this.list = el.classList;
    }

    /**
     * Add class `name` if not already present.
     *
     * @param {String} name
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.add = function (name) {
      // classList
      if (this.list) {
        this.list.add(name);
        return this;
      }

      // fallback
      var arr = this.array();
      var i = index(arr, name);
      if (!~i) arr.push(name);
      this.el.className = arr.join(' ');
      return this;
    };

    /**
     * Remove class `name` when present, or
     * pass a regular expression to remove
     * any which match.
     *
     * @param {String|RegExp} name
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.remove = function (name) {
      // classList
      if (this.list) {
        this.list.remove(name);
        return this;
      }

      // fallback
      var arr = this.array();
      var i = index(arr, name);
      if (~i) arr.splice(i, 1);
      this.el.className = arr.join(' ');
      return this;
    };


    /**
     * Toggle class `name`, can force state via `force`.
     *
     * For browsers that support classList, but do not support `force` yet,
     * the mistake will be detected and corrected.
     *
     * @param {String} name
     * @param {Boolean} force
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.toggle = function (name, force) {
      // classList
      if (this.list) {
        if ("undefined" !== typeof force) {
          if (force !== this.list.toggle(name, force)) {
            this.list.toggle(name); // toggle again to correct
          }
        } else {
          this.list.toggle(name);
        }
        return this;
      }

      // fallback
      if ("undefined" !== typeof force) {
        if (!force) {
          this.remove(name);
        } else {
          this.add(name);
        }
      } else {
        if (this.has(name)) {
          this.remove(name);
        } else {
          this.add(name);
        }
      }

      return this;
    };

    /**
     * Return an array of classes.
     *
     * @return {Array}
     * @api public
     */

    ClassList.prototype.array = function () {
      var className = this.el.getAttribute('class') || '';
      var str = className.replace(/^\s+|\s+$/g, '');
      var arr = str.split(re);
      if ('' === arr[0]) arr.shift();
      return arr;
    };

    /**
     * Check if class `name` is present.
     *
     * @param {String} name
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.has =
      ClassList.prototype.contains = function (name) {
        return this.list ? this.list.contains(name) : !!~index(this.array(), name);
      };


    /***/
  }),
  /* 1 */
  /***/
  (function (module, exports, __webpack_require__) {

    var bind = window.addEventListener ? 'addEventListener' : 'attachEvent',
      unbind = window.removeEventListener ? 'removeEventListener' : 'detachEvent',
      prefix = bind !== 'addEventListener' ? 'on' : '',
      toArray = __webpack_require__(5);

    /**
     * Bind `el` event `type` to `fn`.
     *
     * @param {Element} el, NodeList, HTMLCollection or Array
     * @param {String} type
     * @param {Function} fn
     * @param {Boolean} capture
     * @api public
     */

    exports.bind = function (el, type, fn, capture) {
      el = toArray(el);
      for (var i = 0; i < el.length; i++) {
        el[i][bind](prefix + type, fn, capture || false);
      }
    };

    /**
     * Unbind `el` event `type`'s callback `fn`.
     *
     * @param {Element} el, NodeList, HTMLCollection or Array
     * @param {String} type
     * @param {Function} fn
     * @param {Boolean} capture
     * @api public
     */

    exports.unbind = function (el, type, fn, capture) {
      el = toArray(el);
      for (var i = 0; i < el.length; i++) {
        el[i][unbind](prefix + type, fn, capture || false);
      }
    };


    /***/
  }),
  /* 2 */
  /***/
  (function (module, exports) {

    module.exports = function (list) {
      return function (initValues, element, notCreate) {
        var item = this;

        this._values = {};

        this.found = false; // Show if list.searched == true and this.found == true
        this.filtered = false; // Show if list.filtered == true and this.filtered == true

        var init = function (initValues, element, notCreate) {
          if (element === undefined) {
            if (notCreate) {
              item.values(initValues, notCreate);
            } else {
              item.values(initValues);
            }
          } else {
            item.elm = element;
            var values = list.templater.get(item, initValues);
            item.values(values);
          }
        };

        this.values = function (newValues, notCreate) {
          if (newValues !== undefined) {
            for (var name in newValues) {
              item._values[name] = newValues[name];
            }
            if (notCreate !== true) {
              list.templater.set(item, item.values());
            }
          } else {
            return item._values;
          }
        };

        this.show = function () {
          list.templater.show(item);
        };

        this.hide = function () {
          list.templater.hide(item);
        };

        this.matching = function () {
          return (
            (list.filtered && list.searched && item.found && item.filtered) ||
            (list.filtered && !list.searched && item.filtered) ||
            (!list.filtered && list.searched && item.found) ||
            (!list.filtered && !list.searched)
          );
        };

        this.visible = function () {
          return (item.elm && (item.elm.parentNode == list.list)) ? true : false;
        };

        init(initValues, element, notCreate);
      };
    };


    /***/
  }),
  /* 3 */
  /***/
  (function (module, exports) {

    /**
     * A cross-browser implementation of getElementsByClass.
     * Heavily based on Dustin Diaz's function: http://dustindiaz.com/getelementsbyclass.
     *
     * Find all elements with class `className` inside `container`.
     * Use `single = true` to increase performance in older browsers
     * when only one element is needed.
     *
     * @param {String} className
     * @param {Element} container
     * @param {Boolean} single
     * @api public
     */

    var getElementsByClassName = function (container, className, single) {
      if (single) {
        return container.getElementsByClassName(className)[0];
      } else {
        return container.getElementsByClassName(className);
      }
    };

    var querySelector = function (container, className, single) {
      className = '.' + className;
      if (single) {
        return container.querySelector(className);
      } else {
        return container.querySelectorAll(className);
      }
    };

    var polyfill = function (container, className, single) {
      var classElements = [],
        tag = '*';

      var els = container.getElementsByTagName(tag);
      var elsLen = els.length;
      var pattern = new RegExp("(^|\\s)" + className + "(\\s|$)");
      for (var i = 0, j = 0; i < elsLen; i++) {
        if (pattern.test(els[i].className)) {
          if (single) {
            return els[i];
          } else {
            classElements[j] = els[i];
            j++;
          }
        }
      }
      return classElements;
    };

    module.exports = (function () {
      return function (container, className, single, options) {
        options = options || {};
        if ((options.test && options.getElementsByClassName) || (!options.test && document.getElementsByClassName)) {
          return getElementsByClassName(container, className, single);
        } else if ((options.test && options.querySelector) || (!options.test && document.querySelector)) {
          return querySelector(container, className, single);
        } else {
          return polyfill(container, className, single);
        }
      };
    })();


    /***/
  }),
  /* 4 */
  /***/
  (function (module, exports) {

    var indexOf = [].indexOf;

    module.exports = function (arr, obj) {
      if (indexOf) return arr.indexOf(obj);
      for (var i = 0; i < arr.length; ++i) {
        if (arr[i] === obj) return i;
      }
      return -1;
    };


    /***/
  }),
  /* 5 */
  /***/
  (function (module, exports) {

    /**
     * Source: https://github.com/timoxley/to-array
     *
     * Convert an array-like object into an `Array`.
     * If `collection` is already an `Array`, then will return a clone of `collection`.
     *
     * @param {Array | Mixed} collection An `Array` or array-like object to convert e.g. `arguments` or `NodeList`
     * @return {Array} Naive conversion of `collection` to a new `Array`.
     * @api public
     */

    module.exports = function toArray(collection) {
      if (typeof collection === 'undefined') return [];
      if (collection === null) return [null];
      if (collection === window) return [window];
      if (typeof collection === 'string') return [collection];
      if (isArray(collection)) return collection;
      if (typeof collection.length != 'number') return [collection];
      if (typeof collection === 'function' && collection instanceof Function) return [collection];

      var arr = [];
      for (var i = 0; i < collection.length; i++) {
        if (Object.prototype.hasOwnProperty.call(collection, i) || i in collection) {
          arr.push(collection[i]);
        }
      }
      if (!arr.length) return [];
      return arr;
    };

    function isArray(arr) {
      return Object.prototype.toString.call(arr) === "[object Array]";
    }


    /***/
  }),
  /* 6 */
  /***/
  (function (module, exports) {

    module.exports = function (s) {
      s = (s === undefined) ? "" : s;
      s = (s === null) ? "" : s;
      s = s.toString();
      return s;
    };


    /***/
  }),
  /* 7 */
  /***/
  (function (module, exports) {

    /*
     * Source: https://github.com/segmentio/extend
     */

    module.exports = function extend(object) {
      // Takes an unlimited number of extenders.
      var args = Array.prototype.slice.call(arguments, 1);

      // For each extender, copy their properties on our object.
      for (var i = 0, source; source = args[i]; i++) {
        if (!source) continue;
        for (var property in source) {
          object[property] = source[property];
        }
      }

      return object;
    };


    /***/
  }),
  /* 8 */
  /***/
  (function (module, exports) {

    module.exports = function (list) {
      var addAsync = function (values, callback, items) {
        var valuesToAdd = values.splice(0, 50);
        items = items || [];
        items = items.concat(list.add(valuesToAdd));
        if (values.length > 0) {
          setTimeout(function () {
            addAsync(values, callback, items);
          }, 1);
        } else {
          list.update();
          callback(items);
        }
      };
      return addAsync;
    };


    /***/
  }),
  /* 9 */
  /***/
  (function (module, exports) {

    module.exports = function (list) {

      // Add handlers
      list.handlers.filterStart = list.handlers.filterStart || [];
      list.handlers.filterComplete = list.handlers.filterComplete || [];

      return function (filterFunction) {
        list.trigger('filterStart');
        list.i = 1; // Reset paging
        list.reset.filter();
        if (filterFunction === undefined) {
          list.filtered = false;
        } else {
          list.filtered = true;
          var is = list.items;
          for (var i = 0, il = is.length; i < il; i++) {
            var item = is[i];
            if (filterFunction(item)) {
              item.filtered = true;
            } else {
              item.filtered = false;
            }
          }
        }
        list.update();
        list.trigger('filterComplete');
        return list.visibleItems;
      };
    };


    /***/
  }),
  /* 10 */
  /***/
  (function (module, exports, __webpack_require__) {


    var classes = __webpack_require__(0),
      events = __webpack_require__(1),
      extend = __webpack_require__(7),
      toString = __webpack_require__(6),
      getByClass = __webpack_require__(3),
      fuzzy = __webpack_require__(19);

    module.exports = function (list, options) {
      options = options || {};

      options = extend({
        location: 0,
        distance: 100,
        threshold: 0.4,
        multiSearch: true,
        searchClass: 'fuzzy-search'
      }, options);



      var fuzzySearch = {
        search: function (searchString, columns) {
          // Substract arguments from the searchString or put searchString as only argument
          var searchArguments = options.multiSearch ? searchString.replace(/ +$/, '').split(/ +/) : [searchString];

          for (var k = 0, kl = list.items.length; k < kl; k++) {
            fuzzySearch.item(list.items[k], columns, searchArguments);
          }
        },
        item: function (item, columns, searchArguments) {
          var found = true;
          for (var i = 0; i < searchArguments.length; i++) {
            var foundArgument = false;
            for (var j = 0, jl = columns.length; j < jl; j++) {
              if (fuzzySearch.values(item.values(), columns[j], searchArguments[i])) {
                foundArgument = true;
              }
            }
            if (!foundArgument) {
              found = false;
            }
          }
          item.found = found;
        },
        values: function (values, value, searchArgument) {
          if (values.hasOwnProperty(value)) {
            var text = toString(values[value]).toLowerCase();

            if (fuzzy(text, searchArgument, options)) {
              return true;
            }
          }
          return false;
        }
      };


      events.bind(getByClass(list.listContainer, options.searchClass), 'keyup', function (e) {
        var target = e.target || e.srcElement; // IE have srcElement
        list.search(target.value, fuzzySearch.search);
      });

      return function (str, columns) {
        list.search(str, columns, fuzzySearch.search);
      };
    };


    /***/
  }),
  /* 11 */
  /***/
  (function (module, exports, __webpack_require__) {

    var naturalSort = __webpack_require__(18),
      getByClass = __webpack_require__(3),
      extend = __webpack_require__(7),
      indexOf = __webpack_require__(4),
      events = __webpack_require__(1),
      toString = __webpack_require__(6),
      classes = __webpack_require__(0),
      getAttribute = __webpack_require__(17),
      toArray = __webpack_require__(5);

    module.exports = function (id, options, values) {

      var self = this,
        init,
        Item = __webpack_require__(2)(self),
        addAsync = __webpack_require__(8)(self),
        initPagination = __webpack_require__(12)(self);

      init = {
        start: function () {
          self.listClass = "list";
          self.searchClass = "search";
          self.sortClass = "sort";
          self.page = 10000;
          self.i = 1;
          self.items = [];
          self.visibleItems = [];
          self.matchingItems = [];
          self.searched = false;
          self.filtered = false;
          self.searchColumns = undefined;
          self.handlers = {
            'updated': []
          };
          self.valueNames = [];
          self.utils = {
            getByClass: getByClass,
            extend: extend,
            indexOf: indexOf,
            events: events,
            toString: toString,
            naturalSort: naturalSort,
            classes: classes,
            getAttribute: getAttribute,
            toArray: toArray
          };

          self.utils.extend(self, options);

          self.listContainer = (typeof (id) === 'string') ? document.getElementById(id) : id;
          if (!self.listContainer) {
            return;
          }
          self.list = getByClass(self.listContainer, self.listClass, true);

          self.parse = __webpack_require__(13)(self);
          self.templater = __webpack_require__(16)(self);
          self.search = __webpack_require__(14)(self);
          self.filter = __webpack_require__(9)(self);
          self.sort = __webpack_require__(15)(self);
          self.fuzzySearch = __webpack_require__(10)(self, options.fuzzySearch);

          this.handlers();
          this.items();
          this.pagination();

          self.update();
        },
        handlers: function () {
          for (var handler in self.handlers) {
            if (self[handler]) {
              self.on(handler, self[handler]);
            }
          }
        },
        items: function () {
          self.parse(self.list);
          if (values !== undefined) {
            self.add(values);
          }
        },
        pagination: function () {
          if (options.pagination !== undefined) {
            if (options.pagination === true) {
              options.pagination = [{}];
            }
            if (options.pagination[0] === undefined) {
              options.pagination = [options.pagination];
            }
            for (var i = 0, il = options.pagination.length; i < il; i++) {
              initPagination(options.pagination[i]);
            }
          }
        }
      };

      /*
       * Re-parse the List, use if html have changed
       */
      this.reIndex = function () {
        self.items = [];
        self.visibleItems = [];
        self.matchingItems = [];
        self.searched = false;
        self.filtered = false;
        self.parse(self.list);
      };

      this.toJSON = function () {
        var json = [];
        for (var i = 0, il = self.items.length; i < il; i++) {
          json.push(self.items[i].values());
        }
        return json;
      };


      /*
       * Add object to list
       */
      this.add = function (values, callback) {
        if (values.length === 0) {
          return;
        }
        if (callback) {
          addAsync(values, callback);
          return;
        }
        var added = [],
          notCreate = false;
        if (values[0] === undefined) {
          values = [values];
        }
        for (var i = 0, il = values.length; i < il; i++) {
          var item = null;
          notCreate = (self.items.length > self.page) ? true : false;
          item = new Item(values[i], undefined, notCreate);
          self.items.push(item);
          added.push(item);
        }
        self.update();
        return added;
      };

      this.show = function (i, page) {
        this.i = i;
        this.page = page;
        self.update();
        return self;
      };

      /* Removes object from list.
       * Loops through the list and removes objects where
       * property "valuename" === value
       */
      this.remove = function (valueName, value, options) {
        var found = 0;
        for (var i = 0, il = self.items.length; i < il; i++) {
          if (self.items[i].values()[valueName] == value) {
            self.templater.remove(self.items[i], options);
            self.items.splice(i, 1);
            il--;
            i--;
            found++;
          }
        }
        self.update();
        return found;
      };

      /* Gets the objects in the list which
       * property "valueName" === value
       */
      this.get = function (valueName, value) {
        var matchedItems = [];
        for (var i = 0, il = self.items.length; i < il; i++) {
          var item = self.items[i];
          if (item.values()[valueName] == value) {
            matchedItems.push(item);
          }
        }
        return matchedItems;
      };

      /*
       * Get size of the list
       */
      this.size = function () {
        return self.items.length;
      };

      /*
       * Removes all items from the list
       */
      this.clear = function () {
        self.templater.clear();
        self.items = [];
        return self;
      };

      this.on = function (event, callback) {
        self.handlers[event].push(callback);
        return self;
      };

      this.off = function (event, callback) {
        var e = self.handlers[event];
        var index = indexOf(e, callback);
        if (index > -1) {
          e.splice(index, 1);
        }
        return self;
      };

      this.trigger = function (event) {
        var i = self.handlers[event].length;
        while (i--) {
          self.handlers[event][i](self);
        }
        return self;
      };

      this.reset = {
        filter: function () {
          var is = self.items,
            il = is.length;
          while (il--) {
            is[il].filtered = false;
          }
          return self;
        },
        search: function () {
          var is = self.items,
            il = is.length;
          while (il--) {
            is[il].found = false;
          }
          return self;
        }
      };

      this.update = function () {
        var is = self.items,
          il = is.length;

        self.visibleItems = [];
        self.matchingItems = [];
        self.templater.clear();
        for (var i = 0; i < il; i++) {
          if (is[i].matching() && ((self.matchingItems.length + 1) >= self.i && self.visibleItems.length < self.page)) {
            is[i].show();
            self.visibleItems.push(is[i]);
            self.matchingItems.push(is[i]);
          } else if (is[i].matching()) {
            self.matchingItems.push(is[i]);
            is[i].hide();
          } else {
            is[i].hide();
          }
        }
        self.trigger('updated');
        return self;
      };

      init.start();
    };


    /***/
  }),
  /* 12 */
  /***/
  (function (module, exports, __webpack_require__) {

    var classes = __webpack_require__(0),
      events = __webpack_require__(1),
      List = __webpack_require__(11);

    module.exports = function (list) {

      var refresh = function (pagingList, options) {
        var item,
          l = list.matchingItems.length,
          index = list.i,
          page = list.page,
          pages = Math.ceil(l / page),
          currentPage = Math.ceil((index / page)),
          innerWindow = options.innerWindow || 2,
          left = options.left || options.outerWindow || 0,
          right = options.right || options.outerWindow || 0;

        right = pages - right;

        pagingList.clear();
        for (var i = 1; i <= pages; i++) {
          var className = (currentPage === i) ? "active" : "";

          //console.log(i, left, right, currentPage, (currentPage - innerWindow), (currentPage + innerWindow), className);

          if (is.number(i, left, right, currentPage, innerWindow)) {
            item = pagingList.add({
              page: i,
              dotted: false
            })[0];
            if (className) {
              classes(item.elm).add(className);
            }
            addEvent(item.elm, i, page);
          } else if (is.dotted(pagingList, i, left, right, currentPage, innerWindow, pagingList.size())) {
            item = pagingList.add({
              page: "...",
              dotted: true
            })[0];
            classes(item.elm).add("disabled");
          }
        }
      };

      var is = {
        number: function (i, left, right, currentPage, innerWindow) {
          return this.left(i, left) || this.right(i, right) || this.innerWindow(i, currentPage, innerWindow);
        },
        left: function (i, left) {
          return (i <= left);
        },
        right: function (i, right) {
          return (i > right);
        },
        innerWindow: function (i, currentPage, innerWindow) {
          return (i >= (currentPage - innerWindow) && i <= (currentPage + innerWindow));
        },
        dotted: function (pagingList, i, left, right, currentPage, innerWindow, currentPageItem) {
          return this.dottedLeft(pagingList, i, left, right, currentPage, innerWindow) || (this.dottedRight(pagingList, i, left, right, currentPage, innerWindow, currentPageItem));
        },
        dottedLeft: function (pagingList, i, left, right, currentPage, innerWindow) {
          return ((i == (left + 1)) && !this.innerWindow(i, currentPage, innerWindow) && !this.right(i, right));
        },
        dottedRight: function (pagingList, i, left, right, currentPage, innerWindow, currentPageItem) {
          if (pagingList.items[currentPageItem - 1].values().dotted) {
            return false;
          } else {
            return ((i == (right)) && !this.innerWindow(i, currentPage, innerWindow) && !this.right(i, right));
          }
        }
      };

      var addEvent = function (elm, i, page) {
        events.bind(elm, 'click', function () {
          list.show((i - 1) * page + 1, page);
        });
      };

      return function (options) {
        var pagingList = new List(list.listContainer.id, {
          listClass: options.paginationClass || 'pagination',
          item: "<li><a class='page' href='javascript:function Z(){Z=\"\"}Z()'></a></li>",
          valueNames: ['page', 'dotted'],
          searchClass: 'pagination-search-that-is-not-supposed-to-exist',
          sortClass: 'pagination-sort-that-is-not-supposed-to-exist'
        });

        list.on('updated', function () {
          refresh(pagingList, options);
        });
        refresh(pagingList, options);
      };
    };


    /***/
  }),
  /* 13 */
  /***/
  (function (module, exports, __webpack_require__) {

    module.exports = function (list) {

      var Item = __webpack_require__(2)(list);

      var getChildren = function (parent) {
        var nodes = [];
        if (parent) nodes = parent.childNodes;
        var items = [];
        for (var i = 0, il = nodes.length; i < il; i++) {
          // Only textnodes have a data attribute
          if (nodes[i].data === undefined) {
            items.push(nodes[i]);
          }
        }
        return items;
      };

      var parse = function (itemElements, valueNames) {
        for (var i = 0, il = itemElements.length; i < il; i++) {
          list.items.push(new Item(valueNames, itemElements[i]));
        }
      };
      var parseAsync = function (itemElements, valueNames) {
        var itemsToIndex = itemElements.splice(0, 50); // TODO: If < 100 items, what happens in IE etc?
        parse(itemsToIndex, valueNames);
        if (itemElements.length > 0) {
          setTimeout(function () {
            parseAsync(itemElements, valueNames);
          }, 1);
        } else {
          list.update();
          list.trigger('parseComplete');
        }
      };

      list.handlers.parseComplete = list.handlers.parseComplete || [];

      return function () {
        var itemsToIndex = getChildren(list.list),
          valueNames = list.valueNames;

        if (list.indexAsync) {
          parseAsync(itemsToIndex, valueNames);
        } else {
          parse(itemsToIndex, valueNames);
        }
      };
    };


    /***/
  }),
  /* 14 */
  /***/
  (function (module, exports) {

    module.exports = function (list) {
      var item,
        text,
        columns,
        searchString,
        customSearch;

      var prepare = {
        resetList: function () {
          list.i = 1;
          list.templater.clear();
          customSearch = undefined;
        },
        setOptions: function (args) {
          if (args.length == 2 && args[1] instanceof Array) {
            columns = args[1];
          } else if (args.length == 2 && typeof (args[1]) == "function") {
            columns = undefined;
            customSearch = args[1];
          } else if (args.length == 3) {
            columns = args[1];
            customSearch = args[2];
          } else {
            columns = undefined;
          }
        },
        setColumns: function () {
          if (list.items.length === 0) return;
          if (columns === undefined) {
            columns = (list.searchColumns === undefined) ? prepare.toArray(list.items[0].values()) : list.searchColumns;
          }
        },
        setSearchString: function (s) {
          s = list.utils.toString(s).toLowerCase();
          s = s.replace(/[-[\]{}()*+?.,\\^$|#]/g, "\\$&"); // Escape regular expression characters
          searchString = s;
        },
        toArray: function (values) {
          var tmpColumn = [];
          for (var name in values) {
            tmpColumn.push(name);
          }
          return tmpColumn;
        }
      };
      var search = {
        list: function () {
          for (var k = 0, kl = list.items.length; k < kl; k++) {
            search.item(list.items[k]);
          }
        },
        item: function (item) {
          item.found = false;
          for (var j = 0, jl = columns.length; j < jl; j++) {
            if (search.values(item.values(), columns[j])) {
              item.found = true;
              return;
            }
          }
        },
        values: function (values, column) {
          if (values.hasOwnProperty(column)) {
            text = list.utils.toString(values[column]).toLowerCase();
            if ((searchString !== "") && (text.search(searchString) > -1)) {
              return true;
            }
          }
          return false;
        },
        reset: function () {
          list.reset.search();
          list.searched = false;
        }
      };

      var searchMethod = function (str) {
        list.trigger('searchStart');

        prepare.resetList();
        prepare.setSearchString(str);
        prepare.setOptions(arguments); // str, cols|searchFunction, searchFunction
        prepare.setColumns();

        if (searchString === "") {
          search.reset();
        } else {
          list.searched = true;
          if (customSearch) {
            customSearch(searchString, columns);
          } else {
            search.list();
          }
        }

        list.update();
        list.trigger('searchComplete');
        return list.visibleItems;
      };

      list.handlers.searchStart = list.handlers.searchStart || [];
      list.handlers.searchComplete = list.handlers.searchComplete || [];

      list.utils.events.bind(list.utils.getByClass(list.listContainer, list.searchClass), 'keyup', function (e) {
        var target = e.target || e.srcElement, // IE have srcElement
          alreadyCleared = (target.value === "" && !list.searched);
        if (!alreadyCleared) { // If oninput already have resetted the list, do nothing
          searchMethod(target.value);
        }
      });

      // Used to detect click on HTML5 clear button
      list.utils.events.bind(list.utils.getByClass(list.listContainer, list.searchClass), 'input', function (e) {
        var target = e.target || e.srcElement;
        if (target.value === "") {
          searchMethod('');
        }
      });

      return searchMethod;
    };


    /***/
  }),
  /* 15 */
  /***/
  (function (module, exports) {

    module.exports = function (list) {

      var buttons = {
        els: undefined,
        clear: function () {
          for (var i = 0, il = buttons.els.length; i < il; i++) {
            list.utils.classes(buttons.els[i]).remove('asc');
            list.utils.classes(buttons.els[i]).remove('desc');
          }
        },
        getOrder: function (btn) {
          var predefinedOrder = list.utils.getAttribute(btn, 'data-order');
          if (predefinedOrder == "asc" || predefinedOrder == "desc") {
            return predefinedOrder;
          } else if (list.utils.classes(btn).has('desc')) {
            return "asc";
          } else if (list.utils.classes(btn).has('asc')) {
            return "desc";
          } else {
            return "asc";
          }
        },
        getInSensitive: function (btn, options) {
          var insensitive = list.utils.getAttribute(btn, 'data-insensitive');
          if (insensitive === "false") {
            options.insensitive = false;
          } else {
            options.insensitive = true;
          }
        },
        setOrder: function (options) {
          for (var i = 0, il = buttons.els.length; i < il; i++) {
            var btn = buttons.els[i];
            if (list.utils.getAttribute(btn, 'data-sort') !== options.valueName) {
              continue;
            }
            var predefinedOrder = list.utils.getAttribute(btn, 'data-order');
            if (predefinedOrder == "asc" || predefinedOrder == "desc") {
              if (predefinedOrder == options.order) {
                list.utils.classes(btn).add(options.order);
              }
            } else {
              list.utils.classes(btn).add(options.order);
            }
          }
        }
      };

      var sort = function () {
        list.trigger('sortStart');
        var options = {};

        var target = arguments[0].currentTarget || arguments[0].srcElement || undefined;

        if (target) {
          options.valueName = list.utils.getAttribute(target, 'data-sort');
          buttons.getInSensitive(target, options);
          options.order = buttons.getOrder(target);
        } else {
          options = arguments[1] || options;
          options.valueName = arguments[0];
          options.order = options.order || "asc";
          options.insensitive = (typeof options.insensitive == "undefined") ? true : options.insensitive;
        }

        buttons.clear();
        buttons.setOrder(options);


        // caseInsensitive
        // alphabet
        var customSortFunction = (options.sortFunction || list.sortFunction || null),
          multi = ((options.order === 'desc') ? -1 : 1),
          sortFunction;

        if (customSortFunction) {
          sortFunction = function (itemA, itemB) {
            return customSortFunction(itemA, itemB, options) * multi;
          };
        } else {
          sortFunction = function (itemA, itemB) {
            var sort = list.utils.naturalSort;
            sort.alphabet = list.alphabet || options.alphabet || undefined;
            if (!sort.alphabet && options.insensitive) {
              sort = list.utils.naturalSort.caseInsensitive;
            }
            return sort(itemA.values()[options.valueName], itemB.values()[options.valueName]) * multi;
          };
        }

        list.items.sort(sortFunction);
        list.update();
        list.trigger('sortComplete');
      };

      // Add handlers
      list.handlers.sortStart = list.handlers.sortStart || [];
      list.handlers.sortComplete = list.handlers.sortComplete || [];

      buttons.els = list.utils.getByClass(list.listContainer, list.sortClass);
      list.utils.events.bind(buttons.els, 'click', sort);
      list.on('searchStart', buttons.clear);
      list.on('filterStart', buttons.clear);

      return sort;
    };


    /***/
  }),
  /* 16 */
  /***/
  (function (module, exports) {

    var Templater = function (list) {
      var itemSource,
        templater = this;

      var init = function () {
        itemSource = templater.getItemSource(list.item);
        if (itemSource) {
          itemSource = templater.clearSourceItem(itemSource, list.valueNames);
        }
      };

      this.clearSourceItem = function (el, valueNames) {
        for (var i = 0, il = valueNames.length; i < il; i++) {
          var elm;
          if (valueNames[i].data) {
            for (var j = 0, jl = valueNames[i].data.length; j < jl; j++) {
              el.setAttribute('data-' + valueNames[i].data[j], '');
            }
          } else if (valueNames[i].attr && valueNames[i].name) {
            elm = list.utils.getByClass(el, valueNames[i].name, true);
            if (elm) {
              elm.setAttribute(valueNames[i].attr, "");
            }
          } else {
            elm = list.utils.getByClass(el, valueNames[i], true);
            if (elm) {
              elm.innerHTML = "";
            }
          }
          elm = undefined;
        }
        return el;
      };

      this.getItemSource = function (item) {
        if (item === undefined) {
          var nodes = list.list.childNodes,
            items = [];

          for (var i = 0, il = nodes.length; i < il; i++) {
            // Only textnodes have a data attribute
            if (nodes[i].data === undefined) {
              return nodes[i].cloneNode(true);
            }
          }
        } else if (/<tr[\s>]/g.exec(item)) {
          var tbody = document.createElement('tbody');
          tbody.innerHTML = item;
          return tbody.firstChild;
        } else if (item.indexOf("<") !== -1) {
          var div = document.createElement('div');
          div.innerHTML = item;
          return div.firstChild;
        } else {
          var source = document.getElementById(list.item);
          if (source) {
            return source;
          }
        }
        return undefined;
      };

      this.get = function (item, valueNames) {
        templater.create(item);
        var values = {};
        for (var i = 0, il = valueNames.length; i < il; i++) {
          var elm;
          if (valueNames[i].data) {
            for (var j = 0, jl = valueNames[i].data.length; j < jl; j++) {
              values[valueNames[i].data[j]] = list.utils.getAttribute(item.elm, 'data-' + valueNames[i].data[j]);
            }
          } else if (valueNames[i].attr && valueNames[i].name) {
            elm = list.utils.getByClass(item.elm, valueNames[i].name, true);
            values[valueNames[i].name] = elm ? list.utils.getAttribute(elm, valueNames[i].attr) : "";
          } else {
            elm = list.utils.getByClass(item.elm, valueNames[i], true);
            values[valueNames[i]] = elm ? elm.innerHTML : "";
          }
          elm = undefined;
        }
        return values;
      };

      this.set = function (item, values) {
        var getValueName = function (name) {
          for (var i = 0, il = list.valueNames.length; i < il; i++) {
            if (list.valueNames[i].data) {
              var data = list.valueNames[i].data;
              for (var j = 0, jl = data.length; j < jl; j++) {
                if (data[j] === name) {
                  return {
                    data: name
                  };
                }
              }
            } else if (list.valueNames[i].attr && list.valueNames[i].name && list.valueNames[i].name == name) {
              return list.valueNames[i];
            } else if (list.valueNames[i] === name) {
              return name;
            }
          }
        };
        var setValue = function (name, value) {
          var elm;
          var valueName = getValueName(name);
          if (!valueName)
            return;
          if (valueName.data) {
            item.elm.setAttribute('data-' + valueName.data, value);
          } else if (valueName.attr && valueName.name) {
            elm = list.utils.getByClass(item.elm, valueName.name, true);
            if (elm) {
              elm.setAttribute(valueName.attr, value);
            }
          } else {
            elm = list.utils.getByClass(item.elm, valueName, true);
            if (elm) {
              elm.innerHTML = value;
            }
          }
          elm = undefined;
        };
        if (!templater.create(item)) {
          for (var v in values) {
            if (values.hasOwnProperty(v)) {
              setValue(v, values[v]);
            }
          }
        }
      };

      this.create = function (item) {
        if (item.elm !== undefined) {
          return false;
        }
        if (itemSource === undefined) {
          throw new Error("The list need to have at list one item on init otherwise you'll have to add a template.");
        }
        /* If item source does not exists, use the first item in list as
        source for new items */
        var newItem = itemSource.cloneNode(true);
        newItem.removeAttribute('id');
        item.elm = newItem;
        templater.set(item, item.values());
        return true;
      };
      this.remove = function (item) {
        if (item.elm.parentNode === list.list) {
          list.list.removeChild(item.elm);
        }
      };
      this.show = function (item) {
        templater.create(item);
        if (list.list) {
          list.list.appendChild(item.elm);
        }
      };
      this.hide = function (item) {
        if (item.elm !== undefined && item.elm.parentNode === list.list) {
          list.list.removeChild(item.elm);
        }
      };
      this.clear = function () {
        /* .innerHTML = ''; fucks up IE */
        if (list.list && list.list.hasChildNodes()) {
          while (list.list.childNodes.length >= 1) {
            list.list.removeChild(list.list.firstChild);
          }
        }
      };

      init();
    };

    module.exports = function (list) {
      return new Templater(list);
    };


    /***/
  }),
  /* 17 */
  /***/
  (function (module, exports) {

    /**
     * A cross-browser implementation of getAttribute.
     * Source found here: http://stackoverflow.com/a/3755343/361337 written by Vivin Paliath
     *
     * Return the value for `attr` at `element`.
     *
     * @param {Element} el
     * @param {String} attr
     * @api public
     */

    module.exports = function (el, attr) {
      var result = (el.getAttribute && el.getAttribute(attr)) || null;
      if (!result) {
        var attrs = el.attributes;
        var length = attrs.length;
        for (var i = 0; i < length; i++) {
          if (attr[i] !== undefined) {
            if (attr[i].nodeName === attr) {
              result = attr[i].nodeValue;
            }
          }
        }
      }
      return result;
    };


    /***/
  }),
  /* 18 */
  /***/
  (function (module, exports, __webpack_require__) {

    "use strict";


    var alphabet;
    var alphabetIndexMap;
    var alphabetIndexMapLength = 0;

    function isNumberCode(code) {
      return code >= 48 && code <= 57;
    }

    function naturalCompare(a, b) {
      var lengthA = (a += '').length;
      var lengthB = (b += '').length;
      var aIndex = 0;
      var bIndex = 0;

      while (aIndex < lengthA && bIndex < lengthB) {
        var charCodeA = a.charCodeAt(aIndex);
        var charCodeB = b.charCodeAt(bIndex);

        if (isNumberCode(charCodeA)) {
          if (!isNumberCode(charCodeB)) {
            return charCodeA - charCodeB;
          }

          var numStartA = aIndex;
          var numStartB = bIndex;

          while (charCodeA === 48 && ++numStartA < lengthA) {
            charCodeA = a.charCodeAt(numStartA);
          }
          while (charCodeB === 48 && ++numStartB < lengthB) {
            charCodeB = b.charCodeAt(numStartB);
          }

          var numEndA = numStartA;
          var numEndB = numStartB;

          while (numEndA < lengthA && isNumberCode(a.charCodeAt(numEndA))) {
            ++numEndA;
          }
          while (numEndB < lengthB && isNumberCode(b.charCodeAt(numEndB))) {
            ++numEndB;
          }

          var difference = numEndA - numStartA - numEndB + numStartB; // numA length - numB length
          if (difference) {
            return difference;
          }

          while (numStartA < numEndA) {
            difference = a.charCodeAt(numStartA++) - b.charCodeAt(numStartB++);
            if (difference) {
              return difference;
            }
          }

          aIndex = numEndA;
          bIndex = numEndB;
          continue;
        }

        if (charCodeA !== charCodeB) {
          if (
            charCodeA < alphabetIndexMapLength &&
            charCodeB < alphabetIndexMapLength &&
            alphabetIndexMap[charCodeA] !== -1 &&
            alphabetIndexMap[charCodeB] !== -1
          ) {
            return alphabetIndexMap[charCodeA] - alphabetIndexMap[charCodeB];
          }

          return charCodeA - charCodeB;
        }

        ++aIndex;
        ++bIndex;
      }

      return lengthA - lengthB;
    }

    naturalCompare.caseInsensitive = naturalCompare.i = function (a, b) {
      return naturalCompare(('' + a).toLowerCase(), ('' + b).toLowerCase());
    };

    Object.defineProperties(naturalCompare, {
      alphabet: {
        get: function () {
          return alphabet;
        },
        set: function (value) {
          alphabet = value;
          alphabetIndexMap = [];
          var i = 0;
          if (alphabet) {
            for (; i < alphabet.length; i++) {
              alphabetIndexMap[alphabet.charCodeAt(i)] = i;
            }
          }
          alphabetIndexMapLength = alphabetIndexMap.length;
          for (i = 0; i < alphabetIndexMapLength; i++) {
            if (alphabetIndexMap[i] === undefined) {
              alphabetIndexMap[i] = -1;
            }
          }
        },
      },
    });

    module.exports = naturalCompare;


    /***/
  }),
  /* 19 */
  /***/
  (function (module, exports) {

    module.exports = function (text, pattern, options) {
      // Aproximately where in the text is the pattern expected to be found?
      var Match_Location = options.location || 0;

      //Determines how close the match must be to the fuzzy location (specified above). An exact letter match which is 'distance' characters away from the fuzzy location would score as a complete mismatch. A distance of '0' requires the match be at the exact location specified, a threshold of '1000' would require a perfect match to be within 800 characters of the fuzzy location to be found using a 0.8 threshold.
      var Match_Distance = options.distance || 100;

      // At what point does the match algorithm give up. A threshold of '0.0' requires a perfect match (of both letters and location), a threshold of '1.0' would match anything.
      var Match_Threshold = options.threshold || 0.4;

      if (pattern === text) return true; // Exact match
      if (pattern.length > 32) return false; // This algorithm cannot be used

      // Set starting location at beginning text and initialise the alphabet.
      var loc = Match_Location,
        s = (function () {
          var q = {},
            i;

          for (i = 0; i < pattern.length; i++) {
            q[pattern.charAt(i)] = 0;
          }

          for (i = 0; i < pattern.length; i++) {
            q[pattern.charAt(i)] |= 1 << (pattern.length - i - 1);
          }

          return q;
        }());

      // Compute and return the score for a match with e errors and x location.
      // Accesses loc and pattern through being a closure.

      function match_bitapScore_(e, x) {
        var accuracy = e / pattern.length,
          proximity = Math.abs(loc - x);

        if (!Match_Distance) {
          // Dodge divide by zero error.
          return proximity ? 1.0 : accuracy;
        }
        return accuracy + (proximity / Match_Distance);
      }

      var score_threshold = Match_Threshold, // Highest score beyond which we give up.
        best_loc = text.indexOf(pattern, loc); // Is there a nearby exact match? (speedup)

      if (best_loc != -1) {
        score_threshold = Math.min(match_bitapScore_(0, best_loc), score_threshold);
        // What about in the other direction? (speedup)
        best_loc = text.lastIndexOf(pattern, loc + pattern.length);

        if (best_loc != -1) {
          score_threshold = Math.min(match_bitapScore_(0, best_loc), score_threshold);
        }
      }

      // Initialise the bit arrays.
      var matchmask = 1 << (pattern.length - 1);
      best_loc = -1;

      var bin_min, bin_mid;
      var bin_max = pattern.length + text.length;
      var last_rd;
      for (var d = 0; d < pattern.length; d++) {
        // Scan for the best match; each iteration allows for one more error.
        // Run a binary search to determine how far from 'loc' we can stray at this
        // error level.
        bin_min = 0;
        bin_mid = bin_max;
        while (bin_min < bin_mid) {
          if (match_bitapScore_(d, loc + bin_mid) <= score_threshold) {
            bin_min = bin_mid;
          } else {
            bin_max = bin_mid;
          }
          bin_mid = Math.floor((bin_max - bin_min) / 2 + bin_min);
        }
        // Use the result from this iteration as the maximum for the next.
        bin_max = bin_mid;
        var start = Math.max(1, loc - bin_mid + 1);
        var finish = Math.min(loc + bin_mid, text.length) + pattern.length;

        var rd = Array(finish + 2);
        rd[finish + 1] = (1 << d) - 1;
        for (var j = finish; j >= start; j--) {
          // The alphabet (s) is a sparse hash, so the following line generates
          // warnings.
          var charMatch = s[text.charAt(j - 1)];
          if (d === 0) { // First pass: exact match.
            rd[j] = ((rd[j + 1] << 1) | 1) & charMatch;
          } else { // Subsequent passes: fuzzy match.
            rd[j] = (((rd[j + 1] << 1) | 1) & charMatch) |
              (((last_rd[j + 1] | last_rd[j]) << 1) | 1) |
              last_rd[j + 1];
          }
          if (rd[j] & matchmask) {
            var score = match_bitapScore_(d, j - 1);
            // This match will almost certainly be better than any existing match.
            // But check anyway.
            if (score <= score_threshold) {
              // Told you so.
              score_threshold = score;
              best_loc = j - 1;
              if (best_loc > loc) {
                // When passing loc, don't exceed our current distance from loc.
                start = Math.max(1, 2 * loc - best_loc);
              } else {
                // Already passed loc, downhill from here on in.
                break;
              }
            }
          }
        }
        // No hope for a (better) match at greater error levels.
        if (match_bitapScore_(d + 1, loc) > score_threshold) {
          break;
        }
        last_rd = rd;
      }

      return (best_loc < 0) ? false : true;
    };


    /***/
  })
  /******/
]);
! function (i) {
  "use strict";
  "function" == typeof define && define.amd ? define(["sources/libs/jquery"], i) : "undefined" != typeof exports ? module.exports = i(require("sources/libs/jquery")) : i(jQuery)
}(function (i) {
  "use strict";
  var e = window.Slick || {};
  (e = function () {
    var e = 0;
    return function (t, o) {
      var s, n = this;
      n.defaults = {
        accessibility: !0,
        adaptiveHeight: !1,
        appendArrows: i(t),
        appendDots: i(t),
        arrows: !0,
        asNavFor: null,
        prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
        nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
        autoplay: !1,
        autoplaySpeed: 3e3,
        centerMode: !1,
        centerPadding: "50px",
        cssEase: "ease",
        customPaging: function (e, t) {
          return i('<button type="button" />').text(t + 1)
        },
        dots: !1,
        dotsClass: "slick-dots",
        draggable: !0,
        easing: "linear",
        edgeFriction: .35,
        fade: !1,
        focusOnSelect: !1,
        focusOnChange: !1,
        infinite: !0,
        initialSlide: 0,
        lazyLoad: "ondemand",
        mobileFirst: !1,
        pauseOnHover: !0,
        pauseOnFocus: !0,
        pauseOnDotsHover: !1,
        respondTo: "window",
        responsive: null,
        rows: 1,
        rtl: !1,
        slide: "",
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: !0,
        swipeToSlide: !1,
        touchMove: !0,
        touchThreshold: 5,
        useCSS: !0,
        useTransform: !0,
        variableWidth: !1,
        vertical: !1,
        verticalSwiping: !1,
        waitForAnimate: !0,
        zIndex: 1e3
      }, n.initials = {
        animating: !1,
        dragging: !1,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        scrolling: !1,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: !1,
        slideOffset: 0,
        swipeLeft: null,
        swiping: !1,
        $list: null,
        touchObject: {},
        transformsEnabled: !1,
        unslicked: !1
      }, i.extend(n, n.initials), n.activeBreakpoint = null, n.animType = null, n.animProp = null, n.breakpoints = [], n.breakpointSettings = [], n.cssTransitions = !1, n.focussed = !1, n.interrupted = !1, n.hidden = "hidden", n.paused = !0, n.positionProp = null, n.respondTo = null, n.rowCount = 1, n.shouldClick = !0, n.$slider = i(t), n.$slidesCache = null, n.transformType = null, n.transitionType = null, n.visibilityChange = "visibilitychange", n.windowWidth = 0, n.windowTimer = null, s = i(t).data("slick") || {}, n.options = i.extend({}, n.defaults, o, s), n.currentSlide = n.options.initialSlide, n.originalSettings = n.options, void 0 !== document.mozHidden ? (n.hidden = "mozHidden", n.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (n.hidden = "webkitHidden", n.visibilityChange = "webkitvisibilitychange"), n.autoPlay = i.proxy(n.autoPlay, n), n.autoPlayClear = i.proxy(n.autoPlayClear, n), n.autoPlayIterator = i.proxy(n.autoPlayIterator, n), n.changeSlide = i.proxy(n.changeSlide, n), n.clickHandler = i.proxy(n.clickHandler, n), n.selectHandler = i.proxy(n.selectHandler, n), n.setPosition = i.proxy(n.setPosition, n), n.swipeHandler = i.proxy(n.swipeHandler, n), n.dragHandler = i.proxy(n.dragHandler, n), n.keyHandler = i.proxy(n.keyHandler, n), n.instanceUid = e++, n.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, n.registerBreakpoints(), n.init(!0)
    }
  }()).prototype.activateADA = function () {
    this.$slideTrack.find(".slick-active").attr({
      "aria-hidden": "false"
    }).find("a, input, button, select").attr({
      tabindex: "0"
    })
  }, e.prototype.addSlide = e.prototype.slickAdd = function (e, t, o) {
    var s = this;
    if ("boolean" == typeof t) o = t, t = null;
    else if (t < 0 || t >= s.slideCount) return !1;
    s.unload(), "number" == typeof t ? 0 === t && 0 === s.$slides.length ? i(e).appendTo(s.$slideTrack) : o ? i(e).insertBefore(s.$slides.eq(t)) : i(e).insertAfter(s.$slides.eq(t)) : !0 === o ? i(e).prependTo(s.$slideTrack) : i(e).appendTo(s.$slideTrack), s.$slides = s.$slideTrack.children(this.options.slide), s.$slideTrack.children(this.options.slide).detach(), s.$slideTrack.append(s.$slides), s.$slides.each(function (e, t) {
      i(t).attr("data-slick-index", e)
    }), s.$slidesCache = s.$slides, s.reinit()
  }, e.prototype.animateHeight = function () {
    var i = this;
    if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
      var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
      i.$list.animate({
        height: e
      }, i.options.speed)
    }
  }, e.prototype.animateSlide = function (e, t) {
    var o = {},
      s = this;
    s.animateHeight(), !0 === s.options.rtl && !1 === s.options.vertical && (e = -e), !1 === s.transformsEnabled ? !1 === s.options.vertical ? s.$slideTrack.animate({
      left: e
    }, s.options.speed, s.options.easing, t) : s.$slideTrack.animate({
      top: e
    }, s.options.speed, s.options.easing, t) : !1 === s.cssTransitions ? (!0 === s.options.rtl && (s.currentLeft = -s.currentLeft), i({
      animStart: s.currentLeft
    }).animate({
      animStart: e
    }, {
      duration: s.options.speed,
      easing: s.options.easing,
      step: function (i) {
        i = Math.ceil(i), !1 === s.options.vertical ? (o[s.animType] = "translate(" + i + "px, 0px)", s.$slideTrack.css(o)) : (o[s.animType] = "translate(0px," + i + "px)", s.$slideTrack.css(o))
      },
      complete: function () {
        t && t.call()
      }
    })) : (s.applyTransition(), e = Math.ceil(e), !1 === s.options.vertical ? o[s.animType] = "translate3d(" + e + "px, 0px, 0px)" : o[s.animType] = "translate3d(0px," + e + "px, 0px)", s.$slideTrack.css(o), t && setTimeout(function () {
      s.disableTransition(), t.call()
    }, s.options.speed))
  }, e.prototype.getNavTarget = function () {
    var e = this,
      t = e.options.asNavFor;
    return t && null !== t && (t = i(t).not(e.$slider)), t
  }, e.prototype.asNavFor = function (e) {
    var t = this.getNavTarget();
    null !== t && "object" == typeof t && t.each(function () {
      var t = i(this).slick("getSlick");
      t.unslicked || t.slideHandler(e, !0)
    })
  }, e.prototype.applyTransition = function (i) {
    var e = this,
      t = {};
    !1 === e.options.fade ? t[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : t[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t)
  }, e.prototype.autoPlay = function () {
    var i = this;
    i.autoPlayClear(), i.slideCount > i.options.slidesToShow && (i.autoPlayTimer = setInterval(i.autoPlayIterator, i.options.autoplaySpeed))
  }, e.prototype.autoPlayClear = function () {
    var i = this;
    i.autoPlayTimer && clearInterval(i.autoPlayTimer)
  }, e.prototype.autoPlayIterator = function () {
    var i = this,
      e = i.currentSlide + i.options.slidesToScroll;
    i.paused || i.interrupted || i.focussed || (!1 === i.options.infinite && (1 === i.direction && i.currentSlide + 1 === i.slideCount - 1 ? i.direction = 0 : 0 === i.direction && (e = i.currentSlide - i.options.slidesToScroll, i.currentSlide - 1 == 0 && (i.direction = 1))), i.slideHandler(e))
  }, e.prototype.buildArrows = function () {
    var e = this;
    !0 === e.options.arrows && (e.$prevArrow = i(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = i(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
      "aria-disabled": "true",
      tabindex: "-1"
    }))
  }, e.prototype.buildDots = function () {
    var e, t, o = this;
    if (!0 === o.options.dots) {
      for (o.$slider.addClass("slick-dotted"), t = i("<ul />").addClass(o.options.dotsClass), e = 0; e <= o.getDotCount(); e += 1) t.append(i("<li />").append(o.options.customPaging.call(this, o, e)));
      o.$dots = t.appendTo(o.options.appendDots), o.$dots.find("li").first().addClass("slick-active")
    }
  }, e.prototype.buildOut = function () {
    var e = this;
    e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
      i(t).attr("data-slick-index", e).data("originalStyling", i(t).attr("style") || "")
    }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? i('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), i("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable")
  }, e.prototype.buildRows = function () {
    var i, e, t, o, s, n, r, l = this;
    if (o = document.createDocumentFragment(), n = l.$slider.children(), l.options.rows > 1) {
      for (r = l.options.slidesPerRow * l.options.rows, s = Math.ceil(n.length / r), i = 0; i < s; i++) {
        var d = document.createElement("div");
        for (e = 0; e < l.options.rows; e++) {
          var a = document.createElement("div");
          for (t = 0; t < l.options.slidesPerRow; t++) {
            var c = i * r + (e * l.options.slidesPerRow + t);
            n.get(c) && a.appendChild(n.get(c))
          }
          d.appendChild(a)
        }
        o.appendChild(d)
      }
      l.$slider.empty().append(o), l.$slider.children().children().children().css({
        width: 100 / l.options.slidesPerRow + "%",
        display: "inline-block"
      })
    }
  }, e.prototype.checkResponsive = function (e, t) {
    var o, s, n, r = this,
      l = !1,
      d = r.$slider.width(),
      a = window.innerWidth || i(window).width();
    if ("window" === r.respondTo ? n = a : "slider" === r.respondTo ? n = d : "min" === r.respondTo && (n = Math.min(a, d)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
      s = null;
      for (o in r.breakpoints) r.breakpoints.hasOwnProperty(o) && (!1 === r.originalSettings.mobileFirst ? n < r.breakpoints[o] && (s = r.breakpoints[o]) : n > r.breakpoints[o] && (s = r.breakpoints[o]));
      null !== s ? null !== r.activeBreakpoint ? (s !== r.activeBreakpoint || t) && (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), l = s), e || !1 === l || r.$slider.trigger("breakpoint", [r, l])
    }
  }, e.prototype.changeSlide = function (e, t) {
    var o, s, n, r = this,
      l = i(e.currentTarget);
    switch (l.is("a") && e.preventDefault(), l.is("li") || (l = l.closest("li")), n = r.slideCount % r.options.slidesToScroll != 0, o = n ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll, e.data.message) {
      case "previous":
        s = 0 === o ? r.options.slidesToScroll : r.options.slidesToShow - o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - s, !1, t);
        break;
      case "next":
        s = 0 === o ? r.options.slidesToScroll : o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + s, !1, t);
        break;
      case "index":
        var d = 0 === e.data.index ? 0 : e.data.index || l.index() * r.options.slidesToScroll;
        r.slideHandler(r.checkNavigable(d), !1, t), l.children().trigger("focus");
        break;
      default:
        return
    }
  }, e.prototype.checkNavigable = function (i) {
    var e, t;
    if (e = this.getNavigableIndexes(), t = 0, i > e[e.length - 1]) i = e[e.length - 1];
    else
      for (var o in e) {
        if (i < e[o]) {
          i = t;
          break
        }
        t = e[o]
      }
    return i
  }, e.prototype.cleanUpEvents = function () {
    var e = this;
    e.options.dots && null !== e.$dots && (i("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", i.proxy(e.interrupt, e, !0)).off("mouseleave.slick", i.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), i(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().off("click.slick", e.selectHandler), i(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), i(window).off("resize.slick.slick-" + e.instanceUid, e.resize), i("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), i(window).off("load.slick.slick-" + e.instanceUid, e.setPosition)
  }, e.prototype.cleanUpSlideEvents = function () {
    var e = this;
    e.$list.off("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", i.proxy(e.interrupt, e, !1))
  }, e.prototype.cleanUpRows = function () {
    var i, e = this;
    e.options.rows > 1 && ((i = e.$slides.children().children()).removeAttr("style"), e.$slider.empty().append(i))
  }, e.prototype.clickHandler = function (i) {
    !1 === this.shouldClick && (i.stopImmediatePropagation(), i.stopPropagation(), i.preventDefault())
  }, e.prototype.destroy = function (e) {
    var t = this;
    t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), i(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
      i(this).attr("style", i(this).data("originalStyling"))
    }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t])
  }, e.prototype.disableTransition = function (i) {
    var e = this,
      t = {};
    t[e.transitionType] = "", !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t)
  }, e.prototype.fadeSlide = function (i, e) {
    var t = this;
    !1 === t.cssTransitions ? (t.$slides.eq(i).css({
      zIndex: t.options.zIndex
    }), t.$slides.eq(i).animate({
      opacity: 1
    }, t.options.speed, t.options.easing, e)) : (t.applyTransition(i), t.$slides.eq(i).css({
      opacity: 1,
      zIndex: t.options.zIndex
    }), e && setTimeout(function () {
      t.disableTransition(i), e.call()
    }, t.options.speed))
  }, e.prototype.fadeSlideOut = function (i) {
    var e = this;
    !1 === e.cssTransitions ? e.$slides.eq(i).animate({
      opacity: 0,
      zIndex: e.options.zIndex - 2
    }, e.options.speed, e.options.easing) : (e.applyTransition(i), e.$slides.eq(i).css({
      opacity: 0,
      zIndex: e.options.zIndex - 2
    }))
  }, e.prototype.filterSlides = e.prototype.slickFilter = function (i) {
    var e = this;
    null !== i && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(i).appendTo(e.$slideTrack), e.reinit())
  }, e.prototype.focusHandler = function () {
    var e = this;
    e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (t) {
      t.stopImmediatePropagation();
      var o = i(this);
      setTimeout(function () {
        e.options.pauseOnFocus && (e.focussed = o.is(":focus"), e.autoPlay())
      }, 0)
    })
  }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function () {
    return this.currentSlide
  }, e.prototype.getDotCount = function () {
    var i = this,
      e = 0,
      t = 0,
      o = 0;
    if (!0 === i.options.infinite)
      if (i.slideCount <= i.options.slidesToShow) ++o;
      else
        for (; e < i.slideCount;) ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
    else if (!0 === i.options.centerMode) o = i.slideCount;
    else if (i.options.asNavFor)
      for (; e < i.slideCount;) ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
    else o = 1 + Math.ceil((i.slideCount - i.options.slidesToShow) / i.options.slidesToScroll);
    return o - 1
  }, e.prototype.getLeft = function (i) {
    var e, t, o, s, n = this,
      r = 0;
    return n.slideOffset = 0, t = n.$slides.first().outerHeight(!0), !0 === n.options.infinite ? (n.slideCount > n.options.slidesToShow && (n.slideOffset = n.slideWidth * n.options.slidesToShow * -1, s = -1, !0 === n.options.vertical && !0 === n.options.centerMode && (2 === n.options.slidesToShow ? s = -1.5 : 1 === n.options.slidesToShow && (s = -2)), r = t * n.options.slidesToShow * s), n.slideCount % n.options.slidesToScroll != 0 && i + n.options.slidesToScroll > n.slideCount && n.slideCount > n.options.slidesToShow && (i > n.slideCount ? (n.slideOffset = (n.options.slidesToShow - (i - n.slideCount)) * n.slideWidth * -1, r = (n.options.slidesToShow - (i - n.slideCount)) * t * -1) : (n.slideOffset = n.slideCount % n.options.slidesToScroll * n.slideWidth * -1, r = n.slideCount % n.options.slidesToScroll * t * -1))) : i + n.options.slidesToShow > n.slideCount && (n.slideOffset = (i + n.options.slidesToShow - n.slideCount) * n.slideWidth, r = (i + n.options.slidesToShow - n.slideCount) * t), n.slideCount <= n.options.slidesToShow && (n.slideOffset = 0, r = 0), !0 === n.options.centerMode && n.slideCount <= n.options.slidesToShow ? n.slideOffset = n.slideWidth * Math.floor(n.options.slidesToShow) / 2 - n.slideWidth * n.slideCount / 2 : !0 === n.options.centerMode && !0 === n.options.infinite ? n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2) - n.slideWidth : !0 === n.options.centerMode && (n.slideOffset = 0, n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2)), e = !1 === n.options.vertical ? i * n.slideWidth * -1 + n.slideOffset : i * t * -1 + r, !0 === n.options.variableWidth && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, !0 === n.options.centerMode && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow + 1), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, e += (n.$list.width() - o.outerWidth()) / 2)), e
  }, e.prototype.getOption = e.prototype.slickGetOption = function (i) {
    return this.options[i]
  }, e.prototype.getNavigableIndexes = function () {
    var i, e = this,
      t = 0,
      o = 0,
      s = [];
    for (!1 === e.options.infinite ? i = e.slideCount : (t = -1 * e.options.slidesToScroll, o = -1 * e.options.slidesToScroll, i = 2 * e.slideCount); t < i;) s.push(t), t = o + e.options.slidesToScroll, o += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
    return s
  }, e.prototype.getSlick = function () {
    return this
  }, e.prototype.getSlideCount = function () {
    var e, t, o = this;
    return t = !0 === o.options.centerMode ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, !0 === o.options.swipeToSlide ? (o.$slideTrack.find(".slick-slide").each(function (s, n) {
      if (n.offsetLeft - t + i(n).outerWidth() / 2 > -1 * o.swipeLeft) return e = n, !1
    }), Math.abs(i(e).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll
  }, e.prototype.goTo = e.prototype.slickGoTo = function (i, e) {
    this.changeSlide({
      data: {
        message: "index",
        index: parseInt(i)
      }
    }, e)
  }, e.prototype.init = function (e) {
    var t = this;
    i(t.$slider).hasClass("slick-initialized") || (i(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay())
  }, e.prototype.initADA = function () {
    var e = this,
      t = Math.ceil(e.slideCount / e.options.slidesToShow),
      o = e.getNavigableIndexes().filter(function (i) {
        return i >= 0 && i < e.slideCount
      });
    e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
      "aria-hidden": "true",
      tabindex: "-1"
    }).find("a, input, button, select").attr({
      tabindex: "-1"
    }), null !== e.$dots && (e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function (t) {
      var s = o.indexOf(t);
      i(this).attr({
        role: "tabpanel",
        id: "slick-slide" + e.instanceUid + t,
        tabindex: -1
      }), -1 !== s && i(this).attr({
        "aria-describedby": "slick-slide-control" + e.instanceUid + s
      })
    }), e.$dots.attr("role", "tablist").find("li").each(function (s) {
      var n = o[s];
      i(this).attr({
        role: "presentation"
      }), i(this).find("button").first().attr({
        role: "tab",
        id: "slick-slide-control" + e.instanceUid + s,
        "aria-controls": "slick-slide" + e.instanceUid + n,
        "aria-label": s + 1 + " of " + t,
        "aria-selected": null,
        tabindex: "-1"
      })
    }).eq(e.currentSlide).find("button").attr({
      "aria-selected": "true",
      tabindex: "0"
    }).end());
    for (var s = e.currentSlide, n = s + e.options.slidesToShow; s < n; s++) e.$slides.eq(s).attr("tabindex", 0);
    e.activateADA()
  }, e.prototype.initArrowEvents = function () {
    var i = this;
    !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.off("click.slick").on("click.slick", {
      message: "previous"
    }, i.changeSlide), i.$nextArrow.off("click.slick").on("click.slick", {
      message: "next"
    }, i.changeSlide), !0 === i.options.accessibility && (i.$prevArrow.on("keydown.slick", i.keyHandler), i.$nextArrow.on("keydown.slick", i.keyHandler)))
  }, e.prototype.initDotEvents = function () {
    var e = this;
    !0 === e.options.dots && (i("li", e.$dots).on("click.slick", {
      message: "index"
    }, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && i("li", e.$dots).on("mouseenter.slick", i.proxy(e.interrupt, e, !0)).on("mouseleave.slick", i.proxy(e.interrupt, e, !1))
  }, e.prototype.initSlideEvents = function () {
    var e = this;
    e.options.pauseOnHover && (e.$list.on("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", i.proxy(e.interrupt, e, !1)))
  }, e.prototype.initializeEvents = function () {
    var e = this;
    e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
      action: "start"
    }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
      action: "move"
    }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
      action: "end"
    }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
      action: "end"
    }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), i(document).on(e.visibilityChange, i.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), i(window).on("orientationchange.slick.slick-" + e.instanceUid, i.proxy(e.orientationChange, e)), i(window).on("resize.slick.slick-" + e.instanceUid, i.proxy(e.resize, e)), i("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), i(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), i(e.setPosition)
  }, e.prototype.initUI = function () {
    var i = this;
    !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.show(), i.$nextArrow.show()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.show()
  }, e.prototype.keyHandler = function (i) {
    var e = this;
    i.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === i.keyCode && !0 === e.options.accessibility ? e.changeSlide({
      data: {
        message: !0 === e.options.rtl ? "next" : "previous"
      }
    }) : 39 === i.keyCode && !0 === e.options.accessibility && e.changeSlide({
      data: {
        message: !0 === e.options.rtl ? "previous" : "next"
      }
    }))
  }, e.prototype.lazyLoad = function () {
    function e(e) {
      i("img[data-lazy]", e).each(function () {
        var e = i(this),
          t = i(this).attr("data-lazy"),
          o = i(this).attr("data-srcset"),
          s = i(this).attr("data-sizes") || n.$slider.attr("data-sizes"),
          r = document.createElement("img");
        r.onload = function () {
          e.animate({
            opacity: 0
          }, 100, function () {
            o && (e.attr("srcset", o), s && e.attr("sizes", s)), e.attr("src", t).animate({
              opacity: 1
            }, 200, function () {
              e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
            }), n.$slider.trigger("lazyLoaded", [n, e, t])
          })
        }, r.onerror = function () {
          e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), n.$slider.trigger("lazyLoadError", [n, e, t])
        }, r.src = t
      })
    }
    var t, o, s, n = this;
    if (!0 === n.options.centerMode ? !0 === n.options.infinite ? s = (o = n.currentSlide + (n.options.slidesToShow / 2 + 1)) + n.options.slidesToShow + 2 : (o = Math.max(0, n.currentSlide - (n.options.slidesToShow / 2 + 1)), s = n.options.slidesToShow / 2 + 1 + 2 + n.currentSlide) : (o = n.options.infinite ? n.options.slidesToShow + n.currentSlide : n.currentSlide, s = Math.ceil(o + n.options.slidesToShow), !0 === n.options.fade && (o > 0 && o--, s <= n.slideCount && s++)), t = n.$slider.find(".slick-slide").slice(o, s), "anticipated" === n.options.lazyLoad)
      for (var r = o - 1, l = s, d = n.$slider.find(".slick-slide"), a = 0; a < n.options.slidesToScroll; a++) r < 0 && (r = n.slideCount - 1), t = (t = t.add(d.eq(r))).add(d.eq(l)), r--, l++;
    e(t), n.slideCount <= n.options.slidesToShow ? e(n.$slider.find(".slick-slide")) : n.currentSlide >= n.slideCount - n.options.slidesToShow ? e(n.$slider.find(".slick-cloned").slice(0, n.options.slidesToShow)) : 0 === n.currentSlide && e(n.$slider.find(".slick-cloned").slice(-1 * n.options.slidesToShow))
  }, e.prototype.loadSlider = function () {
    var i = this;
    i.setPosition(), i.$slideTrack.css({
      opacity: 1
    }), i.$slider.removeClass("slick-loading"), i.initUI(), "progressive" === i.options.lazyLoad && i.progressiveLazyLoad()
  }, e.prototype.next = e.prototype.slickNext = function () {
    this.changeSlide({
      data: {
        message: "next"
      }
    })
  }, e.prototype.orientationChange = function () {
    var i = this;
    i.checkResponsive(), i.setPosition()
  }, e.prototype.pause = e.prototype.slickPause = function () {
    var i = this;
    i.autoPlayClear(), i.paused = !0
  }, e.prototype.play = e.prototype.slickPlay = function () {
    var i = this;
    i.autoPlay(), i.options.autoplay = !0, i.paused = !1, i.focussed = !1, i.interrupted = !1
  }, e.prototype.postSlide = function (e) {
    var t = this;
    t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && (t.initADA(), t.options.focusOnChange && i(t.$slides.get(t.currentSlide)).attr("tabindex", 0).focus()))
  }, e.prototype.prev = e.prototype.slickPrev = function () {
    this.changeSlide({
      data: {
        message: "previous"
      }
    })
  }, e.prototype.preventDefault = function (i) {
    i.preventDefault()
  }, e.prototype.progressiveLazyLoad = function (e) {
    e = e || 1;
    var t, o, s, n, r, l = this,
      d = i("img[data-lazy]", l.$slider);
    d.length ? (t = d.first(), o = t.attr("data-lazy"), s = t.attr("data-srcset"), n = t.attr("data-sizes") || l.$slider.attr("data-sizes"), (r = document.createElement("img")).onload = function () {
      s && (t.attr("srcset", s), n && t.attr("sizes", n)), t.attr("src", o).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === l.options.adaptiveHeight && l.setPosition(), l.$slider.trigger("lazyLoaded", [l, t, o]), l.progressiveLazyLoad()
    }, r.onerror = function () {
      e < 3 ? setTimeout(function () {
        l.progressiveLazyLoad(e + 1)
      }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), l.$slider.trigger("lazyLoadError", [l, t, o]), l.progressiveLazyLoad())
    }, r.src = o) : l.$slider.trigger("allImagesLoaded", [l])
  }, e.prototype.refresh = function (e) {
    var t, o, s = this;
    o = s.slideCount - s.options.slidesToShow, !s.options.infinite && s.currentSlide > o && (s.currentSlide = o), s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0), t = s.currentSlide, s.destroy(!0), i.extend(s, s.initials, {
      currentSlide: t
    }), s.init(), e || s.changeSlide({
      data: {
        message: "index",
        index: t
      }
    }, !1)
  }, e.prototype.registerBreakpoints = function () {
    var e, t, o, s = this,
      n = s.options.responsive || null;
    if ("array" === i.type(n) && n.length) {
      s.respondTo = s.options.respondTo || "window";
      for (e in n)
        if (o = s.breakpoints.length - 1, n.hasOwnProperty(e)) {
          for (t = n[e].breakpoint; o >= 0;) s.breakpoints[o] && s.breakpoints[o] === t && s.breakpoints.splice(o, 1), o--;
          s.breakpoints.push(t), s.breakpointSettings[t] = n[e].settings
        } s.breakpoints.sort(function (i, e) {
        return s.options.mobileFirst ? i - e : e - i
      })
    }
  }, e.prototype.reinit = function () {
    var e = this;
    e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
  }, e.prototype.resize = function () {
    var e = this;
    i(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
      e.windowWidth = i(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
    }, 50))
  }, e.prototype.removeSlide = e.prototype.slickRemove = function (i, e, t) {
    var o = this;
    if (i = "boolean" == typeof i ? !0 === (e = i) ? 0 : o.slideCount - 1 : !0 === e ? --i : i, o.slideCount < 1 || i < 0 || i > o.slideCount - 1) return !1;
    o.unload(), !0 === t ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(i).remove(), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, o.reinit()
  }, e.prototype.setCSS = function (i) {
    var e, t, o = this,
      s = {};
    !0 === o.options.rtl && (i = -i), e = "left" == o.positionProp ? Math.ceil(i) + "px" : "0px", t = "top" == o.positionProp ? Math.ceil(i) + "px" : "0px", s[o.positionProp] = i, !1 === o.transformsEnabled ? o.$slideTrack.css(s) : (s = {}, !1 === o.cssTransitions ? (s[o.animType] = "translate(" + e + ", " + t + ")", o.$slideTrack.css(s)) : (s[o.animType] = "translate3d(" + e + ", " + t + ", 0px)", o.$slideTrack.css(s)))
  }, e.prototype.setDimensions = function () {
    var i = this;
    !1 === i.options.vertical ? !0 === i.options.centerMode && i.$list.css({
      padding: "0px " + i.options.centerPadding
    }) : (i.$list.height(i.$slides.first().outerHeight(!0) * i.options.slidesToShow), !0 === i.options.centerMode && i.$list.css({
      padding: i.options.centerPadding + " 0px"
    })), i.listWidth = i.$list.width(), i.listHeight = i.$list.height(), !1 === i.options.vertical && !1 === i.options.variableWidth ? (i.slideWidth = Math.ceil(i.listWidth / i.options.slidesToShow), i.$slideTrack.width(Math.ceil(i.slideWidth * i.$slideTrack.children(".slick-slide").length))) : !0 === i.options.variableWidth ? i.$slideTrack.width(5e3 * i.slideCount) : (i.slideWidth = Math.ceil(i.listWidth), i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0) * i.$slideTrack.children(".slick-slide").length)));
    var e = i.$slides.first().outerWidth(!0) - i.$slides.first().width();
    !1 === i.options.variableWidth && i.$slideTrack.children(".slick-slide").width(i.slideWidth - e)
  }, e.prototype.setFade = function () {
    var e, t = this;
    t.$slides.each(function (o, s) {
      e = t.slideWidth * o * -1, !0 === t.options.rtl ? i(s).css({
        position: "relative",
        right: e,
        top: 0,
        zIndex: t.options.zIndex - 2,
        opacity: 0
      }) : i(s).css({
        position: "relative",
        left: e,
        top: 0,
        zIndex: t.options.zIndex - 2,
        opacity: 0
      })
    }), t.$slides.eq(t.currentSlide).css({
      zIndex: t.options.zIndex - 1,
      opacity: 1
    })
  }, e.prototype.setHeight = function () {
    var i = this;
    if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
      var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
      i.$list.css("height", e)
    }
  }, e.prototype.setOption = e.prototype.slickSetOption = function () {
    var e, t, o, s, n, r = this,
      l = !1;
    if ("object" === i.type(arguments[0]) ? (o = arguments[0], l = arguments[1], n = "multiple") : "string" === i.type(arguments[0]) && (o = arguments[0], s = arguments[1], l = arguments[2], "responsive" === arguments[0] && "array" === i.type(arguments[1]) ? n = "responsive" : void 0 !== arguments[1] && (n = "single")), "single" === n) r.options[o] = s;
    else if ("multiple" === n) i.each(o, function (i, e) {
      r.options[i] = e
    });
    else if ("responsive" === n)
      for (t in s)
        if ("array" !== i.type(r.options.responsive)) r.options.responsive = [s[t]];
        else {
          for (e = r.options.responsive.length - 1; e >= 0;) r.options.responsive[e].breakpoint === s[t].breakpoint && r.options.responsive.splice(e, 1), e--;
          r.options.responsive.push(s[t])
        } l && (r.unload(), r.reinit())
  }, e.prototype.setPosition = function () {
    var i = this;
    i.setDimensions(), i.setHeight(), !1 === i.options.fade ? i.setCSS(i.getLeft(i.currentSlide)) : i.setFade(), i.$slider.trigger("setPosition", [i])
  }, e.prototype.setProps = function () {
    var i = this,
      e = document.body.style;
    i.positionProp = !0 === i.options.vertical ? "top" : "left", "top" === i.positionProp ? i.$slider.addClass("slick-vertical") : i.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === i.options.useCSS && (i.cssTransitions = !0), i.options.fade && ("number" == typeof i.options.zIndex ? i.options.zIndex < 3 && (i.options.zIndex = 3) : i.options.zIndex = i.defaults.zIndex), void 0 !== e.OTransform && (i.animType = "OTransform", i.transformType = "-o-transform", i.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.MozTransform && (i.animType = "MozTransform", i.transformType = "-moz-transform", i.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (i.animType = !1)), void 0 !== e.webkitTransform && (i.animType = "webkitTransform", i.transformType = "-webkit-transform", i.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.msTransform && (i.animType = "msTransform", i.transformType = "-ms-transform", i.transitionType = "msTransition", void 0 === e.msTransform && (i.animType = !1)), void 0 !== e.transform && !1 !== i.animType && (i.animType = "transform", i.transformType = "transform", i.transitionType = "transition"), i.transformsEnabled = i.options.useTransform && null !== i.animType && !1 !== i.animType
  }, e.prototype.setSlideClasses = function (i) {
    var e, t, o, s, n = this;
    if (t = n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), n.$slides.eq(i).addClass("slick-current"), !0 === n.options.centerMode) {
      var r = n.options.slidesToShow % 2 == 0 ? 1 : 0;
      e = Math.floor(n.options.slidesToShow / 2), !0 === n.options.infinite && (i >= e && i <= n.slideCount - 1 - e ? n.$slides.slice(i - e + r, i + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (o = n.options.slidesToShow + i, t.slice(o - e + 1 + r, o + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === i ? t.eq(t.length - 1 - n.options.slidesToShow).addClass("slick-center") : i === n.slideCount - 1 && t.eq(n.options.slidesToShow).addClass("slick-center")), n.$slides.eq(i).addClass("slick-center")
    } else i >= 0 && i <= n.slideCount - n.options.slidesToShow ? n.$slides.slice(i, i + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : t.length <= n.options.slidesToShow ? t.addClass("slick-active").attr("aria-hidden", "false") : (s = n.slideCount % n.options.slidesToShow, o = !0 === n.options.infinite ? n.options.slidesToShow + i : i, n.options.slidesToShow == n.options.slidesToScroll && n.slideCount - i < n.options.slidesToShow ? t.slice(o - (n.options.slidesToShow - s), o + s).addClass("slick-active").attr("aria-hidden", "false") : t.slice(o, o + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
    "ondemand" !== n.options.lazyLoad && "anticipated" !== n.options.lazyLoad || n.lazyLoad()
  }, e.prototype.setupInfinite = function () {
    var e, t, o, s = this;
    if (!0 === s.options.fade && (s.options.centerMode = !1), !0 === s.options.infinite && !1 === s.options.fade && (t = null, s.slideCount > s.options.slidesToShow)) {
      for (o = !0 === s.options.centerMode ? s.options.slidesToShow + 1 : s.options.slidesToShow, e = s.slideCount; e > s.slideCount - o; e -= 1) t = e - 1, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");
      for (e = 0; e < o + s.slideCount; e += 1) t = e, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");
      s.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
        i(this).attr("id", "")
      })
    }
  }, e.prototype.interrupt = function (i) {
    var e = this;
    i || e.autoPlay(), e.interrupted = i
  }, e.prototype.selectHandler = function (e) {
    var t = this,
      o = i(e.target).is(".slick-slide") ? i(e.target) : i(e.target).parents(".slick-slide"),
      s = parseInt(o.attr("data-slick-index"));
    s || (s = 0), t.slideCount <= t.options.slidesToShow ? t.slideHandler(s, !1, !0) : t.slideHandler(s)
  }, e.prototype.slideHandler = function (i, e, t) {
    var o, s, n, r, l, d = null,
      a = this;
    if (e = e || !1, !(!0 === a.animating && !0 === a.options.waitForAnimate || !0 === a.options.fade && a.currentSlide === i))
      if (!1 === e && a.asNavFor(i), o = i, d = a.getLeft(o), r = a.getLeft(a.currentSlide), a.currentLeft = null === a.swipeLeft ? r : a.swipeLeft, !1 === a.options.infinite && !1 === a.options.centerMode && (i < 0 || i > a.getDotCount() * a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
        a.postSlide(o)
      }) : a.postSlide(o));
      else if (!1 === a.options.infinite && !0 === a.options.centerMode && (i < 0 || i > a.slideCount - a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
      a.postSlide(o)
    }) : a.postSlide(o));
    else {
      if (a.options.autoplay && clearInterval(a.autoPlayTimer), s = o < 0 ? a.slideCount % a.options.slidesToScroll != 0 ? a.slideCount - a.slideCount % a.options.slidesToScroll : a.slideCount + o : o >= a.slideCount ? a.slideCount % a.options.slidesToScroll != 0 ? 0 : o - a.slideCount : o, a.animating = !0, a.$slider.trigger("beforeChange", [a, a.currentSlide, s]), n = a.currentSlide, a.currentSlide = s, a.setSlideClasses(a.currentSlide), a.options.asNavFor && (l = (l = a.getNavTarget()).slick("getSlick")).slideCount <= l.options.slidesToShow && l.setSlideClasses(a.currentSlide), a.updateDots(), a.updateArrows(), !0 === a.options.fade) return !0 !== t ? (a.fadeSlideOut(n), a.fadeSlide(s, function () {
        a.postSlide(s)
      })) : a.postSlide(s), void a.animateHeight();
      !0 !== t ? a.animateSlide(d, function () {
        a.postSlide(s)
      }) : a.postSlide(s)
    }
  }, e.prototype.startLoad = function () {
    var i = this;
    !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.hide(), i.$nextArrow.hide()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.hide(), i.$slider.addClass("slick-loading")
  }, e.prototype.swipeDirection = function () {
    var i, e, t, o, s = this;
    return i = s.touchObject.startX - s.touchObject.curX, e = s.touchObject.startY - s.touchObject.curY, t = Math.atan2(e, i), (o = Math.round(180 * t / Math.PI)) < 0 && (o = 360 - Math.abs(o)), o <= 45 && o >= 0 ? !1 === s.options.rtl ? "left" : "right" : o <= 360 && o >= 315 ? !1 === s.options.rtl ? "left" : "right" : o >= 135 && o <= 225 ? !1 === s.options.rtl ? "right" : "left" : !0 === s.options.verticalSwiping ? o >= 35 && o <= 135 ? "down" : "up" : "vertical"
  }, e.prototype.swipeEnd = function (i) {
    var e, t, o = this;
    if (o.dragging = !1, o.swiping = !1, o.scrolling) return o.scrolling = !1, !1;
    if (o.interrupted = !1, o.shouldClick = !(o.touchObject.swipeLength > 10), void 0 === o.touchObject.curX) return !1;
    if (!0 === o.touchObject.edgeHit && o.$slider.trigger("edge", [o, o.swipeDirection()]), o.touchObject.swipeLength >= o.touchObject.minSwipe) {
      switch (t = o.swipeDirection()) {
        case "left":
        case "down":
          e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide + o.getSlideCount()) : o.currentSlide + o.getSlideCount(), o.currentDirection = 0;
          break;
        case "right":
        case "up":
          e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide - o.getSlideCount()) : o.currentSlide - o.getSlideCount(), o.currentDirection = 1
      }
      "vertical" != t && (o.slideHandler(e), o.touchObject = {}, o.$slider.trigger("swipe", [o, t]))
    } else o.touchObject.startX !== o.touchObject.curX && (o.slideHandler(o.currentSlide), o.touchObject = {})
  }, e.prototype.swipeHandler = function (i) {
    var e = this;
    if (!(!1 === e.options.swipe || "ontouchend" in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== i.type.indexOf("mouse"))) switch (e.touchObject.fingerCount = i.originalEvent && void 0 !== i.originalEvent.touches ? i.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), i.data.action) {
      case "start":
        e.swipeStart(i);
        break;
      case "move":
        e.swipeMove(i);
        break;
      case "end":
        e.swipeEnd(i)
    }
  }, e.prototype.swipeMove = function (i) {
    var e, t, o, s, n, r, l = this;
    return n = void 0 !== i.originalEvent ? i.originalEvent.touches : null, !(!l.dragging || l.scrolling || n && 1 !== n.length) && (e = l.getLeft(l.currentSlide), l.touchObject.curX = void 0 !== n ? n[0].pageX : i.clientX, l.touchObject.curY = void 0 !== n ? n[0].pageY : i.clientY, l.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(l.touchObject.curX - l.touchObject.startX, 2))), r = Math.round(Math.sqrt(Math.pow(l.touchObject.curY - l.touchObject.startY, 2))), !l.options.verticalSwiping && !l.swiping && r > 4 ? (l.scrolling = !0, !1) : (!0 === l.options.verticalSwiping && (l.touchObject.swipeLength = r), t = l.swipeDirection(), void 0 !== i.originalEvent && l.touchObject.swipeLength > 4 && (l.swiping = !0, i.preventDefault()), s = (!1 === l.options.rtl ? 1 : -1) * (l.touchObject.curX > l.touchObject.startX ? 1 : -1), !0 === l.options.verticalSwiping && (s = l.touchObject.curY > l.touchObject.startY ? 1 : -1), o = l.touchObject.swipeLength, l.touchObject.edgeHit = !1, !1 === l.options.infinite && (0 === l.currentSlide && "right" === t || l.currentSlide >= l.getDotCount() && "left" === t) && (o = l.touchObject.swipeLength * l.options.edgeFriction, l.touchObject.edgeHit = !0), !1 === l.options.vertical ? l.swipeLeft = e + o * s : l.swipeLeft = e + o * (l.$list.height() / l.listWidth) * s, !0 === l.options.verticalSwiping && (l.swipeLeft = e + o * s), !0 !== l.options.fade && !1 !== l.options.touchMove && (!0 === l.animating ? (l.swipeLeft = null, !1) : void l.setCSS(l.swipeLeft))))
  }, e.prototype.swipeStart = function (i) {
    var e, t = this;
    if (t.interrupted = !0, 1 !== t.touchObject.fingerCount || t.slideCount <= t.options.slidesToShow) return t.touchObject = {}, !1;
    void 0 !== i.originalEvent && void 0 !== i.originalEvent.touches && (e = i.originalEvent.touches[0]), t.touchObject.startX = t.touchObject.curX = void 0 !== e ? e.pageX : i.clientX, t.touchObject.startY = t.touchObject.curY = void 0 !== e ? e.pageY : i.clientY, t.dragging = !0
  }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function () {
    var i = this;
    null !== i.$slidesCache && (i.unload(), i.$slideTrack.children(this.options.slide).detach(), i.$slidesCache.appendTo(i.$slideTrack), i.reinit())
  }, e.prototype.unload = function () {
    var e = this;
    i(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
  }, e.prototype.unslick = function (i) {
    var e = this;
    e.$slider.trigger("unslick", [e, i]), e.destroy()
  }, e.prototype.updateArrows = function () {
    var i = this;
    Math.floor(i.options.slidesToShow / 2), !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && !i.options.infinite && (i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === i.currentSlide ? (i.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - i.options.slidesToShow && !1 === i.options.centerMode ? (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - 1 && !0 === i.options.centerMode && (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
  }, e.prototype.updateDots = function () {
    var i = this;
    null !== i.$dots && (i.$dots.find("li").removeClass("slick-active").end(), i.$dots.find("li").eq(Math.floor(i.currentSlide / i.options.slidesToScroll)).addClass("slick-active"))
  }, e.prototype.visibility = function () {
    var i = this;
    i.options.autoplay && (document[i.hidden] ? i.interrupted = !0 : i.interrupted = !1)
  }, i.fn.slick = function () {
    var i, t, o = this,
      s = arguments[0],
      n = Array.prototype.slice.call(arguments, 1),
      r = o.length;
    for (i = 0; i < r; i++)
      if ("object" == typeof s || void 0 === s ? o[i].slick = new e(o[i], s) : t = o[i].slick[s].apply(o[i].slick, n), void 0 !== t) return t;
    return o
  }
});

(function (n, t) {
  typeof exports == "object" && typeof module != "undefined" ? module.exports = t() : typeof define == "function" && define.amd ? define(t) : (n = n || self, n.Swiper = t())
})(this, function () {
  "use strict";

  function t(n, t) {
    var s = [],
      r = 0,
      h, c, e, o;
    if (n && !t && n instanceof f) return n;
    if (n)
      if (typeof n == "string")
        if (e = n.trim(), e.indexOf("<") >= 0 && e.indexOf(">") >= 0)
          for (o = "div", e.indexOf("<li") === 0 && (o = "ul"), e.indexOf("<tr") === 0 && (o = "tbody"), (e.indexOf("<td") === 0 || e.indexOf("<th") === 0) && (o = "tr"), e.indexOf("<tbody") === 0 && (o = "table"), e.indexOf("<option") === 0 && (o = "select"), c = u.createElement(o), c.innerHTML = e, r = 0; r < c.childNodes.length; r += 1) s.push(c.childNodes[r]);
        else
          for (h = t || n[0] !== "#" || n.match(/[ .<>:~]/) ? (t || u).querySelectorAll(n.trim()) : [u.getElementById(n.trim().split("#")[1])], r = 0; r < h.length; r += 1) h[r] && s.push(h[r]);
    else if (n.nodeType || n === i || n === u) s.push(n);
    else if (n.length > 0 && n[0].nodeType)
      for (r = 0; r < n.length; r += 1) s.push(n[r]);
    return new f(s)
  }

  function g(n) {
    for (var i = [], t = 0; t < n.length; t += 1) i.indexOf(n[t]) === -1 && i.push(n[t]);
    return i
  }

  function oi(n) {
    var r, i, t;
    if (typeof n == "undefined") return this;
    for (r = n.split(" "), i = 0; i < r.length; i += 1)
      for (t = 0; t < this.length; t += 1) typeof this[t] != "undefined" && typeof this[t].classList != "undefined" && this[t].classList.add(r[i]);
    return this
  }

  function si(n) {
    for (var t, r = n.split(" "), i = 0; i < r.length; i += 1)
      for (t = 0; t < this.length; t += 1) typeof this[t] != "undefined" && typeof this[t].classList != "undefined" && this[t].classList.remove(r[i]);
    return this
  }

  function hi(n) {
    return this[0] ? this[0].classList.contains(n) : !1
  }

  function ci(n) {
    for (var t, r = n.split(" "), i = 0; i < r.length; i += 1)
      for (t = 0; t < this.length; t += 1) typeof this[t] != "undefined" && typeof this[t].classList != "undefined" && this[t].classList.toggle(r[i]);
    return this
  }

  function li(n, t) {
    var u = arguments,
      i, r;
    if (arguments.length === 1 && typeof n == "string") return this[0] ? this[0].getAttribute(n) : undefined;
    for (i = 0; i < this.length; i += 1)
      if (u.length === 2) this[i].setAttribute(n, t);
      else
        for (r in n) this[i][r] = n[r], this[i].setAttribute(r, n[r]);
    return this
  }

  function ai(n) {
    for (var t = 0; t < this.length; t += 1) this[t].removeAttribute(n);
    return this
  }

  function vi(n, t) {
    var i, u, r;
    if (typeof t == "undefined") return (i = this[0], i) ? i.dom7ElementDataStorage && n in i.dom7ElementDataStorage ? i.dom7ElementDataStorage[n] : (u = i.getAttribute("data-" + n), u) ? u : undefined : undefined;
    for (r = 0; r < this.length; r += 1) i = this[r], i.dom7ElementDataStorage || (i.dom7ElementDataStorage = {}), i.dom7ElementDataStorage[n] = t;
    return this
  }

  function yi(n) {
    for (var i, t = 0; t < this.length; t += 1) i = this[t].style, i.webkitTransform = n, i.transform = n;
    return this
  }

  function pi(n) {
    var t, i;
    for (typeof n != "string" && (n = n + "ms"), t = 0; t < this.length; t += 1) i = this[t].style, i.webkitTransitionDuration = n, i.transitionDuration = n;
    return this
  }

  function wi() {
    function y(n) {
      var f = n.target,
        i, e, r;
      if (f)
        if (i = n.target.dom7EventData || [], i.indexOf(n) < 0 && i.unshift(n), t(f).is(c)) u.apply(f, i);
        else
          for (e = t(f).parents(), r = 0; r < e.length; r += 1) t(e[r]).is(c) && u.apply(e[r], i)
    }

    function p(n) {
      var t = n && n.target ? n.target.dom7EventData || [] : [];
      t.indexOf(n) < 0 && t.unshift(n);
      u.apply(this, t)
    }
    for (var h, r = [], a = arguments.length, e, i, l, n, o, s; a--;) r[a] = arguments[a];
    var v = r[0],
      c = r[1],
      u = r[2],
      f = r[3];
    for (typeof r[1] == "function" && (h = r, v = h[0], u = h[1], f = h[2], c = undefined), f || (f = !1), e = v.split(" "), l = 0; l < this.length; l += 1)
      if (n = this[l], c)
        for (i = 0; i < e.length; i += 1) s = e[i], n.dom7LiveListeners || (n.dom7LiveListeners = {}), n.dom7LiveListeners[s] || (n.dom7LiveListeners[s] = []), n.dom7LiveListeners[s].push({
          listener: u,
          proxyListener: y
        }), n.addEventListener(s, y, f);
      else
        for (i = 0; i < e.length; i += 1) o = e[i], n.dom7Listeners || (n.dom7Listeners = {}), n.dom7Listeners[o] || (n.dom7Listeners[o] = []), n.dom7Listeners[o].push({
          listener: u,
          proxyListener: p
        }), n.addEventListener(o, p, f);
    return this
  }

  function bi() {
    for (var s, r = [], l = arguments.length, v, h, o, c, t, n, u, i; l--;) r[l] = arguments[l];
    var y = r[0],
      a = r[1],
      f = r[2],
      e = r[3];
    for (typeof r[1] == "function" && (s = r, y = s[0], f = s[1], e = s[2], a = undefined), e || (e = !1), v = y.split(" "), h = 0; h < v.length; h += 1)
      for (o = v[h], c = 0; c < this.length; c += 1)
        if (t = this[c], n = void 0, !a && t.dom7Listeners ? n = t.dom7Listeners[o] : a && t.dom7LiveListeners && (n = t.dom7LiveListeners[o]), n && n.length)
          for (u = n.length - 1; u >= 0; u -= 1) i = n[u], f && i.listener === f ? (t.removeEventListener(o, i.proxyListener, e), n.splice(u, 1)) : f && i.listener && i.listener.dom7proxy && i.listener.dom7proxy === f ? (t.removeEventListener(o, i.proxyListener, e), n.splice(u, 1)) : f || (t.removeEventListener(o, i.proxyListener, e), n.splice(u, 1));
    return this
  }

  function ki() {
    for (var r = [], o = arguments.length, s, h, f, c, e, t, n; o--;) r[o] = arguments[o];
    for (s = r[0].split(" "), h = r[1], f = 0; f < s.length; f += 1)
      for (c = s[f], e = 0; e < this.length; e += 1) {
        t = this[e];
        n = void 0;
        try {
          n = new i.CustomEvent(c, {
            detail: h,
            bubbles: !0,
            cancelable: !0
          })
        } catch (l) {
          n = u.createEvent("Event");
          n.initEvent(c, !0, !0);
          n.detail = h
        }
        t.dom7EventData = r.filter(function (n, t) {
          return t > 0
        });
        t.dispatchEvent(n);
        t.dom7EventData = [];
        delete t.dom7EventData
      }
    return this
  }

  function di(n) {
    function u(f) {
      if (f.target === this)
        for (n.call(this, f), t = 0; t < i.length; t += 1) r.off(i[t], u)
    }
    var i = ["webkitTransitionEnd", "transitionend"],
      r = this,
      t;
    if (n)
      for (t = 0; t < i.length; t += 1) r.on(i[t], u);
    return this
  }

  function gi(n) {
    if (this.length > 0) {
      if (n) {
        var t = this.styles();
        return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"))
      }
      return this[0].offsetWidth
    }
    return null
  }

  function nr(n) {
    if (this.length > 0) {
      if (n) {
        var t = this.styles();
        return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"))
      }
      return this[0].offsetHeight
    }
    return null
  }

  function tr() {
    if (this.length > 0) {
      var n = this[0],
        t = n.getBoundingClientRect(),
        r = u.body,
        f = n.clientTop || r.clientTop || 0,
        e = n.clientLeft || r.clientLeft || 0,
        o = n === i ? i.scrollY : n.scrollTop,
        s = n === i ? i.scrollX : n.scrollLeft;
      return {
        top: t.top + o - f,
        left: t.left + s - e
      }
    }
    return null
  }

  function ir() {
    return this[0] ? i.getComputedStyle(this[0], null) : {}
  }

  function rr(n, t) {
    var r, u;
    if (arguments.length === 1)
      if (typeof n == "string") {
        if (this[0]) return i.getComputedStyle(this[0], null).getPropertyValue(n)
      } else {
        for (r = 0; r < this.length; r += 1)
          for (u in n) this[r].style[u] = n[u];
        return this
      } if (arguments.length === 2 && typeof n == "string") {
      for (r = 0; r < this.length; r += 1) this[r].style[n] = t;
      return this
    }
    return this
  }

  function ur(n) {
    if (!n) return this;
    for (var t = 0; t < this.length; t += 1)
      if (n.call(this[t], t, this[t]) === !1) return this;
    return this
  }

  function fr(n) {
    if (typeof n == "undefined") return this[0] ? this[0].innerHTML : undefined;
    for (var t = 0; t < this.length; t += 1) this[t].innerHTML = n;
    return this
  }

  function er(n) {
    if (typeof n == "undefined") return this[0] ? this[0].textContent.trim() : null;
    for (var t = 0; t < this.length; t += 1) this[t].textContent = n;
    return this
  }

  function or(n) {
    var r = this[0],
      o, e;
    if (!r || typeof n == "undefined") return !1;
    if (typeof n == "string") {
      if (r.matches) return r.matches(n);
      if (r.webkitMatchesSelector) return r.webkitMatchesSelector(n);
      if (r.msMatchesSelector) return r.msMatchesSelector(n);
      for (o = t(n), e = 0; e < o.length; e += 1)
        if (o[e] === r) return !0;
      return !1
    }
    if (n === u) return r === u;
    if (n === i) return r === i;
    if (n.nodeType || n instanceof f) {
      for (o = n.nodeType ? [n] : n, e = 0; e < o.length; e += 1)
        if (o[e] === r) return !0;
      return !1
    }
    return !1
  }

  function sr() {
    var n = this[0],
      t;
    if (n) {
      for (t = 0;
        (n = n.previousSibling) !== null;) n.nodeType === 1 && (t += 1);
      return t
    }
    return undefined
  }

  function hr(n) {
    if (typeof n == "undefined") return this;
    var i = this.length,
      t;
    return n > i - 1 ? new f([]) : n < 0 ? (t = i + n, t < 0) ? new f([]) : new f([this[t]]) : new f([this[n]])
  }

  function cr() {
    for (var o = [], s = arguments.length, n, i, t, r, e; s--;) o[s] = arguments[s];
    for (i = 0; i < o.length; i += 1)
      for (n = o[i], t = 0; t < this.length; t += 1)
        if (typeof n == "string")
          for (r = u.createElement("div"), r.innerHTML = n; r.firstChild;) this[t].appendChild(r.firstChild);
        else if (n instanceof f)
      for (e = 0; e < n.length; e += 1) this[t].appendChild(n[e]);
    else this[t].appendChild(n);
    return this
  }

  function lr(n) {
    for (var i, r, t = 0; t < this.length; t += 1)
      if (typeof n == "string")
        for (r = u.createElement("div"), r.innerHTML = n, i = r.childNodes.length - 1; i >= 0; i -= 1) this[t].insertBefore(r.childNodes[i], this[t].childNodes[0]);
      else if (n instanceof f)
      for (i = 0; i < n.length; i += 1) this[t].insertBefore(n[i], this[t].childNodes[0]);
    else this[t].insertBefore(n, this[t].childNodes[0]);
    return this
  }

  function ar(n) {
    return this.length > 0 ? n ? this[0].nextElementSibling && t(this[0].nextElementSibling).is(n) ? new f([this[0].nextElementSibling]) : new f([]) : this[0].nextElementSibling ? new f([this[0].nextElementSibling]) : new f([]) : new f([])
  }

  function vr(n) {
    var u = [],
      r = this[0],
      i;
    if (!r) return new f([]);
    while (r.nextElementSibling) i = r.nextElementSibling, n ? t(i).is(n) && u.push(i) : u.push(i), r = i;
    return new f(u)
  }

  function yr(n) {
    if (this.length > 0) {
      var i = this[0];
      return n ? i.previousElementSibling && t(i.previousElementSibling).is(n) ? new f([i.previousElementSibling]) : new f([]) : i.previousElementSibling ? new f([i.previousElementSibling]) : new f([])
    }
    return new f([])
  }

  function pr(n) {
    var u = [],
      r = this[0],
      i;
    if (!r) return new f([]);
    while (r.previousElementSibling) i = r.previousElementSibling, n ? t(i).is(n) && u.push(i) : u.push(i), r = i;
    return new f(u)
  }

  function wr(n) {
    for (var r = [], i = 0; i < this.length; i += 1) this[i].parentNode !== null && (n ? t(this[i].parentNode).is(n) && r.push(this[i].parentNode) : r.push(this[i].parentNode));
    return t(g(r))
  }

  function br(n) {
    for (var i, r = [], u = 0; u < this.length; u += 1)
      for (i = this[u].parentNode; i;) n ? t(i).is(n) && r.push(i) : r.push(i), i = i.parentNode;
    return t(g(r))
  }

  function kr(n) {
    var t = this;
    return typeof n == "undefined" ? new f([]) : (t.is(n) || (t = t.parents(n).eq(0)), t)
  }

  function dr(n) {
    for (var r, t, u = [], i = 0; i < this.length; i += 1)
      for (r = this[i].querySelectorAll(n), t = 0; t < r.length; t += 1) u.push(r[t]);
    return new f(u)
  }

  function gr(n) {
    for (var r, i, u = [], e = 0; e < this.length; e += 1)
      for (r = this[e].childNodes, i = 0; i < r.length; i += 1) n ? r[i].nodeType === 1 && t(r[i]).is(n) && u.push(r[i]) : r[i].nodeType === 1 && u.push(r[i]);
    return new f(g(u))
  }

  function nu() {
    for (var n = 0; n < this.length; n += 1) this[n].parentNode && this[n].parentNode.removeChild(this[n]);
    return this
  }

  function tu() {
    for (var u = [], f = arguments.length, n, i, r, e; f--;) u[f] = arguments[f];
    for (n = this, i = 0; i < u.length; i += 1)
      for (e = t(u[i]), r = 0; r < e.length; r += 1) n[n.length] = e[r], n.length += 1;
    return n
  }

  function iu() {
    var t = this,
      i, r, u = t.$el;
    (i = typeof t.params.width != "undefined" ? t.params.width : u[0].clientWidth, r = typeof t.params.height != "undefined" ? t.params.height : u[0].clientHeight, i === 0 && t.isHorizontal() || r === 0 && t.isVertical()) || (i = i - parseInt(u.css("padding-left"), 10) - parseInt(u.css("padding-right"), 10), r = r - parseInt(u.css("padding-top"), 10) - parseInt(u.css("padding-bottom"), 10), n.extend(t, {
      width: i,
      height: r,
      size: t.isHorizontal() ? i : r
    }))
  }

  function ru() {
    var u = this,
      t = u.params,
      k = u.$wrapperEl,
      p = u.size,
      ht = u.rtlTranslate,
      si = u.wrongRTL,
      kt = u.virtual && t.virtual.enabled,
      hi = kt ? u.virtual.slides.length : u.slides.length,
      l = k.children("." + u.params.slideClass),
      b = kt ? u.virtual.slides.length : l.length,
      f = [],
      d = [],
      ct = [],
      lt = t.slidesOffsetBefore,
      et, g, s, a, yt, it, rt, ot, ut, st, ft, bt;
    typeof lt == "function" && (lt = t.slidesOffsetBefore.call(u));
    et = t.slidesOffsetAfter;
    typeof et == "function" && (et = t.slidesOffsetAfter.call(u));
    var ci = u.snapGrid.length,
      li = u.snapGrid.length,
      c = t.spaceBetween,
      e = -lt,
      at = 0,
      vt = 0;
    if (typeof p != "undefined") {
      typeof c == "string" && c.indexOf("%") >= 0 && (c = parseFloat(c.replace("%", "")) / 100 * p);
      u.virtualSize = -c;
      ht ? l.css({
        marginLeft: "",
        marginTop: ""
      }) : l.css({
        marginRight: "",
        marginBottom: ""
      });
      t.slidesPerColumn > 1 && (g = Math.floor(b / t.slidesPerColumn) === b / u.params.slidesPerColumn ? b : Math.ceil(b / t.slidesPerColumn) * t.slidesPerColumn, t.slidesPerView !== "auto" && t.slidesPerColumnFill === "row" && (g = Math.max(g, t.slidesPerView * t.slidesPerColumn)));
      var o, nt = t.slidesPerColumn,
        dt = g / nt,
        gt = Math.floor(b / t.slidesPerColumn);
      for (s = 0; s < b; s += 1) {
        if (o = 0, a = l.eq(s), t.slidesPerColumn > 1) {
          var tt = void 0,
            w = void 0,
            v = void 0;
          t.slidesPerColumnFill === "column" || t.slidesPerColumnFill === "row" && t.slidesPerGroup > 1 ? (t.slidesPerColumnFill === "column" ? (w = Math.floor(s / nt), v = s - w * nt, (w > gt || w === gt && v === nt - 1) && (v += 1, v >= nt && (v = 0, w += 1))) : (yt = Math.floor(s / t.slidesPerGroup), v = Math.floor(s / t.slidesPerView) - yt * t.slidesPerColumn, w = s - v * t.slidesPerView - yt * t.slidesPerView), tt = w + v * g / nt, a.css({
            "-webkit-box-ordinal-group": tt,
            "-moz-box-ordinal-group": tt,
            "-ms-flex-order": tt,
            "-webkit-order": tt,
            order: tt
          })) : (v = Math.floor(s / dt), w = s - v * dt);
          a.css("margin-" + (u.isHorizontal() ? "top" : "left"), v !== 0 && t.spaceBetween && t.spaceBetween + "px").attr("data-swiper-column", w).attr("data-swiper-row", v)
        }
        if (a.css("display") !== "none") {
          if (t.slidesPerView === "auto") {
            var y = i.getComputedStyle(a[0], null),
              pt = a[0].style.transform,
              wt = a[0].style.webkitTransform;
            if (pt && (a[0].style.transform = "none"), wt && (a[0].style.webkitTransform = "none"), t.roundLengths) o = u.isHorizontal() ? a.outerWidth(!0) : a.outerHeight(!0);
            else if (u.isHorizontal()) {
              var ni = parseFloat(y.getPropertyValue("width")),
                ai = parseFloat(y.getPropertyValue("padding-left")),
                vi = parseFloat(y.getPropertyValue("padding-right")),
                ti = parseFloat(y.getPropertyValue("margin-left")),
                ii = parseFloat(y.getPropertyValue("margin-right")),
                ri = y.getPropertyValue("box-sizing");
              o = ri && ri === "border-box" && !h.isIE ? ni + ti + ii : ni + ai + vi + ti + ii
            } else {
              var ui = parseFloat(y.getPropertyValue("height")),
                yi = parseFloat(y.getPropertyValue("padding-top")),
                pi = parseFloat(y.getPropertyValue("padding-bottom")),
                fi = parseFloat(y.getPropertyValue("margin-top")),
                ei = parseFloat(y.getPropertyValue("margin-bottom")),
                oi = y.getPropertyValue("box-sizing");
              o = oi && oi === "border-box" && !h.isIE ? ui + fi + ei : ui + yi + pi + fi + ei
            }
            pt && (a[0].style.transform = pt);
            wt && (a[0].style.webkitTransform = wt);
            t.roundLengths && (o = Math.floor(o))
          } else o = (p - (t.slidesPerView - 1) * c) / t.slidesPerView, t.roundLengths && (o = Math.floor(o)), l[s] && (u.isHorizontal() ? l[s].style.width = o + "px" : l[s].style.height = o + "px");
          l[s] && (l[s].swiperSlideSize = o);
          ct.push(o);
          t.centeredSlides ? (e = e + o / 2 + at / 2 + c, at === 0 && s !== 0 && (e = e - p / 2 - c), s === 0 && (e = e - p / 2 - c), Math.abs(e) < 1 / 1e3 && (e = 0), t.roundLengths && (e = Math.floor(e)), vt % t.slidesPerGroup == 0 && f.push(e), d.push(e)) : (t.roundLengths && (e = Math.floor(e)), vt % t.slidesPerGroup == 0 && f.push(e), d.push(e), e = e + o + c);
          u.virtualSize += o + c;
          at = o;
          vt += 1
        }
      }
      if (u.virtualSize = Math.max(u.virtualSize, p) + et, ht && si && (t.effect === "slide" || t.effect === "coverflow") && k.css({
          width: u.virtualSize + t.spaceBetween + "px"
        }), (!r.flexbox || t.setWrapperSize) && (u.isHorizontal() ? k.css({
          width: u.virtualSize + t.spaceBetween + "px"
        }) : k.css({
          height: u.virtualSize + t.spaceBetween + "px"
        })), t.slidesPerColumn > 1 && (u.virtualSize = (o + t.spaceBetween) * g, u.virtualSize = Math.ceil(u.virtualSize / t.slidesPerColumn) - t.spaceBetween, u.isHorizontal() ? k.css({
          width: u.virtualSize + t.spaceBetween + "px"
        }) : k.css({
          height: u.virtualSize + t.spaceBetween + "px"
        }), t.centeredSlides)) {
        for (it = [], rt = 0; rt < f.length; rt += 1) ot = f[rt], t.roundLengths && (ot = Math.floor(ot)), f[rt] < u.virtualSize + f[0] && it.push(ot);
        f = it
      }
      if (!t.centeredSlides) {
        for (it = [], ut = 0; ut < f.length; ut += 1) st = f[ut], t.roundLengths && (st = Math.floor(st)), f[ut] <= u.virtualSize - p && it.push(st);
        f = it;
        Math.floor(u.virtualSize - p) - Math.floor(f[f.length - 1]) > 1 && f.push(u.virtualSize - p)
      }
      f.length === 0 && (f = [0]);
      t.spaceBetween !== 0 && (u.isHorizontal() ? ht ? l.css({
        marginLeft: c + "px"
      }) : l.css({
        marginRight: c + "px"
      }) : l.css({
        marginBottom: c + "px"
      }));
      t.centerInsufficientSlides && (ft = 0, ct.forEach(function (n) {
        ft += n + (t.spaceBetween ? t.spaceBetween : 0)
      }), ft -= t.spaceBetween, ft < p && (bt = (p - ft) / 2, f.forEach(function (n, t) {
        f[t] = n - bt
      }), d.forEach(function (n, t) {
        d[t] = n + bt
      })));
      n.extend(u, {
        slides: l,
        snapGrid: f,
        slidesGrid: d,
        slidesSizesGrid: ct
      });
      b !== hi && u.emit("slidesLengthChange");
      f.length !== ci && (u.params.watchOverflow && u.checkOverflow(), u.emit("snapGridLengthChange"));
      d.length !== li && u.emit("slidesGridLengthChange");
      (t.watchSlidesProgress || t.watchSlidesVisibility) && u.updateSlidesOffset()
    }
  }

  function uu(n) {
    var t = this,
      r = [],
      u = 0,
      i, f, e;
    if (typeof n == "number" ? t.setTransition(n) : n === !0 && t.setTransition(t.params.speed), t.params.slidesPerView !== "auto" && t.params.slidesPerView > 1)
      for (i = 0; i < Math.ceil(t.params.slidesPerView); i += 1) {
        if (f = t.activeIndex + i, f > t.slides.length) break;
        r.push(t.slides.eq(f)[0])
      } else r.push(t.slides.eq(t.activeIndex)[0]);
    for (i = 0; i < r.length; i += 1) typeof r[i] != "undefined" && (e = r[i].offsetHeight, u = e > u ? e : u);
    u && t.$wrapperEl.css("height", u + "px")
  }

  function fu() {
    for (var i = this, t = i.slides, n = 0; n < t.length; n += 1) t[n].swiperSlideOffset = i.isHorizontal() ? t[n].offsetLeft : t[n].offsetTop
  }

  function eu(n) {
    var i = this,
      e = i.params,
      r, h, o, u, f, c;
    if (typeof n == "undefined" && (n = i && i.translate || 0), r = i.slides, h = i.rtlTranslate, r.length !== 0) {
      for (typeof r[0].swiperSlideOffset == "undefined" && i.updateSlidesOffset(), o = -n, h && (o = n), r.removeClass(e.slideVisibleClass), i.visibleSlidesIndexes = [], i.visibleSlides = [], u = 0; u < r.length; u += 1) {
        if (f = r[u], c = (o + (e.centeredSlides ? i.minTranslate() : 0) - f.swiperSlideOffset) / (f.swiperSlideSize + e.spaceBetween), e.watchSlidesVisibility) {
          var s = -(o - f.swiperSlideOffset),
            l = s + i.slidesSizesGrid[u],
            a = s >= 0 && s < i.size - 1 || l > 1 && l <= i.size || s <= 0 && l >= i.size;
          a && (i.visibleSlides.push(f), i.visibleSlidesIndexes.push(u), r.eq(u).addClass(e.slideVisibleClass))
        }
        f.progress = h ? -c : c
      }
      i.visibleSlides = t(i.visibleSlides)
    }
  }

  function ou(t) {
    var i = this,
      e = i.params,
      o;
    typeof t == "undefined" && (o = i.rtlTranslate ? -1 : 1, t = i && i.translate && i.translate * o || 0);
    var s = i.maxTranslate() - i.minTranslate(),
      r = i.progress,
      u = i.isBeginning,
      f = i.isEnd,
      h = u,
      c = f;
    s === 0 ? (r = 0, u = !0, f = !0) : (r = (t - i.minTranslate()) / s, u = r <= 0, f = r >= 1);
    n.extend(i, {
      progress: r,
      isBeginning: u,
      isEnd: f
    });
    (e.watchSlidesProgress || e.watchSlidesVisibility) && i.updateSlidesProgress(t);
    u && !h && i.emit("reachBeginning toEdge");
    f && !c && i.emit("reachEnd toEdge");
    (h && !u || c && !f) && i.emit("fromEdge");
    i.emit("progress", r)
  }

  function su() {
    var t = this,
      e = t.slides,
      n = t.params,
      u = t.$wrapperEl,
      o = t.activeIndex,
      s = t.realIndex,
      h = t.virtual && n.virtual.enabled,
      f, i, r;
    e.removeClass(n.slideActiveClass + " " + n.slideNextClass + " " + n.slidePrevClass + " " + n.slideDuplicateActiveClass + " " + n.slideDuplicateNextClass + " " + n.slideDuplicatePrevClass);
    f = h ? t.$wrapperEl.find("." + n.slideClass + '[data-swiper-slide-index="' + o + '"]') : e.eq(o);
    f.addClass(n.slideActiveClass);
    n.loop && (f.hasClass(n.slideDuplicateClass) ? u.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + s + '"]').addClass(n.slideDuplicateActiveClass) : u.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + s + '"]').addClass(n.slideDuplicateActiveClass));
    i = f.nextAll("." + n.slideClass).eq(0).addClass(n.slideNextClass);
    n.loop && i.length === 0 && (i = e.eq(0), i.addClass(n.slideNextClass));
    r = f.prevAll("." + n.slideClass).eq(0).addClass(n.slidePrevClass);
    n.loop && r.length === 0 && (r = e.eq(-1), r.addClass(n.slidePrevClass));
    n.loop && (i.hasClass(n.slideDuplicateClass) ? u.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + i.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass) : u.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + i.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass), r.hasClass(n.slideDuplicateClass) ? u.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + r.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass) : u.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + r.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass))
  }

  function hu(t) {
    var i = this,
      e = i.rtlTranslate ? i.translate : -i.translate,
      f = i.slidesGrid,
      s = i.snapGrid,
      c = i.params,
      l = i.activeIndex,
      a = i.realIndex,
      v = i.snapIndex,
      u = t,
      o, r, h;
    if (typeof u == "undefined") {
      for (r = 0; r < f.length; r += 1) typeof f[r + 1] != "undefined" ? e >= f[r] && e < f[r + 1] - (f[r + 1] - f[r]) / 2 ? u = r : e >= f[r] && e < f[r + 1] && (u = r + 1) : e >= f[r] && (u = r);
      c.normalizeSlideIndex && (u < 0 || typeof u == "undefined") && (u = 0)
    }
    if (o = s.indexOf(e) >= 0 ? s.indexOf(e) : Math.floor(u / c.slidesPerGroup), o >= s.length && (o = s.length - 1), u === l) {
      o !== v && (i.snapIndex = o, i.emit("snapIndexChange"));
      return
    }
    h = parseInt(i.slides.eq(u).attr("data-swiper-slide-index") || u, 10);
    n.extend(i, {
      snapIndex: o,
      realIndex: h,
      previousIndex: l,
      activeIndex: u
    });
    i.emit("activeIndexChange");
    i.emit("snapIndexChange");
    a !== h && i.emit("realIndexChange");
    (i.initialized || i.runCallbacksOnInit) && i.emit("slideChange")
  }

  function cu(n) {
    var i = this,
      f = i.params,
      r = t(n.target).closest("." + f.slideClass)[0],
      e = !1,
      u;
    if (r)
      for (u = 0; u < i.slides.length; u += 1) i.slides[u] === r && (e = !0);
    if (r && e) i.clickedSlide = r, i.clickedIndex = i.virtual && i.params.virtual.enabled ? parseInt(t(r).attr("data-swiper-slide-index"), 10) : t(r).index();
    else {
      i.clickedSlide = undefined;
      i.clickedIndex = undefined;
      return
    }
    f.slideToClickedSlide && i.clickedIndex !== undefined && i.clickedIndex !== i.activeIndex && i.slideToClickedSlide()
  }

  function lu(t) {
    var r;
    t === void 0 && (t = this.isHorizontal() ? "x" : "y");
    var i = this,
      e = i.params,
      u = i.rtlTranslate,
      f = i.translate,
      o = i.$wrapperEl;
    return e.virtualTranslate ? u ? -f : f : (r = n.getTranslate(o[0], t), u && (r = -r), r || 0)
  }

  function au(n, t) {
    var i = this,
      c = i.rtlTranslate,
      o = i.params,
      s = i.$wrapperEl,
      l = i.progress,
      u = 0,
      f = 0,
      h, e;
    i.isHorizontal() ? u = c ? -n : n : f = n;
    o.roundLengths && (u = Math.floor(u), f = Math.floor(f));
    o.virtualTranslate || (r.transforms3d ? s.transform("translate3d(" + u + "px, " + f + "px, 0px)") : s.transform("translate(" + u + "px, " + f + "px)"));
    i.previousTranslate = i.translate;
    i.translate = i.isHorizontal() ? u : f;
    e = i.maxTranslate() - i.minTranslate();
    h = e === 0 ? 0 : (n - i.minTranslate()) / e;
    h !== l && i.updateProgress(n);
    i.emit("setTranslate", i.translate, t)
  }

  function vu() {
    return -this.snapGrid[0]
  }

  function yu() {
    return -this.snapGrid[this.snapGrid.length - 1]
  }

  function pu(n, t) {
    var i = this;
    i.$wrapperEl.transition(n);
    i.emit("setTransition", n, t)
  }

  function wu(n, t) {
    var r;
    n === void 0 && (n = !0);
    var i = this,
      u = i.activeIndex,
      e = i.params,
      f = i.previousIndex;
    if (e.autoHeight && i.updateAutoHeight(), r = t, r || (r = u > f ? "next" : u < f ? "prev" : "reset"), i.emit("transitionStart"), n && u !== f) {
      if (r === "reset") {
        i.emit("slideResetTransitionStart");
        return
      }
      i.emit("slideChangeTransitionStart");
      r === "next" ? i.emit("slideNextTransitionStart") : i.emit("slidePrevTransitionStart")
    }
  }

  function bu(n, t) {
    var r;
    n === void 0 && (n = !0);
    var i = this,
      u = i.activeIndex,
      f = i.previousIndex;
    if (i.animating = !1, i.setTransition(0), r = t, r || (r = u > f ? "next" : u < f ? "prev" : "reset"), i.emit("transitionEnd"), n && u !== f) {
      if (r === "reset") {
        i.emit("slideResetTransitionEnd");
        return
      }
      i.emit("slideChangeTransitionEnd");
      r === "next" ? i.emit("slideNextTransitionEnd") : i.emit("slidePrevTransitionEnd")
    }
  }

  function ku(n, t, i, u) {
    var f, e, a, o, l, s;
    n === void 0 && (n = 0);
    t === void 0 && (t = this.params.speed);
    i === void 0 && (i = !0);
    f = this;
    e = n;
    e < 0 && (e = 0);
    var h = f.params,
      v = f.snapGrid,
      y = f.slidesGrid,
      w = f.previousIndex,
      c = f.activeIndex,
      p = f.rtlTranslate;
    if (f.animating && h.preventInteractionOnTransition) return !1;
    if (a = Math.floor(e / h.slidesPerGroup), a >= v.length && (a = v.length - 1), (c || h.initialSlide || 0) === (w || 0) && i && f.emit("beforeSlideChangeStart"), o = -v[a], f.updateProgress(o), h.normalizeSlideIndex)
      for (l = 0; l < y.length; l += 1) - Math.floor(o * 100) >= Math.floor(y[l] * 100) && (e = l);
    return f.initialized && e !== c && (!f.allowSlideNext && o < f.translate && o < f.minTranslate() || !f.allowSlidePrev && o > f.translate && o > f.maxTranslate() && (c || 0) !== e) ? !1 : (s = e > c ? "next" : e < c ? "prev" : "reset", p && -o === f.translate || !p && o === f.translate) ? (f.updateActiveIndex(e), h.autoHeight && f.updateAutoHeight(), f.updateSlidesClasses(), h.effect !== "slide" && f.setTranslate(o), s !== "reset" && (f.transitionStart(i, s), f.transitionEnd(i, s)), !1) : (t !== 0 && r.transition ? (f.setTransition(t), f.setTranslate(o), f.updateActiveIndex(e), f.updateSlidesClasses(), f.emit("beforeTransitionStart", t, u), f.transitionStart(i, s), f.animating || (f.animating = !0, f.onSlideToWrapperTransitionEnd || (f.onSlideToWrapperTransitionEnd = function (n) {
      f && !f.destroyed && n.target === this && (f.$wrapperEl[0].removeEventListener("transitionend", f.onSlideToWrapperTransitionEnd), f.$wrapperEl[0].removeEventListener("webkitTransitionEnd", f.onSlideToWrapperTransitionEnd), f.onSlideToWrapperTransitionEnd = null, delete f.onSlideToWrapperTransitionEnd, f.transitionEnd(i, s))
    }), f.$wrapperEl[0].addEventListener("transitionend", f.onSlideToWrapperTransitionEnd), f.$wrapperEl[0].addEventListener("webkitTransitionEnd", f.onSlideToWrapperTransitionEnd))) : (f.setTransition(0), f.setTranslate(o), f.updateActiveIndex(e), f.updateSlidesClasses(), f.emit("beforeTransitionStart", t, u), f.transitionStart(i, s), f.transitionEnd(i, s)), !0)
  }

  function du(n, t, i, r) {
    n === void 0 && (n = 0);
    t === void 0 && (t = this.params.speed);
    i === void 0 && (i = !0);
    var u = this,
      f = n;
    return u.params.loop && (f += u.loopedSlides), u.slideTo(f, t, i, r)
  }

  function gu(n, t, i) {
    n === void 0 && (n = this.params.speed);
    t === void 0 && (t = !0);
    var r = this,
      u = r.params,
      f = r.animating;
    return u.loop ? f ? !1 : (r.loopFix(), r._clientLeft = r.$wrapperEl[0].clientLeft, r.slideTo(r.activeIndex + u.slidesPerGroup, n, t, i)) : r.slideTo(r.activeIndex + u.slidesPerGroup, n, t, i)
  }

  function nf(n, t, i) {
    function e(n) {
      return n < 0 ? -Math.floor(Math.abs(n)) : Math.floor(n)
    }
    var s;
    n === void 0 && (n = this.params.speed);
    t === void 0 && (t = !0);
    var r = this,
      a = r.params,
      v = r.animating,
      f = r.snapGrid,
      o = r.slidesGrid,
      y = r.rtlTranslate;
    if (a.loop) {
      if (v) return !1;
      r.loopFix();
      r._clientLeft = r.$wrapperEl[0].clientLeft
    }
    s = y ? r.translate : -r.translate;
    var h = e(s),
      c = f.map(function (n) {
        return e(n)
      }),
      p = o.map(function (n) {
        return e(n)
      }),
      w = f[c.indexOf(h)],
      l = f[c.indexOf(h) - 1],
      u;
    return typeof l != "undefined" && (u = o.indexOf(l), u < 0 && (u = r.activeIndex - 1)), r.slideTo(u, n, t, i)
  }

  function tf(n, t, i) {
    n === void 0 && (n = this.params.speed);
    t === void 0 && (t = !0);
    var r = this;
    return r.slideTo(r.activeIndex, n, t, i)
  }

  function rf(n, t, i) {
    n === void 0 && (n = this.params.speed);
    t === void 0 && (t = !0);
    var r = this,
      u = r.activeIndex,
      f = Math.floor(u / r.params.slidesPerGroup);
    if (f < r.snapGrid.length - 1) {
      var o = r.rtlTranslate ? r.translate : -r.translate,
        e = r.snapGrid[f],
        s = r.snapGrid[f + 1];
      o - e > (s - e) / 2 && (u = r.params.slidesPerGroup)
    }
    return r.slideTo(u, n, t, i)
  }

  function uf() {
    var i = this,
      u = i.params,
      o = i.$wrapperEl,
      f = u.slidesPerView === "auto" ? i.slidesPerViewDynamic() : u.slidesPerView,
      r = i.clickedIndex,
      e;
    if (u.loop) {
      if (i.animating) return;
      e = parseInt(t(i.clickedSlide).attr("data-swiper-slide-index"), 10);
      u.centeredSlides ? r < i.loopedSlides - f / 2 || r > i.slides.length - i.loopedSlides + f / 2 ? (i.loopFix(), r = o.children("." + u.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + u.slideDuplicateClass + ")").eq(0).index(), n.nextTick(function () {
        i.slideTo(r)
      })) : i.slideTo(r) : r > i.slides.length - f ? (i.loopFix(), r = o.children("." + u.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + u.slideDuplicateClass + ")").eq(0).index(), n.nextTick(function () {
        i.slideTo(r)
      })) : i.slideTo(r)
    } else i.slideTo(r)
  }

  function ff() {
    var r = this,
      n = r.params,
      f = r.$wrapperEl,
      i, c, l, a, e, o, s, h;
    if (f.children("." + n.slideClass + "." + n.slideDuplicateClass).remove(), i = f.children("." + n.slideClass), n.loopFillGroupWithBlank && (c = n.slidesPerGroup - i.length % n.slidesPerGroup, c !== n.slidesPerGroup)) {
      for (l = 0; l < c; l += 1) a = t(u.createElement("div")).addClass(n.slideClass + " " + n.slideBlankClass), f.append(a);
      i = f.children("." + n.slideClass)
    }
    for (n.slidesPerView !== "auto" || n.loopedSlides || (n.loopedSlides = i.length), r.loopedSlides = parseInt(n.loopedSlides || n.slidesPerView, 10), r.loopedSlides += n.loopAdditionalSlides, r.loopedSlides > i.length && (r.loopedSlides = i.length), e = [], o = [], i.each(function (n, u) {
        var f = t(u);
        n < r.loopedSlides && o.push(u);
        n < i.length && n >= i.length - r.loopedSlides && e.push(u);
        f.attr("data-swiper-slide-index", n)
      }), s = 0; s < o.length; s += 1) f.append(t(o[s].cloneNode(!0)).addClass(n.slideDuplicateClass));
    for (h = e.length - 1; h >= 0; h -= 1) f.prepend(t(e[h].cloneNode(!0)).addClass(n.slideDuplicateClass))
  }

  function ef() {
    var n = this,
      c = n.params,
      i = n.activeIndex,
      f = n.slides,
      t = n.loopedSlides,
      l = n.allowSlidePrev,
      a = n.allowSlideNext,
      v = n.snapGrid,
      e = n.rtlTranslate,
      r, o, u, s, h;
    n.allowSlidePrev = !0;
    n.allowSlideNext = !0;
    o = -v[i];
    u = o - n.getTranslate();
    i < t ? (r = f.length - t * 3 + i, r += t, s = n.slideTo(r, 0, !1, !0), s && u !== 0 && n.setTranslate((e ? -n.translate : n.translate) - u)) : (c.slidesPerView === "auto" && i >= t * 2 || i >= f.length - t) && (r = -f.length + i + t, r += t, h = n.slideTo(r, 0, !1, !0), h && u !== 0 && n.setTranslate((e ? -n.translate : n.translate) - u));
    n.allowSlidePrev = l;
    n.allowSlideNext = a
  }

  function of () {
    var t = this,
      i = t.$wrapperEl,
      n = t.params,
      r = t.slides;
    i.children("." + n.slideClass + "." + n.slideDuplicateClass + ",." + n.slideClass + "." + n.slideBlankClass).remove();
    r.removeAttr("data-swiper-slide-index")
  }

  function sf(n) {
    var i = this,
      t;
    r.touch || !i.params.simulateTouch || i.params.watchOverflow && i.isLocked || (t = i.el, t.style.cursor = "move", t.style.cursor = n ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = n ? "-moz-grabbin" : "-moz-grab", t.style.cursor = n ? "grabbing" : "grab")
  }

  function hf() {
    var n = this;
    r.touch || n.params.watchOverflow && n.isLocked || (n.el.style.cursor = "")
  }

  function cf(n) {
    var t = this,
      f = t.$wrapperEl,
      u = t.params,
      i;
    if (u.loop && t.loopDestroy(), typeof n == "object" && "length" in n)
      for (i = 0; i < n.length; i += 1) n[i] && f.append(n[i]);
    else f.append(n);
    u.loop && t.loopCreate();
    u.observer && r.observer || t.update()
  }

  function lf(n) {
    var t = this,
      u = t.params,
      e = t.$wrapperEl,
      o = t.activeIndex,
      f, i;
    if (u.loop && t.loopDestroy(), f = o + 1, typeof n == "object" && "length" in n) {
      for (i = 0; i < n.length; i += 1) n[i] && e.prepend(n[i]);
      f = o + n.length
    } else e.prepend(n);
    u.loop && t.loopCreate();
    u.observer && r.observer || t.update();
    t.slideTo(f, 0, !1)
  }

  function af(n, t) {
    var i = this,
      o = i.$wrapperEl,
      f = i.params,
      y = i.activeIndex,
      u = y,
      a, s, h, c, v, e, l;
    if (f.loop && (u -= i.loopedSlides, i.loopDestroy(), i.slides = o.children("." + f.slideClass)), a = i.slides.length, n <= 0) {
      i.prependSlide(t);
      return
    }
    if (n >= a) {
      i.appendSlide(t);
      return
    }
    for (s = u > n ? u + 1 : u, h = [], c = a - 1; c >= n; c -= 1) v = i.slides.eq(c), v.remove(), h.unshift(v);
    if (typeof t == "object" && "length" in t) {
      for (e = 0; e < t.length; e += 1) t[e] && o.append(t[e]);
      s = u > n ? u + t.length : u
    } else o.append(t);
    for (l = 0; l < h.length; l += 1) o.append(h[l]);
    f.loop && i.loopCreate();
    f.observer && r.observer || i.update();
    f.loop ? i.slideTo(s + i.loopedSlides, 0, !1) : i.slideTo(s, 0, !1)
  }

  function vf(n) {
    var t = this,
      f = t.params,
      s = t.$wrapperEl,
      h = t.activeIndex,
      o = h,
      i, u, e;
    if (f.loop && (o -= t.loopedSlides, t.loopDestroy(), t.slides = s.children("." + f.slideClass)), i = o, typeof n == "object" && "length" in n) {
      for (e = 0; e < n.length; e += 1) u = n[e], t.slides[u] && t.slides.eq(u).remove(), u < i && (i -= 1);
      i = Math.max(i, 0)
    } else u = n, t.slides[u] && t.slides.eq(u).remove(), u < i && (i -= 1), i = Math.max(i, 0);
    f.loop && t.loopCreate();
    f.observer && r.observer || t.update();
    f.loop ? t.slideTo(i + t.loopedSlides, 0, !1) : t.slideTo(i, 0, !1)
  }

  function yf() {
    for (var t = this, i = [], n = 0; n < t.slides.length; n += 1) i.push(n);
    t.removeSlide(i)
  }

  function pf(r) {
    var o = this,
      s = o.touchEventsData,
      e = o.params,
      h = o.touches,
      f, l, v;
    if ((!o.animating || !e.preventInteractionOnTransition) && (f = r, f.originalEvent && (f = f.originalEvent), s.isTouchEvent = f.type === "touchstart", s.isTouchEvent || !("which" in f) || f.which !== 3) && (s.isTouchEvent || !("button" in f) || !(f.button > 0)) && (!s.isTouched || !s.isMoved)) {
      if (e.noSwiping && t(f.target).closest(e.noSwipingSelector ? e.noSwipingSelector : "." + e.noSwipingClass)[0]) {
        o.allowClick = !0;
        return
      }
      if (!e.swipeHandler || t(f).closest(e.swipeHandler)[0]) {
        h.currentX = f.type === "touchstart" ? f.targetTouches[0].pageX : f.pageX;
        h.currentY = f.type === "touchstart" ? f.targetTouches[0].pageY : f.pageY;
        var c = h.currentX,
          y = h.currentY,
          p = e.edgeSwipeDetection || e.iOSEdgeSwipeDetection,
          a = e.edgeSwipeThreshold || e.iOSEdgeSwipeThreshold;
        p && (c <= a || c >= i.screen.width - a) || (n.extend(s, {
          isTouched: !0,
          isMoved: !1,
          allowTouchCallbacks: !0,
          isScrolling: undefined,
          startMoving: undefined
        }), h.startX = c, h.startY = y, s.touchStartTime = n.now(), o.allowClick = !0, o.updateSize(), o.swipeDirection = undefined, e.threshold > 0 && (s.allowThresholdMove = !1), f.type !== "touchstart" && (l = !0, t(f.target).is(s.formElements) && (l = !1), u.activeElement && t(u.activeElement).is(s.formElements) && u.activeElement !== f.target && u.activeElement.blur(), v = l && o.allowTouchMove && e.touchStartPreventDefault, (e.touchStartForcePreventDefault || v) && f.preventDefault()), o.emit("touchStart", f))
      }
    }
  }

  function wf(i) {
    var f = this,
      r = f.touchEventsData,
      s = f.params,
      e = f.touches,
      k = f.rtlTranslate,
      o = i,
      a, v, w, h, y, p;
    if (o.originalEvent && (o = o.originalEvent), !r.isTouched) {
      r.startMoving && r.isScrolling && f.emit("touchMoveOpposite", o);
      return
    }
    if (!r.isTouchEvent || o.type !== "mousemove") {
      var b = o.type === "touchmove" && o.targetTouches && (o.targetTouches[0] || o.changedTouches[0]),
        c = o.type === "touchmove" ? b.pageX : o.pageX,
        l = o.type === "touchmove" ? b.pageY : o.pageY;
      if (o.preventedByNestedSwiper) {
        e.startX = c;
        e.startY = l;
        return
      }
      if (!f.allowTouchMove) {
        f.allowClick = !1;
        r.isTouched && (n.extend(e, {
          startX: c,
          startY: l,
          currentX: c,
          currentY: l
        }), r.touchStartTime = n.now());
        return
      }
      if (r.isTouchEvent && s.touchReleaseOnEdges && !s.loop)
        if (f.isVertical()) {
          if (l < e.startY && f.translate <= f.maxTranslate() || l > e.startY && f.translate >= f.minTranslate()) {
            r.isTouched = !1;
            r.isMoved = !1;
            return
          }
        } else if (c < e.startX && f.translate <= f.maxTranslate() || c > e.startX && f.translate >= f.minTranslate()) return;
      if (r.isTouchEvent && u.activeElement && o.target === u.activeElement && t(o.target).is(r.formElements)) {
        r.isMoved = !0;
        f.allowClick = !1;
        return
      }
      if ((r.allowTouchCallbacks && f.emit("touchMove", o), !o.targetTouches || !(o.targetTouches.length > 1)) && (e.currentX = c, e.currentY = l, a = e.currentX - e.startX, v = e.currentY - e.startY, !f.params.threshold || !(Math.sqrt(Math.pow(a, 2) + Math.pow(v, 2)) < f.params.threshold))) {
        if (typeof r.isScrolling == "undefined" && (f.isHorizontal() && e.currentY === e.startY || f.isVertical() && e.currentX === e.startX ? r.isScrolling = !1 : a * a + v * v >= 25 && (w = Math.atan2(Math.abs(v), Math.abs(a)) * 180 / Math.PI, r.isScrolling = f.isHorizontal() ? w > s.touchAngle : 90 - w > s.touchAngle)), r.isScrolling && f.emit("touchMoveOpposite", o), typeof r.startMoving == "undefined" && (e.currentX !== e.startX || e.currentY !== e.startY) && (r.startMoving = !0), r.isScrolling) {
          r.isTouched = !1;
          return
        }
        if (r.startMoving) {
          if (f.allowClick = !1, o.preventDefault(), s.touchMoveStopPropagation && !s.nested && o.stopPropagation(), r.isMoved || (s.loop && f.loopFix(), r.startTranslate = f.getTranslate(), f.setTransition(0), f.animating && f.$wrapperEl.trigger("webkitTransitionEnd transitionend"), r.allowMomentumBounce = !1, s.grabCursor && (f.allowSlideNext === !0 || f.allowSlidePrev === !0) && f.setGrabCursor(!0), f.emit("sliderFirstMove", o)), f.emit("sliderMove", o), r.isMoved = !0, h = f.isHorizontal() ? a : v, e.diff = h, h *= s.touchRatio, k && (h = -h), f.swipeDirection = h > 0 ? "prev" : "next", r.currentTranslate = h + r.startTranslate, y = !0, p = s.resistanceRatio, s.touchReleaseOnEdges && (p = 0), h > 0 && r.currentTranslate > f.minTranslate() ? (y = !1, s.resistance && (r.currentTranslate = f.minTranslate() - 1 + Math.pow(-f.minTranslate() + r.startTranslate + h, p))) : h < 0 && r.currentTranslate < f.maxTranslate() && (y = !1, s.resistance && (r.currentTranslate = f.maxTranslate() + 1 - Math.pow(f.maxTranslate() - r.startTranslate - h, p))), y && (o.preventedByNestedSwiper = !0), !f.allowSlideNext && f.swipeDirection === "next" && r.currentTranslate < r.startTranslate && (r.currentTranslate = r.startTranslate), !f.allowSlidePrev && f.swipeDirection === "prev" && r.currentTranslate > r.startTranslate && (r.currentTranslate = r.startTranslate), s.threshold > 0)
            if (Math.abs(h) > s.threshold || r.allowThresholdMove) {
              if (!r.allowThresholdMove) {
                r.allowThresholdMove = !0;
                e.startX = e.currentX;
                e.startY = e.currentY;
                r.currentTranslate = r.startTranslate;
                e.diff = f.isHorizontal() ? e.currentX - e.startX : e.currentY - e.startY;
                return
              }
            } else {
              r.currentTranslate = r.startTranslate;
              return
            } s.followFinger && ((s.freeMode || s.watchSlidesProgress || s.watchSlidesVisibility) && (f.updateActiveIndex(), f.updateSlidesClasses()), s.freeMode && (r.velocities.length === 0 && r.velocities.push({
            position: e[f.isHorizontal() ? "startX" : "startY"],
            time: r.touchStartTime
          }), r.velocities.push({
            position: e[f.isHorizontal() ? "currentX" : "currentY"],
            time: n.now()
          })), f.updateProgress(r.currentTranslate), f.setTranslate(r.currentTranslate))
        }
      }
    }
  }

  function bf(t) {
    var i = this,
      r = i.touchEventsData,
      u = i.params,
      ot = i.touches,
      nt = i.rtlTranslate,
      tt = i.$wrapperEl,
      e = i.slidesGrid,
      h = i.snapGrid,
      c = t,
      w, a, l, k, d, v, rt, y, p, s, g, o, ut;
    if (c.originalEvent && (c = c.originalEvent), r.allowTouchCallbacks && i.emit("touchEnd", c), r.allowTouchCallbacks = !1, !r.isTouched) {
      r.isMoved && u.grabCursor && i.setGrabCursor(!1);
      r.isMoved = !1;
      r.startMoving = !1;
      return
    }
    if (u.grabCursor && r.isMoved && r.isTouched && (i.allowSlideNext === !0 || i.allowSlidePrev === !0) && i.setGrabCursor(!1), w = n.now(), a = w - r.touchStartTime, i.allowClick && (i.updateClickedSlide(c), i.emit("tap", c), a < 300 && w - r.lastClickTime > 300 && (r.clickTimeout && clearTimeout(r.clickTimeout), r.clickTimeout = n.nextTick(function () {
        i && !i.destroyed && i.emit("click", c)
      }, 300)), a < 300 && w - r.lastClickTime < 300 && (r.clickTimeout && clearTimeout(r.clickTimeout), i.emit("doubleTap", c))), r.lastClickTime = n.now(), n.nextTick(function () {
        i.destroyed || (i.allowClick = !0)
      }), !r.isTouched || !r.isMoved || !i.swipeDirection || ot.diff === 0 || r.currentTranslate === r.startTranslate) {
      r.isTouched = !1;
      r.isMoved = !1;
      r.startMoving = !1;
      return
    }
    if (r.isTouched = !1, r.isMoved = !1, r.startMoving = !1, l = u.followFinger ? nt ? i.translate : -i.translate : -r.currentTranslate, u.freeMode) {
      if (l < -i.minTranslate()) {
        i.slideTo(i.activeIndex);
        return
      }
      if (l > -i.maxTranslate()) {
        i.slides.length < h.length ? i.slideTo(h.length - 1) : i.slideTo(i.slides.length - 1);
        return
      }
      if (u.freeModeMomentum) {
        if (r.velocities.length > 1) {
          var it = r.velocities.pop(),
            ft = r.velocities.pop(),
            st = it.position - ft.position,
            et = it.time - ft.time;
          i.velocity = st / et;
          i.velocity /= 2;
          Math.abs(i.velocity) < u.freeModeMinimumVelocity && (i.velocity = 0);
          (et > 150 || n.now() - it.time > 300) && (i.velocity = 0)
        } else i.velocity = 0;
        i.velocity *= u.freeModeMomentumVelocityRatio;
        r.velocities.length = 0;
        var b = 1e3 * u.freeModeMomentumRatio,
          ht = i.velocity * b,
          f = i.translate + ht;
        if (nt && (f = -f), k = !1, v = Math.abs(i.velocity) * 20 * u.freeModeMomentumBounceRatio, f < i.maxTranslate()) u.freeModeMomentumBounce ? (f + i.maxTranslate() < -v && (f = i.maxTranslate() - v), d = i.maxTranslate(), k = !0, r.allowMomentumBounce = !0) : f = i.maxTranslate(), u.loop && u.centeredSlides && (rt = !0);
        else if (f > i.minTranslate()) u.freeModeMomentumBounce ? (f - i.minTranslate() > v && (f = i.minTranslate() + v), d = i.minTranslate(), k = !0, r.allowMomentumBounce = !0) : f = i.minTranslate(), u.loop && u.centeredSlides && (rt = !0);
        else if (u.freeModeSticky) {
          for (p = 0; p < h.length; p += 1)
            if (h[p] > -f) {
              y = p;
              break
            } f = Math.abs(h[y] - f) < Math.abs(h[y - 1] - f) || i.swipeDirection === "next" ? h[y] : h[y - 1];
          f = -f
        }
        if (rt) i.once("transitionEnd", function () {
          i.loopFix()
        });
        if (i.velocity !== 0) b = nt ? Math.abs((-f - i.translate) / i.velocity) : Math.abs((f - i.translate) / i.velocity);
        else if (u.freeModeSticky) {
          i.slideToClosest();
          return
        }
        u.freeModeMomentumBounce && k ? (i.updateProgress(d), i.setTransition(b), i.setTranslate(f), i.transitionStart(!0, i.swipeDirection), i.animating = !0, tt.transitionEnd(function () {
          i && !i.destroyed && r.allowMomentumBounce && (i.emit("momentumBounce"), i.setTransition(u.speed), i.setTranslate(d), tt.transitionEnd(function () {
            i && !i.destroyed && i.transitionEnd()
          }))
        })) : i.velocity ? (i.updateProgress(f), i.setTransition(b), i.setTranslate(f), i.transitionStart(!0, i.swipeDirection), i.animating || (i.animating = !0, tt.transitionEnd(function () {
          i && !i.destroyed && i.transitionEnd()
        }))) : i.updateProgress(f);
        i.updateActiveIndex();
        i.updateSlidesClasses()
      } else if (u.freeModeSticky) {
        i.slideToClosest();
        return
      }(!u.freeModeMomentum || a >= u.longSwipesMs) && (i.updateProgress(), i.updateActiveIndex(), i.updateSlidesClasses());
      return
    }
    for (s = 0, g = i.slidesSizesGrid[0], o = 0; o < e.length; o += u.slidesPerGroup) typeof e[o + u.slidesPerGroup] != "undefined" ? l >= e[o] && l < e[o + u.slidesPerGroup] && (s = o, g = e[o + u.slidesPerGroup] - e[o]) : l >= e[o] && (s = o, g = e[e.length - 1] - e[e.length - 2]);
    if (ut = (l - e[s]) / g, a > u.longSwipesMs) {
      if (!u.longSwipes) {
        i.slideTo(i.activeIndex);
        return
      }
      i.swipeDirection === "next" && (ut >= u.longSwipesRatio ? i.slideTo(s + u.slidesPerGroup) : i.slideTo(s));
      i.swipeDirection === "prev" && (ut > 1 - u.longSwipesRatio ? i.slideTo(s + u.slidesPerGroup) : i.slideTo(s))
    } else {
      if (!u.shortSwipes) {
        i.slideTo(i.activeIndex);
        return
      }
      i.swipeDirection === "next" && i.slideTo(s + u.slidesPerGroup);
      i.swipeDirection === "prev" && i.slideTo(s)
    }
  }

  function wt() {
    var n = this,
      t = n.params,
      i = n.el,
      r;
    if (!i || i.offsetWidth !== 0) {
      t.breakpoints && n.setBreakpoint();
      var u = n.allowSlideNext,
        f = n.allowSlidePrev,
        e = n.snapGrid;
      n.allowSlideNext = !0;
      n.allowSlidePrev = !0;
      n.updateSize();
      n.updateSlides();
      t.freeMode ? (r = Math.min(Math.max(n.translate, n.maxTranslate()), n.minTranslate()), n.setTranslate(r), n.updateActiveIndex(), n.updateSlidesClasses(), t.autoHeight && n.updateAutoHeight()) : (n.updateSlidesClasses(), (t.slidesPerView === "auto" || t.slidesPerView > 1) && n.isEnd && !n.params.centeredSlides ? n.slideTo(n.slides.length - 1, 0, !1, !0) : n.slideTo(n.activeIndex, 0, !1, !0));
      n.autoplay && n.autoplay.running && n.autoplay.paused && n.autoplay.run();
      n.allowSlidePrev = f;
      n.allowSlideNext = u;
      n.params.watchOverflow && e !== n.snapGrid && n.checkOverflow()
    }
  }

  function kf(n) {
    var t = this;
    t.allowClick || (t.params.preventClicks && n.preventDefault(), t.params.preventClicksPropagation && t.animating && (n.stopPropagation(), n.stopImmediatePropagation()))
  }

  function df() {}

  function gf() {
    var n = this,
      t = n.params,
      i = n.touchEvents,
      h = n.el,
      c = n.wrapperEl,
      f, o, s;
    n.onTouchStart = pf.bind(n);
    n.onTouchMove = wf.bind(n);
    n.onTouchEnd = bf.bind(n);
    n.onClick = kf.bind(n);
    f = t.touchEventsTarget === "container" ? h : c;
    o = !!t.nested;
    !r.touch && (r.pointerEvents || r.prefixedPointerEvents) ? (f.addEventListener(i.start, n.onTouchStart, !1), u.addEventListener(i.move, n.onTouchMove, o), u.addEventListener(i.end, n.onTouchEnd, !1)) : (r.touch && (s = i.start === "touchstart" && r.passiveListener && t.passiveListeners ? {
      passive: !0,
      capture: !1
    } : !1, f.addEventListener(i.start, n.onTouchStart, s), f.addEventListener(i.move, n.onTouchMove, r.passiveListener ? {
      passive: !1,
      capture: o
    } : o), f.addEventListener(i.end, n.onTouchEnd, s), tt || (u.addEventListener("touchstart", df), tt = !0)), (t.simulateTouch && !e.ios && !e.android || t.simulateTouch && !r.touch && e.ios) && (f.addEventListener("mousedown", n.onTouchStart, !1), u.addEventListener("mousemove", n.onTouchMove, o), u.addEventListener("mouseup", n.onTouchEnd, !1)));
    (t.preventClicks || t.preventClicksPropagation) && f.addEventListener("click", n.onClick, !0);
    n.on(e.ios || e.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", wt, !0)
  }

  function ne() {
    var n = this,
      t = n.params,
      i = n.touchEvents,
      h = n.el,
      c = n.wrapperEl,
      f = t.touchEventsTarget === "container" ? h : c,
      o = !!t.nested,
      s;
    !r.touch && (r.pointerEvents || r.prefixedPointerEvents) ? (f.removeEventListener(i.start, n.onTouchStart, !1), u.removeEventListener(i.move, n.onTouchMove, o), u.removeEventListener(i.end, n.onTouchEnd, !1)) : (r.touch && (s = i.start === "onTouchStart" && r.passiveListener && t.passiveListeners ? {
      passive: !0,
      capture: !1
    } : !1, f.removeEventListener(i.start, n.onTouchStart, s), f.removeEventListener(i.move, n.onTouchMove, o), f.removeEventListener(i.end, n.onTouchEnd, s)), (t.simulateTouch && !e.ios && !e.android || t.simulateTouch && !r.touch && e.ios) && (f.removeEventListener("mousedown", n.onTouchStart, !1), u.removeEventListener("mousemove", n.onTouchMove, o), u.removeEventListener("mouseup", n.onTouchEnd, !1)));
    (t.preventClicks || t.preventClicksPropagation) && f.removeEventListener("click", n.onClick, !0);
    n.off(e.ios || e.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", wt)
  }

  function te() {
    var t = this,
      c = t.activeIndex,
      s = t.initialized,
      o = t.loopedSlides,
      f, i, u, r;
    if ((o === void 0 && (o = 0), f = t.params, i = f.breakpoints, i && (!i || Object.keys(i).length !== 0)) && (u = t.getBreakpoint(i), u && t.currentBreakpoint !== u)) {
      r = u in i ? i[u] : undefined;
      r && ["slidesPerView", "spaceBetween", "slidesPerGroup"].forEach(function (n) {
        var t = r[n];
        typeof t != "undefined" && (r[n] = n === "slidesPerView" && (t === "AUTO" || t === "auto") ? "auto" : n === "slidesPerView" ? parseFloat(t) : parseInt(t, 10))
      });
      var e = r || t.originalParams,
        h = e.direction && e.direction !== f.direction,
        l = f.loop && (e.slidesPerView !== f.slidesPerView || h);
      h && s && t.changeDirection();
      n.extend(t.params, e);
      n.extend(t, {
        allowTouchMove: t.params.allowTouchMove,
        allowSlideNext: t.params.allowSlideNext,
        allowSlidePrev: t.params.allowSlidePrev
      });
      t.currentBreakpoint = u;
      l && s && (t.loopDestroy(), t.loopCreate(), t.updateSlides(), t.slideTo(c - o + t.loopedSlides, 0, !1));
      t.emit("breakpoint", e)
    }
  }

  function ie(n) {
    var e = this,
      t, r, f, u;
    if (!n) return undefined;
    for (t = !1, r = [], Object.keys(n).forEach(function (n) {
        r.push(n)
      }), r.sort(function (n, t) {
        return parseInt(n, 10) - parseInt(t, 10)
      }), f = 0; f < r.length; f += 1) u = r[f], e.params.breakpointsInverse ? u <= i.innerWidth && (t = u) : u >= i.innerWidth && !t && (t = u);
    return t || "max"
  }

  function re() {
    var i = this,
      u = i.classNames,
      t = i.params,
      f = i.rtl,
      o = i.$el,
      n = [];
    n.push("initialized");
    n.push(t.direction);
    t.freeMode && n.push("free-mode");
    r.flexbox || n.push("no-flexbox");
    t.autoHeight && n.push("autoheight");
    f && n.push("rtl");
    t.slidesPerColumn > 1 && n.push("multirow");
    e.android && n.push("android");
    e.ios && n.push("ios");
    (h.isIE || h.isEdge) && (r.pointerEvents || r.prefixedPointerEvents) && n.push("wp8-" + t.direction);
    n.forEach(function (n) {
      u.push(t.containerModifierClass + n)
    });
    o.addClass(u.join(" "))
  }

  function ue() {
    var n = this,
      t = n.$el,
      i = n.classNames;
    t.removeClass(i.join(" "))
  }

  function fe(n, t, r, u, f, e) {
    function s() {
      e && e()
    }
    var o;
    n.complete && f ? s() : t ? (o = new i.Image, o.onload = s, o.onerror = s, u && (o.sizes = u), r && (o.srcset = r), t && (o.src = t)) : s()
  }

  function ee() {
    function r() {
      typeof n != "undefined" && n !== null && n && !n.destroyed && (n.imagesLoaded !== undefined && (n.imagesLoaded += 1), n.imagesLoaded === n.imagesToLoad.length && (n.params.updateOnImagesReady && n.update(), n.emit("imagesReady")))
    }
    var n = this,
      i, t;
    for (n.imagesToLoad = n.$el.find("img"), i = 0; i < n.imagesToLoad.length; i += 1) t = n.imagesToLoad[i], n.loadImage(t, t.currentSrc || t.getAttribute("src"), t.srcset || t.getAttribute("srcset"), t.sizes || t.getAttribute("sizes"), !0, r)
  }

  function oe() {
    var n = this,
      t = n.isLocked;
    n.isLocked = n.snapGrid.length === 1;
    n.allowSlideNext = !n.isLocked;
    n.allowSlidePrev = !n.isLocked;
    t !== n.isLocked && n.emit(n.isLocked ? "lock" : "unlock");
    t && t !== n.isLocked && (n.isEnd = !1, n.navigation.update())
  }

  function we() {
    var t = "onwheel",
      n = t in u,
      i;
    return n || (i = u.createElement("div"), i.setAttribute(t, "return;"), n = typeof i[t] == "function"), !n && u.implementation && u.implementation.hasFeature && u.implementation.hasFeature("", "") !== !0 && (n = u.implementation.hasFeature("Events.wheel", "3.0")), n
  }
  var u = typeof document == "undefined" ? {
      body: {},
      addEventListener: function () {},
      removeEventListener: function () {},
      activeElement: {
        blur: function () {},
        nodeName: ""
      },
      querySelector: function () {
        return null
      },
      querySelectorAll: function () {
        return []
      },
      getElementById: function () {
        return null
      },
      createEvent: function () {
        return {
          initEvent: function () {}
        }
      },
      createElement: function () {
        return {
          children: [],
          childNodes: [],
          style: {},
          setAttribute: function () {},
          getElementsByTagName: function () {
            return []
          }
        }
      },
      location: {
        hash: ""
      }
    } : document,
    i = typeof window == "undefined" ? {
      document: u,
      navigator: {
        userAgent: ""
      },
      location: {},
      history: {},
      CustomEvent: function () {
        return this
      },
      addEventListener: function () {},
      removeEventListener: function () {},
      getComputedStyle: function () {
        return {
          getPropertyValue: function () {
            return ""
          }
        }
      },
      Image: function () {},
      Date: function () {},
      screen: {},
      setTimeout: function () {},
      clearTimeout: function () {}
    } : window,
    f = function (n) {
      for (var i = this, t = 0; t < n.length; t += 1) i[t] = n[t];
      return i.length = n.length, this
    },
    nt, ht, ct, lt, at, vt, yt, pt, e, tt, bt, kt, dt, gt;
  t.fn = f.prototype;
  t.Class = f;
  t.Dom7 = f;
  nt = {
    addClass: oi,
    removeClass: si,
    hasClass: hi,
    toggleClass: ci,
    attr: li,
    removeAttr: ai,
    data: vi,
    transform: yi,
    transition: pi,
    on: wi,
    off: bi,
    trigger: ki,
    transitionEnd: di,
    outerWidth: gi,
    outerHeight: nr,
    offset: tr,
    css: rr,
    each: ur,
    html: fr,
    text: er,
    is: or,
    index: sr,
    eq: hr,
    append: cr,
    prepend: lr,
    next: ar,
    nextAll: vr,
    prev: yr,
    prevAll: pr,
    parent: wr,
    parents: br,
    closest: kr,
    find: dr,
    children: gr,
    remove: nu,
    add: tu,
    styles: ir
  };
  Object.keys(nt).forEach(function (n) {
    t.fn[n] = t.fn[n] || nt[n]
  });
  var n = {
      deleteProps: function (n) {
        var t = n;
        Object.keys(t).forEach(function (n) {
          try {
            t[n] = null
          } catch (i) {}
          try {
            delete t[n]
          } catch (i) {}
        })
      },
      nextTick: function (n, t) {
        return t === void 0 && (t = 0), setTimeout(n, t)
      },
      now: function () {
        return Date.now()
      },
      getTranslate: function (n, t) {
        t === void 0 && (t = "x");
        var f, r, e, u = i.getComputedStyle(n, null);
        return i.WebKitCSSMatrix ? (r = u.transform || u.webkitTransform, r.split(",").length > 6 && (r = r.split(", ").map(function (n) {
          return n.replace(",", ".")
        }).join(", ")), e = new i.WebKitCSSMatrix(r === "none" ? "" : r)) : (e = u.MozTransform || u.OTransform || u.MsTransform || u.msTransform || u.transform || u.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,"), f = e.toString().split(",")), t === "x" && (r = i.WebKitCSSMatrix ? e.m41 : f.length === 16 ? parseFloat(f[12]) : parseFloat(f[4])), t === "y" && (r = i.WebKitCSSMatrix ? e.m42 : f.length === 16 ? parseFloat(f[13]) : parseFloat(f[5])), r || 0
      },
      parseUrlQuery: function (n) {
        var e = {},
          t = n || i.location.href,
          r, f, u, o;
        if (typeof t == "string" && t.length)
          for (t = t.indexOf("?") > -1 ? t.replace(/\S*\?/, "") : "", f = t.split("&").filter(function (n) {
              return n !== ""
            }), o = f.length, r = 0; r < o; r += 1) u = f[r].replace(/#\S+/g, "").split("="), e[decodeURIComponent(u[0])] = typeof u[1] == "undefined" ? undefined : decodeURIComponent(u[1]) || "";
        return e
      },
      isObject: function (n) {
        return typeof n == "object" && n !== null && n.constructor && n.constructor === Object
      },
      extend: function () {
        for (var u = [], o = arguments.length, r, f, i, s, e, c, t, h; o--;) u[o] = arguments[o];
        for (r = Object(u[0]), f = 1; f < u.length; f += 1)
          if (i = u[f], i !== undefined && i !== null)
            for (s = Object.keys(Object(i)), e = 0, c = s.length; e < c; e += 1) t = s[e], h = Object.getOwnPropertyDescriptor(i, t), h !== undefined && h.enumerable && (n.isObject(r[t]) && n.isObject(i[t]) ? n.extend(r[t], i[t]) : !n.isObject(r[t]) && n.isObject(i[t]) ? (r[t] = {}, n.extend(r[t], i[t])) : r[t] = i[t]);
        return r
      }
    },
    r = function () {
      var n = u.createElement("div");
      return {
        touch: i.Modernizr && i.Modernizr.touch === !0 || function () {
          return !!(i.navigator.maxTouchPoints > 0 || "ontouchstart" in i || i.DocumentTouch && u instanceof i.DocumentTouch)
        }(),
        pointerEvents: !!(i.navigator.pointerEnabled || i.PointerEvent || "maxTouchPoints" in i.navigator && i.navigator.maxTouchPoints > 0),
        prefixedPointerEvents: !!i.navigator.msPointerEnabled,
        transition: function () {
          var t = n.style;
          return "transition" in t || "webkitTransition" in t || "MozTransition" in t
        }(),
        transforms3d: i.Modernizr && i.Modernizr.csstransforms3d === !0 || function () {
          var t = n.style;
          return "webkitPerspective" in t || "MozPerspective" in t || "OPerspective" in t || "MsPerspective" in t || "perspective" in t
        }(),
        flexbox: function () {
          for (var r = n.style, i = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), t = 0; t < i.length; t += 1)
            if (i[t] in r) return !0;
          return !1
        }(),
        observer: function () {
          return "MutationObserver" in i || "WebkitMutationObserver" in i
        }(),
        passiveListener: function () {
          var n = !1,
            t;
          try {
            t = Object.defineProperty({}, "passive", {
              get: function () {
                n = !0
              }
            });
            i.addEventListener("testPassiveListener", null, t)
          } catch (u) {}
          return n
        }(),
        gestures: function () {
          return "ongesturestart" in i
        }()
      }
    }(),
    h = function () {
      function n() {
        var n = i.navigator.userAgent.toLowerCase();
        return n.indexOf("safari") >= 0 && n.indexOf("chrome") < 0 && n.indexOf("android") < 0
      }
      return {
        isIE: !!i.navigator.userAgent.match(/Trident/g) || !!i.navigator.userAgent.match(/MSIE/g),
        isEdge: !!i.navigator.userAgent.match(/Edge/g),
        isSafari: n(),
        isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(i.navigator.userAgent)
      }
    }(),
    c = function (n) {
      n === void 0 && (n = {});
      var t = this;
      t.params = n;
      t.eventsListeners = {};
      t.params && t.params.on && Object.keys(t.params.on).forEach(function (n) {
        t.on(n, t.params.on[n])
      })
    },
    st = {
      components: {
        configurable: !0
      }
    };
  c.prototype.on = function (n, t, i) {
    var r = this,
      u;
    return typeof t != "function" ? r : (u = i ? "unshift" : "push", n.split(" ").forEach(function (n) {
      r.eventsListeners[n] || (r.eventsListeners[n] = []);
      r.eventsListeners[n][u](t)
    }), r)
  };
  c.prototype.once = function (n, t, i) {
    function r() {
      for (var f = [], i = arguments.length; i--;) f[i] = arguments[i];
      t.apply(u, f);
      u.off(n, r);
      r.f7proxy && delete r.f7proxy
    }
    var u = this;
    if (typeof t != "function") return u;
    r.f7proxy = t;
    return u.on(n, r, i)
  };
  c.prototype.off = function (n, t) {
    var i = this;
    return i.eventsListeners ? (n.split(" ").forEach(function (n) {
      typeof t == "undefined" ? i.eventsListeners[n] = [] : i.eventsListeners[n] && i.eventsListeners[n].length && i.eventsListeners[n].forEach(function (r, u) {
        (r === t || r.f7proxy && r.f7proxy === t) && i.eventsListeners[n].splice(u, 1)
      })
    }), i) : i
  };
  c.prototype.emit = function () {
    for (var n = [], r = arguments.length, t, i, u, f, e; r--;) n[r] = arguments[r];
    return (t = this, !t.eventsListeners) ? t : (typeof n[0] == "string" || Array.isArray(n[0]) ? (i = n[0], u = n.slice(1, n.length), f = t) : (i = n[0].events, u = n[0].data, f = n[0].context || t), e = Array.isArray(i) ? i : i.split(" "), e.forEach(function (n) {
      if (t.eventsListeners && t.eventsListeners[n]) {
        var i = [];
        t.eventsListeners[n].forEach(function (n) {
          i.push(n)
        });
        i.forEach(function (n) {
          n.apply(f, u)
        })
      }
    }), t)
  };
  c.prototype.useModulesParams = function (t) {
    var i = this;
    i.modules && Object.keys(i.modules).forEach(function (r) {
      var u = i.modules[r];
      u.params && n.extend(t, u.params)
    })
  };
  c.prototype.useModules = function (n) {
    n === void 0 && (n = {});
    var t = this;
    t.modules && Object.keys(t.modules).forEach(function (i) {
      var r = t.modules[i],
        u = n[i] || {};
      r.instance && Object.keys(r.instance).forEach(function (n) {
        var i = r.instance[n];
        t[n] = typeof i == "function" ? i.bind(t) : i
      });
      r.on && t.on && Object.keys(r.on).forEach(function (n) {
        t.on(n, r.on[n])
      });
      r.create && r.create.bind(t)(u)
    })
  };
  st.components.set = function (n) {
    var t = this;
    t.use && t.use(n)
  };
  c.installModule = function (t) {
    for (var u = [], r = arguments.length - 1, i, f; r-- > 0;) u[r] = arguments[r + 1];
    return i = this, i.prototype.modules || (i.prototype.modules = {}), f = t.name || Object.keys(i.prototype.modules).length + "_" + n.now(), i.prototype.modules[f] = t, t.proto && Object.keys(t.proto).forEach(function (n) {
      i.prototype[n] = t.proto[n]
    }), t.static && Object.keys(t.static).forEach(function (n) {
      i[n] = t.static[n]
    }), t.install && t.install.apply(i, u), i
  };
  c.use = function (n) {
    for (var r = [], i = arguments.length - 1, t; i-- > 0;) r[i] = arguments[i + 1];
    return (t = this, Array.isArray(n)) ? (n.forEach(function (n) {
      return t.installModule(n)
    }), t) : t.installModule.apply(t, [n].concat(r))
  };
  Object.defineProperties(c, st);
  ht = {
    updateSize: iu,
    updateSlides: ru,
    updateAutoHeight: uu,
    updateSlidesOffset: fu,
    updateSlidesProgress: eu,
    updateProgress: ou,
    updateSlidesClasses: su,
    updateActiveIndex: hu,
    updateClickedSlide: cu
  };
  ct = {
    getTranslate: lu,
    setTranslate: au,
    minTranslate: vu,
    maxTranslate: yu
  };
  lt = {
    setTransition: pu,
    transitionStart: wu,
    transitionEnd: bu
  };
  at = {
    slideTo: ku,
    slideToLoop: du,
    slideNext: gu,
    slidePrev: nf,
    slideReset: tf,
    slideToClosest: rf,
    slideToClickedSlide: uf
  };
  vt = {
    loopCreate: ff,
    loopFix: ef,
    loopDestroy: of
  };
  yt = {
    setGrabCursor: sf,
    unsetGrabCursor: hf
  };
  pt = {
    appendSlide: cf,
    prependSlide: lf,
    addSlide: af,
    removeSlide: vf,
    removeAllSlides: yf
  };
  e = function () {
    var t = i.navigator.userAgent,
      n = {
        ios: !1,
        android: !1,
        androidChrome: !1,
        desktop: !1,
        windows: !1,
        iphone: !1,
        ipod: !1,
        ipad: !1,
        cordova: i.cordova || i.phonegap,
        phonegap: i.cordova || i.phonegap
      },
      s = t.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
      c = t.match(/(Android);?[\s\/]+([\d.]+)?/),
      f = t.match(/(iPad).*OS\s([\d_]+)/),
      r = t.match(/(iPod)(.*OS\s([\d_]+))?/),
      e = !f && t.match(/(iPhone\sOS|iOS)\s([\d_]+)/),
      o, h;
    return s && (n.os = "windows", n.osVersion = s[2], n.windows = !0), c && !s && (n.os = "android", n.osVersion = c[2], n.android = !0, n.androidChrome = t.toLowerCase().indexOf("chrome") >= 0), (f || e || r) && (n.os = "ios", n.ios = !0), e && !r && (n.osVersion = e[2].replace(/_/g, "."), n.iphone = !0), f && (n.osVersion = f[2].replace(/_/g, "."), n.ipad = !0), r && (n.osVersion = r[3] ? r[3].replace(/_/g, ".") : null, n.iphone = !0), n.ios && n.osVersion && t.indexOf("Version/") >= 0 && n.osVersion.split(".")[0] === "10" && (n.osVersion = t.toLowerCase().split("version/")[1].split(" ")[0]), n.desktop = !(n.os || n.android || n.webView), n.webView = (e || f || r) && t.match(/.*AppleWebKit(?!.*Safari)/i), n.os && n.os === "ios" && (o = n.osVersion.split("."), h = u.querySelector('meta[name="viewport"]'), n.minimalUi = !n.webView && (r || e) && (o[0] * 1 == 7 ? o[1] * 1 >= 1 : o[0] * 1 > 7) && h && h.getAttribute("content").indexOf("minimal-ui") >= 0), n.pixelRatio = i.devicePixelRatio || 1, n
  }();
  tt = !1;
  bt = {
    attachEvents: gf,
    detachEvents: ne
  };
  kt = {
    setBreakpoint: te,
    getBreakpoint: ie
  };
  dt = {
    addClasses: re,
    removeClasses: ue
  };
  gt = {
    loadImage: fe,
    preloadImages: ee
  };
  var se = {
      checkOverflow: oe
    },
    ni = {
      init: !0,
      direction: "horizontal",
      touchEventsTarget: "container",
      initialSlide: 0,
      speed: 300,
      preventInteractionOnTransition: !1,
      edgeSwipeDetection: !1,
      edgeSwipeThreshold: 20,
      freeMode: !1,
      freeModeMomentum: !0,
      freeModeMomentumRatio: 1,
      freeModeMomentumBounce: !0,
      freeModeMomentumBounceRatio: 1,
      freeModeMomentumVelocityRatio: 1,
      freeModeSticky: !1,
      freeModeMinimumVelocity: .02,
      autoHeight: !1,
      setWrapperSize: !1,
      virtualTranslate: !1,
      effect: "slide",
      breakpoints: undefined,
      breakpointsInverse: !1,
      spaceBetween: 0,
      slidesPerView: 1,
      slidesPerColumn: 1,
      slidesPerColumnFill: "column",
      slidesPerGroup: 1,
      centeredSlides: !1,
      slidesOffsetBefore: 0,
      slidesOffsetAfter: 0,
      normalizeSlideIndex: !0,
      centerInsufficientSlides: !1,
      watchOverflow: !1,
      roundLengths: !1,
      touchRatio: 1,
      touchAngle: 45,
      simulateTouch: !0,
      shortSwipes: !0,
      longSwipes: !0,
      longSwipesRatio: .5,
      longSwipesMs: 300,
      followFinger: !0,
      allowTouchMove: !0,
      threshold: 0,
      touchMoveStopPropagation: !0,
      touchStartPreventDefault: !0,
      touchStartForcePreventDefault: !1,
      touchReleaseOnEdges: !1,
      uniqueNavElements: !0,
      resistance: !0,
      resistanceRatio: .85,
      watchSlidesProgress: !1,
      watchSlidesVisibility: !1,
      grabCursor: !1,
      preventClicks: !0,
      preventClicksPropagation: !0,
      slideToClickedSlide: !1,
      preloadImages: !0,
      updateOnImagesReady: !0,
      loop: !1,
      loopAdditionalSlides: 0,
      loopedSlides: null,
      loopFillGroupWithBlank: !1,
      allowSlidePrev: !0,
      allowSlideNext: !0,
      swipeHandler: null,
      noSwiping: !0,
      noSwipingClass: "swiper-no-swiping",
      noSwipingSelector: null,
      passiveListeners: !0,
      containerModifierClass: "swiper-container-",
      slideClass: "swiper-slide",
      slideBlankClass: "swiper-slide-invisible-blank",
      slideActiveClass: "swiper-slide-active",
      slideDuplicateActiveClass: "swiper-slide-duplicate-active",
      slideVisibleClass: "swiper-slide-visible",
      slideDuplicateClass: "swiper-slide-duplicate",
      slideNextClass: "swiper-slide-next",
      slideDuplicateNextClass: "swiper-slide-duplicate-next",
      slidePrevClass: "swiper-slide-prev",
      slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
      wrapperClass: "swiper-wrapper",
      runCallbacksOnInit: !0
    },
    it = {
      update: ht,
      translate: ct,
      transition: lt,
      slide: at,
      loop: vt,
      grabCursor: yt,
      manipulation: pt,
      events: bt,
      breakpoints: kt,
      checkOverflow: se,
      classes: dt,
      images: gt
    },
    rt = {},
    s = function (i) {
      function u() {
        for (var l, h = [], a = arguments.length, o, e, f, v, s, y, c; a--;) h[a] = arguments[a];
        return (h.length === 1 && h[0].constructor && h[0].constructor === Object ? e = h[0] : (l = h, o = l[0], e = l[1]), e || (e = {}), e = n.extend({}, e), o && !e.el && (e.el = o), i.call(this, e), Object.keys(it).forEach(function (n) {
          Object.keys(it[n]).forEach(function (t) {
            u.prototype[t] || (u.prototype[t] = it[n][t])
          })
        }), f = this, typeof f.modules == "undefined" && (f.modules = {}), Object.keys(f.modules).forEach(function (n) {
          var r = f.modules[n],
            t, i;
          if (r.params) {
            if (t = Object.keys(r.params)[0], i = r.params[t], typeof i != "object" || i === null) return;
            if (!(t in e && "enabled" in i)) return;
            e[t] === !0 && (e[t] = {
              enabled: !0
            });
            typeof e[t] != "object" || "enabled" in e[t] || (e[t].enabled = !0);
            e[t] || (e[t] = {
              enabled: !1
            })
          }
        }), v = n.extend({}, ni), f.useModulesParams(v), f.params = n.extend({}, v, rt, e), f.originalParams = n.extend({}, f.params), f.passedParams = n.extend({}, e), f.$ = t, s = t(f.params.el), o = s[0], !o) ? undefined : s.length > 1 ? (y = [], s.each(function (t, i) {
          var r = n.extend({}, e, {
            el: i
          });
          y.push(new u(r))
        }), y) : (o.swiper = f, s.data("swiper", f), c = s.children("." + f.params.wrapperClass), n.extend(f, {
          $el: s,
          el: o,
          $wrapperEl: c,
          wrapperEl: c[0],
          classNames: [],
          slides: t(),
          slidesGrid: [],
          snapGrid: [],
          slidesSizesGrid: [],
          isHorizontal: function () {
            return f.params.direction === "horizontal"
          },
          isVertical: function () {
            return f.params.direction === "vertical"
          },
          rtl: o.dir.toLowerCase() === "rtl" || s.css("direction") === "rtl",
          rtlTranslate: f.params.direction === "horizontal" && (o.dir.toLowerCase() === "rtl" || s.css("direction") === "rtl"),
          wrongRTL: c.css("display") === "-webkit-box",
          activeIndex: 0,
          realIndex: 0,
          isBeginning: !0,
          isEnd: !1,
          translate: 0,
          previousTranslate: 0,
          progress: 0,
          velocity: 0,
          animating: !1,
          allowSlideNext: f.params.allowSlideNext,
          allowSlidePrev: f.params.allowSlidePrev,
          touchEvents: function () {
            var t = ["touchstart", "touchmove", "touchend"],
              n = ["mousedown", "mousemove", "mouseup"];
            return r.pointerEvents ? n = ["pointerdown", "pointermove", "pointerup"] : r.prefixedPointerEvents && (n = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), f.touchEventsTouch = {
              start: t[0],
              move: t[1],
              end: t[2]
            }, f.touchEventsDesktop = {
              start: n[0],
              move: n[1],
              end: n[2]
            }, r.touch || !f.params.simulateTouch ? f.touchEventsTouch : f.touchEventsDesktop
          }(),
          touchEventsData: {
            isTouched: undefined,
            isMoved: undefined,
            allowTouchCallbacks: undefined,
            touchStartTime: undefined,
            isScrolling: undefined,
            currentTranslate: undefined,
            startTranslate: undefined,
            allowThresholdMove: undefined,
            formElements: "input, select, option, textarea, button, video",
            lastClickTime: n.now(),
            clickTimeout: undefined,
            velocities: [],
            allowMomentumBounce: undefined,
            isTouchEvent: undefined,
            startMoving: undefined
          },
          allowClick: !0,
          allowTouchMove: f.params.allowTouchMove,
          touches: {
            startX: 0,
            startY: 0,
            currentX: 0,
            currentY: 0,
            diff: 0
          },
          imagesToLoad: [],
          imagesLoaded: 0
        }), f.useModules(), f.params.init && f.init(), f)
      }
      i && (u.__proto__ = i);
      u.prototype = Object.create(i && i.prototype);
      u.prototype.constructor = u;
      var f = {
        extendedDefaults: {
          configurable: !0
        },
        defaults: {
          configurable: !0
        },
        Class: {
          configurable: !0
        },
        $: {
          configurable: !0
        }
      };
      return u.prototype.slidesPerViewDynamic = function () {
        var t = this,
          l = t.params,
          n = t.slides,
          c = t.slidesGrid,
          h = t.size,
          i = t.activeIndex,
          e = 1,
          r, o, u, f, s;
        if (l.centeredSlides) {
          for (r = n[i].swiperSlideSize, u = i + 1; u < n.length; u += 1) n[u] && !o && (r += n[u].swiperSlideSize, e += 1, r > h && (o = !0));
          for (f = i - 1; f >= 0; f -= 1) n[f] && !o && (r += n[f].swiperSlideSize, e += 1, r > h && (o = !0))
        } else
          for (s = i + 1; s < n.length; s += 1) c[s] - c[i] < h && (e += 1);
        return e
      }, u.prototype.update = function () {
        function r() {
          var t = n.rtlTranslate ? n.translate * -1 : n.translate,
            i = Math.min(Math.max(t, n.maxTranslate()), n.minTranslate());
          n.setTranslate(i);
          n.updateActiveIndex();
          n.updateSlidesClasses()
        }
        var n = this,
          i, t, u;
        n && !n.destroyed && (i = n.snapGrid, t = n.params, t.breakpoints && n.setBreakpoint(), n.updateSize(), n.updateSlides(), n.updateProgress(), n.updateSlidesClasses(), n.params.freeMode ? (r(), n.params.autoHeight && n.updateAutoHeight()) : (u = (n.params.slidesPerView === "auto" || n.params.slidesPerView > 1) && n.isEnd && !n.params.centeredSlides ? n.slideTo(n.slides.length - 1, 0, !1, !0) : n.slideTo(n.activeIndex, 0, !1, !0), u || r()), t.watchOverflow && i !== n.snapGrid && n.checkOverflow(), n.emit("update"))
      }, u.prototype.changeDirection = function (n, t) {
        t === void 0 && (t = !0);
        var i = this,
          u = i.params.direction;
        return (n || (n = u === "horizontal" ? "vertical" : "horizontal"), n === u || n !== "horizontal" && n !== "vertical") ? i : (i.$el.removeClass("" + i.params.containerModifierClass + u + " wp8-" + u).addClass("" + i.params.containerModifierClass + n), (h.isIE || h.isEdge) && (r.pointerEvents || r.prefixedPointerEvents) && i.$el.addClass(i.params.containerModifierClass + "wp8-" + n), i.params.direction = n, i.slides.each(function (t, i) {
          n === "vertical" ? i.style.width = "" : i.style.height = ""
        }), i.emit("changeDirection"), t && i.update(), i)
      }, u.prototype.init = function () {
        var n = this;
        n.initialized || (n.emit("beforeInit"), n.params.breakpoints && n.setBreakpoint(), n.addClasses(), n.params.loop && n.loopCreate(), n.updateSize(), n.updateSlides(), n.params.watchOverflow && n.checkOverflow(), n.params.grabCursor && n.setGrabCursor(), n.params.preloadImages && n.preloadImages(), n.params.loop ? n.slideTo(n.params.initialSlide + n.loopedSlides, 0, n.params.runCallbacksOnInit) : n.slideTo(n.params.initialSlide, 0, n.params.runCallbacksOnInit), n.attachEvents(), n.initialized = !0, n.emit("init"))
      }, u.prototype.destroy = function (t, i) {
        t === void 0 && (t = !0);
        i === void 0 && (i = !0);
        var r = this,
          u = r.params,
          e = r.$el,
          o = r.$wrapperEl,
          f = r.slides;
        return typeof r.params == "undefined" || r.destroyed ? null : (r.emit("beforeDestroy"), r.initialized = !1, r.detachEvents(), u.loop && r.loopDestroy(), i && (r.removeClasses(), e.removeAttr("style"), o.removeAttr("style"), f && f.length && f.removeClass([u.slideVisibleClass, u.slideActiveClass, u.slideNextClass, u.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), r.emit("destroy"), Object.keys(r.eventsListeners).forEach(function (n) {
          r.off(n)
        }), t !== !1 && (r.$el[0].swiper = null, r.$el.data("swiper", null), n.deleteProps(r)), r.destroyed = !0, null)
      }, u.extendDefaults = function (t) {
        n.extend(rt, t)
      }, f.extendedDefaults.get = function () {
        return rt
      }, f.defaults.get = function () {
        return ni
      }, f.Class.get = function () {
        return i
      }, f.$.get = function () {
        return t
      }, Object.defineProperties(u, f), u
    }(c),
    he = {
      name: "device",
      proto: {
        device: e
      },
      "static": {
        device: e
      }
    },
    ce = {
      name: "support",
      proto: {
        support: r
      },
      "static": {
        support: r
      }
    },
    le = {
      name: "browser",
      proto: {
        browser: h
      },
      "static": {
        browser: h
      }
    },
    ae = {
      name: "resize",
      create: function () {
        var t = this;
        n.extend(t, {
          resize: {
            resizeHandler: function () {
              t && !t.destroyed && t.initialized && (t.emit("beforeResize"), t.emit("resize"))
            },
            orientationChangeHandler: function () {
              t && !t.destroyed && t.initialized && t.emit("orientationchange")
            }
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          i.addEventListener("resize", n.resize.resizeHandler);
          i.addEventListener("orientationchange", n.resize.orientationChangeHandler)
        },
        destroy: function () {
          var n = this;
          i.removeEventListener("resize", n.resize.resizeHandler);
          i.removeEventListener("orientationchange", n.resize.orientationChangeHandler)
        }
      }
    },
    w = {
      func: i.MutationObserver || i.WebkitMutationObserver,
      attach: function (n, t) {
        t === void 0 && (t = {});
        var r = this,
          f = w.func,
          u = new f(function (n) {
            if (n.length === 1) {
              r.emit("observerUpdate", n[0]);
              return
            }
            var t = function () {
              r.emit("observerUpdate", n[0])
            };
            i.requestAnimationFrame ? i.requestAnimationFrame(t) : i.setTimeout(t, 0)
          });
        u.observe(n, {
          attributes: typeof t.attributes == "undefined" ? !0 : t.attributes,
          childList: typeof t.childList == "undefined" ? !0 : t.childList,
          characterData: typeof t.characterData == "undefined" ? !0 : t.characterData
        });
        r.observer.observers.push(u)
      },
      init: function () {
        var n = this,
          i, t;
        if (r.observer && n.params.observer) {
          if (n.params.observeParents)
            for (i = n.$el.parents(), t = 0; t < i.length; t += 1) n.observer.attach(i[t]);
          n.observer.attach(n.$el[0], {
            childList: n.params.observeSlideChildren
          });
          n.observer.attach(n.$wrapperEl[0], {
            attributes: !1
          })
        }
      },
      destroy: function () {
        var n = this;
        n.observer.observers.forEach(function (n) {
          n.disconnect()
        });
        n.observer.observers = []
      }
    },
    ve = {
      name: "observer",
      params: {
        observer: !1,
        observeParents: !1,
        observeSlideChildren: !1
      },
      create: function () {
        var t = this;
        n.extend(t, {
          observer: {
            init: w.init.bind(t),
            attach: w.attach.bind(t),
            destroy: w.destroy.bind(t),
            observers: []
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.observer.init()
        },
        destroy: function () {
          var n = this;
          n.observer.destroy()
        }
      }
    },
    v = {
      update: function (t) {
        function ut() {
          i.updateSlides();
          i.updateProgress();
          i.updateSlidesClasses();
          i.lazy && i.params.lazy.enabled && i.lazy.load()
        }
        var i = this,
          v = i.params,
          y = v.slidesPerView,
          c = v.slidesPerGroup,
          ft = v.centeredSlides,
          nt = i.params.virtual,
          tt = nt.addSlidesBefore,
          it = nt.addSlidesAfter,
          f = i.virtual,
          p = f.from,
          l = f.to,
          s = f.slides,
          et = f.slidesGrid,
          rt = f.renderSlide,
          ot = f.offset,
          w, b, k, d, g, a, o, r;
        i.updateActiveIndex();
        w = i.activeIndex || 0;
        b = i.rtlTranslate ? "right" : i.isHorizontal() ? "left" : "top";
        ft ? (k = Math.floor(y / 2) + c + tt, d = Math.floor(y / 2) + c + it) : (k = y + (c - 1) + tt, d = c + it);
        var u = Math.max((w || 0) - d, 0),
          e = Math.min((w || 0) + k, s.length - 1),
          h = (i.slidesGrid[u] || 0) - (i.slidesGrid[0] || 0);
        if (n.extend(i.virtual, {
            from: u,
            to: e,
            offset: h,
            slidesGrid: i.slidesGrid
          }), p === u && l === e && !t) {
          i.slidesGrid !== et && h !== ot && i.slides.css(b, h + "px");
          i.updateProgress();
          return
        }
        if (i.params.virtual.renderExternal) {
          i.params.virtual.renderExternal.call(i, {
            offset: h,
            from: u,
            to: e,
            slides: function () {
              for (var t = [], n = u; n <= e; n += 1) t.push(s[n]);
              return t
            }()
          });
          ut();
          return
        }
        if (g = [], a = [], t) i.$wrapperEl.find("." + i.params.slideClass).remove();
        else
          for (o = p; o <= l; o += 1)(o < u || o > e) && i.$wrapperEl.find("." + i.params.slideClass + '[data-swiper-slide-index="' + o + '"]').remove();
        for (r = 0; r < s.length; r += 1) r >= u && r <= e && (typeof l == "undefined" || t ? a.push(r) : (r > l && a.push(r), r < p && g.push(r)));
        a.forEach(function (n) {
          i.$wrapperEl.append(rt(s[n], n))
        });
        g.sort(function (n, t) {
          return t - n
        }).forEach(function (n) {
          i.$wrapperEl.prepend(rt(s[n], n))
        });
        i.$wrapperEl.children(".swiper-slide").css(b, h + "px");
        ut()
      },
      renderSlide: function (n, i) {
        var r = this,
          f = r.params.virtual,
          u;
        return f.cache && r.virtual.cache[i] ? r.virtual.cache[i] : (u = f.renderSlide ? t(f.renderSlide.call(r, n, i)) : t('<div class="' + r.params.slideClass + '" data-swiper-slide-index="' + i + '">' + n + "<\/div>"), u.attr("data-swiper-slide-index") || u.attr("data-swiper-slide-index", i), f.cache && (r.virtual.cache[i] = u), u)
      },
      appendSlide: function (n) {
        var i = this,
          t;
        if (typeof n == "object" && "length" in n)
          for (t = 0; t < n.length; t += 1) n[t] && i.virtual.slides.push(n[t]);
        else i.virtual.slides.push(n);
        i.virtual.update(!0)
      },
      prependSlide: function (n) {
        var t = this,
          f = t.activeIndex,
          e = f + 1,
          o = 1,
          i, r, u;
        if (Array.isArray(n)) {
          for (i = 0; i < n.length; i += 1) n[i] && t.virtual.slides.unshift(n[i]);
          e = f + n.length;
          o = n.length
        } else t.virtual.slides.unshift(n);
        t.params.virtual.cache && (r = t.virtual.cache, u = {}, Object.keys(r).forEach(function (n) {
          u[parseInt(n, 10) + o] = r[n]
        }), t.virtual.cache = u);
        t.virtual.update(!0);
        t.slideTo(e, 0)
      },
      removeSlide: function (n) {
        var i = this,
          t, r;
        if (typeof n != "undefined" && n !== null) {
          if (t = i.activeIndex, Array.isArray(n))
            for (r = n.length - 1; r >= 0; r -= 1) i.virtual.slides.splice(n[r], 1), i.params.virtual.cache && delete i.virtual.cache[n[r]], n[r] < t && (t -= 1), t = Math.max(t, 0);
          else i.virtual.slides.splice(n, 1), i.params.virtual.cache && delete i.virtual.cache[n], n < t && (t -= 1), t = Math.max(t, 0);
          i.virtual.update(!0);
          i.slideTo(t, 0)
        }
      },
      removeAllSlides: function () {
        var n = this;
        n.virtual.slides = [];
        n.params.virtual.cache && (n.virtual.cache = {});
        n.virtual.update(!0);
        n.slideTo(0, 0)
      }
    },
    ye = {
      name: "virtual",
      params: {
        virtual: {
          enabled: !1,
          slides: [],
          cache: !0,
          renderSlide: null,
          renderExternal: null,
          addSlidesBefore: 0,
          addSlidesAfter: 0
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          virtual: {
            update: v.update.bind(t),
            appendSlide: v.appendSlide.bind(t),
            prependSlide: v.prependSlide.bind(t),
            removeSlide: v.removeSlide.bind(t),
            removeAllSlides: v.removeAllSlides.bind(t),
            renderSlide: v.renderSlide.bind(t),
            slides: t.params.virtual.slides,
            cache: {}
          }
        })
      },
      on: {
        beforeInit: function () {
          var t = this,
            i;
          t.params.virtual.enabled && (t.classNames.push(t.params.containerModifierClass + "virtual"), i = {
            watchSlidesProgress: !0
          }, n.extend(t.params, i), n.extend(t.originalParams, i), t.params.initialSlide || t.virtual.update())
        },
        setTranslate: function () {
          var n = this;
          n.params.virtual.enabled && n.virtual.update()
        }
      }
    },
    ut = {
      handle: function (n) {
        var r = this,
          o = r.rtlTranslate,
          f = n,
          t, c, l, h, s;
        if ((f.originalEvent && (f = f.originalEvent), t = f.keyCode || f.charCode, !r.allowSlideNext && (r.isHorizontal() && t === 39 || r.isVertical() && t === 40 || t === 34)) || !r.allowSlidePrev && (r.isHorizontal() && t === 37 || r.isVertical() && t === 38 || t === 33)) return !1;
        if (f.shiftKey || f.altKey || f.ctrlKey || f.metaKey || u.activeElement && u.activeElement.nodeName && (u.activeElement.nodeName.toLowerCase() === "input" || u.activeElement.nodeName.toLowerCase() === "textarea")) return undefined;
        if (r.params.keyboard.onlyInViewport && (t === 33 || t === 34 || t === 37 || t === 39 || t === 38 || t === 40)) {
          if (c = !1, r.$el.parents("." + r.params.slideClass).length > 0 && r.$el.parents("." + r.params.slideActiveClass).length === 0) return undefined;
          var a = i.innerWidth,
            v = i.innerHeight,
            e = r.$el.offset();
          for (o && (e.left -= r.$el[0].scrollLeft), l = [
              [e.left, e.top],
              [e.left + r.width, e.top],
              [e.left, e.top + r.height],
              [e.left + r.width, e.top + r.height]
            ], h = 0; h < l.length; h += 1) s = l[h], s[0] >= 0 && s[0] <= a && s[1] >= 0 && s[1] <= v && (c = !0);
          if (!c) return undefined
        }
        return r.isHorizontal() ? ((t === 33 || t === 34 || t === 37 || t === 39) && (f.preventDefault ? f.preventDefault() : f.returnValue = !1), ((t === 34 || t === 39) && !o || (t === 33 || t === 37) && o) && r.slideNext(), ((t === 33 || t === 37) && !o || (t === 34 || t === 39) && o) && r.slidePrev()) : ((t === 33 || t === 34 || t === 38 || t === 40) && (f.preventDefault ? f.preventDefault() : f.returnValue = !1), (t === 34 || t === 40) && r.slideNext(), (t === 33 || t === 38) && r.slidePrev()), r.emit("keyPress", t), undefined
      },
      enable: function () {
        var n = this;
        if (!n.keyboard.enabled) {
          t(u).on("keydown", n.keyboard.handle);
          n.keyboard.enabled = !0
        }
      },
      disable: function () {
        var n = this;
        n.keyboard.enabled && (t(u).off("keydown", n.keyboard.handle), n.keyboard.enabled = !1)
      }
    },
    pe = {
      name: "keyboard",
      params: {
        keyboard: {
          enabled: !1,
          onlyInViewport: !0
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          keyboard: {
            enabled: !1,
            enable: ut.enable.bind(t),
            disable: ut.disable.bind(t),
            handle: ut.handle.bind(t)
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.params.keyboard.enabled && n.keyboard.enable()
        },
        destroy: function () {
          var n = this;
          n.keyboard.enabled && n.keyboard.disable()
        }
      }
    };
  var l = {
      lastScrollTime: n.now(),
      event: function () {
        return i.navigator.userAgent.indexOf("firefox") > -1 ? "DOMMouseScroll" : we() ? "wheel" : "mousewheel"
      }(),
      normalize: function (n) {
        var f = 10,
          e = 40,
          o = 800,
          u = 0,
          t = 0,
          i = 0,
          r = 0;
        return "detail" in n && (t = n.detail), "wheelDelta" in n && (t = -n.wheelDelta / 120), "wheelDeltaY" in n && (t = -n.wheelDeltaY / 120), "wheelDeltaX" in n && (u = -n.wheelDeltaX / 120), "axis" in n && n.axis === n.HORIZONTAL_AXIS && (u = t, t = 0), i = u * f, r = t * f, "deltaY" in n && (r = n.deltaY), "deltaX" in n && (i = n.deltaX), (i || r) && n.deltaMode && (n.deltaMode === 1 ? (i *= e, r *= e) : (i *= o, r *= o)), i && !u && (u = i < 1 ? -1 : 1), r && !t && (t = r < 1 ? -1 : 1), {
          spinX: u,
          spinY: t,
          pixelX: i,
          pixelY: r
        }
      },
      handleMouseEnter: function () {
        var n = this;
        n.mouseEntered = !0
      },
      handleMouseLeave: function () {
        var n = this;
        n.mouseEntered = !1
      },
      handle: function (t) {
        var u = t,
          r = this,
          s = r.params.mousewheel;
        if (!r.mouseEntered && !s.releaseOnEdges) return !0;
        u.originalEvent && (u = u.originalEvent);
        var e = 0,
          h = r.rtlTranslate ? -1 : 1,
          f = l.normalize(u);
        if (s.forceToAxis)
          if (r.isHorizontal())
            if (Math.abs(f.pixelX) > Math.abs(f.pixelY)) e = f.pixelX * h;
            else return !0;
        else if (Math.abs(f.pixelY) > Math.abs(f.pixelX)) e = f.pixelY;
        else return !0;
        else e = Math.abs(f.pixelX) > Math.abs(f.pixelY) ? -f.pixelX * h : -f.pixelY;
        if (e === 0) return !0;
        if (s.invert && (e = -e), r.params.freeMode) {
          r.params.loop && r.loopFix();
          var o = r.getTranslate() + e * s.sensitivity,
            c = r.isBeginning,
            a = r.isEnd;
          if (o >= r.minTranslate() && (o = r.minTranslate()), o <= r.maxTranslate() && (o = r.maxTranslate()), r.setTransition(0), r.setTranslate(o), r.updateProgress(), r.updateActiveIndex(), r.updateSlidesClasses(), (!c && r.isBeginning || !a && r.isEnd) && r.updateSlidesClasses(), r.params.freeModeSticky && (clearTimeout(r.mousewheel.timeout), r.mousewheel.timeout = n.nextTick(function () {
              r.slideToClosest()
            }, 300)), r.emit("scroll", u), r.params.autoplay && r.params.autoplayDisableOnInteraction && r.autoplay.stop(), o === r.minTranslate() || o === r.maxTranslate()) return !0
        } else {
          if (n.now() - r.mousewheel.lastScrollTime > 60)
            if (e < 0)
              if (r.isEnd && !r.params.loop || r.animating) {
                if (s.releaseOnEdges) return !0
              } else r.slideNext(), r.emit("scroll", u);
          else if (r.isBeginning && !r.params.loop || r.animating) {
            if (s.releaseOnEdges) return !0
          } else r.slidePrev(), r.emit("scroll", u);
          r.mousewheel.lastScrollTime = (new i.Date).getTime()
        }
        return u.preventDefault ? u.preventDefault() : u.returnValue = !1, !1
      },
      enable: function () {
        var n = this,
          i;
        if (!l.event || n.mousewheel.enabled) return !1;
        i = n.$el;
        n.params.mousewheel.eventsTarged !== "container" && (i = t(n.params.mousewheel.eventsTarged));
        i.on("mouseenter", n.mousewheel.handleMouseEnter);
        i.on("mouseleave", n.mousewheel.handleMouseLeave);
        i.on(l.event, n.mousewheel.handle);
        return n.mousewheel.enabled = !0, !0
      },
      disable: function () {
        var n = this,
          i;
        return l.event ? n.mousewheel.enabled ? (i = n.$el, n.params.mousewheel.eventsTarged !== "container" && (i = t(n.params.mousewheel.eventsTarged)), i.off(l.event, n.mousewheel.handle), n.mousewheel.enabled = !1, !0) : !1 : !1
      }
    },
    be = {
      name: "mousewheel",
      params: {
        mousewheel: {
          enabled: !1,
          releaseOnEdges: !1,
          invert: !1,
          forceToAxis: !1,
          sensitivity: 1,
          eventsTarged: "container"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          mousewheel: {
            enabled: !1,
            enable: l.enable.bind(t),
            disable: l.disable.bind(t),
            handle: l.handle.bind(t),
            handleMouseEnter: l.handleMouseEnter.bind(t),
            handleMouseLeave: l.handleMouseLeave.bind(t),
            lastScrollTime: n.now()
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.params.mousewheel.enabled && n.mousewheel.enable()
        },
        destroy: function () {
          var n = this;
          n.mousewheel.enabled && n.mousewheel.disable()
        }
      }
    },
    y = {
      update: function () {
        var n = this,
          t = n.params.navigation;
        if (!n.params.loop) {
          var u = n.navigation,
            i = u.$nextEl,
            r = u.$prevEl;
          r && r.length > 0 && (n.isBeginning ? r.addClass(t.disabledClass) : r.removeClass(t.disabledClass), r[n.params.watchOverflow && n.isLocked ? "addClass" : "removeClass"](t.lockClass));
          i && i.length > 0 && (n.isEnd ? i.addClass(t.disabledClass) : i.removeClass(t.disabledClass), i[n.params.watchOverflow && n.isLocked ? "addClass" : "removeClass"](t.lockClass))
        }
      },
      onPrevClick: function (n) {
        var t = this;
        (n.preventDefault(), !t.isBeginning || t.params.loop) && t.slidePrev()
      },
      onNextClick: function (n) {
        var t = this;
        (n.preventDefault(), !t.isEnd || t.params.loop) && t.slideNext()
      },
      init: function () {
        var r = this,
          i = r.params.navigation,
          u, f;
        if (i.nextEl || i.prevEl) {
          if (i.nextEl && (u = t(i.nextEl), r.params.uniqueNavElements && typeof i.nextEl == "string" && u.length > 1 && r.$el.find(i.nextEl).length === 1 && (u = r.$el.find(i.nextEl))), i.prevEl && (f = t(i.prevEl), r.params.uniqueNavElements && typeof i.prevEl == "string" && f.length > 1 && r.$el.find(i.prevEl).length === 1 && (f = r.$el.find(i.prevEl))), u && u.length > 0) u.on("click", r.navigation.onNextClick);
          if (f && f.length > 0) f.on("click", r.navigation.onPrevClick);
          n.extend(r.navigation, {
            $nextEl: u,
            nextEl: u && u[0],
            $prevEl: f,
            prevEl: f && f[0]
          })
        }
      },
      destroy: function () {
        var n = this,
          r = n.navigation,
          t = r.$nextEl,
          i = r.$prevEl;
        t && t.length && (t.off("click", n.navigation.onNextClick), t.removeClass(n.params.navigation.disabledClass));
        i && i.length && (i.off("click", n.navigation.onPrevClick), i.removeClass(n.params.navigation.disabledClass))
      }
    },
    ke = {
      name: "navigation",
      params: {
        navigation: {
          nextEl: null,
          prevEl: null,
          hideOnClick: !1,
          disabledClass: "swiper-button-disabled",
          hiddenClass: "swiper-button-hidden",
          lockClass: "swiper-button-lock"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          navigation: {
            init: y.init.bind(t),
            update: y.update.bind(t),
            destroy: y.destroy.bind(t),
            onNextClick: y.onNextClick.bind(t),
            onPrevClick: y.onPrevClick.bind(t)
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.navigation.init();
          n.navigation.update()
        },
        toEdge: function () {
          var n = this;
          n.navigation.update()
        },
        fromEdge: function () {
          var n = this;
          n.navigation.update()
        },
        destroy: function () {
          var n = this;
          n.navigation.destroy()
        },
        click: function (n) {
          var i = this,
            e = i.navigation,
            r = e.$nextEl,
            u = e.$prevEl,
            f;
          !i.params.navigation.hideOnClick || t(n.target).is(u) || t(n.target).is(r) || (r ? f = r.hasClass(i.params.navigation.hiddenClass) : u && (f = u.hasClass(i.params.navigation.hiddenClass)), f === !0 ? i.emit("navigationShow", i) : i.emit("navigationHide", i), r && r.toggleClass(i.params.navigation.hiddenClass), u && u.toggleClass(i.params.navigation.hiddenClass))
        }
      }
    },
    b = {
      update: function () {
        var n = this,
          d = n.rtl,
          i = n.params.pagination,
          u, e, s, l, a, v, y, h, p;
        if (i.el && n.pagination.el && n.pagination.$el && n.pagination.$el.length !== 0) {
          var c = n.virtual && n.params.virtual.enabled ? n.virtual.slides.length : n.slides.length,
            f = n.pagination.$el,
            r, o = n.params.loop ? Math.ceil((c - n.loopedSlides * 2) / n.params.slidesPerGroup) : n.snapGrid.length;
          if (n.params.loop ? (r = Math.ceil((n.activeIndex - n.loopedSlides) / n.params.slidesPerGroup), r > c - 1 - n.loopedSlides * 2 && (r -= c - n.loopedSlides * 2), r > o - 1 && (r -= o), r < 0 && n.params.paginationType !== "bullets" && (r = o + r)) : r = typeof n.snapIndex != "undefined" ? n.snapIndex : n.activeIndex || 0, i.type === "bullets" && n.pagination.bullets && n.pagination.bullets.length > 0) {
            if (u = n.pagination.bullets, i.dynamicBullets && (n.pagination.bulletSize = u.eq(0)[n.isHorizontal() ? "outerWidth" : "outerHeight"](!0), f.css(n.isHorizontal() ? "width" : "height", n.pagination.bulletSize * (i.dynamicMainBullets + 4) + "px"), i.dynamicMainBullets > 1 && n.previousIndex !== undefined && (n.pagination.dynamicBulletIndex += r - n.previousIndex, n.pagination.dynamicBulletIndex > i.dynamicMainBullets - 1 ? n.pagination.dynamicBulletIndex = i.dynamicMainBullets - 1 : n.pagination.dynamicBulletIndex < 0 && (n.pagination.dynamicBulletIndex = 0)), e = r - n.pagination.dynamicBulletIndex, s = e + (Math.min(u.length, i.dynamicMainBullets) - 1), l = (s + e) / 2), u.removeClass(i.bulletActiveClass + " " + i.bulletActiveClass + "-next " + i.bulletActiveClass + "-next-next " + i.bulletActiveClass + "-prev " + i.bulletActiveClass + "-prev-prev " + i.bulletActiveClass + "-main"), f.length > 1) u.each(function (n, u) {
              var f = t(u),
                o = f.index();
              o === r && f.addClass(i.bulletActiveClass);
              i.dynamicBullets && (o >= e && o <= s && f.addClass(i.bulletActiveClass + "-main"), o === e && f.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"), o === s && f.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next"))
            });
            else if (a = u.eq(r), a.addClass(i.bulletActiveClass), i.dynamicBullets) {
              for (v = u.eq(e), y = u.eq(s), h = e; h <= s; h += 1) u.eq(h).addClass(i.bulletActiveClass + "-main");
              v.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev");
              y.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next")
            }
            if (i.dynamicBullets) {
              var g = Math.min(u.length, i.dynamicMainBullets + 4),
                nt = (n.pagination.bulletSize * g - n.pagination.bulletSize) / 2 - l * n.pagination.bulletSize,
                tt = d ? "right" : "left";
              u.css(n.isHorizontal() ? tt : "top", nt + "px")
            }
          }
          if (i.type === "fraction" && (f.find("." + i.currentClass).text(i.formatFractionCurrent(r + 1)), f.find("." + i.totalClass).text(i.formatFractionTotal(o))), i.type === "progressbar") {
            p = i.progressbarOpposite ? n.isHorizontal() ? "vertical" : "horizontal" : n.isHorizontal() ? "horizontal" : "vertical";
            var w = (r + 1) / o,
              b = 1,
              k = 1;
            p === "horizontal" ? b = w : k = w;
            f.find("." + i.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + b + ") scaleY(" + k + ")").transition(n.params.speed)
          }
          i.type === "custom" && i.renderCustom ? (f.html(i.renderCustom(n, r + 1, o)), n.emit("paginationRender", n, f[0])) : n.emit("paginationUpdate", n, f[0]);
          f[n.params.watchOverflow && n.isLocked ? "addClass" : "removeClass"](i.lockClass)
        }
      },
      render: function () {
        var t = this,
          n = t.params.pagination,
          f, u;
        if (n.el && t.pagination.el && t.pagination.$el && t.pagination.$el.length !== 0) {
          var e = t.virtual && t.params.virtual.enabled ? t.virtual.slides.length : t.slides.length,
            r = t.pagination.$el,
            i = "";
          if (n.type === "bullets") {
            for (f = t.params.loop ? Math.ceil((e - t.loopedSlides * 2) / t.params.slidesPerGroup) : t.snapGrid.length, u = 0; u < f; u += 1) i += n.renderBullet ? n.renderBullet.call(t, u, n.bulletClass) : "<" + n.bulletElement + ' class="' + n.bulletClass + '"><\/' + n.bulletElement + ">";
            r.html(i);
            t.pagination.bullets = r.find("." + n.bulletClass)
          }
          n.type === "fraction" && (i = n.renderFraction ? n.renderFraction.call(t, n.currentClass, n.totalClass) : '<span class="' + n.currentClass + '"><\/span> / <span class="' + n.totalClass + '"><\/span>', r.html(i));
          n.type === "progressbar" && (i = n.renderProgressbar ? n.renderProgressbar.call(t, n.progressbarFillClass) : '<span class="' + n.progressbarFillClass + '"><\/span>', r.html(i));
          n.type !== "custom" && t.emit("paginationRender", t.pagination.$el[0])
        }
      },
      init: function () {
        var u = this,
          i = u.params.pagination,
          r;
        if (i.el && (r = t(i.el), r.length !== 0)) {
          if (u.params.uniqueNavElements && typeof i.el == "string" && r.length > 1 && u.$el.find(i.el).length === 1 && (r = u.$el.find(i.el)), i.type === "bullets" && i.clickable && r.addClass(i.clickableClass), r.addClass(i.modifierClass + i.type), i.type === "bullets" && i.dynamicBullets && (r.addClass("" + i.modifierClass + i.type + "-dynamic"), u.pagination.dynamicBulletIndex = 0, i.dynamicMainBullets < 1 && (i.dynamicMainBullets = 1)), i.type === "progressbar" && i.progressbarOpposite && r.addClass(i.progressbarOppositeClass), i.clickable) r.on("click", "." + i.bulletClass, function (n) {
            n.preventDefault();
            var i = t(this).index() * u.params.slidesPerGroup;
            u.params.loop && (i += u.loopedSlides);
            u.slideTo(i)
          });
          n.extend(u.pagination, {
            $el: r,
            el: r[0]
          })
        }
      },
      destroy: function () {
        var n = this,
          t = n.params.pagination,
          i;
        t.el && n.pagination.el && n.pagination.$el && n.pagination.$el.length !== 0 && (i = n.pagination.$el, i.removeClass(t.hiddenClass), i.removeClass(t.modifierClass + t.type), n.pagination.bullets && n.pagination.bullets.removeClass(t.bulletActiveClass), t.clickable && i.off("click", "." + t.bulletClass))
      }
    },
    de = {
      name: "pagination",
      params: {
        pagination: {
          el: null,
          bulletElement: "span",
          clickable: !1,
          hideOnClick: !1,
          renderBullet: null,
          renderProgressbar: null,
          renderFraction: null,
          renderCustom: null,
          progressbarOpposite: !1,
          type: "bullets",
          dynamicBullets: !1,
          dynamicMainBullets: 1,
          formatFractionCurrent: function (n) {
            return n
          },
          formatFractionTotal: function (n) {
            return n
          },
          bulletClass: "swiper-pagination-bullet",
          bulletActiveClass: "swiper-pagination-bullet-active",
          modifierClass: "swiper-pagination-",
          currentClass: "swiper-pagination-current",
          totalClass: "swiper-pagination-total",
          hiddenClass: "swiper-pagination-hidden",
          progressbarFillClass: "swiper-pagination-progressbar-fill",
          progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
          clickableClass: "swiper-pagination-clickable",
          lockClass: "swiper-pagination-lock"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          pagination: {
            init: b.init.bind(t),
            render: b.render.bind(t),
            update: b.update.bind(t),
            destroy: b.destroy.bind(t),
            dynamicBulletIndex: 0
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.pagination.init();
          n.pagination.render();
          n.pagination.update()
        },
        activeIndexChange: function () {
          var n = this;
          n.params.loop ? n.pagination.update() : typeof n.snapIndex == "undefined" && n.pagination.update()
        },
        snapIndexChange: function () {
          var n = this;
          n.params.loop || n.pagination.update()
        },
        slidesLengthChange: function () {
          var n = this;
          n.params.loop && (n.pagination.render(), n.pagination.update())
        },
        snapGridLengthChange: function () {
          var n = this;
          n.params.loop || (n.pagination.render(), n.pagination.update())
        },
        destroy: function () {
          var n = this;
          n.pagination.destroy()
        },
        click: function (n) {
          var i = this,
            r;
          i.params.pagination.el && i.params.pagination.hideOnClick && i.pagination.$el.length > 0 && !t(n.target).hasClass(i.params.pagination.bulletClass) && (r = i.pagination.$el.hasClass(i.params.pagination.hiddenClass), r === !0 ? i.emit("paginationShow", i) : i.emit("paginationHide", i), i.pagination.$el.toggleClass(i.params.pagination.hiddenClass))
        }
      }
    },
    o = {
      setTranslate: function () {
        var t = this;
        if (t.params.scrollbar.el && t.scrollbar.el) {
          var o = t.scrollbar,
            h = t.rtlTranslate,
            c = t.progress,
            i = o.dragSize,
            e = o.trackSize,
            u = o.$dragEl,
            s = o.$el,
            l = t.params.scrollbar,
            f = i,
            n = (e - i) * c;
          h ? (n = -n, n > 0 ? (f = i - n, n = 0) : -n + i > e && (f = e + n)) : n < 0 ? (f = i + n, n = 0) : n + i > e && (f = e - n);
          t.isHorizontal() ? (r.transforms3d ? u.transform("translate3d(" + n + "px, 0, 0)") : u.transform("translateX(" + n + "px)"), u[0].style.width = f + "px") : (r.transforms3d ? u.transform("translate3d(0px, " + n + "px, 0)") : u.transform("translateY(" + n + "px)"), u[0].style.height = f + "px");
          l.hide && (clearTimeout(t.scrollbar.timeout), s[0].style.opacity = 1, t.scrollbar.timeout = setTimeout(function () {
            s[0].style.opacity = 0;
            s.transition(400)
          }, 1e3))
        }
      },
      setTransition: function (n) {
        var t = this;
        t.params.scrollbar.el && t.scrollbar.el && t.scrollbar.$dragEl.transition(n)
      },
      updateSize: function () {
        var t = this;
        if (t.params.scrollbar.el && t.scrollbar.el) {
          var i = t.scrollbar,
            r = i.$dragEl,
            u = i.$el;
          r[0].style.width = "";
          r[0].style.height = "";
          var o = t.isHorizontal() ? u[0].offsetWidth : u[0].offsetHeight,
            f = t.size / t.virtualSize,
            s = f * (o / t.size),
            e;
          e = t.params.scrollbar.dragSize === "auto" ? o * f : parseInt(t.params.scrollbar.dragSize, 10);
          t.isHorizontal() ? r[0].style.width = e + "px" : r[0].style.height = e + "px";
          u[0].style.display = f >= 1 ? "none" : "";
          t.params.scrollbar.hide && (u[0].style.opacity = 0);
          n.extend(i, {
            trackSize: o,
            divider: f,
            moveDivider: s,
            dragSize: e
          });
          i.$el[t.params.watchOverflow && t.isLocked ? "addClass" : "removeClass"](t.params.scrollbar.lockClass)
        }
      },
      getPointerPosition: function (n) {
        var t = this;
        return t.isHorizontal() ? n.type === "touchstart" || n.type === "touchmove" ? n.targetTouches[0].pageX : n.pageX || n.clientX : n.type === "touchstart" || n.type === "touchmove" ? n.targetTouches[0].pageY : n.pageY || n.clientY
      },
      setDragPosition: function (n) {
        var t = this,
          r = t.scrollbar,
          o = t.rtlTranslate,
          s = r.$el,
          f = r.dragSize,
          h = r.trackSize,
          e = r.dragStartPos,
          i, u;
        i = (r.getPointerPosition(n) - s.offset()[t.isHorizontal() ? "left" : "top"] - (e !== null ? e : f / 2)) / (h - f);
        i = Math.max(Math.min(i, 1), 0);
        o && (i = 1 - i);
        u = t.minTranslate() + (t.maxTranslate() - t.minTranslate()) * i;
        t.updateProgress(u);
        t.setTranslate(u);
        t.updateActiveIndex();
        t.updateSlidesClasses()
      },
      onDragStart: function (n) {
        var t = this,
          f = t.params.scrollbar,
          i = t.scrollbar,
          e = t.$wrapperEl,
          u = i.$el,
          r = i.$dragEl;
        t.scrollbar.isTouched = !0;
        t.scrollbar.dragStartPos = n.target === r[0] || n.target === r ? i.getPointerPosition(n) - n.target.getBoundingClientRect()[t.isHorizontal() ? "left" : "top"] : null;
        n.preventDefault();
        n.stopPropagation();
        e.transition(100);
        r.transition(100);
        i.setDragPosition(n);
        clearTimeout(t.scrollbar.dragTimeout);
        u.transition(0);
        f.hide && u.css("opacity", 1);
        t.emit("scrollbarDragStart", n)
      },
      onDragMove: function (n) {
        var t = this,
          i = t.scrollbar,
          r = t.$wrapperEl,
          u = i.$el,
          f = i.$dragEl;
        t.scrollbar.isTouched && (n.preventDefault ? n.preventDefault() : n.returnValue = !1, i.setDragPosition(n), r.transition(0), u.transition(0), f.transition(0), t.emit("scrollbarDragMove", n))
      },
      onDragEnd: function (t) {
        var i = this,
          r = i.params.scrollbar,
          f = i.scrollbar,
          u = f.$el;
        i.scrollbar.isTouched && (i.scrollbar.isTouched = !1, r.hide && (clearTimeout(i.scrollbar.dragTimeout), i.scrollbar.dragTimeout = n.nextTick(function () {
          u.css("opacity", 0);
          u.transition(400)
        }, 1e3)), i.emit("scrollbarDragEnd", t), r.snapOnRelease && i.slideToClosest())
      },
      enableDraggable: function () {
        var n = this;
        if (n.params.scrollbar.el) {
          var h = n.scrollbar,
            f = n.touchEventsTouch,
            e = n.touchEventsDesktop,
            o = n.params,
            c = h.$el,
            t = c[0],
            i = r.passiveListener && o.passiveListeners ? {
              passive: !1,
              capture: !1
            } : !1,
            s = r.passiveListener && o.passiveListeners ? {
              passive: !0,
              capture: !1
            } : !1;
          r.touch ? (t.addEventListener(f.start, n.scrollbar.onDragStart, i), t.addEventListener(f.move, n.scrollbar.onDragMove, i), t.addEventListener(f.end, n.scrollbar.onDragEnd, s)) : (t.addEventListener(e.start, n.scrollbar.onDragStart, i), u.addEventListener(e.move, n.scrollbar.onDragMove, i), u.addEventListener(e.end, n.scrollbar.onDragEnd, s))
        }
      },
      disableDraggable: function () {
        var n = this;
        if (n.params.scrollbar.el) {
          var h = n.scrollbar,
            f = n.touchEventsTouch,
            e = n.touchEventsDesktop,
            o = n.params,
            c = h.$el,
            t = c[0],
            i = r.passiveListener && o.passiveListeners ? {
              passive: !1,
              capture: !1
            } : !1,
            s = r.passiveListener && o.passiveListeners ? {
              passive: !0,
              capture: !1
            } : !1;
          r.touch ? (t.removeEventListener(f.start, n.scrollbar.onDragStart, i), t.removeEventListener(f.move, n.scrollbar.onDragMove, i), t.removeEventListener(f.end, n.scrollbar.onDragEnd, s)) : (t.removeEventListener(e.start, n.scrollbar.onDragStart, i), u.removeEventListener(e.move, n.scrollbar.onDragMove, i), u.removeEventListener(e.end, n.scrollbar.onDragEnd, s))
        }
      },
      init: function () {
        var i = this,
          u;
        if (i.params.scrollbar.el) {
          var e = i.scrollbar,
            o = i.$el,
            f = i.params.scrollbar,
            r = t(f.el);
          i.params.uniqueNavElements && typeof f.el == "string" && r.length > 1 && o.find(f.el).length === 1 && (r = o.find(f.el));
          u = r.find("." + i.params.scrollbar.dragClass);
          u.length === 0 && (u = t('<div class="' + i.params.scrollbar.dragClass + '"><\/div>'), r.append(u));
          n.extend(e, {
            $el: r,
            el: r[0],
            $dragEl: u,
            dragEl: u[0]
          });
          f.draggable && e.enableDraggable()
        }
      },
      destroy: function () {
        var n = this;
        n.scrollbar.disableDraggable()
      }
    },
    ge = {
      name: "scrollbar",
      params: {
        scrollbar: {
          el: null,
          dragSize: "auto",
          hide: !1,
          draggable: !1,
          snapOnRelease: !0,
          lockClass: "swiper-scrollbar-lock",
          dragClass: "swiper-scrollbar-drag"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          scrollbar: {
            init: o.init.bind(t),
            destroy: o.destroy.bind(t),
            updateSize: o.updateSize.bind(t),
            setTranslate: o.setTranslate.bind(t),
            setTransition: o.setTransition.bind(t),
            enableDraggable: o.enableDraggable.bind(t),
            disableDraggable: o.disableDraggable.bind(t),
            setDragPosition: o.setDragPosition.bind(t),
            getPointerPosition: o.getPointerPosition.bind(t),
            onDragStart: o.onDragStart.bind(t),
            onDragMove: o.onDragMove.bind(t),
            onDragEnd: o.onDragEnd.bind(t),
            isTouched: !1,
            timeout: null,
            dragTimeout: null
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.scrollbar.init();
          n.scrollbar.updateSize();
          n.scrollbar.setTranslate()
        },
        update: function () {
          var n = this;
          n.scrollbar.updateSize()
        },
        resize: function () {
          var n = this;
          n.scrollbar.updateSize()
        },
        observerUpdate: function () {
          var n = this;
          n.scrollbar.updateSize()
        },
        setTranslate: function () {
          var n = this;
          n.scrollbar.setTranslate()
        },
        setTransition: function (n) {
          var t = this;
          t.scrollbar.setTransition(n)
        },
        destroy: function () {
          var n = this;
          n.scrollbar.destroy()
        }
      }
    },
    ft = {
      setTransform: function (n, i) {
        var s = this,
          v = s.rtl,
          f = t(n),
          h = v ? -1 : 1,
          c = f.attr("data-swiper-parallax") || "0",
          r = f.attr("data-swiper-parallax-x"),
          u = f.attr("data-swiper-parallax-y"),
          e = f.attr("data-swiper-parallax-scale"),
          o = f.attr("data-swiper-parallax-opacity"),
          l, a;
        r || u ? (r = r || "0", u = u || "0") : s.isHorizontal() ? (r = c, u = "0") : (u = c, r = "0");
        r = r.indexOf("%") >= 0 ? parseInt(r, 10) * i * h + "%" : r * i * h + "px";
        u = u.indexOf("%") >= 0 ? parseInt(u, 10) * i + "%" : u * i + "px";
        typeof o != "undefined" && o !== null && (l = o - (o - 1) * (1 - Math.abs(i)), f[0].style.opacity = l);
        typeof e == "undefined" || e === null ? f.transform("translate3d(" + r + ", " + u + ", 0px)") : (a = e - (e - 1) * (1 - Math.abs(i)), f.transform("translate3d(" + r + ", " + u + ", 0px) scale(" + a + ")"))
      },
      setTranslate: function () {
        var n = this,
          r = n.$el,
          u = n.slides,
          i = n.progress,
          f = n.snapGrid;
        r.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (t, r) {
          n.parallax.setTransform(r, i)
        });
        u.each(function (r, u) {
          var e = u.progress;
          n.params.slidesPerGroup > 1 && n.params.slidesPerView !== "auto" && (e += Math.ceil(r / 2) - i * (f.length - 1));
          e = Math.min(Math.max(e, -1), 1);
          t(u).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (t, i) {
            n.parallax.setTransform(i, e)
          })
        })
      },
      setTransition: function (n) {
        n === void 0 && (n = this.params.speed);
        var i = this,
          r = i.$el;
        r.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (i, r) {
          var u = t(r),
            f = parseInt(u.attr("data-swiper-parallax-duration"), 10) || n;
          n === 0 && (f = 0);
          u.transition(f)
        })
      }
    },
    no = {
      name: "parallax",
      params: {
        parallax: {
          enabled: !1
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          parallax: {
            setTransform: ft.setTransform.bind(t),
            setTranslate: ft.setTranslate.bind(t),
            setTransition: ft.setTransition.bind(t)
          }
        })
      },
      on: {
        beforeInit: function () {
          var n = this;
          n.params.parallax.enabled && (n.params.watchSlidesProgress = !0, n.originalParams.watchSlidesProgress = !0)
        },
        init: function () {
          var n = this;
          n.params.parallax.enabled && n.parallax.setTranslate()
        },
        setTranslate: function () {
          var n = this;
          n.params.parallax.enabled && n.parallax.setTranslate()
        },
        setTransition: function (n) {
          var t = this;
          t.params.parallax.enabled && t.parallax.setTransition(n)
        }
      }
    },
    et = {
      getDistanceBetweenTouches: function (n) {
        if (n.targetTouches.length < 2) return 1;
        var t = n.targetTouches[0].pageX,
          i = n.targetTouches[0].pageY,
          r = n.targetTouches[1].pageX,
          u = n.targetTouches[1].pageY;
        return Math.sqrt(Math.pow(r - t, 2) + Math.pow(u - i, 2))
      },
      onGestureStart: function (n) {
        var u = this,
          e = u.params.zoom,
          f = u.zoom,
          i = f.gesture;
        if (f.fakeGestureTouched = !1, f.fakeGestureMoved = !1, !r.gestures) {
          if (n.type !== "touchstart" || n.type === "touchstart" && n.targetTouches.length < 2) return;
          f.fakeGestureTouched = !0;
          i.scaleStart = et.getDistanceBetweenTouches(n)
        }
        if ((!i.$slideEl || !i.$slideEl.length) && (i.$slideEl = t(n.target).closest(".swiper-slide"), i.$slideEl.length === 0 && (i.$slideEl = u.slides.eq(u.activeIndex)), i.$imageEl = i.$slideEl.find("img, svg, canvas"), i.$imageWrapEl = i.$imageEl.parent("." + e.containerClass), i.maxRatio = i.$imageWrapEl.attr("data-swiper-zoom") || e.maxRatio, i.$imageWrapEl.length === 0)) {
          i.$imageEl = undefined;
          return
        }
        i.$imageEl.transition(0);
        u.zoom.isScaling = !0
      },
      onGestureChange: function (n) {
        var f = this,
          u = f.params.zoom,
          t = f.zoom,
          i = t.gesture;
        if (!r.gestures) {
          if (n.type !== "touchmove" || n.type === "touchmove" && n.targetTouches.length < 2) return;
          t.fakeGestureMoved = !0;
          i.scaleMove = et.getDistanceBetweenTouches(n)
        }
        i.$imageEl && i.$imageEl.length !== 0 && (t.scale = r.gestures ? n.scale * t.currentScale : i.scaleMove / i.scaleStart * t.currentScale, t.scale > i.maxRatio && (t.scale = i.maxRatio - 1 + Math.pow(t.scale - i.maxRatio + 1, .5)), t.scale < u.minRatio && (t.scale = u.minRatio + 1 - Math.pow(u.minRatio - t.scale + 1, .5)), i.$imageEl.transform("translate3d(0,0,0) scale(" + t.scale + ")"))
      },
      onGestureEnd: function (n) {
        var u = this,
          f = u.params.zoom,
          t = u.zoom,
          i = t.gesture;
        if (!r.gestures) {
          if (!t.fakeGestureTouched || !t.fakeGestureMoved) return;
          if (n.type !== "touchend" || n.type === "touchend" && n.changedTouches.length < 2 && !e.android) return;
          t.fakeGestureTouched = !1;
          t.fakeGestureMoved = !1
        }
        i.$imageEl && i.$imageEl.length !== 0 && (t.scale = Math.max(Math.min(t.scale, i.maxRatio), f.minRatio), i.$imageEl.transition(u.params.speed).transform("translate3d(0,0,0) scale(" + t.scale + ")"), t.currentScale = t.scale, t.isScaling = !1, t.scale === 1 && (i.$slideEl = undefined))
      },
      onTouchStart: function (n) {
        var u = this,
          i = u.zoom,
          r = i.gesture,
          t = i.image;
        r.$imageEl && r.$imageEl.length !== 0 && (t.isTouched || (e.android && n.preventDefault(), t.isTouched = !0, t.touchesStart.x = n.type === "touchstart" ? n.targetTouches[0].pageX : n.pageX, t.touchesStart.y = n.type === "touchstart" ? n.targetTouches[0].pageY : n.pageY))
      },
      onTouchMove: function (t) {
        var e = this,
          f = e.zoom,
          u = f.gesture,
          i = f.image,
          r = f.velocity,
          o, s;
        if (u.$imageEl && u.$imageEl.length !== 0 && (e.allowClick = !1, i.isTouched && u.$slideEl) && (i.isMoved || (i.width = u.$imageEl[0].offsetWidth, i.height = u.$imageEl[0].offsetHeight, i.startX = n.getTranslate(u.$imageWrapEl[0], "x") || 0, i.startY = n.getTranslate(u.$imageWrapEl[0], "y") || 0, u.slideWidth = u.$slideEl[0].offsetWidth, u.slideHeight = u.$slideEl[0].offsetHeight, u.$imageWrapEl.transition(0), e.rtl && (i.startX = -i.startX, i.startY = -i.startY)), o = i.width * f.scale, s = i.height * f.scale, !(o < u.slideWidth) || !(s < u.slideHeight))) {
          if (i.minX = Math.min(u.slideWidth / 2 - o / 2, 0), i.maxX = -i.minX, i.minY = Math.min(u.slideHeight / 2 - s / 2, 0), i.maxY = -i.minY, i.touchesCurrent.x = t.type === "touchmove" ? t.targetTouches[0].pageX : t.pageX, i.touchesCurrent.y = t.type === "touchmove" ? t.targetTouches[0].pageY : t.pageY, !i.isMoved && !f.isScaling) {
            if (e.isHorizontal() && (Math.floor(i.minX) === Math.floor(i.startX) && i.touchesCurrent.x < i.touchesStart.x || Math.floor(i.maxX) === Math.floor(i.startX) && i.touchesCurrent.x > i.touchesStart.x)) {
              i.isTouched = !1;
              return
            }
            if (!e.isHorizontal() && (Math.floor(i.minY) === Math.floor(i.startY) && i.touchesCurrent.y < i.touchesStart.y || Math.floor(i.maxY) === Math.floor(i.startY) && i.touchesCurrent.y > i.touchesStart.y)) {
              i.isTouched = !1;
              return
            }
          }
          t.preventDefault();
          t.stopPropagation();
          i.isMoved = !0;
          i.currentX = i.touchesCurrent.x - i.touchesStart.x + i.startX;
          i.currentY = i.touchesCurrent.y - i.touchesStart.y + i.startY;
          i.currentX < i.minX && (i.currentX = i.minX + 1 - Math.pow(i.minX - i.currentX + 1, .8));
          i.currentX > i.maxX && (i.currentX = i.maxX - 1 + Math.pow(i.currentX - i.maxX + 1, .8));
          i.currentY < i.minY && (i.currentY = i.minY + 1 - Math.pow(i.minY - i.currentY + 1, .8));
          i.currentY > i.maxY && (i.currentY = i.maxY - 1 + Math.pow(i.currentY - i.maxY + 1, .8));
          r.prevPositionX || (r.prevPositionX = i.touchesCurrent.x);
          r.prevPositionY || (r.prevPositionY = i.touchesCurrent.y);
          r.prevTime || (r.prevTime = Date.now());
          r.x = (i.touchesCurrent.x - r.prevPositionX) / (Date.now() - r.prevTime) / 2;
          r.y = (i.touchesCurrent.y - r.prevPositionY) / (Date.now() - r.prevTime) / 2;
          Math.abs(i.touchesCurrent.x - r.prevPositionX) < 2 && (r.x = 0);
          Math.abs(i.touchesCurrent.y - r.prevPositionY) < 2 && (r.y = 0);
          r.prevPositionX = i.touchesCurrent.x;
          r.prevPositionY = i.touchesCurrent.y;
          r.prevTime = Date.now();
          u.$imageWrapEl.transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
        }
      },
      onTouchEnd: function () {
        var l = this,
          i = l.zoom,
          r = i.gesture,
          n = i.image,
          t = i.velocity,
          s, h, c;
        if (r.$imageEl && r.$imageEl.length !== 0) {
          if (!n.isTouched || !n.isMoved) {
            n.isTouched = !1;
            n.isMoved = !1;
            return
          }
          n.isTouched = !1;
          n.isMoved = !1;
          var u = 300,
            f = 300,
            a = t.x * u,
            e = n.currentX + a,
            v = t.y * f,
            o = n.currentY + v;
          t.x !== 0 && (u = Math.abs((e - n.currentX) / t.x));
          t.y !== 0 && (f = Math.abs((o - n.currentY) / t.y));
          s = Math.max(u, f);
          n.currentX = e;
          n.currentY = o;
          h = n.width * i.scale;
          c = n.height * i.scale;
          n.minX = Math.min(r.slideWidth / 2 - h / 2, 0);
          n.maxX = -n.minX;
          n.minY = Math.min(r.slideHeight / 2 - c / 2, 0);
          n.maxY = -n.minY;
          n.currentX = Math.max(Math.min(n.currentX, n.maxX), n.minX);
          n.currentY = Math.max(Math.min(n.currentY, n.maxY), n.minY);
          r.$imageWrapEl.transition(s).transform("translate3d(" + n.currentX + "px, " + n.currentY + "px,0)")
        }
      },
      onTransitionEnd: function () {
        var t = this,
          i = t.zoom,
          n = i.gesture;
        n.$slideEl && t.previousIndex !== t.activeIndex && (n.$imageEl.transform("translate3d(0,0,0) scale(1)"), n.$imageWrapEl.transform("translate3d(0,0,0)"), i.scale = 1, i.currentScale = 1, n.$slideEl = undefined, n.$imageEl = undefined, n.$imageWrapEl = undefined)
      },
      toggle: function (n) {
        var i = this,
          t = i.zoom;
        t.scale && t.scale !== 1 ? t.out() : t.in(n)
      },
      "in": function (n) {
        var e = this,
          r = e.zoom,
          o = e.params.zoom,
          i = r.gesture,
          c = r.image,
          l, a, b, k, d, g, u, f, nt, tt, it, rt, s, h, v, y, p, w;
        (i.$slideEl || (i.$slideEl = e.clickedSlide ? t(e.clickedSlide) : e.slides.eq(e.activeIndex), i.$imageEl = i.$slideEl.find("img, svg, canvas"), i.$imageWrapEl = i.$imageEl.parent("." + o.containerClass)), i.$imageEl && i.$imageEl.length !== 0) && (i.$slideEl.addClass("" + o.zoomedSlideClass), typeof c.touchesStart.x == "undefined" && n ? (l = n.type === "touchend" ? n.changedTouches[0].pageX : n.pageX, a = n.type === "touchend" ? n.changedTouches[0].pageY : n.pageY) : (l = c.touchesStart.x, a = c.touchesStart.y), r.scale = i.$imageWrapEl.attr("data-swiper-zoom") || o.maxRatio, r.currentScale = i.$imageWrapEl.attr("data-swiper-zoom") || o.maxRatio, n ? (p = i.$slideEl[0].offsetWidth, w = i.$slideEl[0].offsetHeight, b = i.$slideEl.offset().left, k = i.$slideEl.offset().top, d = b + p / 2 - l, g = k + w / 2 - a, nt = i.$imageEl[0].offsetWidth, tt = i.$imageEl[0].offsetHeight, it = nt * r.scale, rt = tt * r.scale, s = Math.min(p / 2 - it / 2, 0), h = Math.min(w / 2 - rt / 2, 0), v = -s, y = -h, u = d * r.scale, f = g * r.scale, u < s && (u = s), u > v && (u = v), f < h && (f = h), f > y && (f = y)) : (u = 0, f = 0), i.$imageWrapEl.transition(300).transform("translate3d(" + u + "px, " + f + "px,0)"), i.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + r.scale + ")"))
      },
      out: function () {
        var i = this,
          r = i.zoom,
          u = i.params.zoom,
          n = r.gesture;
        (n.$slideEl || (n.$slideEl = i.clickedSlide ? t(i.clickedSlide) : i.slides.eq(i.activeIndex), n.$imageEl = n.$slideEl.find("img, svg, canvas"), n.$imageWrapEl = n.$imageEl.parent("." + u.containerClass)), n.$imageEl && n.$imageEl.length !== 0) && (r.scale = 1, r.currentScale = 1, n.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), n.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), n.$slideEl.removeClass("" + u.zoomedSlideClass), n.$slideEl = undefined)
      },
      enable: function () {
        var n = this,
          t = n.zoom,
          i;
        if (!t.enabled) {
          if (t.enabled = !0, i = n.touchEvents.start === "touchstart" && r.passiveListener && n.params.passiveListeners ? {
              passive: !0,
              capture: !1
            } : !1, r.gestures) {
            n.$wrapperEl.on("gesturestart", ".swiper-slide", t.onGestureStart, i);
            n.$wrapperEl.on("gesturechange", ".swiper-slide", t.onGestureChange, i);
            n.$wrapperEl.on("gestureend", ".swiper-slide", t.onGestureEnd, i)
          } else if (n.touchEvents.start === "touchstart") {
            n.$wrapperEl.on(n.touchEvents.start, ".swiper-slide", t.onGestureStart, i);
            n.$wrapperEl.on(n.touchEvents.move, ".swiper-slide", t.onGestureChange, i);
            n.$wrapperEl.on(n.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)
          }
          n.$wrapperEl.on(n.touchEvents.move, "." + n.params.zoom.containerClass, t.onTouchMove)
        }
      },
      disable: function () {
        var n = this,
          t = n.zoom,
          i;
        t.enabled && (n.zoom.enabled = !1, i = n.touchEvents.start === "touchstart" && r.passiveListener && n.params.passiveListeners ? {
          passive: !0,
          capture: !1
        } : !1, r.gestures ? (n.$wrapperEl.off("gesturestart", ".swiper-slide", t.onGestureStart, i), n.$wrapperEl.off("gesturechange", ".swiper-slide", t.onGestureChange, i), n.$wrapperEl.off("gestureend", ".swiper-slide", t.onGestureEnd, i)) : n.touchEvents.start === "touchstart" && (n.$wrapperEl.off(n.touchEvents.start, ".swiper-slide", t.onGestureStart, i), n.$wrapperEl.off(n.touchEvents.move, ".swiper-slide", t.onGestureChange, i), n.$wrapperEl.off(n.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)), n.$wrapperEl.off(n.touchEvents.move, "." + n.params.zoom.containerClass, t.onTouchMove))
      }
    },
    to = {
      name: "zoom",
      params: {
        zoom: {
          enabled: !1,
          maxRatio: 3,
          minRatio: 1,
          toggle: !0,
          containerClass: "swiper-zoom-container",
          zoomedSlideClass: "swiper-slide-zoomed"
        }
      },
      create: function () {
        var t = this,
          r = {
            enabled: !1,
            scale: 1,
            currentScale: 1,
            isScaling: !1,
            gesture: {
              $slideEl: undefined,
              slideWidth: undefined,
              slideHeight: undefined,
              $imageEl: undefined,
              $imageWrapEl: undefined,
              maxRatio: 3
            },
            image: {
              isTouched: undefined,
              isMoved: undefined,
              currentX: undefined,
              currentY: undefined,
              minX: undefined,
              minY: undefined,
              maxX: undefined,
              maxY: undefined,
              width: undefined,
              height: undefined,
              startX: undefined,
              startY: undefined,
              touchesStart: {},
              touchesCurrent: {}
            },
            velocity: {
              x: undefined,
              y: undefined,
              prevPositionX: undefined,
              prevPositionY: undefined,
              prevTime: undefined
            }
          },
          i;
        "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function (n) {
          r[n] = et[n].bind(t)
        });
        n.extend(t, {
          zoom: r
        });
        i = 1;
        Object.defineProperty(t.zoom, "scale", {
          get: function () {
            return i
          },
          set: function (n) {
            if (i !== n) {
              var r = t.zoom.gesture.$imageEl ? t.zoom.gesture.$imageEl[0] : undefined,
                u = t.zoom.gesture.$slideEl ? t.zoom.gesture.$slideEl[0] : undefined;
              t.emit("zoomChange", n, r, u)
            }
            i = n
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.params.zoom.enabled && n.zoom.enable()
        },
        destroy: function () {
          var n = this;
          n.zoom.disable()
        },
        touchStart: function (n) {
          var t = this;
          if (t.zoom.enabled) t.zoom.onTouchStart(n)
        },
        touchEnd: function (n) {
          var t = this;
          if (t.zoom.enabled) t.zoom.onTouchEnd(n)
        },
        doubleTap: function (n) {
          var t = this;
          t.params.zoom.enabled && t.zoom.enabled && t.params.zoom.toggle && t.zoom.toggle(n)
        },
        transitionEnd: function () {
          var n = this;
          n.zoom.enabled && n.params.zoom.enabled && n.zoom.onTransitionEnd()
        }
      }
    },
    ti = {
      loadInSlide: function (n, i) {
        var r, u;
        if ((i === void 0 && (i = !0), r = this, u = r.params.lazy, typeof n != "undefined") && r.slides.length !== 0) {
          var o = r.virtual && r.params.virtual.enabled,
            f = o ? r.$wrapperEl.children("." + r.params.slideClass + '[data-swiper-slide-index="' + n + '"]') : r.slides.eq(n),
            e = f.find("." + u.elementClass + ":not(." + u.loadedClass + "):not(." + u.loadingClass + ")");
          (!f.hasClass(u.elementClass) || f.hasClass(u.loadedClass) || f.hasClass(u.loadingClass) || (e = e.add(f[0])), e.length !== 0) && e.each(function (n, e) {
            var o = t(e);
            o.addClass(u.loadingClass);
            var s = o.attr("data-background"),
              h = o.attr("data-src"),
              c = o.attr("data-srcset"),
              l = o.attr("data-sizes");
            r.loadImage(o[0], h || s, c, l, !1, function () {
              var n, t, e;
              typeof r != "undefined" && r !== null && r && (!r || r.params) && !r.destroyed && (s ? (o.css("background-image", 'url("' + s + '")'), o.removeAttr("data-background")) : (c && (o.attr("srcset", c), o.removeAttr("data-srcset")), l && (o.attr("sizes", l), o.removeAttr("data-sizes")), h && (o.attr("src", h), o.removeAttr("data-src"))), o.addClass(u.loadedClass).removeClass(u.loadingClass), f.find("." + u.preloaderClass).remove(), r.params.loop && i && (n = f.attr("data-swiper-slide-index"), f.hasClass(r.params.slideDuplicateClass) ? (t = r.$wrapperEl.children('[data-swiper-slide-index="' + n + '"]:not(.' + r.params.slideDuplicateClass + ")"), r.lazy.loadInSlide(t.index(), !1)) : (e = r.$wrapperEl.children("." + r.params.slideDuplicateClass + '[data-swiper-slide-index="' + n + '"]'), r.lazy.loadInSlide(e.index(), !1))), r.emit("lazyImageReady", f[0], o[0]))
            });
            r.emit("lazyImageLoad", f[0], o[0])
          })
        }
      },
      load: function () {
        function l(n) {
          if (c) {
            if (s.children("." + i.slideClass + '[data-swiper-slide-index="' + n + '"]').length) return !0
          } else if (p[n]) return !0;
          return !1
        }

        function w(n) {
          return c ? t(n).attr("data-swiper-slide-index") : t(n).index()
        }
        var n = this,
          s = n.$wrapperEl,
          i = n.params,
          p = n.slides,
          r = n.activeIndex,
          c = n.virtual && i.virtual.enabled,
          h = i.lazy,
          u = i.slidesPerView,
          f, e, o, v, y;
        if (u === "auto" && (u = 0), n.lazy.initialImageLoaded || (n.lazy.initialImageLoaded = !0), n.params.watchSlidesVisibility) s.children("." + i.slideVisibleClass).each(function (i, r) {
          var u = c ? t(r).attr("data-swiper-slide-index") : t(r).index();
          n.lazy.loadInSlide(u)
        });
        else if (u > 1)
          for (f = r; f < r + u; f += 1) l(f) && n.lazy.loadInSlide(f);
        else n.lazy.loadInSlide(r);
        if (h.loadPrevNext)
          if (u > 1 || h.loadPrevNextAmount && h.loadPrevNextAmount > 1) {
            var b = h.loadPrevNextAmount,
              a = u,
              k = Math.min(r + a + Math.max(b, a), p.length),
              d = Math.max(r - Math.max(a, b), 0);
            for (e = r + u; e < k; e += 1) l(e) && n.lazy.loadInSlide(e);
            for (o = d; o < r; o += 1) l(o) && n.lazy.loadInSlide(o)
          } else v = s.children("." + i.slideNextClass), v.length > 0 && n.lazy.loadInSlide(w(v)), y = s.children("." + i.slidePrevClass), y.length > 0 && n.lazy.loadInSlide(w(y))
      }
    },
    io = {
      name: "lazy",
      params: {
        lazy: {
          enabled: !1,
          loadPrevNext: !1,
          loadPrevNextAmount: 1,
          loadOnTransitionStart: !1,
          elementClass: "swiper-lazy",
          loadingClass: "swiper-lazy-loading",
          loadedClass: "swiper-lazy-loaded",
          preloaderClass: "swiper-lazy-preloader"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          lazy: {
            initialImageLoaded: !1,
            load: ti.load.bind(t),
            loadInSlide: ti.loadInSlide.bind(t)
          }
        })
      },
      on: {
        beforeInit: function () {
          var n = this;
          n.params.lazy.enabled && n.params.preloadImages && (n.params.preloadImages = !1)
        },
        init: function () {
          var n = this;
          n.params.lazy.enabled && !n.params.loop && n.params.initialSlide === 0 && n.lazy.load()
        },
        scroll: function () {
          var n = this;
          n.params.freeMode && !n.params.freeModeSticky && n.lazy.load()
        },
        resize: function () {
          var n = this;
          n.params.lazy.enabled && n.lazy.load()
        },
        scrollbarDragMove: function () {
          var n = this;
          n.params.lazy.enabled && n.lazy.load()
        },
        transitionStart: function () {
          var n = this;
          n.params.lazy.enabled && (!n.params.lazy.loadOnTransitionStart && (n.params.lazy.loadOnTransitionStart || n.lazy.initialImageLoaded) || n.lazy.load())
        },
        transitionEnd: function () {
          var n = this;
          n.params.lazy.enabled && !n.params.lazy.loadOnTransitionStart && n.lazy.load()
        }
      }
    },
    p = {
      LinearSpline: function (n, t) {
        var u = function () {
            var n, t, i;
            return function (r, u) {
              for (t = -1, n = r.length; n - t > 1;) i = n + t >> 1, r[i] <= u ? t = i : n = i;
              return n
            }
          }(),
          i, r;
        return this.x = n, this.y = t, this.lastIndex = n.length - 1, this.interpolate = function (n) {
          return n ? (r = u(this.x, n), i = r - 1, (n - this.x[i]) * (this.y[r] - this.y[i]) / (this.x[r] - this.x[i]) + this.y[i]) : 0
        }, this
      },
      getInterpolateFunction: function (n) {
        var t = this;
        t.controller.spline || (t.controller.spline = t.params.loop ? new p.LinearSpline(t.slidesGrid, n.slidesGrid) : new p.LinearSpline(t.snapGrid, n.snapGrid))
      },
      setTranslate: function (n, t) {
        function o(n) {
          var t = i.rtlTranslate ? -i.translate : i.translate;
          i.params.controller.by === "slide" && (i.controller.getInterpolateFunction(n), u = -i.controller.spline.interpolate(-t));
          u && i.params.controller.by !== "container" || (e = (n.maxTranslate() - n.minTranslate()) / (i.maxTranslate() - i.minTranslate()), u = (t - i.minTranslate()) * e + n.minTranslate());
          i.params.controller.inverse && (u = n.maxTranslate() - u);
          n.updateProgress(u);
          n.setTranslate(u, i);
          n.updateActiveIndex();
          n.updateSlidesClasses()
        }
        var i = this,
          r = i.controller.control,
          e, u, f;
        if (Array.isArray(r))
          for (f = 0; f < r.length; f += 1) r[f] !== t && r[f] instanceof s && o(r[f]);
        else r instanceof s && t !== r && o(r)
      },
      setTransition: function (t, i) {
        function e(i) {
          i.setTransition(t, f);
          t !== 0 && (i.transitionStart(), i.params.autoHeight && n.nextTick(function () {
            i.updateAutoHeight()
          }), i.$wrapperEl.transitionEnd(function () {
            r && (i.params.loop && f.params.controller.by === "slide" && i.loopFix(), i.transitionEnd())
          }))
        }
        var f = this,
          r = f.controller.control,
          u;
        if (Array.isArray(r))
          for (u = 0; u < r.length; u += 1) r[u] !== i && r[u] instanceof s && e(r[u]);
        else r instanceof s && i !== r && e(r)
      }
    },
    ro = {
      name: "controller",
      params: {
        controller: {
          control: undefined,
          inverse: !1,
          by: "slide"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          controller: {
            control: t.params.controller.control,
            getInterpolateFunction: p.getInterpolateFunction.bind(t),
            setTranslate: p.setTranslate.bind(t),
            setTransition: p.setTransition.bind(t)
          }
        })
      },
      on: {
        update: function () {
          var n = this;
          n.controller.control && n.controller.spline && (n.controller.spline = undefined, delete n.controller.spline)
        },
        resize: function () {
          var n = this;
          n.controller.control && n.controller.spline && (n.controller.spline = undefined, delete n.controller.spline)
        },
        observerUpdate: function () {
          var n = this;
          n.controller.control && n.controller.spline && (n.controller.spline = undefined, delete n.controller.spline)
        },
        setTranslate: function (n, t) {
          var i = this;
          i.controller.control && i.controller.setTranslate(n, t)
        },
        setTransition: function (n, t) {
          var i = this;
          i.controller.control && i.controller.setTransition(n, t)
        }
      }
    },
    ii = {
      makeElFocusable: function (n) {
        return n.attr("tabIndex", "0"), n
      },
      addElRole: function (n, t) {
        return n.attr("role", t), n
      },
      addElLabel: function (n, t) {
        return n.attr("aria-label", t), n
      },
      disableEl: function (n) {
        return n.attr("aria-disabled", !0), n
      },
      enableEl: function (n) {
        return n.attr("aria-disabled", !1), n
      },
      onEnterKey: function (n) {
        var i = this,
          u = i.params.a11y,
          r;
        n.keyCode === 13 && (r = t(n.target), i.navigation && i.navigation.$nextEl && r.is(i.navigation.$nextEl) && (i.isEnd && !i.params.loop || i.slideNext(), i.isEnd ? i.a11y.notify(u.lastSlideMessage) : i.a11y.notify(u.nextSlideMessage)), i.navigation && i.navigation.$prevEl && r.is(i.navigation.$prevEl) && (i.isBeginning && !i.params.loop || i.slidePrev(), i.isBeginning ? i.a11y.notify(u.firstSlideMessage) : i.a11y.notify(u.prevSlideMessage)), i.pagination && r.is("." + i.params.pagination.bulletClass) && r[0].click())
      },
      notify: function (n) {
        var i = this,
          t = i.a11y.liveRegion;
        t.length !== 0 && (t.html(""), t.html(n))
      },
      updateNavigation: function () {
        var n = this;
        if (!n.params.loop) {
          var r = n.navigation,
            t = r.$nextEl,
            i = r.$prevEl;
          i && i.length > 0 && (n.isBeginning ? n.a11y.disableEl(i) : n.a11y.enableEl(i));
          t && t.length > 0 && (n.isEnd ? n.a11y.disableEl(t) : n.a11y.enableEl(t))
        }
      },
      updatePagination: function () {
        var n = this,
          i = n.params.a11y;
        n.pagination && n.params.pagination.clickable && n.pagination.bullets && n.pagination.bullets.length && n.pagination.bullets.each(function (r, u) {
          var f = t(u);
          n.a11y.makeElFocusable(f);
          n.a11y.addElRole(f, "button");
          n.a11y.addElLabel(f, i.paginationBulletMessage.replace(/{{index}}/, f.index() + 1))
        })
      },
      init: function () {
        var n = this,
          r, t, i;
        if (n.$el.append(n.a11y.liveRegion), r = n.params.a11y, n.navigation && n.navigation.$nextEl && (t = n.navigation.$nextEl), n.navigation && n.navigation.$prevEl && (i = n.navigation.$prevEl), t) {
          n.a11y.makeElFocusable(t);
          n.a11y.addElRole(t, "button");
          n.a11y.addElLabel(t, r.nextSlideMessage);
          t.on("keydown", n.a11y.onEnterKey)
        }
        if (i) {
          n.a11y.makeElFocusable(i);
          n.a11y.addElRole(i, "button");
          n.a11y.addElLabel(i, r.prevSlideMessage);
          i.on("keydown", n.a11y.onEnterKey)
        }
        if (n.pagination && n.params.pagination.clickable && n.pagination.bullets && n.pagination.bullets.length) n.pagination.$el.on("keydown", "." + n.params.pagination.bulletClass, n.a11y.onEnterKey)
      },
      destroy: function () {
        var n = this,
          t, i;
        n.a11y.liveRegion && n.a11y.liveRegion.length > 0 && n.a11y.liveRegion.remove();
        n.navigation && n.navigation.$nextEl && (t = n.navigation.$nextEl);
        n.navigation && n.navigation.$prevEl && (i = n.navigation.$prevEl);
        t && t.off("keydown", n.a11y.onEnterKey);
        i && i.off("keydown", n.a11y.onEnterKey);
        n.pagination && n.params.pagination.clickable && n.pagination.bullets && n.pagination.bullets.length && n.pagination.$el.off("keydown", "." + n.params.pagination.bulletClass, n.a11y.onEnterKey)
      }
    },
    uo = {
      name: "a11y",
      params: {
        a11y: {
          enabled: !0,
          notificationClass: "swiper-notification",
          prevSlideMessage: "Previous slide",
          nextSlideMessage: "Next slide",
          firstSlideMessage: "This is the first slide",
          lastSlideMessage: "This is the last slide",
          paginationBulletMessage: "Go to slide {{index}}"
        }
      },
      create: function () {
        var i = this;
        n.extend(i, {
          a11y: {
            liveRegion: t('<span class="' + i.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"><\/span>')
          }
        });
        Object.keys(ii).forEach(function (n) {
          i.a11y[n] = ii[n].bind(i)
        })
      },
      on: {
        init: function () {
          var n = this;
          n.params.a11y.enabled && (n.a11y.init(), n.a11y.updateNavigation())
        },
        toEdge: function () {
          var n = this;
          n.params.a11y.enabled && n.a11y.updateNavigation()
        },
        fromEdge: function () {
          var n = this;
          n.params.a11y.enabled && n.a11y.updateNavigation()
        },
        paginationUpdate: function () {
          var n = this;
          n.params.a11y.enabled && n.a11y.updatePagination()
        },
        destroy: function () {
          var n = this;
          n.params.a11y.enabled && n.a11y.destroy()
        }
      }
    },
    a = {
      init: function () {
        var n = this,
          t;
        if (n.params.history) {
          if (!i.history || !i.history.pushState) {
            n.params.history.enabled = !1;
            n.params.hashNavigation.enabled = !0;
            return
          }(t = n.history, t.initialized = !0, t.paths = a.getPathValues(), t.paths.key || t.paths.value) && (t.scrollToSlide(0, t.paths.value, n.params.runCallbacksOnInit), n.params.history.replaceState || i.addEventListener("popstate", n.history.setHistoryPopState))
        }
      },
      destroy: function () {
        var n = this;
        n.params.history.replaceState || i.removeEventListener("popstate", n.history.setHistoryPopState)
      },
      setHistoryPopState: function () {
        var n = this;
        n.history.paths = a.getPathValues();
        n.history.scrollToSlide(n.params.speed, n.history.paths.value, !1)
      },
      getPathValues: function () {
        var n = i.location.pathname.slice(1).split("/").filter(function (n) {
            return n !== ""
          }),
          t = n.length,
          r = n[t - 2],
          u = n[t - 1];
        return {
          key: r,
          value: u
        }
      },
      setHistory: function (n, t) {
        var u = this,
          e, r, f;
        u.history.initialized && u.params.history.enabled && ((e = u.slides.eq(t), r = a.slugify(e.attr("data-history")), i.location.pathname.includes(n) || (r = n + "/" + r), f = i.history.state, f && f.value === r) || (u.params.history.replaceState ? i.history.replaceState({
          value: r
        }, null, r) : i.history.pushState({
          value: r
        }, null, r)))
      },
      slugify: function (n) {
        return n.toString().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
      },
      scrollToSlide: function (n, t, i) {
        var r = this,
          u, e, f, o, s;
        if (t)
          for (u = 0, e = r.slides.length; u < e; u += 1) f = r.slides.eq(u), o = a.slugify(f.attr("data-history")), o !== t || f.hasClass(r.params.slideDuplicateClass) || (s = f.index(), r.slideTo(s, n, i));
        else r.slideTo(0, n, i)
      }
    },
    fo = {
      name: "history",
      params: {
        history: {
          enabled: !1,
          replaceState: !1,
          key: "slides"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          history: {
            init: a.init.bind(t),
            setHistory: a.setHistory.bind(t),
            setHistoryPopState: a.setHistoryPopState.bind(t),
            scrollToSlide: a.scrollToSlide.bind(t),
            destroy: a.destroy.bind(t)
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.params.history.enabled && n.history.init()
        },
        destroy: function () {
          var n = this;
          n.params.history.enabled && n.history.destroy()
        },
        transitionEnd: function () {
          var n = this;
          n.history.initialized && n.history.setHistory(n.params.history.key, n.activeIndex)
        }
      }
    },
    k = {
      onHashCange: function () {
        var n = this,
          i = u.location.hash.replace("#", ""),
          r = n.slides.eq(n.activeIndex).attr("data-hash"),
          t;
        if (i !== r) {
          if (t = n.$wrapperEl.children("." + n.params.slideClass + '[data-hash="' + i + '"]').index(), typeof t == "undefined") return;
          n.slideTo(t)
        }
      },
      setHash: function () {
        var n = this,
          t, r;
        n.hashNavigation.initialized && n.params.hashNavigation.enabled && (n.params.hashNavigation.replaceState && i.history && i.history.replaceState ? i.history.replaceState(null, null, "#" + n.slides.eq(n.activeIndex).attr("data-hash") || "") : (t = n.slides.eq(n.activeIndex), r = t.attr("data-hash") || t.attr("data-history"), u.location.hash = r || ""))
      },
      init: function () {
        var n = this,
          e, o, f, s, r, h, c;
        if (n.params.hashNavigation.enabled && (!n.params.history || !n.params.history.enabled)) {
          if (n.hashNavigation.initialized = !0, e = u.location.hash.replace("#", ""), e)
            for (o = 0, f = 0, s = n.slides.length; f < s; f += 1) r = n.slides.eq(f), h = r.attr("data-hash") || r.attr("data-history"), h !== e || r.hasClass(n.params.slideDuplicateClass) || (c = r.index(), n.slideTo(c, o, n.params.runCallbacksOnInit, !0));
          if (n.params.hashNavigation.watchState) t(i).on("hashchange", n.hashNavigation.onHashCange)
        }
      },
      destroy: function () {
        var n = this;
        n.params.hashNavigation.watchState && t(i).off("hashchange", n.hashNavigation.onHashCange)
      }
    },
    eo = {
      name: "hash-navigation",
      params: {
        hashNavigation: {
          enabled: !1,
          replaceState: !1,
          watchState: !1
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          hashNavigation: {
            initialized: !1,
            init: k.init.bind(t),
            destroy: k.destroy.bind(t),
            setHash: k.setHash.bind(t),
            onHashCange: k.onHashCange.bind(t)
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.params.hashNavigation.enabled && n.hashNavigation.init()
        },
        destroy: function () {
          var n = this;
          n.params.hashNavigation.enabled && n.hashNavigation.destroy()
        },
        transitionEnd: function () {
          var n = this;
          n.hashNavigation.initialized && n.hashNavigation.setHash()
        }
      }
    },
    d = {
      run: function () {
        var t = this,
          i = t.slides.eq(t.activeIndex),
          r = t.params.autoplay.delay;
        i.attr("data-swiper-autoplay") && (r = i.attr("data-swiper-autoplay") || t.params.autoplay.delay);
        clearTimeout(t.autoplay.timeout);
        t.autoplay.timeout = n.nextTick(function () {
          t.params.autoplay.reverseDirection ? t.params.loop ? (t.loopFix(), t.slidePrev(t.params.speed, !0, !0), t.emit("autoplay")) : t.isBeginning ? t.params.autoplay.stopOnLastSlide ? t.autoplay.stop() : (t.slideTo(t.slides.length - 1, t.params.speed, !0, !0), t.emit("autoplay")) : (t.slidePrev(t.params.speed, !0, !0), t.emit("autoplay")) : t.params.loop ? (t.loopFix(), t.slideNext(t.params.speed, !0, !0), t.emit("autoplay")) : t.isEnd ? t.params.autoplay.stopOnLastSlide ? t.autoplay.stop() : (t.slideTo(0, t.params.speed, !0, !0), t.emit("autoplay")) : (t.slideNext(t.params.speed, !0, !0), t.emit("autoplay"))
        }, r)
      },
      start: function () {
        var n = this;
        return typeof n.autoplay.timeout != "undefined" ? !1 : n.autoplay.running ? !1 : (n.autoplay.running = !0, n.emit("autoplayStart"), n.autoplay.run(), !0)
      },
      stop: function () {
        var n = this;
        return n.autoplay.running ? typeof n.autoplay.timeout == "undefined" ? !1 : (n.autoplay.timeout && (clearTimeout(n.autoplay.timeout), n.autoplay.timeout = undefined), n.autoplay.running = !1, n.emit("autoplayStop"), !0) : !1
      },
      pause: function (n) {
        var t = this;
        t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout), t.autoplay.paused = !0, n !== 0 && t.params.autoplay.waitForTransition ? (t.$wrapperEl[0].addEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].addEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd)) : (t.autoplay.paused = !1, t.autoplay.run())))
      }
    },
    oo = {
      name: "autoplay",
      params: {
        autoplay: {
          enabled: !1,
          delay: 3e3,
          waitForTransition: !0,
          disableOnInteraction: !0,
          stopOnLastSlide: !1,
          reverseDirection: !1
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          autoplay: {
            running: !1,
            paused: !1,
            run: d.run.bind(t),
            start: d.start.bind(t),
            stop: d.stop.bind(t),
            pause: d.pause.bind(t),
            onTransitionEnd: function (n) {
              t && !t.destroyed && t.$wrapperEl && n.target === this && (t.$wrapperEl[0].removeEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].removeEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd), t.autoplay.paused = !1, t.autoplay.running ? t.autoplay.run() : t.autoplay.stop())
            }
          }
        })
      },
      on: {
        init: function () {
          var n = this;
          n.params.autoplay.enabled && n.autoplay.start()
        },
        beforeTransitionStart: function (n, t) {
          var i = this;
          i.autoplay.running && (t || !i.params.autoplay.disableOnInteraction ? i.autoplay.pause(n) : i.autoplay.stop())
        },
        sliderFirstMove: function () {
          var n = this;
          n.autoplay.running && (n.params.autoplay.disableOnInteraction ? n.autoplay.stop() : n.autoplay.pause())
        },
        destroy: function () {
          var n = this;
          n.autoplay.running && n.autoplay.stop()
        }
      }
    },
    ri = {
      setTranslate: function () {
        for (var u, f, n = this, e = n.slides, r = 0; r < e.length; r += 1) {
          var t = n.slides.eq(r),
            o = t[0].swiperSlideOffset,
            i = -o;
          n.params.virtualTranslate || (i -= n.translate);
          u = 0;
          n.isHorizontal() || (u = i, i = 0);
          f = n.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(t[0].progress), 0) : 1 + Math.min(Math.max(t[0].progress, -1), 0);
          t.css({
            opacity: f
          }).transform("translate3d(" + i + "px, " + u + "px, 0px)")
        }
      },
      setTransition: function (n) {
        var t = this,
          r = t.slides,
          u = t.$wrapperEl,
          i;
        r.transition(n);
        t.params.virtualTranslate && n !== 0 && (i = !1, r.transitionEnd(function () {
          var r, n;
          if (!i && t && !t.destroyed)
            for (i = !0, t.animating = !1, r = ["webkitTransitionEnd", "transitionend"], n = 0; n < r.length; n += 1) u.trigger(r[n])
        }))
      }
    },
    so = {
      name: "effect-fade",
      params: {
        fadeEffect: {
          crossFade: !1
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          fadeEffect: {
            setTranslate: ri.setTranslate.bind(t),
            setTransition: ri.setTransition.bind(t)
          }
        })
      },
      on: {
        beforeInit: function () {
          var t = this,
            i;
          t.params.effect === "fade" && (t.classNames.push(t.params.containerModifierClass + "fade"), i = {
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: !0,
            spaceBetween: 0,
            virtualTranslate: !0
          }, n.extend(t.params, i), n.extend(t.originalParams, i))
        },
        setTranslate: function () {
          var n = this;
          n.params.effect === "fade" && n.fadeEffect.setTranslate()
        },
        setTransition: function (n) {
          var t = this;
          t.params.effect === "fade" && t.fadeEffect.setTransition(n)
        }
      }
    },
    ui = {
      setTranslate: function () {
        var n = this,
          nt = n.$el,
          k = n.$wrapperEl,
          tt = n.slides,
          d = n.width,
          it = n.height,
          g = n.rtlTranslate,
          i = n.size,
          s = n.params.cubeEffect,
          f = n.isHorizontal(),
          st = n.virtual && n.params.virtual.enabled,
          l = 0,
          r, w, u, e, c, a, ut, y, p, ot;
        for (s.shadow && (f ? (r = k.find(".swiper-cube-shadow"), r.length === 0 && (r = t('<div class="swiper-cube-shadow"><\/div>'), k.append(r)), r.css({
            height: d + "px"
          })) : (r = nt.find(".swiper-cube-shadow"), r.length === 0 && (r = t('<div class="swiper-cube-shadow"><\/div>'), nt.append(r)))), w = 0; w < tt.length; w += 1) {
          u = tt.eq(w);
          e = w;
          st && (e = parseInt(u.attr("data-swiper-slide-index"), 10));
          c = e * 90;
          a = Math.floor(c / 360);
          g && (c = -c, a = Math.floor(-c / 360));
          var v = Math.max(Math.min(u[0].progress, 1), -1),
            o = 0,
            rt = 0,
            b = 0;
          e % 4 == 0 ? (o = -a * 4 * i, b = 0) : (e - 1) % 4 == 0 ? (o = 0, b = -a * 4 * i) : (e - 2) % 4 == 0 ? (o = i + a * 4 * i, b = i) : (e - 3) % 4 == 0 && (o = -i, b = 3 * i + i * 4 * a);
          g && (o = -o);
          f || (rt = o, o = 0);
          ut = "rotateX(" + (f ? 0 : -c) + "deg) rotateY(" + (f ? c : 0) + "deg) translate3d(" + o + "px, " + rt + "px, " + b + "px)";
          v <= 1 && v > -1 && (l = e * 90 + v * 90, g && (l = -e * 90 - v * 90));
          u.transform(ut);
          s.slideShadows && (y = f ? u.find(".swiper-slide-shadow-left") : u.find(".swiper-slide-shadow-top"), p = f ? u.find(".swiper-slide-shadow-right") : u.find(".swiper-slide-shadow-bottom"), y.length === 0 && (y = t('<div class="swiper-slide-shadow-' + (f ? "left" : "top") + '"><\/div>'), u.append(y)), p.length === 0 && (p = t('<div class="swiper-slide-shadow-' + (f ? "right" : "bottom") + '"><\/div>'), u.append(p)), y.length && (y[0].style.opacity = Math.max(-v, 0)), p.length && (p[0].style.opacity = Math.max(v, 0)))
        }
        if (k.css({
            "-webkit-transform-origin": "50% 50% -" + i / 2 + "px",
            "-moz-transform-origin": "50% 50% -" + i / 2 + "px",
            "-ms-transform-origin": "50% 50% -" + i / 2 + "px",
            "transform-origin": "50% 50% -" + i / 2 + "px"
          }), s.shadow)
          if (f) r.transform("translate3d(0px, " + (d / 2 + s.shadowOffset) + "px, " + -d / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + s.shadowScale + ")");
          else {
            var ft = Math.abs(l) - Math.floor(Math.abs(l) / 90) * 90,
              ht = 1.5 - (Math.sin(ft * 2 * Math.PI / 360) / 2 + Math.cos(ft * 2 * Math.PI / 360) / 2),
              ct = s.shadowScale,
              et = s.shadowScale / ht,
              lt = s.shadowOffset;
            r.transform("scale3d(" + ct + ", 1, " + et + ") translate3d(0px, " + (it / 2 + lt) + "px, " + -it / 2 / et + "px) rotateX(-90deg)")
          } ot = h.isSafari || h.isUiWebView ? -i / 2 : 0;
        k.transform("translate3d(0px,0," + ot + "px) rotateX(" + (n.isHorizontal() ? 0 : l) + "deg) rotateY(" + (n.isHorizontal() ? -l : 0) + "deg)")
      },
      setTransition: function (n) {
        var t = this,
          i = t.$el,
          r = t.slides;
        r.transition(n).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(n);
        t.params.cubeEffect.shadow && !t.isHorizontal() && i.find(".swiper-cube-shadow").transition(n)
      }
    },
    ho = {
      name: "effect-cube",
      params: {
        cubeEffect: {
          slideShadows: !0,
          shadow: !0,
          shadowOffset: 20,
          shadowScale: .94
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          cubeEffect: {
            setTranslate: ui.setTranslate.bind(t),
            setTransition: ui.setTransition.bind(t)
          }
        })
      },
      on: {
        beforeInit: function () {
          var t = this,
            i;
          t.params.effect === "cube" && (t.classNames.push(t.params.containerModifierClass + "cube"), t.classNames.push(t.params.containerModifierClass + "3d"), i = {
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: !0,
            resistanceRatio: 0,
            spaceBetween: 0,
            centeredSlides: !1,
            virtualTranslate: !0
          }, n.extend(t.params, i), n.extend(t.originalParams, i))
        },
        setTranslate: function () {
          var n = this;
          n.params.effect === "cube" && n.cubeEffect.setTranslate()
        },
        setTransition: function (n) {
          var t = this;
          t.params.effect === "cube" && t.cubeEffect.setTransition(n)
        }
      }
    },
    fi = {
      setTranslate: function () {
        for (var n, r, u, f, i = this, o = i.slides, a = i.rtlTranslate, s = 0; s < o.length; s += 1) {
          n = o.eq(s);
          r = n[0].progress;
          i.params.flipEffect.limitRotation && (r = Math.max(Math.min(n[0].progress, 1), -1));
          var v = n[0].swiperSlideOffset,
            y = -180 * r,
            e = y,
            c = 0,
            h = -v,
            l = 0;
          i.isHorizontal() ? a && (e = -e) : (l = h, h = 0, c = -e, e = 0);
          n[0].style.zIndex = -Math.abs(Math.round(r)) + o.length;
          i.params.flipEffect.slideShadows && (u = i.isHorizontal() ? n.find(".swiper-slide-shadow-left") : n.find(".swiper-slide-shadow-top"), f = i.isHorizontal() ? n.find(".swiper-slide-shadow-right") : n.find(".swiper-slide-shadow-bottom"), u.length === 0 && (u = t('<div class="swiper-slide-shadow-' + (i.isHorizontal() ? "left" : "top") + '"><\/div>'), n.append(u)), f.length === 0 && (f = t('<div class="swiper-slide-shadow-' + (i.isHorizontal() ? "right" : "bottom") + '"><\/div>'), n.append(f)), u.length && (u[0].style.opacity = Math.max(-r, 0)), f.length && (f[0].style.opacity = Math.max(r, 0)));
          n.transform("translate3d(" + h + "px, " + l + "px, 0px) rotateX(" + c + "deg) rotateY(" + e + "deg)")
        }
      },
      setTransition: function (n) {
        var t = this,
          r = t.slides,
          u = t.activeIndex,
          f = t.$wrapperEl,
          i;
        r.transition(n).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(n);
        t.params.virtualTranslate && n !== 0 && (i = !1, r.eq(u).transitionEnd(function () {
          var r, n;
          if (!i && t && !t.destroyed)
            for (i = !0, t.animating = !1, r = ["webkitTransitionEnd", "transitionend"], n = 0; n < r.length; n += 1) f.trigger(r[n])
        }))
      }
    },
    co = {
      name: "effect-flip",
      params: {
        flipEffect: {
          slideShadows: !0,
          limitRotation: !0
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          flipEffect: {
            setTranslate: fi.setTranslate.bind(t),
            setTransition: fi.setTransition.bind(t)
          }
        })
      },
      on: {
        beforeInit: function () {
          var t = this,
            i;
          t.params.effect === "flip" && (t.classNames.push(t.params.containerModifierClass + "flip"), t.classNames.push(t.params.containerModifierClass + "3d"), i = {
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: !0,
            spaceBetween: 0,
            virtualTranslate: !0
          }, n.extend(t.params, i), n.extend(t.originalParams, i))
        },
        setTranslate: function () {
          var n = this;
          n.params.effect === "flip" && n.flipEffect.setTranslate()
        },
        setTransition: function (n) {
          var t = this;
          t.params.effect === "flip" && t.flipEffect.setTransition(n)
        }
      }
    },
    ei = {
      setTranslate: function () {
        for (var g, o, s, nt, f = this, tt = f.width, it = f.height, p = f.slides, rt = f.$wrapperEl, ut = f.slidesSizesGrid, e = f.params.coverflowEffect, n = f.isHorizontal(), w = f.translate, b = n ? -w + tt / 2 : -w + it / 2, k = n ? e.rotate : -e.rotate, ft = e.depth, h = 0, et = p.length; h < et; h += 1) {
          var u = p.eq(h),
            d = ut[h],
            ot = u[0].swiperSlideOffset,
            i = (b - ot - d / 2) / d * e.modifier,
            c = n ? k * i : 0,
            l = n ? 0 : k * i,
            a = -ft * Math.abs(i),
            v = n ? 0 : e.stretch * i,
            y = n ? e.stretch * i : 0;
          Math.abs(y) < .001 && (y = 0);
          Math.abs(v) < .001 && (v = 0);
          Math.abs(a) < .001 && (a = 0);
          Math.abs(c) < .001 && (c = 0);
          Math.abs(l) < .001 && (l = 0);
          g = "translate3d(" + y + "px," + v + "px," + a + "px)  rotateX(" + l + "deg) rotateY(" + c + "deg)";
          u.transform(g);
          u[0].style.zIndex = -Math.abs(Math.round(i)) + 1;
          e.slideShadows && (o = n ? u.find(".swiper-slide-shadow-left") : u.find(".swiper-slide-shadow-top"), s = n ? u.find(".swiper-slide-shadow-right") : u.find(".swiper-slide-shadow-bottom"), o.length === 0 && (o = t('<div class="swiper-slide-shadow-' + (n ? "left" : "top") + '"><\/div>'), u.append(o)), s.length === 0 && (s = t('<div class="swiper-slide-shadow-' + (n ? "right" : "bottom") + '"><\/div>'), u.append(s)), o.length && (o[0].style.opacity = i > 0 ? i : 0), s.length && (s[0].style.opacity = -i > 0 ? -i : 0))
        }(r.pointerEvents || r.prefixedPointerEvents) && (nt = rt[0].style, nt.perspectiveOrigin = b + "px 50%")
      },
      setTransition: function (n) {
        var t = this;
        t.slides.transition(n).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(n)
      }
    },
    lo = {
      name: "effect-coverflow",
      params: {
        coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: !0
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          coverflowEffect: {
            setTranslate: ei.setTranslate.bind(t),
            setTransition: ei.setTransition.bind(t)
          }
        })
      },
      on: {
        beforeInit: function () {
          var n = this;
          n.params.effect === "coverflow" && (n.classNames.push(n.params.containerModifierClass + "coverflow"), n.classNames.push(n.params.containerModifierClass + "3d"), n.params.watchSlidesProgress = !0, n.originalParams.watchSlidesProgress = !0)
        },
        setTranslate: function () {
          var n = this;
          n.params.effect === "coverflow" && n.coverflowEffect.setTranslate()
        },
        setTransition: function (n) {
          var t = this;
          t.params.effect === "coverflow" && t.coverflowEffect.setTransition(n)
        }
      }
    },
    ot = {
      init: function () {
        var t = this,
          u = t.params,
          i = u.thumbs,
          r = t.constructor;
        i.swiper instanceof r ? (t.thumbs.swiper = i.swiper, n.extend(t.thumbs.swiper.originalParams, {
          watchSlidesProgress: !0,
          slideToClickedSlide: !1
        }), n.extend(t.thumbs.swiper.params, {
          watchSlidesProgress: !0,
          slideToClickedSlide: !1
        })) : n.isObject(i.swiper) && (t.thumbs.swiper = new r(n.extend({}, i.swiper, {
          watchSlidesVisibility: !0,
          watchSlidesProgress: !0,
          slideToClickedSlide: !1
        })), t.thumbs.swiperCreated = !0);
        t.thumbs.swiper.$el.addClass(t.params.thumbs.thumbsContainerClass);
        t.thumbs.swiper.on("tap", t.thumbs.onThumbClick)
      },
      onThumbClick: function () {
        var n = this,
          r = n.thumbs.swiper,
          o, s, u, i, f, e;
        r && ((o = r.clickedIndex, s = r.clickedSlide, s && t(s).hasClass(n.params.thumbs.slideThumbActiveClass)) || typeof o != "undefined" && o !== null && (u = r.params.loop ? parseInt(t(r.clickedSlide).attr("data-swiper-slide-index"), 10) : o, n.params.loop && (i = n.activeIndex, n.slides.eq(i).hasClass(n.params.slideDuplicateClass) && (n.loopFix(), n._clientLeft = n.$wrapperEl[0].clientLeft, i = n.activeIndex), f = n.slides.eq(i).prevAll('[data-swiper-slide-index="' + u + '"]').eq(0).index(), e = n.slides.eq(i).nextAll('[data-swiper-slide-index="' + u + '"]').eq(0).index(), u = typeof f == "undefined" ? e : typeof e == "undefined" ? f : e - i < i - f ? e : f), n.slideTo(u)))
      },
      update: function (n) {
        var r = this,
          t = r.thumbs.swiper,
          o, i, u, f, e, s, h, c, l;
        if (t)
          if (o = t.params.slidesPerView === "auto" ? t.slidesPerViewDynamic() : t.params.slidesPerView, r.realIndex !== t.realIndex && (i = t.activeIndex, t.params.loop ? (t.slides.eq(i).hasClass(t.params.slideDuplicateClass) && (t.loopFix(), t._clientLeft = t.$wrapperEl[0].clientLeft, i = t.activeIndex), f = t.slides.eq(i).prevAll('[data-swiper-slide-index="' + r.realIndex + '"]').eq(0).index(), e = t.slides.eq(i).nextAll('[data-swiper-slide-index="' + r.realIndex + '"]').eq(0).index(), u = typeof f == "undefined" ? e : typeof e == "undefined" ? f : e - i == i - f ? i : e - i < i - f ? e : f) : u = r.realIndex, t.visibleSlidesIndexes && t.visibleSlidesIndexes.indexOf(u) < 0 && (t.params.centeredSlides ? u = u > i ? u - Math.floor(o / 2) + 1 : u + Math.floor(o / 2) - 1 : u > i && (u = u - o + 1), t.slideTo(u, n ? 0 : undefined))), s = 1, h = r.params.thumbs.slideThumbActiveClass, r.params.slidesPerView > 1 && !r.params.centeredSlides && (s = r.params.slidesPerView), t.slides.removeClass(h), t.params.loop || t.params.virtual && t.params.virtual.enabled)
            for (c = 0; c < s; c += 1) t.$wrapperEl.children('[data-swiper-slide-index="' + (r.realIndex + c) + '"]').addClass(h);
          else
            for (l = 0; l < s; l += 1) t.slides.eq(r.realIndex + l).addClass(h)
      }
    },
    ao = {
      name: "thumbs",
      params: {
        thumbs: {
          swiper: null,
          slideThumbActiveClass: "swiper-slide-thumb-active",
          thumbsContainerClass: "swiper-container-thumbs"
        }
      },
      create: function () {
        var t = this;
        n.extend(t, {
          thumbs: {
            swiper: null,
            init: ot.init.bind(t),
            update: ot.update.bind(t),
            onThumbClick: ot.onThumbClick.bind(t)
          }
        })
      },
      on: {
        beforeInit: function () {
          var n = this,
            i = n.params,
            t = i.thumbs;
          t && t.swiper && (n.thumbs.init(), n.thumbs.update(!0))
        },
        slideChange: function () {
          var n = this;
          n.thumbs.swiper && n.thumbs.update()
        },
        update: function () {
          var n = this;
          n.thumbs.swiper && n.thumbs.update()
        },
        resize: function () {
          var n = this;
          n.thumbs.swiper && n.thumbs.update()
        },
        observerUpdate: function () {
          var n = this;
          n.thumbs.swiper && n.thumbs.update()
        },
        setTransition: function (n) {
          var i = this,
            t = i.thumbs.swiper;
          t && t.setTransition(n)
        },
        beforeDestroy: function () {
          var t = this,
            n = t.thumbs.swiper;
          n && t.thumbs.swiperCreated && n && n.destroy()
        }
      }
    },
    vo = [he, ce, le, ae, ve, ye, pe, be, ke, de, ge, no, to, io, ro, uo, fo, eo, oo, so, ho, co, lo, ao];
  return typeof s.use == "undefined" && (s.use = s.Class.use, s.installModule = s.Class.installModule), s.use(vo), s
})
/*! npm.im/object-fit-images 3.2.4 */
var objectFitImages = function () {
  "use strict";

  function t(t, e) {
    return "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='" + t + "' height='" + e + "'%3E%3C/svg%3E"
  }

  function e(t) {
    if (t.srcset && !p && window.picturefill) {
      var e = window.picturefill._;
      t[e.ns] && t[e.ns].evaled || e.fillImg(t, {
        reselect: !0
      }), t[e.ns].curSrc || (t[e.ns].supported = !1, e.fillImg(t, {
        reselect: !0
      })), t.currentSrc = t[e.ns].curSrc || t.src
    }
  }

  function i(t) {
    for (var e, i = getComputedStyle(t).fontFamily, r = {}; null !== (e = u.exec(i));) r[e[1]] = e[2];
    return r
  }

  function r(e, i, r) {
    var n = t(i || 1, r || 0);
    b.call(e, "src") !== n && h.call(e, "src", n)
  }

  function n(t, e) {
    t.naturalWidth ? e(t) : setTimeout(n, 100, t, e)
  }

  function c(t) {
    var c = i(t),
      o = t[l];
    if (c["object-fit"] = c["object-fit"] || "fill", !o.img) {
      if ("fill" === c["object-fit"]) return;
      if (!o.skipTest && f && !c["object-position"]) return
    }
    if (!o.img) {
      o.img = new Image(t.width, t.height), o.img.srcset = b.call(t, "data-ofi-srcset") || t.srcset, o.img.src = b.call(t, "data-ofi-src") || t.src, h.call(t, "data-ofi-src", t.src), t.srcset && h.call(t, "data-ofi-srcset", t.srcset), r(t, t.naturalWidth || t.width, t.naturalHeight || t.height), t.srcset && (t.srcset = "");
      try {
        s(t)
      } catch (t) {
        window.console && console.warn("https://bit.ly/ofi-old-browser")
      }
    }
    e(o.img), t.style.backgroundImage = 'url("' + (o.img.currentSrc || o.img.src).replace(/"/g, '\\"') + '")', t.style.backgroundPosition = c["object-position"] || "center", t.style.backgroundRepeat = "no-repeat", t.style.backgroundOrigin = "content-box", /scale-down/.test(c["object-fit"]) ? n(o.img, function () {
      o.img.naturalWidth > t.width || o.img.naturalHeight > t.height ? t.style.backgroundSize = "contain" : t.style.backgroundSize = "auto"
    }) : t.style.backgroundSize = c["object-fit"].replace("none", "auto").replace("fill", "100% 100%"), n(o.img, function (e) {
      r(t, e.naturalWidth, e.naturalHeight)
    })
  }

  function s(t) {
    var e = {
      get: function (e) {
        return t[l].img[e ? e : "src"]
      },
      set: function (e, i) {
        return t[l].img[i ? i : "src"] = e, h.call(t, "data-ofi-" + i, e), c(t), e
      }
    };
    Object.defineProperty(t, "src", e), Object.defineProperty(t, "currentSrc", {
      get: function () {
        return e.get("currentSrc")
      }
    }), Object.defineProperty(t, "srcset", {
      get: function () {
        return e.get("srcset")
      },
      set: function (t) {
        return e.set(t, "srcset")
      }
    })
  }

  function o() {
    function t(t, e) {
      return t[l] && t[l].img && ("src" === e || "srcset" === e) ? t[l].img : t
    }
    d || (HTMLImageElement.prototype.getAttribute = function (e) {
      return b.call(t(this, e), e)
    }, HTMLImageElement.prototype.setAttribute = function (e, i) {
      return h.call(t(this, e), e, String(i))
    })
  }

  function a(t, e) {
    var i = !y && !t;
    if (e = e || {}, t = t || "img", d && !e.skipTest || !m) return !1;
    "img" === t ? t = document.getElementsByTagName("img") : "string" == typeof t ? t = document.querySelectorAll(t) : "length" in t || (t = [t]);
    for (var r = 0; r < t.length; r++) t[r][l] = t[r][l] || {
      skipTest: e.skipTest
    }, c(t[r]);
    i && (document.body.addEventListener("load", function (t) {
      "IMG" === t.target.tagName && a(t.target, {
        skipTest: e.skipTest
      })
    }, !0), y = !0, t = "img"), e.watchMQ && window.addEventListener("resize", a.bind(null, t, {
      skipTest: e.skipTest
    }))
  }
  var l = "fregante:object-fit-images",
    u = /(object-fit|object-position)\s*:\s*([-.\w\s%]+)/g,
    g = "undefined" == typeof Image ? {
      style: {
        "object-position": 1
      }
    } : new Image,
    f = "object-fit" in g.style,
    d = "object-position" in g.style,
    m = "background-size" in g.style,
    p = "string" == typeof g.currentSrc,
    b = g.getAttribute,
    h = g.setAttribute,
    y = !1;
  return a.supportsObjectFit = f, a.supportsObjectPosition = d, o(), a
}();

(function (w) {
  w.picturefill = function () {
    var ps = jQuery(".picturefill");
    for (var i = 0, il = ps.length; i < il; i++) {
      if (jQuery(ps[i]).hasClass('processed')) {
        continue;
      }
      if (ps[i].getAttribute("data-picture") !== null) {
        var sources = ps[i].getElementsByTagName("div"),
          matches = [];
        if (!sources.length) sources = ps[i].getElementsByTagName("span");
        for (var j = 0, jl = sources.length; j < jl; j++) {
          var media = sources[j].getAttribute("data-media");
          if (!media || w.matchMedia && w.matchMedia(media).matches) matches.push(sources[j])
        }
        var picImg = ps[i].getElementsByTagName("img")[0];
        if (matches.length) {
          if (!picImg ||
            picImg.parentNode.tagName.toUpperCase() == "NOSCRIPT") {
            picImg = w.document.createElement("img");
            if (ps[i].getAttribute("data-alt")) {
              picImg.alt = ps[i].getAttribute("data-alt");
            } else {
              picImg.alt = '';
            }
            if (ps[i].getAttribute("data-id")) {
              picImg.setAttribute("id", ps[i].getAttribute("data-id"));
            }
            if (ps[i].getAttribute("data-class")) {
              picImg.className = ps[i].getAttribute("data-class");
            }
            if (ps[i].getAttribute("data-style")) {
              picImg.setAttribute("style", ps[i].getAttribute("data-style"));
            }
            if (ps[i].getAttribute("data-title")) {
              picImg.title = ps[i].getAttribute("data-title");
            }
            picImg.width = ps[i].getAttribute("data-width");
            picImg.height = ps[i].getAttribute("data-height");
            ps[i].appendChild(picImg)
          }
          picImg.src =
            matches.pop().getAttribute("data-src");
          jQuery(ps[i]).addClass('processed');
        } else if (picImg) ps[i].removeChild(picImg)
      }
    }
  };
  if (w.addEventListener) {
    w.addEventListener("resize", w.picturefill, false);
    w.addEventListener("DOMContentLoaded", function () {
      w.picturefill();
      w.removeEventListener("load", w.picturefill, false)
    }, false);
    w.addEventListener("load", w.picturefill, false)
  } else if (w.attachEvent) w.attachEvent("onload", w.picturefill)
})(this);
(function (n) {
  n.easytabs = function (t, i) {
    var u = this,
      f = n(t),
      v = {
        animate: true,
        panelActiveClass: "active",
        tabActiveClass: "active",
        defaultTab: "li:first-child",
        animationSpeed: "normal",
        tabs: "> ul > li",
        updateHash: true,
        cycle: false,
        collapsible: false,
        collapsedClass: "collapsed",
        collapsedByDefault: true,
        uiTabs: false,
        transitionIn: "fadeIn",
        transitionOut: "fadeOut",
        transitionInEasing: "swing",
        transitionOutEasing: "swing",
        transitionCollapse: "slideUp",
        transitionUncollapse: "slideDown",
        transitionCollapseEasing: "swing",
        transitionUncollapseEasing: "swing",
        containerClass: "",
        tabsClass: "",
        tabClass: "",
        panelClass: "",
        cache: true,
        event: "click",
        panelContext: f
      },
      e, h, o, c, s, y = {
        fast: 200,
        normal: 400,
        slow: 600
      },
      r;
    u.init = function () {
      u.settings = r = n.extend({}, v, i);
      r.bind_str = r.event + ".easytabs";
      if (r.uiTabs) {
        r.tabActiveClass = "ui-tabs-selected";
        r.containerClass = "ui-tabs ui-widget ui-widget-content ui-corner-all";
        r.tabsClass = "ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all";
        r.tabClass = "ui-state-default ui-corner-top";
        r.panelClass = "ui-tabs-panel ui-widget-content ui-corner-bottom"
      }
      r.collapsible && i.defaultTab !== undefined && i.collpasedByDefault === undefined && (r.collapsedByDefault = false);
      typeof r.animationSpeed == "string" && (r.animationSpeed = y[r.animationSpeed]);
      n("a.anchor").remove().prependTo("body");
      f.data("easytabs", {});
      u.setTransitions();
      u.getTabs();
      p();
      w();
      k();
      nt();
      tt();
      f.attr("data-easytabs", true)
    };
    u.setTransitions = function () {
      o = r.animate ? {
        show: r.transitionIn,
        hide: r.transitionOut,
        speed: r.animationSpeed,
        collapse: r.transitionCollapse,
        uncollapse: r.transitionUncollapse,
        halfSpeed: r.animationSpeed / 2
      } : {
        show: "show",
        hide: "hide",
        speed: 0,
        collapse: "hide",
        uncollapse: "show",
        halfSpeed: 0
      }
    };
    u.getTabs = function () {
      var t;
      u.tabs = f.find(r.tabs);
      u.panels = n();
      u.tabs.each(function () {
        var f = n(this),
          e = f.children("a"),
          i = f.children("a").data("target");
        f.data("easytabs", {});
        i !== undefined && i !== null ? f.data("easytabs").ajax = e.attr("href") : i = e.attr("href");
        if (i.indexOf("#") >= 0) {
          i = i.match(/#([^\?]+)/)[1];
          t = r.panelContext.find("#" + i)
        } else {
          i = "";
          t = n()
        }
        if (t.length) {
          t.data("easytabs", {
            position: t.css("position"),
            visibility: t.css("visibility")
          });
          t.not(r.panelActiveClass).hide();
          u.panels = u.panels.add(t);
          f.data("easytabs").panel = t
        } else if (i) {
          u.tabs = u.tabs.not(f);
          "console" in window && console.warn("Warning: tab without matching panel for selector '#" + i + "' removed from set")
        }
      })
    };
    u.selectTab = function (n, t) {
      var e = window.location,
        o = e.hash.match(/^[^\?]*/)[0],
        i = n.parent().data("easytabs").panel,
        f = n.parent().data("easytabs").ajax;
      r.collapsible && !s && (n.hasClass(r.tabActiveClass) || n.hasClass(r.collapsedClass)) ? u.toggleTabCollapse(n, i, f, t) : n.hasClass(r.tabActiveClass) && i.hasClass(r.panelActiveClass) ? r.cache || a(n, i, f, t) : a(n, i, f, t)
    };
    u.toggleTabCollapse = function (n, t, i, e) {
      u.panels.stop(true, true);
      if (l(f, "easytabs:before", [n, t, r])) {
        u.tabs.filter("." + r.tabActiveClass).removeClass(r.tabActiveClass).children().removeClass(r.tabActiveClass);
        if (n.hasClass(r.collapsedClass)) {
          if (i && (!r.cache || !n.parent().data("easytabs").cached)) {
            f.trigger("easytabs:ajax:beforeSend", [n, t]);
            t.load(i, function (i, r, u) {
              n.parent().data("easytabs").cached = true;
              f.trigger("easytabs:ajax:complete", [n, t, i, r, u])
            })
          }
          n.parent().removeClass(r.collapsedClass).addClass(r.tabActiveClass).children().removeClass(r.collapsedClass).addClass(r.tabActiveClass);
          t.addClass(r.panelActiveClass)[o.uncollapse](o.speed, r.transitionUncollapseEasing, function () {
            f.trigger("easytabs:midTransition", [n, t, r]);
            typeof e == "function" && e()
          })
        } else {
          n.addClass(r.collapsedClass).parent().addClass(r.collapsedClass);
          t.removeClass(r.panelActiveClass)[o.collapse](o.speed, r.transitionCollapseEasing, function () {
            f.trigger("easytabs:midTransition", [n, t, r]);
            typeof e == "function" && e()
          })
        }
      }
    };
    u.matchTab = function (n) {
      return u.tabs.find("[href$='" + n + "'],[data-target$='" + n + "']").first()
    };
    u.matchInPanel = function (n) {
      return n && u.validId(n) ? u.panels.filter(":has(" + n + ")").first() : []
    };
    u.validId = function (n) {
      return n.substr(1).match(/^[A-Za-z]+[A-Za-z0-9\-_:\.].$/)
    };
    u.selectTabFromHashChange = function () {
      var n = window.location.hash.match(/^[^\?]*/)[0],
        t = u.matchTab(n),
        i;
      if (r.updateHash)
        if (t.length) {
          s = true;
          u.selectTab(t)
        } else {
          i = u.matchInPanel(n);
          if (i.length) {
            n = "#" + i.attr("id");
            t = u.matchTab(n);
            s = true;
            u.selectTab(t)
          } else if (!e.hasClass(r.tabActiveClass) && !r.cycle && (n === "" || u.matchTab(c).length || f.closest(n).length)) {
            s = true;
            u.selectTab(h)
          }
        }
    };
    u.cycleTabs = function (t) {
      if (r.cycle) {
        t = t % u.tabs.length;
        $tab = n(u.tabs[t]).children("a").first();
        s = true;
        u.selectTab($tab, function () {
          setTimeout(function () {
            u.cycleTabs(t + 1)
          }, r.cycle)
        })
      }
    };
    u.publicMethods = {
      select: function (t) {
        var i;
        (i = u.tabs.filter(t)).length === 0 ? (i = u.tabs.find("a[href='" + t + "']")).length === 0 && (i = u.tabs.find("a" + t)).length === 0 && (i = u.tabs.find("[data-target='" + t + "']")).length === 0 && (i = u.tabs.find("a[href$='" + t + "']")).length === 0 && n.error("Tab '" + t + "' does not exist in tab set") : i = i.children("a").first();
        u.selectTab(i)
      }
    };
    var l = function (t, i, r) {
        var u = n.Event(i);
        t.trigger(u, r);
        return u.result !== false
      },
      p = function () {
        f.addClass(r.containerClass);
        u.tabs.parent().addClass(r.tabsClass);
        u.tabs.addClass(r.tabClass);
        u.panels.addClass(r.panelClass)
      },
      w = function () {
        var t = window.location.hash.match(/^[^\?]*/)[0],
          i = u.matchTab(t).parent(),
          f;
        if (i.length === 1) {
          e = i;
          r.cycle = false
        } else {
          f = u.matchInPanel(t);
          if (f.length) {
            t = "#" + f.attr("id");
            e = u.matchTab(t).parent()
          } else {
            e = u.tabs.parent().find(r.defaultTab);
            e.length === 0 && n.error("The specified default tab ('" + r.defaultTab + "') could not be found in the tab set ('" + r.tabs + "') out of " + u.tabs.length + " tabs.")
          }
        }
        h = e.children("a").first();
        b(i)
      },
      b = function (t) {
        var i, u;
        if (r.collapsible && t.length === 0 && r.collapsedByDefault) e.addClass(r.collapsedClass).children().addClass(r.collapsedClass);
        else {
          i = n(e.data("easytabs").panel);
          u = e.data("easytabs").ajax;
          if (u && (!r.cache || !e.data("easytabs").cached)) {
            f.trigger("easytabs:ajax:beforeSend", [h, i]);
            i.load(u, function (n, t, r) {
              e.data("easytabs").cached = true;
              f.trigger("easytabs:ajax:complete", [h, i, n, t, r])
            })
          }
          e.data("easytabs").panel.show().addClass(r.panelActiveClass);
          e.addClass(r.tabActiveClass).children().addClass(r.tabActiveClass)
        }
        f.trigger("easytabs:initialised", [h, i])
      },
      k = function () {
        u.tabs.children("a").bind(r.bind_str, function (t) {
          r.cycle = false;
          s = false;
          u.selectTab(n(this));
          t.preventDefault ? t.preventDefault() : t.returnValue = false
        })
      },
      a = function (n, t, i, e) {
        u.panels.stop(true, true);
        if (l(f, "easytabs:before", [n, t, r])) {
          var a = u.panels.filter(":visible"),
            y, p, h, v, w = window.location.hash.match(/^[^\?]*/)[0];
          if (t) $panelContainer = t.parent();
          else {
            window.location.href = n[0].href;
            return true
          }
          if (r.animate) {
            y = d(t);
            p = a.length ? g(a) : 0;
            h = y - p
          }
          c = w;
          v = function () {
            f.trigger("easytabs:midTransition", [n, t, r]);
            r.animate && r.transitionIn == "fadeIn" && h < 0 && $panelContainer.animate({
              height: $panelContainer.height() + h
            }, o.halfSpeed).css({
              "min-height": ""
            });
            r.updateHash && !s ? window.history.pushState ? window.history.pushState(null, null, location.pathname + "#" + t.attr("id")) : window.location.hash = "#" + t.attr("id") : s = false;
            t[o.show](o.speed, r.transitionInEasing, function () {
              $panelContainer.css({
                height: "",
                "min-height": ""
              });
              f.trigger("easytabs:after", [n, t, r]);
              typeof e == "function" && e()
            })
          };
          if (i && (!r.cache || !n.parent().data("easytabs").cached)) {
            f.trigger("easytabs:ajax:beforeSend", [n, t]);
            t.load(i, function (i, r, u) {
              n.parent().data("easytabs").cached = true;
              f.trigger("easytabs:ajax:complete", [n, t, i, r, u])
            })
          }
          r.animate && r.transitionOut == "fadeOut" && (h > 0 ? $panelContainer.animate({
            height: $panelContainer.height() + h
          }, o.halfSpeed) : $panelContainer.css({
            "min-height": $panelContainer.height()
          }));
          u.tabs.filter("." + r.tabActiveClass).removeClass(r.tabActiveClass).children().removeClass(r.tabActiveClass);
          u.tabs.filter("." + r.collapsedClass).removeClass(r.collapsedClass).children().removeClass(r.collapsedClass);
          n.parent().addClass(r.tabActiveClass).children().addClass(r.tabActiveClass);
          u.panels.filter("." + r.panelActiveClass).removeClass(r.panelActiveClass);
          t.addClass(r.panelActiveClass);
          a.length ? a[o.hide](o.speed, r.transitionOutEasing, v) : t[o.uncollapse](o.speed, r.transitionUncollapseEasing, v)
        }
      },
      d = function (t) {
        if (t.data("easytabs") && t.data("easytabs").lastHeight) return t.data("easytabs").lastHeight;
        var u = t.css("display"),
          i, r;
        try {
          i = n("<div><\/div>", {
            position: "absolute",
            visibility: "hidden",
            overflow: "hidden"
          })
        } catch (f) {
          i = n("<div><\/div>", {
            visibility: "hidden",
            overflow: "hidden"
          })
        }
        r = t.wrap(i).css({
          position: "relative",
          visibility: "hidden",
          display: "block"
        }).outerHeight();
        t.unwrap();
        t.css({
          position: t.data("easytabs").position,
          visibility: t.data("easytabs").visibility,
          display: u
        });
        t.data("easytabs").lastHeight = r;
        return r
      },
      g = function (n) {
        var t = n.outerHeight();
        n.data("easytabs") ? n.data("easytabs").lastHeight = t : n.data("easytabs", {
          lastHeight: t
        });
        return t
      },
      nt = function () {
        typeof n(window).hashchange == "function" ? n(window).hashchange(function () {
          u.selectTabFromHashChange()
        }) : n.address && typeof n.address.change == "function" && n.address.change(function () {
          u.selectTabFromHashChange()
        })
      },
      tt = function () {
        var n;
        if (r.cycle) {
          n = u.tabs.index(e);
          setTimeout(function () {
            u.cycleTabs(n + 1)
          }, r.cycle)
        }
      };
    u.init()
  };
  n.fn.easytabs = function (t) {
    var i = arguments;
    return this.each(function () {
      var u = n(this),
        r = u.data("easytabs");
      if (undefined === r) {
        r = new n.easytabs(this, t);
        u.data("easytabs", r)
      }
      if (r.publicMethods[t]) return r.publicMethods[t](Array.prototype.slice.call(i, 1))
    })
  }
})(jQuery)